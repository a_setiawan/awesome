package com.rintis.marketing.core.app.main.module.qr;


import com.rintis.marketing.beans.bean.bi.*;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by aaw 16/06/2021.
 */

public interface IQRService {
    public List<PanTrxidBean> mPanListTrxId(String param) throws Exception;
    public List<QRAnomalyBean> listAnomalyQRAcq1H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public List<QRAnomalyBean> listAnomalyQRAcq3H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public List<QRAnomalyBean> listAnomalyQRAcq6H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public List<QRAnomalyBean> listAnomalyQRAcq1D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public List<QRAnomalyBean> listAnomalyQRAcq3D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public boolean checkQRAcqUsed(String pan, String acqId) throws Exception;
    public void saveMPanTreshold(String pan, String acquirerId, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree,
            BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) throws Exception;
    public List<DetailMPanAnomalyBean> listDetailMPanQR(String period, String timeFr, String timeTo, String pan, String qrType, String acquirerId, String periodTo, String periodType, String termId, String termName) throws Exception;

}

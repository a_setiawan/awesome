package com.rintis.marketing.core.app.main.module.bi;

import com.martinlinha.c3faces.script.property.Data;
import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.beans.bean.bi.*;
import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.*;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.dao.BiDao;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.MathUtils;
import com.rintis.marketing.core.utils.StringUtils;
import jodd.madvoc.meta.In;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

@Service
@Qualifier(SpringBeanConstant.SPRING_BEAN_BI_SERVICE)
@Named(SpringBeanConstant.SPRING_BEAN_BI_SERVICE)
public class BiService implements IBiService {
    private Logger log = Logger.getLogger(BiService.class);
    @Autowired
    private ModuleFactory moduleFactory;
    @Autowired
    private BiDao biDao;

    @Override
    @Transactional(readOnly = true)
    public List<EsqTrx5MinBean> listEsqTrx5Min(Map<String, Object> param) throws Exception {
        return biDao.listEsqTrx5Min(param);
    }

    @Override
    @Transactional(readOnly = true)
    public EsqTrx5MinBean getEsqTrx5Min(String bankId, String trxId, String period, String hourfr, String hourto,
                                        String timefr, String timeto) throws Exception {
        return biDao.getEsqTrx5Min(bankId, trxId, period, hourfr, hourto, timefr, timeto);
    }

    @Override
    @Transactional(readOnly = true)
    public EsqTrx5MinBean getEsqTrxHourly(String bankId, String trxId, String period, String hourfr, String hourto) throws Exception {
        return biDao.getEsqTrxHourly(bankId, trxId, period, hourfr, hourto);
    }

    @Override
    @Transactional(readOnly = true)
    public EsqTrx5MinBean getEsqTrx5MinWithPrev(String bankId, String trxId, String period, String prevPeriod, String hourfr, String hourto,
                                                String timefr, String timeto) throws Exception {
        return biDao.getEsqTrx5MinWithPrev(bankId, trxId, period, prevPeriod, hourfr, hourto, timefr, timeto);
    }

    @Override
    @Transactional(readOnly = true)
    public EsqTrx5MinBean getEsqTrxHourlyWithPrev(String bankId, String trxId, String period, String prevPeriod, String hourfr, String hourto) throws Exception {
        return biDao.getEsqTrxHourlyWithPrev(bankId, trxId, period, prevPeriod, hourfr, hourto);
    }

    @Override
    @Transactional(readOnly = true)
    public EsqTrx5MinBean getEsqTrxRekap(String acqId, String issId, String bnfId, String trxId, String period, String hourfr, String hourto) throws Exception {
        return biDao.getEsqTrxRekap(acqId, issId, bnfId, trxId, period, hourfr, hourto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BankAlertThresholdBean> listBankAlertThreshold(String trxId, String trxName, String sortby) throws Exception {
        List<BankAlertThresholdBean> list = new ArrayList<>();
        if (sortby.equalsIgnoreCase("NAME")) {
            list = biDao.listBankAlertThreshold(trxId, trxName);
        } else {
            list = biDao.listBankAlertThresholdSortBiggestTransaction(trxId, trxName);
        }

        for(BankAlertThresholdBean bat : list) {
            bat.setComparemethod1(false);
            bat.setComparemethod2(false);
            bat.setComparemethod3(false);
            bat.setComparemethod4(false);
            bat.setActiveBool(false);

            if (bat.getComparemethod1int() != null) {
                if (bat.getComparemethod1int().compareTo(BigDecimal.ONE) == 0) {
                    bat.setComparemethod1(true);
                }
            }
            if (bat.getComparemethod2int() != null) {
                if (bat.getComparemethod2int().compareTo(BigDecimal.ONE) == 0) {
                    bat.setComparemethod2(true);
                }
            }

            if (bat.getComparemethod3int() != null) {
                if (bat.getComparemethod3int().compareTo(BigDecimal.ONE) == 0) {
                    bat.setComparemethod3(true);
                }
            }

            if (bat.getComparemethod4int() != null) {
                if (bat.getComparemethod4int().compareTo(BigDecimal.ONE) == 0) {
                    bat.setComparemethod4(true);
                }
            }

            if (bat.getActive() != null) {
                if (bat.getActive().compareTo(BigDecimal.ONE) == 0) {
                    bat.setActiveBool(true);
                }
            }
        }
        return list;
    }

    @Override
    @Transactional
    public void updateBankAlertThreshold(String trxId, List<BankAlertThresholdBean> list) throws Exception {
        biDao.deleteBankAlertThreshold(trxId);

        for(BankAlertThresholdBean batb : list) {
            BankAlertThresholdPk pk = new BankAlertThresholdPk();
            pk.setBankId(batb.getBankid());
            pk.setTransactionId(trxId);

            BankAlertThreshold bat = new BankAlertThreshold();
            bat.setBankAlertThresholdPk(pk);
            bat.setThresholdM1(batb.getThresholdm1());
            bat.setThreshold5MinWdWd(batb.getThreshold5minwdwd());
            bat.setThreshold5MinWdHd(batb.getThreshold5minwdhd());
            bat.setThresholdHourWdWd(batb.getThresholdhourwdwd());
            bat.setThresholdHourWdHd(batb.getThresholdhourwdhd());
            bat.setCompareMethod1(BigDecimal.ZERO);
            bat.setCompareMethod2(BigDecimal.ZERO);
            bat.setCompareMethod3(BigDecimal.ZERO);
            bat.setCompareMethod4(BigDecimal.ZERO);
            bat.setStartHour(batb.getStarthour());
            bat.setStartMinute(batb.getStartminute());
            bat.setEndHour(batb.getEndhour());
            bat.setEndMinute(batb.getEndminute());
            bat.setLimitIntervalMinute(batb.getLimitintervalminute());
            bat.setActive(BigDecimal.ZERO);

            if (batb.getComparemethod1()) {
                bat.setCompareMethod1(BigDecimal.ONE);
            }
            if (batb.getComparemethod2()) {
                bat.setCompareMethod2(BigDecimal.ONE);
            }
            if (batb.getComparemethod3()) {
                bat.setCompareMethod3(BigDecimal.ONE);
            }
            if (batb.getComparemethod4()) {
                bat.setCompareMethod4(BigDecimal.ONE);
            }

            if (batb.getActiveBool()) {
                bat.setActive(BigDecimal.ONE);
            }


            biDao.save(bat);
        }
    }


    @Override
    @Transactional(readOnly = true)
    public List<AlertThresholdBean> listAlertThresholdHeader() throws Exception {
        return biDao.listAlertThresholdHeader();
    }

    @Override
    @Transactional(readOnly = true)
    public List<AlertThreshold> listAlertThresholdDetail(String bankId, String trxId) throws Exception {
        return biDao.listAlertThresholdDetail(bankId, trxId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AlertThreshold> listAlertThresholdDetail() throws Exception {
        return biDao.listAlertThresholdDetail();
    }

    @Override
    @Transactional(readOnly = true)
    public AlertThreshold getAlertThreshold(String bankId, String trxId, String trxIndicId) throws Exception {
        AlertThresholdPk pk = new AlertThresholdPk();
        pk.setBankId(bankId);
        pk.setTransactionId(trxId);
        pk.setTransactionIndicatorId(trxIndicId);
        return (AlertThreshold)biDao.get(AlertThreshold.class, pk);
    }

    @Override
    @Transactional
    public void createAlertThreshold(List<AlertThreshold> list) throws Exception {
        for(AlertThreshold at : list) {
            biDao.save(at);
        }
    }

    @Override
    @Transactional
    public void editAlertThreshold(List<AlertThreshold> list) throws Exception {
        if (list.size() == 0) return;

        AlertThreshold ato = list.get(0);
        List<AlertThreshold> listat = listAlertThresholdDetail(ato.getAlertThresholdPk().getBankId(),
                ato.getAlertThresholdPk().getTransactionId());
        for(AlertThreshold atx : listat) {
            biDao.delete(atx);
        }

        for(AlertThreshold at : list) {
            if (at.getActiveBool()) {
                at.setActive(StringUtils.bool2bigdecimal(at.getActiveBool()) );
                at.setCompareMethod1(StringUtils.bool2bigdecimal(at.getCompareMethod1Bool()) );
                at.setCompareMethod2(StringUtils.bool2bigdecimal(at.getCompareMethod2Bool()) );
                at.setCompareMethod3(StringUtils.bool2bigdecimal(at.getCompareMethod3Bool()) );
                at.setCompareMethod4(StringUtils.bool2bigdecimal(at.getCompareMethod4Bool()) );

                AlertThresholdPk pk = new AlertThresholdPk();
                pk.setBankId(at.getAlertThresholdPk().getBankId());
                pk.setTransactionId(at.getAlertThresholdPk().getTransactionId());
                pk.setTransactionIndicatorId(at.getAlertThresholdPk().getTransactionIndicatorId());

                AlertThreshold alertThreshold = new AlertThreshold();
                alertThreshold.setAlertThresholdPk(pk);
                alertThreshold.setCompareMethod1(at.getCompareMethod1());
                alertThreshold.setCompareMethod2(at.getCompareMethod2());
                alertThreshold.setCompareMethod3(at.getCompareMethod3());
                alertThreshold.setCompareMethod4(at.getCompareMethod4());
                alertThreshold.setActive(at.getActive());
                alertThreshold.setStartHour(at.getStartHour());
                alertThreshold.setStartMinute(at.getStartMinute());
                alertThreshold.setEndHour(at.getEndHour());
                alertThreshold.setEndMinute(at.getEndMinute());
                alertThreshold.setLimitIntervalMinute(at.getLimitIntervalMinute());
                alertThreshold.setThreshold5MinWdHd(at.getThreshold5MinWdHd());
                alertThreshold.setThreshold5MinWdWd(at.getThreshold5MinWdWd());
                alertThreshold.setThresholdM1(at.getThresholdM1());

                biDao.save(alertThreshold);
            }
        }
    }

    @Override
    @Transactional
    public void deleteAlertThreshold(String bankId, String trxId) throws Exception {
        List<AlertThreshold> listat = listAlertThresholdDetail(bankId, trxId);
        for(AlertThreshold atx : listat) {
            biDao.delete(atx);
        }
    }

    @Override
    @Transactional
    public void deleteAlertThreshold(String bankId, String trxId, String trxIndicId) throws Exception {
        AlertThreshold at = getAlertThreshold(bankId, trxId, trxIndicId);
        biDao.delete(at);
    }

    @Override
    @Transactional(readOnly = true)
    public AlertAnomaly getAlertAnomaly(BigInteger id) throws Exception {
        return (AlertAnomaly)biDao.get(AlertAnomaly.class, id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AlertAnomaly> searchAlertAnomaly(Map<String, Object> param) throws Exception {
        List<AlertAnomaly> result = null;
        result = biDao.searchAlertAnomaly(param);
        return result;
    }

    @Override
    @Transactional
    public void alertAnomalySetStatus(BigInteger id, int status) throws Exception {
        AlertAnomaly alertAnomaly = getAlertAnomaly(id);
        if (alertAnomaly.getSolveStatus() > 0) {
            return;
        }
        alertAnomaly.setSolveStatus(status);
        biDao.update(alertAnomaly);
    }

    /*@Override
    @Transactional
    public void checkAlertAnomaly5Min() throws Exception {
        String compareType = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_ALERT_COMPARE_TYPE);

        Date nowDate = new Date();
        Date nowDateStart = DateUtils.getStartOfDay(nowDate);
        Date nowDateEnd = DateUtils.getDateRound5Min(nowDate);

        Date prevDateStart = DateUtils.addDate(Calendar.DAY_OF_MONTH, nowDateStart, -1);
        Date prevDateEnd = DateUtils.addDate(Calendar.DAY_OF_MONTH, nowDateEnd, -1);

        //log.info(nowDateStart);
        //log.info(nowDateEnd);
        //log.info(prevDateStart);
        //log.info(prevDateEnd);

        int nowHari = DateUtils.getDayOfWeek(nowDateStart);
        int prevHari = DateUtils.getDayOfWeek(prevDateStart);
        boolean weekday2weekday = false;
        boolean weekday2holiday = false;
        if ( (nowHari == Calendar.SATURDAY || nowHari == Calendar.SUNDAY || nowHari == Calendar.MONDAY) ) {
            weekday2holiday = true;
            weekday2weekday = false;
        } else {
            weekday2holiday = false;
            weekday2weekday = true;
        }
        //log.info(weekday2weekday + " " + weekday2holiday);
        //get holiday
        boolean nowHoliday = moduleFactory.getMasterService().checkHoliday(nowDateStart);
        boolean prevHoliday = moduleFactory.getMasterService().checkHoliday(prevDateStart);
        //log.info(nowDateStart + " " + nowHoliday + ", " + prevDateStart + " " + prevHoliday);
        if (nowHoliday || prevHoliday) {
            weekday2holiday = true;
            weekday2weekday = false;
        }
        //log.info(weekday2weekday + " " + weekday2holiday);

        String period = DateUtils.convertDate(nowDateStart, DateUtils.DATE_YYMMDD);
        String prevPeriod = DateUtils.convertDate(prevDateStart, DateUtils.DATE_YYMMDD);
        String shourFrom = "00:00";

        int hourThru = DateUtils.getHour(nowDateEnd) + 1;
        String shourThru = StringUtils.formatFixLength(Integer.toString(hourThru), "0", 2) + ":00";

        String stimeFrom = "00:00";
        String stimeFrom2 = "";
        String stimeThru = "";
        int minute = DateUtils.getMinute(nowDateEnd);
        //log.info(minute);
        if (minute > 0) {
            stimeFrom2 = StringUtils.formatFixLength(Integer.toString(hourThru - 1), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(minute-5), "0", 2);
            stimeThru = StringUtils.formatFixLength(Integer.toString(hourThru - 1), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(minute), "0", 2);
        }

        log.info(period);
        log.info(shourFrom);
        log.info(shourThru);
        log.info(stimeFrom);
        log.info(stimeFrom2);
        log.info(stimeThru);

        BigDecimal bnow = BigDecimal.ZERO;
        BigDecimal bprev = BigDecimal.ZERO;
        List<EsqTrxIdBean> listTrx = moduleFactory.getMasterService().listEsqTrxId();
        //List<BankBean> listBank = moduleFactory.getMasterService().listBank(new HashMap<>());
        List<MktBankMonitoring> listBank = moduleFactory.getBiService().listBankMonitoring();
        for (MktBankMonitoring bank : listBank) {
            String bankId = bank.getBankId();

            //REMARK
            //if (!bankId.equalsIgnoreCase("BC")) continue;

            for(EsqTrxIdBean trxIdBean : listTrx) {
                String trxId = trxIdBean.getTrxid();

                BankAlertThreshold bankAlertThreshold = moduleFactory.getMasterService().getBankAlertThreshold(bankId, trxId);
                if (bankAlertThreshold == null) {
                    continue;
                }
                log.info(bankId + " " + trxId);

                BigDecimal threshold = BigDecimal.ZERO;
                if (weekday2weekday) {
                    threshold = bankAlertThreshold.getThreshold5MinWdWd();
                    //log.info(threshold + " wdwd");
                } else if (weekday2holiday) {
                    threshold = bankAlertThreshold.getThreshold5MinWdHd();
                    //log.info(threshold + " wdhd");
                }

                if (threshold.compareTo(BigDecimal.ZERO) == 0) {
                    continue;
                }

                //now
                bnow = BigDecimal.ZERO;
                if (compareType.equalsIgnoreCase(DataConstant.COMPARE_FROM00)) {
                    EsqTrx5MinBean nowMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxMinutely(bankId, trxId, period,
                            shourFrom, shourThru, stimeFrom, stimeFrom2, stimeThru);
                    if (nowMinuteEsqTrx5MinBean != null) {
                        //log.info(nowMinuteEsqTrx5MinBean.getFreq_acq_app());
                        bnow = bnow.add(MathUtils.checkNull(nowMinuteEsqTrx5MinBean.getFreq_acq_app()));
                    }
                } else if (compareType.equalsIgnoreCase(DataConstant.COMPARE_FROMHOUR)) {
                    EsqTrx5MinBean nowHourEsqTrx5MinBean = biDao.getTotalEsqTrxHourly(bankId, trxId, period, shourFrom, shourThru);
                    if (nowHourEsqTrx5MinBean != null) {
                        //log.info(nowHourEsqTrx5MinBean.getFreq_acq_app());
                        bnow = bnow.add(MathUtils.checkNull(nowHourEsqTrx5MinBean.getFreq_acq_app()));
                    }
                }

                //prev
                bprev = BigDecimal.ZERO;
                if (compareType.equalsIgnoreCase(DataConstant.COMPARE_FROM00)) {
                    EsqTrx5MinBean prevMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxMinutely(bankId, trxId, prevPeriod,
                            shourFrom, shourThru, stimeFrom, stimeFrom2, stimeThru);
                    if (prevMinuteEsqTrx5MinBean != null) {
                        //log.info(prevMinuteEsqTrx5MinBean.getFreq_acq_app());
                        bprev = bprev.add(MathUtils.checkNull(prevMinuteEsqTrx5MinBean.getFreq_acq_app()));
                    }
                } else if (compareType.equalsIgnoreCase(DataConstant.COMPARE_FROMHOUR)) {
                    EsqTrx5MinBean prevHourEsqTrx5MinBean = biDao.getTotalEsqTrxHourly(bankId, trxId, prevPeriod, shourFrom, shourThru);
                    if (prevHourEsqTrx5MinBean != null) {
                        //log.info(prevHourEsqTrx5MinBean.getFreq_acq_app());
                        bnow = bnow.add(MathUtils.checkNull(prevHourEsqTrx5MinBean.getFreq_acq_app()));
                    }
                }

                BigDecimal bprevLimitHi = BigDecimal.ONE.add(threshold.divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP));
                bprevLimitHi = bprevLimitHi.multiply(bprev);
                BigDecimal bprevLimitLo = BigDecimal.ONE.subtract(threshold.divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP));
                bprevLimitLo = bprevLimitLo.multiply(bprev);

                if (bnow.compareTo(bprevLimitHi) > 0) {
                    //log.info("anomaly plus");
                    AlertAnomaly alertAnomaly = new AlertAnomaly();
                    alertAnomaly.setBankId(bankId);
                    alertAnomaly.setTransactionId(trxId);
                    alertAnomaly.setAlertDate(nowDate);
                    alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                    alertAnomaly.setCurrValue(bnow);
                    alertAnomaly.setPrevValue(bprev);
                    alertAnomaly.setThreshold(threshold);
                    alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                    //alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                    biDao.save(alertAnomaly);
                } else if (bnow.compareTo(bprevLimitLo) < 0) {
                    //log.info("anomaly minus");
                    AlertAnomaly alertAnomaly = new AlertAnomaly();
                    alertAnomaly.setBankId(bankId);
                    alertAnomaly.setTransactionId(trxId);
                    alertAnomaly.setAlertDate(nowDate);
                    alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                    alertAnomaly.setCurrValue(bnow);
                    alertAnomaly.setPrevValue(bprev);
                    alertAnomaly.setThreshold(threshold);
                    alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__MINUS);
                    //alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                    biDao.save(alertAnomaly);
                }


            }
        }

    }*/

    @Override
    @Transactional
    public void checkAlertAnomaly5Min() throws Exception {
        //String compareType = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_ALERT_COMPARE_TYPE);
        String emailTo = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_TO);
        String emailMessage = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_MESSAGE);
        String emailSubject = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_SUBJECT);
        String m1Interval = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_M1_INTERVAL);
        String sdelayM1M2 = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_DELAY_M1_M2);
        int delayM1M2 = Integer.parseInt(sdelayM1M2);

        Date nowDate = new Date();
		nowDate = DateUtils.addDate(Calendar.MINUTE, nowDate, -1 * delayM1M2);
        Date nowDateEnd = DateUtils.getDateRound5Min(nowDate);
        Date nowDateStart = DateUtils.addDate(Calendar.MINUTE, nowDateEnd, -5);
        String period = DateUtils.convertDate(nowDateStart, DateUtils.DATE_YYMMDD);


        int nowHr = DateUtils.getHour(nowDate);
        int nowMin = DateUtils.getMinute(nowDate);

        //List<EsqTrxIdBean> listTrx = moduleFactory.getMasterService().listEsqTrxId();
        //List<MktBankMonitoring> listBank = moduleFactory.getBiService().listBankMonitoring();
        List<BankAlertThreshold> listBankAlertThreshold = moduleFactory.getMasterService().listBankAlertThresholdActive();
        for (BankAlertThreshold bankAlertThreshold : listBankAlertThreshold) {
            String bankId = bankAlertThreshold.getBankAlertThresholdPk().getBankId();
            String trxId = bankAlertThreshold.getBankAlertThresholdPk().getTransactionId();
            EisBank bank = moduleFactory.getMasterService().getBank(bankId);
            String bankName = bank.getBankName();
            EsqTrxIdBean trb = moduleFactory.getMasterService().getEsqTrxId(trxId);
            String trxName = trb.getTrxname();
        //for (MktBankMonitoring bank : listBank) {
        //    String bankId = bank.getBankId();

            //for(EsqTrxIdBean trxIdBean : listTrx) {
            //    String trxId = trxIdBean.getTrxid();

                //BankAlertThreshold bankAlertThreshold = moduleFactory.getMasterService().getBankAlertThreshold(bankId, trxId);
                if (bankAlertThreshold == null) {
                    continue;
                }
                //if (StringUtils.checkNull(bankAlertThreshold.getLimitIntervalMinute()).length() == 0) {
                //    continue;
                //}
                //check active time
                int bathsh = Integer.parseInt(bankAlertThreshold.getStartHour());
                int bathsm = Integer.parseInt(bankAlertThreshold.getStartMinute());
                int batheh = Integer.parseInt(bankAlertThreshold.getEndHour());
                int bathem = Integer.parseInt(bankAlertThreshold.getEndMinute());
                if (
                        !( ((bathsh*60)+bathsm) < ((nowHr*60)+nowMin) &&
                        ((batheh*60)+bathem) > ((nowHr*60)+nowMin) )
                        ) {
                    continue;
                }

                log.info(bankId + " " + trxId);

                Date prevDateStart = DateUtils.addDate(Calendar.MINUTE, nowDateStart, -1 * Integer.parseInt(m1Interval) );
                Date prevDateEnd = DateUtils.addDate(Calendar.MINUTE, nowDateEnd, -1 * Integer.parseInt(m1Interval) );
                String prevPeriod = DateUtils.convertDate(prevDateStart, DateUtils.DATE_YYMMDD);

                Date prevDayDateStart = DateUtils.addDate(Calendar.DAY_OF_MONTH, nowDateStart, -1 );
                Date prevDayDateEnd = DateUtils.addDate(Calendar.DAY_OF_MONTH, nowDateEnd, -1 );
                String prevDayPeriod = DateUtils.convertDate(prevDayDateStart, DateUtils.DATE_YYMMDD);

                log.info(nowDateStart + " - " + nowDateEnd);
                log.info(prevDateStart + " - " + prevDateEnd);
                log.info(prevDayDateStart + " - " + prevDayDateEnd);


                int nowHari = DateUtils.getDayOfWeek(nowDateStart);
                int prevHari = DateUtils.getDayOfWeek(prevDateStart);
                boolean weekday2weekday = false;
                boolean weekday2holiday = false;
                if ( (nowHari == Calendar.SATURDAY || nowHari == Calendar.SUNDAY || nowHari == Calendar.MONDAY) ) {
                    weekday2holiday = true;
                    weekday2weekday = false;
                } else {
                    weekday2holiday = false;
                    weekday2weekday = true;
                }

                //get holiday
                boolean nowHoliday = moduleFactory.getMasterService().checkHoliday(nowDateStart);
                boolean prevHoliday = moduleFactory.getMasterService().checkHoliday(prevDateStart);
                //log.info(nowDateStart + " " + nowHoliday + ", " + prevDateStart + " " + prevHoliday);
                if (nowHoliday || prevHoliday) {
                    weekday2holiday = true;
                    weekday2weekday = false;
                }

                log.info(weekday2weekday + " " + weekday2holiday);

                BigDecimal thresholdM1 = BigDecimal.ZERO;
                BigDecimal thresholdM2 = BigDecimal.ZERO;
                if (weekday2weekday) {
                    thresholdM2 = MathUtils.checkNull(bankAlertThreshold.getThreshold5MinWdWd());
                    //log.info(threshold + " wdwd");
                } else if (weekday2holiday) {
                    thresholdM2 = MathUtils.checkNull(bankAlertThreshold.getThreshold5MinWdHd());
                    //log.info(threshold + " wdhd");
                }
                thresholdM1 = MathUtils.checkNull(bankAlertThreshold.getThresholdM1());


                //now
                String shourFrom = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(nowDateStart)), "0", 2) + ":00";
                String shourThru = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(nowDateEnd) + 1), "0", 2) + ":00";
                String stimeFrom = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(prevDateStart)), "0", 2) + ":" +
                        StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getMinute(nowDateStart)), "0", 2);
                String stimeThru = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(prevDateStart)), "0", 2) + ":" +
                        StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getMinute(nowDateEnd)), "0", 2);
                BigDecimal bnow = BigDecimal.ZERO;
                EsqTrx5MinBean nowMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxMinutely(bankId, trxId, period,
                        shourFrom, shourThru, stimeFrom, stimeThru);
                if (nowMinuteEsqTrx5MinBean == null) continue;
                log.info("bnow: bankId " + bankId + ", trxId " + trxId + ", period " + period + ", shourFrom " +
                        shourFrom + ", shourThru " + shourThru + ", stimeFrom " + stimeFrom + ", stimeThru " + stimeThru);
                if (nowMinuteEsqTrx5MinBean != null) {
                    //log.info(nowMinuteEsqTrx5MinBean.getFreq_acq_app());
                    bnow = bnow.add(MathUtils.checkNull(nowMinuteEsqTrx5MinBean.getFreq_acq_app()));
                    log.info("bnow " + bnow);
                }

                //prev
                shourFrom = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(prevDateStart)), "0", 2) + ":00";
                shourThru = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(prevDateEnd) + 1), "0", 2) + ":00";
                stimeFrom = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(prevDateStart)), "0", 2) + ":" +
                        StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getMinute(prevDateStart)), "0", 2);
                stimeThru = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(prevDateStart)), "0", 2) + ":" +
                        StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getMinute(prevDateEnd)), "0", 2);
                BigDecimal bprev = BigDecimal.ZERO;
                EsqTrx5MinBean prevMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxMinutely(bankId, trxId, prevPeriod,
                        shourFrom, shourThru, stimeFrom, stimeThru);
                if (prevMinuteEsqTrx5MinBean == null) continue;
                log.info("bprev: bankId " + bankId + ", trxId " + trxId + ", prevPeriod " + prevPeriod + ", shourFrom " +
                    shourFrom + ", shourThru " + shourThru + ", stimeFrom " + stimeFrom + ", stimeThru " + stimeThru);
                if (prevMinuteEsqTrx5MinBean != null) {
                    //log.info(prevMinuteEsqTrx5MinBean.getFreq_acq_app());
                    bprev = bprev.add(MathUtils.checkNull(prevMinuteEsqTrx5MinBean.getFreq_acq_app()));
                    log.info("bprev " + bprev);
                }


                //prev day
                shourFrom = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(prevDayDateStart)), "0", 2) + ":00";
                shourThru = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(prevDayDateEnd) + 1), "0", 2) + ":00";
                stimeFrom = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(prevDayDateStart)), "0", 2) + ":" +
                        StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getMinute(prevDayDateStart)), "0", 2);
                stimeThru = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(prevDayDateStart)), "0", 2) + ":" +
                        StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getMinute(prevDayDateEnd)), "0", 2);
                BigDecimal bprevDay = BigDecimal.ZERO;
                EsqTrx5MinBean prevDayMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxMinutely(bankId, trxId, prevDayPeriod,
                        shourFrom, shourThru, stimeFrom, stimeThru);
                log.info("bprevDay: bankId " + bankId + ", trxId " + trxId + ", prevDayPeriod " + prevDayPeriod + ", shourFrom " +
                    shourFrom + ", shourThru " + shourThru + ", stimeFrom " + stimeFrom + ", stimeThru " + stimeThru);
                if (prevDayMinuteEsqTrx5MinBean == null) continue;
                if (prevDayMinuteEsqTrx5MinBean != null) {
                    //log.info(prevMinuteEsqTrx5MinBean.getFreq_acq_app());
                    bprevDay = bprevDay.add(MathUtils.checkNull(prevDayMinuteEsqTrx5MinBean.getFreq_acq_app()));
                    log.info("bprevDay " + bprevDay);
                }

                BigDecimal bprevLimitHiM1 = BigDecimal.ONE.add(thresholdM1.divide(new BigDecimal("100"), 6, BigDecimal.ROUND_HALF_UP));
                BigDecimal bprevLimitLoM1 = BigDecimal.ONE.subtract(thresholdM1.divide(new BigDecimal("100"), 6, BigDecimal.ROUND_HALF_UP));
                BigDecimal bprevLimitHiM2 = BigDecimal.ONE.add(thresholdM2.divide(new BigDecimal("100"), 6, BigDecimal.ROUND_HALF_UP));
                BigDecimal bprevLimitLoM2 = BigDecimal.ONE.subtract(thresholdM2.divide(new BigDecimal("100"), 6, BigDecimal.ROUND_HALF_UP));
                log.info("bprevLimitHiM1 " + bprevLimitHiM1 + ", bprevLimitLoM1 " + bprevLimitLoM1);
                log.info("bprevLimitHiM2 " + bprevLimitHiM2 + ", bprevLimitLoM2 " + bprevLimitLoM2);

                if (bankAlertThreshold.getCompareMethod1().compareTo(BigDecimal.ONE) == 0 &&
                        thresholdM1.compareTo(BigDecimal.ZERO) > 0 ) {
                    //check method 1
                    //Compare current trx with trx 10 minute (parameter) ago
                    BigDecimal prevLimitHi = bprevLimitHiM1.multiply(bprev);
                    BigDecimal prevLimitLo = bprevLimitLoM1.multiply(bprev);

                    log.info("check M1: prevLimitHi " + prevLimitHi + ", prevLimitLo " + prevLimitLo);
                    log.info(nowDate + " - " + bankId +  " - " + trxId + " - " + thresholdM1 + " - " + bprev + " - " + bprevLimitLoM1 + " - " + bprevLimitHiM1 + " - " + bnow);

                    if (bnow.compareTo(prevLimitHi) > 0) {
                        log.info("anomaly plus M1");
                        AlertAnomaly alertAnomaly = new AlertAnomaly();
                        alertAnomaly.setBankId(bankId);
                        alertAnomaly.setTransactionId(trxId);
                        alertAnomaly.setAlertDate(nowDate);
                        alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                        alertAnomaly.setCurrValue(bnow);
                        alertAnomaly.setPrevValue(bprev);
                        alertAnomaly.setThreshold(thresholdM1);
                        alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                        alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M1);
                        alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                        biDao.save(alertAnomaly);

                        //send email
                        //emailTo = emailTo.replace()
                        emailSubject = emailSubject.replace("%bank%", bankName);
                        emailSubject = emailSubject.replace("%trx%", trxName);
                        emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M1);
                        emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailSubject = emailSubject.replace("%currval%", bnow.toPlainString());
                        emailSubject = emailSubject.replace("%prevval%", bprev.toPlainString());

                        emailMessage = emailMessage.replace("%bank%", bankName);
                        emailMessage = emailMessage.replace("%trx%", trxName);
                        emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M1);
                        emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailMessage = emailMessage.replace("%currval%", bnow.toPlainString());
                        emailMessage = emailMessage.replace("%prevval%", bprev.toPlainString());

                        EmailQueue emailQueue = new EmailQueue();
                        emailQueue.setEmailTo(emailTo);
                        emailQueue.setEmailMessage(emailMessage);
                        emailQueue.setEmailSubject(emailSubject);
                        emailQueue.setQueueDate(new Date());
                        biDao.save(emailQueue);

                    } else if (bnow.compareTo(prevLimitLo) < 0) {
                        log.info("anomaly minus M1");
                        AlertAnomaly alertAnomaly = new AlertAnomaly();
                        alertAnomaly.setBankId(bankId);
                        alertAnomaly.setTransactionId(trxId);
                        alertAnomaly.setAlertDate(nowDate);
                        alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                        alertAnomaly.setCurrValue(bnow);
                        alertAnomaly.setPrevValue(bprev);
                        alertAnomaly.setThreshold(thresholdM1);
                        alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__MINUS);
                        alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M1);
                        alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                        biDao.save(alertAnomaly);

                        //send email
                        //emailTo = emailTo.replace()
                        emailSubject = emailSubject.replace("%bank%", bankName);
                        emailSubject = emailSubject.replace("%trx%", trxName);
                        emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M1);
                        emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailSubject = emailSubject.replace("%currval%", bnow.toPlainString());
                        emailSubject = emailSubject.replace("%prevval%", bprev.toPlainString());

                        emailMessage = emailMessage.replace("%bank%", bankName);
                        emailMessage = emailMessage.replace("%trx%", trxName);
                        emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M1);
                        emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailMessage = emailMessage.replace("%currval%", bnow.toPlainString());
                        emailMessage = emailMessage.replace("%prevval%", bprev.toPlainString());

                        EmailQueue emailQueue = new EmailQueue();
                        emailQueue.setEmailTo(emailTo);
                        emailQueue.setEmailMessage(emailMessage);
                        emailQueue.setEmailSubject(emailSubject);
                        emailQueue.setQueueDate(new Date());
                        biDao.save(emailQueue);
                    }
                }

                if (bankAlertThreshold.getCompareMethod2().compareTo(BigDecimal.ONE) == 0 &&
                        thresholdM2.compareTo(BigDecimal.ZERO) > 0 ) {
                    //check method 2
                    //Compare current trx with trx in same minute previous day
                    BigDecimal prevLimitHi = bprevLimitHiM2.multiply(bprevDay);
                    BigDecimal prevLimitLo = bprevLimitLoM2.multiply(bprevDay);

                    log.info("check M2: prevLimitHi " + prevLimitHi + ", prevLimitLo " + prevLimitLo);
                    log.info(nowDate + " - " + bankId +  " - " + trxId + " - " + thresholdM2 + " - " + bprevDay + " - " + bprevLimitLoM2 + " - " + bprevLimitHiM2 + " - " + bnow);

                    if (bnow.compareTo(prevLimitHi) > 0) {
                        log.info("anomaly plus M2");
                        //log.info(bprev + " - " + bprevLimitLoM2 + " - " + bprevLimitHiM2 + " - " + bnow);
                        AlertAnomaly alertAnomaly = new AlertAnomaly();
                        alertAnomaly.setBankId(bankId);
                        alertAnomaly.setTransactionId(trxId);
                        alertAnomaly.setAlertDate(nowDate);
                        alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                        alertAnomaly.setCurrValue(bnow);
                        alertAnomaly.setPrevValue(bprevDay);
                        alertAnomaly.setThreshold(thresholdM2);
                        alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                        alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M2);
                        alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                        biDao.save(alertAnomaly);

                        //send email
                        //emailTo = emailTo.replace()
                        emailSubject = emailSubject.replace("%bank%", bankName);
                        emailSubject = emailSubject.replace("%trx%", trxName);
                        emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M2);
                        emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailSubject = emailSubject.replace("%currval%", bnow.toPlainString());
                        emailSubject = emailSubject.replace("%prevval%", bprevDay.toPlainString());

                        emailMessage = emailMessage.replace("%bank%", bankName);
                        emailMessage = emailMessage.replace("%trx%", trxName);
                        emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M2);
                        emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailMessage = emailMessage.replace("%currval%", bnow.toPlainString());
                        emailMessage = emailMessage.replace("%prevval%", bprevDay.toPlainString());

                        EmailQueue emailQueue = new EmailQueue();
                        emailQueue.setEmailTo(emailTo);
                        emailQueue.setEmailMessage(emailMessage);
                        emailQueue.setEmailSubject(emailSubject);
                        emailQueue.setQueueDate(new Date());
                        biDao.save(emailQueue);

                    } else if (bnow.compareTo(prevLimitLo) < 0) {
                        log.info("anomaly minus M2");
                        //log.info(bprev + " - " + bprevLimitLoM2 + " - " + bprevLimitHiM2 + " - " + bnow);
                        AlertAnomaly alertAnomaly = new AlertAnomaly();
                        alertAnomaly.setBankId(bankId);
                        alertAnomaly.setTransactionId(trxId);
                        alertAnomaly.setAlertDate(nowDate);
                        alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                        alertAnomaly.setCurrValue(bnow);
                        alertAnomaly.setPrevValue(bprevDay);
                        alertAnomaly.setThreshold(thresholdM2);
                        alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__MINUS);
                        alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M2);
                        alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                        biDao.save(alertAnomaly);

                        //send email
                        //emailTo = emailTo.replace()
                        emailSubject = emailSubject.replace("%bank%", bankName);
                        emailSubject = emailSubject.replace("%trx%", trxName);
                        emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M2);
                        emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailSubject = emailSubject.replace("%currval%", bnow.toPlainString());
                        emailSubject = emailSubject.replace("%prevval%", bprevDay.toPlainString());

                        emailMessage = emailMessage.replace("%bank%", bankName);
                        emailMessage = emailMessage.replace("%trx%", trxName);
                        emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M2);
                        emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailMessage = emailMessage.replace("%currval%", bnow.toPlainString());
                        emailMessage = emailMessage.replace("%prevval%", bprevDay.toPlainString());

                        EmailQueue emailQueue = new EmailQueue();
                        emailQueue.setEmailTo(emailTo);
                        emailQueue.setEmailMessage(emailMessage);
                        emailQueue.setEmailSubject(emailSubject);
                        emailQueue.setQueueDate(new Date());
                        biDao.save(emailQueue);

                    }

                }

                if (bankAlertThreshold.getCompareMethod3().compareTo(BigDecimal.ONE) == 0) {
                    //check method 3
                    //Compare if %decline charge greater than %approve
                    BigDecimal total = nowMinuteEsqTrx5MinBean.getFreq_acq_app()
                            .add(nowMinuteEsqTrx5MinBean.getFreq_acq_dc())
                            .add(nowMinuteEsqTrx5MinBean.getFreq_acq_df());
                    BigDecimal pctAppr = BigDecimal.ZERO;
                    BigDecimal pctDecC = BigDecimal.ZERO;
                    if (total.compareTo(BigDecimal.ZERO) > 0) {
                        pctAppr = nowMinuteEsqTrx5MinBean.getFreq_acq_app().divide(total, 6, BigDecimal.ROUND_HALF_UP);
                        pctDecC = nowMinuteEsqTrx5MinBean.getFreq_acq_dc().divide(total, 6, BigDecimal.ROUND_HALF_UP);
                    }


                    log.info("check M3");
                    log.info(nowDate + " - " + bankId +  " - " + trxId + " - " + total + " - " + pctAppr + " - " + pctDecC);

                    if (pctDecC.compareTo(pctAppr) > 0) {
                        log.info("%decline charge greater than %approve M3");
                        log.info(pctAppr + " - " + pctDecC);
                        AlertAnomaly alertAnomaly = new AlertAnomaly();
                        alertAnomaly.setBankId(bankId);
                        alertAnomaly.setTransactionId(trxId);
                        alertAnomaly.setAlertDate(nowDate);
                        alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                        alertAnomaly.setCurrValue(pctAppr);
                        alertAnomaly.setPrevValue(pctDecC);
                        alertAnomaly.setThreshold(null);
                        alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                        alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M3);
                        alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                        biDao.save(alertAnomaly);

                        //send email
                        //emailTo = emailTo.replace()
                        emailSubject = emailSubject.replace("%bank%", bankName);
                        emailSubject = emailSubject.replace("%trx%", trxName);
                        emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M3);
                        emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailSubject = emailSubject.replace("%currval%", pctAppr.toPlainString());
                        emailSubject = emailSubject.replace("%prevval%", pctDecC.toPlainString());

                        emailMessage = emailMessage.replace("%bank%", bankName);
                        emailMessage = emailMessage.replace("%trx%", trxName);
                        emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M3);
                        emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailMessage = emailMessage.replace("%currval%", pctAppr.toPlainString());
                        emailMessage = emailMessage.replace("%prevval%", pctDecC.toPlainString());

                        EmailQueue emailQueue = new EmailQueue();
                        emailQueue.setEmailTo(emailTo);
                        emailQueue.setEmailMessage(emailMessage);
                        emailQueue.setEmailSubject(emailSubject);
                        emailQueue.setQueueDate(new Date());
                        biDao.save(emailQueue);

                    }

                }

                if (bankAlertThreshold.getCompareMethod4().compareTo(BigDecimal.ONE) == 0) {
                    //check method 4
                    //Compare if %decline free greater than %approve
                    BigDecimal total = nowMinuteEsqTrx5MinBean.getFreq_acq_app()
                            .add(nowMinuteEsqTrx5MinBean.getFreq_acq_dc())
                            .add(nowMinuteEsqTrx5MinBean.getFreq_acq_df());
                    BigDecimal pctAppr = BigDecimal.ZERO;
                    BigDecimal pctDecF = BigDecimal.ZERO;
                    if (total.compareTo(BigDecimal.ZERO) > 0) {
                        pctAppr = nowMinuteEsqTrx5MinBean.getFreq_acq_app().divide(total, 6, BigDecimal.ROUND_HALF_UP);
                        pctDecF = nowMinuteEsqTrx5MinBean.getFreq_acq_df().divide(total, 6, BigDecimal.ROUND_HALF_UP);
                    }

                    log.info("check M4");
                    log.info(total + " - " + bankId +  " - " + trxId + " - " + pctAppr + " - " + pctDecF);

                    if (pctDecF.compareTo(pctAppr) > 0) {
                        log.info("%decline charge greater than %approve M4");
                        log.info(pctAppr + " - " + pctDecF);
                        AlertAnomaly alertAnomaly = new AlertAnomaly();
                        alertAnomaly.setBankId(bankId);
                        alertAnomaly.setTransactionId(trxId);
                        alertAnomaly.setAlertDate(nowDate);
                        alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                        alertAnomaly.setCurrValue(pctAppr);
                        alertAnomaly.setPrevValue(pctDecF);
                        alertAnomaly.setThreshold(null);
                        alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                        alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M4);
                        alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                        biDao.save(alertAnomaly);

                        //send email
                        //emailTo = emailTo.replace()
                        emailSubject = emailSubject.replace("%bank%", bankName);
                        emailSubject = emailSubject.replace("%trx%", trxName);
                        emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M4);
                        emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailSubject = emailSubject.replace("%currval%", pctAppr.toPlainString());
                        emailSubject = emailSubject.replace("%prevval%", pctDecF.toPlainString());

                        emailMessage = emailMessage.replace("%bank%", bankName);
                        emailMessage = emailMessage.replace("%trx%", trxName);
                        emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M4);
                        emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailMessage = emailMessage.replace("%currval%", pctAppr.toPlainString());
                        emailMessage = emailMessage.replace("%prevval%", pctDecF.toPlainString());

                        EmailQueue emailQueue = new EmailQueue();
                        emailQueue.setEmailTo(emailTo);
                        emailQueue.setEmailMessage(emailMessage);
                        emailQueue.setEmailSubject(emailSubject);
                        emailQueue.setQueueDate(new Date());
                        biDao.save(emailQueue);

                    }

                }

            //}
        }



/*
        Date prevDateStart = DateUtils.addDate(Calendar.DAY_OF_MONTH, nowDateStart, -1);
        Date prevDateEnd = DateUtils.addDate(Calendar.DAY_OF_MONTH, nowDateEnd, -1);

        //log.info(nowDateStart);
        //log.info(nowDateEnd);
        //log.info(prevDateStart);
        //log.info(prevDateEnd);

        int nowHari = DateUtils.getDayOfWeek(nowDateStart);
        int prevHari = DateUtils.getDayOfWeek(prevDateStart);
        boolean weekday2weekday = false;
        boolean weekday2holiday = false;
        if ( (nowHari == Calendar.SATURDAY || nowHari == Calendar.SUNDAY || nowHari == Calendar.MONDAY) ) {
            weekday2holiday = true;
            weekday2weekday = false;
        } else {
            weekday2holiday = false;
            weekday2weekday = true;
        }
        //log.info(weekday2weekday + " " + weekday2holiday);
        //get holiday
        boolean nowHoliday = moduleFactory.getMasterService().checkHoliday(nowDateStart);
        boolean prevHoliday = moduleFactory.getMasterService().checkHoliday(prevDateStart);
        //log.info(nowDateStart + " " + nowHoliday + ", " + prevDateStart + " " + prevHoliday);
        if (nowHoliday || prevHoliday) {
            weekday2holiday = true;
            weekday2weekday = false;
        }
        //log.info(weekday2weekday + " " + weekday2holiday);

        String period = DateUtils.convertDate(nowDateStart, DateUtils.DATE_YYMMDD);
        String prevPeriod = DateUtils.convertDate(prevDateStart, DateUtils.DATE_YYMMDD);
        String shourFrom = "00:00";

        int hourThru = DateUtils.getHour(nowDateEnd) + 1;
        String shourThru = StringUtils.formatFixLength(Integer.toString(hourThru), "0", 2) + ":00";

        String stimeFrom = "00:00";
        String stimeFrom2 = "";
        String stimeThru = "";
        int minute = DateUtils.getMinute(nowDateEnd);
        //log.info(minute);
        if (minute > 0) {
            stimeFrom2 = StringUtils.formatFixLength(Integer.toString(hourThru - 1), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(minute-5), "0", 2);
            stimeThru = StringUtils.formatFixLength(Integer.toString(hourThru - 1), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(minute), "0", 2);
        }

        log.info(period);
        log.info(shourFrom);
        log.info(shourThru);
        log.info(stimeFrom);
        log.info(stimeFrom2);
        log.info(stimeThru);

        BigDecimal bnow = BigDecimal.ZERO;
        BigDecimal bprev = BigDecimal.ZERO;
        List<EsqTrxIdBean> listTrx = moduleFactory.getMasterService().listEsqTrxId();
        //List<BankBean> listBank = moduleFactory.getMasterService().listBank(new HashMap<>());
        List<MktBankMonitoring> listBank = moduleFactory.getBiService().listBankMonitoring();
        for (MktBankMonitoring bank : listBank) {
            String bankId = bank.getBankId();

            //REMARK
            //if (!bankId.equalsIgnoreCase("BC")) continue;

            for(EsqTrxIdBean trxIdBean : listTrx) {
                String trxId = trxIdBean.getTrxid();

                BankAlertThreshold bankAlertThreshold = moduleFactory.getMasterService().getBankAlertThreshold(bankId, trxId);
                if (bankAlertThreshold == null) {
                    continue;
                }
                log.info(bankId + " " + trxId);

                BigDecimal threshold = BigDecimal.ZERO;
                if (weekday2weekday) {
                    threshold = bankAlertThreshold.getThreshold5MinWdWd();
                    //log.info(threshold + " wdwd");
                } else if (weekday2holiday) {
                    threshold = bankAlertThreshold.getThreshold5MinWdHd();
                    //log.info(threshold + " wdhd");
                }

                if (threshold.compareTo(BigDecimal.ZERO) == 0) {
                    continue;
                }

                //now
                bnow = BigDecimal.ZERO;
                if (compareType.equalsIgnoreCase(DataConstant.COMPARE_FROM00)) {
                    EsqTrx5MinBean nowMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxMinutely(bankId, trxId, period,
                            shourFrom, shourThru, stimeFrom, stimeFrom2, stimeThru);
                    if (nowMinuteEsqTrx5MinBean != null) {
                        //log.info(nowMinuteEsqTrx5MinBean.getFreq_acq_app());
                        bnow = bnow.add(MathUtils.checkNull(nowMinuteEsqTrx5MinBean.getFreq_acq_app()));
                    }
                } else if (compareType.equalsIgnoreCase(DataConstant.COMPARE_FROMHOUR)) {
                    EsqTrx5MinBean nowHourEsqTrx5MinBean = biDao.getTotalEsqTrxHourly(bankId, trxId, period, shourFrom, shourThru);
                    if (nowHourEsqTrx5MinBean != null) {
                        //log.info(nowHourEsqTrx5MinBean.getFreq_acq_app());
                        bnow = bnow.add(MathUtils.checkNull(nowHourEsqTrx5MinBean.getFreq_acq_app()));
                    }
                }

                //prev
                bprev = BigDecimal.ZERO;
                if (compareType.equalsIgnoreCase(DataConstant.COMPARE_FROM00)) {
                    EsqTrx5MinBean prevMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxMinutely(bankId, trxId, prevPeriod,
                            shourFrom, shourThru, stimeFrom, stimeFrom2, stimeThru);
                    if (prevMinuteEsqTrx5MinBean != null) {
                        //log.info(prevMinuteEsqTrx5MinBean.getFreq_acq_app());
                        bprev = bprev.add(MathUtils.checkNull(prevMinuteEsqTrx5MinBean.getFreq_acq_app()));
                    }
                } else if (compareType.equalsIgnoreCase(DataConstant.COMPARE_FROMHOUR)) {
                    EsqTrx5MinBean prevHourEsqTrx5MinBean = biDao.getTotalEsqTrxHourly(bankId, trxId, prevPeriod, shourFrom, shourThru);
                    if (prevHourEsqTrx5MinBean != null) {
                        //log.info(prevHourEsqTrx5MinBean.getFreq_acq_app());
                        bnow = bnow.add(MathUtils.checkNull(prevHourEsqTrx5MinBean.getFreq_acq_app()));
                    }
                }

                BigDecimal bprevLimitHi = BigDecimal.ONE.add(threshold.divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP));
                bprevLimitHi = bprevLimitHi.multiply(bprev);
                BigDecimal bprevLimitLo = BigDecimal.ONE.subtract(threshold.divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP));
                bprevLimitLo = bprevLimitLo.multiply(bprev);

                if (bnow.compareTo(bprevLimitHi) > 0) {
                    //log.info("anomaly plus");
                    AlertAnomaly alertAnomaly = new AlertAnomaly();
                    alertAnomaly.setBankId(bankId);
                    alertAnomaly.setTransactionId(trxId);
                    alertAnomaly.setAlertDate(nowDate);
                    alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                    alertAnomaly.setCurrValue(bnow);
                    alertAnomaly.setPrevValue(bprev);
                    alertAnomaly.setThreshold(threshold);
                    alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                    //alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                    biDao.save(alertAnomaly);
                } else if (bnow.compareTo(bprevLimitLo) < 0) {
                    //log.info("anomaly minus");
                    AlertAnomaly alertAnomaly = new AlertAnomaly();
                    alertAnomaly.setBankId(bankId);
                    alertAnomaly.setTransactionId(trxId);
                    alertAnomaly.setAlertDate(nowDate);
                    alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                    alertAnomaly.setCurrValue(bnow);
                    alertAnomaly.setPrevValue(bprev);
                    alertAnomaly.setThreshold(threshold);
                    alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__MINUS);
                    //alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                    biDao.save(alertAnomaly);
                }


            }
        }
*/
    }



    @Override
    @Transactional
    public void checkAlertAnomaly5MinAlertThreshold() throws Exception {
        //String compareType = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_ALERT_COMPARE_TYPE);
        String emailTo = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_TO);
        String emailMessage = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_MESSAGE);
        String emailSubject = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_SUBJECT);
        String m1Interval = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_M1_INTERVAL);
        String sdelayM1M2 = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_DELAY_M1_M2);
        int delayM1M2 = Integer.parseInt(sdelayM1M2);

        Date nowDate = new Date();
        nowDate = DateUtils.addDate(Calendar.MINUTE, nowDate, -1 * delayM1M2);
        Date nowDateEnd = DateUtils.getDateRound5Min(nowDate);
        Date nowDateStart = DateUtils.addDate(Calendar.MINUTE, nowDateEnd, -5);
        String period = DateUtils.convertDate(nowDateStart, DateUtils.DATE_YYMMDD);


        int nowHr = DateUtils.getHour(nowDate);
        int nowMin = DateUtils.getMinute(nowDate);

        //List<EsqTrxIdBean> listTrx = moduleFactory.getMasterService().listEsqTrxId();
        //List<MktBankMonitoring> listBank = moduleFactory.getBiService().listBankMonitoring();
        List<AlertThreshold> listAlertThreshold = moduleFactory.getBiService().listAlertThresholdDetail();
        for (AlertThreshold bankAlertThreshold : listAlertThreshold) {
            String bankId = bankAlertThreshold.getAlertThresholdPk().getBankId();
            String trxId = bankAlertThreshold.getAlertThresholdPk().getTransactionId();
            String trxIndicatorId = bankAlertThreshold.getAlertThresholdPk().getTransactionIndicatorId();
            EisBank bank = moduleFactory.getMasterService().getBank(bankId);
            String bankName = bank.getBankName();
            String trxName = "";
            if (trxId.equalsIgnoreCase(DataConstant.ALL)) {
                trxName = "All";
            } else {
                EsqTrxIdBean trb = moduleFactory.getMasterService().getEsqTrxId(trxId);
                trxName = trb.getTrxname();
            }
            EsqTrxIdBean trbi = moduleFactory.getMasterService().getEsqTrxIdIndicator(trxIndicatorId);
            String trxIndicatorName = trbi.getTrxnameindicator();
            //for (MktBankMonitoring bank : listBank) {
            //    String bankId = bank.getBankId();

            //for(EsqTrxIdBean trxIdBean : listTrx) {
            //    String trxId = trxIdBean.getTrxid();

            //BankAlertThreshold bankAlertThreshold = moduleFactory.getMasterService().getBankAlertThreshold(bankId, trxId);
            if (bankAlertThreshold == null) {
                continue;
            }
            //if (StringUtils.checkNull(bankAlertThreshold.getLimitIntervalMinute()).length() == 0) {
            //    continue;
            //}
            //check active time
            int bathsh = Integer.parseInt(bankAlertThreshold.getStartHour());
            int bathsm = Integer.parseInt(bankAlertThreshold.getStartMinute());
            int batheh = Integer.parseInt(bankAlertThreshold.getEndHour());
            int bathem = Integer.parseInt(bankAlertThreshold.getEndMinute());
            if (
                    !( ((bathsh*60)+bathsm) < ((nowHr*60)+nowMin) &&
                            ((batheh*60)+bathem) > ((nowHr*60)+nowMin) )
                    ) {
                continue;
            }

            log.info(bankId + " " + trxId);

            Date prevDateStart = DateUtils.addDate(Calendar.MINUTE, nowDateStart, -1 * Integer.parseInt(m1Interval) );
            Date prevDateEnd = DateUtils.addDate(Calendar.MINUTE, nowDateEnd, -1 * Integer.parseInt(m1Interval) );
            String prevPeriod = DateUtils.convertDate(prevDateStart, DateUtils.DATE_YYMMDD);

            Date prevDayDateStart = DateUtils.addDate(Calendar.DAY_OF_MONTH, nowDateStart, -1 );
            Date prevDayDateEnd = DateUtils.addDate(Calendar.DAY_OF_MONTH, nowDateEnd, -1 );
            String prevDayPeriod = DateUtils.convertDate(prevDayDateStart, DateUtils.DATE_YYMMDD);

            log.info(nowDateStart + " - " + nowDateEnd);
            log.info(prevDateStart + " - " + prevDateEnd);
            log.info(prevDayDateStart + " - " + prevDayDateEnd);


            int nowHari = DateUtils.getDayOfWeek(nowDateStart);
            int prevHari = DateUtils.getDayOfWeek(prevDateStart);
            boolean weekday2weekday = false;
            boolean weekday2holiday = false;
            if ( (nowHari == Calendar.SATURDAY || nowHari == Calendar.SUNDAY || nowHari == Calendar.MONDAY) ) {
                weekday2holiday = true;
                weekday2weekday = false;
            } else {
                weekday2holiday = false;
                weekday2weekday = true;
            }

            //get holiday
            boolean nowHoliday = moduleFactory.getMasterService().checkHoliday(nowDateStart);
            boolean prevHoliday = moduleFactory.getMasterService().checkHoliday(prevDateStart);
            //log.info(nowDateStart + " " + nowHoliday + ", " + prevDateStart + " " + prevHoliday);
            if (nowHoliday || prevHoliday) {
                weekday2holiday = true;
                weekday2weekday = false;
            }

            log.info(weekday2weekday + " " + weekday2holiday);

            BigDecimal thresholdM1 = BigDecimal.ZERO;
            BigDecimal thresholdM2 = BigDecimal.ZERO;
            if (weekday2weekday) {
                thresholdM2 = MathUtils.checkNull(bankAlertThreshold.getThreshold5MinWdWd());
                //log.info(threshold + " wdwd");
            } else if (weekday2holiday) {
                thresholdM2 = MathUtils.checkNull(bankAlertThreshold.getThreshold5MinWdHd());
                //log.info(threshold + " wdhd");
            }
            thresholdM1 = MathUtils.checkNull(bankAlertThreshold.getThresholdM1());


            //now
            String shourFrom = StringUtils.formatFixLength(
                    Integer.toString(DateUtils.getHour(nowDateStart)), "0", 2) + ":00";
            String shourThru = StringUtils.formatFixLength(
                    Integer.toString(DateUtils.getHour(nowDateEnd) + 1), "0", 2) + ":00";
            String stimeFrom = StringUtils.formatFixLength(
                    Integer.toString(DateUtils.getHour(prevDateStart)), "0", 2) + ":" +
                    StringUtils.formatFixLength(
                            Integer.toString(DateUtils.getMinute(nowDateStart)), "0", 2);
            String stimeThru = StringUtils.formatFixLength(
                    Integer.toString(DateUtils.getHour(prevDateStart)), "0", 2) + ":" +
                    StringUtils.formatFixLength(
                            Integer.toString(DateUtils.getMinute(nowDateEnd)), "0", 2);
            BigDecimal bnow = BigDecimal.ZERO;
            EsqTrx5MinBean nowMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxMinutely(bankId, trxId, period,
                    shourFrom, shourThru, stimeFrom, stimeThru);
            if (nowMinuteEsqTrx5MinBean == null) continue;
            log.info("bnow: bankId " + bankId + ", trxId " + trxId + ", period " + period + ", shourFrom " +
                    shourFrom + ", shourThru " + shourThru + ", stimeFrom " + stimeFrom + ", stimeThru " + stimeThru);
            if (nowMinuteEsqTrx5MinBean != null) {
                //log.info(nowMinuteEsqTrx5MinBean.getFreq_acq_app());
                bnow = BigDecimal.ZERO;
                if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ONLY)) {
                    bnow = bnow.add(MathUtils.checkNull(nowMinuteEsqTrx5MinBean.getFreq_acq_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_ONLY)) {
                    bnow = bnow.add(MathUtils.checkNull(nowMinuteEsqTrx5MinBean.getFreq_iss_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__BNF_ONLY)) {
                    bnow = bnow.add(MathUtils.checkNull(nowMinuteEsqTrx5MinBean.getFreq_bnf_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ISS)) {
                    bnow = bnow.add(MathUtils.checkNull(nowMinuteEsqTrx5MinBean.getFreq_acqIss_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_BNF)) {
                    bnow = bnow.add(MathUtils.checkNull(nowMinuteEsqTrx5MinBean.getFreq_acqBnf_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_BNF)) {
                    bnow = bnow.add(MathUtils.checkNull(nowMinuteEsqTrx5MinBean.getFreq_issBnf_app()));
                }
                log.info("bnow " + bnow);
            }

            //prev
            shourFrom = StringUtils.formatFixLength(
                    Integer.toString(DateUtils.getHour(prevDateStart)), "0", 2) + ":00";
            shourThru = StringUtils.formatFixLength(
                    Integer.toString(DateUtils.getHour(prevDateEnd) + 1), "0", 2) + ":00";
            stimeFrom = StringUtils.formatFixLength(
                    Integer.toString(DateUtils.getHour(prevDateStart)), "0", 2) + ":" +
                    StringUtils.formatFixLength(
                            Integer.toString(DateUtils.getMinute(prevDateStart)), "0", 2);
            stimeThru = StringUtils.formatFixLength(
                    Integer.toString(DateUtils.getHour(prevDateStart)), "0", 2) + ":" +
                    StringUtils.formatFixLength(
                            Integer.toString(DateUtils.getMinute(prevDateEnd)), "0", 2);
            BigDecimal bprev = BigDecimal.ZERO;
            EsqTrx5MinBean prevMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxMinutely(bankId, trxId, prevPeriod,
                    shourFrom, shourThru, stimeFrom, stimeThru);
            if (prevMinuteEsqTrx5MinBean == null) continue;
            log.info("bprev: bankId " + bankId + ", trxId " + trxId + ", prevPeriod " + prevPeriod + ", shourFrom " +
                    shourFrom + ", shourThru " + shourThru + ", stimeFrom " + stimeFrom + ", stimeThru " + stimeThru);
            if (prevMinuteEsqTrx5MinBean != null) {
                //log.info(prevMinuteEsqTrx5MinBean.getFreq_acq_app());
                bprev = BigDecimal.ZERO;
                if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ONLY)) {
                    bprev = bprev.add(MathUtils.checkNull(prevMinuteEsqTrx5MinBean.getFreq_acq_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_ONLY)) {
                    bprev = bprev.add(MathUtils.checkNull(prevMinuteEsqTrx5MinBean.getFreq_iss_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__BNF_ONLY)) {
                    bprev = bprev.add(MathUtils.checkNull(prevMinuteEsqTrx5MinBean.getFreq_bnf_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ISS)) {
                    bprev = bprev.add(MathUtils.checkNull(prevMinuteEsqTrx5MinBean.getFreq_acqIss_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_BNF)) {
                    bprev = bprev.add(MathUtils.checkNull(prevMinuteEsqTrx5MinBean.getFreq_acqBnf_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_BNF)) {
                    bprev = bprev.add(MathUtils.checkNull(prevMinuteEsqTrx5MinBean.getFreq_issBnf_app()));
                }
                //bprev = bprev.add(MathUtils.checkNull(prevMinuteEsqTrx5MinBean.getFreq_acq_app()));
                log.info("bprev " + bprev);
            }


            //prev day
            shourFrom = StringUtils.formatFixLength(
                    Integer.toString(DateUtils.getHour(prevDayDateStart)), "0", 2) + ":00";
            shourThru = StringUtils.formatFixLength(
                    Integer.toString(DateUtils.getHour(prevDayDateEnd) + 1), "0", 2) + ":00";
            stimeFrom = StringUtils.formatFixLength(
                    Integer.toString(DateUtils.getHour(prevDayDateStart)), "0", 2) + ":" +
                    StringUtils.formatFixLength(
                            Integer.toString(DateUtils.getMinute(prevDayDateStart)), "0", 2);
            stimeThru = StringUtils.formatFixLength(
                    Integer.toString(DateUtils.getHour(prevDayDateStart)), "0", 2) + ":" +
                    StringUtils.formatFixLength(
                            Integer.toString(DateUtils.getMinute(prevDayDateEnd)), "0", 2);
            BigDecimal bprevDay = BigDecimal.ZERO;
            EsqTrx5MinBean prevDayMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxMinutely(bankId, trxId, prevDayPeriod,
                    shourFrom, shourThru, stimeFrom, stimeThru);
            log.info("bprevDay: bankId " + bankId + ", trxId " + trxId + ", prevDayPeriod " + prevDayPeriod + ", shourFrom " +
                    shourFrom + ", shourThru " + shourThru + ", stimeFrom " + stimeFrom + ", stimeThru " + stimeThru);
            //if (prevDayMinuteEsqTrx5MinBean == null) continue;
            bprevDay = BigDecimal.ZERO;
            if (prevDayMinuteEsqTrx5MinBean != null) {
                //log.info(prevMinuteEsqTrx5MinBean.getFreq_acq_app());
                bprevDay = BigDecimal.ZERO;
                if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ONLY)) {
                    bprevDay = bprevDay.add(MathUtils.checkNull(prevDayMinuteEsqTrx5MinBean.getFreq_acq_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_ONLY)) {
                    bprevDay = bprevDay.add(MathUtils.checkNull(prevDayMinuteEsqTrx5MinBean.getFreq_iss_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__BNF_ONLY)) {
                    bprevDay = bprevDay.add(MathUtils.checkNull(prevDayMinuteEsqTrx5MinBean.getFreq_bnf_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ISS)) {
                    bprevDay = bprevDay.add(MathUtils.checkNull(prevDayMinuteEsqTrx5MinBean.getFreq_acqIss_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_BNF)) {
                    bprevDay = bprevDay.add(MathUtils.checkNull(prevDayMinuteEsqTrx5MinBean.getFreq_acqBnf_app()));
                } else if (trxIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_BNF)) {
                    bprevDay = bprevDay.add(MathUtils.checkNull(prevDayMinuteEsqTrx5MinBean.getFreq_issBnf_app()));
                }
                //bprevDay = bprevDay.add(MathUtils.checkNull(prevDayMinuteEsqTrx5MinBean.getFreq_acq_app()));
                log.info("bprevDay " + bprevDay);
            }

            BigDecimal bprevLimitHiM1 = BigDecimal.ONE.add(thresholdM1.divide(new BigDecimal("100"), 6, BigDecimal.ROUND_HALF_UP));
            BigDecimal bprevLimitLoM1 = BigDecimal.ONE.subtract(thresholdM1.divide(new BigDecimal("100"), 6, BigDecimal.ROUND_HALF_UP));
            BigDecimal bprevLimitHiM2 = BigDecimal.ONE.add(thresholdM2.divide(new BigDecimal("100"), 6, BigDecimal.ROUND_HALF_UP));
            BigDecimal bprevLimitLoM2 = BigDecimal.ONE.subtract(thresholdM2.divide(new BigDecimal("100"), 6, BigDecimal.ROUND_HALF_UP));
            log.info("bprevLimitHiM1 " + bprevLimitHiM1 + ", bprevLimitLoM1 " + bprevLimitLoM1);
            log.info("bprevLimitHiM2 " + bprevLimitHiM2 + ", bprevLimitLoM2 " + bprevLimitLoM2);

            if (bankAlertThreshold.getCompareMethod1().compareTo(BigDecimal.ONE) == 0 &&
                    thresholdM1.compareTo(BigDecimal.ZERO) > 0 ) {
                //check method 1
                //Compare current trx with trx 10 minute (parameter) ago
                BigDecimal prevLimitHi = bprevLimitHiM1.multiply(bprev);
                BigDecimal prevLimitLo = bprevLimitLoM1.multiply(bprev);

                log.info("check M1: prevLimitHi " + prevLimitHi + ", prevLimitLo " + prevLimitLo);
                log.info(nowDate + " - " + bankId +  " - " + trxId + " - " + thresholdM1 + " - " + bprev + " - " + bprevLimitLoM1 + " - " + bprevLimitHiM1 + " - " + bnow);

                if (bnow.compareTo(prevLimitHi) > 0) {
                    log.info("anomaly plus M1");
                    AlertAnomaly alertAnomaly = new AlertAnomaly();
                    alertAnomaly.setBankId(bankId);
                    alertAnomaly.setTransactionId(trxId);
                    alertAnomaly.setTransactionIndicatorId(trxIndicatorId);
                    alertAnomaly.setAlertDate(nowDate);
                    alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                    alertAnomaly.setCurrValue(bnow);
                    alertAnomaly.setPrevValue(bprev);
                    alertAnomaly.setThreshold(thresholdM1);
                    alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                    alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M1);
                    alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                    biDao.save(alertAnomaly);

                    //send email
                    //emailTo = emailTo.replace()
                    emailSubject = emailSubject.replace("%bank%", bankName);
                    emailSubject = emailSubject.replace("%trx%", trxName);
                    emailSubject = emailSubject.replace("%trxindic%", trxIndicatorName);
                    emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M1);
                    emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                    emailSubject = emailSubject.replace("%currval%", bnow.toPlainString());
                    emailSubject = emailSubject.replace("%prevval%", bprev.toPlainString());

                    emailMessage = emailMessage.replace("%bank%", bankName);
                    emailMessage = emailMessage.replace("%trx%", trxName);
                    emailSubject = emailSubject.replace("%trxindic%", trxIndicatorName);
                    emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M1);
                    emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                    emailMessage = emailMessage.replace("%currval%", bnow.toPlainString());
                    emailMessage = emailMessage.replace("%prevval%", bprev.toPlainString());

                    EmailQueue emailQueue = new EmailQueue();
                    emailQueue.setEmailTo(emailTo);
                    emailQueue.setEmailMessage(emailMessage);
                    emailQueue.setEmailSubject(emailSubject);
                    emailQueue.setQueueDate(new Date());
                    biDao.save(emailQueue);

                } else if (bnow.compareTo(prevLimitLo) < 0) {
                    log.info("anomaly minus M1");
                    AlertAnomaly alertAnomaly = new AlertAnomaly();
                    alertAnomaly.setBankId(bankId);
                    alertAnomaly.setTransactionId(trxId);
                    alertAnomaly.setTransactionIndicatorId(trxIndicatorId);
                    alertAnomaly.setAlertDate(nowDate);
                    alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                    alertAnomaly.setCurrValue(bnow);
                    alertAnomaly.setPrevValue(bprev);
                    alertAnomaly.setThreshold(thresholdM1);
                    alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__MINUS);
                    alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M1);
                    alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                    biDao.save(alertAnomaly);

                    //send email
                    //emailTo = emailTo.replace()
                    emailSubject = emailSubject.replace("%bank%", bankName);
                    emailSubject = emailSubject.replace("%trx%", trxName);
                    emailSubject = emailSubject.replace("%trxindic%", trxIndicatorName);
                    emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M1);
                    emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                    emailSubject = emailSubject.replace("%currval%", bnow.toPlainString());
                    emailSubject = emailSubject.replace("%prevval%", bprev.toPlainString());

                    emailMessage = emailMessage.replace("%bank%", bankName);
                    emailMessage = emailMessage.replace("%trx%", trxName);
                    emailSubject = emailSubject.replace("%trxindic%", trxIndicatorName);
                    emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M1);
                    emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                    emailMessage = emailMessage.replace("%currval%", bnow.toPlainString());
                    emailMessage = emailMessage.replace("%prevval%", bprev.toPlainString());

                    EmailQueue emailQueue = new EmailQueue();
                    emailQueue.setEmailTo(emailTo);
                    emailQueue.setEmailMessage(emailMessage);
                    emailQueue.setEmailSubject(emailSubject);
                    emailQueue.setQueueDate(new Date());
                    biDao.save(emailQueue);
                }
            }

            if (bankAlertThreshold.getCompareMethod2().compareTo(BigDecimal.ONE) == 0 &&
                    thresholdM2.compareTo(BigDecimal.ZERO) > 0 ) {
                //check method 2
                //Compare current trx with trx in same minute previous day
                BigDecimal prevLimitHi = bprevLimitHiM2.multiply(bprevDay);
                BigDecimal prevLimitLo = bprevLimitLoM2.multiply(bprevDay);

                log.info("check M2: prevLimitHi " + prevLimitHi + ", prevLimitLo " + prevLimitLo);
                log.info(nowDate + " - " + bankId +  " - " + trxId + " - " + thresholdM2 + " - " + bprevDay + " - " + bprevLimitLoM2 + " - " + bprevLimitHiM2 + " - " + bnow);

                if (bnow.compareTo(prevLimitHi) > 0) {
                    log.info("anomaly plus M2");
                    //log.info(bprev + " - " + bprevLimitLoM2 + " - " + bprevLimitHiM2 + " - " + bnow);
                    AlertAnomaly alertAnomaly = new AlertAnomaly();
                    alertAnomaly.setBankId(bankId);
                    alertAnomaly.setTransactionId(trxId);
                    alertAnomaly.setTransactionIndicatorId(trxIndicatorId);
                    alertAnomaly.setAlertDate(nowDate);
                    alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                    alertAnomaly.setCurrValue(bnow);
                    alertAnomaly.setPrevValue(bprevDay);
                    alertAnomaly.setThreshold(thresholdM2);
                    alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                    alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M2);
                    alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                    biDao.save(alertAnomaly);

                    //send email
                    //emailTo = emailTo.replace()
                    emailSubject = emailSubject.replace("%bank%", bankName);
                    emailSubject = emailSubject.replace("%trx%", trxName);
                    emailSubject = emailSubject.replace("%trxindic%", trxIndicatorName);
                    emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M2);
                    emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                    emailSubject = emailSubject.replace("%currval%", bnow.toPlainString());
                    emailSubject = emailSubject.replace("%prevval%", bprevDay.toPlainString());

                    emailMessage = emailMessage.replace("%bank%", bankName);
                    emailMessage = emailMessage.replace("%trx%", trxName);
                    emailSubject = emailSubject.replace("%trxindic%", trxIndicatorName);
                    emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M2);
                    emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                    emailMessage = emailMessage.replace("%currval%", bnow.toPlainString());
                    emailMessage = emailMessage.replace("%prevval%", bprevDay.toPlainString());

                    EmailQueue emailQueue = new EmailQueue();
                    emailQueue.setEmailTo(emailTo);
                    emailQueue.setEmailMessage(emailMessage);
                    emailQueue.setEmailSubject(emailSubject);
                    emailQueue.setQueueDate(new Date());
                    biDao.save(emailQueue);

                } else if (bnow.compareTo(prevLimitLo) < 0) {
                    log.info("anomaly minus M2");
                    //log.info(bprev + " - " + bprevLimitLoM2 + " - " + bprevLimitHiM2 + " - " + bnow);
                    AlertAnomaly alertAnomaly = new AlertAnomaly();
                    alertAnomaly.setBankId(bankId);
                    alertAnomaly.setTransactionId(trxId);
                    alertAnomaly.setTransactionIndicatorId(trxIndicatorId);
                    alertAnomaly.setAlertDate(nowDate);
                    alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                    alertAnomaly.setCurrValue(bnow);
                    alertAnomaly.setPrevValue(bprevDay);
                    alertAnomaly.setThreshold(thresholdM2);
                    alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__MINUS);
                    alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M2);
                    alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                    biDao.save(alertAnomaly);

                    //send email
                    //emailTo = emailTo.replace()
                    emailSubject = emailSubject.replace("%bank%", bankName);
                    emailSubject = emailSubject.replace("%trx%", trxName);
                    emailSubject = emailSubject.replace("%trxindic%", trxIndicatorName);
                    emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M2);
                    emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                    emailSubject = emailSubject.replace("%currval%", bnow.toPlainString());
                    emailSubject = emailSubject.replace("%prevval%", bprevDay.toPlainString());

                    emailMessage = emailMessage.replace("%bank%", bankName);
                    emailMessage = emailMessage.replace("%trx%", trxName);
                    emailSubject = emailSubject.replace("%trxindic%", trxIndicatorName);
                    emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M2);
                    emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                    emailMessage = emailMessage.replace("%currval%", bnow.toPlainString());
                    emailMessage = emailMessage.replace("%prevval%", bprevDay.toPlainString());

                    EmailQueue emailQueue = new EmailQueue();
                    emailQueue.setEmailTo(emailTo);
                    emailQueue.setEmailMessage(emailMessage);
                    emailQueue.setEmailSubject(emailSubject);
                    emailQueue.setQueueDate(new Date());
                    biDao.save(emailQueue);

                }

            }

            if (bankAlertThreshold.getCompareMethod3().compareTo(BigDecimal.ONE) == 0) {
                //check method 3
                //Compare if %decline charge greater than %approve
                BigDecimal total = nowMinuteEsqTrx5MinBean.getFreq_acq_app()
                        .add(nowMinuteEsqTrx5MinBean.getFreq_acq_dc())
                        .add(nowMinuteEsqTrx5MinBean.getFreq_acq_df());
                BigDecimal pctAppr = BigDecimal.ZERO;
                BigDecimal pctDecC = BigDecimal.ZERO;
                if (total.compareTo(BigDecimal.ZERO) > 0) {
                    pctAppr = nowMinuteEsqTrx5MinBean.getFreq_acq_app().divide(total, 6, BigDecimal.ROUND_HALF_UP);
                    pctDecC = nowMinuteEsqTrx5MinBean.getFreq_acq_dc().divide(total, 6, BigDecimal.ROUND_HALF_UP);
                }


                log.info("check M3");
                log.info(nowDate + " - " + bankId +  " - " + trxId + " - " + total + " - " + pctAppr + " - " + pctDecC);

                if (pctDecC.compareTo(pctAppr) > 0) {
                    log.info("%decline charge greater than %approve M3");
                    log.info(pctAppr + " - " + pctDecC);
                    AlertAnomaly alertAnomaly = new AlertAnomaly();
                    alertAnomaly.setBankId(bankId);
                    alertAnomaly.setTransactionId(trxId);
                    alertAnomaly.setTransactionIndicatorId(trxIndicatorId);
                    alertAnomaly.setAlertDate(nowDate);
                    alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                    alertAnomaly.setCurrValue(pctAppr);
                    alertAnomaly.setPrevValue(pctDecC);
                    alertAnomaly.setThreshold(null);
                    alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                    alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M3);
                    alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                    biDao.save(alertAnomaly);

                    //send email
                    //emailTo = emailTo.replace()
                    emailSubject = emailSubject.replace("%bank%", bankName);
                    emailSubject = emailSubject.replace("%trx%", trxName);
                    emailSubject = emailSubject.replace("%trxindic%", trxIndicatorName);
                    emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M3);
                    emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                    emailSubject = emailSubject.replace("%currval%", pctAppr.toPlainString());
                    emailSubject = emailSubject.replace("%prevval%", pctDecC.toPlainString());

                    emailMessage = emailMessage.replace("%bank%", bankName);
                    emailMessage = emailMessage.replace("%trx%", trxName);
                    emailSubject = emailSubject.replace("%trxindic%", trxIndicatorName);
                    emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M3);
                    emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                    emailMessage = emailMessage.replace("%currval%", pctAppr.toPlainString());
                    emailMessage = emailMessage.replace("%prevval%", pctDecC.toPlainString());

                    EmailQueue emailQueue = new EmailQueue();
                    emailQueue.setEmailTo(emailTo);
                    emailQueue.setEmailMessage(emailMessage);
                    emailQueue.setEmailSubject(emailSubject);
                    emailQueue.setQueueDate(new Date());
                    biDao.save(emailQueue);

                }

            }

            if (bankAlertThreshold.getCompareMethod4().compareTo(BigDecimal.ONE) == 0) {
                //check method 4
                //Compare if %decline free greater than %approve
                BigDecimal total = nowMinuteEsqTrx5MinBean.getFreq_acq_app()
                        .add(nowMinuteEsqTrx5MinBean.getFreq_acq_dc())
                        .add(nowMinuteEsqTrx5MinBean.getFreq_acq_df());
                BigDecimal pctAppr = BigDecimal.ZERO;
                BigDecimal pctDecF = BigDecimal.ZERO;
                if (total.compareTo(BigDecimal.ZERO) > 0) {
                    pctAppr = nowMinuteEsqTrx5MinBean.getFreq_acq_app().divide(total, 6, BigDecimal.ROUND_HALF_UP);
                    pctDecF = nowMinuteEsqTrx5MinBean.getFreq_acq_df().divide(total, 6, BigDecimal.ROUND_HALF_UP);
                }

                log.info("check M4");
                log.info(total + " - " + bankId +  " - " + trxId + " - " + pctAppr + " - " + pctDecF);

                if (pctDecF.compareTo(pctAppr) > 0) {
                    log.info("%decline charge greater than %approve M4");
                    log.info(pctAppr + " - " + pctDecF);
                    AlertAnomaly alertAnomaly = new AlertAnomaly();
                    alertAnomaly.setBankId(bankId);
                    alertAnomaly.setTransactionId(trxId);
                    alertAnomaly.setTransactionIndicatorId(trxIndicatorId);
                    alertAnomaly.setAlertDate(nowDate);
                    alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                    alertAnomaly.setCurrValue(pctAppr);
                    alertAnomaly.setPrevValue(pctDecF);
                    alertAnomaly.setThreshold(null);
                    alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                    alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M4);
                    alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_5MIN);
                    biDao.save(alertAnomaly);

                    //send email
                    //emailTo = emailTo.replace()
                    emailSubject = emailSubject.replace("%bank%", bankName);
                    emailSubject = emailSubject.replace("%trx%", trxName);
                    emailSubject = emailSubject.replace("%trxindic%", trxIndicatorName);
                    emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M4);
                    emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                    emailSubject = emailSubject.replace("%currval%", pctAppr.toPlainString());
                    emailSubject = emailSubject.replace("%prevval%", pctDecF.toPlainString());

                    emailMessage = emailMessage.replace("%bank%", bankName);
                    emailMessage = emailMessage.replace("%trx%", trxName);
                    emailSubject = emailSubject.replace("%trxindic%", trxIndicatorName);
                    emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M4);
                    emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                    emailMessage = emailMessage.replace("%currval%", pctAppr.toPlainString());
                    emailMessage = emailMessage.replace("%prevval%", pctDecF.toPlainString());

                    EmailQueue emailQueue = new EmailQueue();
                    emailQueue.setEmailTo(emailTo);
                    emailQueue.setEmailMessage(emailMessage);
                    emailQueue.setEmailSubject(emailSubject);
                    emailQueue.setQueueDate(new Date());
                    biDao.save(emailQueue);

                }

            }

            //}
        }


    }



    /*@Override
    @Transactional(readOnly = true)
    public AlertAnomalyHourly getAlertAnomalyHourly(BigInteger id) throws Exception {
        return (AlertAnomalyHourly)biDao.get(AlertAnomalyHourly.class, id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AlertAnomalyHourly> searchAlertAnomalyHourly(Map<String, Object> param) throws Exception {
        List<AlertAnomalyHourly> result = null;
        result = biDao.searchAlertAnomalyHourly(param);
        return result;
    }

    @Override
    @Transactional
    public void alertAnomalyHourlySetStatus(BigInteger id, int status) throws Exception {
        AlertAnomalyHourly alertAnomaly = getAlertAnomalyHourly(id);
        if (alertAnomaly.getSolveStatus() > 0) {
            return;
        }
        alertAnomaly.setSolveStatus(status);
        biDao.update(alertAnomaly);
    }*/

    /*@Override
    @Transactional
    public void checkAlertAnomalyHourly() throws Exception {
        //String compareType = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_ALERT_COMPARE_TYPE);

        String emailTo = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_TO);
        String emailMessage = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_MESSAGE);
        String emailSubject = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_SUBJECT);

        Date nowDate = new Date();
        Date nowDateEnd = DateUtils.setMinute(nowDate, 0);
        nowDateEnd = DateUtils.setSecond(nowDateEnd, 0);
        Date nowDateStart = DateUtils.addDate(Calendar.HOUR, nowDateEnd, -1);
        String period = DateUtils.convertDate(nowDateStart, DateUtils.DATE_YYMMDD);

        int nowHr = DateUtils.getHour(nowDate);
        int nowMin = DateUtils.getMinute(nowDate);

        //List<EsqTrxIdBean> listTrx = moduleFactory.getMasterService().listEsqTrxId();
        //List<MktBankMonitoring> listBank = moduleFactory.getBiService().listBankMonitoring();
        List<BankAlertThreshold> listBankAlertThreshold = moduleFactory.getMasterService().listBankAlertThresholdActive();
        for (BankAlertThreshold bankAlertThreshold : listBankAlertThreshold) {
            String bankId = bankAlertThreshold.getBankAlertThresholdPk().getBankId();
            String trxId = bankAlertThreshold.getBankAlertThresholdPk().getTransactionId();
            EisBank bank = moduleFactory.getMasterService().getBank(bankId);
            String bankName = bank.getBankName();
            EsqTrxIdBean trb = moduleFactory.getMasterService().getEsqTrxId(trxId);
            String trxName = trb.getTrxname();
        //for (MktBankMonitoring bank : listBank) {
        //    String bankId = bank.getBankId();

            //for(EsqTrxIdBean trxIdBean : listTrx) {
            //    String trxId = trxIdBean.getTrxid();

                //BankAlertThreshold bankAlertThreshold = moduleFactory.getMasterService().getBankAlertThreshold(bankId, trxId);
                if (bankAlertThreshold == null) {
                    continue;
                }


                //check active time
                int bathsh = Integer.parseInt(bankAlertThreshold.getStartHour());
                int bathsm = Integer.parseInt(bankAlertThreshold.getStartMinute());
                int batheh = Integer.parseInt(bankAlertThreshold.getEndHour());
                int bathem = Integer.parseInt(bankAlertThreshold.getEndMinute());
                if (
                        !( ((bathsh*60)+bathsm) < ((nowHr*60)+nowMin) &&
                                ((batheh*60)+bathem) > ((nowHr*60)+nowMin) )
                        ) {
                    continue;
                }

                log.info(bankId + " " + trxId);

                Date prevDateStart = DateUtils.addDate(Calendar.HOUR, nowDateStart, -1 );
                Date prevDateEnd = DateUtils.addDate(Calendar.HOUR, nowDateEnd, -1 );
                String prevPeriod = DateUtils.convertDate(prevDateStart, DateUtils.DATE_YYMMDD);

                Date prevDayDateStart = DateUtils.addDate(Calendar.DAY_OF_MONTH, nowDateStart, -1 );
                Date prevDayDateEnd = DateUtils.addDate(Calendar.DAY_OF_MONTH, nowDateEnd, -1 );
                String prevDayPeriod = DateUtils.convertDate(prevDayDateStart, DateUtils.DATE_YYMMDD);

                log.info(nowDateStart + " - " + nowDateEnd);
                log.info(prevDateStart + " - " + prevDateEnd);
                log.info(prevDayDateStart + " - " + prevDayDateEnd);


                int nowHari = DateUtils.getDayOfWeek(nowDateStart);
                int prevHari = DateUtils.getDayOfWeek(prevDateStart);
                boolean weekday2weekday = false;
                boolean weekday2holiday = false;
                if ( (nowHari == Calendar.SATURDAY || nowHari == Calendar.SUNDAY || nowHari == Calendar.MONDAY) ) {
                    weekday2holiday = true;
                    weekday2weekday = false;
                } else {
                    weekday2holiday = false;
                    weekday2weekday = true;
                }

                //get holiday
                boolean nowHoliday = moduleFactory.getMasterService().checkHoliday(nowDateStart);
                boolean prevHoliday = moduleFactory.getMasterService().checkHoliday(prevDateStart);
                //log.info(nowDateStart + " " + nowHoliday + ", " + prevDateStart + " " + prevHoliday);
                if (nowHoliday || prevHoliday) {
                    weekday2holiday = true;
                    weekday2weekday = false;
                }

                log.info(weekday2weekday + " " + weekday2holiday);

                BigDecimal threshold = BigDecimal.ZERO;
                if (weekday2weekday) {
                    threshold = bankAlertThreshold.getThreshold5MinWdWd();
                    //log.info(threshold + " wdwd");
                } else if (weekday2holiday) {
                    threshold = bankAlertThreshold.getThreshold5MinWdHd();
                    //log.info(threshold + " wdhd");
                }

                if (threshold.compareTo(BigDecimal.ZERO) == 0) {
                    continue;
                }


                //now
                String shourFrom = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(nowDateStart)), "0", 2) + ":00";
                String shourThru = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(nowDateEnd) + 1), "0", 2) + ":00";
                BigDecimal bnow = BigDecimal.ZERO;
                EsqTrx5MinBean nowMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxHourly(bankId, trxId, period,
                        shourFrom, shourThru);
                if (nowMinuteEsqTrx5MinBean == null) continue;
                if (nowMinuteEsqTrx5MinBean != null) {
                    //log.info(nowMinuteEsqTrx5MinBean.getFreq_acq_app());
                    bnow = bnow.add(MathUtils.checkNull(nowMinuteEsqTrx5MinBean.getFreq_acq_app()));
                }

                //prev
                shourFrom = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(prevDateStart)), "0", 2) + ":00";
                shourThru = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(prevDateEnd) + 1), "0", 2) + ":00";
                BigDecimal bprev = BigDecimal.ZERO;
                EsqTrx5MinBean prevMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxHourly(bankId, trxId, prevPeriod,
                        shourFrom, shourThru);
                if (prevMinuteEsqTrx5MinBean != null) {
                    //log.info(prevMinuteEsqTrx5MinBean.getFreq_acq_app());
                    bprev = bprev.add(MathUtils.checkNull(prevMinuteEsqTrx5MinBean.getFreq_acq_app()));
                }


                //prev day
                shourFrom = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(prevDayDateStart)), "0", 2) + ":00";
                shourThru = StringUtils.formatFixLength(
                        Integer.toString(DateUtils.getHour(prevDayDateEnd) + 1), "0", 2) + ":00";
                BigDecimal bprevDay = BigDecimal.ZERO;
                EsqTrx5MinBean prevDayMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxHourly(bankId, trxId, prevDayPeriod,
                        shourFrom, shourThru);
                if (prevDayMinuteEsqTrx5MinBean != null) {
                    //log.info(prevMinuteEsqTrx5MinBean.getFreq_acq_app());
                    bprevDay = bprevDay.add(MathUtils.checkNull(prevDayMinuteEsqTrx5MinBean.getFreq_acq_app()));
                }

                BigDecimal bprevLimitHi = BigDecimal.ONE.add(threshold.divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP));
                BigDecimal bprevLimitLo = BigDecimal.ONE.subtract(threshold.divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP));

                if (bankAlertThreshold.getCompareMethod1().compareTo(BigDecimal.ONE) == 0) {
                    //check method 1
                    //Compare current trx with trx 10 minute (parameter) ago
                    BigDecimal prevLimitHi = bprevLimitHi.multiply(bprev);
                    BigDecimal prevLimitLo = bprevLimitLo.multiply(bprev);

                    if (bnow.compareTo(prevLimitHi) > 0) {
                        log.info("anomaly plus");
                        log.info(bprev + " - " + bprevLimitLo + " - " + bprevLimitHi + " - " + bnow);
                        AlertAnomaly alertAnomaly = new AlertAnomaly();
                        alertAnomaly.setBankId(bankId);
                        alertAnomaly.setTransactionId(trxId);
                        alertAnomaly.setAlertDate(nowDate);
                        alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                        alertAnomaly.setCurrValue(bnow);
                        alertAnomaly.setPrevValue(bprev);
                        alertAnomaly.setThreshold(threshold);
                        alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                        alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M1);
                        alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_HOURLY);
                        biDao.save(alertAnomaly);

                        //send email
                        //emailTo = emailTo.replace()
                        emailSubject = emailSubject.replace("%bank%", bankName);
                        emailSubject = emailSubject.replace("%trx%", trxName);
                        emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M1);
                        emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailSubject = emailSubject.replace("%currval%", bnow.toPlainString());
                        emailSubject = emailSubject.replace("%prevval%", bprev.toPlainString());

                        emailMessage = emailMessage.replace("%bank%", bankName);
                        emailMessage = emailMessage.replace("%trx%", trxName);
                        emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M1);
                        emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailMessage = emailMessage.replace("%currval%", bnow.toPlainString());
                        emailMessage = emailMessage.replace("%prevval%", bprev.toPlainString());

                        EmailQueue emailQueue = new EmailQueue();
                        emailQueue.setEmailTo(emailTo);
                        emailQueue.setEmailMessage(emailMessage);
                        emailQueue.setEmailSubject(emailSubject);
                        emailQueue.setQueueDate(new Date());
                        biDao.save(emailQueue);

                    } else if (bnow.compareTo(prevLimitLo) < 0) {
                        log.info("anomaly minus");
                        log.info(bprev + " - " + bprevLimitLo + " - " + bprevLimitHi + " - " + bnow);
                        AlertAnomaly alertAnomaly = new AlertAnomaly();
                        alertAnomaly.setBankId(bankId);
                        alertAnomaly.setTransactionId(trxId);
                        alertAnomaly.setAlertDate(nowDate);
                        alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                        alertAnomaly.setCurrValue(bnow);
                        alertAnomaly.setPrevValue(bprev);
                        alertAnomaly.setThreshold(threshold);
                        alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__MINUS);
                        alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M1);
                        alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_HOURLY);
                        biDao.save(alertAnomaly);

                        //send email
                        //emailTo = emailTo.replace()
                        emailSubject = emailSubject.replace("%bank%", bankName);
                        emailSubject = emailSubject.replace("%trx%", trxName);
                        emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M1);
                        emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailSubject = emailSubject.replace("%currval%", bnow.toPlainString());
                        emailSubject = emailSubject.replace("%prevval%", bprev.toPlainString());

                        emailMessage = emailMessage.replace("%bank%", bankName);
                        emailMessage = emailMessage.replace("%trx%", trxName);
                        emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M1);
                        emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailMessage = emailMessage.replace("%currval%", bnow.toPlainString());
                        emailMessage = emailMessage.replace("%prevval%", bprev.toPlainString());

                        EmailQueue emailQueue = new EmailQueue();
                        emailQueue.setEmailTo(emailTo);
                        emailQueue.setEmailMessage(emailMessage);
                        emailQueue.setEmailSubject(emailSubject);
                        emailQueue.setQueueDate(new Date());
                        biDao.save(emailQueue);

                    }
                }

                if (bankAlertThreshold.getCompareMethod2().compareTo(BigDecimal.ONE) == 0) {
                    //check method 2
                    //Compare current trx with trx in same minute previous day
                    BigDecimal prevLimitHi = bprevLimitHi.multiply(bprevDay);
                    BigDecimal prevLimitLo = bprevLimitLo.multiply(bprevDay);

                    if (bnow.compareTo(prevLimitHi) > 0) {
                        log.info("anomaly plus");
                        log.info(bprev + " - " + bprevLimitLo + " - " + bprevLimitHi + " - " + bnow);
                        AlertAnomaly alertAnomaly = new AlertAnomaly();
                        alertAnomaly.setBankId(bankId);
                        alertAnomaly.setTransactionId(trxId);
                        alertAnomaly.setAlertDate(nowDate);
                        alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                        alertAnomaly.setCurrValue(bnow);
                        alertAnomaly.setPrevValue(bprev);
                        alertAnomaly.setThreshold(threshold);
                        alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                        alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M2);
                        alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_HOURLY);
                        biDao.save(alertAnomaly);

                        //send email
                        //emailTo = emailTo.replace()
                        emailSubject = emailSubject.replace("%bank%", bankName);
                        emailSubject = emailSubject.replace("%trx%", trxName);
                        emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M2);
                        emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailSubject = emailSubject.replace("%currval%", bnow.toPlainString());
                        emailSubject = emailSubject.replace("%prevval%", bprev.toPlainString());

                        emailMessage = emailMessage.replace("%bank%", bankName);
                        emailMessage = emailMessage.replace("%trx%", trxName);
                        emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M2);
                        emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailMessage = emailMessage.replace("%currval%", bnow.toPlainString());
                        emailMessage = emailMessage.replace("%prevval%", bprev.toPlainString());

                        EmailQueue emailQueue = new EmailQueue();
                        emailQueue.setEmailTo(emailTo);
                        emailQueue.setEmailMessage(emailMessage);
                        emailQueue.setEmailSubject(emailSubject);
                        emailQueue.setQueueDate(new Date());
                        biDao.save(emailQueue);

                    } else if (bnow.compareTo(prevLimitLo) < 0) {
                        log.info("anomaly minus");
                        log.info(bprev + " - " + bprevLimitLo + " - " + bprevLimitHi + " - " + bnow);
                        AlertAnomaly alertAnomaly = new AlertAnomaly();
                        alertAnomaly.setBankId(bankId);
                        alertAnomaly.setTransactionId(trxId);
                        alertAnomaly.setAlertDate(nowDate);
                        alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                        alertAnomaly.setCurrValue(bnow);
                        alertAnomaly.setPrevValue(bprev);
                        alertAnomaly.setThreshold(threshold);
                        alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__MINUS);
                        alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M2);
                        alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_HOURLY);
                        biDao.save(alertAnomaly);

                        //send email
                        //emailTo = emailTo.replace()
                        emailSubject = emailSubject.replace("%bank%", bankName);
                        emailSubject = emailSubject.replace("%trx%", trxName);
                        emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M2);
                        emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailSubject = emailSubject.replace("%currval%", bnow.toPlainString());
                        emailSubject = emailSubject.replace("%prevval%", bprev.toPlainString());

                        emailMessage = emailMessage.replace("%bank%", bankName);
                        emailMessage = emailMessage.replace("%trx%", trxName);
                        emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M2);
                        emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailMessage = emailMessage.replace("%currval%", bnow.toPlainString());
                        emailMessage = emailMessage.replace("%prevval%", bprev.toPlainString());

                        EmailQueue emailQueue = new EmailQueue();
                        emailQueue.setEmailTo(emailTo);
                        emailQueue.setEmailMessage(emailMessage);
                        emailQueue.setEmailSubject(emailSubject);
                        emailQueue.setQueueDate(new Date());
                        biDao.save(emailQueue);

                    }

                }

                if (bankAlertThreshold.getCompareMethod3().compareTo(BigDecimal.ONE) == 0) {
                    //check method 3
                    //Compare if %decline charge greater than %approve
                    BigDecimal total = MathUtils.checkNull(nowMinuteEsqTrx5MinBean.getFreq_acq_app())
                            .add(MathUtils.checkNull(nowMinuteEsqTrx5MinBean.getFreq_acq_dc()))
                            .add(MathUtils.checkNull(nowMinuteEsqTrx5MinBean.getFreq_acq_df()));
                    BigDecimal pctAppr = nowMinuteEsqTrx5MinBean.getFreq_acq_app().divide(total, 6, BigDecimal.ROUND_HALF_UP);
                    BigDecimal pctDecC = nowMinuteEsqTrx5MinBean.getFreq_acq_dc().divide(total, 6, BigDecimal.ROUND_HALF_UP);

                    if (pctDecC.compareTo(pctAppr) > 0) {
                        log.info("%decline charge greater than %approve");
                        log.info(pctAppr + " - " + pctDecC);
                        AlertAnomaly alertAnomaly = new AlertAnomaly();
                        alertAnomaly.setBankId(bankId);
                        alertAnomaly.setTransactionId(trxId);
                        alertAnomaly.setAlertDate(nowDate);
                        alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                        alertAnomaly.setCurrValue(pctAppr);
                        alertAnomaly.setPrevValue(pctDecC);
                        alertAnomaly.setThreshold(null);
                        alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                        alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M3);
                        alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_HOURLY);
                        biDao.save(alertAnomaly);

                        //send email
                        //emailTo = emailTo.replace()
                        emailSubject = emailSubject.replace("%bank%", bankName);
                        emailSubject = emailSubject.replace("%trx%", trxName);
                        emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M3);
                        emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailSubject = emailSubject.replace("%currval%", pctAppr.toPlainString());
                        emailSubject = emailSubject.replace("%prevval%", pctDecC.toPlainString());

                        emailMessage = emailMessage.replace("%bank%", bankName);
                        emailMessage = emailMessage.replace("%trx%", trxName);
                        emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M3);
                        emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailMessage = emailMessage.replace("%currval%", pctAppr.toPlainString());
                        emailMessage = emailMessage.replace("%prevval%", pctDecC.toPlainString());

                        EmailQueue emailQueue = new EmailQueue();
                        emailQueue.setEmailTo(emailTo);
                        emailQueue.setEmailMessage(emailMessage);
                        emailQueue.setEmailSubject(emailSubject);
                        emailQueue.setQueueDate(new Date());
                        biDao.save(emailQueue);

                    }

                }

                if (bankAlertThreshold.getCompareMethod4().compareTo(BigDecimal.ONE) == 0) {
                    //check method 4
                    //Compare if %decline free greater than %approve
                    BigDecimal total = nowMinuteEsqTrx5MinBean.getFreq_acq_app()
                            .add(nowMinuteEsqTrx5MinBean.getFreq_acq_dc())
                            .add(nowMinuteEsqTrx5MinBean.getFreq_acq_df());
                    BigDecimal pctAppr = nowMinuteEsqTrx5MinBean.getFreq_acq_app().divide(total, 6, BigDecimal.ROUND_HALF_UP);
                    BigDecimal pctDecF = nowMinuteEsqTrx5MinBean.getFreq_acq_df().divide(total, 6, BigDecimal.ROUND_HALF_UP);

                    if (pctDecF.compareTo(pctAppr) > 0) {
                        log.info("%decline charge greater than %approve");
                        log.info(pctAppr + " - " + pctDecF);
                        AlertAnomaly alertAnomaly = new AlertAnomaly();
                        alertAnomaly.setBankId(bankId);
                        alertAnomaly.setTransactionId(trxId);
                        alertAnomaly.setAlertDate(nowDate);
                        alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                        alertAnomaly.setCurrValue(pctAppr);
                        alertAnomaly.setPrevValue(pctDecF);
                        alertAnomaly.setThreshold(null);
                        alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                        alertAnomaly.setAlertMethod(DataConstant.ALERT_METHOD_M4);
                        alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_HOURLY);
                        biDao.save(alertAnomaly);

                        //send email
                        //emailTo = emailTo.replace()
                        emailSubject = emailSubject.replace("%bank%", bankName);
                        emailSubject = emailSubject.replace("%trx%", trxName);
                        emailSubject = emailSubject.replace("%m%", DataConstant.ALERT_METHOD_M4);
                        emailSubject = emailSubject.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailSubject = emailSubject.replace("%currval%", pctAppr.toPlainString());
                        emailSubject = emailSubject.replace("%prevval%", pctDecF.toPlainString());

                        emailMessage = emailMessage.replace("%bank%", bankName);
                        emailMessage = emailMessage.replace("%trx%", trxName);
                        emailMessage = emailMessage.replace("%m%", DataConstant.ALERT_METHOD_M4);
                        emailMessage = emailMessage.replace("%date%", DateUtils.convertDate(nowDate, DateUtils.DATE_DDMMYYYY_HHMMSS ));
                        emailMessage = emailMessage.replace("%currval%", pctAppr.toPlainString());
                        emailMessage = emailMessage.replace("%prevval%", pctDecF.toPlainString());

                        EmailQueue emailQueue = new EmailQueue();
                        emailQueue.setEmailTo(emailTo);
                        emailQueue.setEmailMessage(emailMessage);
                        emailQueue.setEmailSubject(emailSubject);
                        emailQueue.setQueueDate(new Date());
                        biDao.save(emailQueue);

                    }

                }

            }
        //}
    }*/


    /*@Override
    @Transactional
    public void checkAlertAnomalyHourly() throws Exception {
        String compareType = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_ALERT_COMPARE_TYPE);

        Date nowDate = new Date();
        Date nowDateStart = DateUtils.getStartOfDay(nowDate);
        Date nowDateEnd = DateUtils.getDateRound5Min(nowDate);

        Date prevDateStart = DateUtils.addDate(Calendar.DAY_OF_MONTH, nowDateStart, -1);
        Date prevDateEnd = DateUtils.addDate(Calendar.DAY_OF_MONTH, nowDateEnd, -1);

        //log.info(nowDateStart);
        //log.info(nowDateEnd);
        //log.info(prevDateStart);
        //log.info(prevDateEnd);

        int nowHari = DateUtils.getDayOfWeek(nowDateStart);
        int prevHari = DateUtils.getDayOfWeek(prevDateStart);
        boolean weekday2weekday = false;
        boolean weekday2holiday = false;
        if ( (nowHari == Calendar.SATURDAY || nowHari == Calendar.SUNDAY || nowHari == Calendar.MONDAY) ) {
            weekday2holiday = true;
            weekday2weekday = false;
        } else {
            weekday2holiday = false;
            weekday2weekday = true;
        }
        //log.info(weekday2weekday + " " + weekday2holiday);
        //get holiday
        boolean nowHoliday = moduleFactory.getMasterService().checkHoliday(nowDateStart);
        boolean prevHoliday = moduleFactory.getMasterService().checkHoliday(prevDateStart);
        log.info(nowDateStart + " " + nowHoliday + ", " + prevDateStart + " " + prevHoliday);
        if (nowHoliday || prevHoliday) {
            weekday2holiday = true;
            weekday2weekday = false;
        }
        //log.info(weekday2weekday + " " + weekday2holiday);

        String period = DateUtils.convertDate(nowDateStart, DateUtils.DATE_YYMMDD);
        String prevPeriod = DateUtils.convertDate(prevDateStart, DateUtils.DATE_YYMMDD);
        String shourFrom = "00:00";

        int hourThru = DateUtils.getHour(nowDateEnd) + 1;
        String shourThru = StringUtils.formatFixLength(Integer.toString(hourThru), "0", 2) + ":00";

        //String stimeFrom = "00:00";
        //String stimeFrom2 = "";
        //String stimeThru = "";
        //int minute = DateUtils.getMinute(nowDateEnd);
        //log.info(minute);
        //if (minute > 0) {
        //   stimeFrom2 = StringUtils.formatFixLength(Integer.toString(hourThru - 1), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(minute-5), "0", 2);
        //    stimeThru = StringUtils.formatFixLength(Integer.toString(hourThru - 1), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(minute), "0", 2);
        //}

        //log.info(period);
        //log.info(shourFrom);
        //log.info(shourThru);
        //log.info(stimeFrom);
        //log.info(stimeFrom2);
        //log.info(stimeThru);

        BigDecimal bnow = BigDecimal.ZERO;
        BigDecimal bprev = BigDecimal.ZERO;
        List<EsqTrxIdBean> listTrx = moduleFactory.getMasterService().listEsqTrxId();
        //List<BankBean> listBank = moduleFactory.getMasterService().listBank(new HashMap<>());
        List<MktBankMonitoring> listBank = moduleFactory.getBiService().listBankMonitoring();
        for (MktBankMonitoring bank : listBank) {
            String bankId = bank.getBankId();

            //REMARK
            if (!bankId.equalsIgnoreCase("BC")) continue;

            for(EsqTrxIdBean trxIdBean : listTrx) {
                String trxId = trxIdBean.getTrxid();
                //log.info(bankId + " " + trxId);
                BankAlertThreshold bankAlertThreshold = moduleFactory.getMasterService().getBankAlertThreshold(bankId, trxId);
                if (bankAlertThreshold == null) {
                    continue;
                }

                BigDecimal threshold = BigDecimal.ZERO;
                if (weekday2weekday) {
                    threshold = bankAlertThreshold.getThresholdHourWdWd();
                    //log.info(threshold + " wdwd");
                } else if (weekday2holiday) {
                    threshold = bankAlertThreshold.getThresholdHourWdHd();
                    //log.info(threshold + " wdhd");
                }

                if (threshold.compareTo(BigDecimal.ZERO) == 0) {
                    continue;
                }

                //now
                bnow = BigDecimal.ZERO;
                if (compareType.equalsIgnoreCase(DataConstant.COMPARE_FROM00)) {
                    EsqTrx5MinBean nowMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxHourly(bankId, trxId, period,
                            "00:00", shourThru);
                    if (nowMinuteEsqTrx5MinBean != null) {
                        //log.info(nowMinuteEsqTrx5MinBean.getFreq_acq_app());
                        bnow = bnow.add(MathUtils.checkNull(nowMinuteEsqTrx5MinBean.getFreq_acq_app()));
                    }
                } else if (compareType.equalsIgnoreCase(DataConstant.COMPARE_FROMHOUR)) {
                    EsqTrx5MinBean nowHourEsqTrx5MinBean = biDao.getTotalEsqTrxHourly(bankId, trxId, period, shourFrom, shourThru);
                    if (nowHourEsqTrx5MinBean != null) {
                        //log.info(nowHourEsqTrx5MinBean.getFreq_acq_app());
                        bnow = bnow.add(MathUtils.checkNull(nowHourEsqTrx5MinBean.getFreq_acq_app()));
                    }
                }

                //prev
                bprev = BigDecimal.ZERO;
                if (compareType.equalsIgnoreCase(DataConstant.COMPARE_FROM00)) {
                    EsqTrx5MinBean prevMinuteEsqTrx5MinBean = biDao.getTotalEsqTrxHourly(bankId, trxId, prevPeriod,
                            shourFrom, shourThru);
                    if (prevMinuteEsqTrx5MinBean != null) {
                        //log.info(prevMinuteEsqTrx5MinBean.getFreq_acq_app());
                        bprev = bprev.add(MathUtils.checkNull(prevMinuteEsqTrx5MinBean.getFreq_acq_app()));
                    }
                } else if (compareType.equalsIgnoreCase(DataConstant.COMPARE_FROMHOUR)) {
                    EsqTrx5MinBean prevHourEsqTrx5MinBean = biDao.getTotalEsqTrxHourly(bankId, trxId, prevPeriod, shourFrom, shourThru);
                    if (prevHourEsqTrx5MinBean != null) {
                        //log.info(prevHourEsqTrx5MinBean.getFreq_acq_app());
                        bnow = bnow.add(MathUtils.checkNull(prevHourEsqTrx5MinBean.getFreq_acq_app()));
                    }
                }

                BigDecimal bprevLimitHi = BigDecimal.ONE.add(threshold.divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP));
                bprevLimitHi = bprevLimitHi.multiply(bprev);
                BigDecimal bprevLimitLo = BigDecimal.ONE.subtract(threshold.divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP));
                bprevLimitLo = bprevLimitLo.multiply(bprev);

                if (bnow.compareTo(bprevLimitHi) > 0) {
                    //log.info("anomaly plus");
                    AlertAnomalyHourly alertAnomaly = new AlertAnomalyHourly();
                    alertAnomaly.setBankId(bankId);
                    alertAnomaly.setTransactionId(trxId);
                    alertAnomaly.setAlertDate(nowDate);
                    alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                    alertAnomaly.setCurrValue(bnow);
                    alertAnomaly.setPrevValue(bprev);
                    alertAnomaly.setThreshold(threshold);
                    alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__PLUS);
                    //alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_HOURLY);
                    biDao.save(alertAnomaly);
                } else if (bnow.compareTo(bprevLimitLo) < 0) {
                    //log.info("anomaly minus");
                    AlertAnomalyHourly alertAnomaly = new AlertAnomalyHourly();
                    alertAnomaly.setBankId(bankId);
                    alertAnomaly.setTransactionId(trxId);
                    alertAnomaly.setAlertDate(nowDate);
                    alertAnomaly.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                    alertAnomaly.setCurrValue(bnow);
                    alertAnomaly.setPrevValue(bprev);
                    alertAnomaly.setThreshold(threshold);
                    alertAnomaly.setTypeAlert(DataConstant.ALERT_ANOMALY_TYPE__MINUS);
                    //alertAnomaly.setAlertPeriod(DataConstant.MONITORING_INTERVAL_HOURLY);
                    biDao.save(alertAnomaly);
                }


            }
        }
    }*/


    /*@Override
    @Transactional
    public void updateMasterAcqIssBnf() throws Exception {
        //delete prev record
        List<EsqMasterAcqIssBnf> listOld = biDao.listEsqMasterAcqIssBnf();
        for(EsqMasterAcqIssBnf aib : listOld) {
            biDao.delete(aib);
        }

        Date now = new Date();
        now = DateUtils.addDate(Calendar.DAY_OF_MONTH, now, -1);
        String period = DateUtils.convertDate(now, DateUtils.DATE_YYMMDD);

        List<DistinctAcqIssBnfBean> list = biDao.distinctAcqIssBnfBean(period);
        for(DistinctAcqIssBnfBean aib : list) {
            EsqMasterAcqIssBnfPk pk = new EsqMasterAcqIssBnfPk();
            pk.setAcqId(aib.getAcqid());
            pk.setIssId(aib.getIssid());
            pk.setBnfId(aib.getBnfid());
            pk.setTrxId(aib.getTrxid());

            EsqMasterAcqIssBnf b = new EsqMasterAcqIssBnf();
            b.setEsqMasterAcqIssBnfPk(pk);
            biDao.save(b);
        }
    }*/

    /*@Override
    @Transactional(readOnly = true)
    public EsqMasterAcqIssBnf getEsqMasterAcqIssBnf(EsqMasterAcqIssBnfPk pk) throws Exception {
        return (EsqMasterAcqIssBnf)biDao.get(EsqMasterAcqIssBnf.class, pk);
    }*/

    /*@Override
    @Transactional
    public void checkAcqIssBnfTrx() throws Exception {
        String period = "";
        String hrfrom = "";
        String hrto = "";

        Date now = new Date();
        period = DateUtils.convertDate(now, DateUtils.DATE_YYMMDD);
        int ihrto = DateUtils.getHour(now);
        hrto = StringUtils.formatFixLength(Integer.toString(ihrto), "0", 2) + ":00";
        Date dfr = DateUtils.addDate(Calendar.HOUR_OF_DAY, now, -1);
        int ihrfrom = DateUtils.getHour(dfr);
        hrfrom = StringUtils.formatFixLength(Integer.toString(ihrfrom), "0", 2) + ":00";

        List<AcqIssBnfTrxBean> list = biDao.checkAcqIssBnfTrx(period, hrfrom, hrto);
        if (list.size() > 0) {
            log.info("distinct null");
            Date date = new Date();
            for(AcqIssBnfTrxBean b : list) {
                EsqAcqIssBnfAlert esqAcqIssBnfAlert = new EsqAcqIssBnfAlert();
                esqAcqIssBnfAlert.setAcqId(b.getAcqid());
                esqAcqIssBnfAlert.setIssId(b.getIssid());
                esqAcqIssBnfAlert.setBnfId(b.getBnfid());
                esqAcqIssBnfAlert.setTrxId(b.getTrxid());
                esqAcqIssBnfAlert.setAlertDate(date);
                esqAcqIssBnfAlert.setSolveStatus(DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
                biDao.save(esqAcqIssBnfAlert);
            }
        }
    }*/

    @Override
    @Transactional(readOnly = true)
    public List<MktUserDashboard> listUserDashboard(String userId) throws Exception {
        return biDao.listUserDashboard(userId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<MktUserDashboardDetail> listUserDashboardDetailEntity(BigDecimal id) throws Exception {
        return biDao.listUserDashboardDetail(id);
    }

    @Override
    @Transactional(readOnly = true)
    public MktUserDashboard getUserDashboard(BigDecimal id) throws Exception {
        return (MktUserDashboard)biDao.get(MktUserDashboard.class, id);
    }

    @Override
    @Transactional(readOnly = true)
    public MktUserDashboardDetail getUserDashboardDetail(MktUserDashboardDetailPk pk) throws Exception {
        return (MktUserDashboardDetail)biDao.get(MktUserDashboardDetail.class, pk);
    }

    @Override
    @Transactional
    public BigDecimal createUserDashboard(MktUserDashboard userDashboard, List<MktUserDashboardDetail> listDetail, UserInfoBean userInfoBean) throws Exception {
        long nt = System.nanoTime();
        BigDecimal id = new BigDecimal(nt);

        userDashboard.setId(id);
        biDao.save(userDashboard);

        BigDecimal seq = BigDecimal.ONE;
        for(MktUserDashboardDetail detail : listDetail) {
            MktUserDashboardDetailPk pk = new MktUserDashboardDetailPk();
            pk.setId(id);
            pk.setSeq(seq);
            detail.setMktUserDashboardDetailPk(pk);
            biDao.save(detail);

            seq = seq.add(BigDecimal.ONE);
        }

        //if user role = "DASHBOARD CREATOR", add map
        if (userInfoBean.getUserLogin().getRoleTypeId() == DataConstant.ROLE_ID_DASHBOARD_CREATOR) {
            DashboardRolePk pk = new DashboardRolePk();
            pk.setDashboardId(id);
            pk.setRoleTypeId(DataConstant.ROLE_ID_DASHBOARD_CREATOR);
            DashboardRole dr = new DashboardRole();
            dr.setDashboardRolePk(pk);
            biDao.save(dr);
        }

        return id;
    }

    @Override
    @Transactional
    public void updateUserDashboard(MktUserDashboard userDashboard, List<MktUserDashboardDetail> listDetail) throws Exception {
        MktUserDashboard ud = getUserDashboard(userDashboard.getId());
        ud.setDescription(userDashboard.getDescription());
        ud.setMenuName(userDashboard.getMenuName());
        biDao.update(ud);

        //delete detail
        List<MktUserDashboardDetail> ld = listUserDashboardDetailEntity(userDashboard.getId());
        for(MktUserDashboardDetail d : ld) {
            biDao.delete(d);
        }

        BigDecimal seq = BigDecimal.ONE;
        for(MktUserDashboardDetail detail : listDetail) {
            MktUserDashboardDetailPk pk = new MktUserDashboardDetailPk();
            pk.setId(userDashboard.getId());
            pk.setSeq(seq);
            detail.setMktUserDashboardDetailPk(pk);
            biDao.save(detail);

            seq = seq.add(BigDecimal.ONE);
        }

    }

    @Override
    @Transactional
    public void deleteUserDashboard(BigDecimal id, UserInfoBean userInfoBean) throws Exception {
        //delete detail
        List<MktUserDashboardDetail> ld = listUserDashboardDetailEntity(id);
        for(MktUserDashboardDetail d : ld) {
            biDao.delete(d);
        }

        MktUserDashboard ud = getUserDashboard(id);
        biDao.delete(ud);

        if (userInfoBean.getUserLogin().getRoleTypeId() == DataConstant.ROLE_ID_DASHBOARD_CREATOR) {
            DashboardRolePk pk = new DashboardRolePk();
            pk.setDashboardId(id);
            pk.setRoleTypeId(DataConstant.ROLE_ID_DASHBOARD_CREATOR);
            DashboardRole dr = (DashboardRole) biDao.get(DashboardRole.class, pk);
            if (dr != null) {
                biDao.delete(dr);
            }
        }
    }


    @Override
    @Transactional(readOnly = true)
    public List<UserDashboardDetailBean> listUserDashboardDetailBean(BigDecimal id) throws Exception {
        return biDao.listUserDashboardDetailBean(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DashboardRoleBean> listDashboardByRole(Integer roleId) throws Exception {
        return biDao.listDashboardByRole(roleId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DashboardRole> checkDashboardRole(Integer roleId) throws Exception {
        return biDao.checkDashboardRole(roleId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DashboardRole> listDashboardRole(Integer roleId) throws Exception {
        return biDao.listDashboardRole(roleId);
    }

    //190524 Added by AAH
    @Override
    @Transactional(readOnly = true)
    public List<DashboardRoleBean> listDashboardRoleNew(Integer roleId) throws Exception {
        return biDao.listDashboardRoleNew(roleId);
    }

    @Override
    @Transactional
    public void updateDashboardRole(Integer roleId, List<DashboardRoleBean> listDashboard) throws Exception {
        //delete prev
        List<DashboardRole> list = biDao.listDashboardRole(roleId);
        for(DashboardRole drb : list) {
            biDao.delete(drb);
        }

        for(DashboardRoleBean deb : listDashboard) {
            if (deb.isSelected()) {
                DashboardRolePk pk = new DashboardRolePk();
                pk.setRoleTypeId(roleId);
                pk.setDashboardId(deb.getDashboardid());
                DashboardRole dr = new DashboardRole();
                dr.setDashboardRolePk(pk);
                biDao.save(dr);
            }
        }

    }




    @Override
    @Transactional(readOnly = true)
    public EsqTrx5MinBean getEsqTrx(String tableName, String bankId, String trxId, String period, String timefr, String timeto) throws Exception {
        return biDao.getEsqTrx(tableName, bankId, trxId, period, timefr, timeto);
    }

    @Override
    @Transactional(readOnly = true)
    public EsqTrx5MinBean getEsqTrxWithPrev(String tableName, String bankId, String trxId,
                                            String period, String periodPrev,
                                            String timefr, String timeto) throws Exception {
        return biDao.getEsqTrxWithPrev(tableName, bankId, trxId, period, periodPrev, timefr, timeto);
    }

}

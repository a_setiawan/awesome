package com.rintis.marketing.core.app.main.module.alert;

import com.rintis.marketing.beans.bean.bi.AlertThresholdBean;
import com.rintis.marketing.beans.entity.AlertThresholdGroup;

import java.util.List;

public interface IAlertService {
    public AlertThresholdGroup getAlertThresholdGroup(String groupId, String trxId, String trxIndicatorId, String timeframe) throws Exception;
    public List<AlertThresholdGroup> listAlertThresholdDetail(String groupId, String trxId) throws Exception;
    public List<AlertThresholdGroup> listAlertThresholdDetail(String groupId) throws Exception;
    public void updateAlertThresholdGroup(List<AlertThresholdGroup> list) throws Exception;
    public void updateAlertThresholdGroupV2(List<AlertThresholdGroup> list) throws Exception;
    public List<AlertThresholdBean> listAlertThresholdGroupHeader() throws Exception;
    public void deleteAlertThresholdGroup(String groupId, String trxId) throws Exception;

}

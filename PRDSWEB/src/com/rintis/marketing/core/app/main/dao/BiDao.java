package com.rintis.marketing.core.app.main.dao;

import com.rintis.marketing.beans.bean.bi.*;
import com.rintis.marketing.beans.entity.*;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.constant.ParameterMapName;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_BI_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_BI_DAO)
public class BiDao extends AbstractDao {
    private Logger log = Logger.getLogger(BiDao.class);

    @Transactional(readOnly = true)
    public List<EsqTrx5MinBean> listEsqTrx5Min(Map<String, Object> param) {
        String bankId = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_BANKID) );
        String period = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_PERIOD) );
        String trxId = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_TRANSACTIONID) );


        String sql = "" +
                "select BANK_ID as \"bankid\", trx_id as \"trxid\", period as \"period\", \n" +
                "HOUR_FR as \"hourfr\", HOUR_TO as \"hourto\", TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +
                "freq_acq_app as \"freq_acq_app\", \n" +
                "freq_acq_dc as \"freq_acq_dc\" , \n" +
                "freq_acq_df as \"freq_acq_df\" , \n" +
                "freq_acq_rvsl as \"freq_acq_rvsl\", \n" +
                "freq_iss_app as \"freq_iss_app\", \n" +
                "freq_iss_dc as \"freq_iss_dc\" , \n" +
                "freq_iss_df as \"freq_iss_df\" , \n" +
                "freq_iss_rvsl as \"freq_iss_rvsl\", \n" +
                "freq_bnf_app as \"freq_bnf_app\", \n" +
                "freq_bnf_dc as  \"freq_bnf_dc\", \n" +
                "freq_bnf_df as \"freq_bnf_df\" , \n" +
                "freq_bnf_rvsl as \"freq_bnf_rvsl\", \n" +
                "freq_acqBnf_app as \"freq_acqBnf_app\", \n" +
                "freq_acqBnf_dc as \"freq_acqBnf_dc\", \n" +
                "freq_acqBnf_df as \"freq_acqBnf_df\", \n" +
                "freq_acqBnf_rvsl as \"freq_acqBnf_rvsl\", \n" +
                "freq_acqIss_app as \"freq_acqIss_app\", \n" +
                "freq_acqIss_dc as \"freq_acqIss_dc\", \n" +
                "freq_acqIss_df as \"freq_acqIss_df\", \n" +
                "freq_acqIss_rvsl as \"freq_acqIss_rvsl\", \n" +
                "freq_issBnf_app as \"freq_issBnf_app\", \n" +
                "freq_issBnf_dc as \"freq_issBnf_dc\", \n" +
                "freq_issBnf_df as \"freq_issBnf_df\", \n" +
                "freq_issBnf_rvsl as \"freq_issBnf_rvsl\" \n" +
                "FROM tms_trx5min\n" +
                "where PERIOD = :period \n" +
                "and BANK_ID = :bankId \n" +
                "and TRX_ID = :trxId \n" +
                "ORDER BY period, HOUR_FR, HOUR_TO, TIME_FR, TIME_TO " +
                "";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("period", period);
        query.setParameter("bankId", bankId);
        query.setParameter("trxId", trxId);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrx5MinBean.class));
        return query.list();
    }

    @Transactional(readOnly = true)
    public EsqTrx5MinBean getEsqTrx5Min(String bankId, String trxId, String period, String hourfr, String hourto,
                                        String timefr, String timeto) {
        String sql = "";

        if (trxId.equalsIgnoreCase(DataConstant.ALL)) {
            sql = "" +
                    "select BANK_ID as \"bankid\", 'ALL' as \"trxid\", period as \"period\", \n" +
                    //"HOUR_FR as \"hourfr\", HOUR_TO as \"hourto\", " +
                    "TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl\" \n" +
                    "FROM tms_trx5min\n" +
                    "where PERIOD = :period \n" +
                    "and BANK_ID = :bankId \n" +
                    //"and hour_fr = :hourfr \n" +
                    //"and hour_to = :hourto \n" +
                    "and time_fr = :timefr \n" +
                    "and time_to = :timeto \n" +
                    "group by BANK_ID, PERIOD, " +
                    //"hour_fr, hour_to, " +
                    "time_fr, time_to \n" +
                    "ORDER BY period, " +
                    //"HOUR_FR, HOUR_TO, " +
                    "TIME_FR, TIME_TO " +
                    "";

        } else {
            sql = "" +
                    "select BANK_ID as \"bankid\", trx_id as \"trxid\", period as \"period\", \n" +
                    //"HOUR_FR as \"hourfr\", HOUR_TO as \"hourto\", " +
                    "TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl\" \n" +
                    "FROM tms_trx5min\n" +
                    "where PERIOD = :period \n" +
                    "and BANK_ID = :bankId \n" +
                    //"and hour_fr = :hourfr \n" +
                    //"and hour_to = :hourto \n" +
                    "and time_fr = :timefr \n" +
                    "and time_to = :timeto \n" +
                    "and TRX_ID = :trxId \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, " +
                    //"hour_fr, hour_to, " +
                    "time_fr, time_to \n" +
                    "ORDER BY period, " +
                    //"HOUR_FR, HOUR_TO, " +
                    "TIME_FR, TIME_TO " +
                    "";
        }

        //log.info(sql);

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("period", period);
        query.setParameter("bankId", bankId);
        if (!trxId.equalsIgnoreCase(DataConstant.ALL)) {
            query.setParameter("trxId", trxId);
        }
        //query.setParameter("hourfr", hourfr);
        //query.setParameter("hourto", hourto);
        query.setParameter("timefr", timefr);
        query.setParameter("timeto", timeto);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrx5MinBean.class));
        return (EsqTrx5MinBean)query.uniqueResult();
    }

    @Transactional(readOnly = true)
    public EsqTrx5MinBean getEsqTrx5MinWithPrev(String bankId, String trxId, String period, String periodPrev, String hourfr, String hourto,
                                        String timefr, String timeto) {
        String sql = "";

        if (trxId.equalsIgnoreCase(DataConstant.ALL)) {
            sql = sql + "select x.\"bankid\", x.\"trxid\", x.\"period\", \n" +
                    "x.\"hourfr\", x.\"hourto\", x.\"timefr\", x.\"timeto\", \n" +
                    "sum(x.\"freq_acq_app\") as \"freq_acq_app\", \n" +
                    "sum(x.\"freq_acq_dc\") as \"freq_acq_dc\" , \n" +
                    "sum(x.\"freq_acq_df\") as \"freq_acq_df\" , \n" +
                    "sum(x.\"freq_acq_rvsl\") as \"freq_acq_rvsl\", \n" +
                    "sum(x.\"freq_iss_app\") as \"freq_iss_app\", \n" +
                    "sum(x.\"freq_iss_dc\") as \"freq_iss_dc\" , \n" +
                    "sum(x.\"freq_iss_df\") as \"freq_iss_df\" , \n" +
                    "sum(x.\"freq_iss_rvsl\") as \"freq_iss_rvsl\", \n" +
                    "sum(x.\"freq_bnf_app\") as \"freq_bnf_app\", \n" +
                    "sum(x.\"freq_bnf_dc\") as  \"freq_bnf_dc\", \n" +
                    "sum(x.\"freq_bnf_df\") as \"freq_bnf_df\" , \n" +
                    "sum(x.\"freq_bnf_rvsl\") as \"freq_bnf_rvsl\", \n" +
                    "sum(x.\"freq_acqBnf_app\") as \"freq_acqBnf_app\", \n" +
                    "sum(x.\"freq_acqBnf_dc\") as \"freq_acqBnf_dc\", \n" +
                    "sum(x.\"freq_acqBnf_df\") as \"freq_acqBnf_df\", \n" +
                    "sum(x.\"freq_acqBnf_rvsl\") as \"freq_acqBnf_rvsl\", \n" +
                    "sum(x.\"freq_acqIss_app\") as \"freq_acqIss_app\", \n" +
                    "sum(x.\"freq_acqIss_dc\") as \"freq_acqIss_dc\", \n" +
                    "sum(x.\"freq_acqIss_df\") as \"freq_acqIss_df\", \n" +
                    "sum(x.\"freq_acqIss_rvsl\") as \"freq_acqIss_rvsl\", \n" +
                    "sum(x.\"freq_issBnf_app\") as \"freq_issBnf_app\", \n" +
                    "sum(x.\"freq_issBnf_dc\") as \"freq_issBnf_dc\", \n" +
                    "sum(x.\"freq_issBnf_df\") as \"freq_issBnf_df\", \n" +
                    "sum(x.\"freq_issBnf_rvsl\") as \"freq_issBnf_rvsl\", \n" +
                    "x.\"prevperiod\" as \"prevperiod\", \n" +
                    "sum(x.\"freq_acq_app_prev\") as \"freq_acq_app_prev\", \n" +
                    "sum(x.\"freq_acq_dc_prev\") as \"freq_acq_dc_prev\" , \n" +
                    "sum(x.\"freq_acq_df_prev\") as \"freq_acq_df_prev\" , \n" +
                    "sum(x.\"freq_acq_rvsl_prev\") as \"freq_acq_rvsl_prev\", \n" +
                    "sum(x.\"freq_iss_app_prev\") as \"freq_iss_app_prev\", \n" +
                    "sum(x.\"freq_iss_dc_prev\") as \"freq_iss_dc_prev\" , \n" +
                    "sum(x.\"freq_iss_df_prev\") as \"freq_iss_df_prev\" , \n" +
                    "sum(x.\"freq_iss_rvsl_prev\") as \"freq_iss_rvsl_prev\", \n" +
                    "sum(x.\"freq_bnf_app_prev\") as \"freq_bnf_app_prev\", \n" +
                    "sum(x.\"freq_bnf_dc_prev\") as  \"freq_bnf_dc_prev\", \n" +
                    "sum(x.\"freq_bnf_df_prev\") as \"freq_bnf_df_prev\" , \n" +
                    "sum(x.\"freq_bnf_rvsl_prev\") as \"freq_bnf_rvsl_prev\", \n" +
                    "sum(x.\"freq_acqBnf_app_prev\") as \"freq_acqBnf_app_prev\", \n" +
                    "sum(x.\"freq_acqBnf_dc_prev\") as \"freq_acqBnf_dc_prev\", \n" +
                    "sum(x.\"freq_acqBnf_df_prev\") as \"freq_acqBnf_df_prev\", \n" +
                    "sum(x.\"freq_acqBnf_rvsl_prev\") as \"freq_acqBnf_rvsl_prev\", \n" +
                    "sum(x.\"freq_acqIss_app_prev\") as \"freq_acqIss_app_prev\", \n" +
                    "sum(x.\"freq_acqIss_dc_prev\") as \"freq_acqIss_dc_prev\", \n" +
                    "sum(x.\"freq_acqIss_df_prev\") as \"freq_acqIss_df_prev\", \n" +
                    "sum(x.\"freq_acqIss_rvsl_prev\") as \"freq_acqIss_rvsl_prev\", \n" +
                    "sum(x.\"freq_issBnf_app_prev\") as \"freq_issBnf_app_prev\", \n" +
                    "sum(x.\"freq_issBnf_dc_prev\") as \"freq_issBnf_dc_prev\", \n" +
                    "sum(x.\"freq_issBnf_df_prev\") as \"freq_issBnf_df_prev\", \n" +
                    "sum(x.\"freq_issBnf_rvsl_prev\") as \"freq_issBnf_rvsl_prev\" \n" +
                    "from ( \n";

            sql = sql + " " +
                    "select BANK_ID as \"bankid\", 'ALL' as \"trxid\", period as \"period\", \n" +
                    "HOUR_FR as \"hourfr\", HOUR_TO as \"hourto\", TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl\", \n" +

                    "'" + periodPrev + "' as \"prevperiod\", \n" +
                    "0 as \"freq_acq_app_prev\", \n" +
                    "0 as \"freq_acq_dc_prev\" , \n" +
                    "0 as \"freq_acq_df_prev\" , \n" +
                    "0 as \"freq_acq_rvsl_prev\", \n" +
                    "0 as \"freq_iss_app_prev\", \n" +
                    "0 as \"freq_iss_dc_prev\" , \n" +
                    "0 as \"freq_iss_df_prev\" , \n" +
                    "0 as \"freq_iss_rvsl_prev\", \n" +
                    "0 as \"freq_bnf_app_prev\", \n" +
                    "0 as  \"freq_bnf_dc_prev\", \n" +
                    "0 as \"freq_bnf_df_prev\" , \n" +
                    "0 as \"freq_bnf_rvsl_prev\", \n" +
                    "0 as \"freq_acqBnf_app_prev\", \n" +
                    "0 as \"freq_acqBnf_dc_prev\", \n" +
                    "0 as \"freq_acqBnf_df_prev\", \n" +
                    "0 as \"freq_acqBnf_rvsl_prev\", \n" +
                    "0 as \"freq_acqIss_app_prev\", \n" +
                    "0 as \"freq_acqIss_dc_prev\", \n" +
                    "0 as \"freq_acqIss_df_prev\", \n" +
                    "0 as \"freq_acqIss_rvsl_prev\", \n" +
                    "0 as \"freq_issBnf_app_prev\", \n" +
                    "0 as \"freq_issBnf_dc_prev\", \n" +
                    "0 as \"freq_issBnf_df_prev\", \n" +
                    "0 as \"freq_issBnf_rvsl_prev\" \n" +

                    "FROM tms_trx5min\n" +
                    "where PERIOD = :period \n" +
                    "and BANK_ID = :bankId \n" +
                    "and hour_fr = :hourfr \n" +
                    "and hour_to = :hourto \n" +
                    "and time_fr = :timefr \n" +
                    "and time_to = :timeto \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, hour_fr, hour_to, time_fr, time_to \n" +

                    "union \n" +

                    "select BANK_ID as \"bankid\", 'ALL' as \"trxid\", '" + period + "' as \"period\", \n" +
                    "HOUR_FR as \"hourfr\", HOUR_TO as \"hourto\", TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +

                    "0 as \"freq_acq_app\", \n" +
                    "0 as \"freq_acq_dc\" , \n" +
                    "0 as \"freq_acq_df\" , \n" +
                    "0 as \"freq_acq_rvsl\", \n" +
                    "0 as \"freq_iss_app\", \n" +
                    "0 as \"freq_iss_dc\" , \n" +
                    "0 as \"freq_iss_df\" , \n" +
                    "0 as \"freq_iss_rvsl\", \n" +
                    "0 as \"freq_bnf_app\", \n" +
                    "0 as  \"freq_bnf_dc\", \n" +
                    "0 as \"freq_bnf_df\" , \n" +
                    "0 as \"freq_bnf_rvsl\", \n" +
                    "0 as \"freq_acqBnf_app\", \n" +
                    "0 as \"freq_acqBnf_dc\", \n" +
                    "0 as \"freq_acqBnf_df\", \n" +
                    "0 as \"freq_acqBnf_rvsl\", \n" +
                    "0 as \"freq_acqIss_app\", \n" +
                    "0 as \"freq_acqIss_dc\", \n" +
                    "0 as \"freq_acqIss_df\", \n" +
                    "0 as \"freq_acqIss_rvsl\", \n" +
                    "0 as \"freq_issBnf_app\", \n" +
                    "0 as \"freq_issBnf_dc\", \n" +
                    "0 as \"freq_issBnf_df\", \n" +
                    "0 as \"freq_issBnf_rvsl\", \n" +

                    "'" + periodPrev + "' as \"prevperiod\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app_prev\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc_prev\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df_prev\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl_prev\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app_prev\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc_prev\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df_prev\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl_prev\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app_prev\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc_prev\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df_prev\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl_prev\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app_prev\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc_prev\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df_prev\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl_prev\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app_prev\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc_prev\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df_prev\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl_prev\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app_prev\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc_prev\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df_prev\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl_prev\" \n" +
                    "FROM tms_trx5minhist \n" +
                    "where PERIOD = :periodPrev \n" +
                    "and BANK_ID = :bankId \n" +
                    "and hour_fr = :hourfr \n" +
                    "and hour_to = :hourto \n" +
                    "and time_fr = :timefr \n" +
                    "and time_to = :timeto \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, hour_fr, hour_to, time_fr, time_to \n" +

                    ") x \n" +

                    "GROUP BY x.\"bankid\",  x.\"trxid\", x.\"period\", x.\"hourfr\", x.\"hourto\", x.\"timefr\", x.\"timeto\", x.\"prevperiod\" " +
                    "";

        } else {
            sql = sql + "select x.\"bankid\", x.\"trxid\", x.\"period\", \n" +
                    "x.\"hourfr\", x.\"hourto\", x.\"timefr\", x.\"timeto\", \n" +
                    "sum(x.\"freq_acq_app\") as \"freq_acq_app\", \n" +
                    "sum(x.\"freq_acq_dc\") as \"freq_acq_dc\" , \n" +
                    "sum(x.\"freq_acq_df\") as \"freq_acq_df\" , \n" +
                    "sum(x.\"freq_acq_rvsl\") as \"freq_acq_rvsl\", \n" +
                    "sum(x.\"freq_iss_app\") as \"freq_iss_app\", \n" +
                    "sum(x.\"freq_iss_dc\") as \"freq_iss_dc\" , \n" +
                    "sum(x.\"freq_iss_df\") as \"freq_iss_df\" , \n" +
                    "sum(x.\"freq_iss_rvsl\") as \"freq_iss_rvsl\", \n" +
                    "sum(x.\"freq_bnf_app\") as \"freq_bnf_app\", \n" +
                    "sum(x.\"freq_bnf_dc\") as  \"freq_bnf_dc\", \n" +
                    "sum(x.\"freq_bnf_df\") as \"freq_bnf_df\" , \n" +
                    "sum(x.\"freq_bnf_rvsl\") as \"freq_bnf_rvsl\", \n" +
                    "sum(x.\"freq_acqBnf_app\") as \"freq_acqBnf_app\", \n" +
                    "sum(x.\"freq_acqBnf_dc\") as \"freq_acqBnf_dc\", \n" +
                    "sum(x.\"freq_acqBnf_df\") as \"freq_acqBnf_df\", \n" +
                    "sum(x.\"freq_acqBnf_rvsl\") as \"freq_acqBnf_rvsl\", \n" +
                    "sum(x.\"freq_acqIss_app\") as \"freq_acqIss_app\", \n" +
                    "sum(x.\"freq_acqIss_dc\") as \"freq_acqIss_dc\", \n" +
                    "sum(x.\"freq_acqIss_df\") as \"freq_acqIss_df\", \n" +
                    "sum(x.\"freq_acqIss_rvsl\") as \"freq_acqIss_rvsl\", \n" +
                    "sum(x.\"freq_issBnf_app\") as \"freq_issBnf_app\", \n" +
                    "sum(x.\"freq_issBnf_dc\") as \"freq_issBnf_dc\", \n" +
                    "sum(x.\"freq_issBnf_df\") as \"freq_issBnf_df\", \n" +
                    "sum(x.\"freq_issBnf_rvsl\") as \"freq_issBnf_rvsl\", \n" +
                    "x.\"prevperiod\" as \"prevperiod\", \n" +
                    "sum(x.\"freq_acq_app_prev\") as \"freq_acq_app_prev\", \n" +
                    "sum(x.\"freq_acq_dc_prev\") as \"freq_acq_dc_prev\" , \n" +
                    "sum(x.\"freq_acq_df_prev\") as \"freq_acq_df_prev\" , \n" +
                    "sum(x.\"freq_acq_rvsl_prev\") as \"freq_acq_rvsl_prev\", \n" +
                    "sum(x.\"freq_iss_app_prev\") as \"freq_iss_app_prev\", \n" +
                    "sum(x.\"freq_iss_dc_prev\") as \"freq_iss_dc_prev\" , \n" +
                    "sum(x.\"freq_iss_df_prev\") as \"freq_iss_df_prev\" , \n" +
                    "sum(x.\"freq_iss_rvsl_prev\") as \"freq_iss_rvsl_prev\", \n" +
                    "sum(x.\"freq_bnf_app_prev\") as \"freq_bnf_app_prev\", \n" +
                    "sum(x.\"freq_bnf_dc_prev\") as  \"freq_bnf_dc_prev\", \n" +
                    "sum(x.\"freq_bnf_df_prev\") as \"freq_bnf_df_prev\" , \n" +
                    "sum(x.\"freq_bnf_rvsl_prev\") as \"freq_bnf_rvsl_prev\", \n" +
                    "sum(x.\"freq_acqBnf_app_prev\") as \"freq_acqBnf_app_prev\", \n" +
                    "sum(x.\"freq_acqBnf_dc_prev\") as \"freq_acqBnf_dc_prev\", \n" +
                    "sum(x.\"freq_acqBnf_df_prev\") as \"freq_acqBnf_df_prev\", \n" +
                    "sum(x.\"freq_acqBnf_rvsl_prev\") as \"freq_acqBnf_rvsl_prev\", \n" +
                    "sum(x.\"freq_acqIss_app_prev\") as \"freq_acqIss_app_prev\", \n" +
                    "sum(x.\"freq_acqIss_dc_prev\") as \"freq_acqIss_dc_prev\", \n" +
                    "sum(x.\"freq_acqIss_df_prev\") as \"freq_acqIss_df_prev\", \n" +
                    "sum(x.\"freq_acqIss_rvsl_prev\") as \"freq_acqIss_rvsl_prev\", \n" +
                    "sum(x.\"freq_issBnf_app_prev\") as \"freq_issBnf_app_prev\", \n" +
                    "sum(x.\"freq_issBnf_dc_prev\") as \"freq_issBnf_dc_prev\", \n" +
                    "sum(x.\"freq_issBnf_df_prev\") as \"freq_issBnf_df_prev\", \n" +
                    "sum(x.\"freq_issBnf_rvsl_prev\") as \"freq_issBnf_rvsl_prev\" \n" +
                    "from ( \n";

            sql = sql + " " +
                    "select BANK_ID as \"bankid\", trx_id as \"trxid\", period as \"period\", \n" +
                    "HOUR_FR as \"hourfr\", HOUR_TO as \"hourto\", TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl\", \n" +

                    "'" + periodPrev + "' as \"prevperiod\", \n" +
                    "0 as \"freq_acq_app_prev\", \n" +
                    "0 as \"freq_acq_dc_prev\" , \n" +
                    "0 as \"freq_acq_df_prev\" , \n" +
                    "0 as \"freq_acq_rvsl_prev\", \n" +
                    "0 as \"freq_iss_app_prev\", \n" +
                    "0 as \"freq_iss_dc_prev\" , \n" +
                    "0 as \"freq_iss_df_prev\" , \n" +
                    "0 as \"freq_iss_rvsl_prev\", \n" +
                    "0 as \"freq_bnf_app_prev\", \n" +
                    "0 as  \"freq_bnf_dc_prev\", \n" +
                    "0 as \"freq_bnf_df_prev\" , \n" +
                    "0 as \"freq_bnf_rvsl_prev\", \n" +
                    "0 as \"freq_acqBnf_app_prev\", \n" +
                    "0 as \"freq_acqBnf_dc_prev\", \n" +
                    "0 as \"freq_acqBnf_df_prev\", \n" +
                    "0 as \"freq_acqBnf_rvsl_prev\", \n" +
                    "0 as \"freq_acqIss_app_prev\", \n" +
                    "0 as \"freq_acqIss_dc_prev\", \n" +
                    "0 as \"freq_acqIss_df_prev\", \n" +
                    "0 as \"freq_acqIss_rvsl_prev\", \n" +
                    "0 as \"freq_issBnf_app_prev\", \n" +
                    "0 as \"freq_issBnf_dc_prev\", \n" +
                    "0 as \"freq_issBnf_df_prev\", \n" +
                    "0 as \"freq_issBnf_rvsl_prev\" \n" +

                    "FROM tms_trx5min\n" +
                    "where PERIOD = :period \n" +
                    "and BANK_ID = :bankId \n" +
                    "and hour_fr = :hourfr \n" +
                    "and hour_to = :hourto \n" +
                    "and time_fr = :timefr \n" +
                    "and time_to = :timeto \n" +
                    "and TRX_ID = :trxId \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, hour_fr, hour_to, time_fr, time_to \n" +

                    "union \n" +

                    "select BANK_ID as \"bankid\", trx_id as \"trxid\", '" + period + "' as \"period\", \n" +
                    "HOUR_FR as \"hourfr\", HOUR_TO as \"hourto\", TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +

                    "0 as \"freq_acq_app\", \n" +
                    "0 as \"freq_acq_dc\" , \n" +
                    "0 as \"freq_acq_df\" , \n" +
                    "0 as \"freq_acq_rvsl\", \n" +
                    "0 as \"freq_iss_app\", \n" +
                    "0 as \"freq_iss_dc\" , \n" +
                    "0 as \"freq_iss_df\" , \n" +
                    "0 as \"freq_iss_rvsl\", \n" +
                    "0 as \"freq_bnf_app\", \n" +
                    "0 as  \"freq_bnf_dc\", \n" +
                    "0 as \"freq_bnf_df\" , \n" +
                    "0 as \"freq_bnf_rvsl\", \n" +
                    "0 as \"freq_acqBnf_app\", \n" +
                    "0 as \"freq_acqBnf_dc\", \n" +
                    "0 as \"freq_acqBnf_df\", \n" +
                    "0 as \"freq_acqBnf_rvsl\", \n" +
                    "0 as \"freq_acqIss_app\", \n" +
                    "0 as \"freq_acqIss_dc\", \n" +
                    "0 as \"freq_acqIss_df\", \n" +
                    "0 as \"freq_acqIss_rvsl\", \n" +
                    "0 as \"freq_issBnf_app\", \n" +
                    "0 as \"freq_issBnf_dc\", \n" +
                    "0 as \"freq_issBnf_df\", \n" +
                    "0 as \"freq_issBnf_rvsl\", \n" +

                    "'" + periodPrev + "' as \"prevperiod\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app_prev\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc_prev\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df_prev\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl_prev\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app_prev\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc_prev\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df_prev\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl_prev\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app_prev\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc_prev\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df_prev\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl_prev\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app_prev\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc_prev\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df_prev\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl_prev\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app_prev\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc_prev\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df_prev\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl_prev\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app_prev\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc_prev\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df_prev\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl_prev\" \n" +
                    "FROM tms_trx5minhist \n" +
                    "where PERIOD = :periodPrev \n" +
                    "and BANK_ID = :bankId \n" +
                    "and hour_fr = :hourfr \n" +
                    "and hour_to = :hourto \n" +
                    "and time_fr = :timefr \n" +
                    "and time_to = :timeto \n" +
                    "and TRX_ID = :trxId \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, hour_fr, hour_to, time_fr, time_to \n" +

                    ") x \n" +

                    "GROUP BY x.\"bankid\",  x.\"trxid\", x.\"period\", x.\"hourfr\", x.\"hourto\", x.\"timefr\", x.\"timeto\", x.\"prevperiod\" " +
                    "";
        }



        /*
        "'" + periodPrev + "' as prevperiod \n" +
                    "0 as \"freq_acq_app\", \n" +
                    "0 as \"freq_acq_dc\" , \n" +
                    "0 as \"freq_acq_df\" , \n" +
                    "0 as \"freq_acq_rvsl\", \n" +
                    "0 as \"freq_iss_app\", \n" +
                    "0 as \"freq_iss_dc\" , \n" +
                    "0 as \"freq_iss_df\" , \n" +
                    "0 as \"freq_iss_rvsl\", \n" +
                    "0 as \"freq_bnf_app\", \n" +
                    "0 as  \"freq_bnf_dc\", \n" +
                    "0 as \"freq_bnf_df\" , \n" +
                    "0 as \"freq_bnf_rvsl\", \n" +
                    "0 as \"freq_acqBnf_app\", \n" +
                    "0 as \"freq_acqBnf_dc\", \n" +
                    "0 as \"freq_acqBnf_df\", \n" +
                    "0 as \"freq_acqBnf_rvsl\", \n" +
                    "0 as \"freq_acqIss_app\", \n" +
                    "0 as \"freq_acqIss_dc\", \n" +
                    "0 as \"freq_acqIss_df\", \n" +
                    "0 as \"freq_acqIss_rvsl\", \n" +
                    "0 as \"freq_issBnf_app\", \n" +
                    "0 as \"freq_issBnf_dc\", \n" +
                    "0 as \"freq_issBnf_df\", \n" +
                    "0 as \"freq_issBnf_rvsl\" \n" +
         */

        //log.info(sql);

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("period", period);
        query.setParameter("periodPrev", periodPrev);
        query.setParameter("bankId", bankId);
        if (!trxId.equalsIgnoreCase(DataConstant.ALL)) {
            query.setParameter("trxId", trxId);
        }
        query.setParameter("hourfr", hourfr);
        query.setParameter("hourto", hourto);
        query.setParameter("timefr", timefr);
        query.setParameter("timeto", timeto);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrx5MinBean.class));
        return (EsqTrx5MinBean)query.uniqueResult();
    }

    @Transactional(readOnly = true)
    public EsqTrx5MinBean getEsqTrxHourly(String bankId, String trxId, String period, String timefr, String timeto) {
        String sql = "";

        if (trxId.equalsIgnoreCase(DataConstant.ALL)) {
            sql = "" +
                    "select BANK_ID as \"bankid\", 'ALL' as \"trxid\", period as \"period\", \n" +
                    "TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl\" \n" +
                    "FROM tms_trx60min\n" +
                    "where PERIOD = :period \n" +
                    "and BANK_ID = :bankId \n" +
                    "and timer_fr = :timefr \n" +
                    "and time_to = :timeto \n" +
                    "group by BANK_ID, PERIOD, time_fr, time_to \n" +
                    "ORDER BY period, TIME_FR, TIME_TO " +
                    "";

        } else {
            sql = "" +
                    "select BANK_ID as \"bankid\", trx_id as \"trxid\", period as \"period\", \n" +
                    "TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl\" \n" +
                    "FROM tms_trx60min\n" +
                    "where PERIOD = :period \n" +
                    "and BANK_ID = :bankId \n" +
                    "and TRX_ID = :trxId \n" +
                    "and time_fr = :timefr \n" +
                    "and time_to = :timeto \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, time_fr, time_to \n" +
                    "ORDER BY period, time_fr, time_to " +
                    "";
        }

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("period", period);
        query.setParameter("bankId", bankId);
        if (!trxId.equalsIgnoreCase(DataConstant.ALL)) {
            query.setParameter("trxId", trxId);
        }
        query.setParameter("timefr", timefr);
        query.setParameter("timeto", timeto);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrx5MinBean.class));
        return (EsqTrx5MinBean)query.uniqueResult();
    }



    @Transactional(readOnly = true)
    public EsqTrx5MinBean getEsqTrxHourlyWithPrev(String bankId, String trxId, String period, String periodPrev, String hourfr, String hourto) {
        String sql = "";

        if (trxId.equalsIgnoreCase(DataConstant.ALL)) {
            sql = sql + "select x.\"bankid\", x.\"trxid\", x.\"period\", \n" +
                    "x.\"hourfr\", x.\"hourto\",  \n" +
                    "sum(x.\"freq_acq_app\") as \"freq_acq_app\", \n" +
                    "sum(x.\"freq_acq_dc\") as \"freq_acq_dc\" , \n" +
                    "sum(x.\"freq_acq_df\") as \"freq_acq_df\" , \n" +
                    "sum(x.\"freq_acq_rvsl\") as \"freq_acq_rvsl\", \n" +
                    "sum(x.\"freq_iss_app\") as \"freq_iss_app\", \n" +
                    "sum(x.\"freq_iss_dc\") as \"freq_iss_dc\" , \n" +
                    "sum(x.\"freq_iss_df\") as \"freq_iss_df\" , \n" +
                    "sum(x.\"freq_iss_rvsl\") as \"freq_iss_rvsl\", \n" +
                    "sum(x.\"freq_bnf_app\") as \"freq_bnf_app\", \n" +
                    "sum(x.\"freq_bnf_dc\") as  \"freq_bnf_dc\", \n" +
                    "sum(x.\"freq_bnf_df\") as \"freq_bnf_df\" , \n" +
                    "sum(x.\"freq_bnf_rvsl\") as \"freq_bnf_rvsl\", \n" +
                    "sum(x.\"freq_acqBnf_app\") as \"freq_acqBnf_app\", \n" +
                    "sum(x.\"freq_acqBnf_dc\") as \"freq_acqBnf_dc\", \n" +
                    "sum(x.\"freq_acqBnf_df\") as \"freq_acqBnf_df\", \n" +
                    "sum(x.\"freq_acqBnf_rvsl\") as \"freq_acqBnf_rvsl\", \n" +
                    "sum(x.\"freq_acqIss_app\") as \"freq_acqIss_app\", \n" +
                    "sum(x.\"freq_acqIss_dc\") as \"freq_acqIss_dc\", \n" +
                    "sum(x.\"freq_acqIss_df\") as \"freq_acqIss_df\", \n" +
                    "sum(x.\"freq_acqIss_rvsl\") as \"freq_acqIss_rvsl\", \n" +
                    "sum(x.\"freq_issBnf_app\") as \"freq_issBnf_app\", \n" +
                    "sum(x.\"freq_issBnf_dc\") as \"freq_issBnf_dc\", \n" +
                    "sum(x.\"freq_issBnf_df\") as \"freq_issBnf_df\", \n" +
                    "sum(x.\"freq_issBnf_rvsl\") as \"freq_issBnf_rvsl\", \n" +
                    "x.\"prevperiod\" as \"prevperiod\", \n" +
                    "sum(x.\"freq_acq_app_prev\") as \"freq_acq_app_prev\", \n" +
                    "sum(x.\"freq_acq_dc_prev\") as \"freq_acq_dc_prev\" , \n" +
                    "sum(x.\"freq_acq_df_prev\") as \"freq_acq_df_prev\" , \n" +
                    "sum(x.\"freq_acq_rvsl_prev\") as \"freq_acq_rvsl_prev\", \n" +
                    "sum(x.\"freq_iss_app_prev\") as \"freq_iss_app_prev\", \n" +
                    "sum(x.\"freq_iss_dc_prev\") as \"freq_iss_dc_prev\" , \n" +
                    "sum(x.\"freq_iss_df_prev\") as \"freq_iss_df_prev\" , \n" +
                    "sum(x.\"freq_iss_rvsl_prev\") as \"freq_iss_rvsl_prev\", \n" +
                    "sum(x.\"freq_bnf_app_prev\") as \"freq_bnf_app_prev\", \n" +
                    "sum(x.\"freq_bnf_dc_prev\") as  \"freq_bnf_dc_prev\", \n" +
                    "sum(x.\"freq_bnf_df_prev\") as \"freq_bnf_df_prev\" , \n" +
                    "sum(x.\"freq_bnf_rvsl_prev\") as \"freq_bnf_rvsl_prev\", \n" +
                    "sum(x.\"freq_acqBnf_app_prev\") as \"freq_acqBnf_app_prev\", \n" +
                    "sum(x.\"freq_acqBnf_dc_prev\") as \"freq_acqBnf_dc_prev\", \n" +
                    "sum(x.\"freq_acqBnf_df_prev\") as \"freq_acqBnf_df_prev\", \n" +
                    "sum(x.\"freq_acqBnf_rvsl_prev\") as \"freq_acqBnf_rvsl_prev\", \n" +
                    "sum(x.\"freq_acqIss_app_prev\") as \"freq_acqIss_app_prev\", \n" +
                    "sum(x.\"freq_acqIss_dc_prev\") as \"freq_acqIss_dc_prev\", \n" +
                    "sum(x.\"freq_acqIss_df_prev\") as \"freq_acqIss_df_prev\", \n" +
                    "sum(x.\"freq_acqIss_rvsl_prev\") as \"freq_acqIss_rvsl_prev\", \n" +
                    "sum(x.\"freq_issBnf_app_prev\") as \"freq_issBnf_app_prev\", \n" +
                    "sum(x.\"freq_issBnf_dc_prev\") as \"freq_issBnf_dc_prev\", \n" +
                    "sum(x.\"freq_issBnf_df_prev\") as \"freq_issBnf_df_prev\", \n" +
                    "sum(x.\"freq_issBnf_rvsl_prev\") as \"freq_issBnf_rvsl_prev\" \n" +
                    "from ( \n";

            sql = sql + " " +
                    "select BANK_ID as \"bankid\", 'ALL' as \"trxid\", period as \"period\", \n" +
                    "HOUR_FR as \"hourfr\", HOUR_TO as \"hourto\",  \n" +
                    "sum(freq_acq_app) as \"freq_acq_app\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl\", \n" +

                    "'" + periodPrev + "' as \"prevperiod\", \n" +
                    "0 as \"freq_acq_app_prev\", \n" +
                    "0 as \"freq_acq_dc_prev\" , \n" +
                    "0 as \"freq_acq_df_prev\" , \n" +
                    "0 as \"freq_acq_rvsl_prev\", \n" +
                    "0 as \"freq_iss_app_prev\", \n" +
                    "0 as \"freq_iss_dc_prev\" , \n" +
                    "0 as \"freq_iss_df_prev\" , \n" +
                    "0 as \"freq_iss_rvsl_prev\", \n" +
                    "0 as \"freq_bnf_app_prev\", \n" +
                    "0 as  \"freq_bnf_dc_prev\", \n" +
                    "0 as \"freq_bnf_df_prev\" , \n" +
                    "0 as \"freq_bnf_rvsl_prev\", \n" +
                    "0 as \"freq_acqBnf_app_prev\", \n" +
                    "0 as \"freq_acqBnf_dc_prev\", \n" +
                    "0 as \"freq_acqBnf_df_prev\", \n" +
                    "0 as \"freq_acqBnf_rvsl_prev\", \n" +
                    "0 as \"freq_acqIss_app_prev\", \n" +
                    "0 as \"freq_acqIss_dc_prev\", \n" +
                    "0 as \"freq_acqIss_df_prev\", \n" +
                    "0 as \"freq_acqIss_rvsl_prev\", \n" +
                    "0 as \"freq_issBnf_app_prev\", \n" +
                    "0 as \"freq_issBnf_dc_prev\", \n" +
                    "0 as \"freq_issBnf_df_prev\", \n" +
                    "0 as \"freq_issBnf_rvsl_prev\" \n" +

                    "FROM tms_trx5min\n" +
                    "where PERIOD = :period \n" +
                    "and BANK_ID = :bankId \n" +
                    "and hour_fr = :hourfr \n" +
                    "and hour_to = :hourto \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, hour_fr, hour_to \n" +

                    "union \n" +

                    "select BANK_ID as \"bankid\", 'ALL' as \"trxid\", '" + period + "' as \"period\", \n" +
                    "HOUR_FR as \"hourfr\", HOUR_TO as \"hourto\",  \n" +

                    "0 as \"freq_acq_app\", \n" +
                    "0 as \"freq_acq_dc\" , \n" +
                    "0 as \"freq_acq_df\" , \n" +
                    "0 as \"freq_acq_rvsl\", \n" +
                    "0 as \"freq_iss_app\", \n" +
                    "0 as \"freq_iss_dc\" , \n" +
                    "0 as \"freq_iss_df\" , \n" +
                    "0 as \"freq_iss_rvsl\", \n" +
                    "0 as \"freq_bnf_app\", \n" +
                    "0 as  \"freq_bnf_dc\", \n" +
                    "0 as \"freq_bnf_df\" , \n" +
                    "0 as \"freq_bnf_rvsl\", \n" +
                    "0 as \"freq_acqBnf_app\", \n" +
                    "0 as \"freq_acqBnf_dc\", \n" +
                    "0 as \"freq_acqBnf_df\", \n" +
                    "0 as \"freq_acqBnf_rvsl\", \n" +
                    "0 as \"freq_acqIss_app\", \n" +
                    "0 as \"freq_acqIss_dc\", \n" +
                    "0 as \"freq_acqIss_df\", \n" +
                    "0 as \"freq_acqIss_rvsl\", \n" +
                    "0 as \"freq_issBnf_app\", \n" +
                    "0 as \"freq_issBnf_dc\", \n" +
                    "0 as \"freq_issBnf_df\", \n" +
                    "0 as \"freq_issBnf_rvsl\", \n" +

                    "'" + periodPrev + "' as \"prevperiod\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app_prev\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc_prev\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df_prev\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl_prev\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app_prev\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc_prev\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df_prev\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl_prev\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app_prev\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc_prev\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df_prev\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl_prev\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app_prev\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc_prev\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df_prev\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl_prev\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app_prev\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc_prev\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df_prev\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl_prev\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app_prev\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc_prev\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df_prev\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl_prev\" \n" +
                    "FROM tms_trx5minhist \n" +
                    "where PERIOD = :periodPrev \n" +
                    "and BANK_ID = :bankId \n" +
                    "and hour_fr = :hourfr \n" +
                    "and hour_to = :hourto \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, hour_fr, hour_to \n" +

                    ") x \n" +

                    "GROUP BY x.\"bankid\",  x.\"trxid\", x.\"period\", x.\"hourfr\", x.\"hourto\", x.\"prevperiod\" " +
                    "";

        } else {
            sql = sql + "select x.\"bankid\", x.\"trxid\", x.\"period\", \n" +
                    "x.\"hourfr\", x.\"hourto\",  \n" +
                    "sum(x.\"freq_acq_app\") as \"freq_acq_app\", \n" +
                    "sum(x.\"freq_acq_dc\") as \"freq_acq_dc\" , \n" +
                    "sum(x.\"freq_acq_df\") as \"freq_acq_df\" , \n" +
                    "sum(x.\"freq_acq_rvsl\") as \"freq_acq_rvsl\", \n" +
                    "sum(x.\"freq_iss_app\") as \"freq_iss_app\", \n" +
                    "sum(x.\"freq_iss_dc\") as \"freq_iss_dc\" , \n" +
                    "sum(x.\"freq_iss_df\") as \"freq_iss_df\" , \n" +
                    "sum(x.\"freq_iss_rvsl\") as \"freq_iss_rvsl\", \n" +
                    "sum(x.\"freq_bnf_app\") as \"freq_bnf_app\", \n" +
                    "sum(x.\"freq_bnf_dc\") as  \"freq_bnf_dc\", \n" +
                    "sum(x.\"freq_bnf_df\") as \"freq_bnf_df\" , \n" +
                    "sum(x.\"freq_bnf_rvsl\") as \"freq_bnf_rvsl\", \n" +
                    "sum(x.\"freq_acqBnf_app\") as \"freq_acqBnf_app\", \n" +
                    "sum(x.\"freq_acqBnf_dc\") as \"freq_acqBnf_dc\", \n" +
                    "sum(x.\"freq_acqBnf_df\") as \"freq_acqBnf_df\", \n" +
                    "sum(x.\"freq_acqBnf_rvsl\") as \"freq_acqBnf_rvsl\", \n" +
                    "sum(x.\"freq_acqIss_app\") as \"freq_acqIss_app\", \n" +
                    "sum(x.\"freq_acqIss_dc\") as \"freq_acqIss_dc\", \n" +
                    "sum(x.\"freq_acqIss_df\") as \"freq_acqIss_df\", \n" +
                    "sum(x.\"freq_acqIss_rvsl\") as \"freq_acqIss_rvsl\", \n" +
                    "sum(x.\"freq_issBnf_app\") as \"freq_issBnf_app\", \n" +
                    "sum(x.\"freq_issBnf_dc\") as \"freq_issBnf_dc\", \n" +
                    "sum(x.\"freq_issBnf_df\") as \"freq_issBnf_df\", \n" +
                    "sum(x.\"freq_issBnf_rvsl\") as \"freq_issBnf_rvsl\", \n" +
                    "x.\"prevperiod\" as \"prevperiod\", \n" +
                    "sum(x.\"freq_acq_app_prev\") as \"freq_acq_app_prev\", \n" +
                    "sum(x.\"freq_acq_dc_prev\") as \"freq_acq_dc_prev\" , \n" +
                    "sum(x.\"freq_acq_df_prev\") as \"freq_acq_df_prev\" , \n" +
                    "sum(x.\"freq_acq_rvsl_prev\") as \"freq_acq_rvsl_prev\", \n" +
                    "sum(x.\"freq_iss_app_prev\") as \"freq_iss_app_prev\", \n" +
                    "sum(x.\"freq_iss_dc_prev\") as \"freq_iss_dc_prev\" , \n" +
                    "sum(x.\"freq_iss_df_prev\") as \"freq_iss_df_prev\" , \n" +
                    "sum(x.\"freq_iss_rvsl_prev\") as \"freq_iss_rvsl_prev\", \n" +
                    "sum(x.\"freq_bnf_app_prev\") as \"freq_bnf_app_prev\", \n" +
                    "sum(x.\"freq_bnf_dc_prev\") as  \"freq_bnf_dc_prev\", \n" +
                    "sum(x.\"freq_bnf_df_prev\") as \"freq_bnf_df_prev\" , \n" +
                    "sum(x.\"freq_bnf_rvsl_prev\") as \"freq_bnf_rvsl_prev\", \n" +
                    "sum(x.\"freq_acqBnf_app_prev\") as \"freq_acqBnf_app_prev\", \n" +
                    "sum(x.\"freq_acqBnf_dc_prev\") as \"freq_acqBnf_dc_prev\", \n" +
                    "sum(x.\"freq_acqBnf_df_prev\") as \"freq_acqBnf_df_prev\", \n" +
                    "sum(x.\"freq_acqBnf_rvsl_prev\") as \"freq_acqBnf_rvsl_prev\", \n" +
                    "sum(x.\"freq_acqIss_app_prev\") as \"freq_acqIss_app_prev\", \n" +
                    "sum(x.\"freq_acqIss_dc_prev\") as \"freq_acqIss_dc_prev\", \n" +
                    "sum(x.\"freq_acqIss_df_prev\") as \"freq_acqIss_df_prev\", \n" +
                    "sum(x.\"freq_acqIss_rvsl_prev\") as \"freq_acqIss_rvsl_prev\", \n" +
                    "sum(x.\"freq_issBnf_app_prev\") as \"freq_issBnf_app_prev\", \n" +
                    "sum(x.\"freq_issBnf_dc_prev\") as \"freq_issBnf_dc_prev\", \n" +
                    "sum(x.\"freq_issBnf_df_prev\") as \"freq_issBnf_df_prev\", \n" +
                    "sum(x.\"freq_issBnf_rvsl_prev\") as \"freq_issBnf_rvsl_prev\" \n" +
                    "from ( \n";

            sql = sql + " " +
                    "select BANK_ID as \"bankid\", trx_id as \"trxid\", period as \"period\", \n" +
                    "HOUR_FR as \"hourfr\", HOUR_TO as \"hourto\",  \n" +
                    "sum(freq_acq_app) as \"freq_acq_app\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl\", \n" +

                    "'" + periodPrev + "' as \"prevperiod\", \n" +
                    "0 as \"freq_acq_app_prev\", \n" +
                    "0 as \"freq_acq_dc_prev\" , \n" +
                    "0 as \"freq_acq_df_prev\" , \n" +
                    "0 as \"freq_acq_rvsl_prev\", \n" +
                    "0 as \"freq_iss_app_prev\", \n" +
                    "0 as \"freq_iss_dc_prev\" , \n" +
                    "0 as \"freq_iss_df_prev\" , \n" +
                    "0 as \"freq_iss_rvsl_prev\", \n" +
                    "0 as \"freq_bnf_app_prev\", \n" +
                    "0 as  \"freq_bnf_dc_prev\", \n" +
                    "0 as \"freq_bnf_df_prev\" , \n" +
                    "0 as \"freq_bnf_rvsl_prev\", \n" +
                    "0 as \"freq_acqBnf_app_prev\", \n" +
                    "0 as \"freq_acqBnf_dc_prev\", \n" +
                    "0 as \"freq_acqBnf_df_prev\", \n" +
                    "0 as \"freq_acqBnf_rvsl_prev\", \n" +
                    "0 as \"freq_acqIss_app_prev\", \n" +
                    "0 as \"freq_acqIss_dc_prev\", \n" +
                    "0 as \"freq_acqIss_df_prev\", \n" +
                    "0 as \"freq_acqIss_rvsl_prev\", \n" +
                    "0 as \"freq_issBnf_app_prev\", \n" +
                    "0 as \"freq_issBnf_dc_prev\", \n" +
                    "0 as \"freq_issBnf_df_prev\", \n" +
                    "0 as \"freq_issBnf_rvsl_prev\" \n" +

                    "FROM tms_trx5min\n" +
                    "where PERIOD = :period \n" +
                    "and BANK_ID = :bankId \n" +
                    "and hour_fr = :hourfr \n" +
                    "and hour_to = :hourto \n" +
                    "and TRX_ID = :trxId \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, hour_fr, hour_to \n" +

                    "union \n" +

                    "select BANK_ID as \"bankid\", trx_id as \"trxid\", '" + period + "' as \"period\", \n" +
                    "HOUR_FR as \"hourfr\", HOUR_TO as \"hourto\", \n" +

                    "0 as \"freq_acq_app\", \n" +
                    "0 as \"freq_acq_dc\" , \n" +
                    "0 as \"freq_acq_df\" , \n" +
                    "0 as \"freq_acq_rvsl\", \n" +
                    "0 as \"freq_iss_app\", \n" +
                    "0 as \"freq_iss_dc\" , \n" +
                    "0 as \"freq_iss_df\" , \n" +
                    "0 as \"freq_iss_rvsl\", \n" +
                    "0 as \"freq_bnf_app\", \n" +
                    "0 as  \"freq_bnf_dc\", \n" +
                    "0 as \"freq_bnf_df\" , \n" +
                    "0 as \"freq_bnf_rvsl\", \n" +
                    "0 as \"freq_acqBnf_app\", \n" +
                    "0 as \"freq_acqBnf_dc\", \n" +
                    "0 as \"freq_acqBnf_df\", \n" +
                    "0 as \"freq_acqBnf_rvsl\", \n" +
                    "0 as \"freq_acqIss_app\", \n" +
                    "0 as \"freq_acqIss_dc\", \n" +
                    "0 as \"freq_acqIss_df\", \n" +
                    "0 as \"freq_acqIss_rvsl\", \n" +
                    "0 as \"freq_issBnf_app\", \n" +
                    "0 as \"freq_issBnf_dc\", \n" +
                    "0 as \"freq_issBnf_df\", \n" +
                    "0 as \"freq_issBnf_rvsl\", \n" +

                    "'" + periodPrev + "' as \"prevperiod\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app_prev\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc_prev\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df_prev\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl_prev\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app_prev\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc_prev\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df_prev\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl_prev\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app_prev\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc_prev\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df_prev\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl_prev\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app_prev\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc_prev\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df_prev\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl_prev\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app_prev\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc_prev\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df_prev\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl_prev\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app_prev\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc_prev\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df_prev\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl_prev\" \n" +
                    "FROM tms_trx5minhist \n" +
                    "where PERIOD = :periodPrev \n" +
                    "and BANK_ID = :bankId \n" +
                    "and hour_fr = :hourfr \n" +
                    "and hour_to = :hourto \n" +
                    "and TRX_ID = :trxId \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, hour_fr, hour_to \n" +


                    ") x \n" +


                    "GROUP BY x.\"bankid\",  x.\"trxid\", x.\"period\", x.\"hourfr\", x.\"hourto\", x.\"prevperiod\" " +
                    "";
        }

        //log.info(sql);

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("period", period);
        query.setParameter("periodPrev", periodPrev);
        query.setParameter("bankId", bankId);
        if (!trxId.equalsIgnoreCase(DataConstant.ALL)) {
            query.setParameter("trxId", trxId);
        }
        query.setParameter("hourfr", hourfr);
        query.setParameter("hourto", hourto);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrx5MinBean.class));
        return (EsqTrx5MinBean)query.uniqueResult();
    }


    @Transactional(readOnly = true)
    public EsqTrx5MinBean getEsqTrxRekap(String acqId, String issId, String bnfId, String trxId, String period, String hourfr, String hourto) throws Exception {
        String sql = "";

        if (trxId.equalsIgnoreCase("ALL")) {
            sql = "SELECT a.period as \"period\", a.hour_fr as \"hourfr\", a.hour_to as \"hourto\", \n" +
                    "sum(a.freq_app) as \"freq_app\", sum(a.freq_dc) as \"freq_dc\", sum(a.freq_df) as \"freq_df\" \n" +
                    "from tms_TRXREKAP a \n" +
                    "where 1=1 \n" +
                    "and a.ACQ_ID = :acqId \n" +
                    "and a.ISS_ID = :issId \n" +
                    "and a.BNF_ID = :bnfId \n" +
                    "and a.PERIOD = :period \n " +
                    "and a.HOUR_FR = :hourfr \n " +
                    "and a.HOUR_TO = :hourto \n" +
                    "group by a.period, a.hour_fr, a.hour_to ";

        } else {
            sql = "SELECT a.period as \"period\", a.hour_fr as \"hourfr\", a.hour_to as \"hourto\", \n" +
                    "sum(a.freq_app) as \"freq_app\", sum(a.freq_dc) as \"freq_dc\", sum(a.freq_df) as \"freq_df\" \n" +
                    "from tms_TRXREKAP a \n" +
                    "where 1=1 \n" +
                    "and a.ACQ_ID = :acqId \n" +
                    "and a.ISS_ID = :issId \n" +
                    "and a.BNF_ID = :bnfId \n" +
                    "and a.TRX_ID = :trxId \n" +
                    "and a.PERIOD = :period \n " +
                    "and a.HOUR_FR = :hourfr \n " +
                    "and a.HOUR_TO = :hourto \n" +
                    "group by a.period, a.hour_fr, a.hour_to ";
        }

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("period", period);
        query.setParameter("acqId", acqId);
        query.setParameter("issId", issId);
        query.setParameter("bnfId", bnfId);
        if (!trxId.equalsIgnoreCase(DataConstant.ALL)) {
            query.setParameter("trxId", trxId);
        }
        query.setParameter("hourfr", hourfr);
        query.setParameter("hourto", hourto);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrx5MinBean.class));
        return (EsqTrx5MinBean)query.uniqueResult();
    }

    @Transactional(readOnly = true)
    public List<BankAlertThresholdBean> listBankAlertThreshold(String trxId, String trxName) {
        String sql = "" +
                "select a.bank_id as \"bankid\", a.bank_name as \"bankname\", \n" +
                ":transactionId as \"transactionid\", :transactionName as \"transactionname\", \n" +
                "b.threshold_m1 as \"thresholdm1\", \n" +
                "b.THRESHOLD_5MIN_WDWD as \"threshold5minwdwd\", \n" +
                "b.THRESHOLD_5MIN_WDHD as \"threshold5minwdhd\", \n" +
                "b.THRESHOLD_HOUR_WDWD as \"thresholdhourwdwd\", \n" +
                "b.THRESHOLD_HOUR_WDHD as \"thresholdhourwdhd\", \n" +
                "b.start_hour as \"starthour\", \n" +
                "b.start_minute as \"startminute\", \n" +
                "b.end_hour as \"endhour\", \n" +
                "b.end_minute as \"endminute\", \n" +
                "b.compare_method1 as \"comparemethod1int\", \n" +
                "b.compare_method2 as \"comparemethod2int\", \n" +
                "b.compare_method3 as \"comparemethod3int\", \n" +
                "b.compare_method4 as \"comparemethod4int\", \n" +
                "b.limit_interval_minute as \"limitintervalminute\", \n" +
                "b.active as \"active\" \n" +
                "from tms_bank a left join tms_bank_alert_threshold b on " +
                "(a.bank_id = b.bank_id and b.transaction_id = :transactionId) \n" +
                "where 1=1 \n" +
                "order by a.bank_name ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("transactionId", trxId);
        query.setParameter("transactionName", trxName);
        query.setResultTransformer(new AliasToBeanResultTransformer(BankAlertThresholdBean.class));
        return query.list();
    }

    @Transactional(readOnly = true)
    public List<BankAlertThresholdBean> listBankAlertThresholdSortBiggestTransaction(String trxId, String trxName) {
        Date date = new Date();
        date = DateUtils.addDate(Calendar.MONTH, date, -1);
        int year = DateUtils.getYear(date);
        int month = DateUtils.getMonth(date);
        String syear = Integer.toString(year);
        String smonth = StringUtils.formatFixLength(Integer.toString(month), "0", 2 );

        String sql = "" +
                "select a.bank_id as \"bankid\", a.bank_name as \"bankname\", \n" +
                ":transactionId as \"transactionid\", :transactionName as \"transactionname\", \n" +
                "b.threshold_m1 as \"thresholdm1\", \n" +
                "b.THRESHOLD_5MIN_WDWD as \"threshold5minwdwd\", \n" +
                "b.THRESHOLD_5MIN_WDHD as \"threshold5minwdhd\", \n" +
                "b.THRESHOLD_HOUR_WDWD as \"thresholdhourwdwd\", \n" +
                "b.THRESHOLD_HOUR_WDHD as \"thresholdhourwdhd\", \n" +
                "b.start_hour as \"starthour\", \n" +
                "b.start_minute as \"startminute\", \n" +
                "b.end_hour as \"endhour\", \n" +
                "b.end_minute as \"endminute\", \n" +
                "b.compare_method1 as \"comparemethod1int\", \n" +
                "b.compare_method2 as \"comparemethod2int\", \n" +
                "b.compare_method3 as \"comparemethod3int\", \n" +
                "b.compare_method4 as \"comparemethod4int\", \n" +
                "b.limit_interval_minute as \"limitintervalminute\", \n" +
                "b.active as \"active\"," +
                "coalesce(yy.\"total\", 0) as \"total\" \n" +
                "from tms_bank a left join tms_bank_alert_threshold b on " +
                "(a.bank_id = b.bank_id and b.transaction_id = :transactionId) \n" +

                "left join (" +
                "select x.\"bank_id\" as \"bank_id\", coalesce(sum(x.\"total\"), 0) as \"total\" from \n" +
                "( \n" +
                "select c.bank_id as \"bank_id\", coalesce(sum(c.iss_total_freq), 0) as \"total\" \n" +
                "from eis.EIS_ATMFreqSum c \n" +
                "where 1=1 \n" +
                "and c.year = :year \n" +
                "and c.month = :month \n" +
                "group by c.BANK_ID \n" +
                "\n" +
                "UNION\n" +
                "\n" +
                "select d.bank_id as \"bank_id\", coalesce(sum(d.iss_total_freq), 0) as \"total\" \n" +
                "from eis.EIS_POSFreqSum d \n" +
                "where 1=1 \n" +
                "and d.year = :year \n" +
                "and d.month = :month \n" +
                "group by d.BANK_ID \n" +
                ") x \n" +
                "group by x.\"bank_id\" \n" +
                "order by \"total\" desc \n" +
                ") yy on (yy.\"bank_id\" = a.bank_id) \n " +

                "where 1=1 \n" +
                "order by coalesce(yy.\"total\", 0) desc \n";

        log.info(sql);

        /*

        select x.bank_id, sum(x.total) as total from
(
select c.bank_id, sum(c.iss_total_freq) as total
from EIS_ATMFreqSum c
group by c.BANK_ID

UNION

select d.bank_id, sum(d.iss_total_freq) as total
from EIS_POSFreqSum d
group by d.BANK_ID
) x
group by x.bank_id
order by total desc

         */

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("transactionId", trxId);
        query.setParameter("transactionName", trxName);
        query.setParameter("year", syear);
        query.setParameter("month", smonth);
        query.setResultTransformer(new AliasToBeanResultTransformer(BankAlertThresholdBean.class));
        return query.list();
    }

    @Transactional
    public void deleteBankAlertThreshold(String trxId) {
        String sql = "" +
                "delete from tms_bank_alert_threshold where transaction_id = :trxId ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("trxId", trxId);
        query.executeUpdate();
    }




    /*
select *
from zz
where pe = '170516'
order by pe, hrfr, hrth, tmfr, tmth
and hrfr in ('00:00', '01:00')

select *
from zz
where pe = '170516'
and hrfr >= '00:00'
and hrth <= '01:00'
order by pe, hrfr, hrth, tmfr, tmth

select *
from zz
where pe = '170516'
and hrfr >= '00:00'
and hrth <= '01:00'
and tmfr >= '00:00'
and tmfr <= '00:40'
and tmth <= '00:45'
order by pe, hrfr, hrth, tmfr, tmth

and tmfr in ('00:05','00:10','00:15','00:20','00:25')
 */
    @Transactional(readOnly = true)
    public EsqTrx5MinBean getTotalEsqTrxHourly(String bankId, String trxId, String period, String hourfr, String hourto) {
        String sql = "" +
                "select BANK_ID as \"bankid\", trx_id as \"trxid\", period as \"period\", \n" +
                "sum(freq_acq_app) as \"freq_acq_app\", \n" +
                "sum(freq_acq_dc) as \"freq_acq_dc\" , \n" +
                "sum(freq_acq_df) as \"freq_acq_df\" , \n" +
                "sum(freq_acq_rvsl) as \"freq_acq_rvsl\", \n" +
                "sum(freq_iss_app) as \"freq_iss_app\", \n" +
                "sum(freq_iss_dc) as \"freq_iss_dc\" , \n" +
                "sum(freq_iss_df) as \"freq_iss_df\" , \n" +
                "sum(freq_iss_rvsl) as \"freq_iss_rvsl\", \n" +
                "sum(freq_bnf_app) as \"freq_bnf_app\", \n" +
                "sum(freq_bnf_dc) as  \"freq_bnf_dc\", \n" +
                "sum(freq_bnf_df) as \"freq_bnf_df\" , \n" +
                "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl\", \n" +
                "sum(freq_acqBnf_app) as \"freq_acqBnf_app\", \n" +
                "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc\", \n" +
                "sum(freq_acqBnf_df) as \"freq_acqBnf_df\", \n" +
                "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl\", \n" +
                "sum(freq_acqIss_app) as \"freq_acqIss_app\", \n" +
                "sum(freq_acqIss_dc) as \"freq_acqIss_dc\", \n" +
                "sum(freq_acqIss_df) as \"freq_acqIss_df\", \n" +
                "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl\", \n" +
                "sum(freq_issBnf_app) as \"freq_issBnf_app\", \n" +
                "sum(freq_issBnf_dc) as \"freq_issBnf_dc\", \n" +
                "sum(freq_issBnf_df) as \"freq_issBnf_df\", \n" +
                "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl\" \n" +
                "FROM tms_trx5min\n" +
                "where PERIOD = :period \n" +
                "and BANK_ID = :bankId \n" +
                "and TRX_ID = :trxId \n" +
                "and hour_fr >= :hourfr \n" +
                "and hour_to <= :hourto \n" +
                "group by BANK_ID, TRX_ID, PERIOD \n" +
                "";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("period", period);
        query.setParameter("bankId", bankId);
        query.setParameter("trxId", trxId);
        query.setParameter("hourfr", hourfr);
        query.setParameter("hourto", hourto);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrx5MinBean.class));
        return (EsqTrx5MinBean)query.uniqueResult();
    }

    @Transactional(readOnly = true)
    public EsqTrx5MinBean getTotalEsqTrxMinutely(String bankId, String trxId, String period, String hourfr, String hourto,
                                                 String timefrom, String timefrom2, String timeto) {
        String sql = "" +
                "select BANK_ID as \"bankid\", trx_id as \"trxid\", period as \"period\", \n" +
                "coalesce(sum(freq_acq_app), 0) as \"freq_acq_app\", \n" +
                "coalesce(sum(freq_acq_dc), 0) as \"freq_acq_dc\" , \n" +
                "coalesce(sum(freq_acq_df), 0) as \"freq_acq_df\" , \n" +
                "coalesce(sum(freq_acq_rvsl), 0) as \"freq_acq_rvsl\", \n" +
                "coalesce(sum(freq_iss_app), 0) as \"freq_iss_app\", \n" +
                "coalesce(sum(freq_iss_dc), 0) as \"freq_iss_dc\" , \n" +
                "coalesce(sum(freq_iss_df), 0) as \"freq_iss_df\" , \n" +
                "coalesce(sum(freq_iss_rvsl), 0) as \"freq_iss_rvsl\", \n" +
                "coalesce(sum(freq_bnf_app), 0) as \"freq_bnf_app\", \n" +
                "coalesce(sum(freq_bnf_dc), 0) as  \"freq_bnf_dc\", \n" +
                "coalesce(sum(freq_bnf_df), 0) as \"freq_bnf_df\" , \n" +
                "coalesce(sum(freq_bnf_rvsl), 0) as \"freq_bnf_rvsl\", \n" +
                "coalesce(sum(freq_acqBnf_app), 0) as \"freq_acqBnf_app\", \n" +
                "coalesce(sum(freq_acqBnf_dc), 0) as \"freq_acqBnf_dc\", \n" +
                "coalesce(sum(freq_acqBnf_df), 0) as \"freq_acqBnf_df\", \n" +
                "coalesce(sum(freq_acqBnf_rvsl), 0) as \"freq_acqBnf_rvsl\", \n" +
                "coalesce(sum(freq_acqIss_app), 0) as \"freq_acqIss_app\", \n" +
                "coalesce(sum(freq_acqIss_dc), 0) as \"freq_acqIss_dc\", \n" +
                "coalesce(sum(freq_acqIss_df), 0) as \"freq_acqIss_df\", \n" +
                "coalesce(sum(freq_acqIss_rvsl), 0) as \"freq_acqIss_rvsl\", \n" +
                "coalesce(sum(freq_issBnf_app), 0) as \"freq_issBnf_app\", \n" +
                "coalesce(sum(freq_issBnf_dc), 0) as \"freq_issBnf_dc\", \n" +
                "coalesce(sum(freq_issBnf_df), 0) as \"freq_issBnf_df\", \n" +
                "coalesce(sum(freq_issBnf_rvsl), 0) as \"freq_issBnf_rvsl\" \n" +
                "FROM tms_trx5min\n" +
                "where PERIOD = :period \n" +
                "and BANK_ID = :bankId \n" +
                "and TRX_ID = :trxId \n" +
                "and hour_fr >= :hourfr \n" +
                "and hour_to <= :hourto \n" +
                "and time_fr >= :timefrom \n" +
                "and time_fr <= :timefrom2 \n" +
                "and time_to <= :timeto \n" +
                "group by BANK_ID, TRX_ID, PERIOD \n" +
                "";

        //log.info(sql);

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("period", period);
        query.setParameter("bankId", bankId);
        query.setParameter("trxId", trxId);
        query.setParameter("hourfr", hourfr);
        query.setParameter("hourto", hourto);
        query.setParameter("timefrom", timefrom);
        query.setParameter("timefrom2", timefrom2);
        query.setParameter("timeto", timeto);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrx5MinBean.class));
        return (EsqTrx5MinBean)query.uniqueResult();
    }

    @Transactional(readOnly = true)
    public EsqTrx5MinBean getTotalEsqTrxMinutely(String bankId, String trxId, String period, String hourfr, String hourto,
                                                 String timefrom, String timeto) {
        String sql = "";

        if (trxId.equalsIgnoreCase(DataConstant.ALL)) {
            sql = "" +
                    "select BANK_ID as \"bankid\", 'ALL' as \"trxid\", period as \"period\", \n" +
                    "coalesce(sum(freq_acq_app), 0) as \"freq_acq_app\", \n" +
                    "coalesce(sum(freq_acq_dc), 0) as \"freq_acq_dc\" , \n" +
                    "coalesce(sum(freq_acq_df), 0) as \"freq_acq_df\" , \n" +
                    "coalesce(sum(freq_acq_rvsl), 0) as \"freq_acq_rvsl\", \n" +
                    "coalesce(sum(freq_iss_app), 0) as \"freq_iss_app\", \n" +
                    "coalesce(sum(freq_iss_dc), 0) as \"freq_iss_dc\" , \n" +
                    "coalesce(sum(freq_iss_df), 0) as \"freq_iss_df\" , \n" +
                    "coalesce(sum(freq_iss_rvsl), 0) as \"freq_iss_rvsl\", \n" +
                    "coalesce(sum(freq_bnf_app), 0) as \"freq_bnf_app\", \n" +
                    "coalesce(sum(freq_bnf_dc), 0) as  \"freq_bnf_dc\", \n" +
                    "coalesce(sum(freq_bnf_df), 0) as \"freq_bnf_df\" , \n" +
                    "coalesce(sum(freq_bnf_rvsl), 0) as \"freq_bnf_rvsl\", \n" +
                    "coalesce(sum(freq_acqBnf_app), 0) as \"freq_acqBnf_app\", \n" +
                    "coalesce(sum(freq_acqBnf_dc), 0) as \"freq_acqBnf_dc\", \n" +
                    "coalesce(sum(freq_acqBnf_df), 0) as \"freq_acqBnf_df\", \n" +
                    "coalesce(sum(freq_acqBnf_rvsl), 0) as \"freq_acqBnf_rvsl\", \n" +
                    "coalesce(sum(freq_acqIss_app), 0) as \"freq_acqIss_app\", \n" +
                    "coalesce(sum(freq_acqIss_dc), 0) as \"freq_acqIss_dc\", \n" +
                    "coalesce(sum(freq_acqIss_df), 0) as \"freq_acqIss_df\", \n" +
                    "coalesce(sum(freq_acqIss_rvsl), 0) as \"freq_acqIss_rvsl\", \n" +
                    "coalesce(sum(freq_issBnf_app), 0) as \"freq_issBnf_app\", \n" +
                    "coalesce(sum(freq_issBnf_dc), 0) as \"freq_issBnf_dc\", \n" +
                    "coalesce(sum(freq_issBnf_df), 0) as \"freq_issBnf_df\", \n" +
                    "coalesce(sum(freq_issBnf_rvsl), 0) as \"freq_issBnf_rvsl\" \n" +
                    "FROM tms_trx5min\n" +
                    "where PERIOD = :period \n" +
                    "and BANK_ID = :bankId \n" +
                    "and hour_fr = :hourfr \n" +
                    "and hour_to = :hourto \n" +
                    "and time_fr = :timefrom \n" +
                    "and time_to = :timeto \n" +
                    "group by BANK_ID, PERIOD \n" +
                    "";
        } else {
            sql = "" +
                    "select BANK_ID as \"bankid\", trx_id as \"trxid\", period as \"period\", \n" +
                    "coalesce(sum(freq_acq_app), 0) as \"freq_acq_app\", \n" +
                    "coalesce(sum(freq_acq_dc), 0) as \"freq_acq_dc\" , \n" +
                    "coalesce(sum(freq_acq_df), 0) as \"freq_acq_df\" , \n" +
                    "coalesce(sum(freq_acq_rvsl), 0) as \"freq_acq_rvsl\", \n" +
                    "coalesce(sum(freq_iss_app), 0) as \"freq_iss_app\", \n" +
                    "coalesce(sum(freq_iss_dc), 0) as \"freq_iss_dc\" , \n" +
                    "coalesce(sum(freq_iss_df), 0) as \"freq_iss_df\" , \n" +
                    "coalesce(sum(freq_iss_rvsl), 0) as \"freq_iss_rvsl\", \n" +
                    "coalesce(sum(freq_bnf_app), 0) as \"freq_bnf_app\", \n" +
                    "coalesce(sum(freq_bnf_dc), 0) as  \"freq_bnf_dc\", \n" +
                    "coalesce(sum(freq_bnf_df), 0) as \"freq_bnf_df\" , \n" +
                    "coalesce(sum(freq_bnf_rvsl), 0) as \"freq_bnf_rvsl\", \n" +
                    "coalesce(sum(freq_acqBnf_app), 0) as \"freq_acqBnf_app\", \n" +
                    "coalesce(sum(freq_acqBnf_dc), 0) as \"freq_acqBnf_dc\", \n" +
                    "coalesce(sum(freq_acqBnf_df), 0) as \"freq_acqBnf_df\", \n" +
                    "coalesce(sum(freq_acqBnf_rvsl), 0) as \"freq_acqBnf_rvsl\", \n" +
                    "coalesce(sum(freq_acqIss_app), 0) as \"freq_acqIss_app\", \n" +
                    "coalesce(sum(freq_acqIss_dc), 0) as \"freq_acqIss_dc\", \n" +
                    "coalesce(sum(freq_acqIss_df), 0) as \"freq_acqIss_df\", \n" +
                    "coalesce(sum(freq_acqIss_rvsl), 0) as \"freq_acqIss_rvsl\", \n" +
                    "coalesce(sum(freq_issBnf_app), 0) as \"freq_issBnf_app\", \n" +
                    "coalesce(sum(freq_issBnf_dc), 0) as \"freq_issBnf_dc\", \n" +
                    "coalesce(sum(freq_issBnf_df), 0) as \"freq_issBnf_df\", \n" +
                    "coalesce(sum(freq_issBnf_rvsl), 0) as \"freq_issBnf_rvsl\" \n" +
                    "FROM tms_trx5min\n" +
                    "where PERIOD = :period \n" +
                    "and BANK_ID = :bankId \n" +
                    "and TRX_ID = :trxId \n" +
                    "and hour_fr = :hourfr \n" +
                    "and hour_to = :hourto \n" +
                    "and time_fr = :timefrom \n" +
                    "and time_to = :timeto \n" +
                    "group by BANK_ID, TRX_ID, PERIOD \n" +
                    "";
        }

        //log.info(sql);

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("period", period);
        query.setParameter("bankId", bankId);
        if (!trxId.equalsIgnoreCase(DataConstant.ALL)) {
            query.setParameter("trxId", trxId);
        }
        query.setParameter("hourfr", hourfr);
        query.setParameter("hourto", hourto);
        query.setParameter("timefrom", timefrom);
        query.setParameter("timeto", timeto);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrx5MinBean.class));
        return (EsqTrx5MinBean)query.uniqueResult();
    }

    @Transactional(readOnly = true)
    public List<DistinctAcqIssBnfBean> distinctAcqIssBnfBean(String period) {
        String sql = "" +
                "select distinct a.acq_id as \"acqid\", a.iss_id as \"issid\", \n" +
                "a.bnf_id as \"bnfid\", a.trx_id as \"trxid\" \n" +
                "from tms_trxrekap a \n" +
                "where period = :period ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(DistinctAcqIssBnfBean.class));
        query.setParameter("period", period);
        return query.list();
    }

    /*@Transactional(readOnly = true)
    public List<EsqMasterAcqIssBnf> listEsqMasterAcqIssBnf() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(EsqMasterAcqIssBnf.class);
        return criteria.list();
    }*/

    @Transactional(readOnly = true)
    public List<AlertAnomaly> searchAlertAnomaly(Map<String, Object> param) throws Exception {
        String bankId = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_BANKID) );
        String trxId = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_TRANSACTIONID) );
        Date startDate = (Date)param.get(ParameterMapName.PARAM_STARTDATE);
        Date endDate = (Date)param.get(ParameterMapName.PARAM_ENDDATE);
        Integer statusId = (Integer)param.get(ParameterMapName.PARAM_STATUSID);

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertAnomaly.class);

        if (bankId.length() > 0) {
            criteria.add(Restrictions.eq("bankId", bankId));
        }

        if (trxId.length() > 0 && !trxId.toUpperCase().equals("ALL")) {
            criteria.add(Restrictions.eq("transactionId", trxId));
        }

        if (startDate != null) {
            criteria.add(Restrictions.ge("alertDate", startDate));
        }

        if (endDate != null) {
            criteria.add(Restrictions.le("alertDate", endDate));
        }

        if (statusId != null) {
            if (statusId >= 0) {
                criteria.add(Restrictions.eq("solveStatus", statusId));
            }
        }

        criteria.addOrder(Order.desc("alertDate"));

        return criteria.list();
    }

    /*@Transactional(readOnly = true)
    public List<AlertAnomalyHourly> searchAlertAnomalyHourly(Map<String, Object> param) throws Exception {
        String bankId = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_BANKID) );
        String trxId = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_TRANSACTIONID) );
        Date startDate = (Date)param.get(ParameterMapName.PARAM_STARTDATE);
        Date endDate = (Date)param.get(ParameterMapName.PARAM_ENDDATE);
        Integer statusId = (Integer)param.get(ParameterMapName.PARAM_STATUSID);

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertAnomalyHourly.class);

        if (bankId.length() > 0) {
            criteria.add(Restrictions.eq("bankId", bankId));
        }

        if (trxId.length() > 0) {
            criteria.add(Restrictions.eq("transactionId", trxId));
        }

        if (startDate != null) {
            criteria.add(Restrictions.ge("alertDate", startDate));
        }

        if (endDate != null) {
            criteria.add(Restrictions.le("alertDate", endDate));
        }

        if (statusId != null) {
            if (statusId >= 0) {
                criteria.add(Restrictions.eq("solveStatus", statusId));
            }
        }

        return criteria.list();
    }*/

    @Transactional(readOnly = true)
    public List<MktUserDashboard> listUserDashboard(String userId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MktUserDashboard.class);
        if (userId != null) {
            criteria.add(Restrictions.eq("userId", userId));
        }
        criteria.addOrder(Order.asc("menuName"));
        return criteria.list();
    }

    @Transactional(readOnly = true)
    public List<MktUserDashboardDetail> listUserDashboardDetail(BigDecimal id) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MktUserDashboardDetail.class);
        criteria.add(Restrictions.eq("mktUserDashboardDetailPk.id", id));
        criteria.addOrder(Order.asc("mktUserDashboardDetailPk.seq"));
        return criteria.list();
    }

    @Transactional(readOnly = true)
    public List<UserDashboardDetailBean> listUserDashboardDetailBean(BigDecimal id) throws Exception {
        /*String sql = "" +
                "select a.id as \"id\", a.seq as \"seq\", a.bank_id as \"bankid\", \n" +
                "a.transaction_id as \"transactionid\", a.transaction_indicator_id as \"transactionindicatorid\", \n" +
                "a.sortidx as \"sortidx\", a.monitoring_interval as \"monitoringinterval\", \n" +
                "b.bank_name as \"bankname\", c.TRX_NAME as \"transactionname\", d.TRX_NAME as \"transactionindicatorname\" \n" +
                "from tms_user_dashboard_detail a left join tms_TRXINDICATOR d on (a.TRANSACTION_INDICATOR_ID = d.TRX_ID) \n" +
                "left join tms_TRXID c on (a.TRANSACTION_ID = c.TRX_ID), \n" +
                "tms_bank b \n" +
                " \n" +
                "where a.bank_id = b.bank_id \n" +
                "and 1=1 \n" +
                "and 1=1 \n" +
                "and a.id = :id \n" +
                "order by a.sortidx ";*/

        //Modify by AAH 210222
        String sql = "" +
            "select a.id as \"id\", a.seq as \"seq\", a.bank_id as \"bankid\", \n" +
            "a.transaction_id as \"transactionid\", a.transaction_indicator_id as \"transactionindicatorid\", \n" +
            "a.sortidx as \"sortidx\", a.monitoring_interval as \"monitoringinterval\", \n" +
            "b.bank_name as \"bankname\", c.TRX_NAME as \"transactionname\", d.TRX_NAME as \"transactionindicatorname\" \n" +
            "from tms_user_dashboard_detail a left join tms_TRXINDICATOR d on (a.TRANSACTION_INDICATOR_ID = d.TRX_ID) \n" +
            "left join tms_TRXID c on (a.TRANSACTION_ID = c.TRX_ID), \n" +
            "V_TMS_BANK_CA_BILLER b \n" +
            " \n" +
            "where a.bank_id = b.bank_id \n" +
            "and 1=1 \n" +
            "and 1=1 \n" +
            "and a.id = :id \n" +
            "order by a.sortidx ";

        //log.info(sql);
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(UserDashboardDetailBean.class));
        query.setParameter("id", id);
        return query.list();
    }


    @Transactional(readOnly = true)
    public List<DistinctAcqIssBnfBean> listMasterAcqIssBnf() throws Exception {
        String sql = "" +
                "select acq_id as \"acqid\", iss_id as \"issid\", bnf_id as \"bnfid\" \n" +
                "from tms_master_acqissbnf ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(DistinctAcqIssBnfBean.class));
        return query.list();
    }


    @Transactional(readOnly = true)
    public List<AcqIssBnfTrxBean> checkAcqIssBnfTrx(String period, String hrfrom, String hrto) throws Exception {
        String sql = "" +
                "select a.ACQ_ID as \"acqid\", a.ISS_ID as \"issid\", a.BNF_ID as \"bnfid\", a.TRX_ID as \"trxid\"  \n" +
                "from tms_MASTER_ACQISSBNF a \n" +
                "left join ( \n" +
                "select distinct b.ACQ_ID, b.ISS_ID, b.BNF_ID, b.TRX_ID \n" +
                "from tms_TRXREKAP b \n" +
                "where b.PERIOD = :period \n" +
                "and b.HOUR_FR = :hrfrom \n" +
                "and b.HOUR_TO = :hrto \n" +
                ") b \n" +
                "on (a.ACQ_ID = b.ACQ_ID \n" +
                "and a.ISS_ID = b.ISS_ID \n" +
                "and a.BNF_ID = b.BNF_ID \n" +
                "and a.TRX_ID = b.TRX_ID) \n" +
                "where 1=1 \n" +
                "and b.ACQ_ID is NULL \n" +
                "and b.ISS_ID is NULL \n" +
                "and b.BNF_ID is NULL \n" +
                "and b.TRX_ID is NULL \n" +
                "\n ";

        //log.info(sql);

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(AcqIssBnfTrxBean.class));
        query.setParameter("period", period);
        query.setParameter("hrfrom", hrfrom);
        query.setParameter("hrto", hrto);
        return query.list();
    }


    @Transactional(readOnly=true)
    public List<DashboardRole> listDashboardRole(Integer roleId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(DashboardRole.class);
        criteria.add(Restrictions.eq("dashboardRolePk.roleTypeId", roleId));
        return criteria.list();
    }

    //190524 Added by AAH
    @Transactional(readOnly=true)
    public List<DashboardRoleBean> listDashboardRoleNew(Integer roleId) throws Exception {
            String sql = "select a.role_type_id as \"roletypeid\", a.dashboard_id as \"dashboardid\" \n" +
                    "from TMS_DASHBOARD_ROLE a join TMS_USER_DASHBOARD b on (b.id = a.DASHBOARD_ID) \n" +
                    "and a.ROLE_TYPE_ID = :roleId \n" +
                    "ORDER BY b.MENU_NAME ";

            SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
            query.setInteger("roleId", roleId);
            query.setResultTransformer(new AliasToBeanResultTransformer(DashboardRoleBean.class));
            return query.list();
    }

    @Transactional(readOnly=true)
    public List<DashboardRoleBean> listDashboardByRole(Integer roleId) throws Exception {
        String sql = "select a.id as \"dashboardid\", a.MENU_NAME as \"menuname\", a.DESCRIPTION as \"description\", \n" +
                "b.ROLE_TYPE_ID as \"roletypeid\" \n" +
                "from TMS_USER_DASHBOARD a left join TMS_DASHBOARD_ROLE b on (a.id = b.DASHBOARD_ID) \n" +
                "and b.ROLE_TYPE_ID = :roleId \n" +
                "ORDER BY a.MENU_NAME ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setInteger("roleId", roleId);
        query.setResultTransformer(new AliasToBeanResultTransformer(DashboardRoleBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List<DashboardRole> checkDashboardRole(Integer roleId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(DashboardRole.class);
        criteria.add(Restrictions.eq("dashboardRolePk.roleTypeId", roleId));
        return criteria.list();
    }


    @Transactional(readOnly = true)
    public List<AlertThresholdBean> listAlertThresholdHeader() throws Exception {
        String sql = "select distinct a.bank_id as \"bankid\", b.bank_name as \"bankname\", " +
                "a.transaction_id as \"transactionid\", \n" +
                "c.trx_name as \"transactionname\" \n" +
                "from TMS_ALERT_THRESHOLD a left join tms_bank b on (a.bank_id = b.bank_id) \n" +
                "left join tms_trxid c on (a.transaction_id = c.trx_id) \n" +
                "ORDER BY b.bank_name ";

        log.info(sql);

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(AlertThresholdBean.class));
        return query.list();
    }

    @Transactional(readOnly = true)
    public List<AlertThreshold> listAlertThresholdDetail(String bankId, String trxId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertThreshold.class);
        criteria.add(Restrictions.eq("alertThresholdPk.bankId", bankId));
        criteria.add(Restrictions.eq("alertThresholdPk.transactionId", trxId));
        return criteria.list();
    }

    @Transactional(readOnly = true)
    public List<AlertThreshold> listAlertThresholdDetail() throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertThreshold.class);
        return criteria.list();
    }



    @Transactional(readOnly = true)
    public EsqTrx5MinBean getEsqTrx(String tableName, String bankId, String trxId, String period, String timefr, String timeto) {
        String sql = "";

        if (trxId.equalsIgnoreCase(DataConstant.ALL)) {
            sql = "" +
                    "select BANK_ID as \"bankid\", 'ALL' as \"trxid\", period as \"period\", \n" +
                    "TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl\" \n" +
                    "FROM " + tableName + "\n" +
                    "where PERIOD = :period \n" +
                    "and BANK_ID = :bankId \n" +
                    "and time_fr = :timefr \n" +
                    "and time_to = :timeto \n" +
                    "group by BANK_ID, PERIOD, time_fr, time_to \n" +
                    "ORDER BY period, TIME_FR, TIME_TO " +
                    "";

        } else {
            sql = "" +
                    "select BANK_ID as \"bankid\", trx_id as \"trxid\", period as \"period\", \n" +
                    "TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl\" \n" +
                    "FROM " + tableName + "\n" +
                    "where PERIOD = :period \n" +
                    "and BANK_ID = :bankId \n" +
                    "and TRX_ID = :trxId \n" +
                    "and time_fr = :timefr \n" +
                    "and time_to = :timeto \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, time_fr, time_to \n" +
                    "ORDER BY period, time_fr, time_to " +
                    "";
        }

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("period", period);
        query.setParameter("bankId", bankId);
        if (!trxId.equalsIgnoreCase(DataConstant.ALL)) {
            query.setParameter("trxId", trxId);
        }
        query.setParameter("timefr", timefr);
        query.setParameter("timeto", timeto);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrx5MinBean.class));
        return (EsqTrx5MinBean)query.uniqueResult();
    }



    @Transactional(readOnly = true)
    public EsqTrx5MinBean getEsqTrxWithPrev(String tableName, String bankId, String trxId,
                                            String period, String periodPrev, String timefr, String timeto) {
        String sql = "";

        if (trxId.equalsIgnoreCase(DataConstant.ALL)) {
            sql = sql + "select x.\"bankid\", x.\"trxid\", x.\"period\", \n" +
                    "x.\"timefr\", x.\"timeto\", \n" +
                    "sum(x.\"freq_acq_app\") as \"freq_acq_app\", \n" +
                    "sum(x.\"freq_acq_dc\") as \"freq_acq_dc\" , \n" +
                    "sum(x.\"freq_acq_df\") as \"freq_acq_df\" , \n" +
                    "sum(x.\"freq_acq_rvsl\") as \"freq_acq_rvsl\", \n" +
                    "sum(x.\"freq_iss_app\") as \"freq_iss_app\", \n" +
                    "sum(x.\"freq_iss_dc\") as \"freq_iss_dc\" , \n" +
                    "sum(x.\"freq_iss_df\") as \"freq_iss_df\" , \n" +
                    "sum(x.\"freq_iss_rvsl\") as \"freq_iss_rvsl\", \n" +
                    "sum(x.\"freq_bnf_app\") as \"freq_bnf_app\", \n" +
                    "sum(x.\"freq_bnf_dc\") as  \"freq_bnf_dc\", \n" +
                    "sum(x.\"freq_bnf_df\") as \"freq_bnf_df\" , \n" +
                    "sum(x.\"freq_bnf_rvsl\") as \"freq_bnf_rvsl\", \n" +
                    "sum(x.\"freq_acqBnf_app\") as \"freq_acqBnf_app\", \n" +
                    "sum(x.\"freq_acqBnf_dc\") as \"freq_acqBnf_dc\", \n" +
                    "sum(x.\"freq_acqBnf_df\") as \"freq_acqBnf_df\", \n" +
                    "sum(x.\"freq_acqBnf_rvsl\") as \"freq_acqBnf_rvsl\", \n" +
                    "sum(x.\"freq_acqIss_app\") as \"freq_acqIss_app\", \n" +
                    "sum(x.\"freq_acqIss_dc\") as \"freq_acqIss_dc\", \n" +
                    "sum(x.\"freq_acqIss_df\") as \"freq_acqIss_df\", \n" +
                    "sum(x.\"freq_acqIss_rvsl\") as \"freq_acqIss_rvsl\", \n" +
                    "sum(x.\"freq_issBnf_app\") as \"freq_issBnf_app\", \n" +
                    "sum(x.\"freq_issBnf_dc\") as \"freq_issBnf_dc\", \n" +
                    "sum(x.\"freq_issBnf_df\") as \"freq_issBnf_df\", \n" +
                    "sum(x.\"freq_issBnf_rvsl\") as \"freq_issBnf_rvsl\", \n" +
                    "x.\"prevperiod\" as \"prevperiod\", \n" +
                    "sum(x.\"freq_acq_app_prev\") as \"freq_acq_app_prev\", \n" +
                    "sum(x.\"freq_acq_dc_prev\") as \"freq_acq_dc_prev\" , \n" +
                    "sum(x.\"freq_acq_df_prev\") as \"freq_acq_df_prev\" , \n" +
                    "sum(x.\"freq_acq_rvsl_prev\") as \"freq_acq_rvsl_prev\", \n" +
                    "sum(x.\"freq_iss_app_prev\") as \"freq_iss_app_prev\", \n" +
                    "sum(x.\"freq_iss_dc_prev\") as \"freq_iss_dc_prev\" , \n" +
                    "sum(x.\"freq_iss_df_prev\") as \"freq_iss_df_prev\" , \n" +
                    "sum(x.\"freq_iss_rvsl_prev\") as \"freq_iss_rvsl_prev\", \n" +
                    "sum(x.\"freq_bnf_app_prev\") as \"freq_bnf_app_prev\", \n" +
                    "sum(x.\"freq_bnf_dc_prev\") as  \"freq_bnf_dc_prev\", \n" +
                    "sum(x.\"freq_bnf_df_prev\") as \"freq_bnf_df_prev\" , \n" +
                    "sum(x.\"freq_bnf_rvsl_prev\") as \"freq_bnf_rvsl_prev\", \n" +
                    "sum(x.\"freq_acqBnf_app_prev\") as \"freq_acqBnf_app_prev\", \n" +
                    "sum(x.\"freq_acqBnf_dc_prev\") as \"freq_acqBnf_dc_prev\", \n" +
                    "sum(x.\"freq_acqBnf_df_prev\") as \"freq_acqBnf_df_prev\", \n" +
                    "sum(x.\"freq_acqBnf_rvsl_prev\") as \"freq_acqBnf_rvsl_prev\", \n" +
                    "sum(x.\"freq_acqIss_app_prev\") as \"freq_acqIss_app_prev\", \n" +
                    "sum(x.\"freq_acqIss_dc_prev\") as \"freq_acqIss_dc_prev\", \n" +
                    "sum(x.\"freq_acqIss_df_prev\") as \"freq_acqIss_df_prev\", \n" +
                    "sum(x.\"freq_acqIss_rvsl_prev\") as \"freq_acqIss_rvsl_prev\", \n" +
                    "sum(x.\"freq_issBnf_app_prev\") as \"freq_issBnf_app_prev\", \n" +
                    "sum(x.\"freq_issBnf_dc_prev\") as \"freq_issBnf_dc_prev\", \n" +
                    "sum(x.\"freq_issBnf_df_prev\") as \"freq_issBnf_df_prev\", \n" +
                    "sum(x.\"freq_issBnf_rvsl_prev\") as \"freq_issBnf_rvsl_prev\" \n" +
                    "from ( \n";

            sql = sql + " " +
                    "select BANK_ID as \"bankid\", 'ALL' as \"trxid\", period as \"period\", \n" +
                    "TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl\", \n" +

                    "'" + periodPrev + "' as \"prevperiod\", \n" +
                    "0 as \"freq_acq_app_prev\", \n" +
                    "0 as \"freq_acq_dc_prev\" , \n" +
                    "0 as \"freq_acq_df_prev\" , \n" +
                    "0 as \"freq_acq_rvsl_prev\", \n" +
                    "0 as \"freq_iss_app_prev\", \n" +
                    "0 as \"freq_iss_dc_prev\" , \n" +
                    "0 as \"freq_iss_df_prev\" , \n" +
                    "0 as \"freq_iss_rvsl_prev\", \n" +
                    "0 as \"freq_bnf_app_prev\", \n" +
                    "0 as  \"freq_bnf_dc_prev\", \n" +
                    "0 as \"freq_bnf_df_prev\" , \n" +
                    "0 as \"freq_bnf_rvsl_prev\", \n" +
                    "0 as \"freq_acqBnf_app_prev\", \n" +
                    "0 as \"freq_acqBnf_dc_prev\", \n" +
                    "0 as \"freq_acqBnf_df_prev\", \n" +
                    "0 as \"freq_acqBnf_rvsl_prev\", \n" +
                    "0 as \"freq_acqIss_app_prev\", \n" +
                    "0 as \"freq_acqIss_dc_prev\", \n" +
                    "0 as \"freq_acqIss_df_prev\", \n" +
                    "0 as \"freq_acqIss_rvsl_prev\", \n" +
                    "0 as \"freq_issBnf_app_prev\", \n" +
                    "0 as \"freq_issBnf_dc_prev\", \n" +
                    "0 as \"freq_issBnf_df_prev\", \n" +
                    "0 as \"freq_issBnf_rvsl_prev\" \n" +

                    "FROM " + tableName + " \n" +
                    "where PERIOD = :period \n" +
                    "and BANK_ID = :bankId \n" +
                    "and time_fr = :timefr \n" +
                    "and time_to = :timeto \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, time_fr, time_to \n" +

                    "union \n" +

                    "select BANK_ID as \"bankid\", 'ALL' as \"trxid\", '" + period + "' as \"period\", \n" +
                    "TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +

                    "0 as \"freq_acq_app\", \n" +
                    "0 as \"freq_acq_dc\" , \n" +
                    "0 as \"freq_acq_df\" , \n" +
                    "0 as \"freq_acq_rvsl\", \n" +
                    "0 as \"freq_iss_app\", \n" +
                    "0 as \"freq_iss_dc\" , \n" +
                    "0 as \"freq_iss_df\" , \n" +
                    "0 as \"freq_iss_rvsl\", \n" +
                    "0 as \"freq_bnf_app\", \n" +
                    "0 as  \"freq_bnf_dc\", \n" +
                    "0 as \"freq_bnf_df\" , \n" +
                    "0 as \"freq_bnf_rvsl\", \n" +
                    "0 as \"freq_acqBnf_app\", \n" +
                    "0 as \"freq_acqBnf_dc\", \n" +
                    "0 as \"freq_acqBnf_df\", \n" +
                    "0 as \"freq_acqBnf_rvsl\", \n" +
                    "0 as \"freq_acqIss_app\", \n" +
                    "0 as \"freq_acqIss_dc\", \n" +
                    "0 as \"freq_acqIss_df\", \n" +
                    "0 as \"freq_acqIss_rvsl\", \n" +
                    "0 as \"freq_issBnf_app\", \n" +
                    "0 as \"freq_issBnf_dc\", \n" +
                    "0 as \"freq_issBnf_df\", \n" +
                    "0 as \"freq_issBnf_rvsl\", \n" +

                    "'" + periodPrev + "' as \"prevperiod\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app_prev\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc_prev\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df_prev\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl_prev\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app_prev\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc_prev\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df_prev\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl_prev\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app_prev\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc_prev\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df_prev\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl_prev\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app_prev\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc_prev\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df_prev\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl_prev\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app_prev\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc_prev\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df_prev\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl_prev\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app_prev\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc_prev\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df_prev\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl_prev\" \n" +
                    "FROM " + tableName + " \n" +
                    "where PERIOD = :periodPrev \n" +
                    "and BANK_ID = :bankId \n" +
                    "and time_fr = :timefr \n" +
                    "and time_to = :timeto \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, time_fr, time_to \n" +

                    ") x \n" +

                    "GROUP BY x.\"bankid\",  x.\"trxid\", x.\"period\", x.\"timefr\", x.\"timeto\", x.\"prevperiod\" " +
                    "";

        } else {
            sql = sql + "select x.\"bankid\", x.\"trxid\", x.\"period\", \n" +
                    "x.\"timefr\", x.\"timeto\", \n" +
                    "sum(x.\"freq_acq_app\") as \"freq_acq_app\", \n" +
                    "sum(x.\"freq_acq_dc\") as \"freq_acq_dc\" , \n" +
                    "sum(x.\"freq_acq_df\") as \"freq_acq_df\" , \n" +
                    "sum(x.\"freq_acq_rvsl\") as \"freq_acq_rvsl\", \n" +
                    "sum(x.\"freq_iss_app\") as \"freq_iss_app\", \n" +
                    "sum(x.\"freq_iss_dc\") as \"freq_iss_dc\" , \n" +
                    "sum(x.\"freq_iss_df\") as \"freq_iss_df\" , \n" +
                    "sum(x.\"freq_iss_rvsl\") as \"freq_iss_rvsl\", \n" +
                    "sum(x.\"freq_bnf_app\") as \"freq_bnf_app\", \n" +
                    "sum(x.\"freq_bnf_dc\") as  \"freq_bnf_dc\", \n" +
                    "sum(x.\"freq_bnf_df\") as \"freq_bnf_df\" , \n" +
                    "sum(x.\"freq_bnf_rvsl\") as \"freq_bnf_rvsl\", \n" +
                    "sum(x.\"freq_acqBnf_app\") as \"freq_acqBnf_app\", \n" +
                    "sum(x.\"freq_acqBnf_dc\") as \"freq_acqBnf_dc\", \n" +
                    "sum(x.\"freq_acqBnf_df\") as \"freq_acqBnf_df\", \n" +
                    "sum(x.\"freq_acqBnf_rvsl\") as \"freq_acqBnf_rvsl\", \n" +
                    "sum(x.\"freq_acqIss_app\") as \"freq_acqIss_app\", \n" +
                    "sum(x.\"freq_acqIss_dc\") as \"freq_acqIss_dc\", \n" +
                    "sum(x.\"freq_acqIss_df\") as \"freq_acqIss_df\", \n" +
                    "sum(x.\"freq_acqIss_rvsl\") as \"freq_acqIss_rvsl\", \n" +
                    "sum(x.\"freq_issBnf_app\") as \"freq_issBnf_app\", \n" +
                    "sum(x.\"freq_issBnf_dc\") as \"freq_issBnf_dc\", \n" +
                    "sum(x.\"freq_issBnf_df\") as \"freq_issBnf_df\", \n" +
                    "sum(x.\"freq_issBnf_rvsl\") as \"freq_issBnf_rvsl\", \n" +
                    "x.\"prevperiod\" as \"prevperiod\", \n" +
                    "sum(x.\"freq_acq_app_prev\") as \"freq_acq_app_prev\", \n" +
                    "sum(x.\"freq_acq_dc_prev\") as \"freq_acq_dc_prev\" , \n" +
                    "sum(x.\"freq_acq_df_prev\") as \"freq_acq_df_prev\" , \n" +
                    "sum(x.\"freq_acq_rvsl_prev\") as \"freq_acq_rvsl_prev\", \n" +
                    "sum(x.\"freq_iss_app_prev\") as \"freq_iss_app_prev\", \n" +
                    "sum(x.\"freq_iss_dc_prev\") as \"freq_iss_dc_prev\" , \n" +
                    "sum(x.\"freq_iss_df_prev\") as \"freq_iss_df_prev\" , \n" +
                    "sum(x.\"freq_iss_rvsl_prev\") as \"freq_iss_rvsl_prev\", \n" +
                    "sum(x.\"freq_bnf_app_prev\") as \"freq_bnf_app_prev\", \n" +
                    "sum(x.\"freq_bnf_dc_prev\") as  \"freq_bnf_dc_prev\", \n" +
                    "sum(x.\"freq_bnf_df_prev\") as \"freq_bnf_df_prev\" , \n" +
                    "sum(x.\"freq_bnf_rvsl_prev\") as \"freq_bnf_rvsl_prev\", \n" +
                    "sum(x.\"freq_acqBnf_app_prev\") as \"freq_acqBnf_app_prev\", \n" +
                    "sum(x.\"freq_acqBnf_dc_prev\") as \"freq_acqBnf_dc_prev\", \n" +
                    "sum(x.\"freq_acqBnf_df_prev\") as \"freq_acqBnf_df_prev\", \n" +
                    "sum(x.\"freq_acqBnf_rvsl_prev\") as \"freq_acqBnf_rvsl_prev\", \n" +
                    "sum(x.\"freq_acqIss_app_prev\") as \"freq_acqIss_app_prev\", \n" +
                    "sum(x.\"freq_acqIss_dc_prev\") as \"freq_acqIss_dc_prev\", \n" +
                    "sum(x.\"freq_acqIss_df_prev\") as \"freq_acqIss_df_prev\", \n" +
                    "sum(x.\"freq_acqIss_rvsl_prev\") as \"freq_acqIss_rvsl_prev\", \n" +
                    "sum(x.\"freq_issBnf_app_prev\") as \"freq_issBnf_app_prev\", \n" +
                    "sum(x.\"freq_issBnf_dc_prev\") as \"freq_issBnf_dc_prev\", \n" +
                    "sum(x.\"freq_issBnf_df_prev\") as \"freq_issBnf_df_prev\", \n" +
                    "sum(x.\"freq_issBnf_rvsl_prev\") as \"freq_issBnf_rvsl_prev\" \n" +
                    "from ( \n";

            sql = sql + " " +
                    "select BANK_ID as \"bankid\", trx_id as \"trxid\", period as \"period\", \n" +
                    "TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl\", \n" +

                    "'" + periodPrev + "' as \"prevperiod\", \n" +
                    "0 as \"freq_acq_app_prev\", \n" +
                    "0 as \"freq_acq_dc_prev\" , \n" +
                    "0 as \"freq_acq_df_prev\" , \n" +
                    "0 as \"freq_acq_rvsl_prev\", \n" +
                    "0 as \"freq_iss_app_prev\", \n" +
                    "0 as \"freq_iss_dc_prev\" , \n" +
                    "0 as \"freq_iss_df_prev\" , \n" +
                    "0 as \"freq_iss_rvsl_prev\", \n" +
                    "0 as \"freq_bnf_app_prev\", \n" +
                    "0 as  \"freq_bnf_dc_prev\", \n" +
                    "0 as \"freq_bnf_df_prev\" , \n" +
                    "0 as \"freq_bnf_rvsl_prev\", \n" +
                    "0 as \"freq_acqBnf_app_prev\", \n" +
                    "0 as \"freq_acqBnf_dc_prev\", \n" +
                    "0 as \"freq_acqBnf_df_prev\", \n" +
                    "0 as \"freq_acqBnf_rvsl_prev\", \n" +
                    "0 as \"freq_acqIss_app_prev\", \n" +
                    "0 as \"freq_acqIss_dc_prev\", \n" +
                    "0 as \"freq_acqIss_df_prev\", \n" +
                    "0 as \"freq_acqIss_rvsl_prev\", \n" +
                    "0 as \"freq_issBnf_app_prev\", \n" +
                    "0 as \"freq_issBnf_dc_prev\", \n" +
                    "0 as \"freq_issBnf_df_prev\", \n" +
                    "0 as \"freq_issBnf_rvsl_prev\" \n" +

                    "FROM " + tableName + " \n" +
                    "where PERIOD = :period \n" +
                    "and BANK_ID = :bankId \n" +
                    "and time_fr = :timefr \n" +
                    "and time_to = :timeto \n" +
                    "and TRX_ID = :trxId \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, time_fr, time_to \n" +

                    "union \n" +

                    "select BANK_ID as \"bankid\", trx_id as \"trxid\", '" + period + "' as \"period\", \n" +
                    "TIME_FR as \"timefr\", TIME_TO as \"timeto\", \n" +

                    "0 as \"freq_acq_app\", \n" +
                    "0 as \"freq_acq_dc\" , \n" +
                    "0 as \"freq_acq_df\" , \n" +
                    "0 as \"freq_acq_rvsl\", \n" +
                    "0 as \"freq_iss_app\", \n" +
                    "0 as \"freq_iss_dc\" , \n" +
                    "0 as \"freq_iss_df\" , \n" +
                    "0 as \"freq_iss_rvsl\", \n" +
                    "0 as \"freq_bnf_app\", \n" +
                    "0 as  \"freq_bnf_dc\", \n" +
                    "0 as \"freq_bnf_df\" , \n" +
                    "0 as \"freq_bnf_rvsl\", \n" +
                    "0 as \"freq_acqBnf_app\", \n" +
                    "0 as \"freq_acqBnf_dc\", \n" +
                    "0 as \"freq_acqBnf_df\", \n" +
                    "0 as \"freq_acqBnf_rvsl\", \n" +
                    "0 as \"freq_acqIss_app\", \n" +
                    "0 as \"freq_acqIss_dc\", \n" +
                    "0 as \"freq_acqIss_df\", \n" +
                    "0 as \"freq_acqIss_rvsl\", \n" +
                    "0 as \"freq_issBnf_app\", \n" +
                    "0 as \"freq_issBnf_dc\", \n" +
                    "0 as \"freq_issBnf_df\", \n" +
                    "0 as \"freq_issBnf_rvsl\", \n" +

                    "'" + periodPrev + "' as \"prevperiod\", \n" +
                    "sum(freq_acq_app) as \"freq_acq_app_prev\", \n" +
                    "sum(freq_acq_dc) as \"freq_acq_dc_prev\" , \n" +
                    "sum(freq_acq_df) as \"freq_acq_df_prev\" , \n" +
                    "sum(freq_acq_rvsl) as \"freq_acq_rvsl_prev\", \n" +
                    "sum(freq_iss_app) as \"freq_iss_app_prev\", \n" +
                    "sum(freq_iss_dc) as \"freq_iss_dc_prev\" , \n" +
                    "sum(freq_iss_df) as \"freq_iss_df_prev\" , \n" +
                    "sum(freq_iss_rvsl) as \"freq_iss_rvsl_prev\", \n" +
                    "sum(freq_bnf_app) as \"freq_bnf_app_prev\", \n" +
                    "sum(freq_bnf_dc) as  \"freq_bnf_dc_prev\", \n" +
                    "sum(freq_bnf_df) as \"freq_bnf_df_prev\" , \n" +
                    "sum(freq_bnf_rvsl) as \"freq_bnf_rvsl_prev\", \n" +
                    "sum(freq_acqBnf_app) as \"freq_acqBnf_app_prev\", \n" +
                    "sum(freq_acqBnf_dc) as \"freq_acqBnf_dc_prev\", \n" +
                    "sum(freq_acqBnf_df) as \"freq_acqBnf_df_prev\", \n" +
                    "sum(freq_acqBnf_rvsl) as \"freq_acqBnf_rvsl_prev\", \n" +
                    "sum(freq_acqIss_app) as \"freq_acqIss_app_prev\", \n" +
                    "sum(freq_acqIss_dc) as \"freq_acqIss_dc_prev\", \n" +
                    "sum(freq_acqIss_df) as \"freq_acqIss_df_prev\", \n" +
                    "sum(freq_acqIss_rvsl) as \"freq_acqIss_rvsl_prev\", \n" +
                    "sum(freq_issBnf_app) as \"freq_issBnf_app_prev\", \n" +
                    "sum(freq_issBnf_dc) as \"freq_issBnf_dc_prev\", \n" +
                    "sum(freq_issBnf_df) as \"freq_issBnf_df_prev\", \n" +
                    "sum(freq_issBnf_rvsl) as \"freq_issBnf_rvsl_prev\" \n" +
                    "FROM " + tableName + " \n" +
                    "where PERIOD = :periodPrev \n" +
                    "and BANK_ID = :bankId \n" +
                    "and time_fr = :timefr \n" +
                    "and time_to = :timeto \n" +
                    "and TRX_ID = :trxId \n" +
                    "group by BANK_ID, TRX_ID, PERIOD, time_fr, time_to \n" +

                    ") x \n" +

                    "GROUP BY x.\"bankid\",  x.\"trxid\", x.\"period\", x.\"timefr\", x.\"timeto\", x.\"prevperiod\" " +
                    "";
        }



        //log.info(sql);

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("period", period);
        query.setParameter("periodPrev", periodPrev);
        query.setParameter("bankId", bankId);
        if (!trxId.equalsIgnoreCase(DataConstant.ALL)) {
            query.setParameter("trxId", trxId);
        }

        query.setParameter("timefr", timefr);
        query.setParameter("timeto", timeto);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrx5MinBean.class));
        return (EsqTrx5MinBean)query.uniqueResult();
    }


}

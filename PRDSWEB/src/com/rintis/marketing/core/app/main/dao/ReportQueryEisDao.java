package com.rintis.marketing.core.app.main.dao;

import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.constant.ParameterMapName;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.StringUtils;
import jodd.madvoc.meta.In;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_REPORT_QUERY_EIS_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_REPORT_QUERY_EIS_DAO)
public class ReportQueryEisDao extends AbstractDao {

    /*@Transactional
    public void saveSqlLog(UserInfoBean userInfoBean, String sql) throws Exception {
        if (userInfoBean.checkRoleAdmin()) {
            MktSqlLog mktSqlLog = new MktSqlLog();
            mktSqlLog.setSqlDate(new Date());
            mktSqlLog.setUserId(userInfoBean.getUserId());
            mktSqlLog.setSqlLog(sql);
            save(mktSqlLog);
        }
    }*/




}

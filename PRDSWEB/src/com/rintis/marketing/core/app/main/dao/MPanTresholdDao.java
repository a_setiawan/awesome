package com.rintis.marketing.core.app.main.dao;

import com.rintis.marketing.beans.bean.bi.WhitelistQRBean;
import com.rintis.marketing.core.SpringBeanConstant;
import org.hibernate.SQLQuery;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.List;

/*Created by aaw 28/06/2021*/

@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_MPAN_TRESHOLD_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_MPAN_TRESHOLD_DAO)
public class MPanTresholdDao extends AbstractDao {

    @Transactional(readOnly=true)
    public List listMPanTreshold(){
        String sql = "select pan as \"pan\", acquirer_id as \"acquirerId\",  nvl(tms_bankfiid_api.get_bank_name(acquirer_id), acquirer_id) as \"acquirerBank\", freq_app as \"freqApp\", freq_dc as \"freqDc\", freq_dc_free as \"freqDcFree\", freq_rvsl as \"freqRvsl\", total_amt_app as \"totalAmtApp\", total_amt_dc as \"totalAmtDc\", total_amt_dc_free as \"totalAmtDcFree\", total_amt_rvsl as \"totalAmtRvsl\", total_amt as \"totalAmt\", active as \"active\" from TMS_MPAN_QR_DUMMY order by pan";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(WhitelistQRBean.class));
        return query.list();
    }

    @Transactional
    public void deleteMPanTreshold(String pan, String acquirerId) {
        String sql = "delete from TMS_MPAN_QR_DUMMY where trim(pan) = :pan and UPPER(acquirer_id) = :acquirerId";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan.trim());
        query.setString("acquirerId", acquirerId.toUpperCase());
        query.executeUpdate();
    }

    @Transactional(readOnly=true)
    public List listMpanTreshold(String pan, String acquirerId){
        String sql = "select pan as \"pan\", acquirer_id as \"acquirerId\", freq_app as \"freqApp\", freq_dc as \"freqDc\", freq_dc_free as \"freqDcFree\", freq_rvsl as \"freqRvsl\", total_amt_app as \"totalAmtApp\"," +
                "total_amt_dc as \"totalAmtDc\", total_amt_dc_free as \"totalAmtDcFree\", total_amt_rvsl as \"totalAmtRvsl\", total_amt as \"totalAmt\", active as \"active\" from\n" +
                "TMS_MPAN_QR_DUMMY where trim(pan) = :pan and upper(acquirer_id) = :acquirerId";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan.trim());
        query.setString("acquirerId", acquirerId.toUpperCase());
        return query.list();
    }

    @Transactional
    public void saveMPanTreshold(WhitelistQRBean whitelistQRBean) {
        String sql = "insert into TMS_MPAN_QR_DUMMY values (:pan, :acquirerId, :freqApp, :freqDc, :freqDcFree, :freqRvsl, :totalAmtApp, :totalAmtDc, :totalAmtDcFree, :totalAmtRvsl, :totalAmt, :active)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", whitelistQRBean.getPan().trim());
        query.setString("acquirerId", whitelistQRBean.getAcquirerId());
        query.setBigDecimal("freqApp", whitelistQRBean.getFreqApp());
        query.setBigDecimal("freqDc", whitelistQRBean.getFreqDc());
        query.setBigDecimal("freqDcFree", whitelistQRBean.getFreqDcFree());
        query.setBigDecimal("freqRvsl", whitelistQRBean.getFreqRvsl());
        query.setBigDecimal("totalAmtApp", whitelistQRBean.getTotalAmtApp());
        query.setBigDecimal("totalAmtDc", whitelistQRBean.getTotalAmtDc());
        query.setBigDecimal("totalAmtDcFree", whitelistQRBean.getTotalAmtDcFree());
        query.setBigDecimal("totalAmtRvsl", whitelistQRBean.getTotalAmtRvsl());
        query.setBigDecimal("totalAmt", whitelistQRBean.getTotalAmt());
        query.setString("active", whitelistQRBean.getActive());
        query.executeUpdate();
    }

    @Transactional(readOnly=true)
    public List getWhitelistMPan(String pan, String acquirerId){
        String sql = "select pan as \"pan\", acquirer_id as \"acquirerId\",  nvl(tms_bankfiid_api.get_bank_name(acquirer_id), acquirer_id) as \"acquirerBank\", freq_app as \"freqApp\", freq_dc as \"freqDc\", freq_dc_free as \"freqDcFree\", freq_rvsl as \"freqRvsl\", total_amt_app as \"totalAmtApp\", total_amt_dc as \"totalAmtDc\", total_amt_dc_free as \"totalAmtDcFree\", total_amt_rvsl as \"totalAmtRvsl\", total_amt as \"totalAmt\", active as \"active\" from TMS_MPAN_QR_DUMMY where trim(pan) = :pan and upper(acquirer_id) = upper(:acquirerId)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan.trim());
        query.setString("acquirerId", acquirerId);
        query.setResultTransformer(new AliasToBeanResultTransformer(WhitelistQRBean.class));
        return query.list();
    }

    @Transactional
    public void updateMPan(String pan, String acquirerId, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree, BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) {
        String sql = "update TMS_MPAN_QR_DUMMY set freq_app = :freqApp, freq_dc = :freqDc, freq_dc_free = :freqDcFree, freq_rvsl = :freqRvsl, total_amt_app = :totalAmtApp, total_amt_dc = :totalAmtDc, total_amt_dc_free = :totalAmtDcFree, total_amt_rvsl = :totalAmtRvsl, total_amt = :totalAmt, active = :active where trim(pan) = :pan and UPPER(acquirer_id) = upper(:acquirerId)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan.trim());
        query.setString("acquirerId", acquirerId);
        query.setBigDecimal("freqApp", freqApp);
        query.setBigDecimal("freqDc", freqDc);
        query.setBigDecimal("freqDcFree", freqDcFree);
        query.setBigDecimal("freqRvsl", freqRvsl);
        query.setBigDecimal("totalAmtApp", totalAmtApp);
        query.setBigDecimal("totalAmtDc", totalAmtDc);
        query.setBigDecimal("totalAmtDcFree", totalAmtDcFree);
        query.setBigDecimal("totalAmtRvsl", totalAmtRvsl);
        query.setBigDecimal("totalAmt", totalAmt);
        query.setString("active", active);
        query.executeUpdate();
    }
}
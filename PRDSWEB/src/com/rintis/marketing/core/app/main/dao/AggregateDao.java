package com.rintis.marketing.core.app.main.dao;

import com.rintis.marketing.core.SpringBeanConstant;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_AGGREGATE_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_AGGREGATE_DAO)
public class AggregateDao extends AbstractDao {

    @Transactional
    public void update(Object obj) {
        sessionFactory.getCurrentSession().update(obj);
    }

    @Transactional
    public void save(Object obj) {
        sessionFactory.getCurrentSession().save(obj);
    }

    @Transactional(readOnly = true)
    public Object get(Class<?> clazz, Serializable id) {
        return sessionFactory.getCurrentSession().get(clazz, id);
    }


    @Transactional(readOnly = true)
    public String getGroupIdFromTrxId(String trxId) {
        String sql = "select group_id from tms_trxid \n" +
                "where 1=1 \n" +
                "and trx_id = :trxId ";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("trxId", trxId);
        return (String)query.uniqueResult();
    }

}

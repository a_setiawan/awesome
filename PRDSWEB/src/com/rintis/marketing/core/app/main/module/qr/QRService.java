package com.rintis.marketing.core.app.main.module.qr;

import com.rintis.marketing.beans.bean.bi.*;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.dao.QRDao;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by aaw on 16/06/2021.
 */

@Service
@Qualifier(SpringBeanConstant.SPRING_BEAN_QR_SERVICE)
@Named(SpringBeanConstant.SPRING_BEAN_QR_SERVICE)
public class QRService implements IQRService{
    private Logger log = Logger.getLogger(this.getClass());

    @Autowired
    private QRDao qrDao;

    @Override
    @Transactional(readOnly = true)
    public List<PanTrxidBean> mPanListTrxId(String param) throws Exception {
        return qrDao.listTrxIdBene(param);
    }

    @Override
    @Transactional(readOnly = true)
    public List<QRAnomalyBean> listAnomalyQRAcq1H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return qrDao.listQRAnomaliAcq1H(periodfr, periodto, timefr, timeto, trxtype, whitelist, bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<QRAnomalyBean> listAnomalyQRAcq3H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return qrDao.listQRAnomaliAcq3H(periodfr, periodto, timefr, timeto, trxtype, whitelist, bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<QRAnomalyBean> listAnomalyQRAcq6H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return qrDao.listQRAnomaliAcq6H(periodfr, periodto, timefr, timeto, trxtype, whitelist, bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<QRAnomalyBean> listAnomalyQRAcq1D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return qrDao.listQRAnomaliAcq1D(periodfr, periodto, trxtype, whitelist, bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<QRAnomalyBean> listAnomalyQRAcq3D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return qrDao.listQRAnomaliAcq3D(periodfr, periodto, trxtype, whitelist, bank_id);
    }

    @Transactional(readOnly = true)
    public List<DetailMPanAnomalyBean> listDetailMPanQR(String period, String timeFr, String timeTo, String pan, String qrType, String acquirerId, String periodTo, String periodType, String termId, String termName) throws Exception {
        return qrDao.listDetailMPanQR(period, timeFr, timeTo, pan, qrType, acquirerId, periodTo, periodType, termId, termName);
    }

    @Override
    @Transactional(readOnly=true)
    public boolean checkQRAcqUsed(String pan, String acqId) throws Exception {
        List<WhitelistQRBean> list = qrDao.listMPanExist(pan, acqId);
        if (list.size() > 0) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    @Transactional
    public void saveMPanTreshold(String pan, String acquirerId, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree,
        BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) throws Exception {
        qrDao.saveMPanTreshold(pan, acquirerId, freqApp, freqDc, freqDcFree, freqRvsl, totalAmtApp, totalAmtDc, totalAmtDcFree, totalAmtRvsl, totalAmt, active);
    }

}

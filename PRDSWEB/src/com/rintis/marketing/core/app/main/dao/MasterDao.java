package com.rintis.marketing.core.app.main.dao;

import com.rintis.marketing.beans.bean.master.BankEmailBean;
import com.rintis.marketing.beans.bean.master.BinBean;
import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.bean.report.BillerBean;
import com.rintis.marketing.beans.entity.*;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.constant.ParameterMapName;
import com.rintis.marketing.core.utils.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_MASTER_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_MASTER_DAO)
public class MasterDao extends AbstractDao {
    private Logger log = Logger.getLogger(MasterDao.class);

    @Transactional(readOnly = true)
    public List<BankBean> listBank(Map<String, Object> param) throws Exception {
        String bankActiveStatus = StringUtils.checkNull((String) param.get(ParameterMapName.PARAM_BANK_ACTIVE_STATUS));
        String bankIssAtmStatus = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_BANK_ISS_ATM_STATUS));
        String bankAcqAtmStatus = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_BANK_ACQ_ATM_STATUS));
        String bankBnfStatus = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_BANK_BNF_STATUS));
        String bankIsLocalSwitcher = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_BANK_IS_LOCAL_SWITCHER));
        String bankAcqPosStatus = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_BANK_ACQ_POS_STATUS));
        String bankIssPosStatus = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_BANK_ISS_POS_STATUS));
        String bankIssPmtStatus = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_BANK_ISS_PMT_STATUS));
        String bankAcqPmtStatus = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_BANK_ACQ_PMT_STATUS));
        String bankPayment = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_BANK_PAYMENT));

        StringBuffer sql = new StringBuffer("");
        sql.append("select bank_id as \"bankid\", bank_name as \"bankname\", " +
                "bank_short_name as \"bankshortname\", bank_iss_atm_status as \"bankissatmstatus\", " +
                "bank_acq_atm_status as \"bankacqatmstatus\", " +
                "bank_atm_qty as \"bankatmqty\", " +
                "bank_edc_qty as \"bankedcqty\", " +
                "bank_payment as \"bankpayment\", " +
                "bank_apn as \"bankapn\", " +
                "bank_cup as \"bankcup\", " +
                "bank_cnet as \"bankcnet\", " +
                /*Add by aaw -2/07/2021*/
                "bank_fiid as \"bankFiid\", " +
                /*end add*/
                "bank_cardholder_qty as \"bankcardholderqty\" " +
                "from " + DataConstant.MASTER_BANK + " " +
                "where 1=1 \n");

        if (bankIssAtmStatus.length() > 0) {
            sql.append("and bank_iss_atm_status = :bankIssAtmStatus ");
        }

        if (bankAcqAtmStatus.length() > 0) {
            sql.append("and bank_acq_atm_status = :bankAcqAtmStatus ");
        }

        if (bankBnfStatus.length() > 0) {
            sql.append("and bank_bnf_status = :bankBnfStatus ");
        }

        if (bankIsLocalSwitcher.length() > 0) {
            sql.append("and nvl(bank_islocalswitcher, 'NO') = :bankIsLocalSwitcher ");
        }

        if (bankAcqPosStatus.length() > 0) {
            sql.append("and bank_acq_pos_status = :bankAcqPosStatus ");
        }

        if (bankIssPosStatus.length() > 0) {
            sql.append("and bank_iss_pos_status = :bankIssPosStatus ");
        }

        if (bankAcqPmtStatus.length() > 0) {
            sql.append("and bank_acq_pmt_status = :bankAcqPmtStatus ");
        }

        if (bankIssPmtStatus.length() > 0) {
            sql.append("and bank_iss_pmt_status = :bankIssPmtStatus ");
        }

        if (bankActiveStatus.length() > 0) {
            sql.append("and bank_active_status = :bankActiveStatus ");
        }

        if (bankPayment.length() > 0) {
            sql.append("and bank_payment = :bankPayment ");
        }

        sql.append("order by bank_name \n");

        //log.info(sql);

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql.toString());

        if (bankIssAtmStatus.length() > 0) {
            query.setString("bankIssAtmStatus", bankIssAtmStatus);
        }

        if (bankAcqAtmStatus.length() > 0) {
            query.setString("bankAcqAtmStatus", bankAcqAtmStatus);
        }

        if (bankBnfStatus.length() > 0) {
            query.setString("bankBnfStatus", bankBnfStatus);
        }

        if (bankIsLocalSwitcher.length() > 0) {
            query.setString("bankIsLocalSwitcher", bankIsLocalSwitcher);
        }

        if (bankAcqPosStatus.length() > 0) {
            query.setString("bankAcqPosStatus", bankAcqPosStatus);
        }

        if (bankIssPosStatus.length() > 0) {
            query.setString("bankIssPosStatus", bankIssPosStatus);
        }

        if (bankAcqPmtStatus.length() > 0) {
            query.setString("bankAcqPmtStatus", bankAcqPmtStatus);
        }

        if (bankIssPmtStatus.length() > 0) {
            query.setString("bankIssPmtStatus", bankIssPmtStatus);
        }

        if (bankActiveStatus.length() > 0) {
            query.setString("bankActiveStatus", bankActiveStatus);
        }

        if (bankPayment.length() > 0) {
            query.setString("bankPayment", bankPayment);
        }

        query.setResultTransformer(new AliasToBeanResultTransformer(BankBean.class));
        //System.out.println(sql);
        return query.list();
    }

    /*@Transactional(readOnly = true)
    public List<EisBank> listBankEntity() throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(EisBank.class);
        criteria.addOrder(Order.asc("bankName"));
        return criteria.list();
    }*/

    // 210226 Modify by AAH
    @Transactional(readOnly = true)
    public List<BankBean> listBankEntity() throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(BankBean.class);
        criteria.addOrder(Order.asc("bankName"));
        return criteria.list();
    }

    //180801 Added By AAH
    @Transactional(readOnly = true)
    public List<BankBean> listBankCA(Map<String, Object> param) throws Exception {
        String sql = "select bank_id as \"bankid\", bank_name as \"bankname\", " +
                "bank_short_name as \"bankshortname\", bank_iss_atm_status as \"bankissatmstatus\", " +
                "bank_acq_atm_status as \"bankacqatmstatus\", " +
                "bank_atm_qty as \"bankatmqty\", " +
                "bank_edc_qty as \"bankedcqty\", " +
                "bank_payment as \"bankpayment\", " +
                "bank_apn as \"bankapn\", " +
                "bank_cup as \"bankcup\", " +
                "bank_cnet as \"bankcnet\", " +
                "bank_cardholder_qty as \"bankcardholderqty\" " +
                "from v_tms_bank_ca_biller ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(BankBean.class));
        //System.out.println(sql);
        return query.list();
    }

    @Transactional(readOnly = true)
    public List<TmsBankCa> listBankBIN() throws Exception {
        String sql = "select bank_id as \"bankId\", bank_name as \"bankName\" " +
                "from v_tms_bank_ca ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(TmsBankCa.class));
        return query.list();
    }

    //210218 Added By AAH
    /*@Transactional(readOnly = true)
    public List<BankBean> listBankCA(Map<String, Object> param) throws Exception {
        String sql = "SELECT * " +
                "FROM (SELECT " +
                "bank_id AS \"bankid\", bank_name AS \"bankname\", bank_short_name AS \"bankshortname\", " +
                "bank_iss_atm_status AS \"bankissatmstatus\", bank_acq_atm_status AS \"bankacqatmstatus\", " +
                "bank_atm_qty AS \"bankatmqty\", bank_edc_qty AS \"bankedcqty\", bank_payment AS \"bankpayment\", " +
                "bank_apn AS \"bankapn\", bank_cup AS \"bankcup\", bank_cnet AS \"bankcnet\", bank_cardholder_qty AS \"bankcardholderqty\" " +
                "FROM tms_bank where bank_fiid not in (SELECT distinct(ca_fiid) FROM tms_ca) ORDER BY bank_name) " +

        "UNION ALL " +

        "SELECT * " +
                "FROM (SELECT " +
                "ca_id AS \"bankid\", ca_id || '-' || ca_name AS \"bankname\", ca_name AS \"bankshortname\", " +
                "'YES' AS \"bankissatmstatus\", 'YES' AS \"bankacqatmstatus\", 0 AS \"bankatmqty\", " +
                "0 AS \"bankedcqty\", 'YES' AS \"bankpayment\", 'NO' AS \"bankapn\", " +
                "'NO' AS \"bankcup\", 'NO' AS \"bankcnet\", 0 AS \"bankcardholderqty\" " +
                "FROM tms_ca ORDER BY ca_name, ca_id) " +

        "UNION ALL " +

        "SELECT * " +
                "FROM (SELECT " +
                "biller_id AS \"bankid\", biller_id || '-' || biller_name AS \"bankname\", biller_name AS \"bankshortname\", " +
                "'YES' AS \"bankissatmstatus\", 'YES' AS \"bankacqatmstatus\", 0 AS \"bankatmqty\", " +
                "0 AS \"bankedcqty\", 'YES' AS \"bankpayment\", 'NO' AS \"bankapn\", " +
                "'NO' AS \"bankcup\", 'NO' AS \"bankcnet\", 0 AS \"bankcardholderqty\" " +
                "FROM tms_biller where biller_active_status = 'YES' ORDER BY biller_name, biller_id) \n";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(BankBean.class));
        System.out.println(sql);
        return query.list();
    }*/

    @Transactional(readOnly = true)
    public EisBank getBankByBankName(String bankName) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(EisBank.class);
        criteria.add(Restrictions.eq("bankName", bankName));
        return (EisBank)criteria.uniqueResult();
    }

    @Transactional(readOnly = true)
    public List<BillerBean> listBiller(Map<String, Object> param) throws Exception {
        String sql = "select biller_id as \"billerid\", biller_name as \"billername\", " +
                "biller_folder as \"billerfolder\", biller_active_status as \"billeractivestatus\", " +
                "biller_fiid as \"billerfiid\" " +
                "from mkt_biller \n" +
                "order by biller_name \n";

        //log.info(sql);

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(BillerBean.class));
        return query.list();
    }

    /*@Transactional(readOnly = true)
    public EisBiller getBillerByBillerName(String billerName) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(EisBiller.class);
        criteria.add(Restrictions.eq("billerName", billerName));
        return (EisBiller)criteria.uniqueResult();
    }*/




    @Transactional(readOnly = true)
    public List<EsqTrxIdBean> listEsqTrxId() throws Exception {

        String sql = "" +
                "select trx_id as \"trxid\",  trx_name as \"trxname\" \n" +
                "from tms_TrxId " +
                "order by trx_id ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);

        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrxIdBean.class));

        return query.list();
    }

    @Transactional(readOnly = true)
    public EsqTrxIdBean getEsqTrxId(String trxId) throws Exception {
        String sql = "" +
                "select trx_id as \"trxid\",  trx_name as \"trxname\" \n" +
                "from tms_TrxId " +
                "where trx_id = :trxId ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("trxId", trxId);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrxIdBean.class));

        return (EsqTrxIdBean)query.uniqueResult();
    }

    @Transactional(readOnly = true)
    public List<EsqTrxIdBean> listEsqTrxIndicator() throws Exception {

        String sql = "" +
                "select trx_id as \"trxidindicator\",  trx_name as \"trxnameindicator\" \n" +
                "from tms_trxindicator  " +
                "order by trx_id ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);

        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrxIdBean.class));

        return query.list();
    }

    @Transactional(readOnly = true)
    public EsqTrxIdBean getEsqTrxIdIndicator(String trxIndicatorId) throws Exception {
        String sql = "" +
                "select trx_id as \"trxidindicator\",  trx_name as \"trxnameindicator\" \n" +
                "from tms_trxindicator  " +
                "where trx_id = :trxIndicatorId ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("trxIndicatorId", trxIndicatorId);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrxIdBean.class));

        return (EsqTrxIdBean)query.uniqueResult();
    }

    @Transactional(readOnly = true)
    public List<EsqTrxIdBean> listEsqTrxIdTrxIndicator(String trxId) throws Exception {

        String sql = "" +
                "select a.trx_id as \"trxid\", a.trx_name as \"trxname\", \n" +
                "b.trx_id as \"trxidindicator\", b.trx_name as \"trxnameindicator\" \n" +
                "from tms_trxid a, tms_trxindicator b, tms_trxid_trxindicator c\n" +
                "where c.trx_id = :trxId \n" +
                "and c.trx_id = a.trx_id\n" +
                "and c.trx_indicator = b.trx_id \n" +
                "order by b.trx_id ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("trxId", trxId);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrxIdBean.class));

        return query.list();
    }

    @Transactional(readOnly = true)
    public List<EsqTrxIdBean> listEsqTrxIdTrxIndicator() throws Exception {

        String sql = "" +
                "select distinct \n" +
                "b.trx_id as \"trxidindicator\", b.trx_name as \"trxnameindicator\" \n" +
                "from tms_trxid a, tms_trxindicator b, tms_trxid_trxindicator c\n" +
                "where 1=1 \n" +
                "and c.trx_id = a.trx_id\n" +
                "and c.trx_indicator = b.trx_id \n" +
                "order by b.trx_id ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(EsqTrxIdBean.class));

        return query.list();
    }

    @Transactional(readOnly=true)
    public List<BankAlertThreshold> listBankAlertThresholdActive() throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(BankAlertThreshold.class);
        criteria.add(Restrictions.eq("active", BigDecimal.ONE));
        return criteria.list();
    }

    @Transactional(readOnly=true)
    public List<TmsMasterHoliday> listMasterHoliday(int year) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TmsMasterHoliday.class);
        criteria.add(Restrictions.eq("year", year));
        criteria.addOrder(Order.asc("holidayDate"));
        return criteria.list();
    }

    @Transactional(readOnly=true)
    public TmsMasterHoliday getMasterHoliday(Date date) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TmsMasterHoliday.class);
        criteria.add(Restrictions.ge("holidayDate", date));
        criteria.add(Restrictions.le("holidayDate", date));
        return (TmsMasterHoliday)criteria.uniqueResult();
    }



    @Transactional(readOnly = true)
    public BankBean getBankIntercon(String bankId, String bankCode) throws Exception {
        String sql = "select bank_id as \"bankid\", bank_code as \"bankcode\", bank_name as \"bankname\", " +
                "bank_shortname as \"bankshortname\" " +
                "from eis_bankintercon \n" +
                "where bank_code = :bankCode \n" +
                "and bank_id = :bankId \n" +
                "order by bank_name \n";

        //log.info(sql);

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("bankCode", bankCode);
        query.setString("bankId", bankId);
        query.setResultTransformer(new AliasToBeanResultTransformer(BankBean.class));
        return (BankBean)query.uniqueResult();
    }

    //180801 Added By AAH
    @Transactional(readOnly = true)
    public BankBean getBankCA(String bankId) throws Exception {
        String sql = "select bank_id as \"bankid\", NULL as \"bankcode\", bank_name as \"bankname\", " +
                "bank_short_name as \"bankshortname\" " +
                "from v_tms_bank_ca_biller " +
                "where bank_id = :bankId \n" +
                "order by bank_id \n";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("bankId", bankId);
        query.setResultTransformer(new AliasToBeanResultTransformer(BankBean.class));
        //System.out.println(sql);
        return (BankBean)query.uniqueResult();
    }




    @Transactional(readOnly=true)
    public List<GroupBank> listGroupBank() throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(GroupBank.class);
        criteria.addOrder(Order.asc("groupName"));
        return criteria.list();
    }

    @Transactional(readOnly=true)
    public List<GroupBankMember> listGroupBankMember(String groupId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(GroupBankMember.class);
        criteria.add(Restrictions.eq("groupBankMemberPk.groupId", groupId));
        return criteria.list();
    }

    @Transactional(readOnly=true)
    public List<BankEmail> listBankEmail() throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(BankEmail.class);
        return criteria.list();
    }

    @Transactional(readOnly=true)
    public List<BankEmailBean> listBankEmailBean() throws Exception {
        /*modify by aaw 15/09/2021*/
        String sql = "select a.bank_id as \"bankid\", a.bank_name as \"bankname\", b.email as \"email\", replace(replace(b.alert_mcc_status,'YES','true'),'NO','false') as \"alertMccVelocity\", replace(replace(b.alert_seq_status,'YES','true'),'NO','false') as \"alertSeqVelocity\", \n" +
                "replace(replace(b.alert_mpan_acq,'YES','true'),'NO','false') as \"alertMpanAcq\" \n" +
                "from v_tms_bank_ca_biller a left join tms_bank_email b on (a.bank_id = b.bank_id) \n" +
                "order by a.bank_name ";
        /*End modify*/
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(Transformers.aliasToBean(BankEmailBean.class));
        return query.list();
    }

    @Transactional(readOnly = true)
    public List<TmsUserQuickView> listUserQuickview(String userId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TmsUserQuickView.class);
        criteria.add(Restrictions.eq("tmsUserQuickViewPk.userId", userId));
        criteria.addOrder(Order.desc("quickDate"));
        return criteria.list();
    }

    @Transactional(readOnly=true)
    public List<BinList> listBin() throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(BinList.class);
        criteria.addOrder(Order.asc("bankId"));
        return criteria.list();
    }

    @Transactional(readOnly = true)
    public List<EsqTrxid> listTrx() throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(EsqTrxid.class);
        criteria.addOrder(Order.asc("trxName"));
        return criteria.list();
    }

    @Transactional(readOnly=true)
    public List<BinListTrxId> listBinTrxId(String listId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(BinListTrxId.class);
        criteria.add(Restrictions.eq("binListPk.listId", listId));
        return criteria.list();
    }

    @Transactional(readOnly=true)
    public List<MCCList> listMcc(String mcc) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MCCList.class);
        if (mcc != null) {
            if (!mcc.equals("-1")) {
                criteria.add(Restrictions.eq("mcc", mcc));
            }
        }

        criteria.addOrder(Order.asc("mcc"));
        return criteria.list();
    }

    @Transactional
    public void updateMcc(String mcc, String description) {
        String sql = "update tms_mcc set description = :description where mcc = :mcc";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("mcc", mcc);
        query.setString("description", description);

        query.executeUpdate();
    }

    @Transactional
    public void saveMcc(String mcc, String description) {
        String sql = "insert into tms_mcc values (:mcc, :description)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("mcc", mcc);
        query.setString("description", description);
        query.executeUpdate();
    }

    @Transactional(readOnly=true)
    public List<MCCParameter> listMccParameter(String mcc) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MCCParameter.class);
        if (mcc != null) {
            if (!mcc.equals("-1")) {
                criteria.add(Restrictions.eq("mcc", mcc));
            }
        }

        criteria.addOrder(Order.asc("mcc"));
        return criteria.list();
    }

    @Transactional
    public void updateMccParameter(String mcc, BigDecimal trxapp, BigDecimal trxtot, BigDecimal amtapp, BigDecimal amttot) {
        String sql = "update tms_param_mcc set min_trx_app_1d = :trxapp, min_trx_tot_1d = :trxtot, min_amt_app_1d = :amtapp, min_amt_tot_1d = :amttot where mcc = :mcc";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("mcc", mcc);
        query.setBigDecimal("trxapp", trxapp);
        query.setBigDecimal("trxtot", trxtot);
        query.setBigDecimal("amtapp", amtapp);
        query.setBigDecimal("amttot", amttot);
        query.executeUpdate();
    }

    @Transactional
    public void saveMccParameter(String mcc, BigDecimal trxapp, BigDecimal trxtot, BigDecimal amtapp, BigDecimal amttot) {
        String sql = "insert into tms_param_mcc values (:mcc, :trxapp, :trxtot, :amtapp, :amttot)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("mcc", mcc);
        query.setBigDecimal("trxapp", trxapp);
        query.setBigDecimal("trxtot", trxtot);
        query.setBigDecimal("amtapp", amtapp);
        query.setBigDecimal("amttot", amttot);
        query.executeUpdate();
    }

    /*Add by aaw 13/09/2021*/
    @Transactional(readOnly=true)
    public String checkDataExist(String bankId){
        String sql = "SELECT BANK_ID FROM TMS_PARAM_ANMLY_PER_BANK WHERE BANK_ID = :bankId AND ROWNUM = 1";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("bankId", bankId.trim());
        //query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyParamBean.class));
        return (String) query.uniqueResult();
    }
    /*End add*/

    @Transactional(readOnly=true)
    public List<BinBean> binList() throws Exception {
        String sql = "select a.bank_id as \"bankId\", b.bank_name as \"bankName\", a.bin_no as \"binNo\", a.bin_length as \"binLength\" \n" +
                "from tms_bin a join v_tms_bank_ca b on (a.bank_id = b.bank_id) \n" +
                "order by b.bank_name ";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(Transformers.aliasToBean(BinBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public String getBinBankId(String pan){
        String sql = "SELECT NVL(NVL(TMS_BIN_API.GET_BANK_ID(SUBSTR(:pan,1,6)), NVL(TMS_BIN_API.GET_BANK_ID(SUBSTR(:pan,1,8)), TMS_BIN_API.GET_BANK_ID(SUBSTR(:pan,1,2)))), ' ') pan FROM DUAL WHERE ROWNUM = 1";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan);
        return (String) query.uniqueResult();
    }

    @Transactional(readOnly = true)
    public List<BankBean> getListBankCA(String bankId) throws Exception {
        String sql = "select bank_id as \"bankid\", NULL as \"bankcode\", bank_name as \"bankname\", " +
                "bank_short_name as \"bankshortname\" " +
                "from v_tms_bank_ca_biller " +
                "where bank_id = :bankId \n" +
                "order by bank_id \n";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("bankId", bankId);
        query.setResultTransformer(new AliasToBeanResultTransformer(BankBean.class));
        return query.list();
    }

    @Transactional
    public void addBin(String selectedBankId, String binNo, String binLength) {
        String sql = "insert into tms_bin (bank_id, bin_no, bin_length) values (:selectedBankId, :binNo, :binLength)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("selectedBankId", selectedBankId);
        query.setString("binNo", binNo);
        query.setInteger("binLength", Integer.parseInt(binLength));
        query.executeUpdate();
    }

    @Transactional
    public void deleteBin(String bankId, String binNo, BigDecimal binLength) {
        String sql = "delete from tms_bin where bank_id = :bankId and bin_no = :binNo and bin_length = :binLength";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("bankId", bankId);
        query.setString("binNo", binNo);
        query.setBigDecimal("binLength", binLength);
        query.executeUpdate();
    }
}

package com.rintis.marketing.core.app.main.dao;

import com.rintis.marketing.beans.bean.bi.AlertThresholdBean;
import com.rintis.marketing.beans.entity.AlertThresholdGroup;
import com.rintis.marketing.core.SpringBeanConstant;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.util.List;

@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_ALERT_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_ALERT_DAO)
public class AlertDao extends AbstractDao {

    @Transactional(readOnly = true)
    public List<AlertThresholdGroup> listAlertThresholdDetail(String groupId, String trxId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertThresholdGroup.class);
        criteria.add(Restrictions.eq("alertThresholdGroupPk.groupBankId", groupId));
        criteria.add(Restrictions.eq("alertThresholdGroupPk.trxId", trxId));
        return criteria.list();
    }

    @Transactional(readOnly = true)
    public List<AlertThresholdGroup> listAlertThresholdDetail(String groupId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertThresholdGroup.class);
        criteria.add(Restrictions.eq("alertThresholdGroupPk.groupBankId", groupId));
        return criteria.list();
    }

    @Transactional(readOnly = true)
    public List<AlertThresholdBean> listAlertThresholdGroupHeader() throws Exception {
        String sql = "select distinct a.group_bank_id as \"groupid\", b.group_name as \"groupname\", " +
                "a.trx_id as \"transactionid\", \n" +
                "c.trx_name as \"transactionname\" \n" +
                "from tms_alert_th_group a left join tms_group_bank b on (a.group_bank_id = b.group_id) \n" +
                "left join tms_trxid c on (a.trx_id = c.trx_id) \n" +
                "ORDER BY b.group_name, c.trx_name ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(AlertThresholdBean.class));
        return query.list();
    }

    @Transactional
    public void deleteAlertThresholdGroup(String groupId, String trxId) throws Exception {
        String sql = "delete from tms_alert_th_group where group_bank_id = :groupId \n" +
                "and trx_id = :trxId \n";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("groupId", groupId);
        query.setString("trxId", trxId);
        query.executeUpdate();
    }

}

package com.rintis.marketing.core.app.main.module.mpanTreshold;

/*Created by aaw 28/062021*/

import com.rintis.marketing.beans.bean.bi.WhitelistQRBean;

import java.math.BigDecimal;
import java.util.List;

public interface IMPanTresholdService {

    public List<WhitelistQRBean> listMPanQR() throws Exception;

    public void deleteMPanTreshold(String pan, String acquirerId) throws Exception;

    public boolean checkMPanUsed(String pan, String acquirerId) throws Exception;

    public void saveMPanTreshold(WhitelistQRBean whitelistQRBean) throws Exception;

    public List<WhitelistQRBean> getWhitelistMPan(String pan, String acquirerId) throws Exception;

    public void updateMPan(String pan, String acquirerId, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc,
                                  BigDecimal totalAmtDcFree, BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) throws Exception;
}
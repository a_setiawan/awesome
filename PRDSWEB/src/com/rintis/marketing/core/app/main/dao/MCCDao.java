package com.rintis.marketing.core.app.main.dao;

import com.rintis.marketing.beans.bean.bi.MCCAnomalyBean;
import com.rintis.marketing.beans.entity.UserLoginBank;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import org.hibernate.SQLQuery;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.util.List;


@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_MCC_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_MCC_DAO)
public class MCCDao extends AbstractDao {
    private String issuerbank;
    private UserLoginBank ulb;
    private ModuleFactory moduleFactory;

    @Transactional(readOnly=true)
    public List listMCCAnomaly(String periodFr, String periodTo, String bank_id){
        String sql = "select \n" +
                "to_char(to_date(ta.period,'YYMMDD'), 'DD-MM-YYYY') as \"period\", \n" +
                "to_char(to_date(ta.period,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "ta.mcc as \"mcc\", \n" +
                "tm.description as \"description\", \n" +
                "ta.pan as \"pan\", \n" +
                "TMS_BANK_API.Get_Bank_Name(ta.bank_id) as \"issuerbank\", \n" +
                "ta.freq_app as \"freqapp\", \n" +
                "ta.freq_tot as \"freqtot\", \n" +
                "nvl(ta.amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(ta.amt_tot/100,0) as \"totalamttot\" \n" +
                "from \n" +
                "tms_trxmcc1day ta join tms_mcc tm on ta.mcc = tm.mcc \n" +
                "where \n" +
                "ta.period >= :periodFr and ta.period <= :periodTo \n";

        if(bank_id != ""){
            sql = sql + "and ta.bank_id = bank_id \n";
        }

        sql = sql + "order by ta.period, ta.mcc, tm.description, ta.pan, ta.bank_id ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodFr", periodFr);
        query.setString("periodTo", periodTo);
        query.setResultTransformer(new AliasToBeanResultTransformer(MCCAnomalyBean.class));
        return query.list();
    }
}

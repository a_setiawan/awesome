package com.rintis.marketing.core.app.main.module.translog;

import com.rintis.marketing.beans.entity.TransactionLog;
import com.rintis.marketing.beans.entity.UserLoginHistory;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.dao.TransLogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.util.List;
import java.util.Map;

@Service
@Qualifier(SpringBeanConstant.SPRING_BEAN_TRANSLOG_SERVICE)
@Named(SpringBeanConstant.SPRING_BEAN_TRANSLOG_SERVICE)
public class TransLogService implements ITransLogService {
    @Autowired
    private TransLogDao translogDao;

    @Override
    @Transactional(readOnly = true)
    public List<UserLoginHistory> searchUserLoginLog(Map<String, Object> param) throws Exception {
        return translogDao.searchUserLoginLog(param);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TransactionLog> searchTransactionLog(Map<String, Object> param) throws Exception {
        return translogDao.searchTransactionLog(param);
    }
}

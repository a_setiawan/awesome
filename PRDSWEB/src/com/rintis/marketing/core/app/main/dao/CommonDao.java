package com.rintis.marketing.core.app.main.dao;

import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.beans.entity.*;
import com.rintis.marketing.core.SpringBeanConstant;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.beans.Transient;
import java.util.Date;
import java.util.List;

@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_COMMON_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_COMMON_DAO)
public class CommonDao extends AbstractDao {


    @Transactional(readOnly=true)
    public SystemProperty getSystemProperty(String key) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SystemProperty.class);
        criteria.add(Restrictions.eq("systemName", key));
        return (SystemProperty)criteria.uniqueResult();
    }

    @Transactional(readOnly=true)
    public List<SystemProperty> listSystemProperty() throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SystemProperty.class);
        criteria.addOrder(Order.asc("id"));
        return criteria.list();
    }

    @Transactional(readOnly=true)
    public List<SystemProperty> listSystemPropertyPerUserWithExclude(Integer perUser, List<String> listExclude) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SystemProperty.class);
        criteria.add(Restrictions.eq("perUser", perUser));
        if (listExclude != null) {
            if (listExclude.size() > 0) {
                criteria.add(Restrictions.not(Restrictions.in("systemName", listExclude  )));
            }
        }
        criteria.addOrder(Order.asc("id"));
        return criteria.list();
    }

    @Transactional(readOnly=true)
    public List<SystemProperty> listSystemPropertyPerUser(Integer perUser, String name) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SystemProperty.class);
        criteria.add(Restrictions.eq("perUser", perUser));
        criteria.add(Restrictions.like("systemName", name, MatchMode.ANYWHERE));
        criteria.addOrder(Order.asc("id"));
        return criteria.list();
    }

    @Transactional(readOnly=true)
    public SystemPropertyUser getSystemProperty(String userId, String key) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SystemPropertyUser.class);
        criteria.add(Restrictions.eq("systemName", key));
        criteria.add(Restrictions.eq("userLoginId", userId));
        return (SystemPropertyUser)criteria.uniqueResult();
    }

    @Transactional(readOnly=true)
    public List<SystemPropertyUser> listSystemProperty(String userId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SystemPropertyUser.class);
        criteria.add(Restrictions.eq("userLoginId", userId));
        criteria.addOrder(Order.asc("systemName"));
        return criteria.list();
    }

    @Transactional(readOnly=true)
    public List<Enumeration> listEnumeration(String enumTypeId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Enumeration.class);
        criteria.add(Restrictions.eq("enumTypeId", enumTypeId));
        criteria.addOrder(Order.asc("seq"));
        return criteria.list();
    }

    @Transactional(readOnly = true)
    public Enumeration getEnumerationByEnumId(String enumId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Enumeration.class);
        criteria.add(Restrictions.eq("enumId", enumId));
        return (Enumeration)criteria.uniqueResult();
    }

    @Transactional(readOnly = true)
    public Enumeration getEnumerationByDescription(String description) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Enumeration.class);
        criteria.add(Restrictions.eq("description", description));
        return (Enumeration)criteria.uniqueResult();
    }

    @Transactional
    public List<EmailQueue> listEmailQueue() throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(EmailQueue.class);
        return criteria.list();
    }
}

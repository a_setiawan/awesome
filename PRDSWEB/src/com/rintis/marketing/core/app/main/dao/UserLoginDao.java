package com.rintis.marketing.core.app.main.dao;

import java.util.List;

import javax.inject.Named;

import com.rintis.marketing.beans.entity.UserLogin;
import com.rintis.marketing.beans.entity.UserLoginBank;
import com.rintis.marketing.core.SpringBeanConstant;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_USERLOGIN_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_USERLOGIN_DAO)
public class UserLoginDao extends AbstractDao {

    @Transactional(readOnly=true)
    public boolean login(String userId, String password) {
        boolean result = false;
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserLogin.class);
        criteria.add(Restrictions.eq("userLoginId", userId));
        criteria.add(Restrictions.eq("currentPassword", password));
        criteria.add(Restrictions.eq("enabled", 1));
        UserLogin ul = (UserLogin)criteria.uniqueResult();
        if (ul != null) {
            result = true;
        }
        return result;
    }

    @Transactional(readOnly=true)
    public boolean loginBank(String userId, String password) {
        boolean result = false;
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserLoginBank.class);
        criteria.add(Restrictions.eq("userLoginId", userId));
        criteria.add(Restrictions.eq("currentPassword", password));
        criteria.add(Restrictions.eq("enabled", 1));
        UserLoginBank ul = (UserLoginBank)criteria.uniqueResult();
        if (ul != null) {
            result = true;
        }
        return result;
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(readOnly=true)
    public List<UserLogin> listUserName(String partyId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserLogin.class);
        criteria.add(Restrictions.eq("partyId", partyId));
        criteria.addOrder(Order.asc("userLoginId"));
        return criteria.list();
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(readOnly=true)
    public List<UserLoginBank> listUserLoginBank(Integer roleId, int userBank, String bankId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserLoginBank.class);
        if (roleId != null) {
            if (roleId > 0) {
                criteria.add(Restrictions.eq("roleTypeId", roleId));
            }
        }
        criteria.add(Restrictions.eq("userBank", userBank));
        if (bankId != null) {
            if (bankId.length() > 0) {
                criteria.add(Restrictions.eq("bankId", bankId));
            }
        }
        criteria.addOrder(Order.asc("userLoginId"));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly=true)
    public List<UserLogin> listUserLogin(Integer roleId, int userBank) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserLogin.class);
        if (roleId != null) {
            if (roleId > 0) {
                criteria.add(Restrictions.eq("roleTypeId", roleId));
            }
        }

        criteria.add(Restrictions.eq("userBank", userBank));
        criteria.addOrder(Order.asc("userLoginId"));
        return criteria.list();
    }
   
    @Transactional(readOnly=true)
    public UserLogin getUserLogin(String userLoginId, Integer enabled) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserLogin.class);
        criteria.add(Restrictions.eq("userLoginId", userLoginId));
        if (enabled > -1) {
            criteria.add(Restrictions.eq("enabled", enabled));
        }
        return (UserLogin)criteria.uniqueResult();
    }

    @Transactional(readOnly=true)
    public UserLoginBank getUserLoginBank(String userLoginId, Integer enabled) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserLoginBank.class);
        criteria.add(Restrictions.eq("userLoginId", userLoginId));
        if (enabled > -1) {
            criteria.add(Restrictions.eq("enabled", enabled));
        }
        return (UserLoginBank)criteria.uniqueResult();
    }

    @Transactional(readOnly = true)
    public String getEmail(String userId) {
        String sql = "select email from tms_user_login ul \n" +
                "where user_login_id = :userId ";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("userId", userId);
        return (String)query.uniqueResult();
    }
    
    
}

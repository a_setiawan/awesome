package com.rintis.marketing.core.app.main.module.pan;

import com.rintis.marketing.beans.bean.bi.*;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.dao.PanDao;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by aabraham on 11/02/2019.
 */
@Service
@Qualifier(SpringBeanConstant.SPRING_BEAN_PAN_SERVICE)
@Named(SpringBeanConstant.SPRING_BEAN_PAN_SERVICE)
public class PanService implements IPanService{
    private Logger log = Logger.getLogger(this.getClass());

    @Autowired
    private PanDao panDao;

    @Override
    @Transactional(readOnly = true)
    public List<DetailPanAnomalyBean> listDetailAtm(String period, String timeFr, String timeTo, String pan, String tranType, String pengirim) throws Exception {
        return panDao.listDetailAtm(period, timeFr, timeTo, pan, tranType, pengirim);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DetailPanAnomalyBean> listDetailPos(String period, String timeFr, String timeTo, String pan, String tranType, String pengirim, String status) throws Exception {
        return panDao.listDetailPos(period, timeFr, timeTo, pan, tranType, pengirim, status);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DetailPanAnomalyPaymentBean> listDetailPgw(String period, String timeFr, String timeTo, String pan, String tranType, String pengirim) throws Exception {
        return panDao.listDetailPgw(period, timeFr, timeTo, pan, tranType, pengirim);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanTrxidBean> panListTrxId() throws Exception {
        return panDao.listTrxId();
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanTrxidBean> panListTrxIdBene() throws Exception {
        return panDao.listTrxIdBene();
    }

    @Override
    @Transactional(readOnly = true)
    public List<WhitelistBean> listPan() throws Exception {
        return panDao.listPan();
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyEmailBean> listEmail() throws Exception {
        return panDao.listEmailAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyParamBean> listParam() throws Exception {
        return panDao.listParam();
    }

    //start add (ERE 200313)
    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyParamBean> listParamBankBene() throws Exception {
        return panDao.listParamBankBene();
    }
    //stop add (ERE 200313)

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyParamBean> listParamBank() throws Exception {
        return panDao.listParamBank();
    }

    //add
    @Override
    @Transactional(readOnly=true)
    public List<PanAnomalyParamBean> listMaxAmountParam() throws Exception{
        return panDao.listMaxAmountParam();
    }

    @Override
    @Transactional
    public void deletePan(String pan, String pengirim) throws Exception {
        panDao.deletePan(pan, pengirim);
    }

    @Override
    @Transactional
    public void deleteEmail(String useremail) throws Exception {
        panDao.deleteEmail(useremail);
    }

    @Override
    @Transactional
    public void deleteParamBank(String bank_id) throws Exception {
        panDao.deleteParamBank(bank_id);
    }

    @Override
    @Transactional
    public void savePan(String pan, String pengirim, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree, BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) throws Exception {
        /*Add by aaw 22/07/2021, beberapa tran code sender = '' ataupun null, untuk menghindari constraint sender null di db maka set sender ' '*/
        if ("".equals(pengirim.trim())){
            pengirim = " ";
        }
        /*End add*/
        panDao.savePan(pan.trim(), pengirim, freqApp, freqDc, freqDcFree, freqRvsl, totalAmtApp, totalAmtDc, totalAmtDcFree, totalAmtRvsl, totalAmt, active);
    }

    @Override
    @Transactional
    public void saveParamBank(List<PanAnomalyParamBean> list, String bank_id) throws Exception {

        for(PanAnomalyParamBean papb : list){
            PanAnomalyParamBean panAnomalyParam = new PanAnomalyParamBean();
            panAnomalyParam.setTrx(papb.getTrx());
            panAnomalyParam.setTrxApp1h(papb.getTrxApp1h());
            panAnomalyParam.setAmtApp1h(papb.getAmtApp1h());
            panAnomalyParam.setTrxDc1h(papb.getTrxDc1h());
            panAnomalyParam.setAmtDc1h(papb.getAmtDc1h());
            panAnomalyParam.setTrxDcFree1h(papb.getTrxDcFree1h());
            panAnomalyParam.setAmtDcFree1h(papb.getAmtDcFree1h());
            panAnomalyParam.setTrxRvsl1h(papb.getTrxRvsl1h());
            panAnomalyParam.setAmtRvsl1h(papb.getAmtRvsl1h());

            panAnomalyParam.setTrxApp3h(papb.getTrxApp3h());
            panAnomalyParam.setAmtApp3h(papb.getAmtApp3h());
            panAnomalyParam.setTrxDc3h(papb.getTrxDc3h());
            panAnomalyParam.setAmtDc3h(papb.getAmtDc3h());
            panAnomalyParam.setTrxDcFree3h(papb.getTrxDcFree3h());
            panAnomalyParam.setAmtDcFree3h(papb.getAmtDcFree3h());
            panAnomalyParam.setTrxRvsl3h(papb.getTrxRvsl3h());
            panAnomalyParam.setAmtRvsl3h(papb.getAmtRvsl3h());

            panAnomalyParam.setTrxApp6h(papb.getTrxApp6h());
            panAnomalyParam.setAmtApp6h(papb.getAmtApp6h());
            panAnomalyParam.setTrxDc6h(papb.getTrxDc6h());
            panAnomalyParam.setAmtDc6h(papb.getAmtDc6h());
            panAnomalyParam.setTrxDcFree6h(papb.getTrxDcFree6h());
            panAnomalyParam.setAmtDcFree6h(papb.getAmtDcFree6h());
            panAnomalyParam.setTrxRvsl6h(papb.getTrxRvsl6h());
            panAnomalyParam.setAmtRvsl6h(papb.getAmtRvsl6h());

            panAnomalyParam.setTrxApp1d(papb.getTrxApp1d());
            panAnomalyParam.setAmtApp1d(papb.getAmtApp1d());
            panAnomalyParam.setTrxDc1d(papb.getTrxDc1d());
            panAnomalyParam.setAmtDc1d(papb.getAmtDc1d());
            panAnomalyParam.setTrxDcFree1d(papb.getTrxDcFree1d());
            panAnomalyParam.setAmtDcFree1d(papb.getAmtDcFree1d());
            panAnomalyParam.setTrxRvsl1d(papb.getTrxRvsl1d());
            panAnomalyParam.setAmtRvsl1d(papb.getAmtRvsl1d());

            panAnomalyParam.setTrxApp3d(papb.getTrxApp3d());
            panAnomalyParam.setAmtApp3d(papb.getAmtApp3d());
            panAnomalyParam.setTrxDc3d(papb.getTrxDc3d());
            panAnomalyParam.setAmtDc3d(papb.getAmtDc3d());
            panAnomalyParam.setTrxDcFree3d(papb.getTrxDcFree3d());
            panAnomalyParam.setAmtDcFree3d(papb.getAmtDcFree3d());
            panAnomalyParam.setTrxRvsl3d(papb.getTrxRvsl3d());
            panAnomalyParam.setAmtRvsl3d(papb.getAmtRvsl3d());

        }
        panDao.saveParamBank(list, bank_id);
    }

    @Override
    @Transactional
    public void updateParamBank(List<PanAnomalyParamBean> list, String bank_id) throws Exception {

        for(PanAnomalyParamBean papb : list){

            PanAnomalyParamBean panAnomalyParam = new PanAnomalyParamBean();
            panAnomalyParam.setTrx(papb.getTrx());
            panAnomalyParam.setTrxApp1h(papb.getTrxApp1h());
            panAnomalyParam.setAmtApp1h(papb.getAmtApp1h());
            panAnomalyParam.setTrxDc1h(papb.getTrxDc1h());
            panAnomalyParam.setAmtDc1h(papb.getAmtDc1h());
            panAnomalyParam.setTrxDcFree1h(papb.getTrxDcFree1h());
            panAnomalyParam.setAmtDcFree1h(papb.getAmtDcFree1h());
            panAnomalyParam.setTrxRvsl1h(papb.getTrxRvsl1h());
            panAnomalyParam.setAmtRvsl1h(papb.getAmtRvsl1h());

            panAnomalyParam.setTrxApp3h(papb.getTrxApp3h());
            panAnomalyParam.setAmtApp3h(papb.getAmtApp3h());
            panAnomalyParam.setTrxDc3h(papb.getTrxDc3h());
            panAnomalyParam.setAmtDc3h(papb.getAmtDc3h());
            panAnomalyParam.setTrxDcFree3h(papb.getTrxDcFree3h());
            panAnomalyParam.setAmtDcFree3h(papb.getAmtDcFree3h());
            panAnomalyParam.setTrxRvsl3h(papb.getTrxRvsl3h());
            panAnomalyParam.setAmtRvsl3h(papb.getAmtRvsl3h());

            panAnomalyParam.setTrxApp6h(papb.getTrxApp6h());
            panAnomalyParam.setAmtApp6h(papb.getAmtApp6h());
            panAnomalyParam.setTrxDc6h(papb.getTrxDc6h());
            panAnomalyParam.setAmtDc6h(papb.getAmtDc6h());
            panAnomalyParam.setTrxDcFree6h(papb.getTrxDcFree6h());
            panAnomalyParam.setAmtDcFree6h(papb.getAmtDcFree6h());
            panAnomalyParam.setTrxRvsl6h(papb.getTrxRvsl6h());
            panAnomalyParam.setAmtRvsl6h(papb.getAmtRvsl6h());

            panAnomalyParam.setTrxApp1d(papb.getTrxApp1d());
            panAnomalyParam.setAmtApp1d(papb.getAmtApp1d());
            panAnomalyParam.setTrxDc1d(papb.getTrxDc1d());
            panAnomalyParam.setAmtDc1d(papb.getAmtDc1d());
            panAnomalyParam.setTrxDcFree1d(papb.getTrxDcFree1d());
            panAnomalyParam.setAmtDcFree1d(papb.getAmtDcFree1d());
            panAnomalyParam.setTrxRvsl1d(papb.getTrxRvsl1d());
            panAnomalyParam.setAmtRvsl1d(papb.getAmtRvsl1d());

            panAnomalyParam.setTrxApp3d(papb.getTrxApp3d());
            panAnomalyParam.setAmtApp3d(papb.getAmtApp3d());
            panAnomalyParam.setTrxDc3d(papb.getTrxDc3d());
            panAnomalyParam.setAmtDc3d(papb.getAmtDc3d());
            panAnomalyParam.setTrxDcFree3d(papb.getTrxDcFree3d());
            panAnomalyParam.setAmtDcFree3d(papb.getAmtDcFree3d());
            panAnomalyParam.setTrxRvsl3d(papb.getTrxRvsl3d());
            panAnomalyParam.setAmtRvsl3d(papb.getAmtRvsl3d());

        }

        panDao.updateParamBank(list, bank_id);
    }

    //start modify by erichie
    @Override
    @Transactional
    public void saveEmail(String useremail, String gender, String username, String alert1h, String alert3h, String alert6h, String alert1d, String alert3d, String alertMaxAmt, String alertMccVelocity, String alertSeqVelocity, String alertMpanAcq) throws Exception {

        //panDao.saveEmail(useremail, gender, username, alert1h, alert3h, alert6h, alert1d, alert3d, alertMaxAmt, alertMccVelocity, alertSeqVelocity, alertMpanAcq);
        panDao.saveEmailInstitusi(useremail, gender, username, alert1h, alert3h, alert6h, alert1d, alert3d, alertMaxAmt, alertMccVelocity, alertSeqVelocity, alertMpanAcq);
    }
    //end modify by erichie

    @Override
    @Transactional
    public void updatePan(String pan, String pengirim, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree, BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) throws Exception {
        panDao.updatePan(pan, pengirim, freqApp, freqDc, freqDcFree, freqRvsl, totalAmtApp, totalAmtDc, totalAmtDcFree, totalAmtRvsl, totalAmt, active);
    }

    @Override
    @Transactional
    public void updateEmail(String useremail, String gender, String username, String alert1h, String alert3h, String alert6h, String alert1d, String alert3d, String alertMaxAmt, String alertMccVelocity, String alertSeqVelocity, String alertMpanAcq) throws Exception {
        /*Modify by aaw 02/07/2021*/
        panDao.updateEmail(useremail, gender, username, alert1h, alert3h, alert6h, alert1d, alert3d, alertMaxAmt, alertMccVelocity, alertSeqVelocity, alertMpanAcq);
    }

    //start modify by erichie
    @Override
    @Transactional
    public void updateParam(String trx, BigDecimal mintrxapp1h, BigDecimal minamtapp1h, BigDecimal mintrxdc1h, BigDecimal minamtdc1h, BigDecimal mintrxdcfree1h, BigDecimal minamtdcfree1h, BigDecimal mintrxrvsl1h, BigDecimal minamtrvsl1h, BigDecimal minamttot1h, BigDecimal mintrxapp3h, BigDecimal minamtapp3h, BigDecimal mintrxdc3h, BigDecimal minamtdc3h, BigDecimal mintrxdcfree3h, BigDecimal minamtdcfree3h, BigDecimal mintrxrvsl3h, BigDecimal minamtrvsl3h, BigDecimal minamttot3h, BigDecimal mintrxapp6h, BigDecimal minamtapp6h, BigDecimal mintrxdc6h, BigDecimal minamtdc6h, BigDecimal mintrxdcfree6h, BigDecimal minamtdcfree6h, BigDecimal mintrxrvsl6h, BigDecimal minamtrvsl6h, BigDecimal minamttot6h, BigDecimal mintrxapp1d, BigDecimal minamtapp1d, BigDecimal mintrxdc1d, BigDecimal minamtdc1d, BigDecimal mintrxdcfree1d, BigDecimal minamtdcfree1d, BigDecimal mintrxrvsl1d, BigDecimal minamtrvsl1d, BigDecimal minamttot1d, BigDecimal mintrxapp3d, BigDecimal minamtapp3d, BigDecimal mintrxdc3d, BigDecimal minamtdc3d, BigDecimal mintrxdcfree3d, BigDecimal minamtdcfree3d, BigDecimal mintrxrvsl3d, BigDecimal minamtrvsl3d, BigDecimal minamttot3d) throws Exception {
        panDao.updateParam(trx, mintrxapp1h, minamtapp1h, mintrxdc1h, minamtdc1h, mintrxdcfree1h, minamtdcfree1h, mintrxrvsl1h, minamtrvsl1h, minamttot1h, mintrxapp3h, minamtapp3h, mintrxdc3h, minamtdc3h, mintrxdcfree3h, minamtdcfree3h, mintrxrvsl3h, minamtrvsl3h, minamttot3h, mintrxapp6h, minamtapp6h, mintrxdc6h, minamtdc6h, mintrxdcfree6h, minamtdcfree6h, mintrxrvsl6h, minamtrvsl6h, minamttot6h, mintrxapp1d, minamtapp1d, mintrxdc1d, minamtdc1d, mintrxdcfree1d, minamtdcfree1d, mintrxrvsl1d, minamtrvsl1d, minamttot1d, mintrxapp3d, minamtapp3d, mintrxdc3d, minamtdc3d, mintrxdcfree3d, minamtdcfree3d, mintrxrvsl3d, minamtrvsl3d, minamttot3d);
    }
    //end modify by erichie

    //start add (ERE 200313)
    @Override
    @Transactional
    public void updateParamBankBene(String trx, BigDecimal mintrxapp1h, BigDecimal minamtapp1h, BigDecimal mintrxdc1h, BigDecimal minamtdc1h, BigDecimal mintrxdcfree1h, BigDecimal minamtdcfree1h, BigDecimal mintrxrvsl1h, BigDecimal minamtrvsl1h, BigDecimal minamttot1h, BigDecimal mintrxapp3h, BigDecimal minamtapp3h, BigDecimal mintrxdc3h, BigDecimal minamtdc3h, BigDecimal mintrxdcfree3h, BigDecimal minamtdcfree3h, BigDecimal mintrxrvsl3h, BigDecimal minamtrvsl3h, BigDecimal minamttot3h, BigDecimal mintrxapp6h, BigDecimal minamtapp6h, BigDecimal mintrxdc6h, BigDecimal minamtdc6h, BigDecimal mintrxdcfree6h, BigDecimal minamtdcfree6h, BigDecimal mintrxrvsl6h, BigDecimal minamtrvsl6h, BigDecimal minamttot6h, BigDecimal mintrxapp1d, BigDecimal minamtapp1d, BigDecimal mintrxdc1d, BigDecimal minamtdc1d, BigDecimal mintrxdcfree1d, BigDecimal minamtdcfree1d, BigDecimal mintrxrvsl1d, BigDecimal minamtrvsl1d, BigDecimal minamttot1d, BigDecimal mintrxapp3d, BigDecimal minamtapp3d, BigDecimal mintrxdc3d, BigDecimal minamtdc3d, BigDecimal mintrxdcfree3d, BigDecimal minamtdcfree3d, BigDecimal mintrxrvsl3d, BigDecimal minamtrvsl3d, BigDecimal minamttot3d) throws Exception {
        panDao.updateParamBankBene(trx, mintrxapp1h, minamtapp1h, mintrxdc1h, minamtdc1h, mintrxdcfree1h, minamtdcfree1h, mintrxrvsl1h, minamtrvsl1h, minamttot1h, mintrxapp3h, minamtapp3h, mintrxdc3h, minamtdc3h, mintrxdcfree3h, minamtdcfree3h, mintrxrvsl3h, minamtrvsl3h, minamttot3h, mintrxapp6h, minamtapp6h, mintrxdc6h, minamtdc6h, mintrxdcfree6h, minamtdcfree6h, mintrxrvsl6h, minamtrvsl6h, minamttot6h, mintrxapp1d, minamtapp1d, mintrxdc1d, minamtdc1d, mintrxdcfree1d, minamtdcfree1d, mintrxrvsl1d, minamtrvsl1d, minamttot1d, mintrxapp3d, minamtapp3d, mintrxdc3d, minamtdc3d, mintrxdcfree3d, minamtdcfree3d, mintrxrvsl3d, minamtrvsl3d, minamttot3d);
    }
    //stop add (ERE 200313)

    //add
    @Override
    @Transactional
    public void updateMaxAmtParam(String trx, BigDecimal maxamt1h){
        panDao.updateMaxAmtParam(trx, maxamt1h);
    }

    @Override
    @Transactional(readOnly=true)
    public boolean checkPanSenderUsed(String pan, String pengirim) throws Exception {
        /*Add by aaw 22/07/2021, beberapa tran code sender = '' ataupun null, untuk menghindari constraint sender null di db maka set sender ' '*/
        if ("".equals(pengirim.trim())){
            pengirim = " ";
        }
        /*End add*/
        List<WhitelistBean> list = panDao.listPanSender(pan.trim(), pengirim);
        if (list.size() > 0) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    @Transactional(readOnly=true)
    public boolean checkEmailUsed(String useremail) throws Exception {
        List<PanAnomalyEmailBean> list = panDao.listEmail(useremail);
        if (list.size() > 0) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<WhitelistBean> getWhitelistPan(String pan, String pengirim) throws Exception {
        return panDao.getWhitelistPan(pan, pengirim);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyEmailBean> getEmail(String useremail) throws Exception {
        return panDao.getEmail(useremail);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyParamBean> getParam(String trx) throws Exception {
        return panDao.getParam(trx);
    }

    //start add (ERE 200318)
    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyParamBean> getParamBankBene(String trx) throws Exception {
        return panDao.getParamBankBene(trx);
    }
    //stop add (ERE 200318)

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyParamBean> getParamBank(String id) throws Exception {
        return panDao.getParamBank(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyParamBean> getParamBankId(String bank_id) throws Exception {
        return panDao.getParamBankId(bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyParamBean> getParamMaxAmount(String trx) throws Exception {
        return panDao.getParamMaxAmount(trx);
    }

    @Override
    @Transactional(readOnly = true)
    public PanTrxidBean getPanTrxId(String trxId) throws Exception {
        return panDao.getPanTrxId(trxId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyBean> listAnomalyPan1H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return panDao.listPanAnomaly1H(periodfr, periodto, timefr, timeto, trxtype, whitelist, bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyBean> listMaxAmountAnomalyPan1H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return panDao.listMaxAmountAnomalyPan1H(periodfr, periodto, timefr, timeto, trxtype, whitelist, bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyBean> listAnomalyPan3H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return panDao.listPanAnomaly3H(periodfr, periodto, timefr, timeto, trxtype, whitelist, bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyBean> listAnomalyPan6H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return panDao.listPanAnomaly6H(periodfr, periodto, timefr, timeto, trxtype, whitelist, bank_id);
    }

    @Override
     @Transactional(readOnly = true)
     public List<PanAnomalyBean> listAnomalyPan1D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return panDao.listPanAnomaly1D(periodfr, periodto, trxtype, whitelist, bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyBean> listAnomalyPan3D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return panDao.listPanAnomaly3D(periodfr, periodto, trxtype, whitelist, bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyBean> listAnomalyPanBene1H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return panDao.listPanAnomalyBene1H(periodfr, periodto, timefr, timeto, trxtype, whitelist, bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyBean> listAnomalyPanBene3H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return panDao.listPanAnomalyBene3H(periodfr, periodto, timefr, timeto, trxtype, whitelist, bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyBean> listAnomalyPanBene6H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return panDao.listPanAnomalyBene6H(periodfr, periodto, timefr, timeto, trxtype, whitelist, bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyBean> listAnomalyPanBene1D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return panDao.listPanAnomalyBene1D(periodfr, periodto, trxtype, whitelist, bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyBean> listAnomalyPanBene3D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return panDao.listPanAnomalyBene3D(periodfr, periodto, trxtype, whitelist, bank_id);
    }

    @Override
    @Transactional(readOnly = true)
    public String getTrxIdFromTrxName(String trxName) throws Exception {
        return (String) panDao.getTrxIdFromTrxName(trxName);
    }

    @Override
    @Transactional(readOnly = true)
    public String getGroupIdFromTrxName(String trxName) throws Exception {
        return (String) panDao.getGroupIdFromTrxName(trxName);
    }

    /*Add by ait 10/05/21 - 24/05/21*/
    @Transactional(readOnly = true)
    public List<DetailBeneficiaryAnomalyBean> listDetailBeneficiaryAtm(String period, String timeFr, String timeTo, String acct, String tranType, String receiver, String periodTo, String periodType) throws Exception {
        return panDao.listDetailBeneficiaryAtm(period, timeFr, timeTo, acct, tranType, receiver, periodTo, periodType);
    }

    @Override
    @Transactional(readOnly = true)
    public List<WhitelistBeneficiaryBean> listBeneficiary() throws Exception {
        return panDao.listBeneficiary();
    }

    @Override
    @Transactional
    public void deleteBeneficiary(String account, String penerima) throws Exception {
        panDao.deleteBeneficiary(account.trim(), penerima.trim());
    }

    @Override
    @Transactional(readOnly=true)
    public boolean checkBeneficiaryReceiverUsed(String account, String penerima) throws Exception {
        List<WhitelistBeneficiaryBean> list = panDao.listBeneficiaryReceiver(account, penerima);
        if (list.size() > 0) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    @Transactional
    public void saveBeneficiary(String account, String penerima, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree,
        BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) throws Exception {
        panDao.saveBeneficiary(account, penerima, freqApp, freqDc, freqDcFree, freqRvsl, totalAmtApp, totalAmtDc, totalAmtDcFree, totalAmtRvsl, totalAmt, active);
    }

    @Override
    @Transactional(readOnly = true)
    public List<WhitelistBeneficiaryBean> getWhitelistBeneficiary(String account, String penerima) throws Exception {
        return panDao.getWhitelistBeneficiary(account, penerima);
    }

    @Override
    @Transactional
    public void updateBeneficiary(String account, String penerima, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree, BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) throws Exception {
        panDao.updateBeneficiary(account, penerima, freqApp, freqDc, freqDcFree, freqRvsl, totalAmtApp, totalAmtDc, totalAmtDcFree, totalAmtRvsl, totalAmt, active);
    }

    /*End add*/

}

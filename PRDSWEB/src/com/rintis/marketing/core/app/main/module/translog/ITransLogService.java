package com.rintis.marketing.core.app.main.module.translog;


import com.rintis.marketing.beans.entity.TransactionLog;
import com.rintis.marketing.beans.entity.UserLoginHistory;

import java.util.List;
import java.util.Map;

public interface ITransLogService {
    public List<UserLoginHistory> searchUserLoginLog(Map<String, Object> param) throws Exception;
    public List<TransactionLog> searchTransactionLog(Map<String, Object> param) throws Exception;
}

package com.rintis.marketing.core.app.main.module.common;

import com.rintis.marketing.beans.entity.*;

import java.util.List;


public interface ICommonService {
    public String getSystemProperty(String key) throws Exception;
    public SystemProperty getSystemPropertyEntity(String key) throws Exception;
    public void editSystemProperty(SystemProperty systemProperty) throws Exception;
    public void editSystemProperty(List<SystemProperty> listSystemProperty) throws Exception;
    public List<SystemProperty> listSystemProperty() throws Exception;
    public List<SystemProperty> listSystemPropertyNotPerUser(List<String> listExclude) throws Exception;
    public List<SystemProperty> listSystemPropertyPerUser() throws Exception;
    public List<SystemProperty> listSystemPropertyPerUser(String name) throws Exception;

    public SystemPropertyUser getSystemPropertyUserObject(String userId, String key) throws Exception;
    public String getSystemPropertyUser(String userId, String key) throws Exception;
    public String getSystemPropertyUserDefault(String userId, String key) throws Exception;
    public void editSystemPropertyUser(String userId, List<SystemPropertyUser> listSystemProperty) throws Exception;
    public List<SystemPropertyUser> listSystemPropertyUser(String userId) throws Exception;
    public void checkSystemPropertyUser(String userId) throws Exception;

    public Integer getLimitYear(String userId) throws Exception;

    public void createTransLog(TransactionLog tlog) throws Exception;


    public List<Enumeration> listEnumeration(String enumTypeId) throws Exception;
    public Enumeration getEnumeration(Integer id) throws Exception;
    public Enumeration getEnumerationByEnumId(String enumId) throws Exception;
    public Enumeration getEnumerationByDescription(String description) throws Exception;

    public void deleteEmailQueue(Integer id) throws Exception;
    public List<EmailQueue> listEmailQueue() throws Exception;


}

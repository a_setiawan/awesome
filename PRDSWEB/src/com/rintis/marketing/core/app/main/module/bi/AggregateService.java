package com.rintis.marketing.core.app.main.module.bi;


import com.rintis.marketing.beans.entity.TmsAggrLastTime;
import com.rintis.marketing.beans.entity.TmsAggrLastTimePk;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.dao.AggregateDao;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Qualifier(SpringBeanConstant.SPRING_BEAN_AGGREGATE_SERVICE)
@Named(SpringBeanConstant.SPRING_BEAN_AGGREGATE_SERVICE)
public class AggregateService implements IAggregateService {
    private Logger log = Logger.getLogger(this.getClass());
    @Autowired
    private AggregateDao aggregateDao;


    @Override
    @Transactional(readOnly = true)
    public TmsAggrLastTime getTmsAggrLastTime(String groupId, String aggrId) throws Exception {
        TmsAggrLastTimePk pk = new TmsAggrLastTimePk();
        pk.setGroupId(groupId);
        pk.setAggrId(aggrId);
        return (TmsAggrLastTime)aggregateDao.get(TmsAggrLastTime.class, pk);
    }

    @Override
    @Transactional(readOnly = true)
    public String getAggrLastTime(String trxId, String aggrId) throws Exception {
        String result = "";
        if (trxId.equalsIgnoreCase(DataConstant.ALL)) {
            TmsAggrLastTime taltAtm = getTmsAggrLastTime(DataConstant.ATM, aggrId);
            TmsAggrLastTime taltPos = getTmsAggrLastTime(DataConstant.POS, aggrId);
            Date datm = DateUtils.convertDate(taltAtm.getLastTimeRnd(), DateUtils.DATE_YYMMDD_HHMM);
            Date dpos = DateUtils.convertDate(taltPos.getLastTimeRnd(), DateUtils.DATE_YYMMDD_HHMM);
            if (datm.getTime() > dpos.getTime()) {
                result = taltPos.getLastTimeRnd();
            } else if (datm.getTime() < dpos.getTime()) {
                result = taltAtm.getLastTimeRnd();
            } else {
                result = taltAtm.getLastTimeRnd();
            }

        } else {
            String groupId = aggregateDao.getGroupIdFromTrxId(trxId);
            //get last_time_rnd
            TmsAggrLastTime talt = getTmsAggrLastTime(groupId, aggrId);
            if (talt != null) {
                result = talt.getLastTimeRnd();
            }
        }
        return result;
    }


}

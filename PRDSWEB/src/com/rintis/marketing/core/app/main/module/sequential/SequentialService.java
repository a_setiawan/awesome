package com.rintis.marketing.core.app.main.module.sequential;

import com.rintis.marketing.beans.bean.bi.PanAnomalyEmailBean;
import com.rintis.marketing.beans.bean.bi.SequentialAnomalyBean;
import com.rintis.marketing.beans.bean.blocked.BlockedAdministrationDetail;
import com.rintis.marketing.beans.bean.blocked.BlockedAdministrationHeader;
import com.rintis.marketing.beans.bean.blocked.PanHistoryBean;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.beans.bean.sequential.SequentialParamBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.dao.SequentialDao;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by aabraham on 20/04/2021.
 */
@Service
@Qualifier(SpringBeanConstant.SPRING_BEAN_SEQUENTIAL_SERVICE)
@Named(SpringBeanConstant.SPRING_BEAN_SEQUENTIAL_SERVICE)
public class SequentialService implements ISequentialService {

    private Logger log = Logger.getLogger(this.getClass());

    @Autowired
    private SequentialDao sequentialDao;

    @Override
    @Transactional(readOnly = true)
    public List<SequentialParamBean> listParam() throws Exception {
        return sequentialDao.listParam();
    }

    @Override
    @Transactional(readOnly = true)
    public List<SequentialParamBean> getParam(String trx) throws Exception {
        return sequentialDao.getParam(trx);
    }

    @Override
    @Transactional
    public void updateParam(String trx, BigDecimal mintrxapp1d, BigDecimal minamtapp1d, BigDecimal mintrxtot1d, BigDecimal minamttot1d) throws Exception {
        sequentialDao.updateParam(trx, mintrxapp1d, minamtapp1d, mintrxtot1d, minamttot1d);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanTrxidBean> listTrxId() throws Exception {
        return sequentialDao.listTrxId();
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanTrxidBean> listTrxIdBlocked() throws Exception {
        return sequentialDao.listTrxIdBlocked();
    }

    @Override
    @Transactional(readOnly = true)
    public List<SequentialAnomalyBean> listSequentialAnomaly(String periodFr, String periodTo, String trxtype, String bank_id) throws Exception {
        return sequentialDao.listSequentialAnomaly(periodFr, periodTo, trxtype, bank_id);
    }

    @Override
    @Transactional(readOnly=true)
    public boolean checkPanBankUsed(String pan, String bankid) throws Exception {
        List<SequentialAnomalyBean> list = sequentialDao.listPanBank(pan.trim(), bankid);
        if (list.size() > 0) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    @Transactional
    public void saveBlockedPanHeader(String docId, Date createDate, String createBy, Date statusDate, String status, String notes) throws Exception {
        sequentialDao.saveBlockedPanHeader(docId, createDate, createBy, statusDate, status, notes);
    }

    @Override
    @Transactional
    public void saveBlockedPanDetail(String docId, String pan, String bank_id, String blocked_to, String notes) throws Exception {
        sequentialDao.saveBlockedPanDetail(docId, pan, bank_id, blocked_to, notes);
    }

    @Override
    @Transactional
    public void updatePan(String pan, String status) throws Exception {
        sequentialDao.updatePan(pan, status);
    }

    @Override
    @Transactional(readOnly = true)
    public String getDocId(String date, String kode) throws Exception {
        return (String) sequentialDao.getDocId(date, kode);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BlockedAdministrationHeader> listBlockedAdministrationHeader(String docId) throws Exception {
        return sequentialDao.listBlockedAdministrationHeader(docId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BlockedAdministrationHeader> listBlockedAdministrationHeaderAll() throws Exception {
        return sequentialDao.listBlockedAdministrationHeaderAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<BlockedAdministrationDetail> listBlockedAdministrationDetail(String docId) throws Exception {
        return sequentialDao.listBlockedAdministrationDetail(docId);
    }

    @Override
    @Transactional
    public void deleteBlockedAdministrationDetail(String docId, String pan) throws Exception {
        sequentialDao.deleteBlockedAdministrationDetail(docId, pan);
        sequentialDao.updatePan(pan, "");
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanAnomalyEmailBean> getEmail() throws Exception {
        return sequentialDao.getEmail();
    }

    @Override
    @Transactional
    public void deleteBlockedAdministrationDetail(String docId) throws Exception {
        sequentialDao.deleteBlockedAdministrationDetail(docId);
    }

    @Override
    @Transactional
    public void deleteBlockedAdministrationHeader(String docId) throws Exception {
        sequentialDao.deleteBlockedAdministrationHeader(docId);
    }

    @Override
    @Transactional
    public void updatePanDetail(String docId, String status) throws Exception {
        sequentialDao.updatePanDetail(docId, status);
    }

    @Override
    @Transactional
    public void addPanBlockedHistory(String docId, String datetime, String user_id, String status) throws Exception {
        sequentialDao.addPanBlockedHistory(docId, datetime, user_id, status);
    }

    @Override
     @Transactional(readOnly = true)
     public List<BlockedAdministrationHeader> listDocBlocked(String status) throws Exception {
        return sequentialDao.listDocBlocked(status);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BlockedAdministrationHeader> listDocUnblocked(String status) throws Exception {
        return sequentialDao.listDocUnblocked(status);
    }

    @Override
    @Transactional
    public void insertPgw(String docId) throws Exception {
        sequentialDao.insertPgw(docId);
    }

    @Override
    @Transactional
    public void insertB24(String docId) throws Exception {
        sequentialDao.insertB24(docId);
    }

    @Override
    @Transactional
    public void updateBlockedHeader(String docId, String datetime, String status) throws Exception {
        sequentialDao.updateBlockedHeader(docId, datetime, status);
    }

    @Override
    @Transactional
    public void updateBlockedNotesHeader(String docId, String notes) throws Exception {
        sequentialDao.updateBlockedNotesHeader(docId, notes);
    }

    @Override
    @Transactional
    public void addPanBlockedApproval(String docId, String datetime, String user_id, String status) throws Exception {
        sequentialDao.addPanBlockedApproval(docId, datetime, user_id, status);
    }

    @Override
    @Transactional(readOnly = true)
    public List<SequentialAnomalyBean> listUnblockedDetail() throws Exception {
        return sequentialDao.listUnblockedDetail();
    }

    @Override
    @Transactional
    public void deletePgw(String docId) throws Exception {
        sequentialDao.deletePgw(docId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PanHistoryBean> listHistoryPan() throws Exception {
        return sequentialDao.listHistoryPan();
    }

    @Override
    @Transactional
    public void updatePanDetailManual(String docId, String status) throws Exception {
        sequentialDao.updatePanDetailManual(docId, status);
    }

    @Override
    @Transactional
    public void deletePanDetailManual(String bankId, String pan) throws Exception {
        sequentialDao.deletePanDetailManual(bankId, pan);
    }

    @Override
    @Transactional
    public void savePanDetailManual(String trxType, String bank_id, String pan, String status) throws Exception {
        sequentialDao.savePanDetailManual(trxType, bank_id, pan, status);
    }

    @Override
    @Transactional(readOnly = true)
    public String getEmailDocId(String docId) throws Exception {
        return (String) sequentialDao.getEmailDocId(docId);
    }

    @Override
    @Transactional(readOnly = true)
    public String getApprovedBy(String docId) throws Exception {
        return (String) sequentialDao.getApprovedBy(docId);
    }

    @Override
    @Transactional(readOnly = true)
    public BigDecimal getCheckPan(String pan) throws Exception {
        return (BigDecimal) sequentialDao.getCheckPan(pan);
    }

    @Override
    @Transactional(readOnly = true)
    public BigDecimal getCheckPanManual(String pan) throws Exception {
        return (BigDecimal) sequentialDao.getCheckPanManual(pan);
    }
}

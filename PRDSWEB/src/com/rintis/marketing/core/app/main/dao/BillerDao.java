package com.rintis.marketing.core.app.main.dao;

import com.rintis.marketing.beans.bean.biller.BillerAnomalyBean;
import com.rintis.marketing.beans.bean.biller.DetailBillerAnomalyBean;
import com.rintis.marketing.beans.bean.biller.TmsBillerBean;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.beans.entity.BillerAnomalyInstitusiParam;
import com.rintis.marketing.beans.entity.BillerDummy;
import com.rintis.marketing.core.SpringBeanConstant;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.util.List;

/*Add by aaw 20211110*/

@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_BILLERD_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_BILLERD_DAO)
public class BillerDao extends AbstractDao {

    @Transactional(readOnly = true)
    public List<PanTrxidBean> listTrxIdBiller(String trx) throws Exception {

        String sql = "" +
                "select trx_id as \"trxid\",  trx_name as \"trxname\" \n" +
                "from tms_TrxId " +
                "where trx_id in (" + trx + ")" +
                "order by trx_id ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanTrxidBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listBiller1H(String periodFr, String periodTo, String timeFr, String timeTo, String tranType, Boolean whitelist, String instCd){
        String sql = "select \n" +
                "tt.period as \"period\", \n" +
                "to_char(to_date(tt.period,'YYMMDD'), 'DD-MM-YYYY') as \"periodDisplay\", \n" +
                "tt.time_fr_1h as \"timeFr\", \n" +
                "tt.time_to_1h as \"timeTo\", \n" +
                "tt.inst_cd as \"instCd\", \n" +
                "tt.inst_cd || ' - ' || biller_name_active(inst_cd) as \"instName\", \n" +
                "tt.cust_no as \"custNo\", \n" +
                "tt.cust_name as \"custName\", \n" +
                "tt.freq_app as \"freqApp\", \n" +
                "tt.freq_dc as \"freqDc\", \n" +
                "tt.freq_dc_free as \"freqDcFree\", \n" +
                "nvl(tt.total_amt_app/100,0) as \"totalAmtApp\", \n" +
                "nvl(tt.total_amt_dc/100,0) as \"totalAmtDc\", \n" +
                "nvl(tt.total_amt_dc_free/100,0) as \"totalAmtDcFree\", \n" +
                "nvl(tt.total_amt_app/100,0) + nvl(tt.total_amt_dc_free/100,0) as \"totalAmt\", \n" +

                "tms_billerdummy_check_exist(trim(tt.inst_cd), trim(tt.cust_no)) as \"chkColor\" \n" +

                "from \n" +
                "tms_trxanmly_biller_acq_1hour tt \n" +
                "join \n" +
                "tms_trxid ti on ti.trx_id = tt.tran_type \n" +
                "where \n" +
                "tt.period >= :periodFr \n" +
                "and tt.period <= :periodTo \n" +
                "and substr(tt.time_fr_1h,1,2) || substr(tt.time_fr_1h,4,2) >= :timeFr \n" +
                "and replace(substr(tt.time_to_1h,1,2),'00','24') || substr(tt.time_to_1h,4,2) <= :timeTo and tt.tran_type = :tranType \n";

        if(instCd != ""){
            sql = sql + "and tt.inst_cd = '"+ instCd +"' \n";
        }

        if(!whitelist) {
            sql = sql + " and tms_billerdummy_status(trim(tt.inst_cd), trim(tt.cust_no), nvl(tt.freq_app,0), nvl(tt.freq_dc_free,0), nvl(tt.total_amt_app/100,0),\n" +
                    "nvl(tt.total_amt_dc_free/100,0),\n" +
                    "nvl(tt.total_amt_app/100,0) + nvl(tt.total_amt_dc_free/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by tt.period, tt.time_fr_1h, tt.time_fr_1h, tt.inst_cd, tt.cust_no";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodFr", periodFr);
        query.setString("periodTo", periodTo);
        query.setString("timeFr", timeFr);
        query.setString("timeTo", timeTo);
        query.setString("tranType", tranType);

        query.setResultTransformer(new AliasToBeanResultTransformer(BillerAnomalyBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listBiller3H(String periodFr, String periodTo, String timeFr, String timeTo, String tranType, Boolean whitelist, String instCd){
        String sql = "select \n" +
                "tt.period as \"period\", \n" +
                "to_char(to_date(tt.period,'YYMMDD'), 'DD-MM-YYYY') as \"periodDisplay\", \n" +
                "tt.time_fr_3h as \"timeFr\", \n" +
                "tt.time_to_3h as \"timeTo\", \n" +
                "tt.inst_cd as \"instCd\", \n" +
                "tt.inst_cd || ' - ' || biller_name_active(inst_cd) as \"instName\", \n" +
                "tt.cust_no as \"custNo\", \n" +
                "tt.cust_name as \"custName\", \n" +
                "tt.freq_app as \"freqApp\", \n" +
                "tt.freq_dc as \"freqDc\", \n" +
                "tt.freq_dc_free as \"freqDcFree\", \n" +
                "nvl(tt.total_amt_app/100,0) as \"totalAmtApp\", \n" +
                "nvl(tt.total_amt_dc/100,0) as \"totalAmtDc\", \n" +
                "nvl(tt.total_amt_dc_free/100,0) as \"totalAmtDcFree\", \n" +
                "nvl(tt.total_amt_app/100,0) + nvl(tt.total_amt_dc_free/100,0) as \"totalAmt\", \n" +

                "tms_billerdummy_check_exist(trim(tt.inst_cd), trim(tt.cust_no)) as \"chkColor\" \n" +

                "from \n" +
                "tms_trxanmly_biller_acq_3hour tt \n" +
                "join \n" +
                "tms_trxid ti on ti.trx_id = tt.tran_type \n" +
                "where \n" +
                "tt.period >= :periodFr \n" +
                "and tt.period <= :periodTo \n" +
                "and substr(tt.time_fr_3h,1,2) || substr(tt.time_fr_3h,4,2) >= :timeFr \n" +
                "and replace(substr(tt.time_to_3h,1,2),'00','24') || substr(tt.time_to_3h,4,2) <= :timeTo and tt.tran_type = :tranType \n";

        if(instCd != ""){
            sql = sql + "and tt.inst_cd = '"+ instCd +"' \n";
        }

        if(!whitelist) {
            sql = sql + " and tms_billerdummy_status(trim(tt.inst_cd), trim(tt.cust_no), nvl(tt.freq_app,0), nvl(tt.freq_dc_free,0), nvl(tt.total_amt_app/100,0),\n" +
                    "nvl(tt.total_amt_dc_free/100,0),\n" +
                    "nvl(tt.total_amt_app/100,0) + nvl(tt.total_amt_dc_free/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by tt.period, tt.time_fr_3h, tt.time_fr_3h, tt.inst_cd, tt.cust_no";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodFr", periodFr);
        query.setString("periodTo", periodTo);
        query.setString("timeFr", timeFr);
        query.setString("timeTo", timeTo);
        query.setString("tranType", tranType);

        query.setResultTransformer(new AliasToBeanResultTransformer(BillerAnomalyBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listBiller6H(String periodFr, String periodTo, String timeFr, String timeTo, String tranType, Boolean whitelist, String instCd){
        String sql = "select \n" +
                "tt.period as \"period\", \n" +
                "to_char(to_date(tt.period,'YYMMDD'), 'DD-MM-YYYY') as \"periodDisplay\", \n" +
                "tt.time_fr_6h as \"timeFr\", \n" +
                "tt.time_to_6h as \"timeTo\", \n" +
                "tt.inst_cd as \"instCd\", \n" +
                "tt.inst_cd || ' - ' || biller_name_active(inst_cd) as \"instName\", \n" +
                "tt.cust_no as \"custNo\", \n" +
                "tt.cust_name as \"custName\", \n" +
                "tt.freq_app as \"freqApp\", \n" +
                "tt.freq_dc as \"freqDc\", \n" +
                "tt.freq_dc_free as \"freqDcFree\", \n" +
                "nvl(tt.total_amt_app/100,0) as \"totalAmtApp\", \n" +
                "nvl(tt.total_amt_dc/100,0) as \"totalAmtDc\", \n" +
                "nvl(tt.total_amt_dc_free/100,0) as \"totalAmtDcFree\", \n" +
                "nvl(tt.total_amt_app/100,0) + nvl(tt.total_amt_dc_free/100,0) as \"totalAmt\", \n" +

                "tms_billerdummy_check_exist(trim(tt.inst_cd), trim(tt.cust_no)) as \"chkColor\" \n" +

                "from \n" +
                "tms_trxanmly_biller_acq_6hour tt \n" +
                "join \n" +
                "tms_trxid ti on ti.trx_id = tt.tran_type \n" +
                "where \n" +
                "tt.period >= :periodFr \n" +
                "and tt.period <= :periodTo \n" +
                "and substr(tt.time_fr_6h,1,2) || substr(tt.time_fr_6h,4,2) >= :timeFr \n" +
                "and replace(substr(tt.time_to_6h,1,2),'00','24') || substr(tt.time_to_6h,4,2) <= :timeTo and tt.tran_type = :tranType \n";

        if(instCd != ""){
            sql = sql + "and tt.inst_cd = '"+ instCd +"' \n";
        }

        if(!whitelist) {
            sql = sql + " and tms_billerdummy_status(trim(tt.inst_cd), trim(tt.cust_no), nvl(tt.freq_app,0), nvl(tt.freq_dc_free,0), nvl(tt.total_amt_app/100,0),\n" +
                    "nvl(tt.total_amt_dc_free/100,0),\n" +
                    "nvl(tt.total_amt_app/100,0) + nvl(tt.total_amt_dc_free/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by tt.period, tt.time_fr_6h, tt.time_fr_6h, tt.inst_cd, tt.cust_no";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodFr", periodFr);
        query.setString("periodTo", periodTo);
        query.setString("timeFr", timeFr);
        query.setString("timeTo", timeTo);
        query.setString("tranType", tranType);

        query.setResultTransformer(new AliasToBeanResultTransformer(BillerAnomalyBean.class));

        return query.list();
    }

    @Transactional(readOnly=true)
    public List listBiller1D(String periodFr, String periodTo, String tranType, Boolean whitelist, String instCd){
        String sql = "select \n" +
                "period as \"period\", \n" +
                "to_char(to_date(tt.period,'YYMMDD'), 'DD-MM-YYYY') as \"periodDisplay\", \n" +
                "tt.inst_cd as \"instCd\", \n" +
                "tt.inst_cd || ' - ' || biller_name_active(inst_cd) as \"instName\", \n" +
                "tt.cust_no as \"custNo\", \n" +
                "tt.cust_name as \"custName\", \n" +
                "tt.freq_app as \"freqApp\", \n" +
                "tt.freq_dc as \"freqDc\", \n" +
                "tt.freq_dc_free as \"freqDcFree\", \n" +
                "nvl(tt.total_amt_app/100,0) as \"totalAmtApp\", \n" +
                "nvl(tt.total_amt_dc/100,0) as \"totalAmtDc\", \n" +
                "nvl(tt.total_amt_dc_free/100,0) as \"totalAmtDcFree\", \n" +
                "nvl(tt.total_amt_app/100,0) + nvl(tt.total_amt_dc_free/100,0) as \"totalAmt\", \n" +

                "tms_billerdummy_check_exist(trim(tt.inst_cd), trim(tt.cust_no)) as \"chkColor\" \n" +

                "from \n" +
                "tms_trxanmly_biller_acq_1day tt \n" +
                "join \n" +
                "tms_trxid ti on ti.trx_id = tt.tran_type \n" +
                "where \n" +
                "tt.period between :periodFr and :periodTo and tt.tran_type = :tranType \n";

        if(instCd != ""){
            sql = sql + "and tt.inst_cd = '"+ instCd +"' \n";
        }

        if(!whitelist) {
            sql = sql + " and tms_billerdummy_status(trim(tt.inst_cd), trim(tt.cust_no), nvl(tt.freq_app,0), nvl(tt.freq_dc_free,0), nvl(tt.total_amt_app/100,0),\n" +
                    "nvl(tt.total_amt_dc_free/100,0),\n" +
                    "nvl(tt.total_amt_app/100,0) + nvl(tt.total_amt_dc_free/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by tt.period, tt.inst_cd, tt.cust_no";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodFr", periodFr);
        query.setString("periodTo", periodTo);
        query.setString("tranType", tranType);

        query.setResultTransformer(new AliasToBeanResultTransformer(BillerAnomalyBean.class));

        return query.list();
    }

    @Transactional(readOnly=true)
    public List listBiller3D(String periodFr, String periodTo, String tranType, Boolean whitelist, String instCd){
        String sql = "select \n" +
                "to_char(to_date(tt.period_fr, 'YYMMDD'), 'DD-MM-YYYY') as \"period\",\n" +
                "to_char(to_date(tt.period_to, 'YYMMDD'), 'DD-MM-YYYY') as \"periodDisplay\",\n" +
                "tt.period_fr as \"periodFrom\", \n" +
                "tt.period_to as \"periodTo\", \n" +
                "tt.inst_cd as \"instCd\", \n" +
                "tt.inst_cd || ' - ' || biller_name_active(inst_cd) as \"instName\", \n" +
                "tt.cust_no as \"custNo\", \n" +
                "tt.cust_name as \"custName\", \n" +
                "tt.freq_app as \"freqApp\",\n" +
                "tt.freq_dc as \"freqDc\",\n" +
                "tt.freq_dc_free as \"freqDcFree\",\n" +
                "nvl(tt.total_amt_app / 100, 0) as \"totalAmtApp\",\n" +
                "nvl(tt.total_amt_dc / 100, 0) as \"totalAmtDc\",\n" +
                "nvl(tt.total_amt_dc_free / 100, 0) as \"totalAmtDcFree\",\n" +
                "nvl(tt.total_amt_app / 100, 0) + nvl(tt.total_amt_dc_free / 100, 0) AS \"totalAmt\",\n" +

                "tms_billerdummy_check_exist(trim(tt.inst_cd), trim(tt.cust_no)) as \"chkColor\" \n" +

                "from\n" +
                "tms_trxanmly_biller_acq_3day tt \n" +
                "join \n" +
                "tms_trxid ti on ti.trx_id = tt.tran_type \n" +
                "where\n" +
                "tt.period_fr >= :periodFr\n" +
                "and tt.period_to <= :periodTo and tt.tran_type = :tranType \n";

        if(instCd != ""){
            sql = sql + "and tt.inst_cd = '"+ instCd +"' \n";
        }

        if(!whitelist) {
            sql = sql + " and tms_billerdummy_status(trim(tt.inst_cd), trim(tt.cust_no), nvl(tt.freq_app,0), nvl(tt.freq_dc_free,0), nvl(tt.total_amt_app/100,0),\n" +
                    "nvl(tt.total_amt_dc_free/100,0),\n" +
                    "nvl(tt.total_amt_app/100,0) + nvl(tt.total_amt_dc_free/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by tt.period_fr, tt.inst_cd, tt.cust_no";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodFr", periodFr);
        query.setString("periodTo", periodTo);
        query.setString("tranType", tranType);

        query.setResultTransformer(new AliasToBeanResultTransformer(BillerAnomalyBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listDetailBiller(String period, String timeFr, String timeTo, String instCode, String custNo, String custName, String tranType, String periodType,  String periodFrom, String periodTo){
        String sql = "select \n" +
                "to_char(sys_trx_dt,'DD-MM-YYYY HH24:MI:SS') as \"dateTime\", \n" +
                "actn_from_id || ' - ' || tms_bank_code(actn_from_id) as \"issuer\", \n" +
                "trx_typ_cd_1 as \"productCode\", \n" +
                "audit_no as \"stan\", \n" +
                "to_char(actn_by_trmnl) as \"termId\", \n" +
                "channel_cd AS \"channelId\", \n" +
                "nvl(initiator_ref_no, ' ') AS \"refNo\", \n" +
                "nvl(host_ref_no, ' ') AS \"billerNo\", \n" +
                "nvl((case when tms_trxid_map_api.get_trx_id(trx_typ_cd_1) = '37' then amt when tms_trxid_map_api.get_trx_id(trx_typ_cd_1) = '17' then paid_amt end), 0) as \"amount\", \n" +
                "to_char(err_map_cd) AS \"rc\", \n" +
                "(case when err_map_cd = '00' or err_map_cd = '68' then 'Approve' else 'Decline' end) as \"description\" \n" +
                "from \n" +
                "psh_act_log_bs@link_pqs \n" +
                "where \n" +
                "trunc(sys_trx_dt) >= to_date('2021-11-08', 'YYYY-MM-DD') \n" +
                "and tms_trxid_map_api.get_trx_id(trx_typ_cd_1) = :tranType  \n" +
                "and inst_cd = :instCode and cust_no = :custNo \n";

        if (!" ".equals(custName)) {
            sql = sql + " and cust_nm = :custName \n";
        } else {
            sql = sql + " and cust_nm is null \n";
        }

        if ("1D".equalsIgnoreCase(periodType) || "3D".equalsIgnoreCase(periodType)){
            sql = sql  + "and to_char(sys_trx_dt, 'yyyymmddhh24miss') between to_char(to_date(:periodFrom,'yymmdd'), 'yyyymmdd') || '00000000' || '00' and to_char(to_date(:periodTo,'yymmdd'), 'yyyymmdd') || '24000000' || '00' \n";
        } else {
            sql = sql  + "and to_char(sys_trx_dt, 'yyyymmddhh24miss') between to_char(to_date(:period,'yymmdd'), 'yyyymmdd') || :timeFr || '00' AND to_char(to_date(:period,'yymmdd'), 'yyyymmdd') || :timeTo || '00' \n";
        }

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.addScalar("dateTime", new StringType());
        query.addScalar("issuer", new StringType());
        query.addScalar("productCode", new StringType());
        query.addScalar("stan", new StringType());
        query.addScalar("termId", new StringType());
        query.addScalar("channelId", new StringType());
        query.addScalar("refNo", new StringType());
        query.addScalar("billerNo", new StringType());
        query.addScalar("amount", new BigDecimalType());
        query.addScalar("rc", new StringType());
        query.addScalar("description", new StringType());

        if(!"".equals(timeFr)) {
            if (timeFr.replace(":", "").length() != 6) {
                query.setString("period", period);
                query.setString("timeFr", timeFr.replace(":", ""));
                query.setString("timeTo", timeTo.replace(":", ""));
            }
        }

        query.setString("tranType", tranType);
        query.setString("instCode", instCode);
        query.setString("custNo", custNo);
        if (!" ".equals(custName)) {
            query.setString("custName", custName);
        }

        if ("1D".equalsIgnoreCase(periodType) || "3D".equalsIgnoreCase(periodType)){
            query.setString("periodFrom", periodFrom);
            query.setString("periodTo", periodTo);
        }
        query.setResultTransformer(new AliasToBeanResultTransformer(DetailBillerAnomalyBean.class));
        return query.list();
    }

    @Transactional(readOnly = true)
    public TmsBillerBean getDataBiller(String billerId) throws Exception {
        String sql = "" +
                "select biller_id as \"billerId\",  biller_fiid as \"billerFiid\", biller_name as \"billerName\", biller_active_status as \"activeStatus\" \n" +
                "from tms_biller " +
                "where biller_id = " + billerId + "\n" +
                "order by biller_id ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(TmsBillerBean.class));
        return (TmsBillerBean)query.uniqueResult();
    }

    @Transactional(readOnly = true)
    public BillerDummy checkExistTreshold(String instCd, String custNo) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(BillerDummy.class);
        criteria.add(Restrictions.eq("instCd", instCd));
        criteria.add(Restrictions.eq("custNo", custNo));
        return (BillerDummy) criteria.uniqueResult();
    }

    @Transactional(readOnly = true)
    public List<Object> listWhitelistBiller() throws Exception {
        return findAll(BillerDummy.class, "instCd", true);
    }

    @Transactional(readOnly = true)
    public List<TmsBillerBean> allDataBillerActive() throws Exception {
        String sql = "" +
                "select biller_id as \"billerId\",  biller_fiid as \"billerFiid\", biller_name as \"billerName\", biller_active_status as \"activeStatus\" \n" +
                "from tms_biller " +
                "where lower(biller_active_status) =  'yes' \n" +
                "order by biller_id ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(TmsBillerBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List <BillerAnomalyInstitusiParam>listBillerInstitusiParameter(){
        String sql = "select \n" +
                "ti.trx_id as \"trxIdTransient\", \n" +
                "ti.trx_name as \"trxNameTransient\", \n" +
                "'' as \"minTrxApp1h\", \n" +
                "'' as \"minTrxDc1h\", \n" +
                "'' as \"minTrxDcFree1h\", \n" +
                "'' as \"minTrxApp3h\", \n" +
                "'' as \"minTrxDc3h\", \n" +
                "'' as \"minTrxDcFree3h\", \n" +
                "'' as \"minTrxApp6h\", \n" +
                "'' as \"minTrxDc6h\", \n" +
                "'' as \"minTrxDcFree6h\", \n" +
                "'' as \"minTrxApp1d\", \n" +
                "'' as \"minTrxDc1d\", \n" +
                "'' as \"minTrxDcFree1d\", \n" +
                "'' as \"minTrxApp3d\", \n" +
                "'' as \"minTrxDc3d\", \n" +
                "'' as \"minTrxDcFree3d\", \n" +
                "'' as \"minAmtApp1h\", \n" +
                "'' as \"minAmtDc1h\", \n" +
                "'' as \"minAmtDcFree1h\", \n" +
                "'' as \"minAmtApp3h\", \n" +
                "'' as \"minAmtDc3h\", \n" +
                "'' as \"minAmtDcFree3h\", \n" +
                "'' as \"minAmtApp6h\", \n" +
                "'' as \"minAmtDc6h\", \n" +
                "'' as \"minAmtDcFree6h\", \n" +
                "'' as \"minAmtApp1d\", \n" +
                "'' as \"minAmtDc1d\", \n" +
                "'' as \"minAmtDcFree1d\", \n" +
                "'' as \"minAmtApp3d\", \n" +
                "'' as \"minAmtDc3d\", \n" +
                "'' as \"minAmtDcFree3d\" \n" +
                "from tms_param_anmly_biller tp join tms_trxid ti on ti.trx_id = tp.trx_id where tp.trx_id in ('17', '37') order by tp.trx_id";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(BillerAnomalyInstitusiParam.class));
        return query.list();
    }

    @Transactional(readOnly = true)
    public List<BillerAnomalyInstitusiParam> fetchListBillerInsitusiParam(String instCd) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(BillerAnomalyInstitusiParam.class);
        criteria.add(Restrictions.eq("instCd", instCd));
        return criteria.list();
    }

    @Transactional(readOnly=true)
    public List<BillerAnomalyInstitusiParam> fetchDataBillerParamInstitusiByID(String instCd) {
        String sql = "select \n" +
                "tp.trx_id as \"trxId\", \n" +
                "ti.trx_name as \"trxNameTransient\", \n" +
                "tp.min_trx_app_1h as \"minTrxApp1h\", \n" +
                "tp.min_trx_dc_1h as \"minTrxDc1h\", \n" +
                "tp.min_trx_dc_free_1h as \"minTrxDcFree1h\", \n" +
                "tp.min_trx_app_3h as \"minTrxApp3h\", \n" +
                "tp.min_trx_dc_3h as \"minTrxDc3h\", \n" +
                "tp.min_trx_dc_free_3h as \"minTrxDcFree3h\", \n" +
                "tp.min_trx_app_6h as \"minTrxApp6h\", \n" +
                "tp.min_trx_dc_6h as \"minTrxDc6h\", \n" +
                "tp.min_trx_dc_free_6h as \"minTrxDcFree6h\", \n" +
                "tp.min_trx_app_1d as \"minTrxApp1d\", \n" +
                "tp.min_trx_dc_1d as \"minTrxDc1d\", \n" +
                "tp.min_trx_dc_free_1d as \"minTrxDcFree1d\", \n" +
                "tp.min_trx_app_3d as \"minTrxApp3d\", \n" +
                "tp.min_trx_dc_3d as \"minTrxDc3d\", \n" +
                "tp.min_trx_dc_free_3d as \"minTrxDcFree3d\", \n" +
                "tp.min_amt_app_1h  as \"minAmtApp1h\", \n" +
                "tp.min_amt_dc_1h as \"minAmtDc1h\", \n" +
                "tp.min_amt_dc_free_1h as \"minAmtDcFree1h\", \n" +
                "tp.min_amt_app_3h as \"minAmtApp3h\", \n" +
                "tp.min_amt_dc_3h as \"minAmtDc3h\", \n" +
                "tp.min_amt_dc_free_3h as \"minAmtDcFree3h\", \n" +
                "tp.min_amt_app_6h as \"minAmtApp6h\", \n" +
                "tp.min_amt_dc_6h as \"minAmtDc6h\", \n" +
                "tp.min_amt_dc_free_6h as \"minAmtDcFree6h\", \n" +
                "tp.min_amt_app_1d as \"minAmtApp1d\", \n" +
                "tp.min_amt_dc_1d as \"minAmtDc1d\", \n" +
                "tp.min_amt_dc_free_1d as \"minAmtDcFree1d\", \n" +
                "tp.min_amt_app_3d as \"minAmtApp3d\", \n" +
                "tp.min_amt_dc_3d as \"minAmtDc3d\", \n" +
                "tp.min_amt_dc_free_3d as \"minAmtDcFree3d\" \n" +
                "from tms_param_anmly_per_institusi tp join tms_trxid ti on ti.trx_id = tp.trx_id where tp.inst_cd = :instCd and tp.trx_id in ('17', '37')";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("instCd", instCd);
        query.setResultTransformer(new AliasToBeanResultTransformer(BillerAnomalyInstitusiParam.class));
        return query.list();
    }
}
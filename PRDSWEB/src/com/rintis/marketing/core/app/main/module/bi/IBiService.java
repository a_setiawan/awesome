package com.rintis.marketing.core.app.main.module.bi;

import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.beans.bean.bi.*;
import com.rintis.marketing.beans.entity.*;
import com.rintis.marketing.core.constant.DataConstant;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

public interface IBiService {
    public List<EsqTrx5MinBean> listEsqTrx5Min(Map<String, Object> param) throws Exception;
    public EsqTrx5MinBean getEsqTrx5Min(String bankId, String trxId, String period, String hourfr, String hourto,
                                        String timefr, String timeto) throws Exception;
    public EsqTrx5MinBean getEsqTrxHourly(String bankId, String trxId, String period, String hourfr, String hourto) throws Exception;
    public EsqTrx5MinBean getEsqTrx5MinWithPrev(String bankId, String trxId, String period, String prevPeriod, String hourfr, String hourto,
                                        String timefr, String timeto) throws Exception;
    public EsqTrx5MinBean getEsqTrxHourlyWithPrev(String bankId, String trxId, String period, String prevPeriod, String hourfr, String hourto) throws Exception;
    public EsqTrx5MinBean getEsqTrxRekap(String acqId, String issId, String bnfId, String trxId, String period, String hourfr, String hourto) throws Exception;

    public List<BankAlertThresholdBean> listBankAlertThreshold(String trxId, String trxName, String sortby) throws Exception;
    public void updateBankAlertThreshold(String trxId, List<BankAlertThresholdBean> list) throws Exception;
    public List<AlertThresholdBean> listAlertThresholdHeader() throws Exception;
    public List<AlertThreshold> listAlertThresholdDetail(String bankId, String trxId) throws Exception;
    public List<AlertThreshold> listAlertThresholdDetail() throws Exception;
    public AlertThreshold getAlertThreshold(String bankId, String trxId, String trxIndicId) throws Exception;
    public void createAlertThreshold(List<AlertThreshold> list) throws Exception;
    public void editAlertThreshold(List<AlertThreshold> list) throws Exception;
    public void deleteAlertThreshold(String bankId, String trxId) throws Exception;
    public void deleteAlertThreshold(String bankId, String trxId, String trxIndicId) throws Exception;
    public List<AlertAnomaly> searchAlertAnomaly(Map<String, Object> param) throws Exception;
    public void alertAnomalySetStatus(BigInteger id, int status) throws Exception;
    public AlertAnomaly getAlertAnomaly(BigInteger id) throws Exception;

    //public List<AlertAnomalyHourly> searchAlertAnomalyHourly(Map<String, Object> param) throws Exception;
    //public void alertAnomalyHourlySetStatus(BigInteger id, int status) throws Exception;
    //public AlertAnomalyHourly getAlertAnomalyHourly(BigInteger id) throws Exception;

    public void checkAlertAnomaly5Min() throws Exception;
    public void checkAlertAnomaly5MinAlertThreshold() throws Exception;
    //public void checkAlertAnomalyHourly() throws Exception;

    //public void updateMasterAcqIssBnf() throws Exception;
    //public EsqMasterAcqIssBnf getEsqMasterAcqIssBnf(EsqMasterAcqIssBnfPk pk) throws Exception;
    //public void checkAcqIssBnfTrx() throws Exception;

    public List<MktUserDashboard> listUserDashboard(String userId) throws Exception;
    public List<MktUserDashboardDetail> listUserDashboardDetailEntity(BigDecimal id) throws Exception;
    public MktUserDashboard getUserDashboard(BigDecimal id) throws Exception;
    public MktUserDashboardDetail getUserDashboardDetail(MktUserDashboardDetailPk pk) throws Exception;
    public BigDecimal createUserDashboard(MktUserDashboard userDashboard, List<MktUserDashboardDetail> listDetail, UserInfoBean userInfoBean) throws Exception;
    public void updateUserDashboard(MktUserDashboard userDashboard, List<MktUserDashboardDetail> listDetail) throws Exception;
    public void deleteUserDashboard(BigDecimal id, UserInfoBean userInfoBean) throws Exception;
    public List<UserDashboardDetailBean> listUserDashboardDetailBean(BigDecimal id) throws Exception;
    public List<DashboardRoleBean> listDashboardByRole(Integer roleId) throws Exception;
    public List<DashboardRole> checkDashboardRole(Integer roleId) throws Exception;
    public void updateDashboardRole(Integer roleId, List<DashboardRoleBean> listDashboard) throws Exception;
    public List<DashboardRole> listDashboardRole(Integer roleId) throws Exception;

    //190524 Added by AAH
    public List<DashboardRoleBean> listDashboardRoleNew(Integer roleId) throws Exception;

    public EsqTrx5MinBean getEsqTrx(String tableName, String bankId, String trxId, String period, String timefr, String timeto) throws Exception;
    public EsqTrx5MinBean getEsqTrxWithPrev(String tableName,String bankId, String trxId, String period, String periodPrev,
                                                String timefr, String timeto) throws Exception;
}

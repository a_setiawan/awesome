package com.rintis.marketing.core.app.main.module.bi;


import com.rintis.marketing.beans.entity.TmsAggrLastTime;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface IAggregateService {


    public TmsAggrLastTime getTmsAggrLastTime(String groupId, String aggrId) throws Exception;
    public String getAggrLastTime(String trxId, String aggrId) throws Exception;

}

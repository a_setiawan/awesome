package com.rintis.marketing.core.app.main.dao;

import com.rintis.marketing.beans.bean.bi.DetailMPanAnomalyBean;
import com.rintis.marketing.beans.bean.bi.QRAnomalyBean;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.config.spring.ReadProperty;
import com.rintis.marketing.core.SpringBeanConstant;
import org.hibernate.SQLQuery;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by aaw on 16/06/2021.
 */

@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_QR_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_QR_DAO)
public class QRDao extends AbstractDao{

    @Transactional(readOnly = true)
    public List<PanTrxidBean> listTrxIdBene(String param) throws Exception {

        String sql = "" +
                "select trx_id as \"trxid\",  trx_name as \"trxname\" \n" +
                "from tms_TrxId " +
                "where trx_id in (:trxId)" +
                "order by trx_id ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("trxId", param);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanTrxidBean.class));

        return query.list();
    }

    @Transactional(readOnly=true)
    public List listQRAnomaliAcq1H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "select \n" +
                "period as \"period\", \n" +
                "to_char(to_date(period,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "time_fr_1h as \"timefr\", \n" +
                "time_to_1h as \"timeto\", \n" +
                "pan as \"pan\", \n" +
                "acquirer_id as \"acquirerId\", \n" +
                "qr_type as \"qrType\", \n" +
                "nvl(tms_bankfiid_api.get_bank_name(acquirer_id), acquirer_id) as \"acquirerBank\", \n" +
                "term_id as \"termid\"," +
                "term_owner_name as \"terminal\"," +
                "freq_app as \"freqapp\", \n" +
                "freq_dc as \"freqdc\", \n" +
                "freq_dc_free as \"freqdcfree\", \n" +
                "freq_rvsl as \"freqrvsl\", \n" +
                "nvl(total_amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(total_amt_dc/100,0) as \"totalamtdc\", \n" +
                "nvl(total_amt_dc_free/100,0) as \"totalamtdcfree\", \n" +
                "nvl(total_amt_rvsl/100,0) as \"totalamtrvsl\", \n" +
                "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0) as \"totalamt\", \n" +
                "TMS_MPAN_QR_DUMMY_CHECK_EXIST(pan, acquirer_id) as \"chkcolor\" \n" +
                "from \n" +
                "TMS_TRXANMLY_QR_ACQ_1HOUR \n" +
                "where \n" +
                "period >= :periodfr \n" +
                "and period <= :periodto \n" +
                "and substr(time_fr_1h,1,2) || substr(time_fr_1h,4,2) >= :timefr \n" +
                "and replace(substr(time_to_1h,1,2),'00','24') || substr(time_to_1h,4,2) <= :timeto \n";

        if(bank_id != ""){
            sql = sql + "and trim(tms_bankfiid_api.get_bank_id(acquirer_id)) = '"+ bank_id.trim() +"' \n";
        }

        /*Jika whitelist = false, maka hanya data yang melebihi standar(lebih dari '>') parameter pada tabel 'TMS_MPAN_QR_DUMMY' yang ditampilkan   => checkbox 'Show All Transaction' is unchecked.
        Jika whitelist = true, maka akan menampilkan data yang belum melebihi standar(kurang dari '<') parameter pada tabel 'TMS_MPAN_QR_DUMMY      => checkbox 'Show All Transaction' is checked.
        Jika chkcolor = true, maka grid akan berwarna kuning.*/

        if(!whitelist) {
            sql = sql + " and TMS_MPAN_QR_DUMMY_STTS(pan, acquirer_id, nvl(freq_app,0), nvl(freq_dc,0), nvl(freq_dc_free,0), nvl(freq_rvsl,0), nvl(total_amt_app/100,0),\n" +
                    "nvl(total_amt_dc/100,0), nvl(total_amt_dc_free/100,0), nvl(total_amt_rvsl/100,0),\n" +
                    "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by period, time_fr_1h, time_to_1h, nvl(tms_bankfiid_api.get_bank_name(acquirer_id), acquirer_id)";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
        query.setString("timefr", timefr);
        query.setString("timeto", timeto);

        query.setResultTransformer(new AliasToBeanResultTransformer(QRAnomalyBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listQRAnomaliAcq3H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "select \n" +
                "period as \"period\", \n" +
                "to_char(to_date(period,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "time_fr_3h as \"timefr\", \n" +
                "time_to_3h as \"timeto\", \n" +
                "pan as \"pan\", \n" +
                "acquirer_id as \"acquirerId\", \n" +
                "qr_type as \"qrType\", \n" +
                "nvl(tms_bankfiid_api.get_bank_name(acquirer_id), acquirer_id) as \"acquirerBank\", \n" +
                "term_id as \"termid\"," +
                "term_owner_name as \"terminal\"," +
                "freq_app as \"freqapp\", \n" +
                "freq_dc as \"freqdc\", \n" +
                "freq_dc_free as \"freqdcfree\", \n" +
                "freq_rvsl as \"freqrvsl\", \n" +
                "nvl(total_amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(total_amt_dc/100,0) as \"totalamtdc\", \n" +
                "nvl(total_amt_dc_free/100,0) as \"totalamtdcfree\", \n" +
                "nvl(total_amt_rvsl/100,0) as \"totalamtrvsl\", \n" +
                "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0) as \"totalamt\", \n" +
                "TMS_MPAN_QR_DUMMY_CHECK_EXIST(pan, acquirer_id) as \"chkcolor\" \n" +
                "from \n" +
                "TMS_TRXANMLY_QR_ACQ_3HOUR \n" +
                "where \n" +
                "period >= :periodfr \n" +
                "and period <= :periodto \n" +
                "and substr(time_fr_3h,1,2) || substr(time_fr_3h,4,2) >= :timefr \n" +
                "and replace(substr(time_to_3h,1,2),'00','24') || substr(time_to_3h,4,2) <= :timeto \n";

        if(bank_id != ""){
            sql = sql + "and trim(tms_bankfiid_api.get_bank_id(acquirer_id)) = '"+ bank_id.trim() +"' \n";
        }

        if(!whitelist) {
            sql = sql + " and TMS_MPAN_QR_DUMMY_STTS(pan, acquirer_id, nvl(freq_app,0), nvl(freq_dc,0), nvl(freq_dc_free,0), nvl(freq_rvsl,0), nvl(total_amt_app/100,0),\n" +
                    "nvl(total_amt_dc/100,0), nvl(total_amt_dc_free/100,0), nvl(total_amt_rvsl/100,0),\n" +
                    "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by period, time_fr_3h, nvl(tms_bankfiid_api.get_bank_name(acquirer_id), acquirer_id)";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
        query.setString("timefr", timefr);
        query.setString("timeto", timeto);
        query.setResultTransformer(new AliasToBeanResultTransformer(QRAnomalyBean.class));
        System.out.println(sql);
        System.out.println(timefr);
        System.out.println(timeto);
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listQRAnomaliAcq6H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "select \n" +
                "period as \"period\", \n" +
                "to_char(to_date(period,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "time_fr_6h as \"timefr\", \n" +
                "time_to_6h as \"timeto\", \n" +
                "pan as \"pan\", \n" +
                "acquirer_id as \"acquirerId\", \n" +
                "qr_type as \"qrType\", \n" +
                "nvl(tms_bankfiid_api.get_bank_name(acquirer_id), acquirer_id) as \"acquirerBank\", \n" +
                "term_id as \"termid\"," +
                "term_owner_name as \"terminal\"," +
                "freq_app as \"freqapp\", \n" +
                "freq_dc as \"freqdc\", \n" +
                "freq_dc_free as \"freqdcfree\", \n" +
                "freq_rvsl as \"freqrvsl\", \n" +
                "nvl(total_amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(total_amt_dc/100,0) as \"totalamtdc\", \n" +
                "nvl(total_amt_dc_free/100,0) as \"totalamtdcfree\", \n" +
                "nvl(total_amt_rvsl/100,0) as \"totalamtrvsl\", \n" +
                "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0) as \"totalamt\", \n" +
                "TMS_MPAN_QR_DUMMY_CHECK_EXIST(pan, acquirer_id) as \"chkcolor\" \n" +
                "from \n" +
                "TMS_TRXANMLY_QR_ACQ_6HOUR \n" +
                "where \n" +
                "period >= :periodfr \n" +
                "and period <= :periodto \n" +
                "and substr(time_fr_6h,1,2) || substr(time_fr_6h,4,2) >= :timefr \n" +
                "and replace(substr(time_to_6h,1,2),'00','24') || substr(time_to_6h,4,2) <= :timeto \n";

        if(bank_id != ""){
            sql = sql + "and trim(tms_bankfiid_api.get_bank_id(acquirer_id)) = '"+ bank_id.trim() +"' \n";
        }

        if(!whitelist) {
            sql = sql + " and TMS_MPAN_QR_DUMMY_STTS(pan, acquirer_id, nvl(freq_app,0), nvl(freq_dc,0), nvl(freq_dc_free,0), nvl(freq_rvsl,0), nvl(total_amt_app/100,0),\n" +
                    "nvl(total_amt_dc/100,0), nvl(total_amt_dc_free/100,0), nvl(total_amt_rvsl/100,0),\n" +
                    "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by period, time_fr_6h, nvl(tms_bankfiid_api.get_bank_name(acquirer_id), acquirer_id)";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
        query.setString("timefr", timefr);
        query.setString("timeto", timeto);
        query.setResultTransformer(new AliasToBeanResultTransformer(QRAnomalyBean.class));
        System.out.println(sql);
        System.out.println(timefr);
        System.out.println(timeto);
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listQRAnomaliAcq1D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "select \n" +
                "period as \"period\", \n" +
                "to_char(to_date(period,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "pan as \"pan\", \n" +
                "acquirer_id as \"acquirerId\", \n" +
                "qr_type as \"qrType\", \n" +
                "nvl(tms_bankfiid_api.get_bank_name(acquirer_id), acquirer_id) as \"acquirerBank\", \n" +
                "term_id as \"termid\"," +
                "term_owner_name as \"terminal\"," +
                "freq_app as \"freqapp\", \n" +
                "freq_dc as \"freqdc\", \n" +
                "freq_dc_free as \"freqdcfree\", \n" +
                "freq_rvsl as \"freqrvsl\", \n" +
                "nvl(total_amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(total_amt_dc/100,0) as \"totalamtdc\", \n" +
                "nvl(total_amt_dc_free/100,0) as \"totalamtdcfree\", \n" +
                "nvl(total_amt_rvsl/100,0) as \"totalamtrvsl\", \n" +
                "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0) as \"totalamt\", \n" +
                "TMS_MPAN_QR_DUMMY_CHECK_EXIST(pan, acquirer_id) as \"chkcolor\" \n" +
                "from \n" +
                "TMS_TRXANMLY_QR_ACQ_1DAY \n" +
                "where \n" +
                "period between :periodfr and :periodto \n";

        if(bank_id != ""){
            sql = sql + "and trim(tms_bankfiid_api.get_bank_id(acquirer_id)) = '"+ bank_id.trim() +"' \n";
        }

        if(!whitelist) {
            sql = sql + " and TMS_MPAN_QR_DUMMY_STTS(pan, acquirer_id, nvl(freq_app,0), nvl(freq_dc,0), nvl(freq_dc_free,0), nvl(freq_rvsl,0), nvl(total_amt_app/100,0),\n" +
                    "nvl(total_amt_dc/100,0), nvl(total_amt_dc_free/100,0), nvl(total_amt_rvsl/100,0),\n" +
                    "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by period, nvl(tms_bankfiid_api.get_bank_name(acquirer_id), acquirer_id)";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
        query.setResultTransformer(new AliasToBeanResultTransformer(QRAnomalyBean.class));
        System.out.println(sql);
        System.out.println(periodfr);
        System.out.println(periodto);
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listQRAnomaliAcq3D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "SELECT\n" +
                "TO_CHAR(to_date(period_fr, 'YYMMDD'), 'DD-MM-YYYY') AS \"period\",\n" +
                "TO_CHAR(to_date(period_to, 'YYMMDD'), 'DD-MM-YYYY') AS \"perioddisplay\",\n" +
                "period_fr as \"periodFrom\", \n" +
                "period_to as \"periodTo\", \n" +
                "pan as \"pan\", \n" +
                "acquirer_id as \"acquirerId\", \n" +
                "qr_type as \"qrType\", \n" +
                "nvl(tms_bankfiid_api.get_bank_name(acquirer_id), acquirer_id) as \"acquirerBank\", \n" +
                "term_id as \"termid\"," +
                "term_owner_name as \"terminal\"," +
                "freq_app AS \"freqapp\",\n" +
                "freq_dc AS \"freqdc\",\n" +
                "freq_dc_free AS \"freqdcfree\",\n" +
                "freq_rvsl AS \"freqrvsl\",\n" +
                "NVL(total_amt_app / 100, 0) AS \"totalamtapp\",\n" +
                "NVL(total_amt_dc / 100, 0) AS \"totalamtdc\",\n" +
                "NVL(total_amt_dc_free / 100, 0) AS \"totalamtdcfree\",\n" +
                "NVL(total_amt_rvsl / 100, 0) AS \"totalamtrvsl\",\n" +
                "NVL(total_amt_app / 100, 0) + NVL(total_amt_dc / 100, 0) + NVL(total_amt_dc_free / 100, 0) + NVL(total_amt_rvsl / 100, 0) AS \"totalamt\",\n" +
                "TMS_MPAN_QR_DUMMY_CHECK_EXIST(pan, acquirer_id) as \"chkcolor\" \n" +
                "FROM\n" +
                "TMS_TRXANMLY_QR_ACQ_3DAY\n" +
                "WHERE\n" +
                "period_fr >= :periodfr\n" +
                "AND period_to <= :periodto\n";

        if(bank_id != ""){
            sql = sql + "and trim(tms_bankfiid_api.get_bank_id(acquirer_id)) = '"+ bank_id.trim() +"' \n";
        }

        if(!whitelist) {
            sql = sql + " and TMS_MPAN_QR_DUMMY_STTS(pan, acquirer_id, nvl(freq_app,0), nvl(freq_dc,0), nvl(freq_dc_free,0), nvl(freq_rvsl,0), nvl(total_amt_app/100,0),\n" +
                    "nvl(total_amt_dc/100,0), nvl(total_amt_dc_free/100,0), nvl(total_amt_rvsl/100,0),\n" +
                    "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by period_fr, nvl(tms_bankfiid_api.get_bank_name(acquirer_id), acquirer_id)";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
        query.setResultTransformer(new AliasToBeanResultTransformer(QRAnomalyBean.class));
        return query.list();
    }


    @Transactional(readOnly=true)
    public List listDetailMPanQR(String period, String timeFr, String timeTo, String pan, String qrType, String acquirerId, String periodTo, String periodType, String termId, String termName) throws IOException{
        String linkDbPos =  ReadProperty.dbLinkActive().getLinkPos();

        String sql = "select \n" +
                "to_char(to_date(cur_dat_add,'YYMMDD'), 'DD-MM-YYYY') as \"period\", \n" +
                "substr(cur_tim_add,1,2) || ':' || substr(cur_tim_add,3,2) || ':' || substr(cur_tim_add,5,2) as \"trxtime\", \n" +
                //"pos_term_owner_name as \"terminal\", \n" +
                //"pos_term_id as \"termid\", \n" +
                //"(case when pos_type = '0210' and (pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001') then 1 else 0 end) as \"app\", \n" +
                "0 as \"dc\", \n" +
                //"(case when pos_type = '0210' and (pos_responder || pos_resp <> '7000' and pos_responder || pos_resp <> '7001') then 1 else 0 end) as \"dcfree\", \n" +
                //"(case when pos_type = '0420' and (pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001') then 1 else 0 end) as \"rvsl\", \n" +
                "nvl(pos_amt_1/100,0) as \"amount\", \n" +
                "pos_responder || pos_resp as \"rc\", \n" +
                "pos_type as \"msgtype\", \n" +
                "pos_pt_srv_entry_mde as \"posentrymode\", \n" +
                "TMS_BANK_API.Get_Bank_Name(nvl(substr(trim(pos_term_fiid), 2, 2), substr(trim(pos_term_ln), 2, 2))) as \"issuerBank\", \n" +
                "(case when pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001' then 'Approve' else 'Decline Free' end) as \"description\", \n";

        /*Jika type MPM, maka MPAN => card_no_prikey, jika CPM maka, MPAN => pos_acct_num.
        Sehingga card no untuk MPAN =>  pos_acct_num dan untuk CPM card no adalah card_no_prikey*/
        if("MPM".equalsIgnoreCase(qrType)) {
            //sql = sql + "card_no_prikey as \"pan\", pos_acct_num as \"cardNo\" \n";
            sql = sql + "pos_acct_num as \"cardNo\" \n";
        } else {
            //sql = sql + "pos_acct_num as \"pan\", card_no_prikey as \"cardNo\" \n";
            /*Modify by aaw 16/08/2021, ubah dari card_no_prikey ke card_no*/
            //sql = sql + "card_no as \"cardNo\" \n";
            sql = sql + "card_no_prikey as \"cardNo\" \n";
        }

        sql = sql  +" from " + linkDbPos + "\n where \n" +
                "pos_crd_fiid = :acquirerId \n" +
                //"and ((substr(pos_tran_cde, 1, 2) in('10','29')) or (substr(pos_tran_cde, 1, 2) in ('14') and pos_responder || pos_resp in ('7000','7001') and pos_type = '0210')) \n" +
                "and SUBSTR(POS_TRAN_CDE, 1, 2) IN('10','29', '14') and pos_term_id = :termId and pos_term_owner_name = :termName \n" +
                "and (case when substr(pos_tran_cde, 1, 2) = '29' then 'MPM' when pos_term_ln like 'Q%' and pos_term_ln not like '%01' then 'CPM' else '' end) = upper(:status) \n";

        if (!"".equals(timeFr.trim())&& !"".equals(timeTo.trim())) {
            sql = sql + "and cur_tim_add >= :timefr \n" +
                    "and cur_tim_add <= :timeto \n";
        } else {
            sql = sql + "and cur_tim_add >= '0000' \n";
        }

        if("MPM".equalsIgnoreCase(qrType)) {
            /*Modify by aaw 16/08/2021, ubah dari card_no_prikey ke card_no*/
            //sql = sql + "and trim(card_no) = :pan \n";
            sql = sql + "and trim(card_no_prikey) = :pan \n";
        } else {
            sql = sql + "and trim(pos_acct_num) = :pan \n";
        }

        if("3D".equalsIgnoreCase(periodType)) {
            sql = sql + " and cur_dat_add >= :period and cur_dat_add <= :periodTo \n";
        } else {
            sql = sql + " and cur_dat_add = :period \n";
        }

        sql = sql + "group by \n" +
                "to_char(to_date(cur_dat_add,'YYMMDD'), 'DD-MM-YYYY')," +
                "substr(cur_tim_add,1,2) || ':' || substr(cur_tim_add,3,2) || ':' || substr(cur_tim_add,5,2), \n" +
                "pos_term_owner_name, pos_term_id, \n" +
                //app
                // "(case when pos_type = '0210' and (pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001') then 1 else 0 end), \n" +
                //dcfree
                //"(case when pos_type = '0210' and (pos_responder || pos_resp <> '7000' and pos_responder || pos_resp <> '7001') then 1 else 0 end), \n" +
                //rvsl
                //"(case when pos_type = '0420' and (pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001') then 1 else 0 end), \n" +
                "pos_amt_1, \n" +
                "pos_responder || pos_resp, \n"+
                "pos_type, \n"+
                //acquirerBank
                //"nvl(tms_bankfiid_api.get_bank_name(pos_crd_fiid), pos_crd_fiid), \n" +
                "pos_pt_srv_entry_mde, pos_invoice_num, \n" +
                //issuerBank
                "tms_bank_api.get_bank_name(NVL(SUBSTR(TRIM(POS_TERM_FIID), 2, 2), SUBSTR(TRIM(POS_TERM_LN), 2, 2))), \n" +
                "(case when pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001' then 'Approve' else 'Decline Free' end), \n";

        /*Group by*/
        if("MPM".equalsIgnoreCase(qrType)) {
            sql = sql + "pos_acct_num";
        } else {
            /*Modify by aaw 16/08/2021, ubah dari card_no_prikey ke card_no*/
            sql = sql + "card_no_prikey";
        }

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);

        if(timeFr.replace(":", "").length() != 6){
            query.setString("period", period);
        }

        if (!"".equals(timeFr.trim())&& !"".equals(timeTo.trim())) {
            query.setString("timefr", timeFr.replace(":", ""));
            query.setString("timeto", timeTo.replace(":", ""));
        }

        if("3D".equalsIgnoreCase(periodType)) {
            query.setString("periodTo", periodTo);
        }

        query.setString("pan", pan);
        query.setString("acquirerId", acquirerId);
        query.setString("status", qrType);
        query.setString("termId", termId);
        query.setString("termName", termName);
        //System.out.println("Period Type : " + periodType + " PAN : " + pan + " Acq Id : " + acquirerId + " Period : " + period + " Period To : " + periodTo + " Time From : " + timeFr + " Time To : " + timeTo);
        //System.out.println("================== Query detail MPAN ================== \n" + sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(DetailMPanAnomalyBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listMPanExist(String pan, String acquirerId){
        String sql = "select pan as \"pan\", acquirer_id as \"acquirerId\", freq_app as \"freqApp\", freq_dc as \"freqDc\", freq_dc_free as \"freqDcFree\", freq_rvsl as \"freqRvsl\", total_amt_app as \"totalAmtApp\"," +
                "total_amt_dc as \"totalAmtDc\", total_amt_dc_free as \"totalAmtDcFree\", total_amt_rvsl as \"totalAmtRvsl\", total_amt as \"totalAmt\", active as \"active\" from\n" +
                "TMS_MPAN_QR_DUMMY where trim(pan) = :pan and upper(trim(acquirer_id)) = :acquirerId";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan.trim());
        query.setString("acquirerId", acquirerId.trim());
        return query.list();
    }

    @Transactional
    public void saveMPanTreshold(String pan, String acquirerId, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree,
            BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) {
        String sql = "insert into TMS_MPAN_QR_DUMMY values (:pan, :acquirerId, :freqApp, :freqDc, :freqDcFree, :freqRvsl, :totalAmtApp, :totalAmtDc, :totalAmtDcFree, :totalAmtRvsl, :totalAmt, :active)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan.trim());
        query.setString("acquirerId", acquirerId.trim());
        query.setBigDecimal("freqApp", freqApp);
        query.setBigDecimal("freqDc", freqDc);
        query.setBigDecimal("freqDcFree", freqDcFree);
        query.setBigDecimal("freqRvsl", freqRvsl);
        query.setBigDecimal("totalAmtApp", totalAmtApp);
        query.setBigDecimal("totalAmtDc", totalAmtDc);
        query.setBigDecimal("totalAmtDcFree", totalAmtDcFree);
        query.setBigDecimal("totalAmtRvsl", totalAmtRvsl);
        query.setBigDecimal("totalAmt", totalAmt);
        query.setString("active", active);
        query.executeUpdate();
    }

}

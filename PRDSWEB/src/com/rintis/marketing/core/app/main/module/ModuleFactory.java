package com.rintis.marketing.core.app.main.module;

import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.alert.IAlertService;
import com.rintis.marketing.core.app.main.module.bi.IAggregateService;
import com.rintis.marketing.core.app.main.module.bi.IBiService;
import com.rintis.marketing.core.app.main.module.biller.IBillerService;
import com.rintis.marketing.core.app.main.module.common.ICommonService;
import com.rintis.marketing.core.app.main.module.login.ILoginService;
import com.rintis.marketing.core.app.main.module.master.IMasterService;
import com.rintis.marketing.core.app.main.module.mcc.IMCCService;
import com.rintis.marketing.core.app.main.module.menu.IMenuService;
import com.rintis.marketing.core.app.main.module.mpanTreshold.IMPanTresholdService;
import com.rintis.marketing.core.app.main.module.pan.IPanService;
import com.rintis.marketing.core.app.main.module.qr.IQRService;
import com.rintis.marketing.core.app.main.module.sequential.ISequentialService;
import com.rintis.marketing.core.app.main.module.translog.ITransLogService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.inject.Named;

@Component
@Qualifier(SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY)
@Named(SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY)
public class ModuleFactory implements ApplicationContextAware {
    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public ApplicationContext getApplicationContext() throws BeansException {
        return this.applicationContext;
    }



    public ICommonService getCommonService() {
        return (ICommonService)applicationContext.getBean(SpringBeanConstant.SPRING_BEAN_COMMON_SERVICE);
    }

    public ILoginService getLoginService() {
        return (ILoginService)applicationContext.getBean(SpringBeanConstant.SPRING_BEAN_LOGIN_SERVICE);
    }
    
    public IMenuService getMenuService() {
        return (IMenuService)applicationContext.getBean(SpringBeanConstant.SPRING_BEAN_MENU_SERVICE);
    }

    public ITransLogService getTransLogService() {
        return (ITransLogService)applicationContext.getBean(SpringBeanConstant.SPRING_BEAN_TRANSLOG_SERVICE);
    }


    public IPanService getPanService() {
        return (IPanService)applicationContext.getBean(SpringBeanConstant.SPRING_BEAN_PAN_SERVICE);
    }

    public IMasterService getMasterService() {
        return (IMasterService)applicationContext.getBean(SpringBeanConstant.SPRING_BEAN_MASTER_SERVICE);
    }


    public IBiService getBiService() {
        return (IBiService)applicationContext.getBean(SpringBeanConstant.SPRING_BEAN_BI_SERVICE);
    }

    public IAggregateService getAggregateService() {
        return (IAggregateService)applicationContext.getBean(SpringBeanConstant.SPRING_BEAN_AGGREGATE_SERVICE);
    }

    public IAlertService getAlertService() {
        return (IAlertService)applicationContext.getBean(SpringBeanConstant.SPRING_BEAN_ALERT_SERVICE);
    }

    public ISequentialService getSequentialService() {
        return (ISequentialService)applicationContext.getBean(SpringBeanConstant.SPRING_BEAN_SEQUENTIAL_SERVICE);
    }

    public IMCCService getMCCService() {
        return (IMCCService)applicationContext.getBean(SpringBeanConstant.SPRING_BEAN_MCC_SERVICE);
    }

    /*Add by aaw 16/06/2021*/
    public IQRService getQRService() {
        return (IQRService)applicationContext.getBean(SpringBeanConstant.SPRING_BEAN_QR_SERVICE);
    }

    public IMPanTresholdService getMPanTresholdService() {
        return (IMPanTresholdService)applicationContext.getBean(SpringBeanConstant.SPRING_BEAN_MPAN_TRESHOLD_SERVICE);
    }

    public IBillerService getBillerService() {
        return (IBillerService) applicationContext.getBean(SpringBeanConstant.SPRING_BEAN_BILLER_SERVICE);
    }
    /*End add*/
}

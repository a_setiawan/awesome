package com.rintis.marketing.core.app.main.module.mcc;


import com.rintis.marketing.beans.bean.bi.MCCAnomalyBean;

import java.util.List;

public interface IMCCService {
    public List<MCCAnomalyBean> listMCCAnomaly(String periodFr, String periodTo, String bank_id) throws Exception;
}

package com.rintis.marketing.core.app.main.module.biller;

import com.rintis.marketing.beans.bean.biller.BillerAnomalyBean;
import com.rintis.marketing.beans.bean.biller.DetailBillerAnomalyBean;
import com.rintis.marketing.beans.bean.biller.TmsBillerBean;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.beans.entity.BillerAnomalyInstitusiParam;
import com.rintis.marketing.beans.entity.BillerAnomalyParam;
import com.rintis.marketing.beans.entity.BillerDummy;

import java.util.List;

/*Add by aaw 20211110*/

public interface IBillerService {
    public List<PanTrxidBean> listTrxIdBiller(String trx) throws Exception;

    public List<BillerAnomalyBean> listAnomalyBiller1H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception;

    public List<BillerAnomalyBean> listAnomalyBiller3H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception;

    public List<BillerAnomalyBean> listAnomalyBiller6H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception;

    public List<BillerAnomalyBean> listAnomalyBiller1D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception;

    public List<BillerAnomalyBean> listAnomalyBiller3D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception;

    public List<DetailBillerAnomalyBean> listDetailBiller(String period, String timeFr, String timeTo, String instCode, String custNo, String custName, String tranType, String periodType, String periodFrom, String periodTo) throws Exception;

    public TmsBillerBean findBillerById(String billerId) throws Exception;

    public void saveBillerTreshold(BillerDummy billerDummy) throws Exception;

    public boolean checkTresholdExist(String instCd, String custNo) throws Exception;

    public List<BillerDummy> listWhitelistBiller() throws Exception;

    public void deleteWhitelistBiller(String instCd, String custNo) throws Exception;

    public List<TmsBillerBean> fetchAllActiveBiller() throws Exception;

    public BillerDummy findBillerDummyById(String instCd, String custNo) throws Exception;

    public void updateWhitelistBiller(BillerDummy billerDummy) throws Exception;

    public List<BillerAnomalyParam> fetchBillerAnomalyParam() throws Exception;

    public BillerAnomalyParam fetchBillerParamByTrxId(String trxId) throws Exception;

    public void updateBillerAnomalyParam(BillerAnomalyParam billerAnomalyParam) throws Exception;

    public List<BillerAnomalyInstitusiParam> fetchBillerAnomalyInstitusiParam(String billerId) throws Exception;

    public List<BillerAnomalyInstitusiParam> checkBillerInstitusiExist(String billerId) throws Exception;

    public void deleteBillerAnomalyInstitusi(String billerId) throws Exception;

    public List<BillerAnomalyInstitusiParam> listBillerInstitutionParam() throws Exception;

    public void saveMultipleBillerAnomalyinstitution(List<BillerAnomalyInstitusiParam> listBillerAnomalyInstitusiParam) throws Exception;

    public List<BillerAnomalyInstitusiParam> fetchDataBillerParamInstitusiByID(String instCd) throws Exception;

    public void updateMultipleBillerAnomalyinstitution(List<BillerAnomalyInstitusiParam> listBillerAnomalyInstitusiParam) throws Exception;
}
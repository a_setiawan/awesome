package com.rintis.marketing.core.app.main.module.login;


import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.beans.entity.*;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.dao.UserLoginDao;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.EncDecUtils;

import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.LdapUtils;
import com.rintis.marketing.web.jsf.main.pan.PanAnomalyInquiryManagedBean;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.inject.Named;
import java.util.Date;
import java.util.List;

@Service
@Qualifier(SpringBeanConstant.SPRING_BEAN_LOGIN_SERVICE)
@Named(SpringBeanConstant.SPRING_BEAN_LOGIN_SERVICE)
public class LoginService implements ILoginService {
    private Logger log = Logger.getLogger(this.getClass());
    public static String internal = "Internal";
    public static String ldap = "LDAP";
    public static String bank = "Bank";
    public static String getRoleID;
    public static String getIDBank;
    public static String ipaddress;
    
    @Autowired
    private UserLoginDao userLoginDao;
    @Autowired
    private ModuleFactory moduleFactory;
    

    
    @Override
    @Transactional
    public UserInfoBean login(String userId, String password, String ip) throws Exception {
        //return authenticationService.login(userId, password, ip);
        return processLoginWithoutSpringSec(userId, password, ip);
    }
    

    @Override
    @Transactional
    public void createUserLoginHistory(UserLoginHistory userLoginHistory) throws Exception {
        userLoginDao.save(userLoginHistory);
    }
    
    

    @Transactional
    public void updateUserLogin(UserLogin userLogin) throws Exception {
        UserLogin ul = getUserLogin(userLogin.getUserLoginId());
        ul.setEnabled(userLogin.getEnabled());
        ul.setUserName(userLogin.getUserName());
        ul.setEmail(userLogin.getEmail());
        ul.setMobile(userLogin.getMobile());
        ul.setRoleTypeId(userLogin.getRoleTypeId());
        ul.setLastLogin(userLogin.getLastLogin());
        ul.setValidationType(userLogin.getValidationType());
        ul.setUserBank(userLogin.getUserBank());
        ul.setBankId(userLogin.getBankId());
        userLoginDao.update(ul);

        //TransactionLog tlog = new TransactionLog();
        //tlog.setUserId(userLogin.getUserLoginId());
        //tlog.setDescription("UPDATE USER");
        //moduleFactory.getCommonService().createTransLog(tlog);
    }

    @Transactional
    public boolean changePassword(String userId, String oldPassword, String newPassword) throws Exception {
        boolean result = false;
        String op = cryptBytes(oldPassword.getBytes());
        String np = cryptBytes(newPassword.getBytes());
        UserLogin userLogin = userLoginDao.getUserLogin(userId, DataConstant.ACTIVE);
        if (userLogin.getCurrentPassword().equals(op)) {
            userLogin.setCurrentPassword(np);
            userLogin.setLastLogin(new Date());
            userLoginDao.update(userLogin);
            result = true;

            TransactionLog tlog = new TransactionLog();
            tlog.setUserId(userId);
            tlog.setDescription("CHANGE PASSWORD");
            moduleFactory.getCommonService().createTransLog(tlog);
        }
        return result;
    }

    @Transactional
    public String resetPassword(String userId, String userLoginId) throws Exception {
        String newPass = "";
        Long nt = System.nanoTime();
        newPass = EncDecUtils.encodeBase36(nt);
        //log.info(newPass);
        newPass = newPass.substring(0, 4);
        //log.info(newPass);

        String np = cryptBytes(newPass.getBytes());
        UserLogin userLogin = userLoginDao.getUserLogin(userId, DataConstant.ACTIVE);
        userLogin.setCurrentPassword(np);
        userLogin.setLastLogin(null); //force change pass
        userLoginDao.update(userLogin);

        TransactionLog tlog = new TransactionLog();
        tlog.setUserId(userLoginId);
        tlog.setDescription("RESET PASSWORD " + userId);
        moduleFactory.getCommonService().createTransLog(tlog);

        return newPass;
    }


    @Transactional
    private UserInfoBean processLoginWithoutSpringSec(String username, String password, String ip) {
        UserInfoBean userInfoBean = null;
        try {
            //log.info(username);
            //log.info(cryptBytes(password.getBytes()));
            UserLogin ul = getUserLogin(username);
            UserLoginBank ulb = getUserLoginBank(username);

            if (ul == null && ulb == null) {
                return null;
            }
            boolean loginResult = false;
            //start modify by erichie
            if (ul != null) {
                if (ul.getValidationType().equalsIgnoreCase(DataConstant.USER_VALIDATION_TYPE_INTERNAL)) {
                    loginResult = userLoginDao.login(username, cryptBytes(password.getBytes()));
                    getRoleID = String.valueOf(ul.getRoleTypeId());
                    //System.out.println(getRoleID);
                    //getID = ul.getBankId();
                    //ipaddress = ip;
                    //System.out.println(ul.getBankId());
                } else if (ul.getValidationType().equalsIgnoreCase(DataConstant.USER_VALIDATION_TYPE_LDAP)) {
                    String ldapHost = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_LDAP_URL);
                    String ldapDomain = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_LDAP_DOMAIN);
                    loginResult = LdapUtils.authenticateLdapBool(ldapHost, ldapDomain, username, password);
                    getRoleID = String.valueOf(ul.getRoleTypeId());
                   // System.out.println(getRoleID);
                    //getID = ul.getBankId();
                    //ipaddress = ip;
                    //System.out.println(ip);
                    //System.out.println(ul.getBankId());
                }
            } else if (ulb != null) {
                loginResult = userLoginDao.loginBank(username, cryptBytes(password.getBytes()));
               // ipaddress = ip;
                //System.out.println(ip);
              //  getIDBank = ulb.getBankId();
                getRoleID = String.valueOf(ulb.getRoleTypeId());
              //  System.out.println(getRoleID);
                //System.out.println(ulb.getBankId());
            }
            //end modify by erichie

            UserLoginHistory userLoginHistory = new UserLoginHistory();
            userLoginHistory.setIpAddress(ip);

            UserLoginHistoryPk pk = new UserLoginHistoryPk();
            pk.setFromDate(new Date());
            pk.setUserLoginId(username);
            userLoginHistory.setUserLoginHistoryPk(pk);

            if (loginResult) {
                userInfoBean = getUserInfo(username);

                moduleFactory.getLoginService().createUserLoginHistory(userLoginHistory);

                //update last_login
                UserLogin userLogin = userLoginDao.getUserLogin(username, DataConstant.ACTIVE);
                if (userLogin != null) {
                    if (userLogin.getLastLogin() != null) {
                        userLogin.setLastLogin(new Date());
                        moduleFactory.getLoginService().updateUserLogin(userLogin);
                    }
                }

                UserLoginBank userLoginBank = userLoginDao.getUserLoginBank(username, DataConstant.ACTIVE);
                if (userLoginBank != null) {
                    if (userLoginBank.getLastLogin() != null) {
                        userLoginBank.setLastLogin(new Date());
                        moduleFactory.getLoginService().updateUserLoginBank(userLoginBank);
                    }
                }

                //check system_property_user
                moduleFactory.getCommonService().checkSystemPropertyUser(username);

                return userInfoBean;
            } else {
                moduleFactory.getLoginService().createUserLoginHistory(userLoginHistory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }


    @Override
    @Transactional(readOnly = true)
    public List<UserLogin> listUserLoginNonBank(Integer roleId) throws Exception {
        return userLoginDao.listUserLogin(roleId, DataConstant.USER_NONBANK);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserLoginBank> listUserLoginBank(Integer roleId) throws Exception {
        return userLoginDao.listUserLoginBank(roleId, DataConstant.USER_BANK, null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserLoginBank> listUserLoginBank(Integer roleId, String bankId) throws Exception {
        return userLoginDao.listUserLoginBank(roleId, DataConstant.USER_BANK, bankId);
    }

    @Override
    @Transactional
    public void deleteUserLogin(String userId) throws Exception {
        UserLogin ul = getUserLogin(userId);
        userLoginDao.delete(ul);

        //moduleFactory.getReportService().deleteAllReportFavorite(userId);
    }

    @Override
    @Transactional
    public void addUserLogin(UserLogin userLogin) throws Exception {
        userLogin.setCurrentPassword(cryptBytes(userLogin.getCurrentPassword().toLowerCase().getBytes()) );
        userLoginDao.save(userLogin);
    }

    @Override
    @Transactional(readOnly = true)
    public UserLogin getUserLogin(String userId) throws Exception {
        UserLogin ul = userLoginDao.getUserLogin(userId, DataConstant.ACTIVE);
        return ul;
    }





    private UserInfoBean getUserInfo(String username) {
        UserInfoBean userInfoBean = new UserInfoBean();
        try {
            userInfoBean.setUserId(username);
            userInfoBean.setUserLogin(userLoginDao.getUserLogin(username, DataConstant.ACTIVE) );
            userInfoBean.setUserLoginBank(userLoginDao.getUserLoginBank(username, DataConstant.ACTIVE));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return userInfoBean;
    }

    private String cryptBytes(byte[] bytes) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append(EncDecUtils.getCryptedBytes(bytes, "YoRELKpJ0Q3PihLhM0m7dcJDyE"));
        return sb.toString();
    }



    @Override
    @Transactional
    public void deleteUserLoginBank(String userId) throws Exception {
        UserLoginBank ul = getUserLoginBank(userId);
        userLoginDao.delete(ul);

        //moduleFactory.getReportService().deleteAllReportFavorite(userId);
    }

    @Override
    @Transactional
    public void addUserLoginBank(UserLoginBank userLogin) throws Exception {
        userLogin.setCurrentPassword(cryptBytes(userLogin.getCurrentPassword().toLowerCase().getBytes()) );
        userLoginDao.save(userLogin);
    }

    @Override
    @Transactional(readOnly = true)
    public UserLoginBank getUserLoginBank(String userId) throws Exception {
        UserLoginBank ul = userLoginDao.getUserLoginBank(userId, DataConstant.ACTIVE);
        return ul;
    }

    @Transactional
    public void updateUserLoginBank(UserLoginBank userLogin) throws Exception {
        UserLoginBank ul = getUserLoginBank(userLogin.getUserLoginId());
        ul.setEnabled(userLogin.getEnabled());
        ul.setUserName(userLogin.getUserName());
        ul.setEmail(userLogin.getEmail());
        ul.setMobile(userLogin.getMobile());
        ul.setRoleTypeId(userLogin.getRoleTypeId());
        ul.setLastLogin(userLogin.getLastLogin());
        ul.setValidationType(userLogin.getValidationType());
        ul.setUserBank(userLogin.getUserBank());
        ul.setBankId(userLogin.getBankId());
        userLoginDao.update(ul);

        //TransactionLog tlog = new TransactionLog();
        //tlog.setUserId(userLogin.getUserLoginId());
        //tlog.setDescription("UPDATE USER");
        //moduleFactory.getCommonService().createTransLog(tlog);
    }

    @Transactional
    public boolean changePasswordBank(String userId, String oldPassword, String newPassword) throws Exception {
        boolean result = false;
        String op = cryptBytes(oldPassword.getBytes());
        String np = cryptBytes(newPassword.getBytes());
        UserLoginBank userLoginBank = userLoginDao.getUserLoginBank(userId, DataConstant.ACTIVE);
        if (userLoginBank.getCurrentPassword().equals(op)) {
            userLoginBank.setCurrentPassword(np);
            userLoginBank.setLastLogin(new Date());
            userLoginDao.update(userLoginBank);
            result = true;

            TransactionLog tlog = new TransactionLog();
            tlog.setUserId(userId);
            tlog.setDescription("CHANGE PASSWORD");
            moduleFactory.getCommonService().createTransLog(tlog);
        }
        return result;
    }

    @Transactional
    public String resetPasswordBank(String userId, String userLoginId) throws Exception {
        String newPass = "";
        Long nt = System.nanoTime();
        newPass = EncDecUtils.encodeBase36(nt);
        //log.info(newPass);
        newPass = newPass.substring(0, 4);
        //log.info(newPass);

        String np = cryptBytes(newPass.getBytes());
        UserLoginBank userLoginBank = userLoginDao.getUserLoginBank(userId, DataConstant.ACTIVE);
        userLoginBank.setCurrentPassword(np);
        userLoginBank.setLastLogin(null);
        userLoginDao.update(userLoginBank);

        TransactionLog tlog = new TransactionLog();
        tlog.setUserId(userLoginId);
        tlog.setDescription("RESET PASSWORD " + userId);
        moduleFactory.getCommonService().createTransLog(tlog);

        return newPass;
    }

    @Override
    @Transactional(readOnly = true)
    public String getEmail(String userId) throws Exception {
       String email = userLoginDao.getEmail(userId);
        return email;
    }

}

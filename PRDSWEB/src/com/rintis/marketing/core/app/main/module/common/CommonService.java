package com.rintis.marketing.core.app.main.module.common;


import com.rintis.marketing.beans.entity.*;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.dao.CommonDao;
import com.rintis.marketing.core.constant.DataConstant;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.util.Date;
import java.util.List;

@Service
@Qualifier(SpringBeanConstant.SPRING_BEAN_COMMON_SERVICE)
@Named(SpringBeanConstant.SPRING_BEAN_COMMON_SERVICE)
public class CommonService implements ICommonService {
    private Logger log = Logger.getLogger(this.getClass());
    @Autowired
    private CommonDao commonDao;

    @Override
    @Transactional(readOnly = true)
    public String getSystemProperty(String key) throws Exception {
        SystemProperty sp = commonDao.getSystemProperty(key);
        if (sp == null) {
            return "";
        }
        return sp.getSystemValue();
    }

    @Override
    @Transactional(readOnly = true)
    public SystemProperty getSystemPropertyEntity(String key) throws Exception {
        SystemProperty sp = commonDao.getSystemProperty(key);
        return sp;
    }

    @Override
    @Transactional
    public void editSystemProperty(SystemProperty systemProperty) throws Exception {
        SystemProperty sp = (SystemProperty)commonDao.get(SystemProperty.class, systemProperty.getId());
        sp.setSystemValue(systemProperty.getSystemValue());
        commonDao.update(sp);
    }

    @Override
    @Transactional
    public void editSystemProperty(List<SystemProperty> listSystemProperty) throws Exception {
        for(SystemProperty systemProperty : listSystemProperty) {
            SystemProperty sp = (SystemProperty) commonDao.get(SystemProperty.class, systemProperty.getId());
            sp.setSystemValue(systemProperty.getSystemValue());
            commonDao.update(sp);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<SystemProperty> listSystemProperty() throws Exception {
        return commonDao.listSystemProperty();
    }

    @Override
    @Transactional(readOnly = true)
    public List<SystemProperty> listSystemPropertyNotPerUser(List<String> listExclude) throws Exception {
        return commonDao.listSystemPropertyPerUserWithExclude(0, listExclude);
    }

    @Override
    @Transactional(readOnly = true)
    public List<SystemProperty> listSystemPropertyPerUser() throws Exception {
        return commonDao.listSystemPropertyPerUserWithExclude(1, null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<SystemProperty> listSystemPropertyPerUser(String name) throws Exception {
        return commonDao.listSystemPropertyPerUser(1, name);
    }

    @Override
    @Transactional(readOnly = true)
    public SystemPropertyUser getSystemPropertyUserObject(String userId, String key) throws Exception {
        return commonDao.getSystemProperty(userId, key);
    }

    @Override
    @Transactional(readOnly = true)
    public String getSystemPropertyUser(String userId, String key) throws Exception {
        SystemPropertyUser spu = commonDao.getSystemProperty(userId, key);
        if (spu == null) return "";
        return spu.getSystemValue();
    }

    @Override
    @Transactional(readOnly = true)
    public String getSystemPropertyUserDefault(String userId, String key) throws Exception {
        String value = "";
        SystemPropertyUser spu = commonDao.getSystemProperty(userId, key);
        if (spu == null) {
            SystemProperty sp = commonDao.getSystemProperty(key);
            if (sp != null) {
                value = sp.getSystemValue();
            }
        } else {
            //check if system_property.per_user = 0, ambil dari system_property
            SystemProperty sp = commonDao.getSystemProperty(key);
            if (sp != null) {
                if (sp.getPerUser() == 1) {
                    value = spu.getSystemValue();
                } else {
                    value = sp.getSystemValue();
                }
            }

        }
        return value;
    }

    @Override
    @Transactional
    public void editSystemPropertyUser(String userId, List<SystemPropertyUser> listSystemProperty) throws Exception {
        for(SystemPropertyUser systemProperty : listSystemProperty) {
            log.info(systemProperty.getId());
            SystemPropertyUser sp = (SystemPropertyUser) commonDao.get(SystemPropertyUser.class, systemProperty.getId());
            sp.setSystemValue(systemProperty.getSystemValue());
            commonDao.update(sp);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<SystemPropertyUser> listSystemPropertyUser(String userId) throws Exception {
        return commonDao.listSystemProperty(userId);
    }

    @Override
    @Transactional
    public void checkSystemPropertyUser(String userId) throws Exception {
        List<SystemProperty> list = listSystemPropertyPerUser();
        for(SystemProperty sp : list) {
            SystemPropertyUser spu = getSystemPropertyUserObject(userId, sp.getSystemName());
            if (spu == null) {
                spu = new SystemPropertyUser();
                spu.setUserLoginId(userId);
                spu.setSystemName(sp.getSystemName());
                spu.setSystemValue(sp.getSystemValue());
                commonDao.save(spu);
            }
        }
    }


    @Override
    @Transactional(readOnly = true)
    public Integer getLimitYear(String userId) throws Exception {
        String s = getSystemPropertyUser(userId, DataConstant.SYSTEM_PROPERTY_LIMIT_YEAR);
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            return 5;
        }
    }

    @Override
    @Transactional
    public void createTransLog(TransactionLog tlog) throws Exception {
        tlog.setLogDate(new Date());
        commonDao.save(tlog);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Enumeration> listEnumeration(String enumTypeId) throws Exception {
        return commonDao.listEnumeration(enumTypeId);
    }

    @Override
    @Transactional(readOnly = true)
    public Enumeration getEnumeration(Integer id) throws Exception {
        return (Enumeration) commonDao.get(Enumeration.class, id);
    }

    @Override
    @Transactional(readOnly = true)
    public Enumeration getEnumerationByEnumId(String enumId) throws Exception {
        return commonDao.getEnumerationByEnumId(enumId);
    }

    @Override
    @Transactional(readOnly = true)
    public Enumeration getEnumerationByDescription(String description) throws Exception {
        return commonDao.getEnumerationByDescription(description);
    }


    @Override
    @Transactional
    public void deleteEmailQueue(Integer id) throws Exception {
        EmailQueue ceq = (EmailQueue)commonDao.get(EmailQueue.class, id);
        commonDao.delete(ceq);
    }

    @Override
    @Transactional
    public List<EmailQueue> listEmailQueue() throws Exception {
        return commonDao.listEmailQueue();
    }


}

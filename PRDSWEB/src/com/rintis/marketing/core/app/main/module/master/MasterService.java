package com.rintis.marketing.core.app.main.module.master;

import com.rintis.marketing.beans.bean.master.BankEmailBean;
import com.rintis.marketing.beans.bean.master.BinBean;
import com.rintis.marketing.beans.bean.master.HolidayBean;
import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.bean.report.BillerBean;
import com.rintis.marketing.beans.entity.*;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.dao.MasterDao;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Qualifier(SpringBeanConstant.SPRING_BEAN_MASTER_SERVICE)
@Named(SpringBeanConstant.SPRING_BEAN_MASTER_SERVICE)
public class MasterService implements IMasterService {
    private Logger log = Logger.getLogger(MasterService.class);
    @Autowired
    private MasterDao masterDao;
    @Autowired
    private ModuleFactory moduleFactory;


    @Override
    @Transactional(readOnly = true)
    public List<BankBean> listBank(Map<String, Object> param) throws Exception {
        return masterDao.listBank(param);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TmsBankCa> listBankBIN() throws Exception {
        return masterDao.listBankBIN();
    }

    @Override
    @Transactional(readOnly = true)
    public List<BankBean> listBankEntity() throws Exception {
        return masterDao.listBankEntity();
    }

    //180801 Added By AAH
    @Override
    @Transactional(readOnly = true)
    public List<BankBean> listBankCA(Map<String, Object> param) throws Exception {
        return masterDao.listBankCA(param);
    }

    @Override
    @Transactional
    public void updateMktBank(List<BankBean> list) throws Exception {
        for(BankBean bb : list) {
            EisBank eisBank = getBank(bb.getBankid());

            eisBank.setBankCardholderQty(bb.getBankcardholderqty());
            eisBank.setBankAtmQty(bb.getBankatmqty());
            eisBank.setBankEdcQty(bb.getBankedcqty());

            if (bb.getBankapnbool()) {
                eisBank.setBankApn(DataConstant.YES);
            } else {
                eisBank.setBankApn(DataConstant.NO);
            }

            if (bb.getBankcupbool()) {
                eisBank.setBankCup(DataConstant.YES);
            } else {
                eisBank.setBankCup(DataConstant.NO);
            }

            if (bb.getBankpaymentbool()) {
                eisBank.setBankPayment(DataConstant.YES);
            } else {
                eisBank.setBankPayment(DataConstant.NO);
            }

            masterDao.update(eisBank);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public EisBank getBankByBankName(String bankName) throws Exception {
        return masterDao.getBankByBankName(bankName);
    }

    @Override
    @Transactional(readOnly = true)
    public EisBank getBank(String bankId) throws Exception {
        return (EisBank) masterDao.get(EisBank.class, bankId);
    }

    //180801 Added By AAH
    @Override
    @Transactional(readOnly = true)
    public BankBean getBankCA(String bankId) throws Exception {
        return masterDao.getBankCA(bankId);
    }

    @Override
    @Transactional(readOnly = true)
    public BankBean getBankIntercon(String bankId, String bankCode) throws Exception {
        return masterDao.getBankIntercon(bankId, bankCode);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BillerBean> listBiller(Map<String, Object> param) throws Exception {
        return masterDao.listBiller(param);
    }

    /*@Override
    @Transactional(readOnly = true)
    public EisBiller getBillerByBillerName(String billerName) throws Exception {
        return masterDao.getBillerByBillerName(billerName);
    }

    @Override
    @Transactional(readOnly = true)
    public EisBiller getBiller(String billerId) throws Exception {
        return (EisBiller) masterDao.get(EisBiller.class, billerId);
    }*/



    //////////////////////////



    @Override
    @Transactional(readOnly = true)
    public List<EsqTrxIdBean> listEsqTrxId() throws Exception {
        return masterDao.listEsqTrxId();
    }

    @Override
    @Transactional(readOnly = true)
    public EsqTrxIdBean getEsqTrxId(String trxId) throws Exception {
        return masterDao.getEsqTrxId(trxId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EsqTrxIdBean> listEsqTrxIdIndicator() throws Exception {
        return masterDao.listEsqTrxIndicator();
    }

    @Override
    @Transactional(readOnly = true)
    public EsqTrxIdBean getEsqTrxIdIndicator(String trxIndicatorId) throws Exception {
        return masterDao.getEsqTrxIdIndicator(trxIndicatorId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EsqTrxIdBean> listEsqTrxIdTrxIndicator(String trxId) throws Exception {
        return masterDao.listEsqTrxIdTrxIndicator(trxId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EsqTrxIdBean> listEsqTrxIdTrxIndicator() throws Exception {
        return masterDao.listEsqTrxIdTrxIndicator();
    }

    @Override
    @Transactional(readOnly = true)
    public BankAlertThreshold getBankAlertThreshold(String bankId, String trxId) throws Exception {
        BankAlertThresholdPk pk = new BankAlertThresholdPk();
        pk.setBankId(bankId);
        pk.setTransactionId(trxId);
        return (BankAlertThreshold) masterDao.get(BankAlertThreshold.class, pk);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BankAlertThreshold> listBankAlertThresholdActive() throws Exception {
        return masterDao.listBankAlertThresholdActive();
    }


    @Override
    @Transactional(readOnly = true)
    public TmsMasterHoliday getMasterHoliday(BigDecimal id) throws Exception {
        return (TmsMasterHoliday)masterDao.get(TmsMasterHoliday.class, id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TmsMasterHoliday> listMasterHoliday(int year) throws Exception {
        return masterDao.listMasterHoliday(year);
    }

    /*@Override
    @Transactional
    public void updateMasterHoliday(int year, List<MktMasterHoliday> list) throws Exception {
        List<MktMasterHoliday> listmh = listMasterHoliday(year);
        for(MktMasterHoliday mh : listmh) {
            masterDao.delete(mh);
        }

        BigDecimal id = new BigDecimal(System.nanoTime());
        for(MktMasterHoliday mh : list) {
            mh.setId(id);
            mh.setYear(year);
            masterDao.save(mh);

            id = id.add(BigDecimal.ONE);
        }
    }*/

    @Override
    @Transactional
    public void updateMasterHoliday2(int year, List<List<HolidayBean>> list) throws Exception {
        List<TmsMasterHoliday> listmh = listMasterHoliday(year);
        for(TmsMasterHoliday mh : listmh) {
            masterDao.delete(mh);
        }

        BigDecimal id = new BigDecimal(System.nanoTime());
        for(List<HolidayBean> listhb : list) {
            for(HolidayBean hb : listhb) {
                //log.info(hb.getDay() + " " + hb.getMonth() + " " + hb.getYear());
                if (hb.isHoliday()) {
                    Date d = new Date();
                    d= DateUtils.setHour(d, 0);
                    d= DateUtils.setMinute(d, 0);
                    d= DateUtils.setSecond(d, 0);
                    d= DateUtils.setMonth(d, 1);
                    d= DateUtils.setDay(d, 1);
                    d= DateUtils.setMonth(d, hb.getMonth() + 1);
                    d= DateUtils.setDay(d, hb.getDay());
                    d= DateUtils.setYear(d, year);
                    /*String s = Integer.toString(year) + "-" +
                            StringUtils.formatFixLength(Integer.toString(hb.getMonth()), "0",2) + "-" +
                            StringUtils.formatFixLength(Integer.toString(hb.getDay()), "0", 2) + " 00:00:00";
                    Date d = DateUtils.convertDate(s, DateUtils.DATE_YYYYMMDD_HHMMSS);*/
                    //log.info(d);
                    TmsMasterHoliday mh = new TmsMasterHoliday();
                    mh.setId(id);
                    mh.setYear(year);
                    mh.setHolidayDate(d);
                    mh.setDescription("holiday");
                    masterDao.save(mh);

                    id = id.add(BigDecimal.ONE);
                }
            }
        }
    }

    /*@Override
    @Transactional
    public void deleteMasterHoliday(BigDecimal id) throws Exception {
        MktMasterHoliday h = getMasterHoliday(id);
        masterDao.delete(h);
    }*/

    @Override
    @Transactional(readOnly = true)
    public boolean checkHoliday(Date date) throws Exception {
        TmsMasterHoliday holiday = masterDao.getMasterHoliday(date);
        if (holiday != null) {
            return true;
        }
        return false;
    }


    @Override
    @Transactional(readOnly = true)
    public List<GroupBank> listGroupBank() throws Exception {
        return masterDao.listGroupBank();
    }

    @Override
    @Transactional(readOnly = true)
    public GroupBank getGroupBank(String id) throws Exception {
        return (GroupBank) masterDao.get(GroupBank.class, id);
    }

    @Override
    @Transactional
    public void addGroupBank(GroupBank groupBank, List<String> listBank) throws Exception {
        masterDao.save(groupBank);

        for(String bankId : listBank) {
            GroupBankMemberPk pk = new GroupBankMemberPk();
            pk.setGroupId(groupBank.getGroupId());
            pk.setBankId(bankId);

            GroupBankMember gbm = new GroupBankMember();
            gbm.setGroupBankMemberPk(pk);
            masterDao.save(gbm);
        }
    }

    @Override
    @Transactional
    public void updateGroupBank(GroupBank groupBank, List<String> listBank) throws Exception {
        GroupBank gb = getGroupBank(groupBank.getGroupId());
        gb.setGroupName(groupBank.getGroupName());
        masterDao.update(gb);

        //delete old bank
        List<GroupBankMember> oldList = listGroupBankMember(groupBank.getGroupId());
        for(GroupBankMember gbm : oldList) {
            masterDao.delete(gbm);
        }

        for(String s : listBank) {
            GroupBankMemberPk pk = new GroupBankMemberPk();
            pk.setBankId(s);
            pk.setGroupId(groupBank.getGroupId());
            GroupBankMember gbm = new GroupBankMember();
            gbm.setGroupBankMemberPk(pk);
            masterDao.save(gbm);
        }


    }

    @Override
    @Transactional
    public void deleteGroupBank(String id) throws Exception {
        List<GroupBankMember> list = masterDao.listGroupBankMember(id);
        for(GroupBankMember gbm : list) {
            masterDao.delete(gbm);
        }

        GroupBank gb = getGroupBank(id);
        masterDao.delete(gb);


    }

    @Override
    @Transactional(readOnly = true)
    public List<GroupBankMember> listGroupBankMember(String groupId) throws Exception {
        return masterDao.listGroupBankMember(groupId);
    }


    @Override
    @Transactional
    public void addBankEmail(BankEmail bankEmail) throws Exception {
        masterDao.save(bankEmail);
    }

    @Override
    @Transactional
    public void updateBankEmail(BankEmail bankEmail) throws Exception {
        BankEmail be = getBankEmail(bankEmail.getBankId());
        be.setEmail(bankEmail.getEmail());
        be.setAlert_mcc_status(bankEmail.getAlert_mcc_status());
        be.setAlert_seq_status(bankEmail.getAlert_seq_status());
        /*Add by aaw 02/07/2021*/
        be.setAlert_mpan_acq(bankEmail.getAlert_mpan_acq());
        /*End add*/
        masterDao.update(be);
    }

    @Override
    @Transactional
    public void deleteBankEmail(String bankId) throws Exception {
        BankEmail be = getBankEmail(bankId);
        masterDao.delete(be);
    }

    @Override
    @Transactional(readOnly = true)
    public BankEmail getBankEmail(String bankId) throws Exception {
        return (BankEmail)masterDao.get(BankEmail.class, bankId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BankEmail> listBankEmail() throws Exception {
        return masterDao.listBankEmail();
    }

    @Override
    @Transactional(readOnly = true)
    public List<BankEmailBean> listBankEmailBean() throws Exception {
        return masterDao.listBankEmailBean();
    }

    /*@Override
    @Transactional(readOnly = true)
    public TmsTimeFrame getTimeFrame(String id) throws Exception {
        return (TmsTimeFrame) masterDao.get(TmsTimeFrame.class, id);
    }*/

    @Override
    @Transactional(readOnly = true)
    public TmsUserQuickView getUserQuickview(TmsUserQuickViewPk pk) throws Exception {
        return (TmsUserQuickView)masterDao.get(TmsUserQuickView.class, pk);
    }

    @Override
    @Transactional
    public boolean updateUserQuickView(TmsUserQuickView userQuickview) throws Exception {
        boolean result = false;
        TmsUserQuickView uq = getUserQuickview(userQuickview.getTmsUserQuickViewPk());
        if (uq == null) {
            masterDao.save(userQuickview);
        } else {
            /*uq.setQuickDate(userQuickview.getQuickDate());
            uq.setCbTot(userQuickview.getCbTot());
            uq.setCbApp(userQuickview.getCbApp());
            uq.setCbDc(userQuickview.getCbDc());
            uq.setCbDf(userQuickview.getCbDf());
            masterDao.update(uq);*/
            result = true;
        }
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<TmsUserQuickView> listUserQuickview(String userId) throws Exception {
        return masterDao.listUserQuickview(userId);
    }

    @Override
    @Transactional
    public void deleteUserQuickView(TmsUserQuickView[] list) throws Exception {
        for(TmsUserQuickView uqv : list) {
            TmsUserQuickView qv = getUserQuickview(uqv.getTmsUserQuickViewPk());
            masterDao.delete(qv);
        }
    }

    @Override
    @Transactional
    public void deleteAllUserQuickView(String userId) throws Exception {
        List<TmsUserQuickView> list = listUserQuickview(userId);
        for(TmsUserQuickView uqv : list) {
            masterDao.delete(uqv);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<BinList> listBin() throws Exception {
        return masterDao.listBin();
    }

    @Override
    @Transactional
    public void addListBin(BinList listBin, List<String> listTrx) throws Exception {
        masterDao.save(listBin);

        for(String trxId : listTrx) {
            BinListPk pk = new BinListPk();
            pk.setListId(listBin.getListId());
            pk.setTrxId(trxId);

            BinListTrxId blti = new BinListTrxId();
            blti.setBinListPk(pk);
            masterDao.save(blti);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<EsqTrxid> listTrx() throws Exception {
        return masterDao.listTrx();
    }

    @Override
    @Transactional(readOnly = true)
    public EsqTrxid getTrx(String trxId) throws Exception {
        return (EsqTrxid) masterDao.get(EsqTrxid.class, trxId);
    }

    @Override
    @Transactional
    public void deleteListBin(String listId) throws Exception {
        List<BinListTrxId> list = masterDao.listBinTrxId(listId);
        for(BinListTrxId gbm : list) {
            masterDao.delete(gbm);
        }

        BinList gb = getBinList(listId);
        masterDao.delete(gb);

    }

    @Override
    @Transactional(readOnly = true)
    public BinList getBinList(String listId) throws Exception {
        return (BinList) masterDao.get(BinList.class, listId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BinListTrxId> listBinTrxId(String listId) throws Exception {
        return masterDao.listBinTrxId(listId);
    }

    @Override
    @Transactional
    public void updateListBin(BinList listBin, List<String> listTrx) throws Exception {

        //delete old bank
        List<BinListTrxId> oldList = listBinTrxId(listBin.getListId());
        for(BinListTrxId gbm : oldList) {
            masterDao.delete(gbm);
        }

        for(String s : listTrx) {
            BinListPk pk = new BinListPk();
            pk.setTrxId(s);
            pk.setListId(listBin.getListId());
            BinListTrxId gbm = new BinListTrxId();
            gbm.setBinListPk(pk);
            masterDao.save(gbm);
        }

    }

    @Override
      @Transactional(readOnly = true)
      public List<MCCList> listMcc(String mcc) throws Exception {
        return masterDao.listMcc(mcc);
    }

    @Override
    @Transactional(readOnly = true)
    public MCCList getMcc(String mcc) throws Exception {
        return (MCCList)masterDao.get(MCCList.class, mcc);
    }

    @Override
    @Transactional(readOnly = true)
    public MCCParameter getMccParameter(String mcc) throws Exception {
        return (MCCParameter)masterDao.get(MCCParameter.class, mcc);
    }

    @Override
    @Transactional
    public void deleteMcc(String mcc) throws Exception {
        MCCList ul = getMcc(mcc);
        masterDao.delete(ul);
    }

    @Override
    @Transactional
    public void updateMcc(String mcc, String description) throws Exception {
        masterDao.updateMcc(mcc, description);
    }

    @Override
    @Transactional(readOnly=true)
    public boolean checkMccUsed(String mcc) throws Exception {
        List<MCCList> list = masterDao.listMcc(mcc);
        if (list.size() > 0) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    @Transactional
    public void saveMcc(String mcc, String description) throws Exception {
        masterDao.saveMcc(mcc, description);
    }

    @Override
    @Transactional(readOnly = true)
    public List<MCCParameter> listMccParameter(String mcc) throws Exception {
        return masterDao.listMccParameter(mcc);
    }

    @Override
    @Transactional
    public void updateMccParameter(String mcc, BigDecimal trxapp, BigDecimal trxtot, BigDecimal amtapp, BigDecimal amttot) throws Exception {
        masterDao.updateMccParameter(mcc, trxapp, trxtot, amtapp, amttot);
    }

    @Override
    @Transactional
    public void saveMccParameter(String mcc, BigDecimal trxapp, BigDecimal trxtot, BigDecimal amtapp, BigDecimal amttot) throws Exception {
        masterDao.saveMccParameter(mcc, trxapp, trxtot, amtapp, amttot);
    }

    @Override
    @Transactional
    public void deleteMccParameter(String mcc) throws Exception {
        MCCParameter ul = getMccParameter(mcc);
        masterDao.delete(ul);
    }

    @Override
    @Transactional(readOnly=true)
    public boolean checkMccParameterUsed(String mcc) throws Exception {
        List<MCCParameter> list = masterDao.listMccParameter(mcc);
        if (list.size() > 0) {
            return true;
        }else{
            return false;
        }
    }

    /*Add by aaw 13/09/2021*/
    @Override
    @Transactional(readOnly = true)
    public String checkDataIsExist(String bankId) throws Exception {
        return masterDao.checkDataExist(bankId);
    }
    /*End add*/

    @Override
    @Transactional(readOnly = true)
    public List<BinBean> binList() throws Exception {
        return masterDao.binList();
    }

    @Override
    @Transactional
    public void deleteBin(String bankId, String binNo, BigDecimal binLength) throws Exception {
        masterDao.deleteBin(bankId, binNo, binLength);
    }

    @Override
    @Transactional(readOnly = true)
    public String getBinBankId(String pan) throws Exception {
        return masterDao.getBinBankId(pan);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BankBean> getListBankCA(String bankId) throws Exception {
        return masterDao.getListBankCA(bankId);
    }

    @Override
    @Transactional
    public void addBin(String selectedBankId, String binNo, String binLength) throws Exception {
        masterDao.addBin(selectedBankId, binNo, binLength);
    }
}

package com.rintis.marketing.core.app.main.module.mpanTreshold;

/*Created by aaw 28/06/2021*/

import com.rintis.marketing.beans.bean.bi.WhitelistQRBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.dao.MPanTresholdDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.List;

@Service
@Qualifier(SpringBeanConstant.SPRING_BEAN_MPAN_TRESHOLD_SERVICE)
@Named(SpringBeanConstant.SPRING_BEAN_MPAN_TRESHOLD_SERVICE)
public class MPanTresholdService implements IMPanTresholdService {

    @Autowired
    private MPanTresholdDao mPanTresholdDao;

    @Override
    @Transactional(readOnly = true)
    public List<WhitelistQRBean> listMPanQR() throws Exception {
        return mPanTresholdDao.listMPanTreshold();
    }

    @Override
    public void deleteMPanTreshold(String pan, String acquirerId) throws Exception {
        mPanTresholdDao.deleteMPanTreshold(pan, acquirerId);
    }

    @Override
    public boolean checkMPanUsed(String pan, String acquirerId) throws Exception {
        List<WhitelistQRBean> list = mPanTresholdDao.listMpanTreshold(pan, acquirerId);
        if (list.size() > 0) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void saveMPanTreshold(WhitelistQRBean whitelistQRBean) throws Exception {
        mPanTresholdDao.saveMPanTreshold(whitelistQRBean);
    }

    @Override
    public List<WhitelistQRBean> getWhitelistMPan(String pan, String acquirerId) throws Exception {
        return mPanTresholdDao.getWhitelistMPan(pan, acquirerId);
    }

    @Override
    public void updateMPan(String pan, String acquirerId, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree, BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) throws Exception {
        mPanTresholdDao.updateMPan(pan, acquirerId, freqApp, freqDc, freqDcFree, freqRvsl, totalAmtApp, totalAmtDc, totalAmtDcFree, totalAmtRvsl, totalAmt, active);
    }
}
package com.rintis.marketing.core.app.main.module.master;

import com.rintis.marketing.beans.bean.master.BankEmailBean;
import com.rintis.marketing.beans.bean.master.BinBean;
import com.rintis.marketing.beans.bean.master.HolidayBean;
import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.bean.report.BillerBean;
import com.rintis.marketing.beans.entity.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface IMasterService {
    public List<BankBean> listBank(Map<String, Object> param) throws Exception;
    /*Add by aaw 13/09/2021*/
    public String checkDataIsExist(String bankId) throws Exception;
    /*End add*/
    //public List<EisBank> listBankEntity() throws Exception;
    public List<BankBean> listBankEntity() throws Exception;
    public void updateMktBank(List<BankBean> list) throws Exception;
    public EisBank getBankByBankName(String bankName) throws Exception;
    public EisBank getBank(String bankId) throws Exception;
    public BankBean getBankIntercon(String bankId, String bankCode) throws Exception;

    //180801 Added By AAH
    public List<BankBean> listBankCA(Map<String, Object> param) throws Exception;
    public List<TmsBankCa> listBankBIN() throws Exception;
    public BankBean getBankCA(String bankId) throws Exception;
    public String getBinBankId(String pan) throws Exception;
    public List<BankBean> getListBankCA(String bankId) throws Exception;

    public List<BillerBean> listBiller(Map<String, Object> param) throws Exception;
    //public EisBiller getBillerByBillerName(String billerName) throws Exception;
    //public EisBiller getBiller(String billerId) throws Exception;


    public List<EsqTrxIdBean> listEsqTrxId() throws Exception;
    public EsqTrxIdBean getEsqTrxId(String trxId) throws Exception;
    public List<EsqTrxIdBean> listEsqTrxIdIndicator() throws Exception;
    public EsqTrxIdBean getEsqTrxIdIndicator(String trxIndicatorId) throws Exception;
    public List<EsqTrxIdBean> listEsqTrxIdTrxIndicator(String trxId) throws Exception;
    public List<EsqTrxIdBean> listEsqTrxIdTrxIndicator() throws Exception;
    public BankAlertThreshold getBankAlertThreshold(String bankId, String trxId) throws Exception;
    public List<BankAlertThreshold> listBankAlertThresholdActive() throws Exception;

    public TmsMasterHoliday getMasterHoliday(BigDecimal id) throws Exception;
    public List<TmsMasterHoliday> listMasterHoliday(int year) throws Exception;
    //public void updateMasterHoliday(int year, List<MktMasterHoliday> list) throws Exception;
    public void updateMasterHoliday2(int year, List<List<HolidayBean>> list) throws Exception;
    //public void deleteMasterHoliday(BigDecimal id) throws Exception;
    public boolean checkHoliday(Date date) throws Exception;


    public List<GroupBank> listGroupBank() throws Exception;
    public GroupBank getGroupBank(String id) throws Exception;
    public void addGroupBank(GroupBank groupBank, List<String> listBank) throws Exception;
    public void updateGroupBank(GroupBank groupBank, List<String> listBank) throws Exception;
    public void deleteGroupBank(String id) throws Exception;
    public List<GroupBankMember> listGroupBankMember(String groupId) throws Exception;

    public void addBankEmail(BankEmail bankEmail) throws Exception;
    public void updateBankEmail(BankEmail bankEmail) throws Exception;
    public void deleteBankEmail(String bankId) throws Exception;
    public BankEmail getBankEmail(String bankId) throws Exception;
    public List<BankEmail> listBankEmail() throws Exception;
    public List<BankEmailBean> listBankEmailBean() throws Exception;

    //public TmsTimeFrame getTimeFrame(String id) throws Exception;
    public TmsUserQuickView getUserQuickview(TmsUserQuickViewPk pk) throws Exception;
    public boolean updateUserQuickView(TmsUserQuickView userQuickview) throws Exception;
    public List<TmsUserQuickView> listUserQuickview(String userId) throws Exception;
    public void deleteUserQuickView(TmsUserQuickView[]list) throws Exception;
    public void deleteAllUserQuickView(String userId) throws Exception;

    public List<BinList> listBin() throws Exception;
    public List<EsqTrxid> listTrx() throws Exception;
    public void addListBin(BinList listBin, List<String> listTrx) throws Exception;
    public EsqTrxid getTrx(String trxId) throws Exception;
    public void deleteListBin(String listId) throws Exception;
    public BinList getBinList(String listId) throws Exception;
    public List<BinListTrxId> listBinTrxId(String listId) throws Exception;
    public void updateListBin(BinList listBin, List<String> listTrx) throws Exception;

    public List<MCCList> listMcc(String mcc) throws Exception;
    public void deleteMcc(String mcc) throws Exception;
    public MCCList getMcc(String mcc) throws Exception;
    public void updateMcc(String mcc, String description) throws Exception;
    public boolean checkMccUsed(String mcc) throws Exception;
    public void saveMcc(String mcc, String description) throws Exception;

    public List<MCCParameter> listMccParameter(String mcc) throws Exception;
    public void deleteMccParameter(String mcc) throws Exception;
    public MCCParameter getMccParameter(String mcc) throws Exception;
    public void updateMccParameter(String mcc, BigDecimal trxapp, BigDecimal trxtot, BigDecimal amtapp, BigDecimal amttot) throws Exception;
    public void saveMccParameter(String mcc, BigDecimal trxapp, BigDecimal trxtot, BigDecimal amtapp, BigDecimal amttot) throws Exception;
    public boolean checkMccParameterUsed(String mcc) throws Exception;
    public List<BinBean> binList() throws Exception;
    public void deleteBin(String bankId, String binNo, BigDecimal binLength) throws Exception;
    public void addBin(String selectedBankId, String binNo, String binLength) throws Exception;
}

package com.rintis.marketing.core.app.main.module.login;

import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.beans.entity.UserLogin;
import com.rintis.marketing.beans.entity.UserLoginBank;
import com.rintis.marketing.beans.entity.UserLoginHistory;

import java.util.List;


public interface ILoginService {
    public UserInfoBean login(String userId, String password, String ip) throws Exception;
    public void createUserLoginHistory(UserLoginHistory userLoginHistory) throws Exception;
    
    public void updateUserLogin(UserLogin userLogin) throws Exception;
    public boolean changePassword(String userId, String oldPassword, String newPassword) throws Exception;
    public String resetPassword(String userId, String userLoginId) throws Exception;

    public List<UserLogin> listUserLoginNonBank(Integer roleId) throws Exception;
    public List<UserLoginBank> listUserLoginBank(Integer roleId) throws Exception;
    public List<UserLoginBank> listUserLoginBank(Integer roleId, String bankId) throws Exception;
    public void deleteUserLogin(String userId) throws Exception;
    public void addUserLogin(UserLogin userLogin) throws Exception;
    public UserLogin getUserLogin(String userId) throws Exception;

    public void deleteUserLoginBank(String userId) throws Exception;
    public void addUserLoginBank(UserLoginBank userLogin) throws Exception;
    public UserLoginBank getUserLoginBank(String userId) throws Exception;
    public void updateUserLoginBank(UserLoginBank userLogin) throws Exception;
    public boolean changePasswordBank(String userId, String oldPassword, String newPassword) throws Exception;
    public String resetPasswordBank(String userId, String userLoginId) throws Exception;
    public String getEmail(String userId) throws Exception;
}

package com.rintis.marketing.core.app.main.module.menu;

import com.rintis.marketing.beans.bean.bi.DashboardRoleBean;
import com.rintis.marketing.beans.bean.menu.UserMenuBean;
import com.rintis.marketing.beans.entity.*;
import com.rintis.marketing.core.app.main.dao.MenuDao;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.util.List;

@Service
@Qualifier(SpringBeanConstant.SPRING_BEAN_MENU_SERVICE)
@Named(SpringBeanConstant.SPRING_BEAN_MENU_SERVICE)
public class MenuService implements IMenuService {
    private Logger log = Logger.getLogger(MenuService.class);
    @Autowired
    private ModuleFactory moduleFactory;
    @Autowired
    private MenuDao menuDao;

    @Override
    @Transactional(readOnly=true)
    public List<Menu> listMenuByRole(String userId, String parentId) throws Exception {
        return menuDao.listMenuByRole(userId, parentId);
    }

    @Override
    @Transactional(readOnly=true)
    public List<Menu> listMenuByRoleBank(String userId, String parentId) throws Exception {
        return menuDao.listMenuByRoleBank(userId, parentId);
    }


    @Override
    @Transactional(readOnly=true)
    public List<MenuRoleType> listMenuRoleType(Integer hidden) throws Exception {
        return menuDao.listMenuRoleType(hidden);
    }

    @Override
    @Transactional(readOnly=true)
    public MenuRoleType getMenuRoleType(Integer menuRoleTypeId) {
        return menuDao.getMenuRoleType(menuRoleTypeId);
    }

    @Override
    @Transactional
    public void createMenuRoleType(MenuRoleType menuRoleType) throws Exception {
        menuDao.save(menuRoleType);
    }

    @Override
    @Transactional
    public void updateMenuRoleType(MenuRoleType menuRoleType) throws Exception {
        MenuRoleType mrt = getMenuRoleType(menuRoleType.getMenuRoleTypeId());
        mrt.setMenuRoleName(menuRoleType.getMenuRoleName());
        menuDao.update(mrt);
    }

    @Override
    @Transactional
    public void deleteMenuRoleType(Integer menuRoleTypeId) throws Exception {
        MenuRoleType mrt = getMenuRoleType(menuRoleTypeId);
        menuDao.delete(mrt);
    }

    @Override
    @Transactional(readOnly = true)
    public MenuRoleType getMenuRoleByName(String name) throws Exception {
        return menuDao.getMenuRoleByName(name);
    }

    @Override
    @Transactional(readOnly=true)
    public List<UserMenuBean> listRoleAllMenu(Integer roleTypeId, String parentId, Integer active) throws Exception {
        List<UserMenuBean> list = menuDao.listRoleAllMenu(roleTypeId, parentId, active);
        for(UserMenuBean umb : list) {
            if (umb.getRoletypeid() == null ) {
                umb.setStatusmenu(false);
            } else {
                umb.setStatusmenu(true);
            }
        }
        return list;
    }


    @Override
    @Transactional
    public void updateRoleMenu(Integer roleTypeId, String menuId, boolean status) throws Exception {
        if (status) {
            menuDao.addRoleMenu(roleTypeId, menuId);
        } else {
            menuDao.removeRoleMenu(roleTypeId, menuId);
        }
    }

    @Override
    @Transactional
    public void selectAllRoleMenu(Integer roleTypeId) throws Exception {
        menuDao.removeAllRoleMenu(roleTypeId);
        menuDao.addAllRoleMenu(roleTypeId);
    }

    @Override
    @Transactional
    public void clearAllRoleMenu(Integer roleTypeId) throws Exception {
        menuDao.removeAllRoleMenu(roleTypeId);
    }


    @Override
    @Transactional(readOnly = true)
    public List<Menu> listReportMenu(String userId) throws Exception {
        return menuDao.listReportMenu(userId);
    }

    @Override
    @Transactional(readOnly = true)
    public Menu getMenu(String menuId) throws Exception {
        return (Menu)menuDao.get(Menu.class, menuId);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean checkMenuRoleTypeUsed(Integer roleTypeId) throws Exception {
        //check user_login
        List<UserLogin> listUNB = moduleFactory.getLoginService().listUserLoginNonBank(roleTypeId);
        if (listUNB.size() > 0) {
            return true;
        }
        List<UserLoginBank> listUB = moduleFactory.getLoginService().listUserLoginBank(roleTypeId);
        if (listUB.size() > 0) {
            return true;
        }
        //check dashboard_role
        List<DashboardRole> listDR = moduleFactory.getBiService().checkDashboardRole(roleTypeId);
        if (listDR.size() > 0) {
            return true;
        }
        return false;
    }
}

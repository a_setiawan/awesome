package com.rintis.marketing.core.app.main.dao;

import com.rintis.marketing.beans.bean.menu.UserMenuBean;
import com.rintis.marketing.beans.entity.Menu;
import com.rintis.marketing.beans.entity.MenuRole;
import com.rintis.marketing.beans.entity.MenuRolePk;
import com.rintis.marketing.beans.entity.MenuRoleType;
import com.rintis.marketing.core.SpringBeanConstant;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.util.Date;
import java.util.List;

@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_MENU_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_MENU_DAO)
public class MenuDao extends AbstractDao {


    @Transactional(readOnly=true)
    public List<Menu> listMenuByRole(String userId, String parentId) throws Exception {
        String sql = "SELECT a.* \n" +
                "FROM tms_menu a LEFT JOIN tms_menu_role b ON a.menu_id=b.menu_id \n"+
                "WHERE ((a.menu_type=1 AND b.role_type_id=(SELECT c.role_type_id FROM tms_user_login c WHERE c.user_login_id = :userId ) ) \n" +
                "OR \n" +
                "(a.menu_type=2 AND b.role_type_id=(SELECT d.role_type_id FROM tms_user_login d WHERE d.user_login_id = :userId ) ))  \n"+
                "AND a.active=1 \n";
        if (parentId != null) {
            sql = sql + "AND a.parent_menu_id=:parentId \n";
        } else {
            sql = sql + "AND a.parent_menu_id is null \n";
        }
        sql = sql + "ORDER BY a.sequence_id ";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("userId", userId);
        if (parentId != null) {
            query.setString("parentId", parentId);
        }
        query.addEntity(Menu.class);
        return query.list();
    }

    @Transactional(readOnly=true)
    public List<Menu> listMenuByRoleBank(String userId, String parentId) throws Exception {
        String sql = "SELECT a.* \n" +
                "FROM tms_menu a LEFT JOIN tms_menu_role b ON a.menu_id=b.menu_id \n"+
                "WHERE ((a.menu_type=1 AND b.role_type_id=(SELECT c.role_type_id FROM tms_user_login_bank c WHERE c.user_login_id = :userId ) ) \n" +
                "OR \n" +
                "(a.menu_type=2 AND b.role_type_id=(SELECT d.role_type_id FROM tms_user_login_bank d WHERE d.user_login_id = :userId ) ))  \n"+
                "AND a.active=1 \n";
        if (parentId != null) {
            sql = sql + "AND a.parent_menu_id=:parentId \n";
        } else {
            sql = sql + "AND a.parent_menu_id is null \n";
        }
        sql = sql + "ORDER BY a.sequence_id ";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("userId", userId);
        if (parentId != null) {
            query.setString("parentId", parentId);
        }
        query.addEntity(Menu.class);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly=true)
    public List<MenuRoleType> listMenuRoleType(Integer hidden) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MenuRoleType.class);
        if (hidden != null) {
            criteria.add(Restrictions.eq("hidden", hidden));
        }
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly=true)
    public MenuRoleType getMenuRoleType(Integer menuRoleTypeId) {
        return (MenuRoleType)sessionFactory.getCurrentSession().get(MenuRoleType.class, menuRoleTypeId);
    }

    @Transactional(readOnly=true)
    public MenuRoleType getMenuRoleByName(String name) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MenuRoleType.class);
        criteria.add(Restrictions.eq("menuRoleName", name));
        return (MenuRoleType)criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly=true)
    public List<UserMenuBean> listRoleAllMenu(Integer roleTypeId, String parentId, Integer active) {
        //log.info(roleTypeId);
        String sql = "SELECT a.menu_id as \"menuid\", a.menu_type as \"menutype\", a.menu_name as \"menuname\", a.menu_url as \"menuurl\", " +
                "a.parent_menu_id as \"parentmenuid\", a.sequence_id as \"sequenceid\", a.active as \"active\", "+
                "b.role_type_id as \"roletypeid\" " +
                " "+
                "FROM tms_menu a LEFT JOIN tms_menu_role b ON (a.menu_id=b.menu_id AND b.role_type_id = :roleTypeId ) ";
        if (parentId == null) {
            sql = sql + "WHERE a.parent_menu_id is null ";
        } else {
            sql = sql + "WHERE a.parent_menu_id = :parentId ";
        }
        if (active >= 0) {
            sql = sql + "AND a.active = :active ";
        }
        sql = sql + "ORDER BY a.sequence_id ";
        //log.info(sql);
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setInteger("roleTypeId", roleTypeId);
        if (parentId != null) {
            query.setString("parentId", parentId);
        }
        if (active >= 0) {
            query.setInteger("active", active);
        }
        query.setResultTransformer(Transformers.aliasToBean(UserMenuBean.class));
        return query.list();
    }

    @Transactional
    public void addRoleMenu(Integer roleTypeId, String menuId) {
        Date date = new Date();
        MenuRolePk menuRolePk = new MenuRolePk();
        menuRolePk.setMenuId(menuId);
        menuRolePk.setRoleTypeId(roleTypeId);

        MenuRole menuRole = new MenuRole();
        menuRole.setMenuRolePk(menuRolePk);
        save(menuRole);
    }

    @Transactional
    public void removeRoleMenu(Integer roleTypeId, String menuId) {
        MenuRolePk menuRolePk = new MenuRolePk();
        menuRolePk.setMenuId(menuId);
        menuRolePk.setRoleTypeId(roleTypeId);
        MenuRole menuRole = (MenuRole)sessionFactory.getCurrentSession().get(MenuRole.class, menuRolePk);
        delete(menuRole);
    }

    @Transactional
    public void addAllRoleMenu(Integer roleTypeId) {
        String sql = "insert into tms_menu_role (role_type_id, menu_id) " +
                "select :roleTypeId, menu_id from menu where active = 1 ";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setInteger("roleTypeId", roleTypeId);
        query.executeUpdate();
    }

    @Transactional
    public void removeAllRoleMenu(Integer roleTypeId) {
        String sql = "delete from tms_menu_role where role_type_id = :roleTypeId ";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setInteger("roleTypeId", roleTypeId);
        query.executeUpdate();
    }

    @Transactional(readOnly = true)
    public List<Menu> listReportMenu(String userId) throws Exception {
        String sql = "select c.* \n" +
                "from tms_USER_LOGIN a, tms_MENU_ROLE b, mrkt_MENU c \n" +
                "where a.USER_LOGIN_ID = :userId \n" +
                "and a.ROLE_TYPE_ID = b.ROLE_TYPE_ID \n" +
                "and b.MENU_ID = c.MENU_ID \n" +
                "and c.MENU_TYPE = 2 \n" +
                "and c.IS_REPORT = 1 \n" +
                "and c.active = 1 \n" +
                "order by c.MENU_ID ";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.addEntity(Menu.class);
        query.setString("userId", userId);
        return query.list();
    }


}

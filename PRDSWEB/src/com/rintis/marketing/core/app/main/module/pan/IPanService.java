package com.rintis.marketing.core.app.main.module.pan;


import com.rintis.marketing.beans.bean.bi.*;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by aabraham on 11/02/2019.
 */

/**
 * modified by erichie
 */
public interface IPanService {
    public void deletePan(String pan, String pengirim) throws Exception;
    public void deleteEmail(String useremail) throws Exception;

    public List<WhitelistBean> listPan() throws Exception;
    public List<PanAnomalyEmailBean> listEmail() throws Exception;
    public List<PanAnomalyParamBean> listParam() throws Exception;
    //start add (ERE 200313)
    public List<PanAnomalyParamBean> listParamBankBene() throws Exception;
    //stop add (ERE 200313)
    public List<PanAnomalyParamBean> listParamBank() throws Exception;
    //add
    public List<PanAnomalyParamBean> listMaxAmountParam() throws Exception;
    //add
    public List<PanTrxidBean> panListTrxId() throws Exception;
    public List<PanTrxidBean> panListTrxIdBene() throws Exception;
    //add
    public PanTrxidBean getPanTrxId (String trxId) throws Exception;

    public List<DetailPanAnomalyBean> listDetailAtm(String period, String timeFr, String timeTo, String pan, String tranType, String pengirim) throws Exception;
    public List<DetailPanAnomalyBean> listDetailPos(String period, String timeFr, String timeTo, String pan, String tranType, String pengirim, String status) throws Exception;
    public List<DetailPanAnomalyPaymentBean> listDetailPgw(String period, String timeFr, String timeTo, String pan, String tranType, String pengirim) throws Exception;

    public void savePan(String pan, String pengirim, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree, BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) throws Exception;
    //modify
    /*Modify by aaw 02/07/2021*/
    public void saveEmail(String useremail, String gender, String username, String alert1h, String alert3h, String alert6h, String alert1d, String aler3d, String alertMaxAmt, String alertMccVelocity, String alertSeqVelocity, String alertMpanAcq) throws Exception;  //add
    /*End modify*/
    public void updatePan(String pan, String pengirim, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree, BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) throws Exception;
    //modify
    /*Modify by aaw 02/07/2021*/
    public void updateEmail(String useremail, String gender, String username, String alert1h, String alert3h, String alert6h, String alert1d, String alert3d, String alertMaxAmt, String alertMccVelocity, String alertSeqVelocity, String alertMpanAcq) throws Exception;
    /*End*/
    //modify
    public void updateParam(String trx, BigDecimal mintrxapp1h, BigDecimal minamtapp1h, BigDecimal mintrxdc1h, BigDecimal minamtdc1h, BigDecimal mintrxdcfree1h, BigDecimal minamtdcfree1h, BigDecimal mintrxrvsl1h, BigDecimal minamtrvsl1h, BigDecimal minamttot1h, BigDecimal mintrxapp3h, BigDecimal minamtapp3h, BigDecimal mintrxdc3h, BigDecimal minamtdc3h, BigDecimal mintrxdcfree3h, BigDecimal minamtdcfree3h, BigDecimal mintrxrvsl3h, BigDecimal minamtrvsl3h, BigDecimal minamttot3h, BigDecimal mintrxapp6h, BigDecimal minamtapp6h, BigDecimal mintrxdc6h, BigDecimal minamtdc6h, BigDecimal mintrxdcfree6h, BigDecimal minamtdcfree6h, BigDecimal mintrxrvsl6h, BigDecimal minamtrvsl6h, BigDecimal minamttot6h, BigDecimal mintrxapp1d, BigDecimal minamtapp1d, BigDecimal mintrxdc1d, BigDecimal minamtdc1d, BigDecimal mintrxdcfree1d, BigDecimal minamtdcfree1d, BigDecimal mintrxrvsl1d, BigDecimal minamtrvsl1d, BigDecimal minamttot1d, BigDecimal mintrxapp3d, BigDecimal minamtapp3d, BigDecimal mintrxdc3d, BigDecimal minamtdc3d, BigDecimal mintrxdcfree3d, BigDecimal minamtdcfree3d, BigDecimal mintrxrvsl3d, BigDecimal minamtrvsl3d, BigDecimal minamttot3d) throws Exception;
    //add

    //start add (ERE 200313)
    public void updateParamBankBene(String trx, BigDecimal mintrxapp1h, BigDecimal minamtapp1h, BigDecimal mintrxdc1h, BigDecimal minamtdc1h, BigDecimal mintrxdcfree1h, BigDecimal minamtdcfree1h, BigDecimal mintrxrvsl1h, BigDecimal minamtrvsl1h, BigDecimal minamttot1h, BigDecimal mintrxapp3h, BigDecimal minamtapp3h, BigDecimal mintrxdc3h, BigDecimal minamtdc3h, BigDecimal mintrxdcfree3h, BigDecimal minamtdcfree3h, BigDecimal mintrxrvsl3h, BigDecimal minamtrvsl3h, BigDecimal minamttot3h, BigDecimal mintrxapp6h, BigDecimal minamtapp6h, BigDecimal mintrxdc6h, BigDecimal minamtdc6h, BigDecimal mintrxdcfree6h, BigDecimal minamtdcfree6h, BigDecimal mintrxrvsl6h, BigDecimal minamtrvsl6h, BigDecimal minamttot6h, BigDecimal mintrxapp1d, BigDecimal minamtapp1d, BigDecimal mintrxdc1d, BigDecimal minamtdc1d, BigDecimal mintrxdcfree1d, BigDecimal minamtdcfree1d, BigDecimal mintrxrvsl1d, BigDecimal minamtrvsl1d, BigDecimal minamttot1d, BigDecimal mintrxapp3d, BigDecimal minamtapp3d, BigDecimal mintrxdc3d, BigDecimal minamtdc3d, BigDecimal mintrxdcfree3d, BigDecimal minamtdcfree3d, BigDecimal mintrxrvsl3d, BigDecimal minamtrvsl3d, BigDecimal minamttot3d) throws Exception;
    //stop add (ERE 200313)

    public void saveParamBank(List<PanAnomalyParamBean> list, String bank_id) throws Exception;
    public void updateParamBank(List<PanAnomalyParamBean> list, String bank_id) throws Exception;

    public void updateMaxAmtParam(String trx, BigDecimal maxamt1h);

    public boolean checkPanSenderUsed(String pan, String pengirim) throws Exception;
    public boolean checkEmailUsed(String useremail) throws Exception;

    public List<WhitelistBean> getWhitelistPan(String pan, String pengirim) throws Exception;
    public List<PanAnomalyEmailBean> getEmail(String useremail) throws Exception;
    public List<PanAnomalyParamBean> getParam(String trx) throws Exception;
    //start add (ERE 200318)
    public List<PanAnomalyParamBean> getParamBankBene(String trx) throws Exception;
    //stop add (ERE 200318)
    public List<PanAnomalyParamBean> getParamBank(String id) throws Exception;
    public void deleteParamBank(String bank_id) throws Exception;
    public List<PanAnomalyParamBean> getParamBankId(String bank_id) throws Exception;
    public List<PanAnomalyParamBean> getParamMaxAmount(String trx) throws Exception;

    //start modify
    public List<PanAnomalyBean> listAnomalyPan1H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public List<PanAnomalyBean> listAnomalyPan3H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public List<PanAnomalyBean> listAnomalyPan6H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public List<PanAnomalyBean> listAnomalyPan1D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public List<PanAnomalyBean> listAnomalyPan3D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public List<PanAnomalyBean> listAnomalyPanBene1H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public List<PanAnomalyBean> listAnomalyPanBene3H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public List<PanAnomalyBean> listAnomalyPanBene6H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public List<PanAnomalyBean> listAnomalyPanBene1D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public List<PanAnomalyBean> listAnomalyPanBene3D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    public List<PanAnomalyBean> listMaxAmountAnomalyPan1H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception;
    //end modify

    public String getTrxIdFromTrxName(String trxName) throws Exception;
    public String getGroupIdFromTrxName(String trxName) throws Exception;

    /*Add by aaw 10/05/21*/
    public List<DetailBeneficiaryAnomalyBean> listDetailBeneficiaryAtm(String period, String timeFr, String timeTo, String pan, String tranType, String pengirim, String periodTo, String periodType) throws Exception;
    /*End add*/
    /*Add by aaw 20/05/21*/
    public List<WhitelistBeneficiaryBean> listBeneficiary() throws Exception;
    public void deleteBeneficiary(String account, String penerima) throws Exception;
    public boolean checkBeneficiaryReceiverUsed(String account, String penerima) throws Exception;
    public void saveBeneficiary(String account, String penerima, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree,
        BigDecimal totalAmtRvsl,BigDecimal totalAmt, String active) throws Exception;
    public List<WhitelistBeneficiaryBean> getWhitelistBeneficiary(String account, String penerima) throws Exception;
    public void updateBeneficiary(String account, String penerima, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc,
        BigDecimal totalAmtDcFree, BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) throws Exception;
    /*End add*/


}

package com.rintis.marketing.core.app.main.dao;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * @author popo
 * @version 1.0
 * @since 2012-01-13
 */
public abstract class AbstractDao {
    protected Logger log = Logger.getLogger(this.getClass());
    @Autowired
    @Qualifier("sessionFactory")
    protected SessionFactory sessionFactory;
    
    @Transactional
    public void flush() {
        sessionFactory.getCurrentSession().flush();
    }
    
    @Transactional
    public void save(Object obj) {
        sessionFactory.getCurrentSession().save(obj);
    }
    
    @Transactional
    public void delete(Object obj) {
        sessionFactory.getCurrentSession().delete(obj);
    }
    
    @Transactional
    public void update(Object obj) {
        sessionFactory.getCurrentSession().update(obj);
    }
    
    @Transactional
    public void saveOrUpdate(Object obj) {
        sessionFactory.getCurrentSession().saveOrUpdate(obj);
    }
    
    @Transactional(readOnly = true)
    public Object get(Class<?> clazz, Serializable id) {
        return sessionFactory.getCurrentSession().get(clazz, id);        
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<Object> findAll(Class<?> clazz, String sortBy, boolean isAsc) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(clazz);
        if (sortBy != null) {
            if (isAsc) {
                criteria.addOrder(Order.asc(sortBy));
            } else {
                criteria.addOrder(Order.desc(sortBy));
            }
        }
        return criteria.list();
    }
    

    protected String sqlListTreeChild(String tableName, String primaryKey, String parentField,
                                      String parentValue) {
        StringBuffer sb = new StringBuffer("");
        sb.append("WITH RECURSIVE cczz AS ");
        sb.append("( ");
        sb.append("SELECT * FROM " + tableName + " WHERE " + parentField + " = '" + parentValue + "' ");
        sb.append("UNION ALL ");
        sb.append("SELECT d.* ");
        sb.append("FROM ");
        sb.append(tableName + " AS d ");
        sb.append("inner JOIN ");
        sb.append("cczz AS sd ");
        sb.append("ON (d." + parentField + " = sd." + primaryKey + ") ");
        sb.append(") ");
        sb.append("SELECT * FROM cczz ");
        return sb.toString();
    }

    protected String sqlListTreeChildReverse(String tableName, String primaryKey, String primaryKeyValue,
                                             String parentField) {
        StringBuffer sb = new StringBuffer("");
        sb.append("WITH RECURSIVE cczz AS ");
        sb.append("( ");
        sb.append("SELECT * FROM " + tableName + " WHERE " + primaryKey + " = '" + primaryKeyValue + "' ");
        sb.append("UNION ALL ");
        sb.append("SELECT d.* ");
        sb.append("FROM ");
        sb.append(tableName + " AS d ");
        sb.append("inner JOIN ");
        sb.append("cczz AS sd ");
        sb.append("ON (sd." + parentField + " = d." + primaryKey + ") ");
        sb.append(") ");
        sb.append("SELECT * FROM cczz ");
        return sb.toString();
    }

    protected String sqlListRecordEndTree(String tableName, String primaryKey, String parentField) {
        StringBuffer sb = new StringBuffer("");
        sb.append("select bean.* from " + tableName + " bean  ");
        sb.append("where bean." + primaryKey + " not in (select distinct entity." + parentField + " from " + tableName + " entity where entity." + parentField + " is not null) ");
        return sb.toString();
    }
}

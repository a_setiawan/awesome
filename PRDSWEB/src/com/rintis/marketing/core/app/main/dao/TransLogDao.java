package com.rintis.marketing.core.app.main.dao;

import com.rintis.marketing.beans.entity.TransactionLog;
import com.rintis.marketing.beans.entity.UserLoginHistory;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.constant.ParameterMapName;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_TRANSLOG_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_TRANSLOG_DAO)
public class TransLogDao extends AbstractDao {

    @Transactional(readOnly = true)
    public List<UserLoginHistory> searchUserLoginLog(Map<String, Object> param) {
        Date startDate = (Date)param.get(ParameterMapName.PARAM_STARTDATE);
        Date endDate = (Date)param.get(ParameterMapName.PARAM_ENDDATE);
        String userId = StringUtils.checkNull((String) param.get(ParameterMapName.PARAM_USERID));

        if (startDate != null) {
            startDate = DateUtils.getStartOfDay(startDate);
        }
        if (endDate != null) {
            endDate = DateUtils.getEndOfDay235959(endDate);
        }

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserLoginHistory.class);
        criteria.add(Restrictions.ge("userLoginHistoryPk.fromDate", startDate) );
        criteria.add(Restrictions.le("userLoginHistoryPk.fromDate", endDate) );

        if (userId.length() > 0) {
            criteria.add(Restrictions.eq("userLoginHistoryPk.userLoginId", userId) );
        }

        criteria.addOrder(Order.asc("userLoginHistoryPk.fromDate"));
        return criteria.list();
    }


    @Transactional(readOnly = true)
    public List<TransactionLog> searchTransactionLog(Map<String, Object> param) {
        Date startDate = (Date)param.get(ParameterMapName.PARAM_STARTDATE);
        Date endDate = (Date)param.get(ParameterMapName.PARAM_ENDDATE);
        String userId = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_USERID));
        String description = StringUtils.checkNull((String)param.get(ParameterMapName.PARAM_DESCRIPTION));

        if (startDate != null) {
            startDate = DateUtils.getStartOfDay(startDate);
        }
        if (endDate != null) {
            endDate = DateUtils.getEndOfDay235959(endDate);
        }

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TransactionLog.class);
        criteria.add(Restrictions.ge("logDate", startDate) );
        criteria.add(Restrictions.le("logDate", endDate) );

        if (userId.length() > 0) {
            criteria.add(Restrictions.eq("userId", userId) );
        }

        if (description.length() > 0) {
            criteria.add(Restrictions.ilike("description", description, MatchMode.ANYWHERE) );
        }

        criteria.addOrder(Order.asc("logDate"));
        return criteria.list();
    }





}

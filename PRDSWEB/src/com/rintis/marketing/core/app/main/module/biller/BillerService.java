package com.rintis.marketing.core.app.main.module.biller;

import com.rintis.marketing.beans.bean.biller.BillerAnomalyBean;
import com.rintis.marketing.beans.bean.biller.DetailBillerAnomalyBean;
import com.rintis.marketing.beans.bean.biller.TmsBillerBean;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.beans.entity.BillerAnomalyInstitusiParam;
import com.rintis.marketing.beans.entity.BillerAnomalyParam;
import com.rintis.marketing.beans.entity.BillerDummy;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.dao.BillerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.inject.Named;
import java.util.List;

/*Add by aaw 20211110*/

@Service
@Qualifier(SpringBeanConstant.SPRING_BEAN_BILLER_SERVICE)
@Named(SpringBeanConstant.SPRING_BEAN_BILLER_SERVICE)
public class BillerService implements  IBillerService {

    @Autowired
    private BillerDao billerDao;

    @Override
    public List<PanTrxidBean> listTrxIdBiller(String trx) throws Exception {
        return billerDao.listTrxIdBiller(trx);
    }

    @Override
    public List<BillerAnomalyBean> listAnomalyBiller1H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return billerDao.listBiller1H(periodfr, periodto, timefr, timeto, trxtype, whitelist, bank_id);
    }

    @Override
    public List<BillerAnomalyBean> listAnomalyBiller3H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return billerDao.listBiller3H(periodfr, periodto, timefr, timeto, trxtype, whitelist, bank_id);
    }

    @Override
    public List<BillerAnomalyBean> listAnomalyBiller6H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return billerDao.listBiller6H(periodfr, periodto, timefr, timeto, trxtype, whitelist, bank_id);
    }

    @Override
    public List<BillerAnomalyBean> listAnomalyBiller1D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return billerDao.listBiller1D(periodfr, periodto, trxtype,  whitelist, bank_id);
    }

    @Override
    public List<BillerAnomalyBean> listAnomalyBiller3D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id) throws Exception {
        return billerDao.listBiller3D(periodfr, periodto, trxtype, whitelist, bank_id);
    }

    @Override
    public List<DetailBillerAnomalyBean> listDetailBiller(String period, String timeFr, String timeTo, String instCode, String custNo, String custName, String tranType, String periodType, String periodFrom, String periodTo) throws Exception {
        return billerDao.listDetailBiller(period, timeFr, timeTo, instCode, custNo, custName, tranType, periodType, periodFrom, periodTo);
    }

    @Override
    public TmsBillerBean findBillerById(String billerId) throws Exception {
        return billerDao.getDataBiller(billerId);
    }

    @Override
    public void saveBillerTreshold(BillerDummy billerDummy) throws Exception {
        billerDao.save(billerDummy);
    }

    @Override
    public boolean checkTresholdExist(String instCd, String custNo) throws Exception {
        boolean res = false;
        BillerDummy billerDummy = billerDao.checkExistTreshold(instCd, custNo);
        if (billerDummy != null) {
            res = true;
        }
        return res;
    }

    @Override
    public List<BillerDummy> listWhitelistBiller() throws Exception {
        return (List<BillerDummy>)(Object) billerDao.findAll(BillerDummy.class, "instCd", true) ;
    }

    @Override
    public void deleteWhitelistBiller(String instCd, String custNo) throws Exception {
        BillerDummy billerDummy = billerDao.checkExistTreshold(instCd, custNo);
        if (billerDummy != null) {
            billerDao.delete(billerDummy);
        }
    }

    @Override
    public List<TmsBillerBean> fetchAllActiveBiller() throws Exception {
        return billerDao.allDataBillerActive();
    }

    @Override
    public BillerDummy findBillerDummyById(String instCd, String custNo) throws Exception {
        return billerDao.checkExistTreshold(instCd, custNo);
    }

    @Override
    public void updateWhitelistBiller(BillerDummy billerDummy) throws Exception {
        billerDao.update(billerDummy);
    }

    @Override
    public List<BillerAnomalyParam> fetchBillerAnomalyParam() throws Exception {
        return (List<BillerAnomalyParam>)(Object)billerDao.findAll(BillerAnomalyParam.class, "trxId", true);
    }

    @Override
    public BillerAnomalyParam fetchBillerParamByTrxId(String trxId) throws Exception {
        return (BillerAnomalyParam) billerDao.get(BillerAnomalyParam.class, trxId);
    }

    @Override
    public void updateBillerAnomalyParam(BillerAnomalyParam billerAnomalyParam) throws Exception {
        billerDao.update(billerAnomalyParam);
    }

    @Override
    public List<BillerAnomalyInstitusiParam> fetchBillerAnomalyInstitusiParam(String billerId) throws Exception {
        return billerDao.fetchListBillerInsitusiParam(billerId);
    }

    @Override
    public List<BillerAnomalyInstitusiParam> checkBillerInstitusiExist(String instCd) throws Exception {
        List<BillerAnomalyInstitusiParam> data = billerDao.fetchListBillerInsitusiParam(instCd);
        return data;
    }

    @Override
    public void deleteBillerAnomalyInstitusi(String billerId) throws Exception {
        List<BillerAnomalyInstitusiParam> data = billerDao.fetchListBillerInsitusiParam(billerId);
        if (data.size() > 0) {
            for (BillerAnomalyInstitusiParam billerAnomalyInstitusiParam : data) {
                billerDao.delete(billerAnomalyInstitusiParam);
            }
        }
    }

    @Override
    public List<BillerAnomalyInstitusiParam> listBillerInstitutionParam() throws Exception {
        return billerDao.listBillerInstitusiParameter();
    }

    @Override
    public void saveMultipleBillerAnomalyinstitution(List<BillerAnomalyInstitusiParam> listBillerAnomalyInstitusiParam) throws Exception {
        if (listBillerAnomalyInstitusiParam.size() > 0) {
            for (BillerAnomalyInstitusiParam biller : listBillerAnomalyInstitusiParam) {
                billerDao.save(biller);
            }
        }
    }

    @Override
    public List<BillerAnomalyInstitusiParam> fetchDataBillerParamInstitusiByID(String instCd) throws Exception {
        return billerDao.fetchDataBillerParamInstitusiByID(instCd);
    }

    @Override
    public void updateMultipleBillerAnomalyinstitution(List<BillerAnomalyInstitusiParam> listBillerAnomalyInstitusiParam) throws Exception {
        if (listBillerAnomalyInstitusiParam.size() > 0) {
            for (BillerAnomalyInstitusiParam biller : listBillerAnomalyInstitusiParam) {
                billerDao.update(biller);
            }
        }
    }
}
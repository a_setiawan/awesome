package com.rintis.marketing.core.app.main.dao;

import com.rintis.marketing.beans.bean.bi.PanAnomalyEmailBean;
import com.rintis.marketing.beans.bean.bi.SequentialAnomalyBean;
import com.rintis.marketing.beans.bean.blocked.BlockedAdministrationDetail;
import com.rintis.marketing.beans.bean.blocked.BlockedAdministrationHeader;
import com.rintis.marketing.beans.bean.blocked.PanHistoryBean;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.beans.bean.sequential.SequentialParamBean;
import com.rintis.marketing.core.SpringBeanConstant;
import org.hibernate.SQLQuery;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by aabraham on 20/04/2021.
 */

@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_SEQUENTIAL_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_SEQUENTIAL_DAO)
public class SequentialDao extends AbstractDao{

    @Transactional(readOnly=true)
    public List listParam(){
        String sql = "select \n" +
                "ti.trx_name as \"trx\", \n" +
                "tp.min_trx_app_1d as \"mintrxapp1d\", \n" +
                "tp.min_trx_tot_1d as \"mintrxtot1d\", \n" +
                "tp.min_amt_app_1d as \"minamtapp1d\", \n" +
                "tp.min_amt_tot_1d as \"minamttot1d\" \n" +
                "from tms_param_seq tp join tms_trxid ti on ti.trx_id = tp.trx_id order by tp.trx_id";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(SequentialParamBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List getParam(String trx){
        String sql = "select \n" +
                "ti.trx_name as \"trx\", \n" +
                "tp.min_trx_app_1d as \"mintrxapp1d\", \n" +
                "tp.min_trx_tot_1d as \"mintrxtot1d\", \n" +
                "tp.min_amt_app_1d as \"minamtapp1d\", \n" +
                "tp.min_amt_tot_1d as \"minamttot1d\" \n" +
                "from tms_param_seq tp join tms_trxid ti on ti.trx_id = tp.trx_id where tp.trx_id = :trx";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("trx", trx);
        query.setResultTransformer(new AliasToBeanResultTransformer(SequentialParamBean.class));
        return query.list();
    }

    @Transactional
    public void updateParam(String trx, BigDecimal mintrxapp1d, BigDecimal minamtapp1d, BigDecimal mintrxtot1d, BigDecimal minamttot1d) {

        String sql = "update tms_param_seq set min_trx_app_1d = :mintrxapp1d, min_amt_app_1d = :minamtapp1d, min_trx_tot_1d = :mintrxtot1d, min_amt_tot_1d = :minamttot1d where trx_id = :trx_id";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        String trx_id = "";
        if(trx.equals("POS/Debit")){
            trx_id = "01";
        }else if(trx.equals("Cash Withdrawal")){
            trx_id = "10";
        }else if(trx.equals("Payment")){
            trx_id = "17";
        }else if(trx.equals("Transfer Debet/Kredit")){
            trx_id = "40";
        }else if(trx.equals("Top-up E-Money")){
            trx_id = "85";
        }else if(trx.equals("QR")){
            trx_id = "29";
        }

        query.setString("trx_id", trx_id);
        query.setBigDecimal("mintrxapp1d", mintrxapp1d);
        query.setBigDecimal("minamtapp1d", minamtapp1d);
        query.setBigDecimal("mintrxtot1d", mintrxtot1d);
        query.setBigDecimal("minamttot1d", minamttot1d);
        query.executeUpdate();
    }

    @Transactional(readOnly = true)
    public List<PanTrxidBean> listTrxId() throws Exception {

        String sql = "" +
                "select trx_id as \"trxid\",  trx_name as \"trxname\" \n" +
                "from tms_TrxId " +
                "where trx_id in (10,40,17,29,85,01)" +
                "order by trx_id ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanTrxidBean.class));
        return query.list();
    }

    @Transactional(readOnly = true)
    public List<PanTrxidBean> listTrxIdBlocked() throws Exception {

        String sql = "" +
                "select trx_id as \"trxid\",  trx_name as \"trxname\" \n" +
                "from tms_TrxId " +
                "where trx_id in (10,40,17,29,85,01)" +
                "order by trx_id ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanTrxidBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listSequentialAnomaly(String periodFr, String periodTo, String trxtype, String bank_id){
        String sql = "select \n" +
                "to_char(to_date(ta.period,'YYMMDD'), 'YYMMDD') as \"period\", \n" +
                "to_char(to_date(ta.period,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "ta.trx_type as \"trxtype\", \n" +
                "ti.trx_name as \"trantype\", \n" +
                "ta.pan as \"pan\", \n" +
                "ta.pengirim as \"pengirim\", \n" +
                "bc.bank_name as \"issuerbank\", \n" +
                "ta.bank_id as \"bankid\", \n" +
                "ta.freq_app as \"freqapp\", \n" +
                "ta.freq_tot as \"freqtot\", \n" +
                "nvl(ta.amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(ta.amt_tot/100,0) as \"totalamttot\", \n" +
                "nvl(ta.status, 'TRUE') as \"status\" \n" +
                "from \n" +
                "tms_trxsequential1day ta \n" +
                "join \n" +
                "tms_trxid ti on ti.trx_id = ta.trx_id \n" +
                "join \n" +
                "v_tms_bank_ca bc on ta.bank_id = bc.bank_id \n" +
                "where \n" +
                "ta.period >= :periodFr and ta.period <= :periodTo  and ta.trx_id = :trxtype \n";

        if(bank_id != ""){
            sql = sql + "and ta.bank_id = bank_id \n";
        }

        sql = sql + "order by ta.period, ti.trx_name, ta.pan, ta.bank_id ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodFr", periodFr);
        query.setString("periodTo", periodTo);
        query.setString("trxtype", trxtype);
        query.setResultTransformer(new AliasToBeanResultTransformer(SequentialAnomalyBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listPanBank(String pan, String bankid){
        String sql = "select pan as \"pan\" from tms_trxsequential1day where pan = :pan and bank_id = :bankid and status is null";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan);
        query.setString("bankid", bankid);
        return query.list();
    }

    @Transactional
    public void saveBlockedPanHeader(String docId, Date createDate, String createBy, Date statusDate, String status, String notes) {
        String sql = "insert into tms_blocked_header values (:docId, :createDate, :createBy, :statusDate, :status, :notes)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.setTimestamp("createDate", createDate);
        query.setString("createBy", createBy);
        query.setTimestamp("statusDate", statusDate);
        query.setString("status", status);
        query.setString("notes", notes);
        query.executeUpdate();
    }

    @Transactional
    public void saveBlockedPanDetail(String docId, String pan, String bank_id, String blocked_to, String notes) {
        String sql = "insert into tms_blocked_detail (document_id, pan, bank_id, blocked_to, notes) values (:docId, :pan, :bank_id, :blocked_to, :notes)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.setString("pan", pan);
        query.setString("bank_id", bank_id);
        query.setString("blocked_to", blocked_to);
        query.setString("notes", notes);
        query.executeUpdate();
    }

    @Transactional
    public void updatePan(String pan, String status) {

        String sql = "update tms_trxsequential1day set status = :status where pan = :pan";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan);
        query.setString("status", status);
        query.executeUpdate();
    }

    @Transactional(readOnly = true)
    public String getDocId(String date, String kode) {
        String sql = "select :kode || :date || lpad(count(document_id) +  1, 3, '0') docId from tms_blocked_header  \n" +
                "where to_char(created_date, 'RRRRMMDD') = :date ";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("date", date);
        query.setString("kode", kode);
        return (String)query.uniqueResult();
    }

    @Transactional(readOnly=true)
    public List listBlockedAdministrationHeader(String docId){
        String sql = "select \n" +
                "document_id as \"docId\", \n" +
                "to_char(created_date, 'DD-MM-YYYY HH24:MI:SS') as \"createdDate\", \n" +
                "created_by as \"createdBy\", \n" +
                "to_char(status_date, 'DD-MM-YYYY HH24:MI:SS') as \"statusDate\", \n" +
                "upper(status) as \"status\", \n" +
                "notes as \"notes\" \n" +
                "from \n" +
                "tms_blocked_header \n" +
                "where \n" +
                "document_id = :docId";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.setResultTransformer(new AliasToBeanResultTransformer(BlockedAdministrationHeader.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listBlockedAdministrationHeaderAll(){
        String sql = "select \n" +
                "document_id as \"docId\", \n" +
                "to_char(created_date, 'DD-MM-YYYY HH24:MI:SS') as \"createdDate\", \n" +
                "created_by as \"createdBy\", \n" +
                "to_char(status_date, 'DD-MM-YYYY HH24:MI:SS') as \"statusDate\", \n" +
                "upper(status) as \"status\", \n" +
                "notes as \"notes\", \n" +
                "(case when status = 'Created' and notes is null then 'TRUE' else 'FALSE' end) as \"enabled\" \n" +
                "from \n" +
                "tms_blocked_header";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(BlockedAdministrationHeader.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listBlockedAdministrationDetail(String docId){
        String sql = "select \n" +
                "dt.document_id as \"docId\", \n" +
                "dt.pan as \"pan\", \n" +
                "dt.bank_id as \"bankId\", \n" +
                "bc.bank_name as \"bankName\", \n" +
                "dt.blocked_to as \"blockedTo\", \n" +
                "dt.notes as \"notes\" \n" +
                "from \n" +
                "tms_blocked_detail dt \n" +
                "join \n" +
                "v_tms_bank_ca bc on dt.bank_id = bc.bank_id \n" +
                "where \n" +
                "dt.document_id = :docId";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.setResultTransformer(new AliasToBeanResultTransformer(BlockedAdministrationDetail.class));
        return query.list();
    }

    @Transactional
    public void deleteBlockedAdministrationDetail(String docId, String pan) {
        String sql = "delete from tms_blocked_detail where document_id = :docId and pan = :pan";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.setString("pan", pan);
        query.executeUpdate();
    }

    @Transactional(readOnly=true)
    public List getEmail(){
        String sql = "select \n" +
                "distinct(ul.email) as \"useremail\" \n" +
                "from tms_user_login ul join tms_menu_role mr on ul.role_type_id = mr.role_type_id where mr.menu_id = '0602'";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyEmailBean.class));
        return query.list();
    }

    @Transactional
    public void deleteBlockedAdministrationDetail(String docId) {
        String sql = "delete from tms_blocked_detail where document_id = :docId";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.executeUpdate();
    }

    @Transactional
    public void deleteBlockedAdministrationHeader(String docId) {
        String sql = "delete from tms_blocked_header where document_id = :docId";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.executeUpdate();
    }

    @Transactional
    public void updatePanDetail(String docId, String status) {
        String sql = "update tms_trxsequential1day set status = '" + status + "' where pan in (select pan from tms_blocked_detail where document_id = :docId)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.executeUpdate();
    }

    @Transactional
    public void addPanBlockedHistory(String docId, String datetime, String user_id, String status) {
        String sql = "insert into tms_blocked_pan_history (document_id, datetime, user_id, pan, bank_id, status, notes) select document_id, TO_TIMESTAMP('" + datetime +"', 'DD-MM-YYYY HH24:MI:SS') as datetime, '" + user_id +"', pan, bank_id, '" + status + "', '' from tms_blocked_detail where document_id = :docId";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.executeUpdate();
    }

    @Transactional(readOnly=true)
    public List listDocBlocked(String status){
        String sql = "select \n" +
                "document_id as \"docId\" \n" +
                "from tms_blocked_header where status = :status and document_id like 'B%' \n";

                if(status.equals("Created")){
                    sql = sql + "and notes = 'Request' \n";
                }

                sql = sql + "order by document_id";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("status", status);
        query.setResultTransformer(new AliasToBeanResultTransformer(BlockedAdministrationHeader.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listDocUnblocked(String status){
        String sql = "select \n" +
                "document_id as \"docId\" \n" +
                "from tms_blocked_header where status = :status and document_id like 'U%' \n";

                if(status.equals("Created")){
                    sql = sql + "and notes = 'Request' \n";
                }

                sql = sql + "order by document_id";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("status", status);
        query.setResultTransformer(new AliasToBeanResultTransformer(BlockedAdministrationHeader.class));
        return query.list();
    }

    @Transactional
    public void insertPgw(String docId) {
        String sql = "insert into MT_B_PAN@LINK_PGW_UAT (pan, issuer) select bd.pan pan, v.bank_code issuer from tms_blocked_detail bd join v_tms_bank_ca v on bd.bank_id = v.bank_id where bd.document_id = :docId and bd.blocked_to = 'PGW'";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.executeUpdate();
    }

    @Transactional
    public void insertB24(String docId) {
        String sql = "insert into TMS_BLOCKED_PAN_B24 (document_id, block_number, block_type, block_status, bank_code, datetime, flag) select bd.document_id document_id, bd.pan block_number, '1' block_type, 'Y' block_status, v.bank_code issuer, sysdate datetime, 'P' flag from tms_blocked_detail bd join v_tms_bank_ca v on bd.bank_id = v.bank_id where bd.document_id = :docId and bd.blocked_to = 'B24'";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        System.out.println(sql);
        query.executeUpdate();
    }

    @Transactional
    public void updateBlockedHeader(String docId, String datetime, String status) {
        String sql = "update tms_blocked_header set status = :status, status_date = TO_TIMESTAMP(:datetime, 'DD-MM-YYYY HH24:MI:SS') where document_id = :docId";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.setString("datetime", datetime);
        query.setString("status", status);
        query.executeUpdate();
    }

    @Transactional
    public void updateBlockedNotesHeader(String docId, String notes) {
        String sql = "update tms_blocked_header set notes = :notes where document_id = :docId";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.setString("notes", notes);
        query.executeUpdate();
    }

    @Transactional
    public void addPanBlockedApproval(String docId, String datetime, String user_id, String status) {
        String sql = "insert into tms_blocked_approval_history (document_id, approval_date, approved_by, status, notes) values (:docId, TO_TIMESTAMP(:datetime, 'DD-MM-YYYY HH24:MI:SS'), :user_id, :status, '')";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.setString("datetime", datetime);
        query.setString("user_id", user_id);
        query.setString("status", status);
        query.executeUpdate();
    }

    @Transactional(readOnly=true)
    public List listUnblockedDetail(){
        String sql = "select \n" +
                "ta.pan as \"pan\", \n" +
                "bc.bank_name as \"issuerbank\", \n" +
                "ta.bank_id as \"bankid\" \n" +
                "from \n" +
                "tms_trxsequential1day ta \n" +
                "join \n" +
                "v_tms_bank_ca bc on ta.bank_id = bc.bank_id \n" +
                "where \n" +
                "upper(ta.status) = 'BLOCKED' \n" +
                "group by \n" +
                "ta.pan, bc.bank_name, ta.bank_id \n" +
                "union all \n" +
                "select \n" +
                "ml.pan as \"pan\", \n" +
                "bc.bank_name as \"issuerbank\", \n" +
                "ml.bank_id as \"bankid\" \n" +
                "from \n" +
                "tms_blocked_manual_list ml \n" +
                "join \n" +
                "v_tms_bank_ca bc on ml.bank_id = bc.bank_id \n" +
                "where \n" +
                "upper(ml.status) = 'BLOCKED' \n" +
                "group by \n" +
                "ml.pan, bc.bank_name, ml.bank_id \n";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(SequentialAnomalyBean.class));
        return query.list();
    }

    @Transactional
    public void deletePgw(String docId) {
        String sql = "delete from MT_B_PAN@LINK_PGW_UAT where pan in (select pan from tms_blocked_detail where document_id = :docId)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.executeUpdate();
    }

    @Transactional(readOnly=true)
    public List listHistoryPan(){
        String sql = "select \n" +
                "bh.document_id as \"document_id\", \n" +
                "to_char(bh.datetime, 'DD-MM-YYYY HH24:MI:SS') as \"datetime\", \n" +
                "bh.pan as \"pan\", \n" +
                "bh.user_id || ' - ' || ul.user_name as \"user_id\", \n" +
                "bh.bank_id as \"bank_id\", \n" +
                "bc.bank_name as \"bank_name\", \n" +
                "bh.status as \"status\" \n" +
                "from \n" +
                "tms_blocked_pan_history bh \n" +
                "join \n" +
                "v_tms_bank_ca bc on bh.bank_id = bc.bank_id \n" +
                "join \n" +
                "tms_user_login ul on ul.user_login_id = bh.user_id \n";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanHistoryBean.class));
        return query.list();
    }

    @Transactional
    public void updatePanDetailManual(String docId, String status) {
        String sql = "update tms_blocked_manual_list set status = '" + status + "' where pan in (select pan from tms_blocked_detail where document_id = :docId)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        query.executeUpdate();
    }

    @Transactional
    public void deletePanDetailManual(String bankId, String pan) {
        String sql = "delete from tms_blocked_manual_list where bank_id = :bankId and pan = :pan";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("bankId", bankId);
        query.setString("pan", pan);
        query.executeUpdate();
    }

    @Transactional
    public void savePanDetailManual(String trxType, String bank_id, String pan, String status) {
        String sql = "insert into tms_blocked_manual_list values (:trxType, :bank_id, :pan, :status)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("trxType", trxType);
        query.setString("bank_id", bank_id);
        query.setString("pan", pan);
        query.setString("status", status);
        query.executeUpdate();
    }

    @Transactional(readOnly = true)
    public String getEmailDocId(String docId) {
        String sql = "select ul.email email from tms_blocked_header bh join tms_user_login ul on bh.created_by = ul.user_login_id  \n" +
                "where bh.document_id = :docId ";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        return (String)query.uniqueResult();
    }

    @Transactional(readOnly = true)
    public String getApprovedBy(String docId) {
        String sql = "select nvl(approved_by, '-') from tms_blocked_approval_history where document_id = :docId ";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("docId", docId);
        return (String)query.uniqueResult();
    }

    @Transactional(readOnly = true)
    public BigDecimal getCheckPan(String pan) {
        String sql = "select count(bh.status) from tms_blocked_header bh join tms_blocked_detail bd on bh.document_id = bd.document_id  \n" +
                "where bh.status = 'Created' and bd.pan = :pan";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan);
        return (BigDecimal)query.uniqueResult();
    }

    @Transactional(readOnly = true)
    public BigDecimal getCheckPanManual(String pan) {
        String sql = "select count(status) from tms_blocked_manual_list  \n" +
                "where status = 'Blocked' and pan = :pan";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan);
        return (BigDecimal)query.uniqueResult();
    }
}

package com.rintis.marketing.core.app.main.module.menu;

import com.rintis.marketing.beans.entity.Menu;
import com.rintis.marketing.beans.entity.MenuRoleType;
import com.rintis.marketing.beans.bean.menu.UserMenuBean;

import java.util.List;



public interface IMenuService {

    public List<Menu> listMenuByRole(String userId, String parentId) throws Exception;
    public List<Menu> listMenuByRoleBank(String userId, String parentId) throws Exception;


    public List<MenuRoleType> listMenuRoleType(Integer hidden) throws Exception;
    public MenuRoleType getMenuRoleType(Integer menuRoleTypeId) throws Exception;
    public void createMenuRoleType(MenuRoleType menuRoleType) throws Exception;
    public void updateMenuRoleType(MenuRoleType menuRoleType) throws Exception;
    public void deleteMenuRoleType(Integer menuRoleTypeId) throws Exception;
    public MenuRoleType getMenuRoleByName(String name) throws Exception;

    public List<UserMenuBean> listRoleAllMenu(Integer roleTypeId, String parentId, Integer active) throws Exception;

    public void updateRoleMenu(Integer roleTypeId, String menuId, boolean status) throws Exception;
    public void selectAllRoleMenu(Integer roleTypeId) throws Exception;
    public void clearAllRoleMenu(Integer roleTypeId) throws Exception;

    public List<Menu> listReportMenu(String userId) throws Exception;
    public Menu getMenu(String menuId) throws Exception;

    public boolean checkMenuRoleTypeUsed(Integer roleTypeId) throws Exception;
}

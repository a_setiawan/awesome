package com.rintis.marketing.core.app.main.module.alert;

import com.rintis.marketing.beans.bean.bi.AlertThresholdBean;
import com.rintis.marketing.beans.entity.AlertThreshold;
import com.rintis.marketing.beans.entity.AlertThresholdGroup;
import com.rintis.marketing.beans.entity.AlertThresholdGroupPk;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.dao.AlertDao;
import com.rintis.marketing.core.utils.MathUtils;
import com.rintis.marketing.core.utils.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.util.List;

@Service
@Qualifier(SpringBeanConstant.SPRING_BEAN_ALERT_SERVICE)
@Named(SpringBeanConstant.SPRING_BEAN_ALERT_SERVICE)
public class AlertService implements IAlertService {
    private Logger log = Logger.getLogger(this.getClass());
    @Autowired
    private AlertDao alertDao;

    @Override
    @Transactional(readOnly = true)
    public AlertThresholdGroup getAlertThresholdGroup(String groupId, String trxId, String trxIndicatorId, String timeframe) {
        AlertThresholdGroupPk pk = new AlertThresholdGroupPk();
        pk.setGroupBankId(groupId);
        pk.setTrxId(trxId);
        pk.setTrxIndicatorId(trxIndicatorId);
        pk.setTimeframe(timeframe);
        return (AlertThresholdGroup)alertDao.get(AlertThresholdGroup.class, pk);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AlertThresholdGroup> listAlertThresholdDetail(String groupId, String trxId) throws Exception {
        return alertDao.listAlertThresholdDetail(groupId, trxId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AlertThresholdGroup> listAlertThresholdDetail(String groupId) throws Exception {
        return alertDao.listAlertThresholdDetail(groupId);
    }

    @Override
    @Transactional
    public void updateAlertThresholdGroup(List<AlertThresholdGroup> list) throws Exception {
        if (list.size() == 0) return;

        AlertThresholdGroup ato = list.get(0);
        List<AlertThresholdGroup> listat = listAlertThresholdDetail(ato.getAlertThresholdGroupPk().getGroupBankId(),
                ato.getAlertThresholdGroupPk().getTrxId());
        for(AlertThresholdGroup atx : listat) {
            alertDao.delete(atx);
        }

        for(AlertThresholdGroup at : list) {
            at.setActiveM1(StringUtils.bool2int(at.getActiveM1Bool()) );
            at.setActiveM2(StringUtils.bool2int(at.getActiveM2Bool()) );
            at.setActiveM3(StringUtils.bool2int(at.getActiveM3Bool()) );
            at.setActiveM4(StringUtils.bool2int(at.getActiveM4Bool()) );

            AlertThresholdGroupPk pk = new AlertThresholdGroupPk();
            pk.setGroupBankId(at.getAlertThresholdGroupPk().getGroupBankId());
            pk.setTrxId(at.getAlertThresholdGroupPk().getTrxId());
            pk.setTrxIndicatorId(at.getAlertThresholdGroupPk().getTrxIndicatorId());
            pk.setTimeframe(at.getAlertThresholdGroupPk().getTimeframe());

            AlertThresholdGroup alertThreshold = new AlertThresholdGroup();
            alertThreshold.setAlertThresholdGroupPk(pk);
            alertThreshold.setActiveM1(at.getActiveM1());
            alertThreshold.setActiveM2(at.getActiveM2());
            alertThreshold.setActiveM3(at.getActiveM3());
            alertThreshold.setActiveM4(at.getActiveM4());

            alertThreshold.setStartTime(at.getStartTime());
            alertThreshold.setEndTime(at.getEndTime());

            alertThreshold.setPrevNTt(at.getPrevNTt());
            alertThreshold.setPrevNAp(at.getPrevNAp());
            alertThreshold.setPrevNDc(at.getPrevNDc());
            alertThreshold.setPrevNDf(at.getPrevNDf());

            alertThreshold.setMinTrxPeriodTt(MathUtils.zeroAsNull(at.getMinTrxPeriodTt()));
            alertThreshold.setMinTrxPeriodAp(MathUtils.zeroAsNull(at.getMinTrxPeriodAp()));
            alertThreshold.setMinTrxPeriodDc(MathUtils.zeroAsNull(at.getMinTrxPeriodDc()));
            alertThreshold.setMinTrxPeriodDf(MathUtils.zeroAsNull(at.getMinTrxPeriodDf()));

            alertThreshold.setMinTrxDayTt(MathUtils.zeroAsNull(at.getMinTrxDayTt()));
            alertThreshold.setMinTrxDayAp(MathUtils.zeroAsNull(at.getMinTrxDayAp()));
            alertThreshold.setMinTrxDayDc(MathUtils.zeroAsNull(at.getMinTrxDayDc()));
            alertThreshold.setMinTrxDayDf(MathUtils.zeroAsNull(at.getMinTrxDayDf()));

            alertThreshold.setTrhTrxPeriodPctTt(MathUtils.zeroAsNull(at.getTrhTrxPeriodPctTt()));
            alertThreshold.setTrhTrxPeriodPctAp(MathUtils.zeroAsNull(at.getTrhTrxPeriodPctAp()));
            alertThreshold.setTrhTrxPeriodPctDc(MathUtils.zeroAsNull(at.getTrhTrxPeriodPctDc()));
            alertThreshold.setTrhTrxPeriodPctDf(MathUtils.zeroAsNull(at.getTrhTrxPeriodPctDf()));

            alertThreshold.setTrhTrxDayPctTt(MathUtils.zeroAsNull(at.getTrhTrxDayPctTt()));
            alertThreshold.setTrhTrxDayPctAp(MathUtils.zeroAsNull(at.getTrhTrxDayPctAp()));
            alertThreshold.setTrhTrxDayPctDc(MathUtils.zeroAsNull(at.getTrhTrxDayPctDc()));
            alertThreshold.setTrhTrxDayPctDf(MathUtils.zeroAsNull(at.getTrhTrxDayPctDf()));

            alertThreshold.setTrhTrxDayPctTtHoliday(MathUtils.zeroAsNull(at.getTrhTrxDayPctTtHoliday()));
            alertThreshold.setTrhTrxDayPctApHoliday(MathUtils.zeroAsNull(at.getTrhTrxDayPctApHoliday()));
            alertThreshold.setTrhTrxDayPctDcHoliday(MathUtils.zeroAsNull(at.getTrhTrxDayPctDcHoliday()));
            alertThreshold.setTrhTrxDayPctDfHoliday(MathUtils.zeroAsNull(at.getTrhTrxDayPctDfHoliday()));

            alertThreshold.setMinTrxPctDc(MathUtils.zeroAsNull(at.getMinTrxPctDc()));
            alertThreshold.setMinTrxPctDf(MathUtils.zeroAsNull(at.getMinTrxPctDf()));
            alertThreshold.setTrhPctDc(MathUtils.zeroAsNull(at.getTrhPctDc()));
            alertThreshold.setTrhPctDf(MathUtils.zeroAsNull(at.getTrhPctDf()));


            alertDao.save(alertThreshold);

        }
    }

    @Override
    @Transactional
    public void updateAlertThresholdGroupV2(List<AlertThresholdGroup> list) throws Exception {
        if (list.size() == 0) return;

        AlertThresholdGroup ato = list.get(0);
        List<AlertThresholdGroup> listat = listAlertThresholdDetail(ato.getAlertThresholdGroupPk().getGroupBankId(),
                ato.getAlertThresholdGroupPk().getTrxId());
        for(AlertThresholdGroup atx : listat) {
            alertDao.delete(atx);
        }

        for(AlertThresholdGroup at : list) {
            at.setActiveM1(StringUtils.bool2int(at.getActiveM1Bool()));
            at.setActiveM2(StringUtils.bool2int(at.getActiveM2Bool()));
            at.setActiveM3(StringUtils.bool2int(at.getActiveM3Bool()));
            at.setActiveM4(StringUtils.bool2int(at.getActiveM4Bool()));

            AlertThresholdGroupPk pk = new AlertThresholdGroupPk();
            pk.setGroupBankId(at.getAlertThresholdGroupPk().getGroupBankId());
            pk.setTrxId(at.getAlertThresholdGroupPk().getTrxId());
            pk.setTrxIndicatorId(at.getAlertThresholdGroupPk().getTrxIndicatorId());
            pk.setTimeframe(at.getAlertThresholdGroupPk().getTimeframe());

            AlertThresholdGroup alertThreshold = new AlertThresholdGroup();
            alertThreshold.setAlertThresholdGroupPk(pk);
            alertThreshold.setActiveM1(at.getActiveM1());
            alertThreshold.setActiveM2(at.getActiveM2());
            alertThreshold.setActiveM3(at.getActiveM3());
            alertThreshold.setActiveM4(at.getActiveM4());

            alertThreshold.setStartTime(at.getStartTime());
            alertThreshold.setEndTime(at.getEndTime());

            alertThreshold.setPrevNTt(at.getPrevNTt());
            alertThreshold.setPrevNAp(at.getPrevNAp());
            alertThreshold.setPrevNDc(at.getPrevNDc());
            alertThreshold.setPrevNDf(at.getPrevNDf());

            alertThreshold.setMinTrxPeriodTt(MathUtils.zeroAsNull(at.getMinTrxPeriodTt()));
            alertThreshold.setMinTrxPeriodAp(MathUtils.zeroAsNull(at.getMinTrxPeriodAp()));
            alertThreshold.setMinTrxPeriodDc(MathUtils.zeroAsNull(at.getMinTrxPeriodDc()));
            alertThreshold.setMinTrxPeriodDf(MathUtils.zeroAsNull(at.getMinTrxPeriodDf()));

            alertThreshold.setMinTrxDayTt(MathUtils.zeroAsNull(at.getMinTrxDayTt()));
            alertThreshold.setMinTrxDayAp(MathUtils.zeroAsNull(at.getMinTrxDayAp()));
            alertThreshold.setMinTrxDayDc(MathUtils.zeroAsNull(at.getMinTrxDayDc()));
            alertThreshold.setMinTrxDayDf(MathUtils.zeroAsNull(at.getMinTrxDayDf()));

            alertThreshold.setTrhTrxPeriodPctTt(MathUtils.zeroAsNull(at.getTrhTrxPeriodPctTt()));
            alertThreshold.setTrhTrxPeriodPctAp(MathUtils.zeroAsNull(at.getTrhTrxPeriodPctAp()));
            alertThreshold.setTrhTrxPeriodPctDc(MathUtils.zeroAsNull(at.getTrhTrxPeriodPctDc()));
            alertThreshold.setTrhTrxPeriodPctDf(MathUtils.zeroAsNull(at.getTrhTrxPeriodPctDf()));

            alertThreshold.setTrhTrxDayPctTtDec(MathUtils.zeroAsNull(at.getTrhTrxDayPctTtDec()));
            alertThreshold.setTrhTrxDayPctTtInc(MathUtils.zeroAsNull(at.getTrhTrxDayPctTtInc()));
            alertThreshold.setTrhTrxDayPctTtWhDec(MathUtils.zeroAsNull(at.getTrhTrxDayPctTtWhDec()));
            alertThreshold.setTrhTrxDayPctTtWhInc(MathUtils.zeroAsNull(at.getTrhTrxDayPctTtWhInc()));
            alertThreshold.setTrhTrxDayPctTtHwDec(MathUtils.zeroAsNull(at.getTrhTrxDayPctTtHwDec()));
            alertThreshold.setTrhTrxDayPctTtHwInc(MathUtils.zeroAsNull(at.getTrhTrxDayPctTtHwInc()));

            alertThreshold.setTrhTrxDayPctApDec(MathUtils.zeroAsNull(at.getTrhTrxDayPctApDec()));
            alertThreshold.setTrhTrxDayPctApInc(MathUtils.zeroAsNull(at.getTrhTrxDayPctApInc()));
            alertThreshold.setTrhTrxDayPctApWhDec(MathUtils.zeroAsNull(at.getTrhTrxDayPctApWhDec()));
            alertThreshold.setTrhTrxDayPctApWhInc(MathUtils.zeroAsNull(at.getTrhTrxDayPctApWhInc()));
            alertThreshold.setTrhTrxDayPctApHwDec(MathUtils.zeroAsNull(at.getTrhTrxDayPctApHwDec()));
            alertThreshold.setTrhTrxDayPctApHwInc(MathUtils.zeroAsNull(at.getTrhTrxDayPctApHwInc()));

            alertThreshold.setTrhTrxDayPctDcDec(MathUtils.zeroAsNull(at.getTrhTrxDayPctDcDec()));
            alertThreshold.setTrhTrxDayPctDcInc(MathUtils.zeroAsNull(at.getTrhTrxDayPctDcInc()));
            alertThreshold.setTrhTrxDayPctDcWhDec(MathUtils.zeroAsNull(at.getTrhTrxDayPctDcWhDec()));
            alertThreshold.setTrhTrxDayPctDcWhInc(MathUtils.zeroAsNull(at.getTrhTrxDayPctDcWhInc()));
            alertThreshold.setTrhTrxDayPctDcHwDec(MathUtils.zeroAsNull(at.getTrhTrxDayPctDcHwDec()));
            alertThreshold.setTrhTrxDayPctDcHwInc(MathUtils.zeroAsNull(at.getTrhTrxDayPctDcHwInc()));

            alertThreshold.setTrhTrxDayPctDfDec(MathUtils.zeroAsNull(at.getTrhTrxDayPctDfDec()));
            alertThreshold.setTrhTrxDayPctDfInc(MathUtils.zeroAsNull(at.getTrhTrxDayPctDfInc()));
            alertThreshold.setTrhTrxDayPctDfWhDec(MathUtils.zeroAsNull(at.getTrhTrxDayPctDfWhDec()));
            alertThreshold.setTrhTrxDayPctDfWhInc(MathUtils.zeroAsNull(at.getTrhTrxDayPctDfWhInc()));
            alertThreshold.setTrhTrxDayPctDfHwDec(MathUtils.zeroAsNull(at.getTrhTrxDayPctDfHwDec()));
            alertThreshold.setTrhTrxDayPctDfHwInc(MathUtils.zeroAsNull(at.getTrhTrxDayPctDfHwInc()));

            alertThreshold.setMinTrxPctDc(MathUtils.zeroAsNull(at.getMinTrxPctDc()));
            alertThreshold.setMinTrxPctDf(MathUtils.zeroAsNull(at.getMinTrxPctDf()));
            alertThreshold.setTrhPctDc(MathUtils.zeroAsNull(at.getTrhPctDc()));
            alertThreshold.setTrhPctDf(MathUtils.zeroAsNull(at.getTrhPctDf()));

            alertDao.save(alertThreshold);

        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<AlertThresholdBean> listAlertThresholdGroupHeader() throws Exception {
        return alertDao.listAlertThresholdGroupHeader();
    }

    @Override
    @Transactional
    public void deleteAlertThresholdGroup(String groupId, String trxId) throws Exception {
        alertDao.deleteAlertThresholdGroup(groupId, trxId);
    }
}

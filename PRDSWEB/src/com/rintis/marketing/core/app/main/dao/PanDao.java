package com.rintis.marketing.core.app.main.dao;

import com.rintis.marketing.beans.bean.bi.*;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.beans.entity.PanAnomalyEmail;
import com.rintis.marketing.beans.entity.UserLoginBank;
import com.rintis.marketing.config.spring.ReadProperty;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import org.hibernate.SQLQuery;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by aabraham on 11/02/2019.
 */

@Repository
@Qualifier(SpringBeanConstant.SPRING_BEAN_PAN_DAO)
@Named(SpringBeanConstant.SPRING_BEAN_PAN_DAO)
public class PanDao extends AbstractDao{
    private String issuerbank;
    private UserLoginBank ulb;
    private ModuleFactory moduleFactory;

    @Transactional(readOnly=true)
    public List listDetailAtm(String period, String timeFr, String timeTo, String pan, String tranType, String pengirim) throws IOException{
        /*Modify by aaw 21/07/2021*/
        String linkDb = ReadProperty.dbLinkActive().getLinkAtm();
        /*End*/
        String sql = "select \n" +
                "to_char(to_date(cur_dat_add,'YYMMDD'), 'DD-MM-YYYY') as \"period\", \n" +
                "substr(cur_tim_add,1,2) || ':' || substr(cur_tim_add,3,2) || ':' || substr(cur_tim_add,5,2) as \"trxtime\", \n" +
                "atm_term_owner_name as \"terminal\", \n" +
                "atm_term_id as \"termid\", \n" +
                /*Modify by aaw 21/07/2021 menambahkan tran code = 32*/
                //"tms_bank_api.Get_Bank_Name_Fiid(case when atm_tran_cde = '40' then atm_crd_ln else '' end) as \"bankbene\", \n" +
                "tms_bank_api.Get_Bank_Name_Fiid(case when atm_tran_cde = '40' or atm_tran_cde = '32' then atm_crd_ln else '' end) as \"bankbene\", \n" +
                /*End*/
                "r1_beneficiary as \"namebene\", \n" +
                /*Modify by aaw 21/07/2021 menambahkan tran code = 32*/
                //"(case when atm_tran_cde = '40' then atm_to_acct else '' end) as \"toaccount\", \n" +
                "(case when atm_tran_cde = '40' or atm_tran_cde = '32' then atm_to_acct else '' end) as \"toaccount\", \n" +
                /*End*/
                "(case when atm_typ = '0210' and (atm_rte_responder || atm_resp = '7000' or atm_rte_responder || atm_resp = '7001') then 1 else 0 end) as \"app\", \n" +
                "(case when atm_typ = '0210' and (atm_rte_responder || atm_resp <> '7000' and atm_rte_responder || atm_resp <> '7001' and (atm_rte_responder || atm_resp = '7059' or atm_rte_responder || atm_resp = '7058' or atm_rte_responder || atm_resp = '7053' or atm_rte_responder || atm_resp = '7051')) then 1 else 0 end) as \"dc\", \n" +
                "(case when atm_typ = '0210' and (atm_rte_responder || atm_resp <> '7000' and atm_rte_responder || atm_resp <> '7001' and atm_rte_responder || atm_resp <> '7059' and atm_rte_responder || atm_resp <> '7058' and atm_rte_responder || atm_resp <> '7053' and atm_rte_responder || atm_resp <> '7051') then 1 else 0 end) as \"dcfree\", \n" +
                "(case when atm_typ = '0420' and (atm_rte_responder || atm_resp = '7000' or atm_rte_responder || atm_resp = '7001') then 1 else 0 end) as \"rvsl\", \n" +
                "nvl(atm_amt_1/100,0) as \"amount\", \n" +
                "atm_rte_responder || atm_resp as \"rc\", \n" +
                "atm_typ as \"msgtype\", \n" +
                "pos_entry_mode as \"posentrymode\", \n" +
                "tms_bankfiid_api.Get_Bank_Name(atm_term_fiid) as \"bankacq\", \n" +
                "nvl(tms_respid_api.Get_Resp_Name(atm_rte_responder || atm_resp), 'Decline Free') as \"description\" \n" +
                "from \n" +
                /*Add by aaw 8/06/21, get link db from profile active*/
                /*Production*/
                //"ilf_atm_new@LINK_ILFGPS \n" +

                /*Development*/
                //"ilf_atm_new@ILF_GPS \n" +
                linkDb  + "\n" +
                /*End add*/
                "where \n";

        //Modify by AAH 191127
        if(timeFr.replace(":", "").length() == 6){
            sql = sql  +"cur_dat_add >= :timefr \n" +
                    "and cur_dat_add <= :timeto \n" +
                    "and cur_tim_add >= '00000000' \n" +
                    "and cur_tim_add <= '24000000' \n" +
                    "and atm_tran_cde = :trantype \n" +
                    /*Modify by aaw 16/08/2021, ubah dari card_no_prikey ke card_no*/
                    "and card_no = :pan \n";
        }else{
            sql = sql  +"cur_dat_add = :period \n" +
                    "and cur_tim_add >= :timefr || '0000' \n" +
                    "and cur_tim_add <= :timeto || '0000' \n" +
                    "and atm_tran_cde = :trantype \n" +
                     /*Modify by aaw 16/08/2021, ubah dari card_no_prikey ke card_no*/
                    "and card_no = :pan \n";
        }

        /*Modify by aaw 21/07/2021, menambahkan kondisi untuk tran code 32*/
        if(tranType.equals("40") || "32".equals(tranType)){sql = sql + "and nvl(upper(trim(r1_issuer)),' ') = nvl(upper(trim(:pengirim)),' ') \n";}
        /*End modify*/

        sql = sql + "group by \n" +
                "cur_dat_add, substr(cur_tim_add,1,2) || ':' || substr(cur_tim_add,3,2) || ':' || substr(cur_tim_add,5,2), \n" +
                "atm_term_owner_name, atm_term_id, \n" +
                /*Modify by aaw 21/07/2021 menambahkan tran code = 32*/
                //"tms_bank_api.Get_Bank_Name_Fiid(case when atm_tran_cde = '40' then atm_crd_ln else '' end), \n" +
                "tms_bank_api.Get_Bank_Name_Fiid(case when atm_tran_cde = '40' or atm_tran_cde = '32' then atm_crd_ln else '' end), \n" +
                //"r1_beneficiary, (case when atm_tran_cde = '40' then atm_to_acct else '' end), \n" +
                "r1_beneficiary, (case when atm_tran_cde = '40' or atm_tran_cde = '32' then atm_to_acct else '' end), \n" +
                /*End*/
                "(case when atm_typ = '0210' and (atm_rte_responder || atm_resp = '7000' or atm_rte_responder || atm_resp = '7001') then 1 else 0 end), \n" +
                "(case when atm_typ = '0210' and (atm_rte_responder || atm_resp <> '7000' and atm_rte_responder || atm_resp <> '7001' and (atm_rte_responder || atm_resp = '7059' or atm_rte_responder || atm_resp = '7058' or atm_rte_responder || atm_resp = '7053' or atm_rte_responder || atm_resp = '7051')) then 1 else 0 end), \n" +
                "(case when atm_typ = '0210' and (atm_rte_responder || atm_resp <> '7000' and atm_rte_responder || atm_resp <> '7001' and atm_rte_responder || atm_resp <> '7059' and atm_rte_responder || atm_resp <> '7058' and atm_rte_responder || atm_resp <> '7053' and atm_rte_responder || atm_resp <> '7051') then 1 else 0 end), \n" +
                "(case when atm_typ = '0420' and (atm_rte_responder || atm_resp = '7000' or atm_rte_responder || atm_resp = '7001') then 1 else 0 end), \n" +
                "atm_amt_1, \n" +
                "atm_rte_responder || atm_resp, \n"+
                "atm_typ, \n"+
                "pos_entry_mode,\n" +
                "tms_bankfiid_api.Get_Bank_Name(atm_term_fiid), \n" +
                //Added by AAH 210426
                "atm_seq_num, \n" +
                "tms_respid_api.Get_Resp_Name(atm_rte_responder || atm_resp)";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        if(timeFr.replace(":", "").length() != 6){
            query.setString("period", period);
        }
        query.setString("timefr", timeFr.replace(":", ""));
        query.setString("timeto", timeTo.replace(":", ""));
        query.setString("trantype", tranType);
        query.setString("pan", pan);
        /*Modify by aaw 21/07/2021, menambahkan kondisi tran code 32*/
        if(tranType.equals("40") || "32".equals(tranType)){query.setString("pengirim", pengirim);}
        /*End*/
        //System.out.println("ATM - " + period + " - " + timeFr + " - " + timeTo + " - " + pan + " - " + tranType + " - " + pengirim);
        query.setResultTransformer(new AliasToBeanResultTransformer(DetailPanAnomalyBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listDetailPos(String period, String timeFr, String timeTo, String pan, String tranType, String pengirim, String status) throws IOException{
        /*Add by aaw 21/07/2021*/
        String linkDbPos =  ReadProperty.dbLinkActive().getLinkPos();
        /*End add*/
        String sql;
        if(status.trim().equals("MPM")){
            sql = "select \n" +
                    "to_char(to_date(cur_dat_add,'YYMMDD'), 'DD-MM-YYYY') as \"period\", \n" +
                    "substr(cur_tim_add,1,2) || ':' || substr(cur_tim_add,3,2) || ':' || substr(cur_tim_add,5,2) as \"trxtime\", \n" +
                    "pos_term_owner_name as \"terminal\", \n" +
                    "pos_term_id as \"termid\", \n" +
                    "'' as \"bankbene\", \n" +
                    "'' as \"namebene\", \n" +
                    "'' as \"toaccount\", \n" +
                    "(case when pos_type = '0210' and (pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001') then 1 else 0 end) as \"app\", \n" +
                    "0 as \"dc\", \n" +
                    "(case when pos_type = '0210' and (pos_responder || pos_resp <> '7000' and pos_responder || pos_resp <> '7001') then 1 else 0 end) as \"dcfree\", \n" +
                    "(case when pos_type = '0420' and (pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001') then 1 else 0 end) as \"rvsl\", \n" +
                    "nvl(pos_amt_1/100,0) as \"amount\", \n" +
                    "pos_responder || pos_resp as \"rc\", \n" +
                    "pos_type as \"msgtype\", \n" +
                    "pos_pt_srv_entry_mde as \"posentrymode\", \n" +
                    "tms_bankfiid_api.Get_Bank_Name(pos_crd_fiid) as \"bankacq\", \n" +
                    "(case when pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001' then 'Approve' else 'Decline Free' end) as \"description\" \n" +
                    "from \n" +
                    /*Modify by aaw 21/07/2021*/
                    //"ilf_pos_new@LINK_ILFGPS  \n" +
                    linkDbPos + "\n" +
                    /*End*/
                    "where \n" +
                    "cur_dat_add = :period \n" +
                    "and cur_tim_add >= :timefr || '0000' \n" +
                    "and cur_tim_add <= :timeto || '0000' \n" +
                    "and substr(pos_tran_cde, 1, 2) = :trantype \n" +
                    "and pos_acct_num = :pan \n" +
                    "group by \n" +
                    "cur_dat_add, substr(cur_tim_add,1,2) || ':' || substr(cur_tim_add,3,2) || ':' || substr(cur_tim_add,5,2), \n" +
                    "pos_term_owner_name, pos_term_id, \n" +
                    "(case when pos_type = '0210' and (pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001') then 1 else 0 end), \n" +
                    "(case when pos_type = '0210' and (pos_responder || pos_resp <> '7000' and pos_responder || pos_resp <> '7001') then 1 else 0 end), \n" +
                    "(case when pos_type = '0420' and (pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001') then 1 else 0 end), \n" +
                    "pos_amt_1, \n" +
                    "pos_responder || pos_resp, \n" +
                    "pos_type, \n" +
                    "pos_pt_srv_entry_mde, \n" +
                    "tms_bankfiid_api.Get_Bank_Name(pos_crd_fiid), \n" +
                    "(case when pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001' then 'Approve' else 'Decline Free' end)";
        }else{
            sql = "select \n" +
                    "to_char(to_date(cur_dat_add,'YYMMDD'), 'DD-MM-YYYY') as \"period\", \n" +
                    "substr(cur_tim_add,1,2) || ':' || substr(cur_tim_add,3,2) || ':' || substr(cur_tim_add,5,2) as \"trxtime\", \n" +
                    "pos_term_owner_name as \"terminal\", \n" +
                    "pos_term_id as \"termid\", \n" +
                    "'' as \"bankbene\", \n" +
                    "'' as \"namebene\", \n" +
                    "'' as \"toaccount\", \n" +
                    "(case when pos_type = '0210' and (pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001') then 1 else 0 end) as \"app\", \n" +
                    "0 as \"dc\", \n" +
                    "(case when pos_type = '0210' and (pos_responder || pos_resp <> '7000' and pos_responder || pos_resp <> '7001') then 1 else 0 end) as \"dcfree\", \n" +
                    "(case when pos_type = '0420' and (pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001') then 1 else 0 end) as \"rvsl\", \n" +
                    "nvl(pos_amt_1/100,0) as \"amount\", \n" +
                    "pos_responder || pos_resp as \"rc\", \n" +
                    "pos_type as \"msgtype\", \n" +
                    "pos_pt_srv_entry_mde as \"posentrymode\", \n" +
                    "tms_bankfiid_api.Get_Bank_Name(pos_term_fiid) as \"bankacq\", \n" +
                    "(case when pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001' then 'Approve' else 'Decline Free' end) as \"description\" \n" +
                    "from \n" +
                     /*Modify by aaw 21/07/2021*/
                    //"ilf_pos_new@LINK_ILFGPS  \n" +
                    linkDbPos + "\n" +
                    /*End*/
                    "where \n" +
                    "cur_dat_add = :period \n" +
                    "and cur_tim_add >= :timefr || '0000' \n" +
                    "and cur_tim_add <= :timeto || '0000' \n" +
                    "and substr(pos_tran_cde, 1, 2) = :trantype \n" +
                     /*Modify by aaw 16/08/2021, ubah dari card_no_prikey ke card_no*/
                    //"and card_no = :pan \n" +
                    "and card_no_prikey = :pan \n" +
                    "group by \n" +
                    "cur_dat_add, substr(cur_tim_add,1,2) || ':' || substr(cur_tim_add,3,2) || ':' || substr(cur_tim_add,5,2), \n" +
                    "pos_term_owner_name, pos_term_id, \n" +
                    "(case when pos_type = '0210' and (pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001') then 1 else 0 end), \n" +
                    "(case when pos_type = '0210' and (pos_responder || pos_resp <> '7000' and pos_responder || pos_resp <> '7001') then 1 else 0 end), \n" +
                    "(case when pos_type = '0420' and (pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001') then 1 else 0 end), \n" +
                    "pos_amt_1, \n" +
                    "pos_responder || pos_resp, \n" +
                    "pos_type, \n" +
                    "pos_pt_srv_entry_mde, \n" +
                    "tms_bankfiid_api.Get_Bank_Name(pos_term_fiid), \n" +
                    "(case when pos_responder || pos_resp = '7000' or pos_responder || pos_resp = '7001' then 'Approve' else 'Decline Free' end)";
        }

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("period", period);
        query.setString("timefr", timeFr.replace(":", ""));
        query.setString("timeto", timeTo.replace(":", ""));
        query.setString("trantype", tranType);
        query.setString("pan", pan);
        //System.out.println("POS - " + period + " - " + timeFr + " - " + timeTo + " - " + pan + " - " + tranType + " - " + status);
        //System.out.println(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(DetailPanAnomalyBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listDetailPgw(String period, String timeFr, String timeTo, String pan, String tranType, String pengirim){
        String sql = "SELECT \n" +
                "to_char(sys_trx_dt,'DD-MM-YYYY HH24:MI:SS') AS \"datetime\", \n" +
                "actn_from_id || ' - ' || TMS_BANK_CODE(actn_from_id) AS \"acquirer\", \n" +
                "inst_cd || ' - ' || inst_nm AS \"biller\", \n" +
                "trx_typ_cd_1 AS \"product_code\", \n" +
                "cust_no AS \"cust_id\", \n" +
                "audit_no AS \"stan\", \n" +
                "to_char(actn_by_trmnl) AS \"term_id\", \n" +
                "channel_cd AS \"channel_id\", \n" +
                "nvl(initiator_ref_no, ' ') AS \"ref_no\", \n" +
                "nvl(host_ref_no, ' ') AS \"biller_no\", \n" +
                "cust_nm AS \"cust_name\", \n" +
                "nvl((case when tms_trxid_map_api.get_trx_id(trx_typ_cd_1) = '37' then amt when tms_trxid_map_api.get_trx_id(trx_typ_cd_1) = '17' then paid_amt end), 0) as \"amount\", \n" +
                "to_char(err_map_cd) AS \"rc\", \n" +
                "(CASE WHEN err_map_cd = '00' or err_map_cd = '68' THEN 'Approve' ELSE 'Decline' END) AS \"description\" \n" +
                "FROM \n" +
                "psh_act_log_bs@LINK_PQS \n" +
                "WHERE \n" +
                "TRUNC(sys_trx_dt) >= TO_DATE('2021-01-01', 'YYYY-MM-DD') \n" +
                "AND TMS_TRXID_MAP_API.Get_Trx_Id(trx_typ_cd_1) = :trantype  \n" +
                "AND pan = :pan \n";

        /*Modify by aaw 27/07/2021, menambahkan untuk detail 3D*/
        if(timeFr.replace(":", "").length() == 6){
            sql = sql  + "and to_char(sys_trx_dt, 'yyyymmddhh24miss') between to_char(to_date(:timefr,'yymmdd'), 'yyyymmdd') || '00000000' || '00' AND to_char(to_date(:timeto,'yymmdd'), 'yyyymmdd') || '24000000' || '00' \n";
        }else{
            sql = sql  + "and to_char(sys_trx_dt, 'yyyymmddhh24miss') between to_char(to_date(:period,'yymmdd'), 'yyyymmdd') || :timefr || '00' AND to_char(to_date(:period,'yymmdd'), 'yyyymmdd') || :timeto || '00' \n";
        }
        /*End*/

        //System.out.println(sql + "\n" + period + "\n" + timeFr + "\n" + timeTo + "\n" + tranType);
        //System.out.println(sql);
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.addScalar("datetime", new StringType());
        query.addScalar("acquirer", new StringType());
        query.addScalar("biller", new StringType());
        query.addScalar("product_code", new StringType());
        query.addScalar("cust_id", new StringType());
        query.addScalar("stan", new StringType());
        query.addScalar("term_id", new StringType());
        query.addScalar("channel_id", new StringType());
        query.addScalar("ref_no", new StringType());
        query.addScalar("biller_no", new StringType());
        query.addScalar("cust_name", new StringType());
        query.addScalar("amount", new BigDecimalType());
        query.addScalar("rc", new StringType());
        query.addScalar("description", new StringType());

        /*Add condition by aaw 27/07/2021*/
        if(timeFr.replace(":", "").length() != 6) {
            query.setString("period", period);
        }
        /*End add*/
        query.setString("timefr", timeFr.replace(":", ""));
        query.setString("timeto", timeTo.replace(":", ""));
        query.setString("trantype", tranType);
        query.setString("pan", pan);
        query.setResultTransformer(new AliasToBeanResultTransformer(DetailPanAnomalyPaymentBean.class));
        return query.list();
    }

    @Transactional(readOnly = true)
    public String getTrxIdFromTrxName(String trxName) {
        String sql = "select replace(trx_id,'01','10') from tms_trxid \n" +
                "where upper(trx_name) = upper(:trxName) ";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("trxName", trxName);
        return (String)query.uniqueResult();
    }

    @Transactional(readOnly = true)
    public String getGroupIdFromTrxName(String trxName) {
        String sql = "select group_id from tms_trxid \n" +
                "where upper(trx_name) = upper(:trxName) ";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("trxName", trxName);
        return (String)query.uniqueResult();
    }

    //start modify be eriichie
    @Transactional(readOnly = true)
    public List<PanTrxidBean> listTrxId() throws Exception {

        String sql = "" +
                "select trx_id as \"trxid\",  trx_name as \"trxname\" \n" +
                "from tms_TrxId " +
                /*Modify by aaw 19/07/2021, penambahan kode transaksi 30,32,37*/
                "where trx_id in (10,40,17,29,30,32,37)" +
                /*End modify*/
                "order by trx_id ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);

        query.setResultTransformer(new AliasToBeanResultTransformer(PanTrxidBean.class));

        return query.list();
    }
    //end modify be erichie

    @Transactional(readOnly = true)
    public List<PanTrxidBean> listTrxIdBene() throws Exception {

        String sql = "" +
                "select trx_id as \"trxid\",  trx_name as \"trxname\" \n" +
                "from tms_TrxId " +
                "where trx_id in (40)" +
                "order by trx_id ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);

        query.setResultTransformer(new AliasToBeanResultTransformer(PanTrxidBean.class));

        return query.list();
    }

    @Transactional(readOnly = true)
    public PanTrxidBean getPanTrxId(String trxId) throws Exception {
        String sql = "" +
                "select trx_id as \"trxid\",  trx_name as \"trxname\" \n" +
                "from tms_TrxId " +
                "where trx_id = :trxId ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setParameter("trxId", trxId);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanTrxidBean.class));

        return (PanTrxidBean)query.uniqueResult();
    }

    @Transactional(readOnly=true)
    public List listPan(){
        String sql = "select pan as \"pan\", pengirim as \"pengirim\", freq_app as \"freqApp\", freq_dc as \"freqDc\", freq_dc_free as \"freqDcFree\", freq_rvsl as \"freqRvsl\", total_amt_app as \"totalAmtApp\", total_amt_dc as \"totalAmtDc\", total_amt_dc_free as \"totalAmtDcFree\", total_amt_rvsl as \"totalAmtRvsl\", total_amt as \"totalAmt\", active as \"active\" from tms_pandummy order by pan";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(WhitelistBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listMaxAmountParam(){
        String sql = "select ti.trx_name as \"trx\", tp.max_amt_tot_1h as \"maxamt1h\" from tms_param_anmly tp join tms_trxid ti on ti.trx_id = tp.trx_id order by tp.trx_id";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyParamBean.class));
        return query.list();
    }

    //start modify by erichie
    @Transactional(readOnly=true)
    public List listParam(){
        String sql = "select \n" +
                "ti.trx_name as \"trx\", \n" +
                "tp.min_trx_app_1h as \"mintrxapp1h\", \n" +
                "tp.min_trx_dc_1h as \"mintrxdc1h\", \n" +
                "tp.min_trx_dc_free_1h as \"mintrxdcfree1h\", \n" +
                "tp.min_trx_rvsl_1h as \"mintrxrvsl1h\", \n" +
                "tp.min_trx_app_3h as \"mintrxapp3h\", \n" +
                "tp.min_trx_dc_3h as \"mintrxdc3h\", \n" +
                "tp.min_trx_dc_free_3h as \"mintrxdcfree3h\", \n" +
                "tp.min_trx_rvsl_3h as \"mintrxrvsl3h\", \n" +
                "tp.min_trx_app_6h as \"mintrxapp6h\", \n" +
                "tp.min_trx_dc_6h as \"mintrxdc6h\", \n" +
                "tp.min_trx_dc_free_6h as \"mintrxdcfree6h\", \n" +
                "tp.min_trx_rvsl_6h as \"mintrxrvsl6h\", \n" +
                "tp.min_trx_app_1d as \"mintrxapp1d\", \n" +
                "tp.min_trx_dc_1d as \"mintrxdc1d\", \n" +
                "tp.min_trx_dc_free_1d as \"mintrxdcfree1d\", \n" +
                "tp.min_trx_rvsl_1d as \"mintrxrvsl1d\", \n" +
                "tp.min_trx_app_3d as \"mintrxapp3d\", \n" +
                "tp.min_trx_dc_3d as \"mintrxdc3d\", \n" +
                "tp.min_trx_dc_free_3d as \"mintrxdcfree3d\", \n" +
                "tp.min_trx_rvsl_3d as \"mintrxrvsl3d\", \n" +
                "tp.min_amt_app_1h as \"minamtapp1h\", \n" +
                "tp.min_amt_dc_1h as \"minamtdc1h\", \n" +
                "tp.min_amt_dc_free_1h as \"minamtdcfree1h\", \n" +
                "tp.min_amt_rvsl_1h as \"minamtrvsl1h\", \n" +
                "tp.min_amt_app_3h as \"minamtapp3h\", \n" +
                "tp.min_amt_dc_3h as \"minamtdc3h\", \n" +
                "tp.min_amt_dc_free_3h as \"minamtdcfree3h\", \n" +
                "tp.min_amt_rvsl_3h as \"minamtrvsl3h\", \n" +
                "tp.min_amt_app_6h as \"minamtapp6h\", \n" +
                "tp.min_amt_dc_6h as \"minamtdc6h\", \n" +
                "tp.min_amt_dc_free_6h as \"minamtdcfree6h\", \n" +
                "tp.min_amt_rvsl_6h as \"minamtrvsl6h\", \n" +
                "tp.min_amt_app_1d as \"minamtapp1d\", \n" +
                "tp.min_amt_dc_1d as \"minamtdc1d\", \n" +
                "tp.min_amt_dc_free_1d as \"minamtdcfree1d\", \n" +
                "tp.min_amt_rvsl_1d as \"minamtrvsl1d\", \n" +
                "tp.min_amt_app_3d as \"minamtapp3d\", \n" +
                "tp.min_amt_dc_3d as \"minamtdc3d\", \n" +
                "tp.min_amt_dc_free_3d as \"minamtdcfree3d\", \n" +
                "tp.min_amt_rvsl_3d as \"minamtrvsl3d\" \n" +
                "from tms_param_anmly tp join tms_trxid ti on ti.trx_id = tp.trx_id order by tp.trx_id";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyParamBean.class));
        return query.list();
    }
    //end modify by erichie

    //start add (ERE 200313)
    @Transactional(readOnly=true)
    public List listParamBankBene(){
        String sql = "select \n" +
                "ti.trx_name as \"trx\", \n" +
                "tp.min_trx_app_1h as \"mintrxapp1h\", \n" +
                "tp.min_trx_dc_1h as \"mintrxdc1h\", \n" +
                "tp.min_trx_dc_free_1h as \"mintrxdcfree1h\", \n" +
                "tp.min_trx_rvsl_1h as \"mintrxrvsl1h\", \n" +
                "tp.min_trx_app_3h as \"mintrxapp3h\", \n" +
                "tp.min_trx_dc_3h as \"mintrxdc3h\", \n" +
                "tp.min_trx_dc_free_3h as \"mintrxdcfree3h\", \n" +
                "tp.min_trx_rvsl_3h as \"mintrxrvsl3h\", \n" +
                "tp.min_trx_app_6h as \"mintrxapp6h\", \n" +
                "tp.min_trx_dc_6h as \"mintrxdc6h\", \n" +
                "tp.min_trx_dc_free_6h as \"mintrxdcfree6h\", \n" +
                "tp.min_trx_rvsl_6h as \"mintrxrvsl6h\", \n" +
                "tp.min_trx_app_1d as \"mintrxapp1d\", \n" +
                "tp.min_trx_dc_1d as \"mintrxdc1d\", \n" +
                "tp.min_trx_dc_free_1d as \"mintrxdcfree1d\", \n" +
                "tp.min_trx_rvsl_1d as \"mintrxrvsl1d\", \n" +
                "tp.min_trx_app_3d as \"mintrxapp3d\", \n" +
                "tp.min_trx_dc_3d as \"mintrxdc3d\", \n" +
                "tp.min_trx_dc_free_3d as \"mintrxdcfree3d\", \n" +
                "tp.min_trx_rvsl_3d as \"mintrxrvsl3d\", \n" +
                "tp.min_amt_app_1h as \"minamtapp1h\", \n" +
                "tp.min_amt_dc_1h as \"minamtdc1h\", \n" +
                "tp.min_amt_dc_free_1h as \"minamtdcfree1h\", \n" +
                "tp.min_amt_rvsl_1h as \"minamtrvsl1h\", \n" +
                "tp.min_amt_app_3h as \"minamtapp3h\", \n" +
                "tp.min_amt_dc_3h as \"minamtdc3h\", \n" +
                "tp.min_amt_dc_free_3h as \"minamtdcfree3h\", \n" +
                "tp.min_amt_rvsl_3h as \"minamtrvsl3h\", \n" +
                "tp.min_amt_app_6h as \"minamtapp6h\", \n" +
                "tp.min_amt_dc_6h as \"minamtdc6h\", \n" +
                "tp.min_amt_dc_free_6h as \"minamtdcfree6h\", \n" +
                "tp.min_amt_rvsl_6h as \"minamtrvsl6h\", \n" +
                "tp.min_amt_app_1d as \"minamtapp1d\", \n" +
                "tp.min_amt_dc_1d as \"minamtdc1d\", \n" +
                "tp.min_amt_dc_free_1d as \"minamtdcfree1d\", \n" +
                "tp.min_amt_rvsl_1d as \"minamtrvsl1d\", \n" +
                "tp.min_amt_app_3d as \"minamtapp3d\", \n" +
                "tp.min_amt_dc_3d as \"minamtdc3d\", \n" +
                "tp.min_amt_dc_free_3d as \"minamtdcfree3d\", \n" +
                "tp.min_amt_rvsl_3d as \"minamtrvsl3d\" \n" +
                "from tms_param_anmly_bene tp join tms_trxid ti on ti.trx_id = tp.trx_id order by tp.trx_id";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyParamBean.class));
        return query.list();
    }
    //stop add (ERE 200313)

    @Transactional(readOnly=true)
    public List listParamBank(){
        String sql = "select \n" +
                "ti.trx_name as \"trx\", \n" +
                "'' as \"mintrxapp1h\", \n" +
                "'' as \"mintrxdc1h\", \n" +
                "'' as \"mintrxdcfree1h\", \n" +
                "'' as \"mintrxrvsl1h\", \n" +
                "'' as \"mintrxapp3h\", \n" +
                "'' as \"mintrxdc3h\", \n" +
                "'' as \"mintrxdcfree3h\", \n" +
                "'' as \"mintrxrvsl3h\", \n" +
                "'' as \"mintrxapp6h\", \n" +
                "'' as \"mintrxdc6h\", \n" +
                "'' as \"mintrxdcfree6h\", \n" +
                "'' as \"mintrxrvsl6h\", \n" +
                "'' as \"mintrxapp1d\", \n" +
                "'' as \"mintrxdc1d\", \n" +
                "'' as \"mintrxdcfree1d\", \n" +
                "'' as \"mintrxrvsl1d\", \n" +
                "'' as \"mintrxapp3d\", \n" +
                "'' as \"mintrxdc3d\", \n" +
                "'' as \"mintrxdcfree3d\", \n" +
                "'' as \"mintrxrvsl3d\", \n" +
                "'' as \"minamtapp1h\", \n" +
                "'' as \"minamtdc1h\", \n" +
                "'' as \"minamtdcfree1h\", \n" +
                "'' as \"minamtrvsl1h\", \n" +
                "'' as \"minamtapp3h\", \n" +
                "'' as \"minamtdc3h\", \n" +
                "'' as \"minamtdcfree3h\", \n" +
                "'' as \"minamtrvsl3h\", \n" +
                "'' as \"minamtapp6h\", \n" +
                "'' as \"minamtdc6h\", \n" +
                "'' as \"minamtdcfree6h\", \n" +
                "'' as \"minamtrvsl6h\", \n" +
                "'' as \"minamtapp1d\", \n" +
                "'' as \"minamtdc1d\", \n" +
                "'' as \"minamtdcfree1d\", \n" +
                "'' as \"minamtrvsl1d\", \n" +
                "'' as \"minamtapp3d\", \n" +
                "'' as \"minamtdc3d\", \n" +
                "'' as \"minamtdcfree3d\", \n" +
                "'' as \"minamtrvsl3d\" \n" +
                "from tms_param_anmly tp join tms_trxid ti on ti.trx_id = tp.trx_id order by tp.trx_id";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyParamBean.class));
        return query.list();
    }

    @Transactional
    public void deletePan(String pan, String pengirim) {
        String sql = "delete from tms_pandummy where pan = :pan and pengirim = :pengirim";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan);
        query.setString("pengirim", pengirim);
        query.executeUpdate();
    }

    @Transactional
    public void deleteEmail(String useremail) {
        String sql = "delete from tms_anmly_email where user_email = :useremail";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("useremail", useremail);
        query.executeUpdate();
    }

    @Transactional
    public void deleteParamBank(String bank_id) {
        String sql = "delete from tms_param_anmly_per_bank where bank_id = :bank_id";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("bank_id", bank_id);
        query.executeUpdate();
    }

    @Transactional
    public void savePan(String pan, String pengirim, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree, BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) {
        String sql = "insert into tms_pandummy values (:pan, :pengirim, :freqApp, :freqDc, :freqDcFree, :freqRvsl, :totalAmtApp, :totalAmtDc, :totalAmtDcFree, :totalAmtRvsl, :totalAmt, :active)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan);
        query.setString("pengirim", pengirim);
        query.setBigDecimal("freqApp", freqApp);
        query.setBigDecimal("freqDc", freqDc);
        query.setBigDecimal("freqDcFree", freqDcFree);
        query.setBigDecimal("freqRvsl", freqRvsl);
        query.setBigDecimal("totalAmtApp", totalAmtApp);
        query.setBigDecimal("totalAmtDc", totalAmtDc);
        query.setBigDecimal("totalAmtDcFree", totalAmtDcFree);
        query.setBigDecimal("totalAmtRvsl", totalAmtRvsl);
        query.setBigDecimal("totalAmt", totalAmt);
        query.setString("active", active);
        query.executeUpdate();
    }

    @Transactional
    public void saveParamBank(List<PanAnomalyParamBean> list, String bank_id) {

        String trx_id = "";
        String sql = "insert into tms_param_anmly_per_bank values(:bank_id, :trx_id, :mintrxapp1h, :minamtapp1h, :mintrxdc1h, :minamtdc1h, :mintrxdcfree1h, :minamtdcfree1h, :mintrxrvsl1h, :minamtrvsl1h, :mintrxapp3h, :minamtapp3h, :mintrxdc3h, :minamtdc3h, :mintrxdcfree3h, :minamtdcfree3h, :mintrxrvsl3h, :minamtrvsl3h, :mintrxapp6h, :minamtapp6h, :mintrxdc6h, :minamtdc6h, :mintrxdcfree6h, :minamtdcfree6h, :mintrxrvsl6h, :minamtrvsl6h, :mintrxapp1d, :minamtapp1d, :mintrxdc1d, :minamtdc1d, :mintrxdcfree1d, :minamtdcfree1d, :mintrxrvsl1d, :minamtrvsl1d, :mintrxapp3d, :minamtapp3d, :mintrxdc3d, :minamtdc3d, :mintrxdcfree3d, :minamtdcfree3d, :mintrxrvsl3d, :minamtrvsl3d)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        /*Modify by aaw 13/09/2021*/
        for(int i=0;i</*6*/list.size();i++){
        /*End modify*/
            if(list.get(i).getTrx().equals("POS/Debit")){
                trx_id = "01";
            }else if(list.get(i).getTrx().equals("Cash Withdrawal")){
                trx_id = "10";
            }else if(list.get(i).getTrx().equals("Payment")){
                trx_id = "17";
            }else if(list.get(i).getTrx().equals("Transfer Debet/Kredit")){
                trx_id = "40";
            }else if(list.get(i).getTrx().equals("Top-up E-Money")){
                trx_id = "85";
            }else if(list.get(i).getTrx().equals("QR")){
                trx_id = "29";
            }/*Add by aaw 13/09/2021*/ else if ("Balance Inquiry".equalsIgnoreCase(list.get(i).getTrx())) {
                trx_id = "30";
            } else if ("Transfer Inquiry".equalsIgnoreCase(list.get(i).getTrx())) {
                trx_id = "32";
            } else if ("Payment Inquiry".equalsIgnoreCase(list.get(i).getTrx())) {
                trx_id = "37";
            }
            /*End add*/

            query.setString("bank_id", bank_id);
            query.setString("trx_id", trx_id);
            query.setBigDecimal("mintrxapp1h", list.get(i).getTrxApp1h());
            query.setBigDecimal("minamtapp1h", list.get(i).getAmtApp1h());
            query.setBigDecimal("mintrxdc1h", list.get(i).getTrxDc1h());
            query.setBigDecimal("minamtdc1h", list.get(i).getAmtDc1h());
            query.setBigDecimal("mintrxdcfree1h", list.get(i).getTrxDcFree1h());
            query.setBigDecimal("minamtdcfree1h", list.get(i).getAmtDcFree1h());
            query.setBigDecimal("mintrxrvsl1h", list.get(i).getTrxRvsl1h());
            query.setBigDecimal("minamtrvsl1h", list.get(i).getAmtRvsl1h());

            query.setBigDecimal("mintrxapp3h", list.get(i).getTrxApp3h());
            query.setBigDecimal("minamtapp3h", list.get(i).getAmtApp3h());
            query.setBigDecimal("mintrxdc3h", list.get(i).getTrxDc3h());
            query.setBigDecimal("minamtdc3h", list.get(i).getAmtDc3h());
            query.setBigDecimal("mintrxdcfree3h", list.get(i).getTrxDcFree3h());
            query.setBigDecimal("minamtdcfree3h", list.get(i).getAmtDcFree3h());
            query.setBigDecimal("mintrxrvsl3h", list.get(i).getTrxRvsl3h());
            query.setBigDecimal("minamtrvsl3h", list.get(i).getAmtRvsl3h());

            query.setBigDecimal("mintrxapp6h", list.get(i).getTrxApp6h());
            query.setBigDecimal("minamtapp6h", list.get(i).getAmtApp6h());
            query.setBigDecimal("mintrxdc6h", list.get(i).getTrxDc6h());
            query.setBigDecimal("minamtdc6h", list.get(i).getAmtDc6h());
            query.setBigDecimal("mintrxdcfree6h", list.get(i).getTrxDcFree6h());
            query.setBigDecimal("minamtdcfree6h", list.get(i).getAmtDcFree6h());
            query.setBigDecimal("mintrxrvsl6h", list.get(i).getTrxRvsl6h());
            query.setBigDecimal("minamtrvsl6h", list.get(i).getAmtRvsl6h());

            query.setBigDecimal("mintrxapp1d", list.get(i).getTrxApp1d());
            query.setBigDecimal("minamtapp1d", list.get(i).getAmtApp1d());
            query.setBigDecimal("mintrxdc1d", list.get(i).getTrxDc1d());
            query.setBigDecimal("minamtdc1d", list.get(i).getAmtDc1d());
            query.setBigDecimal("mintrxdcfree1d", list.get(i).getTrxDcFree1d());
            query.setBigDecimal("minamtdcfree1d", list.get(i).getAmtDcFree1d());
            query.setBigDecimal("mintrxrvsl1d", list.get(i).getTrxRvsl1d());
            query.setBigDecimal("minamtrvsl1d", list.get(i).getAmtRvsl1d());

            query.setBigDecimal("mintrxapp3d", list.get(i).getTrxApp3d());
            query.setBigDecimal("minamtapp3d", list.get(i).getAmtApp3d());
            query.setBigDecimal("mintrxdc3d", list.get(i).getTrxDc3d());
            query.setBigDecimal("minamtdc3d", list.get(i).getAmtDc3d());
            query.setBigDecimal("mintrxdcfree3d", list.get(i).getTrxDcFree3d());
            query.setBigDecimal("minamtdcfree3d", list.get(i).getAmtDcFree3d());
            query.setBigDecimal("mintrxrvsl3d", list.get(i).getTrxRvsl3d());
            query.setBigDecimal("minamtrvsl3d", list.get(i).getAmtRvsl3d());
            query.executeUpdate();
        }

    }

    @Transactional
    public void updateParamBank(List<PanAnomalyParamBean> list, String bank_id) {

        String trx_id = "";
        String sql = "update tms_param_anmly_per_bank set bank_id = :bank_id, trx_id = :trx_id, min_trx_app_1h = :mintrxapp1h, min_amt_app_1h = :minamtapp1h, min_trx_dc_1h = :mintrxdc1h, min_amt_dc_1h = :minamtdc1h, min_trx_dc_free_1h = :mintrxdcfree1h, min_amt_dc_free_1h = :minamtdcfree1h, min_trx_rvsl_1h = :mintrxrvsl1h, min_amt_rvsl_1h = :minamtrvsl1h, min_trx_app_3h = :mintrxapp3h, min_amt_app_3h = :minamtapp3h, min_trx_dc_3h = :mintrxdc3h, min_amt_dc_3h = :minamtdc3h, min_trx_dc_free_3h = :mintrxdcfree3h, min_amt_dc_free_3h = :minamtdcfree3h, min_trx_rvsl_3h = :mintrxrvsl3h, min_amt_rvsl_3h = :minamtrvsl3h, min_trx_app_6h = :mintrxapp6h, min_amt_app_6h = :minamtapp6h, min_trx_dc_6h = :mintrxdc6h, min_amt_dc_6h = :minamtdc6h, min_trx_dc_free_6h = :mintrxdcfree6h, min_amt_dc_free_6h = :minamtdcfree6h, min_trx_rvsl_6h = :mintrxrvsl6h, min_amt_rvsl_6h = :minamtrvsl6h, min_trx_app_1d = :mintrxapp1d, min_amt_app_1d = :minamtapp1d, min_trx_dc_1d = :mintrxdc1d, min_amt_dc_1d = :minamtdc1d, min_trx_dc_free_1d = :mintrxdcfree1d, min_amt_dc_free_1d = :minamtdcfree1d, min_trx_rvsl_1d = :mintrxrvsl1d, min_amt_rvsl_1d = :minamtrvsl1d, min_trx_app_3d = :mintrxapp3d, min_amt_app_3d = :minamtapp3d, min_trx_dc_3d = :mintrxdc3d, min_amt_dc_3d = :minamtdc3d, min_trx_dc_free_3d = :mintrxdcfree3d, min_amt_dc_free_3d = :minamtdcfree3d, min_trx_rvsl_3d = :mintrxrvsl3d, min_amt_rvsl_3d = :minamtrvsl3d where bank_id = :bank_id and trx_id = :trx_id";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        /*Modify by aaw 13/09/021*/
        for(int i=0;i</*6*/list.size();i++){
        /*End modify*/
            if(list.get(i).getTrx().equals("POS/Debit")){
                trx_id = "01";
            }else if(list.get(i).getTrx().equals("Cash Withdrawal")){
                trx_id = "10";
            }else if(list.get(i).getTrx().equals("Payment")){
                trx_id = "17";
            }else if(list.get(i).getTrx().equals("Transfer Debet/Kredit")){
                trx_id = "40";
            }else if(list.get(i).getTrx().equals("Top-up E-Money")){
                trx_id = "85";
            }else if(list.get(i).getTrx().equals("QR")){
                trx_id = "29";
            }/*Add by aaw 13/09/2021*/ else if ("Balance Inquiry".equalsIgnoreCase(list.get(i).getTrx())) {
                trx_id = "30";
            } else if ("Transfer Inquiry".equalsIgnoreCase(list.get(i).getTrx())) {
                trx_id = "32";
            } else if ("Payment Inquiry".equalsIgnoreCase(list.get(i).getTrx())) {
                trx_id = "37";
            }
            /*End add*/

            query.setString("bank_id", bank_id);
            query.setString("trx_id", trx_id);
            query.setBigDecimal("mintrxapp1h", list.get(i).getTrxApp1h());
            query.setBigDecimal("minamtapp1h", list.get(i).getAmtApp1h());
            query.setBigDecimal("mintrxdc1h", list.get(i).getTrxDc1h());
            query.setBigDecimal("minamtdc1h", list.get(i).getAmtDc1h());
            query.setBigDecimal("mintrxdcfree1h", list.get(i).getTrxDcFree1h());
            query.setBigDecimal("minamtdcfree1h", list.get(i).getAmtDcFree1h());
            query.setBigDecimal("mintrxrvsl1h", list.get(i).getTrxRvsl1h());
            query.setBigDecimal("minamtrvsl1h", list.get(i).getAmtRvsl1h());

            query.setBigDecimal("mintrxapp3h", list.get(i).getTrxApp3h());
            query.setBigDecimal("minamtapp3h", list.get(i).getAmtApp3h());
            query.setBigDecimal("mintrxdc3h", list.get(i).getTrxDc3h());
            query.setBigDecimal("minamtdc3h", list.get(i).getAmtDc3h());
            query.setBigDecimal("mintrxdcfree3h", list.get(i).getTrxDcFree3h());
            query.setBigDecimal("minamtdcfree3h", list.get(i).getAmtDcFree3h());
            query.setBigDecimal("mintrxrvsl3h", list.get(i).getTrxRvsl3h());
            query.setBigDecimal("minamtrvsl3h", list.get(i).getAmtRvsl3h());

            query.setBigDecimal("mintrxapp6h", list.get(i).getTrxApp6h());
            query.setBigDecimal("minamtapp6h", list.get(i).getAmtApp6h());
            query.setBigDecimal("mintrxdc6h", list.get(i).getTrxDc6h());
            query.setBigDecimal("minamtdc6h", list.get(i).getAmtDc6h());
            query.setBigDecimal("mintrxdcfree6h", list.get(i).getTrxDcFree6h());
            query.setBigDecimal("minamtdcfree6h", list.get(i).getAmtDcFree6h());
            query.setBigDecimal("mintrxrvsl6h", list.get(i).getTrxRvsl6h());
            query.setBigDecimal("minamtrvsl6h", list.get(i).getAmtRvsl6h());

            query.setBigDecimal("mintrxapp1d", list.get(i).getTrxApp1d());
            query.setBigDecimal("minamtapp1d", list.get(i).getAmtApp1d());
            query.setBigDecimal("mintrxdc1d", list.get(i).getTrxDc1d());
            query.setBigDecimal("minamtdc1d", list.get(i).getAmtDc1d());
            query.setBigDecimal("mintrxdcfree1d", list.get(i).getTrxDcFree1d());
            query.setBigDecimal("minamtdcfree1d", list.get(i).getAmtDcFree1d());
            query.setBigDecimal("mintrxrvsl1d", list.get(i).getTrxRvsl1d());
            query.setBigDecimal("minamtrvsl1d", list.get(i).getAmtRvsl1d());

            query.setBigDecimal("mintrxapp3d", list.get(i).getTrxApp3d());
            query.setBigDecimal("minamtapp3d", list.get(i).getAmtApp3d());
            query.setBigDecimal("mintrxdc3d", list.get(i).getTrxDc3d());
            query.setBigDecimal("minamtdc3d", list.get(i).getAmtDc3d());
            query.setBigDecimal("mintrxdcfree3d", list.get(i).getTrxDcFree3d());
            query.setBigDecimal("minamtdcfree3d", list.get(i).getAmtDcFree3d());
            query.setBigDecimal("mintrxrvsl3d", list.get(i).getTrxRvsl3d());
            query.setBigDecimal("minamtrvsl3d", list.get(i).getAmtRvsl3d());
            query.executeUpdate();
        }

    }

    //start modify by erichie
    @Transactional
    public void saveEmail(String useremail, String gender, String username, String alert1h, String alert3h, String alert6h, String alert1d, String alert3d, String alertMaxAmt, String alertMccVelocity, String alertSeqVelocity, String alertMpanAcq) {
        /*Modify by aaw 02/07/2021*/
        String sql = "insert into tms_anmly_email values (:useremail, :gender, :username, :alert1h, :alert3h, :alert6h, :alert1d, :alert3d, :alertMaxAmt, :alertMccVelocity, :alertSeqVelocity, :alertMpanAcq)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("useremail", useremail);
        query.setString("gender", gender);
        query.setString("username", username);
        query.setString("alert1h", alert1h.replace("true", "YES").replace("false", "NO"));
        query.setString("alert3h", alert3h.replace("true", "YES").replace("false", "NO"));
        query.setString("alert6h", alert6h.replace("true", "YES").replace("false", "NO"));
        query.setString("alert1d", alert1d.replace("true", "YES").replace("false", "NO"));
        query.setString("alert3d", alert3d.replace("true", "YES").replace("false", "NO"));
        query.setString("alertMaxAmt", alertMaxAmt.replace("true", "YES").replace("false", "NO"));
        query.setString("alertMccVelocity", alertMccVelocity.replace("true","YES").replace("false","NO"));
        query.setString("alertSeqVelocity", alertSeqVelocity.replace("true", "YES").replace("false","NO"));
        query.setString("alertMpanAcq", alertMpanAcq.replace("true", "YES").replace("false","NO"));
        query.executeUpdate();
    }
    //end modify by erichie

    /*Add by aaw 16/09/2021*/
    @Transactional
    public void saveEmailInstitusi(String useremail, String gender, String username, String alert1h, String alert3h, String alert6h, String alert1d, String alert3d, String alertMaxAmt, String alertMccVelocity, String alertSeqVelocity, String alertMpanAcq){
        PanAnomalyEmail p = new PanAnomalyEmail();
        p.setUserEmail(useremail);
        p.setGender(gender);
        p.setUserName(username);
        p.setAlert1H(alert1h.replace("true", "YES").replace("false", "NO"));
        p.setAlert3H(alert3h.replace("true", "YES").replace("false", "NO"));
        p.setAlert6H(alert6h.replace("true", "YES").replace("false", "NO"));
        p.setAlert1D( alert1d.replace("true", "YES").replace("false", "NO"));
        p.setAlert3D(alert3d.replace("true", "YES").replace("false", "NO"));
        p.setAlertMaxAmt(alertMaxAmt.replace("true", "YES").replace("false", "NO"));
        p.setAlertMccVelocity(alertMccVelocity.replace("true","YES").replace("false","NO"));
        p.setAlertSeqVelocity(alertSeqVelocity.replace("true", "YES").replace("false","NO"));
        p.setAlertMpanAcq(alertMpanAcq.replace("true", "YES").replace("false","NO"));

        save(p);
    }
    /*End add*/

    @Transactional
    public void updatePan(String pan, String pengirim, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree, BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) {
        String sql = "update tms_pandummy set freq_app = :freqApp, freq_dc = :freqDc, freq_dc_free = :freqDcFree, freq_rvsl = :freqRvsl, total_amt_app = :totalAmtApp, total_amt_dc = :totalAmtDc, total_amt_dc_free = :totalAmtDcFree, total_amt_rvsl = :totalAmtRvsl, total_amt = :totalAmt, active = :active where pan = :pan and pengirim = :pengirim";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan);
        query.setString("pengirim", pengirim);
        query.setBigDecimal("freqApp", freqApp);
        query.setBigDecimal("freqDc", freqDc);
        query.setBigDecimal("freqDcFree", freqDcFree);
        query.setBigDecimal("freqRvsl", freqRvsl);
        query.setBigDecimal("totalAmtApp", totalAmtApp);
        query.setBigDecimal("totalAmtDc", totalAmtDc);
        query.setBigDecimal("totalAmtDcFree", totalAmtDcFree);
        query.setBigDecimal("totalAmtRvsl", totalAmtRvsl);
        query.setBigDecimal("totalAmt", totalAmt);
        query.setString("active", active);
        query.executeUpdate();
    }

    //start modify by erichie
    @Transactional
    public void updateEmail(String useremail, String gender, String username, String alert1h, String alert3h, String alert6h, String alert1d, String alert3d, String alertMaxAmt, String alertMccVelocity, String alertSeqVelocity, String alertMpanAcq) {
        /*Modify by aaw 02/07/2021*/
        String sql = "update tms_anmly_email set user_email = :useremail, gender = :gender, user_name = :username, alert_1h_status = :alert1h, alert_3h_status = :alert3h, alert_6h_status = :alert6h, alert_1d_status = :alert1d, alert_3d_status = :alert3d, alert_maxamt_status = :alertMaxAmt, alert_mcc_status = :alertMccVelocity, alert_seq_status = :alertSeqVelocity, alert_mpan_acq = :alertMpanAcq where user_email = :useremail";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("useremail", useremail);
        query.setString("gender", gender);
        query.setString("username", username);
        query.setString("alert1h", alert1h.replace("true", "YES").replace("false", "NO"));
        query.setString("alert3h", alert3h.replace("true", "YES").replace("false", "NO"));
        query.setString("alert6h", alert6h.replace("true","YES").replace("false","NO"));
        query.setString("alert1d", alert1d.replace("true","YES").replace("false","NO"));
        query.setString("alert3d", alert3d.replace("true","YES").replace("false","NO"));
        query.setString("alertMaxAmt", alertMaxAmt.replace("true","YES").replace("false","NO"));
        query.setString("alertMccVelocity", alertMccVelocity.replace("true","YES").replace("false","NO"));
        query.setString("alertSeqVelocity", alertSeqVelocity.replace("true", "YES").replace("false","NO"));
        query.setString("alertMpanAcq", alertMpanAcq.replace("true", "YES").replace("false","NO"));
        query.executeUpdate();
    }

    @Transactional
    public void updateParam(String trx, BigDecimal mintrxapp1h, BigDecimal minamtapp1h, BigDecimal mintrxdc1h, BigDecimal minamtdc1h, BigDecimal mintrxdcfree1h, BigDecimal minamtdcfree1h, BigDecimal mintrxrvsl1h, BigDecimal minamtrvsl1h, BigDecimal minamttot1h, BigDecimal mintrxapp3h, BigDecimal minamtapp3h, BigDecimal mintrxdc3h, BigDecimal minamtdc3h, BigDecimal mintrxdcfree3h, BigDecimal minamtdcfree3h, BigDecimal mintrxrvsl3h, BigDecimal minamtrvsl3h, BigDecimal minamttot3h, BigDecimal mintrxapp6h, BigDecimal minamtapp6h, BigDecimal mintrxdc6h, BigDecimal minamtdc6h, BigDecimal mintrxdcfree6h, BigDecimal minamtdcfree6h, BigDecimal mintrxrvsl6h, BigDecimal minamtrvsl6h, BigDecimal minamttot6h, BigDecimal mintrxapp1d, BigDecimal minamtapp1d, BigDecimal mintrxdc1d, BigDecimal minamtdc1d, BigDecimal mintrxdcfree1d, BigDecimal minamtdcfree1d, BigDecimal mintrxrvsl1d, BigDecimal minamtrvsl1d, BigDecimal minamttot1d, BigDecimal mintrxapp3d, BigDecimal minamtapp3d, BigDecimal mintrxdc3d, BigDecimal minamtdc3d, BigDecimal mintrxdcfree3d, BigDecimal minamtdcfree3d, BigDecimal mintrxrvsl3d, BigDecimal minamtrvsl3d, BigDecimal minamttot3d) {
        //String sql = "update tms_param_anmly set min_trx_app_1h = :mintrxapp1h, min_amt_app_1h = :minamtapp1h, min_trx_dc_1h = :mintrxdc1h, min_amt_dc_1h = :minamtdc1h, min_trx_dc_free_1h = :mintrxdcfree1h, min_amt_dc_free_1h = :minamtdcfree1h, min_trx_rvsl_1h = :mintrxrvsl1h, min_amt_rvsl_1h = :minamtrvsl1h, min_amt_tot_1h = :minamttot1h, min_trx_app_3h = :mintrxapp3h, min_amt_app_3h = :minamtapp3h, min_trx_dc_3h = :mintrxdc3h, min_amt_dc_3h = :minamtdc3h, min_trx_dc_free_3h = :mintrxdcfree3h, min_amt_dc_free_3h = :minamtdcfree3h, min_trx_rvsl_3h = :mintrxrvsl3h, min_amt_rvsl_3h = :minamtrvsl3h, min_amt_tot_3h = :minamttot3h, min_trx_app_6h = :mintrxapp6h, min_amt_app_6h = :minamtapp6h, min_trx_dc_6h = :mintrxdc6h, min_amt_dc_6h = :minamtdc6h, min_trx_dc_free_6h = :mintrxdcfree6h, min_amt_dc_free_6h = :minamtdcfree6h, min_trx_rvsl_6h = :mintrxrvsl6h, min_amt_rvsl_6h = :minamtrvsl6h, min_amt_tot_6h = :minamttot6h, min_trx_app_1d = :mintrxapp1d, min_amt_app_1d = :minamtapp1d, min_trx_dc_1d = :mintrxdc1d, min_amt_dc_1d = :minamtdc1d, min_trx_dc_free_1d = :mintrxdcfree1d, min_amt_dc_free_1d = :minamtdcfree1d, min_trx_rvsl_1d = :mintrxrvsl1d, min_amt_rvsl_1d = :minamtrvsl1d, min_amt_tot_1d = :minamttot1d, min_trx_app_3d = :mintrxapp3d, min_amt_app_3d = :minamtapp3d, min_trx_dc_3d = :mintrxdc3d, min_amt_dc_3d = :minamtdc3d, min_trx_dc_free_3d = :mintrxdcfree3d, min_amt_dc_free_3d = :minamtdcfree3d, min_trx_rvsl_3d = :mintrxrvsl3d, min_amt_rvsl_3d = :minamtrvsl3d, min_amt_tot_3d = :minamttot3d where trx_id = :trx_id";
        String sql = "update tms_param_anmly set min_trx_app_1h = :mintrxapp1h, min_amt_app_1h = :minamtapp1h, min_trx_dc_1h = :mintrxdc1h, min_amt_dc_1h = :minamtdc1h, min_trx_dc_free_1h = :mintrxdcfree1h, min_amt_dc_free_1h = :minamtdcfree1h, min_trx_rvsl_1h = :mintrxrvsl1h, min_amt_rvsl_1h = :minamtrvsl1h, min_trx_app_3h = :mintrxapp3h, min_amt_app_3h = :minamtapp3h, min_trx_dc_3h = :mintrxdc3h, min_amt_dc_3h = :minamtdc3h, min_trx_dc_free_3h = :mintrxdcfree3h, min_amt_dc_free_3h = :minamtdcfree3h, min_trx_rvsl_3h = :mintrxrvsl3h, min_amt_rvsl_3h = :minamtrvsl3h, min_trx_app_6h = :mintrxapp6h, min_amt_app_6h = :minamtapp6h, min_trx_dc_6h = :mintrxdc6h, min_amt_dc_6h = :minamtdc6h, min_trx_dc_free_6h = :mintrxdcfree6h, min_amt_dc_free_6h = :minamtdcfree6h, min_trx_rvsl_6h = :mintrxrvsl6h, min_amt_rvsl_6h = :minamtrvsl6h, min_trx_app_1d = :mintrxapp1d, min_amt_app_1d = :minamtapp1d, min_trx_dc_1d = :mintrxdc1d, min_amt_dc_1d = :minamtdc1d, min_trx_dc_free_1d = :mintrxdcfree1d, min_amt_dc_free_1d = :minamtdcfree1d, min_trx_rvsl_1d = :mintrxrvsl1d, min_amt_rvsl_1d = :minamtrvsl1d, min_trx_app_3d = :mintrxapp3d, min_amt_app_3d = :minamtapp3d, min_trx_dc_3d = :mintrxdc3d, min_amt_dc_3d = :minamtdc3d, min_trx_dc_free_3d = :mintrxdcfree3d, min_amt_dc_free_3d = :minamtdcfree3d, min_trx_rvsl_3d = :mintrxrvsl3d, min_amt_rvsl_3d = :minamtrvsl3d where trx_id = :trx_id";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        String trx_id = "";
        if(trx.equals("POS/Debit")){
            trx_id = "01";
        }else if(trx.equals("Cash Withdrawal")){
            trx_id = "10";
        }else if(trx.equals("Payment")){
            trx_id = "17";
        }else if(trx.equals("Transfer Debet/Kredit")){
            trx_id = "40";
        }else if(trx.equals("Top-up E-Money")){
            trx_id = "85";
        }else if(trx.equals("QR")){
            trx_id = "29";
        }/*Add by aaw 13/09/2021*/else if ("Balance Inquiry".equalsIgnoreCase(trx)) {
            trx_id = "30";
        } else if ("Transfer Inquiry".equalsIgnoreCase(trx)) {
            trx_id = "32";
        } else if("Payment Inquiry".equalsIgnoreCase(trx)) {
            trx_id = "37";
        }
        //query.setString("trx", trx);
        query.setString("trx_id", trx_id);
        query.setBigDecimal("mintrxapp1h", mintrxapp1h);
        query.setBigDecimal("minamtapp1h", minamtapp1h);
        query.setBigDecimal("mintrxdc1h", mintrxdc1h);
        query.setBigDecimal("minamtdc1h", minamtdc1h);
        query.setBigDecimal("mintrxdcfree1h", mintrxdcfree1h);
        query.setBigDecimal("minamtdcfree1h", minamtdcfree1h);
        query.setBigDecimal("mintrxrvsl1h", mintrxrvsl1h);
        query.setBigDecimal("minamtrvsl1h", minamtrvsl1h);
        //query.setBigDecimal("minamttot1h", minamttot1h);

        query.setBigDecimal("mintrxapp3h", mintrxapp3h);
        query.setBigDecimal("minamtapp3h", minamtapp3h);
        query.setBigDecimal("mintrxdc3h", mintrxdc3h);
        query.setBigDecimal("minamtdc3h", minamtdc3h);
        query.setBigDecimal("mintrxdcfree3h", mintrxdcfree3h);
        query.setBigDecimal("minamtdcfree3h", minamtdcfree3h);
        query.setBigDecimal("mintrxrvsl3h", mintrxrvsl3h);
        query.setBigDecimal("minamtrvsl3h", minamtrvsl3h);
        //query.setBigDecimal("minamttot3h", minamttot3h);

        query.setBigDecimal("mintrxapp6h", mintrxapp6h);
        query.setBigDecimal("minamtapp6h", minamtapp6h);
        query.setBigDecimal("mintrxdc6h", mintrxdc6h);
        query.setBigDecimal("minamtdc6h", minamtdc6h);
        query.setBigDecimal("mintrxdcfree6h", mintrxdcfree6h);
        query.setBigDecimal("minamtdcfree6h", minamtdcfree6h);
        query.setBigDecimal("mintrxrvsl6h", mintrxrvsl6h);
        query.setBigDecimal("minamtrvsl6h", minamtrvsl6h);
        //query.setBigDecimal("minamttot6h", minamttot6h);

        query.setBigDecimal("mintrxapp1d", mintrxapp1d);
        query.setBigDecimal("minamtapp1d", minamtapp1d);
        query.setBigDecimal("mintrxdc1d", mintrxdc1d);
        query.setBigDecimal("minamtdc1d", minamtdc1d);
        query.setBigDecimal("mintrxdcfree1d", mintrxdcfree1d);
        query.setBigDecimal("minamtdcfree1d", minamtdcfree1d);
        query.setBigDecimal("mintrxrvsl1d", mintrxrvsl1d);
        query.setBigDecimal("minamtrvsl1d", minamtrvsl1d);
        //query.setBigDecimal("minamttot1d", minamttot1d);

        query.setBigDecimal("mintrxapp3d", mintrxapp3d);
        query.setBigDecimal("minamtapp3d", minamtapp3d);
        query.setBigDecimal("mintrxdc3d", mintrxdc3d);
        query.setBigDecimal("minamtdc3d", minamtdc3d);
        query.setBigDecimal("mintrxdcfree3d", mintrxdcfree3d);
        query.setBigDecimal("minamtdcfree3d", minamtdcfree3d);
        query.setBigDecimal("mintrxrvsl3d", mintrxrvsl3d);
        query.setBigDecimal("minamtrvsl3d", minamtrvsl3d);
        //query.setBigDecimal("minamttot3d", minamttot3d);
        query.executeUpdate();
    }
    //end modify by erichie

    //start add (ERE 200313)
    @Transactional
    public void updateParamBankBene(String trx, BigDecimal mintrxapp1h, BigDecimal minamtapp1h, BigDecimal mintrxdc1h, BigDecimal minamtdc1h, BigDecimal mintrxdcfree1h, BigDecimal minamtdcfree1h, BigDecimal mintrxrvsl1h, BigDecimal minamtrvsl1h, BigDecimal minamttot1h, BigDecimal mintrxapp3h, BigDecimal minamtapp3h, BigDecimal mintrxdc3h, BigDecimal minamtdc3h, BigDecimal mintrxdcfree3h, BigDecimal minamtdcfree3h, BigDecimal mintrxrvsl3h, BigDecimal minamtrvsl3h, BigDecimal minamttot3h, BigDecimal mintrxapp6h, BigDecimal minamtapp6h, BigDecimal mintrxdc6h, BigDecimal minamtdc6h, BigDecimal mintrxdcfree6h, BigDecimal minamtdcfree6h, BigDecimal mintrxrvsl6h, BigDecimal minamtrvsl6h, BigDecimal minamttot6h, BigDecimal mintrxapp1d, BigDecimal minamtapp1d, BigDecimal mintrxdc1d, BigDecimal minamtdc1d, BigDecimal mintrxdcfree1d, BigDecimal minamtdcfree1d, BigDecimal mintrxrvsl1d, BigDecimal minamtrvsl1d, BigDecimal minamttot1d, BigDecimal mintrxapp3d, BigDecimal minamtapp3d, BigDecimal mintrxdc3d, BigDecimal minamtdc3d, BigDecimal mintrxdcfree3d, BigDecimal minamtdcfree3d, BigDecimal mintrxrvsl3d, BigDecimal minamtrvsl3d, BigDecimal minamttot3d) {
        String sql = "update tms_param_anmly_bene set min_trx_app_1h = :mintrxapp1h, min_amt_app_1h = :minamtapp1h, min_trx_dc_1h = :mintrxdc1h, min_amt_dc_1h = :minamtdc1h, min_trx_dc_free_1h = :mintrxdcfree1h, min_amt_dc_free_1h = :minamtdcfree1h, min_trx_rvsl_1h = :mintrxrvsl1h, min_amt_rvsl_1h = :minamtrvsl1h, min_trx_app_3h = :mintrxapp3h, min_amt_app_3h = :minamtapp3h, min_trx_dc_3h = :mintrxdc3h, min_amt_dc_3h = :minamtdc3h, min_trx_dc_free_3h = :mintrxdcfree3h, min_amt_dc_free_3h = :minamtdcfree3h, min_trx_rvsl_3h = :mintrxrvsl3h, min_amt_rvsl_3h = :minamtrvsl3h, min_trx_app_6h = :mintrxapp6h, min_amt_app_6h = :minamtapp6h, min_trx_dc_6h = :mintrxdc6h, min_amt_dc_6h = :minamtdc6h, min_trx_dc_free_6h = :mintrxdcfree6h, min_amt_dc_free_6h = :minamtdcfree6h, min_trx_rvsl_6h = :mintrxrvsl6h, min_amt_rvsl_6h = :minamtrvsl6h, min_trx_app_1d = :mintrxapp1d, min_amt_app_1d = :minamtapp1d, min_trx_dc_1d = :mintrxdc1d, min_amt_dc_1d = :minamtdc1d, min_trx_dc_free_1d = :mintrxdcfree1d, min_amt_dc_free_1d = :minamtdcfree1d, min_trx_rvsl_1d = :mintrxrvsl1d, min_amt_rvsl_1d = :minamtrvsl1d, min_trx_app_3d = :mintrxapp3d, min_amt_app_3d = :minamtapp3d, min_trx_dc_3d = :mintrxdc3d, min_amt_dc_3d = :minamtdc3d, min_trx_dc_free_3d = :mintrxdcfree3d, min_amt_dc_free_3d = :minamtdcfree3d, min_trx_rvsl_3d = :mintrxrvsl3d, min_amt_rvsl_3d = :minamtrvsl3d where trx_id = :trx";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);

        if(trx.equals("Transfer Debet/Kredit")){
            trx = "40";
        }

        query.setString("trx", trx);
        query.setBigDecimal("mintrxapp1h", mintrxapp1h);
        query.setBigDecimal("minamtapp1h", minamtapp1h);
        query.setBigDecimal("mintrxdc1h", mintrxdc1h);
        query.setBigDecimal("minamtdc1h", minamtdc1h);
        query.setBigDecimal("mintrxdcfree1h", mintrxdcfree1h);
        query.setBigDecimal("minamtdcfree1h", minamtdcfree1h);
        query.setBigDecimal("mintrxrvsl1h", mintrxrvsl1h);
        query.setBigDecimal("minamtrvsl1h", minamtrvsl1h);
//        query.setBigDecimal("minamttot1h", minamttot1h);

        query.setBigDecimal("mintrxapp3h", mintrxapp3h);
        query.setBigDecimal("minamtapp3h", minamtapp3h);
        query.setBigDecimal("mintrxdc3h", mintrxdc3h);
        query.setBigDecimal("minamtdc3h", minamtdc3h);
        query.setBigDecimal("mintrxdcfree3h", mintrxdcfree3h);
        query.setBigDecimal("minamtdcfree3h", minamtdcfree3h);
        query.setBigDecimal("mintrxrvsl3h", mintrxrvsl3h);
        query.setBigDecimal("minamtrvsl3h", minamtrvsl3h);
//        query.setBigDecimal("minamttot3h", minamttot3h);

        query.setBigDecimal("mintrxapp6h", mintrxapp6h);
        query.setBigDecimal("minamtapp6h", minamtapp6h);
        query.setBigDecimal("mintrxdc6h", mintrxdc6h);
        query.setBigDecimal("minamtdc6h", minamtdc6h);
        query.setBigDecimal("mintrxdcfree6h", mintrxdcfree6h);
        query.setBigDecimal("minamtdcfree6h", minamtdcfree6h);
        query.setBigDecimal("mintrxrvsl6h", mintrxrvsl6h);
        query.setBigDecimal("minamtrvsl6h", minamtrvsl6h);
//        query.setBigDecimal("minamttot6h", minamttot6h);

        query.setBigDecimal("mintrxapp1d", mintrxapp1d);
        query.setBigDecimal("minamtapp1d", minamtapp1d);
        query.setBigDecimal("mintrxdc1d", mintrxdc1d);
        query.setBigDecimal("minamtdc1d", minamtdc1d);
        query.setBigDecimal("mintrxdcfree1d", mintrxdcfree1d);
        query.setBigDecimal("minamtdcfree1d", minamtdcfree1d);
        query.setBigDecimal("mintrxrvsl1d", mintrxrvsl1d);
        query.setBigDecimal("minamtrvsl1d", minamtrvsl1d);
//        query.setBigDecimal("minamttot1d", minamttot1d);

        query.setBigDecimal("mintrxapp3d", mintrxapp3d);
        query.setBigDecimal("minamtapp3d", minamtapp3d);
        query.setBigDecimal("mintrxdc3d", mintrxdc3d);
        query.setBigDecimal("minamtdc3d", minamtdc3d);
        query.setBigDecimal("mintrxdcfree3d", mintrxdcfree3d);
        query.setBigDecimal("minamtdcfree3d", minamtdcfree3d);
        query.setBigDecimal("mintrxrvsl3d", mintrxrvsl3d);
        query.setBigDecimal("minamtrvsl3d", minamtrvsl3d);
//        query.setBigDecimal("minamttot3d", minamttot3d);
        query.executeUpdate();
    }
    //stop add (ERE 200313)

    //add
    @Transactional
    public void updateMaxAmtParam(String trx, BigDecimal maxamt1h){
        String sql = "update tms_param_anmly set max_amt_tot_1h = :maxamt1h where trx_id = :trx";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);

        query.setString("trx",trx);
        query.setBigDecimal("maxamt1h", maxamt1h);
        query.executeUpdate();
    }

    @Transactional(readOnly=true)
    public List listPanSender(String pan, String pengirim){
        /*Modify query by aaw 2107/2021, untuk beberapa jenis tran code, sender tidak ada atau '' atau null, sehingga saat save untuk sender di set ' ' untuk mengindari error null constraint pada db*/
        //String sql = "select pan as \"pan\", pengirim as \"pengirim\", freq_app as \"freqApp\", freq_dc as \"freqDc\", freq_dc_free as \"freqDcFree\", freq_rvsl as \"freqRvsl\", total_amt_app as \"totalAmtApp\", total_amt_dc as \"totalAmtDc\", total_amt_dc_free as \"totalAmtDcFree\", total_amt_rvsl as \"totalAmtRvsl\", total_amt as \"totalAmt\", active as \"active\" from tms_pandummy where pan = :pan and upper(trim(pengirim)) = :pengirim";
        String sql = "select pan as \"pan\", pengirim as \"pengirim\", freq_app as \"freqApp\", freq_dc as \"freqDc\", freq_dc_free as \"freqDcFree\", freq_rvsl as \"freqRvsl\", total_amt_app as \"totalAmtApp\", total_amt_dc as \"totalAmtDc\", total_amt_dc_free as \"totalAmtDcFree\", total_amt_rvsl as \"totalAmtRvsl\", total_amt as \"totalAmt\", active as \"active\" from tms_pandummy where pan = :pan and upper(pengirim) = :pengirim";
        /*End*/
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan);
        query.setString("pengirim", pengirim);
        return query.list();
    }

    //start modify by erichie
    @Transactional(readOnly=true)
    public List listEmailAll(){
        /*Modify by aaw 02/07/2021*/
        String sql = "select user_email as \"useremail\", gender as \"gender\", user_name as \"username\", replace(replace(alert_1h_status,'YES','true'),'NO','false') as \"alert1h\", replace(replace(alert_3h_status,'YES','true'),'NO','false') as \"alert3h\", replace(replace(alert_6h_status,'YES','true'),'NO','false') as \"alert6h\", replace(replace(alert_1d_status,'YES','true'),'NO','false') as \"alert1d\", replace(replace(alert_3d_status,'YES','true'),'NO','false') as \"alert3d\", replace(replace(alert_maxamt_status,'YES','true'),'NO','false') as \"alertMaxAmt\", replace(replace(alert_mcc_status,'YES','true'),'NO','false') as \"alertMccVelocity\", replace(replace(alert_seq_status,'YES','true'),'NO','false') as \"alertSeqVelocity\", replace(replace(alert_mpan_acq,'YES','true'),'NO','false') as \"alertMpanAcq\" from tms_anmly_email";
        /*End modify*/
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyEmailBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listEmail(String useremail){
        String sql = "select user_email as \"useremail\", gender as \"gender\", user_name as \"username\", replace(replace(alert_1h_status,'YES','true'),'NO','false') as \"alert1h\", replace(replace(alert_3h_status,'YES','true'),'NO','false') as \"alert3h\", replace(replace(alert_6h_status,'YES','true'),'NO','false') as \"alert6h\", replace(replace(alert_1d_status,'YES','true'),'NO','false') as \"alert1d\", replace(replace(alert_3d_status,'YES','true'),'NO','false') as \"alert3d\", replace(replace(alert_maxamt_status,'YES','true'),'NO','false') as \"alertMaxAmt\", replace(replace(alert_mcc_status,'YES','true'),'NO','false') as \"alertMccVelocity\", replace(replace(alert_seq_status,'YES','true'),'NO','false') as \"alertSeqVelocity\" from tms_anmly_email where user_email = :useremail";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("useremail", useremail);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyEmailBean.class));
        return query.list();
    }
    //end modify by erichie

    @Transactional(readOnly=true)
    public List getWhitelistPan(String pan, String pengirim){
        String sql = "select pan as \"pan\", pengirim as \"pengirim\", freq_app as \"freqApp\", freq_dc as \"freqDc\", freq_dc_free as \"freqDcFree\", freq_rvsl as \"freqRvsl\", total_amt_app as \"totalAmtApp\", total_amt_dc as \"totalAmtDc\", total_amt_dc_free as \"totalAmtDcFree\", total_amt_rvsl as \"totalAmtRvsl\", total_amt as \"totalAmt\", active as \"active\" from tms_pandummy where pan = :pan and nvl(trim(upper(pengirim)),' ') = nvl(trim(upper(:pengirim)),' ')";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("pan", pan);
        query.setString("pengirim", pengirim);
        query.setResultTransformer(new AliasToBeanResultTransformer(WhitelistBean.class));
        return query.list();
    }

    //start modify by erichie
    @Transactional(readOnly=true)
    public String getBankName(String bankID){
        //String sql = "select bank_id, bank_name from tms_bank where bank_id = :bankID";
        //SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        //query.setString("bankID", bankID);
        //query.setResultTransformer(new AliasToBeanResultTransformer(BankBean.class));
        //return query.list();


        String sql = "select bank_name from tms_bank where bank_id = :bankID";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("bankID", bankID);
        return (String)query.uniqueResult();
    }

    @Transactional(readOnly=true)
    public List getEmail(String useremail){
        /*Modify by aaw 02/07/2021*/
        String sql = "select user_email as \"useremail\", gender as \"gender\", user_name as \"username\", replace(replace(alert_1h_status,'YES','true'),'NO','false') as \"alert1h\", replace(replace(alert_3h_status,'YES','true'),'NO','false') as \"alert3h\", replace(replace(alert_6h_status,'YES','true'),'NO','false') as \"alert6h\", replace(replace(alert_1d_status,'YES','true'),'NO','false') as \"alert1d\", replace(replace(alert_3d_status,'YES','true'),'NO','false') as \"alert3d\", replace(replace(alert_maxamt_status,'YES','true'),'NO','false') as \"alertMaxAmt\", replace(replace(alert_mcc_status,'YES','true'),'NO','false') as \"alertMccVelocity\", replace(replace(alert_seq_status,'YES','true'),'NO','false') as \"alertSeqVelocity\", replace(replace(alert_mpan_acq,'YES','true'),'NO','false') as \"alertMpanAcq\" from tms_anmly_email where user_email = :useremail";
        /*End modify*/
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("useremail", useremail);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyEmailBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List getParam(String trx){
        String sql = "select \n" +
                "ti.trx_name as \"trx\", \n" +
                "tp.min_trx_app_1h as \"mintrxapp1h\", \n" +
                "tp.min_trx_dc_1h as \"mintrxdc1h\", \n" +
                "tp.min_trx_dc_free_1h as \"mintrxdcfree1h\", \n" +
                "tp.min_trx_rvsl_1h as \"mintrxrvsl1h\", \n" +
                "tp.min_trx_app_3h as \"mintrxapp3h\", \n" +
                "tp.min_trx_dc_3h as \"mintrxdc3h\", \n" +
                "tp.min_trx_dc_free_3h as \"mintrxdcfree3h\", \n" +
                "tp.min_trx_rvsl_3h as \"mintrxrvsl3h\", \n" +
                "tp.min_trx_app_6h as \"mintrxapp6h\", \n" +
                "tp.min_trx_dc_6h as \"mintrxdc6h\", \n" +
                "tp.min_trx_dc_free_6h as \"mintrxdcfree6h\", \n" +
                "tp.min_trx_rvsl_6h as \"mintrxrvsl6h\", \n" +
                "tp.min_trx_app_1d as \"mintrxapp1d\", \n" +
                "tp.min_trx_dc_1d as \"mintrxdc1d\", \n" +
                "tp.min_trx_dc_free_1d as \"mintrxdcfree1d\", \n" +
                "tp.min_trx_app_3d as \"mintrxapp3d\", \n" +
                "tp.min_trx_dc_3d as \"mintrxdc3d\", \n" +
                "tp.min_trx_dc_free_3d as \"mintrxdcfree3d\", \n" +
                "tp.min_trx_rvsl_3d as \"mintrxrvsl3d\", \n" +
                "tp.min_trx_rvsl_1d as \"mintrxrvsl1d\", \n" +
                "tp.min_amt_app_1h as \"minamtapp1h\", \n" +
                "tp.min_amt_dc_1h as \"minamtdc1h\", \n" +
                "tp.min_amt_dc_free_1h as \"minamtdcfree1h\", \n" +
                "tp.min_amt_rvsl_1h as \"minamtrvsl1h\", \n" +
                "tp.min_amt_app_3h as \"minamtapp3h\", \n" +
                "tp.min_amt_dc_3h as \"minamtdc3h\", \n" +
                "tp.min_amt_dc_free_3h as \"minamtdcfree3h\", \n" +
                "tp.min_amt_rvsl_3h as \"minamtrvsl3h\", \n" +
                "tp.min_amt_app_6h as \"minamtapp6h\", \n" +
                "tp.min_amt_dc_6h as \"minamtdc6h\", \n" +
                "tp.min_amt_dc_free_6h as \"minamtdcfree6h\", \n" +
                "tp.min_amt_rvsl_6h as \"minamtrvsl6h\", \n" +
                "tp.min_amt_app_1d as \"minamtapp1d\", \n" +
                "tp.min_amt_dc_1d as \"minamtdc1d\", \n" +
                "tp.min_amt_dc_free_1d as \"minamtdcfree1d\", \n" +
                "tp.min_amt_rvsl_1d as \"minamtrvsl1d\", \n" +
                "tp.min_amt_app_3d as \"minamtapp3d\", \n" +
                "tp.min_amt_dc_3d as \"minamtdc3d\", \n" +
                "tp.min_amt_dc_free_3d as \"minamtdcfree3d\", \n" +
                "tp.min_amt_rvsl_3d as \"minamtrvsl3d\" \n" +
                "from tms_param_anmly tp join tms_trxid ti on ti.trx_id = tp.trx_id where tp.trx_id = :trx";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("trx", trx);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyParamBean.class));
        return query.list();
    }

    //start add (ERE 200318)
    @Transactional(readOnly=true)
    public List getParamBankBene(String trx){
        String sql = "select \n" +
                "ti.trx_name as \"trx\", \n" +
                "tp.min_trx_app_1h as \"mintrxapp1h\", \n" +
                "tp.min_trx_dc_1h as \"mintrxdc1h\", \n" +
                "tp.min_trx_dc_free_1h as \"mintrxdcfree1h\", \n" +
                "tp.min_trx_rvsl_1h as \"mintrxrvsl1h\", \n" +
                "tp.min_trx_app_3h as \"mintrxapp3h\", \n" +
                "tp.min_trx_dc_3h as \"mintrxdc3h\", \n" +
                "tp.min_trx_dc_free_3h as \"mintrxdcfree3h\", \n" +
                "tp.min_trx_rvsl_3h as \"mintrxrvsl3h\", \n" +
                "tp.min_trx_app_6h as \"mintrxapp6h\", \n" +
                "tp.min_trx_dc_6h as \"mintrxdc6h\", \n" +
                "tp.min_trx_dc_free_6h as \"mintrxdcfree6h\", \n" +
                "tp.min_trx_rvsl_6h as \"mintrxrvsl6h\", \n" +
                "tp.min_trx_app_1d as \"mintrxapp1d\", \n" +
                "tp.min_trx_dc_1d as \"mintrxdc1d\", \n" +
                "tp.min_trx_dc_free_1d as \"mintrxdcfree1d\", \n" +
                "tp.min_trx_app_3d as \"mintrxapp3d\", \n" +
                "tp.min_trx_dc_3d as \"mintrxdc3d\", \n" +
                "tp.min_trx_dc_free_3d as \"mintrxdcfree3d\", \n" +
                "tp.min_trx_rvsl_3d as \"mintrxrvsl3d\", \n" +
                "tp.min_trx_rvsl_1d as \"mintrxrvsl1d\", \n" +
                "tp.min_amt_app_1h as \"minamtapp1h\", \n" +
                "tp.min_amt_dc_1h as \"minamtdc1h\", \n" +
                "tp.min_amt_dc_free_1h as \"minamtdcfree1h\", \n" +
                "tp.min_amt_rvsl_1h as \"minamtrvsl1h\", \n" +
                "tp.min_amt_app_3h as \"minamtapp3h\", \n" +
                "tp.min_amt_dc_3h as \"minamtdc3h\", \n" +
                "tp.min_amt_dc_free_3h as \"minamtdcfree3h\", \n" +
                "tp.min_amt_rvsl_3h as \"minamtrvsl3h\", \n" +
                "tp.min_amt_app_6h as \"minamtapp6h\", \n" +
                "tp.min_amt_dc_6h as \"minamtdc6h\", \n" +
                "tp.min_amt_dc_free_6h as \"minamtdcfree6h\", \n" +
                "tp.min_amt_rvsl_6h as \"minamtrvsl6h\", \n" +
                "tp.min_amt_app_1d as \"minamtapp1d\", \n" +
                "tp.min_amt_dc_1d as \"minamtdc1d\", \n" +
                "tp.min_amt_dc_free_1d as \"minamtdcfree1d\", \n" +
                "tp.min_amt_rvsl_1d as \"minamtrvsl1d\", \n" +
                "tp.min_amt_app_3d as \"minamtapp3d\", \n" +
                "tp.min_amt_dc_3d as \"minamtdc3d\", \n" +
                "tp.min_amt_dc_free_3d as \"minamtdcfree3d\", \n" +
                "tp.min_amt_rvsl_3d as \"minamtrvsl3d\" \n" +
                "from tms_param_anmly_bene tp join tms_trxid ti on ti.trx_id = tp.trx_id where tp.trx_id = :trx";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("trx", trx);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyParamBean.class));
        return query.list();
    }
    //stop add (ERE 200318)

    @Transactional(readOnly=true)
    public List getParamBank(String id){
        String sql = "select \n" +
                "ti.trx_name as \"trx\", \n" +
                "tp.trx_id as \"trxid\", \n" +
                "tp.min_trx_app_1h as \"mintrxapp1h\", \n" +
                "tp.min_trx_dc_1h as \"mintrxdc1h\", \n" +
                "tp.min_trx_dc_free_1h as \"mintrxdcfree1h\", \n" +
                "tp.min_trx_rvsl_1h as \"mintrxrvsl1h\", \n" +
                "tp.min_trx_app_3h as \"mintrxapp3h\", \n" +
                "tp.min_trx_dc_3h as \"mintrxdc3h\", \n" +
                "tp.min_trx_dc_free_3h as \"mintrxdcfree3h\", \n" +
                "tp.min_trx_rvsl_3h as \"mintrxrvsl3h\", \n" +
                "tp.min_trx_app_6h as \"mintrxapp6h\", \n" +
                "tp.min_trx_dc_6h as \"mintrxdc6h\", \n" +
                "tp.min_trx_dc_free_6h as \"mintrxdcfree6h\", \n" +
                "tp.min_trx_rvsl_6h as \"mintrxrvsl6h\", \n" +
                "tp.min_trx_app_1d as \"mintrxapp1d\", \n" +
                "tp.min_trx_dc_1d as \"mintrxdc1d\", \n" +
                "tp.min_trx_dc_free_1d as \"mintrxdcfree1d\", \n" +
                "tp.min_trx_app_3d as \"mintrxapp3d\", \n" +
                "tp.min_trx_dc_3d as \"mintrxdc3d\", \n" +
                "tp.min_trx_dc_free_3d as \"mintrxdcfree3d\", \n" +
                "tp.min_trx_rvsl_3d as \"mintrxrvsl3d\", \n" +
                "tp.min_trx_rvsl_1d as \"mintrxrvsl1d\", \n" +
                "tp.min_amt_app_1h as \"minamtapp1h\", \n" +
                "tp.min_amt_dc_1h as \"minamtdc1h\", \n" +
                "tp.min_amt_dc_free_1h as \"minamtdcfree1h\", \n" +
                "tp.min_amt_rvsl_1h as \"minamtrvsl1h\", \n" +
                "tp.min_amt_app_3h as \"minamtapp3h\", \n" +
                "tp.min_amt_dc_3h as \"minamtdc3h\", \n" +
                "tp.min_amt_dc_free_3h as \"minamtdcfree3h\", \n" +
                "tp.min_amt_rvsl_3h as \"minamtrvsl3h\", \n" +
                "tp.min_amt_app_6h as \"minamtapp6h\", \n" +
                "tp.min_amt_dc_6h as \"minamtdc6h\", \n" +
                "tp.min_amt_dc_free_6h as \"minamtdcfree6h\", \n" +
                "tp.min_amt_rvsl_6h as \"minamtrvsl6h\", \n" +
                "tp.min_amt_app_1d as \"minamtapp1d\", \n" +
                "tp.min_amt_dc_1d as \"minamtdc1d\", \n" +
                "tp.min_amt_dc_free_1d as \"minamtdcfree1d\", \n" +
                "tp.min_amt_rvsl_1d as \"minamtrvsl1d\", \n" +
                "tp.min_amt_app_3d as \"minamtapp3d\", \n" +
                "tp.min_amt_dc_3d as \"minamtdc3d\", \n" +
                "tp.min_amt_dc_free_3d as \"minamtdcfree3d\", \n" +
                "tp.min_amt_rvsl_3d as \"minamtrvsl3d\" \n" +
                "from tms_param_anmly_per_bank tp join tms_trxid ti on ti.trx_id = tp.trx_id where tp.bank_id = :id";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("id", id);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyParamBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List getParamBankId(String bank_id){
        String sql = "select \n" +
                "ti.trx_name as \"trx\", \n" +
                "tp.trx_id as \"trxid\", \n" +
                "tp.min_trx_app_1h as \"mintrxapp1h\", \n" +
                "tp.min_trx_dc_1h as \"mintrxdc1h\", \n" +
                "tp.min_trx_dc_free_1h as \"mintrxdcfree1h\", \n" +
                "tp.min_trx_rvsl_1h as \"mintrxrvsl1h\", \n" +
                "tp.min_trx_app_3h as \"mintrxapp3h\", \n" +
                "tp.min_trx_dc_3h as \"mintrxdc3h\", \n" +
                "tp.min_trx_dc_free_3h as \"mintrxdcfree3h\", \n" +
                "tp.min_trx_rvsl_3h as \"mintrxrvsl3h\", \n" +
                "tp.min_trx_app_6h as \"mintrxapp6h\", \n" +
                "tp.min_trx_dc_6h as \"mintrxdc6h\", \n" +
                "tp.min_trx_dc_free_6h as \"mintrxdcfree6h\", \n" +
                "tp.min_trx_rvsl_6h as \"mintrxrvsl6h\", \n" +
                "tp.min_trx_app_1d as \"mintrxapp1d\", \n" +
                "tp.min_trx_dc_1d as \"mintrxdc1d\", \n" +
                "tp.min_trx_dc_free_1d as \"mintrxdcfree1d\", \n" +
                "tp.min_trx_app_3d as \"mintrxapp3d\", \n" +
                "tp.min_trx_dc_3d as \"mintrxdc3d\", \n" +
                "tp.min_trx_dc_free_3d as \"mintrxdcfree3d\", \n" +
                "tp.min_trx_rvsl_3d as \"mintrxrvsl3d\", \n" +
                "tp.min_trx_rvsl_1d as \"mintrxrvsl1d\", \n" +
                "tp.min_amt_app_1h as \"minamtapp1h\", \n" +
                "tp.min_amt_dc_1h as \"minamtdc1h\", \n" +
                "tp.min_amt_dc_free_1h as \"minamtdcfree1h\", \n" +
                "tp.min_amt_rvsl_1h as \"minamtrvsl1h\", \n" +
                "tp.min_amt_app_3h as \"minamtapp3h\", \n" +
                "tp.min_amt_dc_3h as \"minamtdc3h\", \n" +
                "tp.min_amt_dc_free_3h as \"minamtdcfree3h\", \n" +
                "tp.min_amt_rvsl_3h as \"minamtrvsl3h\", \n" +
                "tp.min_amt_app_6h as \"minamtapp6h\", \n" +
                "tp.min_amt_dc_6h as \"minamtdc6h\", \n" +
                "tp.min_amt_dc_free_6h as \"minamtdcfree6h\", \n" +
                "tp.min_amt_rvsl_6h as \"minamtrvsl6h\", \n" +
                "tp.min_amt_app_1d as \"minamtapp1d\", \n" +
                "tp.min_amt_dc_1d as \"minamtdc1d\", \n" +
                "tp.min_amt_dc_free_1d as \"minamtdcfree1d\", \n" +
                "tp.min_amt_rvsl_1d as \"minamtrvsl1d\", \n" +
                "tp.min_amt_app_3d as \"minamtapp3d\", \n" +
                "tp.min_amt_dc_3d as \"minamtdc3d\", \n" +
                "tp.min_amt_dc_free_3d as \"minamtdcfree3d\", \n" +
                "tp.min_amt_rvsl_3d as \"minamtrvsl3d\" \n" +
                "from tms_param_anmly_per_bank tp join tms_trxid ti on ti.trx_id = tp.trx_id where tp.bank_id = :bank_id";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("bank_id", bank_id);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyParamBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List getParamMaxAmount(String trx){
        String sql = "select ti.trx_name as \"trx\", tp.max_amt_tot_1h as \"maxamt1h\" from tms_param_anmly tp join tms_trxid ti on ti.trx_id = tp.trx_id where tp.trx_id = :trx";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("trx", trx);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyParamBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listPanAnomaly1H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "select \n" +
                "ta.period as \"period\", \n" +
                "to_char(to_date(ta.period,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "ta.time_fr_1h as \"timefr\", \n" +
                "ta.time_to_1h as \"timeto\", \n" +
                "ti.trx_name as \"trantype\", \n" +
                "ta.pan as \"pan\", \n" +
                "ta.issuer_bank as \"issuerbank\", \n" +
                "nvl(ta.pengirim,' ') as \"pengirim\", \n" +
                "ta.freq_app as \"freqapp\", \n" +
                "ta.freq_dc as \"freqdc\", \n" +
                "ta.freq_dc_free as \"freqdcfree\", \n" +
                "ta.freq_rvsl as \"freqrvsl\", \n" +
                "nvl(ta.total_amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(ta.total_amt_dc/100,0) as \"totalamtdc\", \n" +
                "nvl(ta.total_amt_dc_free/100,0) as \"totalamtdcfree\", \n" +
                "nvl(ta.total_amt_rvsl/100,0) as \"totalamtrvsl\", \n" +
                "nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0) as \"totalamt\", \n" +
                "TMS_PANDUMMY_STATUS(ta.pan, upper(ta.pengirim),  nvl(ta.freq_app,0),  nvl(ta.freq_dc,0), nvl(ta.freq_dc_free,0), nvl(ta.freq_rvsl,0), nvl(ta.total_amt_app/100,0), nvl(ta.total_amt_dc/100,0), nvl(ta.total_amt_dc_free/100,0), nvl(ta.total_amt_rvsl/100,0), nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0)) as \"chk\", \n" +
                 /*Modify by aaw 23/07/2021, fixing pan anomaly tidak membaca status aktif pada pan special treatment*/
                //"TMS_PAN_DUMMY_CHECK_EXIST(ta.pan, upper(ta.pengirim)) as \"chkcolor\", \n" +
                /*End*/
                "TMS_PANDUMMY_API.Check_Exist(ta.pan, upper(ta.pengirim)) as \"chkcolor\", \n" +
                "ta.status as \"status\" \n" +
                "from \n" +
                "tms_trxanmly1hour ta \n" +
                "join \n" +
                "tms_trxid ti on ti.trx_id = ta.tran_type \n" +
                "where \n" +
                "ta.period >= :periodfr \n" +
                "and ta.period <= :periodto \n" +
                "and substr(ta.time_fr_1h,1,2) || substr(ta.time_fr_1h,4,2) >= :timefr \n" +
                "and replace(substr(ta.time_to_1h,1,2),'00','24') || substr(ta.time_to_1h,4,2) <= :timeto \n";
        //"and ta.tran_type = :trxtype \n"; Modify by AAH 191125

        if(!trxtype.equals("01")){
            sql = sql + "and ta.tran_type = :trxtype \n";
        }else{
            sql = sql + "and ta.tran_type in ('01','85') \n";
        }

        if(bank_id != ""){
            sql = sql + "and ta.issuer_bank = '"+ getBankName(bank_id) +"' \n";
            //System.out.println();
            //   System.out.println(getBankName(bank_id));
        }//else {
        // System.out.println(getBankName(bank_id));
        // }

        if(!whitelist) {
            sql = sql + " and TMS_PANDUMMY_STATUS(ta.pan, upper(ta.pengirim),  nvl(ta.freq_app,0),  nvl(ta.freq_dc,0), nvl(ta.freq_dc_free,0),  nvl(ta.freq_rvsl,0), nvl(ta.total_amt_app/100,0), nvl(ta.total_amt_dc/100,0), nvl(ta.total_amt_dc_free/100,0), nvl(ta.total_amt_rvsl/100,0), nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by ta.period, ta.time_fr_1h, ti.trx_name, ta.pan, ta.issuer_bank, ta.pengirim ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
        query.setString("timefr", timefr);
        query.setString("timeto", timeto);
        if(!trxtype.equals("01")){
            query.setString("trxtype", trxtype);
        }
        //System.out.println(trxtype);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listPanAnomaly3H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "select \n" +
                "ta.period as \"period\", \n" +
                "to_char(to_date(ta.period,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "ta.time_fr_3h as \"timefr\", \n" +
                "ta.time_to_3h as \"timeto\", \n" +
                "ti.trx_name as \"trantype\", \n" +
                "ta.pan as \"pan\", \n" +
                "ta.issuer_bank as \"issuerbank\", \n" +
                "nvl(ta.pengirim,' ') as \"pengirim\", \n" +
                "ta.freq_app as \"freqapp\", \n" +
                "ta.freq_dc as \"freqdc\", \n" +
                "ta.freq_dc_free as \"freqdcfree\", \n" +
                "ta.freq_rvsl as \"freqrvsl\", \n" +
                "nvl(ta.total_amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(ta.total_amt_dc/100,0) as \"totalamtdc\", \n" +
                "nvl(ta.total_amt_dc_free/100,0) as \"totalamtdcfree\", \n" +
                "nvl(ta.total_amt_rvsl/100,0) as \"totalamtrvsl\", \n" +
                "nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0) as \"totalamt\", \n" +
                "TMS_PANDUMMY_STATUS(ta.pan, upper(ta.pengirim),  nvl(ta.freq_app,0),  nvl(ta.freq_dc,0), nvl(ta.freq_dc_free,0),  nvl(ta.freq_rvsl,0), nvl(ta.total_amt_app/100,0), nvl(ta.total_amt_dc/100,0), nvl(ta.total_amt_dc_free/100,0), nvl(ta.total_amt_rvsl/100,0), nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0)) as \"chk\", \n" +
                /*Modify by aaw 23/07/2021, fixing pan anomaly tidak membaca status aktif pada pan special treatment*/
                //"TMS_PAN_DUMMY_CHECK_EXIST(ta.pan, upper(ta.pengirim)) as \"chkcolor\", \n" +
                /*End*/
                "TMS_PANDUMMY_API.Check_Exist(ta.pan, upper(ta.pengirim)) as \"chkcolor\", \n" +
                "ta.status as \"status\" \n" +
                "from \n" +
                "tms_trxanmly3hour ta \n" +
                "join \n" +
                "tms_trxid ti on ti.trx_id = ta.tran_type \n" +
                "where \n" +
                "ta.period >= :periodfr \n" +
                "and ta.period <= :periodto \n" +
                "and substr(ta.time_fr_3h,1,2) || substr(ta.time_fr_3h,4,2) >= :timefr \n" +
                "and replace(substr(ta.time_to_3h,1,2),'00','24') || substr(ta.time_to_3h,4,2) <= :timeto \n";
        //"and ta.tran_type = :trxtype \n"; Modify by AAH 191125

        if(!trxtype.equals("01")){
            sql = sql + "and ta.tran_type = :trxtype \n";
        }else{
            sql = sql + "and ta.tran_type in ('01','85') \n";
        }

        if(bank_id != ""){
            sql = sql + "and ta.issuer_bank = '"+ getBankName(bank_id) +"' \n";
            //System.out.println();
            //System.out.println(getBankName(bank_id));
        }//else {
        // System.out.println(getBankName(bank_id));
        // }

        if(!whitelist) {
            sql = sql + " and TMS_PANDUMMY_STATUS(ta.pan, upper(ta.pengirim),  nvl(ta.freq_app,0),  nvl(ta.freq_dc,0), nvl(ta.freq_dc_free,0),  nvl(ta.freq_rvsl,0), nvl(ta.total_amt_app/100,0), nvl(ta.total_amt_dc/100,0), nvl(ta.total_amt_dc_free/100,0), nvl(ta.total_amt_rvsl/100,0), nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by ta.period, ta.time_fr_3h, ti.trx_name, ta.pan, ta.issuer_bank, ta.pengirim ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
        query.setString("timefr", timefr);
        query.setString("timeto", timeto);
        if(!trxtype.equals("01")){
            query.setString("trxtype", trxtype);
        }
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listPanAnomaly6H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "select \n" +
                "ta.period as \"period\", \n" +
                "to_char(to_date(ta.period,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "ta.time_fr_6h as \"timefr\", \n" +
                "ta.time_to_6h as \"timeto\", \n" +
                "ti.trx_name as \"trantype\", \n" +
                "ta.pan as \"pan\", \n" +
                "ta.issuer_bank as \"issuerbank\", \n" +
                "nvl(ta.pengirim,' ') as \"pengirim\", \n" +
                "ta.freq_app as \"freqapp\", \n" +
                "ta.freq_dc as \"freqdc\", \n" +
                "ta.freq_dc_free as \"freqdcfree\", \n" +
                "ta.freq_rvsl as \"freqrvsl\", \n" +
                "nvl(ta.total_amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(ta.total_amt_dc/100,0) as \"totalamtdc\", \n" +
                "nvl(ta.total_amt_dc_free/100,0) as \"totalamtdcfree\", \n" +
                "nvl(ta.total_amt_rvsl/100,0) as \"totalamtrvsl\", \n" +
                "nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0) as \"totalamt\", \n" +
                "TMS_PANDUMMY_STATUS(ta.pan, upper(ta.pengirim),  nvl(ta.freq_app,0),  nvl(ta.freq_dc,0), nvl(ta.freq_dc_free,0),  nvl(ta.freq_rvsl,0), nvl(ta.total_amt_app/100,0), nvl(ta.total_amt_dc/100,0), nvl(ta.total_amt_dc_free/100,0), nvl(ta.total_amt_rvsl/100,0), nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0)) as \"chk\", \n" +
                 /*Modify by aaw 23/07/2021, fixing pan anomaly tidak membaca status aktif pada pan special treatment*/
                //"TMS_PAN_DUMMY_CHECK_EXIST(ta.pan, upper(ta.pengirim)) as \"chkcolor\", \n" +
                /*End*/
                "TMS_PANDUMMY_API.Check_Exist(ta.pan, upper(ta.pengirim)) as \"chkcolor\", \n" +
                "ta.status as \"status\" \n" +
                "from \n" +
                "tms_trxanmly6hour ta \n" +
                "join \n" +
                "tms_trxid ti on ti.trx_id = ta.tran_type \n" +
                "where \n" +
                "ta.period >= :periodfr \n" +
                "and ta.period <= :periodto \n" +
                "and substr(ta.time_fr_6h,1,2) || substr(ta.time_fr_6h,4,2) >= :timefr \n" +
                "and replace(substr(ta.time_to_6h,1,2),'00','24') || substr(ta.time_to_6h,4,2) <= :timeto \n";
        //"and ta.tran_type = :trxtype \n"; Modify by AAH 191125

        if(!trxtype.equals("01")){
            sql = sql + "and ta.tran_type = :trxtype \n";
        }else{
            sql = sql + "and ta.tran_type in ('01','85') \n";
        }

        if(bank_id != ""){
            sql = sql + "and ta.issuer_bank = '"+ getBankName(bank_id) +"' \n";
            //System.out.println();
            //System.out.println(getBankName(bank_id));
        }//else {
        // System.out.println(getBankName(bank_id));
        // }

        if(!whitelist) {
            sql = sql + " and TMS_PANDUMMY_STATUS(ta.pan, upper(ta.pengirim),  nvl(ta.freq_app,0),  nvl(ta.freq_dc,0), nvl(ta.freq_dc_free,0),  nvl(ta.freq_rvsl,0), nvl(ta.total_amt_app/100,0), nvl(ta.total_amt_dc/100,0), nvl(ta.total_amt_dc_free/100,0), nvl(ta.total_amt_rvsl/100,0), nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by ta.period, ta.time_fr_6h, ti.trx_name, ta.pan, ta.issuer_bank, ta.pengirim ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
        query.setString("timefr", timefr);
        query.setString("timeto", timeto);
        if(!trxtype.equals("01")){
            query.setString("trxtype", trxtype);
        }
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listPanAnomaly1D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "select \n" +
                "ta.period as \"period\", \n" +
                "to_char(to_date(ta.period,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "'00:00' as \"timefr\", \n" +
                "'24:00' as \"timeto\", \n" +
                "ti.trx_name as \"trantype\", \n" +
                "ta.pan as \"pan\", \n" +
                "ta.issuer_bank as \"issuerbank\", \n" +
                "nvl(ta.pengirim,' ') as \"pengirim\", \n" +
                "ta.freq_app as \"freqapp\", \n" +
                "ta.freq_dc as \"freqdc\", \n" +
                "ta.freq_dc_free as \"freqdcfree\", \n" +
                "ta.freq_rvsl as \"freqrvsl\", \n" +
                "nvl(ta.total_amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(ta.total_amt_dc/100,0) as \"totalamtdc\", \n" +
                "nvl(ta.total_amt_dc_free/100,0) as \"totalamtdcfree\", \n" +
                "nvl(ta.total_amt_rvsl/100,0) as \"totalamtrvsl\", \n" +
                "nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0) as \"totalamt\", \n" +
                "TMS_PANDUMMY_STATUS(ta.pan, upper(ta.pengirim),  nvl(ta.freq_app,0),  nvl(ta.freq_dc,0), nvl(ta.freq_dc_free,0), nvl(ta.freq_rvsl,0), nvl(ta.total_amt_app/100,0), nvl(ta.total_amt_dc/100,0), nvl(ta.total_amt_dc_free/100,0), nvl(ta.total_amt_rvsl/100,0), nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0)) as \"chk\", \n" +
                 /*Modify by aaw 23/07/2021, fixing pan anomaly tidak membaca status aktif pada pan special treatment*/
                //"TMS_PAN_DUMMY_CHECK_EXIST(ta.pan, upper(ta.pengirim)) as \"chkcolor\", \n" +
                /*End*/
                "TMS_PANDUMMY_API.Check_Exist(ta.pan, upper(ta.pengirim)) as \"chkcolor\", \n" +
                "ta.status as \"status\" \n" +
                "from \n" +
                "tms_trxanmly1day ta \n" +
                "join \n" +
                "tms_trxid ti on ti.trx_id = ta.tran_type \n" +
                "where \n" +
                "ta.period between :periodfr and :periodto \n";
        //"and ta.tran_type = :trxtype \n"; Modify by AAH 191125

        if(!trxtype.equals("01")){
            sql = sql + "and ta.tran_type = :trxtype \n";
        }else{
            sql = sql + "and ta.tran_type in ('01','85') \n";
        }

        if(bank_id != ""){
            sql = sql + "and ta.issuer_bank = '"+ getBankName(bank_id) +"' \n";
            //System.out.println();
            //   System.out.println(getBankName(bank_id));
        }//else {
        // System.out.println(getBankName(bank_id));
        // }

        if(!whitelist) {
            sql = sql + " and TMS_PANDUMMY_STATUS(ta.pan, upper(ta.pengirim),  nvl(ta.freq_app,0),  nvl(ta.freq_dc,0), nvl(ta.freq_dc_free,0),  nvl(ta.freq_rvsl,0), nvl(ta.total_amt_app/100,0), nvl(ta.total_amt_dc/100,0), nvl(ta.total_amt_dc_free/100,0), nvl(ta.total_amt_rvsl/100,0), nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by ta.period, ti.trx_name, ta.pan, ta.issuer_bank, ta.pengirim ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
        if(!trxtype.equals("01")){
            query.setString("trxtype", trxtype);
        }
        //System.out.println(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyBean.class));
        return query.list();
    }
    //end modify by erichie

    @Transactional(readOnly=true)
    public List listPanAnomaly3D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "select \n" +
                "to_char(to_date(ta.period_fr,'YYMMDD'), 'DD-MM-YYYY') as \"period\", \n" +
                "to_char(to_date(ta.period_to,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "ta.period_fr as \"timefr\", \n" +
                "ta.period_to as \"timeto\", \n" +
                "ti.trx_name as \"trantype\", \n" +
                "ta.pan as \"pan\", \n" +
                "ta.issuer_bank as \"issuerbank\", \n" +
                "nvl(ta.pengirim,' ') as \"pengirim\", \n" +
                "ta.freq_app as \"freqapp\", \n" +
                "ta.freq_dc as \"freqdc\", \n" +
                "ta.freq_dc_free as \"freqdcfree\", \n" +
                "ta.freq_rvsl as \"freqrvsl\", \n" +
                "nvl(ta.total_amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(ta.total_amt_dc/100,0) as \"totalamtdc\", \n" +
                "nvl(ta.total_amt_dc_free/100,0) as \"totalamtdcfree\", \n" +
                "nvl(ta.total_amt_rvsl/100,0) as \"totalamtrvsl\", \n" +
                "nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0) as \"totalamt\", \n" +
                "TMS_PANDUMMY_STATUS(ta.pan, upper(ta.pengirim),  nvl(ta.freq_app,0),  nvl(ta.freq_dc,0), nvl(ta.freq_dc_free,0), nvl(ta.freq_rvsl,0), nvl(ta.total_amt_app/100,0), nvl(ta.total_amt_dc/100,0), nvl(ta.total_amt_dc_free/100,0), nvl(ta.total_amt_rvsl/100,0), nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0)) as \"chk\", \n" +
                 /*Modify by aaw 23/07/2021, fixing pan anomaly tidak membaca status aktif pada pan special treatment*/
                //"TMS_PAN_DUMMY_CHECK_EXIST(ta.pan, upper(ta.pengirim)) as \"chkcolor\", \n" +
                /*End*/
                "TMS_PANDUMMY_API.Check_Exist(ta.pan, upper(ta.pengirim)) as \"chkcolor\", \n" +
                "ta.status as \"status\" \n" +
                "from \n" +
                "tms_trxanmly3day ta \n" +
                "join \n" +
                "tms_trxid ti on ti.trx_id = ta.tran_type \n" +
                "where \n" +
                "ta.period_fr >= :periodfr and ta.period_to <= :periodto \n";
        //"and ta.tran_type = :trxtype \n"; Modify by AAH 191125

        if(!trxtype.equals("01")){
            sql = sql + "and ta.tran_type = :trxtype \n";
        }else{
            sql = sql + "and ta.tran_type in ('01','85') \n";
        }

        if(bank_id != ""){
            sql = sql + "and ta.issuer_bank = '"+ getBankName(bank_id) +"' \n";
            //System.out.println();
            //   System.out.println(getBankName(bank_id));
        }//else {
        // System.out.println(getBankName(bank_id));
        // }

        if(!whitelist) {
            sql = sql + " and TMS_PANDUMMY_STATUS(ta.pan, upper(ta.pengirim),  nvl(ta.freq_app,0),  nvl(ta.freq_dc,0), nvl(ta.freq_dc_free,0),  nvl(ta.freq_rvsl,0), nvl(ta.total_amt_app/100,0), nvl(ta.total_amt_dc/100,0), nvl(ta.total_amt_dc_free/100,0), nvl(ta.total_amt_rvsl/100,0), nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0)) = 'FALSE' ";
        }

        sql = sql + "order by ta.period_fr, ti.trx_name, ta.pan, ta.issuer_bank, ta.pengirim ";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
        if(!trxtype.equals("01")){
            query.setString("trxtype", trxtype);
        }
        //System.out.println(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listMaxAmountAnomalyPan1H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "select \n" +
                "ta.period as \"period\", \n" +
                "ta.time_trx as \"timefr\", \n" +
                "ta.issuer_bank as \"issuerbank\", \n" +
                "ti.trx_name as \"trantype\", \n" +
                "ta.pan as \"pan\", \n" +
                "nvl(ta.pengirim,' ') as \"pengirim\", \n" +
                "ta.message_type as \"messagetype\", \n" +
                "ta.rt_cd as \"responsecode\", \n" +
                "ta.pcode as \"pcode\", \n" +
                "ta.trace_num as \"tracenum\", \n" +
                "ta.acquirer_bank as \"acquirerbank\", \n" +
                "ta.beneficiary_bank as \"beneficiarybank\", \n" +
                "ta.term_id as \"termid\", \n" +
                "ta.term_owner_name as \"termownername\", \n" +
                "ta.pos_entry_mode as \"posentrymode\", \n" +
                "ta.to_account as \"toacct\", \n" +
                "ta.description as \"description\", \n" +
                "nvl(ta.atm_seq_num,' ') as \"atmseqnum\", \n" +
                "ta.amount / 100 as \"amount\", \n" +
                "ta.status as \"status\" \n" +
                "from \n" +
                "tms_trxanmlyamt1hour ta \n" +
                "join \n" +
                "tms_trxid ti on ti.trx_id = ta.tran_type \n" +
                "where \n" +
                "ta.period >= :periodfr and ta.period <= :periodto \n" +
                "and substr(ta.time_trx,1,2) || substr(ta.time_trx,4,2) >= :timefr and replace(substr(ta.time_trx,1,2),'00','24') || substr(ta.time_trx,4,2) <= :timeto \n";

        if(!trxtype.equals("01")){
            sql = sql + "and ta.tran_type = :trxtype \n";
        }else{
            sql = sql + "and ta.tran_type in ('01','85') \n";
        }

//        if(bank_id != ""){
//            sql = sql + "and ta.issuer_bank = '"+ getBankName(bank_id) +"' \n";
//            //System.out.println();
//            //   System.out.println(getBankName(bank_id));
//        }//else {
//        // System.out.println(getBankName(bank_id));
//        // }

        sql = sql + "order by period, pan, pengirim";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
        query.setString("timefr", timefr);
        query.setString("timeto", timeto);
        if(!trxtype.equals("01")){
            query.setString("trxtype", trxtype);
        }

        /*System.out.println(sql);
        System.out.println("tran_type" + trxtype);
        System.out.println("time_fr" + timefr);
        System.out.println("time_to" + timeto);
        System.out.println("period_fr" + periodfr);
        System.out.println("period_to" + periodto);*/

        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyBean.class));
        return query.list();

    }

    @Transactional(readOnly=true)
    public List listPanAnomalyBene1H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "select \n" +
                "period as \"period\", \n" +
                "to_char(to_date(period,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "time_fr_1h as \"timefr\", \n" +
                "time_to_1h as \"timeto\", \n" +
                "to_acct as \"toacct\", \n" +
                "beneficiary_bank as \"beneficiarybank\", \n" +
                "beneficiary_name as \"beneficiaryname\", \n" +
                "freq_app as \"freqapp\", \n" +
                "freq_dc as \"freqdc\", \n" +
                "freq_dc_free as \"freqdcfree\", \n" +
                "freq_rvsl as \"freqrvsl\", \n" +
                "nvl(total_amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(total_amt_dc/100,0) as \"totalamtdc\", \n" +
                "nvl(total_amt_dc_free/100,0) as \"totalamtdcfree\", \n" +
                "nvl(total_amt_rvsl/100,0) as \"totalamtrvsl\", \n" +
                "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0) as \"totalamt\", \n" +
                //Add by aaw 21/05/21
                "TMS_BENEDUMMY_CHECK_EXIST(trim(to_acct), upper(beneficiary_name)) as \"chkcolor\" \n" +
                //End add
                "from \n" +
                "tms_trxanmly_bene_1hour \n" +
                "where \n" +
                "period >= :periodfr \n" +
                "and period <= :periodto \n" +
                "and substr(time_fr_1h,1,2) || substr(time_fr_1h,4,2) >= :timefr \n" +
                "and replace(substr(time_to_1h,1,2),'00','24') || substr(time_to_1h,4,2) <= :timeto \n";
        //"and ta.tran_type = :trxtype \n"; Modify by AAH 191125

//        if(!trxtype.equals("01")){
//            sql = sql + "and ta.tran_type = :trxtype \n";
//        }else{
//            sql = sql + "and ta.tran_type in ('01','85') \n";
//        }
        /*Uncomment and modify by aaw 29/06/2021*/
        if(bank_id != ""){
            sql = sql + "and beneficiary_bank = '"+ getBankName(bank_id) +"' \n";
//            //System.out.println();
//            //   System.out.println(getBankName(bank_id));
        }//else {
////         System.out.println(getBankName(bank_id));
////         }

        /*Add by aaw 21/5/21*/
        if(!whitelist) {
            sql = sql + " and TMS_BENEDUMMY_STTS(trim(to_acct), upper(trim(beneficiary_name)), nvl(freq_app,0), nvl(freq_dc,0), nvl(freq_dc_free,0), nvl(freq_rvsl,0), nvl(total_amt_app/100,0),\n" +
                    "nvl(total_amt_dc/100,0), nvl(total_amt_dc_free/100,0), nvl(total_amt_rvsl/100,0),\n" +
                    "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0)) = 'FALSE' ";
        }
        /*End add*/

        sql = sql + "order by period, time_fr_1h, beneficiary_bank, beneficiary_name";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
        query.setString("timefr", timefr);
        query.setString("timeto", timeto);
//        if(!trxtype.equals("01")){
//            query.setString("trxtype", trxtype);
//        }
        //System.out.println(trxtype);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyBean.class));
        System.out.println(sql);
        System.out.println(periodfr);
        System.out.println(periodto);
        System.out.println(timefr);
        System.out.println(timeto);
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listPanAnomalyBene3H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "select \n" +
                "period as \"period\", \n" +
                "to_char(to_date(period,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "time_fr_3h as \"timefr\", \n" +
                "time_to_3h as \"timeto\", \n" +
                "to_acct as \"toacct\", \n" +
                "beneficiary_bank as \"beneficiarybank\", \n" +
                "beneficiary_name as \"beneficiaryname\", \n" +
                "freq_app as \"freqapp\", \n" +
                "freq_dc as \"freqdc\", \n" +
                "freq_dc_free as \"freqdcfree\", \n" +
                "freq_rvsl as \"freqrvsl\", \n" +
                "nvl(total_amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(total_amt_dc/100,0) as \"totalamtdc\", \n" +
                "nvl(total_amt_dc_free/100,0) as \"totalamtdcfree\", \n" +
                "nvl(total_amt_rvsl/100,0) as \"totalamtrvsl\", \n" +
                "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0) as \"totalamt\", \n" +
                //Add by aaw 21/05/21
                "TMS_BENEDUMMY_CHECK_EXIST(trim(to_acct), upper(beneficiary_name)) as \"chkcolor\" \n" +
                //End add
                "from \n" +
                "tms_trxanmly_bene_3hour \n" +
                "where \n" +
                "period >= :periodfr \n" +
                "and period <= :periodto \n" +
                "and substr(time_fr_3h,1,2) || substr(time_fr_3h,4,2) >= :timefr \n" +
                "and replace(substr(time_to_3h,1,2),'00','24') || substr(time_to_3h,4,2) <= :timeto \n";
        //"and ta.tran_type = :trxtype \n"; Modify by AAH 191125

//        if(!trxtype.equals("01")){
//            sql = sql + "and ta.tran_type = :trxtype \n";
//        }else{
//            sql = sql + "and ta.tran_type in ('01','85') \n";
//        }
        /*Uncomment and modify by aaw 29/06/2021*/
        if(bank_id != ""){
            sql = sql + "and beneficiary_bank = '"+ getBankName(bank_id) +"' \n";
//            //System.out.println();
//            //System.out.println(getBankName(bank_id));
        }//else {
//        // System.out.println(getBankName(bank_id));
//        // }
//
        /*Add by aaw 21/5/21*/
        if(!whitelist) {
            sql = sql + " and TMS_BENEDUMMY_STTS(trim(to_acct), upper(trim(beneficiary_name)), nvl(freq_app,0), nvl(freq_dc,0), nvl(freq_dc_free,0), nvl(freq_rvsl,0), nvl(total_amt_app/100,0),\n" +
                    "nvl(total_amt_dc/100,0), nvl(total_amt_dc_free/100,0), nvl(total_amt_rvsl/100,0),\n" +
                    "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0)) = 'FALSE' ";
        }
        /*End add*/

        sql = sql + "order by period, time_fr_3h, beneficiary_bank, beneficiary_name";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
        query.setString("timefr", timefr);
        query.setString("timeto", timeto);
//        if(!trxtype.equals("01")){
//            query.setString("trxtype", trxtype);
//        }
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyBean.class));
        System.out.println(sql);
        System.out.println(timefr);
        System.out.println(timeto);
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listPanAnomalyBene6H(String periodfr, String periodto, String timefr, String timeto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "select \n" +
                "period as \"period\", \n" +
                "to_char(to_date(period,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "time_fr_6h as \"timefr\", \n" +
                "time_to_6h as \"timeto\", \n" +
                "to_acct as \"toacct\", \n" +
                "beneficiary_bank as \"beneficiarybank\", \n" +
                "beneficiary_name as \"beneficiaryname\", \n" +
                "freq_app as \"freqapp\", \n" +
                "freq_dc as \"freqdc\", \n" +
                "freq_dc_free as \"freqdcfree\", \n" +
                "freq_rvsl as \"freqrvsl\", \n" +
                "nvl(total_amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(total_amt_dc/100,0) as \"totalamtdc\", \n" +
                "nvl(total_amt_dc_free/100,0) as \"totalamtdcfree\", \n" +
                "nvl(total_amt_rvsl/100,0) as \"totalamtrvsl\", \n" +
                "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0) as \"totalamt\", \n" +
                //Add by aaw 21/05/21
                "TMS_BENEDUMMY_CHECK_EXIST(trim(to_acct), upper(beneficiary_name)) as \"chkcolor\" \n" +
                //End add
                "from \n" +
                "tms_trxanmly_bene_6hour \n" +
                "where \n" +
                "period >= :periodfr \n" +
                "and period <= :periodto \n" +
                "and substr(time_fr_6h,1,2) || substr(time_fr_6h,4,2) >= :timefr \n" +
                "and replace(substr(time_to_6h,1,2),'00','24') || substr(time_to_6h,4,2) <= :timeto \n";
        //"and ta.tran_type = :trxtype \n"; Modify by AAH 191125

//        if(!trxtype.equals("01")){
//            sql = sql + "and ta.tran_type = :trxtype \n";
//        }else{
//            sql = sql + "and ta.tran_type in ('01','85') \n";
//        }
        /*Uncomment and modify by aaw 29/06/2021*/
        if(bank_id != ""){
            sql = sql + "and beneficiary_bank = '"+ getBankName(bank_id) +"' \n";
//            //System.out.println();
//            //System.out.println(getBankName(bank_id));
        }//else {
//        // System.out.println(getBankName(bank_id));
//        // }
//
        /*Add by aaw 21/5/21*/
        if(!whitelist) {
            sql = sql + " and TMS_BENEDUMMY_STTS(trim(to_acct), upper(trim(beneficiary_name)), nvl(freq_app,0), nvl(freq_dc,0), nvl(freq_dc_free,0), nvl(freq_rvsl,0), nvl(total_amt_app/100,0),\n" +
                    "nvl(total_amt_dc/100,0), nvl(total_amt_dc_free/100,0), nvl(total_amt_rvsl/100,0),\n" +
                    "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0)) = 'FALSE' ";
        }
        /*End add*/

        sql = sql + "order by period, time_fr_6h, beneficiary_bank, beneficiary_name";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
        query.setString("timefr", timefr);
        query.setString("timeto", timeto);
//        if(!trxtype.equals("01")){
//            query.setString("trxtype", trxtype);
//        }
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyBean.class));
        System.out.println(sql);
        System.out.println(timefr);
        System.out.println(timeto);
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listPanAnomalyBene1D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "select \n" +
                "period as \"period\", \n" +
                "to_char(to_date(period,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
                "to_acct as \"toacct\", \n" +
                "beneficiary_bank as \"beneficiarybank\", \n" +
                "beneficiary_name as \"beneficiaryname\", \n" +
                "freq_app as \"freqapp\", \n" +
                "freq_dc as \"freqdc\", \n" +
                "freq_dc_free as \"freqdcfree\", \n" +
                "freq_rvsl as \"freqrvsl\", \n" +
                "nvl(total_amt_app/100,0) as \"totalamtapp\", \n" +
                "nvl(total_amt_dc/100,0) as \"totalamtdc\", \n" +
                "nvl(total_amt_dc_free/100,0) as \"totalamtdcfree\", \n" +
                "nvl(total_amt_rvsl/100,0) as \"totalamtrvsl\", \n" +
                "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0) as \"totalamt\", \n" +
                //Add by aaw 21/05/21
                "TMS_BENEDUMMY_CHECK_EXIST(trim(to_acct), upper(beneficiary_name)) as \"chkcolor\" \n" +
                //End add
                "from \n" +
                "tms_trxanmly_bene_1day \n" +
                "where \n" +
                "period between :periodfr and :periodto \n";
        //"and ta.tran_type = :trxtype \n"; Modify by AAH 191125

//        if(!trxtype.equals("01")){
//            sql = sql + "and ta.tran_type = :trxtype \n";
//        }else{
//            sql = sql + "and ta.tran_type in ('01','85') \n";
//        }
        /*Uncomment and modify by aaw 29/06/2021*/
        if(bank_id != ""){
            sql = sql + "and beneficiary_bank = '"+ getBankName(bank_id) +"' \n";
//            //System.out.println();
//            //   System.out.println(getBankName(bank_id));
        }//else {
//        // System.out.println(getBankName(bank_id));
//        // }
//
        /*Add by aaw 21/5/21*/
        if(!whitelist) {
            sql = sql + " and TMS_BENEDUMMY_STTS(trim(to_acct), upper(trim(beneficiary_name)), nvl(freq_app,0), nvl(freq_dc,0), nvl(freq_dc_free,0), nvl(freq_rvsl,0), nvl(total_amt_app/100,0),\n" +
                    "nvl(total_amt_dc/100,0), nvl(total_amt_dc_free/100,0), nvl(total_amt_rvsl/100,0),\n" +
                    "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0)) = 'FALSE' ";
        }
        /*End add*/

        sql = sql + "order by period, beneficiary_bank, beneficiary_name";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
//        if(!trxtype.equals("01")){
//            query.setString("trxtype", trxtype);
//        }
        //System.out.println(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyBean.class));
        System.out.println(sql);
        System.out.println(periodfr);
        System.out.println(periodto);
        return query.list();
    }
    //end modify by erichie

    @Transactional(readOnly=true)
    public List listPanAnomalyBene3D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id){
        String sql = "SELECT\n" +
                "TO_CHAR(to_date(period_fr, 'YYMMDD'), 'DD-MM-YYYY') AS \"period\",\n" +
                "TO_CHAR(to_date(period_to, 'YYMMDD'), 'DD-MM-YYYY') AS \"perioddisplay\",\n" +
                /*Add by aaw 13/07/2021, fixing detail beneficiary for 3D*/
                "period_fr as \"periodFrom\", \n" +
                "period_to as \"periodTo\", \n" +
                /*End add*/
                "to_acct AS \"toacct\",\n" +
                "beneficiary_bank AS \"beneficiarybank\",\n" +
                "beneficiary_name AS \"beneficiaryname\",\n" +
                "freq_app AS \"freqapp\",\n" +
                "freq_dc AS \"freqdc\",\n" +
                "freq_dc_free AS \"freqdcfree\",\n" +
                "freq_rvsl AS \"freqrvsl\",\n" +
                "NVL(total_amt_app / 100, 0) AS \"totalamtapp\",\n" +
                "NVL(total_amt_dc / 100, 0) AS \"totalamtdc\",\n" +
                "NVL(total_amt_dc_free / 100, 0) AS \"totalamtdcfree\",\n" +
                "NVL(total_amt_rvsl / 100, 0) AS \"totalamtrvsl\",\n" +
                "NVL(total_amt_app / 100, 0) + NVL(total_amt_dc / 100, 0) + NVL(total_amt_dc_free / 100, 0) + NVL(total_amt_rvsl / 100, 0) AS \"totalamt\",\n" +
                //Add by aaw 21/05/21
                "TMS_BENEDUMMY_CHECK_EXIST(trim(to_acct), upper(beneficiary_name)) as \"chkcolor\" \n" +
                //End add
                "FROM\n" +
                "TMS_TRXANMLY_BENE_3DAY\n" +
                "WHERE\n" +
                "period_fr >= :periodfr\n" +
                "AND period_to <= :periodto\n";
        //"and ta.tran_type = :trxtype \n"; Modify by AAH 191125

//        if(!trxtype.equals("01")){
//            sql = sql + "and ta.tran_type = :trxtype \n";
//        }else{
//            sql = sql + "and ta.tran_type in ('01','85') \n";
//        }
        /*Uncomment and modify by aaw 29/06/2021*/
        if(bank_id != ""){
            sql = sql + "and beneficiary_bank = '"+ getBankName(bank_id) +"' \n";
//            //System.out.println();
//            //   System.out.println(getBankName(bank_id));
        }//else {
//        // System.out.println(getBankName(bank_id));
//        // }

        /*Add by aaw 21/5/21*/
        if(!whitelist) {
            sql = sql + " and TMS_BENEDUMMY_STTS(trim(to_acct), upper(trim(beneficiary_name)), nvl(freq_app,0), nvl(freq_dc,0), nvl(freq_dc_free,0), nvl(freq_rvsl,0), nvl(total_amt_app/100,0),\n" +
                    "nvl(total_amt_dc/100,0), nvl(total_amt_dc_free/100,0), nvl(total_amt_rvsl/100,0),\n" +
                    "nvl(total_amt_app/100,0) + nvl(total_amt_dc/100,0) + nvl(total_amt_dc_free/100,0) + nvl(total_amt_rvsl/100,0)) = 'FALSE' ";
        }
        /*End add*/

        sql = sql + "order by period_fr, beneficiary_bank, beneficiary_name";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("periodfr", periodfr);
        query.setString("periodto", periodto);
//        if(!trxtype.equals("01")){
//            query.setString("trxtype", trxtype);
//        }
        //System.out.println(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyBean.class));
        return query.list();
    }
//    @Transactional(readOnly=true)
//    public List listPanAnomalyBene3D(String periodfr, String periodto, String trxtype, Boolean whitelist, String bank_id){
//        String sql = "select \n" +
//                "to_char(to_date(ta.period_fr,'YYMMDD'), 'DD-MM-YYYY') as \"period\", \n" +
//                "to_char(to_date(ta.period_to,'YYMMDD'), 'DD-MM-YYYY') as \"perioddisplay\", \n" +
//                "ta.period_fr as \"timefr\", \n" +
//                "ta.period_to as \"timeto\", \n" +
//                "ti.trx_name as \"trantype\", \n" +
//                "ta.pan as \"pan\", \n" +
//                "ta.issuer_bank as \"issuerbank\", \n" +
//                "nvl(ta.pengirim,' ') as \"pengirim\", \n" +
//                "ta.freq_app as \"freqapp\", \n" +
//                "ta.freq_dc as \"freqdc\", \n" +
//                "ta.freq_dc_free as \"freqdcfree\", \n" +
//                "ta.freq_rvsl as \"freqrvsl\", \n" +
//                "nvl(ta.total_amt_app/100,0) as \"totalamtapp\", \n" +
//                "nvl(ta.total_amt_dc/100,0) as \"totalamtdc\", \n" +
//                "nvl(ta.total_amt_dc_free/100,0) as \"totalamtdcfree\", \n" +
//                "nvl(ta.total_amt_rvsl/100,0) as \"totalamtrvsl\", \n" +
//                "nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0) as \"totalamt\", \n" +
//                "TMS_PANDUMMY_STATUS(ta.pan, upper(ta.pengirim),  nvl(ta.freq_app,0),  nvl(ta.freq_dc,0), nvl(ta.freq_dc_free,0), nvl(ta.freq_rvsl,0), nvl(ta.total_amt_app/100,0), nvl(ta.total_amt_dc/100,0), nvl(ta.total_amt_dc_free/100,0), nvl(ta.total_amt_rvsl/100,0), nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0)) as \"chk\", \n" +
//                "TMS_PANDUMMY_API.Check_Exist(ta.pan, upper(ta.pengirim)) as \"chkcolor\" \n" +
//                "from \n" +
//                "tms_trxanmly3day ta \n" +
//                "join \n" +
//                "tms_trxid ti on ti.trx_id = ta.tran_type \n" +
//                "where \n" +
//                "ta.period_fr >= :periodfr and ta.period_to <= :periodto \n";
//        //"and ta.tran_type = :trxtype \n"; Modify by AAH 191125
//
//        if(!trxtype.equals("01")){
//            sql = sql + "and ta.tran_type = :trxtype \n";
//        }else{
//            sql = sql + "and ta.tran_type in ('01','85') \n";
//        }
//
//        if(bank_id != ""){
//            sql = sql + "and ta.issuer_bank = '"+ getBankName(bank_id) +"' \n";
//            //System.out.println();
//            //   System.out.println(getBankName(bank_id));
//        }//else {
//        // System.out.println(getBankName(bank_id));
//        // }
//
//        if(!whitelist) {
//            sql = sql + " and TMS_PANDUMMY_STATUS(ta.pan, upper(ta.pengirim),  nvl(ta.freq_app,0),  nvl(ta.freq_dc,0), nvl(ta.freq_dc_free,0),  nvl(ta.freq_rvsl,0), nvl(ta.total_amt_app/100,0), nvl(ta.total_amt_dc/100,0), nvl(ta.total_amt_dc_free/100,0), nvl(ta.total_amt_rvsl/100,0), nvl(ta.total_amt_app/100,0) + nvl(ta.total_amt_dc/100,0) + nvl(ta.total_amt_dc_free/100,0) + nvl(ta.total_amt_rvsl/100,0)) = 'FALSE' ";
//        }
//
//        sql = sql + "order by ta.period_fr, ti.trx_name, ta.pan, ta.issuer_bank, ta.pengirim ";
//
//        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
//        query.setString("periodfr", periodfr);
//        query.setString("periodto", periodto);
//        if(!trxtype.equals("01")){
//            query.setString("trxtype", trxtype);
//        }
//        //System.out.println(sql);
//        query.setResultTransformer(new AliasToBeanResultTransformer(PanAnomalyBean.class));
//        return query.list();
//    }

    /*Add by ait 10/05/21, add detail beneficiary*/
    @Transactional(readOnly=true)
    public List listDetailBeneficiaryAtm(String period, String timeFr, String timeTo, String acct, String tranType, String receiver, String periodTo, String periodType) throws IOException{
        String linkDbAtm =  ReadProperty.dbLinkActive().getLinkAtm();

        String sql = "select \n" +
                "to_char(to_date(cur_dat_add,'YYMMDD'), 'DD-MM-YYYY') as \"period\", \n" +
                "substr(cur_tim_add,1,2) || ':' || substr(cur_tim_add,3,2) || ':' || substr(cur_tim_add,5,2) as \"trxtime\", \n" +
                "atm_term_owner_name as \"terminal\", \n" +
                "atm_term_id as \"termid\", \n" +
                "nvl(atm_amt_1/100,0) as \"amount\", \n" +
                "atm_rte_responder || atm_resp as \"rc\", \n" +
                "atm_typ as \"msgtype\", \n" +
                "pos_entry_mode as \"posentrymode\", \n" +
                "tms_bankfiid_api.Get_Bank_Name(atm_term_fiid) as \"bankacq\", \n" +
                "nvl(tms_respid_api.Get_Resp_Name(atm_rte_responder || atm_resp), 'Decline Free') as \"description\", \n" +
                "r1_issuer as \"pengirim\", \n" +
                /*Modify by aaw 16/08/2021, ubah dari card_no_prikey ke card_no*/
                "card_no as \"pan\", \n" +
                "nvl(tms_bank_api.get_bank_name(nvl(tms_bin_api.get_bank_id(substr(card_no_prikey,1,6)), nvl(tms_bin_api.get_bank_id(substr(card_no_prikey,1,8)), tms_bin_api.get_bank_id(substr(card_no_prikey,1,2))))), ' ') as \"issuerbank\" \n" +
                "from \n" +
                linkDbAtm  + "\n" +
                "where \n";

        /*Modify and add by aaw 13/07/2021*/
        sql = sql  + " atm_tran_cde = :trantype \n" +
                " and nvl(tms_bank_api.get_bank_code(nvl(tms_bankfiid_api.get_bank_id(atm_crd_ln),' ')),'000') = :bankId \n" +
                " and trim(atm_to_acct) = :acctno \n";


        if (!"".equals(timeFr.trim())&& !"".equals(timeTo.trim())) {
            sql = sql + "and cur_tim_add >= :timefr \n" +
                    "and cur_tim_add <= :timeto \n";
        } else {
            sql = sql + "and cur_tim_add >= '00000000' and cur_tim_add <= '24000000' \n";
        }
        /*End*/

        /*Add by aaw 13/07/2021, fixing detail beneficiary for 3D*/
        if("3D".equalsIgnoreCase(periodType)) {
            sql = sql + " and cur_dat_add >= :period and cur_dat_add <= :periodTo \n";
        } else {
            sql = sql + " and cur_dat_add = :period \n";
        }
        /*End add*/

        if(tranType.equals("40")){sql = sql + " and nvl(upper(trim(r1_beneficiary)),' ') = nvl(:receiver,' ') \n";}

        sql = sql + "group by \n" +
                "cur_dat_add," +
                "substr(cur_tim_add,1,2) || ':' || substr(cur_tim_add,3,2) || ':' || substr(cur_tim_add,5,2), \n" +
                "atm_term_owner_name, atm_term_id, \n" +
                "atm_amt_1, \n" +
                "atm_rte_responder || atm_resp, \n"+
                "atm_typ, \n"+
                "pos_entry_mode,\n" +
                "tms_bankfiid_api.Get_Bank_Name(atm_term_fiid), \n" +
                "tms_respid_api.Get_Resp_Name(atm_rte_responder || atm_resp)," +
                /*Modify by aaw 16/08/2021, ubah dari card_no_prikey ke card_no*/
                "r1_issuer, card_no, \n" +
                /*Add by aaw 14/07/2021, fixing selisih jumlah total transaksi di detail*/
                "atm_seq_num, \n" +
                /*End add*/
                "NVL(tms_bank_api.get_bank_name(NVL(tms_bin_api.get_bank_id(SUBSTR(card_no_prikey, 1, 6)), NVL(tms_bin_api.get_bank_id(SUBSTR(card_no_prikey, 1, 8)), tms_bin_api.get_bank_id(SUBSTR(card_no_prikey, 1, 2))))), ' ') \n";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);

        if(timeFr.replace(":", "").length() != 6){
            query.setString("period", period);
        }
        if (!"".equals(timeFr.trim())&& !"".equals(timeTo.trim())) {
            query.setString("timefr", timeFr.replace(":", ""));
            query.setString("timeto", timeTo.replace(":", ""));
        }

        /*Add by aaw 13/07/2021*/
        if("3D".equalsIgnoreCase(periodType)) {
            query.setString("periodTo", periodTo);
        }
        /*End add*/

        query.setString("trantype", tranType);
        String[] parts = (acct.trim()).split("-");
        String acctno = parts[1];
        /*Add by aaw 14/07/2021, fixing detail*/
        String bankId = parts[0];
        query.setString("bankId", bankId);
        /*End add*/
        query.setString("acctno", acctno);
        if(tranType.equals("40")){query.setString("receiver", (receiver.trim()).toUpperCase());}

        System.out.println("Period Type : " + periodType + " Trantype : " + tranType + " IdBank : " + bankId + " Account : " + acctno + " Receiver: " + receiver + " Period : " + period + " Period To : " + periodTo + " Time From : " + timeFr + " Time To : " + timeTo);
        System.out.println("================== Query detail Beneficiary ================== \n" + sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(DetailBeneficiaryAnomalyBean.class));
        return query.list();
    }

    @Transactional(readOnly=true)
    public List listBeneficiary(){
        String sql = "select account as \"account\", penerima as \"penerima\", freq_app as \"freqApp\", freq_dc as \"freqDc\", freq_dc_free as \"freqDcFree\", freq_rvsl as \"freqRvsl\", total_amt_app as \"totalAmtApp\", total_amt_dc as \"totalAmtDc\", total_amt_dc_free as \"totalAmtDcFree\", total_amt_rvsl as \"totalAmtRvsl\", total_amt as \"totalAmt\", active as \"active\" from tms_benedummy order by account";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(new AliasToBeanResultTransformer(WhitelistBeneficiaryBean.class));
        return query.list();
    }

    @Transactional
    public void deleteBeneficiary(String account, String penerima) {
        String sql = "delete from tms_benedummy where account = :account and penerima = :penerima";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("account", account.trim());
        query.setString("penerima", penerima.trim());
        query.executeUpdate();
    }

    @Transactional(readOnly=true)
    public List listBeneficiaryReceiver(String account, String penerima){
        String sql = "select account as \"account\", penerima as \"penerima\", freq_app as \"freqApp\", freq_dc as \"freqDc\", freq_dc_free as \"freqDcFree\", freq_rvsl as \"freqRvsl\", total_amt_app as \"totalAmtApp\"," +
                "total_amt_dc as \"totalAmtDc\", total_amt_dc_free as \"totalAmtDcFree\", total_amt_rvsl as \"totalAmtRvsl\", total_amt as \"totalAmt\", active as \"active\" from\n" +
                "tms_benedummy where trim(account) = :account and upper(trim(penerima)) = :penerima";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("account", account.trim());
        query.setString("penerima", penerima.trim());
        return query.list();
    }

    @Transactional
    public void saveBeneficiary(String account, String penerima, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree,
                                BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) {
        String sql = "insert into tms_benedummy values (:account, :penerima, :freqApp, :freqDc, :freqDcFree, :freqRvsl, :totalAmtApp, :totalAmtDc, :totalAmtDcFree, :totalAmtRvsl, :totalAmt, :active)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("account", account.trim());
        query.setString("penerima", penerima.trim());
        query.setBigDecimal("freqApp", freqApp);
        query.setBigDecimal("freqDc", freqDc);
        query.setBigDecimal("freqDcFree", freqDcFree);
        query.setBigDecimal("freqRvsl", freqRvsl);
        query.setBigDecimal("totalAmtApp", totalAmtApp);
        query.setBigDecimal("totalAmtDc", totalAmtDc);
        query.setBigDecimal("totalAmtDcFree", totalAmtDcFree);
        query.setBigDecimal("totalAmtRvsl", totalAmtRvsl);
        query.setBigDecimal("totalAmt", totalAmt);
        query.setString("active", active);
        query.executeUpdate();
    }

    @Transactional(readOnly=true)
    public List getWhitelistBeneficiary(String account, String penerima){
        String sql = "select account as \"account\", penerima as \"penerima\", freq_app as \"freqApp\", freq_dc as \"freqDc\", freq_dc_free as \"freqDcFree\", freq_rvsl as \"freqRvsl\", total_amt_app as \"totalAmtApp\", total_amt_dc as \"totalAmtDc\", total_amt_dc_free as \"totalAmtDcFree\", total_amt_rvsl as \"totalAmtRvsl\", total_amt as \"totalAmt\", active as \"active\" from tms_benedummy where account = :account and nvl(trim(upper(penerima)),' ') = nvl(trim(upper(:penerima)),' ')";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("account", account.trim());
        query.setString("penerima", penerima.trim());
        query.setResultTransformer(new AliasToBeanResultTransformer(WhitelistBeneficiaryBean.class));
        return query.list();
    }

    @Transactional
    public void updateBeneficiary(String account, String penerima, BigDecimal freqApp, BigDecimal freqDc, BigDecimal freqDcFree, BigDecimal freqRvsl, BigDecimal totalAmtApp, BigDecimal totalAmtDc, BigDecimal totalAmtDcFree, BigDecimal totalAmtRvsl, BigDecimal totalAmt, String active) {
        String sql = "update tms_benedummy set freq_app = :freqApp, freq_dc = :freqDc, freq_dc_free = :freqDcFree, freq_rvsl = :freqRvsl, total_amt_app = :totalAmtApp, total_amt_dc = :totalAmtDc, total_amt_dc_free = :totalAmtDcFree, total_amt_rvsl = :totalAmtRvsl, total_amt = :totalAmt, active = :active where account = :account and penerima = :penerima";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.setString("account", account.trim());
        query.setString("penerima", penerima.trim());
        query.setBigDecimal("freqApp", freqApp);
        query.setBigDecimal("freqDc", freqDc);
        query.setBigDecimal("freqDcFree", freqDcFree);
        query.setBigDecimal("freqRvsl", freqRvsl);
        query.setBigDecimal("totalAmtApp", totalAmtApp);
        query.setBigDecimal("totalAmtDc", totalAmtDc);
        query.setBigDecimal("totalAmtDcFree", totalAmtDcFree);
        query.setBigDecimal("totalAmtRvsl", totalAmtRvsl);
        query.setBigDecimal("totalAmt", totalAmt);
        query.setString("active", active);
        query.executeUpdate();
    }
    /*End add*/
}
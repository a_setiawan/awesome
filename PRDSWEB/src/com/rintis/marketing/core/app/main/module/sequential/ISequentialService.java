package com.rintis.marketing.core.app.main.module.sequential;

import com.rintis.marketing.beans.bean.bi.PanAnomalyEmailBean;
import com.rintis.marketing.beans.bean.bi.SequentialAnomalyBean;
import com.rintis.marketing.beans.bean.blocked.BlockedAdministrationDetail;
import com.rintis.marketing.beans.bean.blocked.BlockedAdministrationHeader;
import com.rintis.marketing.beans.bean.blocked.PanHistoryBean;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.beans.bean.sequential.SequentialParamBean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
/**
 * Created by aabraham on 20/04/2021.
 */
public interface ISequentialService {
    public List<SequentialParamBean> listParam() throws Exception;
    public List<SequentialParamBean> getParam(String trx) throws Exception;
    public void updateParam(String trx, BigDecimal mintrxapp1d, BigDecimal minamtapp1d, BigDecimal mintrxtot1d, BigDecimal minamttot1d) throws Exception;
    public List<PanTrxidBean> listTrxId() throws Exception;
    public List<PanTrxidBean> listTrxIdBlocked() throws Exception;
    public List<SequentialAnomalyBean> listSequentialAnomaly(String periodFr, String periodTo, String trxtype, String bank_id) throws Exception;
    public boolean checkPanBankUsed(String pan, String bankid) throws Exception;
    public void saveBlockedPanHeader(String docId, Date createDate, String createBy, Date statusDate, String status, String notes) throws Exception;
    public void saveBlockedPanDetail(String docId, String pan, String bank_id, String blocked_to, String notes) throws Exception;
    public void updatePan(String pan, String status) throws Exception;
    public String getDocId(String date, String kode) throws Exception;
    public List<BlockedAdministrationHeader> listBlockedAdministrationHeader(String docId) throws Exception;
    public List<BlockedAdministrationHeader> listBlockedAdministrationHeaderAll() throws Exception;
    public List<BlockedAdministrationDetail> listBlockedAdministrationDetail(String docId) throws Exception;
    public void deleteBlockedAdministrationDetail(String docId, String pan) throws Exception;
    public void deleteBlockedAdministrationDetail(String docId) throws Exception;
    public void deleteBlockedAdministrationHeader(String docId) throws Exception;
    public void updatePanDetail(String docId, String status) throws Exception;
    public List<PanAnomalyEmailBean> getEmail() throws Exception;
    public void addPanBlockedHistory(String docId, String datetime, String user_id, String status) throws Exception;
    public List<BlockedAdministrationHeader> listDocBlocked(String status) throws Exception;
    public List<BlockedAdministrationHeader> listDocUnblocked(String status) throws Exception;
    public void insertPgw(String docId) throws Exception;
    public void insertB24(String docId) throws Exception;
    public void updateBlockedHeader(String docId, String datetime, String status) throws Exception;
    public void updateBlockedNotesHeader(String docId, String notes) throws Exception;
    public void addPanBlockedApproval(String docId, String datetime, String user_id, String status) throws Exception;
    public List<SequentialAnomalyBean> listUnblockedDetail() throws Exception;
    public void deletePgw(String docId) throws Exception;
    public List<PanHistoryBean> listHistoryPan() throws Exception;
    public void updatePanDetailManual(String docId, String status) throws Exception;
    public void deletePanDetailManual(String bankId, String pan) throws Exception;
    public void savePanDetailManual(String trxType, String bank_id, String pan, String status) throws Exception;
    public String getEmailDocId(String docId) throws Exception;
    public String getApprovedBy(String docId) throws Exception;
    public BigDecimal getCheckPan(String pan) throws Exception;
    public BigDecimal getCheckPanManual(String pan) throws Exception;
}

package com.rintis.marketing.core.app.main.module.mcc;

import com.rintis.marketing.beans.bean.bi.MCCAnomalyBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.dao.MCCDao;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.util.List;

@Service
@Qualifier(SpringBeanConstant.SPRING_BEAN_MCC_SERVICE)
@Named(SpringBeanConstant.SPRING_BEAN_MCC_SERVICE)

public class MCCService implements IMCCService {
    private Logger log = Logger.getLogger(this.getClass());

    @Autowired
    private MCCDao mccDao;

    @Override
    @Transactional(readOnly = true)
    public List<MCCAnomalyBean> listMCCAnomaly(String periodFr, String periodTo, String bank_id) throws Exception {
        return mccDao.listMCCAnomaly(periodFr, periodTo, bank_id);
    }
}

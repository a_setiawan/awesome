package com.rintis.marketing.core.constant;


public class DataConstant {
    //public static final String MASTER_BANK = "TEMPBANKNAME";
    public static final String MASTER_BANK = "tms_bank";

    //180731 Added By AAH
    public static final String MASTER_BANK_CA = "tms_ca";

    public static final String UTF8 = "UTF-8";
    public static final long TIME_235959_MILIS = ((24*3600) - 1)*1000;
    public static final String[] monthNumber = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final String[] MONTHS_FULL = {"January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"};
    public static final String[] DAYS = {"", "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"};

    public static String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final String OTHER = "Other";
    public static final String YES = "YES";
    public static final String NO = "NO";
    public static final Integer ACTIVE = 1;
    public static final Integer NON_ACTIVE = 0;
    public static final String STR_ACTIVE = "active";
    public static final String STR_NON_ACTIVE = "non_active";
    public static final String Y = "Y";
    public static final String N = "N";
    public static final String NA = "_NA_";
    public static final String NA2 = "NA";
    public static final String FALSE = "false";
    public static final String TRUE = "true";
    public static final String SORT_ASC = "asc";
    public static final String SORT_DESC = "desc";
    
    public static final String HTTP_GET = "GET";
    public static final String HTTP_POST = "POST";

    public static final Integer MENU_TYPE_SUB = 1;
    public static final Integer MENU_TYPE_ITEM = 2;
    public static final Integer MENU_ROLE_TYPE_NOT_HIDDEN = 0;

    public static final String MENU_ROLE_NAME_BOD = "BOD";
    public static final String MENU_ROLE_NAME_ADMIN = "ADMIN";

    public static final String SMTP = "smtp";
    public static final String SMTP_USER = "smtp_user";
    public static final String SMTP_PASSWORD = "smtp_password";
    public static final String TEMP_FOLDER = "temp_folder";
    public static final String TOP_N = "top_n";
    //public static final String TOP_N_ISSUER = "top_n_issuer";
    //public static final String TOP_N_ACQUIRER = "top_n_acquirer";
    //public static final String TOP_N_BILLER = "top_n_biller";
    public static final String TOP_N_ALL = "10000";

    public static final String SYSTEM_PROPERTY_SMTP = "smtp";
    public static final String SYSTEM_PROPERTY_SMTP_USER = "smtp_user";
    public static final String SYSTEM_PROPERTY_SMTP_PASSWORD = "smtp_password";
    public static final String SYSTEM_PROPERTY_SMTP_PORT = "smtp_port";
    public static final String SYSTEM_PROPERTY_SMTP_SSL = "smtp_ssl";
    public static final String SYSTEM_PROPERTY_DEFAULT_FROM_EMAIL = "default_from_email";
    public static final String SYSTEM_PROPERTY_LIMIT_YEAR = "limit_year";
    public static final String SYSTEM_PROPERTY_TOP_N = "top_n";
    public static final String SYSTEM_PROPERTY_SEND_EMAIL_INTERVAL = "send_email_interval";
    public static final String SYSTEM_PROPERTY_SCHEDULE_ALERT_ANOMALY_5MIN = "schedule_alert_anomaly";
    public static final String SYSTEM_PROPERTY_SCHEDULE_ALERT_ANOMALY_HOURLY = "schedule_alert_anomaly_hourly";
    public static final String SYSTEM_PROPERTY_SCHEDULE_DISTINCT_ACQ_ISS_BNF = "schedule_distinct_acq_iss_bnf";
    public static final String SYSTEM_PROPERTY_SCHEDULE_CHECK_ACQ_ISS_BNF_TRX = "schedule_check_acq_iss_bnf_trx";
    public static final String SYSTEM_PROPERTY_SCHEDULE_ETECT_ROUTING = "schedule_detect_routing";
    public static final String SYSTEM_PROPERTY_LDAP_DOMAIN = "ldap_domain";
    public static final String SYSTEM_PROPERTY_LDAP_URL = "ldap_url";
    public static final String SYSTEM_PROPERTY_LDAP_DN = "ldap_dn";
    public static final String SYSTEM_PROPERTY_ALERT_COMPARE_TYPE = "alert_compare_type";
    public static final String SYSTEM_PROPERTY_THRESHOLD_ANOMALY_TRANSACTION = "threshold_anomaly_transaction";
    public static final String SYSTEM_PROPERTY_ANOMALY_TRANSACTION_START_TIME = "anomaly_transaction_start_time";
    public static final String SYSTEM_PROPERTY_ANOMALY_TRANSACTION_END_TIME = "anomaly_transaction_end_time";
    public static final String SYSTEM_PROPERTY_DEFAULT_RANGE_5M = "default_range_5m";
    public static final String SYSTEM_PROPERTY_DEFAULT_RANGE_15M = "default_range_15m";
    public static final String SYSTEM_PROPERTY_DEFAULT_RANGE_30M = "default_range_30m";
    public static final String SYSTEM_PROPERTY_DEFAULT_RANGE_HOURLY = "default_range_hourly";
    public static final String SYSTEM_PROPERTY_DEFAULT_RANGE_1D = "default_range_1d";
    public static final String SYSTEM_PROPERTY_EMAIL_ALERT_TO = "email_alert_to";
    public static final String SYSTEM_PROPERTY_EMAIL_ALERT_SUBJECT = "email_alert_subject";
    public static final String SYSTEM_PROPERTY_EMAIL_ALERT_MESSAGE = "email_alert_message";
    public static final String SYSTEM_PROPERTY_EMAIL_ALERT_TO_M3M4 = "email_alert_to_m3m4";
    public static final String SYSTEM_PROPERTY_EMAIL_ALERT_SUBJECT_M3M4 = "email_alert_subject_m3m4";
    public static final String SYSTEM_PROPERTY_EMAIL_ALERT_MESSAGE_M3M4 = "email_alert_message_m3m4";
    public static final String SYSTEM_PROPERTY_M1_INTERVAL = "m1_interval";
    public static final String SYSTEM_PROPERTY_LIMIT_CHART_MINUTE = "limit_chart_minute";
    public static final String SYSTEM_PROPERTY_DELAY_M1_M2 = "delay_m1_m2";
	public static final String SYSTEM_PROPERTY_M1_DESCRIPTION = "m1.description";
	public static final String SYSTEM_PROPERTY_M2_DESCRIPTION = "m2.description";
	public static final String SYSTEM_PROPERTY_M3_DESCRIPTION = "m3.description";
	public static final String SYSTEM_PROPERTY_M4_DESCRIPTION = "m4.description";

    public static final String SYSTEM_PROPERTY_REFRESH_INTERVAL_TABLE_ALERT = "refresh.interval.tablealert";

    public static final String ATM = "ATM";
    public static final String POS = "POS";
    public static final String PAYMENT = "PMT";
    public static final String WITHDRAWAL = "WITH";
    public static final String BALANCE_INQUIRY = "BALINQ";
    public static final String TRANSFER = "TRANSF";
    public static final String DEBIT = "DEBIT";
    public static final String DECLINE = "DECL";
    public static final String APPROVE_POS = "APPRPOS";
    public static final String APPROVE_POS_VOID = "APPRPOSVOID";
    public static final String DECLINE_POS = "DECLPOS";
    public static final String DECLINE_POS_VOID = "DECLPOSVOID";
    public static final String BANK = "BANK";
    public static final String APPROVE_PAYMENT = "APPRPMT";
    public static final String DECLINE_PAYMENT = "DECLPMT";

    public static final String PERCENT_GROWTH_FEE = "% Growth Fee Total";
    public static final String PERCENT_GROWTH_FEE_ATM = "% Growth Fee ATM";
    public static final String PERCENT_GROWTH_FEE_POS = "% Growth Fee Debit";
    public static final String PERCENT_GROWTH_FEE_PAYMENT = "% Growth Fee Payment";


    /*public static final String COLOR_WITHDRAWAL = "color_withdrawal";
    public static final String COLOR_BALANCE_INQUIRY = "color_balance_inquiry";
    public static final String COLOR_TRANSFER = "color_transfer";
    public static final String COLOR_PAYMENT = "color_payment";
    public static final String COLOR_DEBIT = "color_debit";
    public static final String COLOR_DECLINE = "color_decline";
    public static final String COLOR_ATM = "color_atm";
    public static final String COLOR_APPROVE = "color_approve";
    public static final String COLOR_TOTAL = "color_total";
    public static final String COLOR_APPROVE_VOID = "color_approve_void";
    public static final String COLOR_DECLINE_VOID = "color_decline_void";
    public static final String COLOR_ACTUAL_PREV = "color_actual_prev";
    public static final String COLOR_TARGET = "color_target";
    public static final String COLOR_ACTUAL_CURR = "color_actual_curr";
    public static final String COLOR_WDW_LOCAL = "color_wdw_local";
    public static final String COLOR_INQ_LOCAL = "color_inq_local";
    public static final String COLOR_TRF_LOCAL = "color_trf_local";
    public static final String COLOR_TRF_INTER = "color_trf_inter";
    public static final String COLOR_PMT_LOCAL = "color_pmt_local";
    public static final String COLOR_POS_LOCAL = "color_pos_local";
    public static final String COLOR_TOP_LOCAL = "color_top_local";
    public static final String COLOR_DEC_ATM = "color_dec_atm";
    public static final String COLOR_DEC_POS = "color_dec_pos";
    public static final String COLOR_APPR_POS = "color_appr_pos";
    public static final String COLOR_CUP = "color_cup";
    public static final String COLOR_APN = "color_apn";
    public static final String COLOR_CHANNEL_ATM = "color_channel_atm";
    public static final String COLOR_CHANNEL_ITB = "color_channel_internetbanking";
    public static final String COLOR_CHANNEL_MOB = "color_channel_mobilebanking";
    public static final String COLOR_CHANNEL_RMT = "color_channel_remittance";
    public static final String COLOR_CHANNEL_SMS = "color_channel_smsbanking";
    public static final String COLOR_CHANNEL_PHN = "color_channel_phonebanking";
    public static final String COLOR_CHANNEL_EDC = "color_channel_edc";*/

    public static final String COLOR_TX_TOTAL = "color_tx_total";
    public static final String COLOR_TX_APPR = "color_tx_approve";
    public static final String COLOR_TX_DC = "color_tx_dc";
    public static final String COLOR_TX_DF = "color_tx_df";
    public static final String COLOR_TX_RVSL = "color_tx_rvsl";
    public static final String COLOR_TX_TOTAL_PREV = "color_tx_total_prev";
    public static final String COLOR_TX_APPR_PREV = "color_tx_approve_prev";
    public static final String COLOR_TX_DC_PREV = "color_tx_dc_prev";
    public static final String COLOR_TX_DF_PREV = "color_tx_df_prev";
    //public static final String COLOR_TX_ACQ_BNF = "color_tx_acq_bnf";
    //public static final String COLOR_TX_ISS_BNF = "color_tx_iss_bnf";


    public static final String[] OPTION_PERCENT_GROWTH_MONTH_NAME = {"Option 1", "Option 2"};
    public static final String[] OPTION_PERCENT_GROWTH_MONTH_VALUE = {"1", "2"};
    public static final String OPTION_PERCENT_GROWTH_SAME_MONTH_PREVIOUS_YEAR = "1";
    public static final String OPTION_PERCENT_GROWTH_PREVIOUS_MONTH = "2";

    public static final String[] OPTION_PERCENT_GROWTH_YEAR_NAME = {"Option 1", "Option 2"};
    public static final String[] OPTION_PERCENT_GROWTH_YEAR_VALUE = {"1", "2"};
    public static final String OPTION_PERCENT_GROWTH_DISETAHUKAN = "1";
    public static final String OPTION_PERCENT_GROWTH_SAMPAI_BULAN_YBS = "2";



    public static final String ACQUIRER = "ACQUIRER";
    public static final String ISSUER = "ISSUER";
    public static final String BILLER = "BILLER";

    public static final String ACQ = "ACQ";
    public static final String ISS = "ISS";
    public static final String BNF = "BNF";
    public static final String BIL = "BIL";

    public static final String ENUM_INPUT_TYPE_TARGET = "TARGET";
    public static final String ENUM_INPUT_TYPE_PROMOTION = "PROMOTION";
    public static final String ENUM_INPUT_TYPE_NEWCHANNEL = "NEWCHANNEL";
    public static final String ENUM_INPUT_TYPE_PAYMENT = "PAYMENT";
    public static final String ENUM_INPUT_TYPE_CROSSBORDER_APN = "CROSSBORDER_APN";
    public static final String ENUM_INPUT_TYPE_CROSSBORDER_UPAY = "CROSSBORDER_UPAY";

    public static final String ENUM_TRANS_TYPE_GROUP = "TRANS_TYPE_GROUP";
    public static final String ENUM_TRANS_TYPE_INPUT_TYPE = "TRANS_TYPE_INPUT_TYPE";

    public static final String ALL = "ALL";
    public static final String WDW_LOCAL = "WDW_LOCAL";
    public static final String INQ_LOCAL = "INQ_LOCAL";
    public static final String TRF_LOCAL = "TRF_LOCAL";
    public static final String TRF_INTER = "TRF_INTER";
    public static final String PMT_LOCAL = "PMT_LOCAL";
    public static final String POS_LOCAL = "POS_LOCAL";
    public static final String TOP_LOCAL = "TOP_LOCAL";
    public static final String DEC_ATM = "DEC_ATM";
    public static final String DEC_POS = "DEC_POS";
    public static final String INQ_CUP = "INQ_CUP";
    public static final String WDW_CUP = "WDW_CUP";
    public static final String INQ_APN = "INQ_APN";
    public static final String WDW_APN = "WDW_APN";
    public static final String CUP = "CUP";
    public static final String APN = "APN";
    public static final String TRF_IBANKING = "TRF_IBANKING";
    public static final String TRF_MBANKING = "TRF_MBANKING";

    public static final String GLOBAL_WDW_APP = "GLOBAL_WDW_APP";
    public static final String GLOBAL_INQ_APP = "GLOBAL_INQ_APP";
    public static final String GLOBAL_APN_APP = "GLOBAL_APN_APP";
    public static final String GLOBAL_TRF_INTER_APP = "GLOBAL_TRF_INTER_APP";
    public static final String GLOBAL_CUP_APP = "GLOBAL_CUP_APP";
    public static final String GLOBAL_POS_APP = "GLOBAL_POS_APP";
    public static final String GLOBAL_PMT_APP = "GLOBAL_PMT_APP";
    public static final String GLOBAL_TOP_APP = "GLOBAL_TOP_APP";
    public static final String GLOBAL_ATM_DEC = "GLOBAL_ATM_DEC";
    public static final String GLOBAL_POS_DEC = "GLOBAL_POS_DEC";
    public static final String GLOBAL_TRF_APP = "GLOBAL_TRF_APP";

    public static final String TARGET_PMT_APP_LOCAL = "TARGET_PMT_APP_LOCAL";
    public static final String TARGET = "TARGET";

    public static final String GENERALSUMMARY_CASHWITHDRAWAL = "Cash Withdrawal";
    public static final String GENERALSUMMARY_BALANCEINQUIRY = "Balance Inquiry";
    public static final String GENERALSUMMARY_TRANSFER = "Transfer";
    public static final String GENERALSUMMARY_DECLINE = "Decline";
    public static final String GENERALSUMMARY_INTERKONEKSI = "Interkoneksi";
    public static final String GENERALSUMMARY_CUP = "CUP";
    public static final String GENERALSUMMARY_APN = "APN";
    public static final String GENERALSUMMARY_PRIMADEBIT = "Prima Debit";
    public static final String GENERALSUMMARY_DECLINEPOS = "Decline (POS)";
    public static final String GENERALSUMMARY_PAYMENT = "Payment";
    public static final String GENERALSUMMARY_TOPUP = "Top Up E-Money";

    public static final String CHANNEL_ATM = "ATM";
    public static final String CHANNEL_INTERNET_BANKING = "ITB";
    public static final String CHANNEL_MOBILE_BANKING = "MOB";
    public static final String CHANNEL_SMS_BANKING = "SMS";
    public static final String CHANNEL_PHONE_BANKING = "PHN";
    public static final String CHANNEL_REMITTANCE = "RMT";
    public static final String CHANNEL_EDC = "EDC";

    public static final String CHANNEL__CASH_WITHDRAWAL = "cashWithdrawal";
    public static final String CHANNEL__BALANCEINQUIRY = "balanceInquiry";
    public static final String CHANNEL__TRANSFER = "transfer";
    public static final String CHANNEL__PAYMENT = "payment";
    public static final String CHANNEL__TOPUP = "topup";
    public static final String CHANNEL__DECLINE = "decline";

    public static final String CHANNEL__DC_FREQ = "dcFreq";
    public static final String CHANNEL__APP_FREQ = "appFreq";

    public static final int MAX_TOPN = 99999999;

    public static final int ROLE_ID_ADMIN = 99;
    public static final int ROLE_ID_DASHBOARD_CREATOR = 98;


    /*public static final String[] ACTIVE_TRANSACTIONID_ALL = {DataConstant.WDW_LOCAL, DataConstant.INQ_LOCAL, DataConstant.TRF_LOCAL,
            DataConstant.DEC_ATM, DataConstant.TRF_INTER, DataConstant.INQ_CUP, DataConstant.WDW_CUP,
            DataConstant.INQ_APN, DataConstant.WDW_APN, DataConstant.POS_LOCAL, DataConstant.DEC_POS,
            DataConstant.PMT_LOCAL, DataConstant.TOP_LOCAL};*/
    public static final String[] ACTIVE_TRANSACTIONID_ALL = {DataConstant.GLOBAL_WDW_APP, DataConstant.GLOBAL_INQ_APP,
            DataConstant.GLOBAL_APN_APP,
            DataConstant.GLOBAL_TRF_INTER_APP, DataConstant.GLOBAL_CUP_APP, DataConstant.GLOBAL_POS_APP, DataConstant.GLOBAL_PMT_APP,
            DataConstant.GLOBAL_TOP_APP, DataConstant.GLOBAL_ATM_DEC, DataConstant.GLOBAL_POS_DEC, DataConstant.GLOBAL_TRF_APP};

    /*public static final String TABLE_SWT_REPORT_R2 = "swt_report_r2";
    public static final String TABLE_SWT_REPORT_X2 = "swt_report_x2";
    public static final String TABLE_SWT_REPORT_X2_LOCAL = "swt_report_x2_local";
    public static final String TABLE_SWT_REPORT_X2_INTERCON = "swt_report_x2_intercon";
    public static final String TABLE_SWT_REPORT_P2 = "swt_report_p2";
    public static final String TABLE_SWT_REPORT_U2 = "swt_report_u2";
	public static final String TABLE_SWT_REPORT_D2 = "swt_report_d2";
	public static final String TABLE_SWT_REPORT_C2 = "swt_report_c2";
    public static final String TABLE_SWT_REPORT_C2_CUP = "v_swt_report_c2_cup";
    public static final String TABLE_SWT_REPORT_C2_APN = "v_swt_report_c2_apn";
    public static final String TABLE_CLEARING_REKAP = "clearing_rekap";*/

    //insert into mkt.mrkt_system_property values (55, 'v_swt_report_c2_cup', 'v_swt_report_c2_cup@link_eft', null, null, null, null, 0);
    //insert into mkt.mrkt_system_property values (56, 'v_swt_report_c2_apn', 'v_swt_report_c2_apn@link_eft', null, null, null, null, 0);
    //update mkt.mkt_mstr_trans_general set pva_actual_link = 'dailyTransactionApn.do' where general_id = 'GLOBAL_APN_APP';
    //insert into mkt.mrkt_system_property values (57, 'swt_report_x2_local', 'v_EIS_X1DailyLocalPrima@link_eft', null, null, null, null, 0);
    //insert into mkt.mrkt_system_property values (58, 'swt_report_x2_intercon', 'v_EIS_X1DailyInterCon@link_eft', null, null, null, null, 0);



    public static final String TRX_INDICATOR__ACQ_ONLY = "01";
    public static final String TRX_INDICATOR__ISS_ONLY = "02";
    public static final String TRX_INDICATOR__BNF_ONLY = "03";
    public static final String TRX_INDICATOR__ACQ_BNF = "04";
    public static final String TRX_INDICATOR__ACQ_ISS = "05";
    public static final String TRX_INDICATOR__ISS_BNF = "06";

    public static final int ALERT_ANOMALY_STATUS__NOTSET = 0;
    public static final int ALERT_ANOMALY_STATUS__NORMAL = 1;
    public static final int ALERT_ANOMALY_STATUS__ABNORMAL = 2;

    public static final String ALERT_ANOMALY_TYPE__PLUS = "PLUS";
    public static final String ALERT_ANOMALY_TYPE__MINUS = "MIN";

    public static final String ALERT_METHOD_M1 = "M1";
    public static final String ALERT_METHOD_M2 = "M2";
    public static final String ALERT_METHOD_M3 = "M3";
    public static final String ALERT_METHOD_M4 = "M4";

    public static final String USER_VALIDATION_TYPE_LDAP = "LDAP";
    public static final String USER_VALIDATION_TYPE_INTERNAL = "INTERNAL";

    public static final String MONITORING_INTERVAL_5MIN = "5MIN";
    public static final String MONITORING_INTERVAL_HOURLY = "HOURLY";

    public static final String COMPARE_FROM00 = "FROM00";
    public static final String COMPARE_FROMHOUR = "FROM_HOUR";

    public static final String DEFAULT_GRAPH_REFRESH_INTERVAL = "80";
    public static final int LIMIT_TOTAL_ROW_MESSAGE = 400;

    public static final String GROWTH = "GROWTH";
    public static final String ACHIEVEMENT = "ACHIEVE";

    public static final String BANK_DKI_KJP = "DP";
    public static final String BANK_DKI = "DK";

    public static final String TLOGID_TARGET_BANK = "TARGET_BANK";
    public static final String TLOGID_TARGET_BANK_GLOBAL = "TARGET_BANK_GLOBAL";
    public static final String TLOGID_TARGET_BILLER = "TARGET_BILLER";
    public static final String TLOGID_AM_BILLER = "AM_BILLER";
    public static final String TLOGID_AM_BANK = "AM_BANK";

    public static final String STRING_TOTAL_TRANSACTION = "Total Transaction";
    public static final String STRING_TOTAL_APPROVE = "Total Approve";
    public static final String STRING_TOTAL_DECLINE_CHARGE = "Total Decline Charge";
    public static final String STRING_TOTAL_DECLINE_FREE = "Total Decline Free";

    public static final String OPERATOR_PLUS = "+";
    public static final String OPERATOR_MINUS = "-";

    public static final String STR_ALL_TRANSACTION = "All Transaction";


    public static final String TABLE_TRX5MIN = "tms_trx5min";
    public static final String TABLE_TRX15MIN = "tms_trx15min";
    public static final String TABLE_TRX30MIN = "tms_trx30min";
    public static final String TABLE_TRX1HOUR = "tms_trx60min";
    public static final String TABLE_TRX1DAY = "tms_trx1day";


    public static final String T5M = "5M";
    public static final String T15M = "15M";
    public static final String T30M = "30M";
    public static final String T1H = "1H";
    public static final String T1D = "1D";
    public static final String[] TIMEFRAME_ARRAY = {"5M", "15M", "30M", "1H", "1D"};

    public static final String PARAM_TIME_FROM = "timeFr";
    public static final String PARAM_TIME_TO = "timeTo";
    public static final String PARAM_PERIOD_FROM = "periodFr";
    public static final String PARAM_PERIOD_TO = "periodTo";

    public static final int USER_NONBANK = 0;
    public static final int USER_BANK = 1;

    public static final int USER_DASHBOARD_MAX_CHART = 10;

    public static String getTimeFrameName(String id) {
        String result = "";
        if (id.equalsIgnoreCase("5M")) {
            result = "5 Minutes";
        } else if (id.equalsIgnoreCase("15M")) {
            result = "15 Minutes";
        } else if (id.equalsIgnoreCase("30M")) {
            result = "30 Minutes";
        } else if (id.equalsIgnoreCase("1H")) {
            result = "Hourly";
        } else if (id.equalsIgnoreCase("1D")) {
            result = "Daily";
        }
        return result;
    }


    public static final String TYPE_TOTAL = "TT";
    public static final String TYPE_APPR = "AP";
    public static final String TYPE_DC = "DC";
    public static final String TYPE_DF = "DF";

    public static String getColTypeName(String type) {
        String typeName = "";
        if (type.equalsIgnoreCase(DataConstant.TYPE_TOTAL)) {
            typeName = "Total";
        } else if (type.equalsIgnoreCase(DataConstant.TYPE_APPR)) {
            typeName = "Approve";
        } else if (type.equalsIgnoreCase(DataConstant.TYPE_DC)) {
            typeName = "Decline Charge";
        } else if (type.equalsIgnoreCase(DataConstant.TYPE_DF)) {
            typeName = "Decline Free";
        }
        return typeName;
    }

}

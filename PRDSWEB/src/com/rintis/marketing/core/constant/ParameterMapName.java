package com.rintis.marketing.core.constant;


public class ParameterMapName {
    public static String PARAM_BOD = "bod";

    public static String PARAM_STARTDATE = "startDate";
    public static String PARAM_ENDDATE = "endDate";
    public static String PARAM_USERID = "userId";
    public static String PARAM_STATUSID = "statusId";
    public static String PARAM_DESCRIPTION = "description";

    public static String PARAM_BANK_ACTIVE_STATUS = "bankActiveStatus";
    public static String PARAM_BANK_ISS_ATM_STATUS = "bankIssAtmStatus";
    public static String PARAM_BANK_ACQ_ATM_STATUS = "bankAcqAtmStatus";
    public static String PARAM_BANK_BNF_STATUS = "bankBnfStatus";
    public static String PARAM_BANK_IS_LOCAL_SWITCHER = "bankIsLocalSwitcher";
    public static String PARAM_BANK_ACQ_POS_STATUS = "bankAcqPosStatus";
    public static String PARAM_BANK_ISS_POS_STATUS = "bankIssPosStatus";
    public static String PARAM_BANK_ISS_PMT_STATUS = "bankIssPmtStatus";
    public static String PARAM_BANK_ACQ_PMT_STATUS = "bankAcqPmtStatus";
    public static String PARAM_BANK_OR_STATUS_YES = "bankOrStatus";
    public static String PARAM_BANK_PAYMENT = "bankPayment";
    public static String PARAM_BANK_CNET = "bankCnet";

    public static String PARAM_GENERALSUMMARYID = "generalSummaryId";
    public static String PARAM_ACCOUNTID = "accountId";
    public static String PARAM_BANKID = "bankId";
    public static String PARAM_BANKID_MULTI = "bankIdMulti";
    public static String PARAM_BILLERID = "billerId";
    public static String PARAM_BILLERID_MULTI = "billerIdMulti";
    public static String PARAM_GROUPID = "groupId";
    public static String PARAM_TRANSACTIONID = "transactionId";
    public static String PARAM_TARGETID = "targetId";
    public static String PARAM_TRANSACTIONIDARR = "transactionIdArr";
    public static String PARAM_SUBTRANSACTIONID = "subTransactionId";
    public static String PARAM_YEAR = "year";
    public static String PARAM_TIPE = "tipe";
    public static String PARAM_TARGET_TRANSACTION_TYPE_ID = "targetTransactioTypeId";
    public static String PARAM_PERIOD = "period";
    public static String PARAM_PERIOD_FROM = "periodFrom";
    public static String PARAM_PERIOD_TO = "periodTo";
    public static String PARAM_SOLVE_STATUS = "solveStatus";
}

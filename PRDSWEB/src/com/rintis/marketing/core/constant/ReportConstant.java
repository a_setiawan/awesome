package com.rintis.marketing.core.constant;


public class ReportConstant {

    public static String TYPE_BAR_STACK = "barstack";
    public static String TYPE_BAR = "bar";

    public static String MENU_TOTAL_FREQUENCY_YEARLY = "020201";
    public static String MENU_PERCENT_GROWTH_FREQUENCY_YEARLY = "020202";
    public static String MENU_TOTAL_FREQUENCY_POS_YEARLY = "020203";
    public static String MENU_TOTAL_FREQUENCY_WBTD_YEARLY = "020204";
    public static String MENU_AVG_FREQUENCY_ATM_POS_PAYMENT_YEARLY = "020205";

    public static String MENU_TOTAL_AMOUNT_YEARLY = "020301";
    public static String MENU_TOTAL_AMOUNT_MONTHLY = "020302";

    public static String MENU_TOTAL_FEE_YEARLY = "020401";
    public static String MENU_TOTAL_PERCENT_GROWTH_FEE_YEARLY = "020402";
    public static String MENU_TOTAL_FEE_ATM_WBTD_YEARLY = "020403";
    public static String MENU_TOTAL_FEE_POS_YEARLY = "020404";
    public static String MENU_AVG_FEE_ATM_POS_PAYMENT_YEARLY = "020405";

    public static String MENU_TOP_5_ACQUIRER_YEARLY = "020501";

    public static String MENU_TOP_5_ISSUER_YEARLY = "020601";

    public static String MENU_DECLINE_PERFORMANCE_YEAR = "020701";

}

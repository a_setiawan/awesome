package com.rintis.marketing.core.utils;

import java.util.ResourceBundle;

public class MessagesResourceBundle {
    private static ResourceBundle res;
    private static ResourceBundle setting;
    
    public static String getMessage(String key) {
        if (res == null) {
            res = ResourceBundle.getBundle("messages");
        }
        return res.getString(key);
    }
    
    public static String getSetting(String key) {
        if (setting == null) {
            setting = ResourceBundle.getBundle("setting");
        }
        return setting.getString(key);
    }
    
}

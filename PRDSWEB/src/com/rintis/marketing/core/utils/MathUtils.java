package com.rintis.marketing.core.utils;


import java.math.BigDecimal;
import java.text.DecimalFormat;

public class MathUtils {

    public static BigDecimal string2BigDecimal(String s) {
        BigDecimal result;
        try {
            result = new BigDecimal(s);
        } catch (Exception e) {
            return null;
        }
        return result;
    }

    public static String formatIfBigDecimal(String s) {
        BigDecimal bd = string2BigDecimal(s);
        if (bd == null) {
            return s;
        }
        DecimalFormat formatter = new DecimalFormat("#,###.00");
        return formatter.format(bd);
    }


    public static Integer bigDecimal2Integer(BigDecimal bd) {
        if (bd != null) {
            return bd.intValue();
        } else {
            return null;
        }
    }

    public static Long bigDecimal2Long(BigDecimal bd) {
        if (bd != null) {
            return bd.longValue();
        } else {
            return null;
        }
    }

    public static BigDecimal checkNull(BigDecimal b) {
        if (b == null) return BigDecimal.ZERO;
        return b;
    }

    public static Integer checkNull(Integer b) {
        if (b == null) return 0;
        return b;
    }

    public static BigDecimal calcPercentGrowth(BigDecimal b1, BigDecimal b2) {
        if (b2.compareTo(BigDecimal.ZERO) == 0) return BigDecimal.ZERO;
        BigDecimal pct = b1.subtract(b2);
        pct = pct.divide(b2, 4, BigDecimal.ROUND_HALF_UP);
        return pct;
    }

    public static BigDecimal calcPercentComparison(BigDecimal b11, BigDecimal b22) {
        BigDecimal b1 = checkNull(b11);
        BigDecimal b2 = checkNull(b22);
        if (b2.compareTo(BigDecimal.ZERO) == 0) return BigDecimal.ZERO;
        BigDecimal pct = b1.divide(b2, 6, BigDecimal.ROUND_HALF_UP);
        return pct;
    }

    public static boolean validatePercentValue(BigDecimal b) {
        boolean result = true;
        if (b.compareTo(BigDecimal.ZERO) < 0 || b.compareTo(new BigDecimal("100")) > 0 ) {
            return false;
        }
        return result;
    }

    public static BigDecimal zeroAsNull(BigDecimal b) {
        if (b == null) return null;
        if (b.compareTo(BigDecimal.ZERO) == 0) {
            return null;
        } else {
            return b;
        }
    }

    public boolean checkNumber(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}

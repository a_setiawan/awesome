package com.rintis.marketing.core.utils;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.util.Hashtable;

public class LdapUtils {

    public static boolean authenticateLdapBool(String ldapHost, String domain,
                                               String user, String pass) {
        //String returnedAtts[] = {"sn", "givenName", "mail"};
        //String searchFilter = "(&(objectClass=user)(sAMAccountName=" + user + "))";

        //Create the search controls
        //SearchControls searchCtls = new SearchControls();
        //searchCtls.setReturningAttributes(returnedAtts);

        //Specify the search scope
        //searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapHost);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, user + "@" + domain);
        env.put(Context.SECURITY_CREDENTIALS, pass);

        LdapContext ctxGC = null;

        try {
            ctxGC = new InitialLdapContext(env, null);
            //System.out.println("ctxGC = " + ctxGC);
        } catch (NamingException ex) {
            //ex.printStackTrace();
        }

        if (ctxGC == null) {
            return false;
        } else {
            return true;
        }
    }

}

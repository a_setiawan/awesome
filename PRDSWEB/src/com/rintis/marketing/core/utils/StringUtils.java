package com.rintis.marketing.core.utils;

import com.rintis.marketing.core.constant.DataConstant;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    public static boolean isEmpty(String input) {
        if (input == null) return true;
        if (input.length() == 0) return true;
        return false;
    }
    
    public static String checkNull(String s) {
        if (s == null) {
            return "";
        } else {
            return s;
        }
    }

    public static String checkInteger(String s) {
        if (s == null) {
            return "";
        } else {
            try {
                Integer.parseInt(s);
                return s;
            } catch (Exception e) {
                return "";
            }
        }
    }

    public static String upperCaseNull(String s) {
        if (s == null) {
            return null;
        } else {
            return s.toUpperCase();
        }
    }

    public static String formatFixLength(String text, String defaultPrefix, int digit) {
        String result = text;
        int len = text.length();
        if (len > digit) {
            result = text.substring(len-digit, len);
        } else if (len < digit) {
            int diff = digit - len;
            for(int i=0; i<diff; i++) {
                result = defaultPrefix + result;
            }
        }
        return result;
    }
    

    public static String centerText(String text, int maxWidth) {
        StringBuffer result = new StringBuffer("");
        int num = (maxWidth - text.length()) / 2;
        for(int i=0; i<num; i++) {
            result.append(" ");
        }
        result.append(text);
        for(int i=0; i<num; i++) {
            result.append(" ");
        }
        return result.toString();
    }
    
    public static String rightText(String text, int maxWidth) {
        StringBuffer result = new StringBuffer("");
        int num = maxWidth - text.length();
        for(int i=0; i<num; i++) {
            result.append(" ");
        }
        result.append(text);
        return result.toString();
    }

    public static boolean validateEmail(String s) {
        Pattern pattern = Pattern.compile(DataConstant.EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }
    
    public static int bool2int(Boolean bool) {
        if (bool == null) return 0;
        if (bool) {
            return 1;
        } else {
            return 0;
        }
    }

    public static boolean int2bool(Integer it) {
        if (it == null) return false;
        if (it > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static BigDecimal bool2bigdecimal(Boolean bool) {
        if (bool == null) return BigDecimal.ZERO;
        if (bool) {
            return BigDecimal.ONE;
        } else {
            return BigDecimal.ZERO;
        }
    }

    public static boolean bigdecimal2bool(BigDecimal bd) {
        if (bd == null) return false;
        if (bd.compareTo(BigDecimal.ZERO) > 0) {
            return true;
        } else {
            return false;
        }
    }


}

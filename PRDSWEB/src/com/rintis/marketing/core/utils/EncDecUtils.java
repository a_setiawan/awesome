package com.rintis.marketing.core.utils;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;


public class EncDecUtils {
    private static final Charset UTF8 = Charset.forName("UTF-8");
    
    public static String getCryptedBytes(byte[] bytes, String salt) throws Exception {
        String result = "";
        MessageDigest messagedigest = null;
        /*if (salt == null) {
            salt = RandomStringUtils.random(new Random().nextInt(15) + 1, CRYPT_CHAR_SET);
        }*/
        try {
            messagedigest = MessageDigest.getInstance("SHA");
            messagedigest.update(salt.getBytes(UTF8));
            messagedigest.update(bytes);
        } catch(Exception e) {
            throw new Exception(e.getMessage());
        }
        return Base64.encodeBase64URLSafeString(messagedigest.digest()).replace('+', '.');
    }
    
    public static String encrypt(String text, String key) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        final SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        return Base64.encodeBase64String(cipher.doFinal(text.getBytes()));
    }

    public static String decrypt(String text, String key) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        final SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        return new String(cipher.doFinal(Base64.decodeBase64(text)));
    }
    
    private byte[] rsaEncrypt(byte[] data, String a, String b) throws Exception {
        // PublicKey pubKey = readKeyFromFile("h:/temp/public.key");
        //bean = m, entity = e
        BigInteger m = new BigInteger(a);
        BigInteger e = new BigInteger(b);
        RSAPublicKeySpec keySpec = new RSAPublicKeySpec(m, e);
        KeyFactory fact = KeyFactory.getInstance("RSA");
        PublicKey pubKey = fact.generatePublic(keySpec);

        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        byte[] cipherData = cipher.doFinal(data);
        return cipherData;
    }

    private byte[] rsaDecrypt(byte[] data, String a, String b) throws Exception {
        // PublicKey pubKey = readKeyFromFile("h:/temp/private.key");
        //bean = m, entity = e
        BigInteger m = new BigInteger(a);
        BigInteger e = new BigInteger(b);
        RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(m, e);
        KeyFactory fact = KeyFactory.getInstance("RSA");
        PrivateKey priKey = fact.generatePrivate(keySpec);

        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, priKey);
        byte[] cipherData = cipher.doFinal(data);
        return cipherData;
    }
    

    public String urlEncrypt(String input) {
        return input;
    }

    public String urlDecrypt(String input) {
        return input;
    }

    public static String encodeBase36(long l)
    {
        StringBuilder ret = new StringBuilder();

        while (l > 0)
        {
            if ((l % 36) < 10) ret.append((char)(((int)'0') + (int)(l % 36)));
            else ret.append((char)(((int)'A') + (int)((l % 36) - 10)));
            l /= 36;
        }

        return ret.toString();
    }

    public static int decodeBase36(String s)
    {
        try
        {
            return Integer.parseInt(s, 36);
        }
        catch (NumberFormatException e)
        {
            return -1;
        }
    }
}

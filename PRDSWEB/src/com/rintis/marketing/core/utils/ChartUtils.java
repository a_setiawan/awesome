package com.rintis.marketing.core.utils;


import org.apache.poi.ss.usermodel.charts.ChartSerie;

import java.math.BigDecimal;

public class ChartUtils {

    public static BigDecimal setNullIfZero(BigDecimal value) {
        if (value.compareTo(BigDecimal.ZERO) == 0) {
            return null;
        } else {
            return value;
        }
    }

}

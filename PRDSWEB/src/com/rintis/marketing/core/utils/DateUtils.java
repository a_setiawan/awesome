package com.rintis.marketing.core.utils;


import com.rintis.marketing.core.constant.DataConstant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class DateUtils {
    public static final String DATE_YYYYMMDD_HHMMSS = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_YYYYMMDD_HHMMSS_NOSEPARATOR = "yyyyMMddHHmmss";
    public static final String DATE_YYYYMMDD_NOSEPARATOR = "yyyyMMdd";
    public static final String DATE_DDMMYYYY = "dd/MM/yyyy";
    public static final String DATE_DDMMYYYY_HHMMSS = "dd/MM/yyyy HH:mm:ss";
    public static final String DATE_MMYYYY = "MM/yyyy";
    public static final String DATE_YYYYMMDD = "yyyy/MM/dd";
    public static final String DATE_YYYYMMDD2 = "yyyy-MM-dd";
    public static final String DATE_YYYYMMDD2_HHMM = "yyyy-MM-dd HH:mm";
    public static final String DATE_DD_MMMM_YYYY = "dd MMMM yyyy";
    public static final String DATE_YYMMDD = "yyMMdd";
    public static final String DATE_YYMMDD_HHMM = "yyMMdd HH:mm";
    public static final String DATE_HHMM = "HH:mm";
    public static final String DATE_DDMM = "dd/MM";
    public static final String DATE_DDMMM = "dd-MMM";
    public static final String DATE_YYYYMMMDD2_HHMM = "yyyy-MMM-dd HH:mm";

    public static List<Integer> listYear(Integer endLimit) {
        int startYr = 2018;
        int endYear = getCurrentYear() + endLimit;
        List<Integer> list = new ArrayList<>();
        for(int i = startYr; i <= endYear; i++) {
            list.add(i);
        }
        return list;
    }

    public static int getCurrentYear() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR);
    }

    public static int getCurrentMonth() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.MONTH) + 1;
    }

    public static Date getEndOfDay235959(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    public static Date getStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date convertDate(String input, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date parsedDate = null;
        try {
            parsedDate = simpleDateFormat.parse(input);
        } catch (ParseException e) {
            //log.error(e.getMessage(), e);
        }
        return parsedDate;
    }

    public static String convertDate(Date input, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("in", "ID"));
        String parsedDate = null;
        try {
            parsedDate = simpleDateFormat.format(input);
        } catch (Exception e) {
            //log.error(e.getMessage(), e);
        }
        return parsedDate;
    }

    public static int getMaxDayInMonth(int year, int month) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month-1);
        return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static Date getPrevMonth(int year, int currMonth) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, currMonth-1);
        cal.add(Calendar.MONTH, -1);
        return cal.getTime();
    }

    public static int getYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    public static int getMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH) + 1;
    }

    public static int getDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static int getDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    public static int getMinute(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MINUTE);
    }

    public static int getHour(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.HOUR_OF_DAY);
    }

    public static int getMinuteRound5Min(Date date) {
        int minute = getMinute(date);
        return minute - (minute % 5);
    }

    public static Date getDateRound5Min(Date date) {
        int minute = getMinuteRound5Min(date);
        Date d = setDate(Calendar.MINUTE, date, minute);
        d = setDate(Calendar.SECOND, d, 0);
        return d;
    }

    public static String includeMonth(int yr) {
        String s = "";
        int nowYr = getCurrentYear();
        if (yr == nowYr) {
            int nowMonth = getCurrentMonth();
            for(int i=1; i < nowMonth; i++) {
                if (i == 1) {
                    s = "'" + StringUtils.formatFixLength(Integer.toString(i), "0", 2) + "'";
                } else {
                    s = s + ",'" + StringUtils.formatFixLength(Integer.toString(i), "0", 2) + "'";
                }
            }
        } else {
            s = "'01','02','03','04','05','06','07','08','09','10','11','12'";
        }
        return s;
    }

    public static int lastMonth(int yr) {
        int s = 12;
        int nowYr = getCurrentYear();
        if (yr == nowYr) {
            int nowMonth = getCurrentMonth();
            s = nowMonth - 1;
        }
        return s;
    }

    public static Date addDate(int type, Date date, int x) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(type, x);
        return calendar.getTime();
    }

    public static Date setDate(int type, Date date, int x) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(type, x);
        return calendar.getTime();
    }


    public static boolean checkMonthComplete(int year, int month) {
        boolean result = true;
        int cy = DateUtils.getCurrentYear();
        int cm = DateUtils.getCurrentMonth();
        if (year >= cy && month >= cm) {
            return false;
        }
        return result;
    }

    public static Date setHour(Date date, int hour) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        return cal.getTime();
    }

    public static Date setMinute(Date date, int minute) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.MINUTE, minute);
        return cal.getTime();
    }

    public static Date setSecond(Date date, int second) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.SECOND, second);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date setMonth(Date date, int month) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.MONTH, month-1);
        return cal.getTime();
    }

    public static Date setYear(Date date, int year) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.YEAR, year);
        return cal.getTime();
    }

    public static Date setDay(Date date, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, day);
        return cal.getTime();
    }

    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public static int getDatePart(Date date, int partType) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int result = -1;
        result = cal.get(partType);
        if (partType == Calendar.MONTH) {
            result++;
        }

        return result;
    }

    public static Date addDayDate(Date date, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, day);
        return calendar.getTime();
    }

    public static Date addMinuteDate(Date date, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minute);
        return calendar.getTime();
    }

    public static Map<String, String> DateToTimeFrTimeTo5m(Date now) {
        Map<String, String> result = new HashMap<>();
        int minute = getDatePart(now, Calendar.MINUTE);
        int hour = getDatePart(now, Calendar.HOUR_OF_DAY);

        String shour = "";
        String smin = "";
        String timefr = "";
        String timeto = "";
        String periodfr = "";
        String periodto = "";

        int min2 = minute / 5;
        //System.out.println("5m " + min2 + " " + (min2 * 5));
        shour = Integer.toString(hour);
        if (shour.length() == 1) shour = "0" + shour;
        smin = Integer.toString(min2 * 5);
        if (smin.length() == 1) smin = "0" + smin;
        timeto = shour + ":" + smin;

        periodfr = DateUtils.convertDate(now, DateUtils.DATE_YYMMDD);
        periodto = DateUtils.convertDate(now, DateUtils.DATE_YYMMDD);

        if ((min2 * 5) - 5 < 0) {
            if ((hour - 1) < 0) {
                shour = "23";
                timeto = "24:00";
                Date dprev = DateUtils.addDayDate(now, -1);
                periodfr = DateUtils.convertDate(dprev, DateUtils.DATE_YYMMDD);
            } else {
                shour = Integer.toString(hour - 1);
                if (shour.length() == 1) shour = "0" + shour;
            }
            timefr = shour + ":55";
        } else {
            smin = Integer.toString((min2 * 5) - 5);
            if (smin.length() == 1) smin = "0" + smin;
            timefr = shour + ":" + smin;
        }

        result.put(DataConstant.PARAM_TIME_FROM, timefr);
        result.put(DataConstant.PARAM_TIME_TO , timeto);
        result.put(DataConstant.PARAM_PERIOD_FROM, periodfr);
        result.put(DataConstant.PARAM_PERIOD_TO , periodto);

        return result;
    }

    public static Map<String, String> DateToTimeFrTimeTo15m(Date now) {
        Map<String, String> result = new HashMap<>();
        int minute = getDatePart(now, Calendar.MINUTE);
        int hour = getDatePart(now, Calendar.HOUR_OF_DAY);

        String shour = "";
        String smin = "";
        String timefr = "";
        String timeto = "";
        String periodfr = "";
        String periodto = "";

        int min3 = minute / 15;
        //System.out.println("15m " + min3 + " " + (min3 * 15));
        shour = Integer.toString(hour);
        if (shour.length() == 1) shour = "0" + shour;
        smin = Integer.toString(min3 * 15);
        if (smin.length() == 1) smin = "0" + smin;
        timeto = shour + ":" + smin;

        periodfr = DateUtils.convertDate(now, DateUtils.DATE_YYMMDD);
        periodto = DateUtils.convertDate(now, DateUtils.DATE_YYMMDD);

        if ((min3 * 5) - 5 < 0) {
            if ((hour - 1) < 0) {
                shour = "23";
                timeto = "24:00";
                Date dprev = DateUtils.addDayDate(now, -1);
                periodfr = DateUtils.convertDate(dprev, DateUtils.DATE_YYMMDD);
            } else {
                shour = Integer.toString(hour - 1);
                if (shour.length() == 1) shour = "0" + shour;
            }
            timefr = shour + ":45";
        } else {
            smin = Integer.toString((min3 * 15) - 15);
            if (smin.length() == 1) smin = "0" + smin;
            timefr = shour + ":" + smin;
        }
        //System.out.println(timefr + " " + timeto);
        result.put(DataConstant.PARAM_TIME_FROM, timefr);
        result.put(DataConstant.PARAM_TIME_TO , timeto);
        result.put(DataConstant.PARAM_PERIOD_FROM, periodfr);
        result.put(DataConstant.PARAM_PERIOD_TO , periodto);

        return result;
    }

    public static Map<String, String> DateToTimeFrTimeTo30m(Date now) {
        Map<String, String> result = new HashMap<>();
        int minute = getDatePart(now, Calendar.MINUTE);
        int hour = getDatePart(now, Calendar.HOUR_OF_DAY);

        String shour = "";
        String shourfr = "00";
        String smin = "";
        String timefr = "";
        String timeto = "";
        String periodfr = "";
        String periodto = "";

        int min4 = minute / 30;
        //System.out.println("30m " + min4 + " " + (min4 * 30));
        shour = Integer.toString(hour);
        if (shour.length() == 1) shour = "0" + shour;
        smin = Integer.toString(min4 * 30);
        if (smin.length() == 1) smin = "0" + smin;
        timeto = shour + ":" + smin;

        periodfr = DateUtils.convertDate(now, DateUtils.DATE_YYMMDD);
        periodto = DateUtils.convertDate(now, DateUtils.DATE_YYMMDD);

        if ((min4 * 30) - 30 < 0) {
            if ((hour - 1) < 0) {
                shour = "23";
                timeto = "24:00";
                Date dprev = DateUtils.addDayDate(now, -1);
                periodfr = DateUtils.convertDate(dprev, DateUtils.DATE_YYMMDD);
            } else {
                shour = Integer.toString(hour - 1);
                if (shour.length() == 1) shour = "0" + shour;
            }
            timefr = shour + ":30";
        } else {
            smin = Integer.toString((min4 * 30) - 30);
            if (smin.length() == 1) smin = "0" + smin;
            timefr = shour + ":" + smin;
        }

        result.put(DataConstant.PARAM_TIME_FROM, timefr);
        result.put(DataConstant.PARAM_TIME_TO , timeto);
        result.put(DataConstant.PARAM_PERIOD_FROM, periodfr);
        result.put(DataConstant.PARAM_PERIOD_TO , periodto);

        return result;
    }

    public static Map<String, String> DateToTimeFrTimeTo1h(Date now) {
        Map<String, String> result = new HashMap<>();
        int minute = getDatePart(now, Calendar.MINUTE);
        int hour = getDatePart(now, Calendar.HOUR_OF_DAY);

        String shour = "";
        String smin = "";
        String timefr = "";
        String timeto = "";
        String periodfr = "";
        String periodto = "";

        periodfr = DateUtils.convertDate(now, DateUtils.DATE_YYMMDD);
        periodto = DateUtils.convertDate(now, DateUtils.DATE_YYMMDD);

        if ((hour - 1) < 0) {
            timefr = "23:00";
            timeto = "24:00";
            Date dprev = DateUtils.addDayDate(now, -1);
            periodfr = DateUtils.convertDate(dprev, DateUtils.DATE_YYMMDD);
        } else {
            shour = Integer.toString(hour - 1);
            if (shour.length() == 1) shour = "0" + shour;
            timefr = shour + ":00";
            shour = Integer.toString(hour);
            if (shour.length() == 1) shour = "0" + shour;
            timeto = shour + ":00";
        }

        result.put(DataConstant.PARAM_TIME_FROM, timefr);
        result.put(DataConstant.PARAM_TIME_TO , timeto);
        result.put(DataConstant.PARAM_PERIOD_FROM, periodfr);
        result.put(DataConstant.PARAM_PERIOD_TO , periodto);

        return result;
    }

    public static Map<String, String> DateToTimeFrTimeTo1d(Date now) {
        Map<String, String> result = new HashMap<>();
        int minute = getDatePart(now, Calendar.MINUTE);
        int hour = getDatePart(now, Calendar.HOUR_OF_DAY);

        String shour = "";
        String smin = "";
        String timefr = "";
        String timeto = "";
        String periodfr = "";
        String periodto = "";

        periodfr = DateUtils.convertDate(now, DateUtils.DATE_YYMMDD);
        periodto = periodfr;//DateUtils.convertDate(now, DateUtils.DATE_YYMMDD);

        //if ((hour - 1) < 0) {
        timefr = "00:00";
        timeto = "24:00";
        Date dprev = DateUtils.addDayDate(now, -1);
        periodfr = DateUtils.convertDate(dprev, DateUtils.DATE_YYMMDD);
        periodto = periodfr;
        /*} else {
            shour = Integer.toString(hour - 1);
            if (shour.length() == 1) shour = "0" + shour;
            timefr = shour + ":00";
            shour = Integer.toString(hour);
            if (shour.length() == 1) shour = "0" + shour;
            timeto = shour + ":00";
        }*/

        result.put(DataConstant.PARAM_TIME_FROM, timefr);
        result.put(DataConstant.PARAM_TIME_TO , timeto);
        result.put(DataConstant.PARAM_PERIOD_FROM, periodfr);
        result.put(DataConstant.PARAM_PERIOD_TO , periodto);

        return result;
    }

}

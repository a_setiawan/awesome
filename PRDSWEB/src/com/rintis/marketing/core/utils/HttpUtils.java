package com.rintis.marketing.core.utils;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.net.URLCodec;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class HttpUtils {
    private static Logger log = Logger.getLogger(HttpUtils.class);

    public static String decodeUrlParam(String param, String userId, String sessionId) {
        String result = "";
        try {
            String key = sessionId.substring(0, 10) + userId + sessionId.substring(11, sessionId.length()-1);
            String sha1hex = DigestUtils.sha1Hex(key);
            //log.info(key);
            //log.info(sha1hex);
            if (sha1hex.length() > 32) {
                sha1hex = sha1hex.substring(0, 32);
            }
            //log.info(sha1hex);
            result = EncDecUtils.encrypt(param, sha1hex);
            //log.info(result);

            result = new URLCodec().encode(result);
            //log.info(result);

            result = Hex.encodeHexString(result.getBytes());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    public static String encodeUrlParam(String param, String userId, String sessionId) {
        String result = "";
        try {
            String key = sessionId.substring(0, 10) + userId + sessionId.substring(11, sessionId.length()-1);
            String sha1hex = DigestUtils.sha1Hex(key);
            //log.info(key);
            //log.info(sha1hex);
            if (sha1hex.length() > 32) {
                sha1hex = sha1hex.substring(0, 32);
            }
            //log.info(sha1hex);

            String s1 = new String(Hex.decodeHex(param.toCharArray()));
            String s = new URLCodec().decode(s1);
            result = EncDecUtils.decrypt(s, sha1hex);
            //log.info(result);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    public static Map<String, String> extractUrlParam(String param) {
        Map<String, String> map = new HashMap<>();
        StringTokenizer st = new StringTokenizer(param, "&");
        while (st.hasMoreTokens()) {
            String s = st.nextToken();
            StringTokenizer sts = new StringTokenizer(s, "=");
            String key = sts.nextToken();
            String val = sts.nextToken();
            map.put(key, val);
        }
        return map;
    }

}

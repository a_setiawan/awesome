package com.rintis.marketing.core.utils;


import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.math.BigDecimal;
import java.util.Date;

public class PoiUtils {

    public static final String DATE_FORMAT_DDMMYY_HHMMSS = "dd/MM/yyyy hh:mm:ss";
    public static final String DATE_FORMAT_DDMMYY = "dd/MM/yyyy";

    public static CellStyle cellStyleBold(HSSFWorkbook workbook, CellStyle style) {
        CellStyle cs = null;
        if (style == null) {
            cs = workbook.createCellStyle();
        } else {
            cs = style;
        }
        HSSFFont font = workbook.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        cs.setFont(font);
        return cs;
    }

    public static CellStyle cellStyleFormatDate(HSSFWorkbook workbook, CellStyle style, String dateFormat) {
        CellStyle cs = null;
        HSSFDataFormat dataFormat = workbook.createDataFormat();
        if (style == null) {
            cs = workbook.createCellStyle();
        } else {
            cs = style;
        }
        cs.setDataFormat(dataFormat.getFormat(dateFormat));
        return cs;
    }

    public static CellStyle cellStyleFormatNumber(HSSFWorkbook workbook, CellStyle style) {
        CellStyle cs = null;
        HSSFDataFormat dataFormat = workbook.createDataFormat();
        if (style == null) {
            cs = workbook.createCellStyle();
        } else {
            cs = style;
        }
        cs.setDataFormat(dataFormat.getFormat("#,##0.00"));
        return  cs;
    }

    public static CellStyle cellStyleFormatNumberInteger(HSSFWorkbook workbook, CellStyle style) {
        CellStyle cs = null;
        HSSFDataFormat dataFormat = workbook.createDataFormat();
        if (style == null) {
            cs = workbook.createCellStyle();
        } else {
            cs = style;
        }
        cs.setDataFormat(dataFormat.getFormat("#,##0"));
        return  cs;
    }

    public static CellStyle cellStyleTableHeader(HSSFWorkbook workbook) {
        CellStyle cs = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        cs.setFont(font);
        cs.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
        cs.setFillPattern(CellStyle.SOLID_FOREGROUND);
        return cs;
    }


    public static CellStyle cellStyleBold(XSSFWorkbook workbook, CellStyle style) {
        CellStyle cs = null;
        if (style == null) {
            cs = workbook.createCellStyle();
        } else {
            cs = style;
        }
        XSSFFont font = workbook.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        cs.setFont(font);
        return cs;
    }

    public static CellStyle cellStyleFormatDate(XSSFWorkbook workbook, CellStyle style, String dateFormat) {
        CellStyle cs = null;
        XSSFDataFormat dataFormat = workbook.createDataFormat();
        if (style == null) {
            cs = workbook.createCellStyle();
        } else {
            cs = style;
        }
        cs.setDataFormat(dataFormat.getFormat(dateFormat));
        return cs;
    }

    public static CellStyle cellStyleFormatNumber(XSSFWorkbook workbook, CellStyle style) {
        CellStyle cs = null;
        XSSFDataFormat dataFormat = workbook.createDataFormat();
        if (style == null) {
            cs = workbook.createCellStyle();
        } else {
            cs = style;
        }
        cs.setDataFormat(dataFormat.getFormat("#,##0.00"));
        return  cs;
    }

    public static CellStyle cellStyleFormatNumberInteger(XSSFWorkbook workbook, CellStyle style) {
        CellStyle cs = null;
        XSSFDataFormat dataFormat = workbook.createDataFormat();
        if (style == null) {
            cs = workbook.createCellStyle();
        } else {
            cs = style;
        }
        cs.setDataFormat(dataFormat.getFormat("#,##0"));
        return  cs;
    }

    public static CellStyle cellStyleTableHeader(XSSFWorkbook workbook) {
        CellStyle cs = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        cs.setFont(font);
        cs.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
        cs.setFillPattern(CellStyle.SOLID_FOREGROUND);
        return cs;
    }




    public static CellStyle cellStyleBold(SXSSFWorkbook workbook, CellStyle style) {
        CellStyle cs = null;
        if (style == null) {
            cs = workbook.createCellStyle();
        } else {
            cs = style;
        }
        Font font = workbook.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        cs.setFont(font);
        return cs;
    }

    public static CellStyle cellStyleFormatDate(SXSSFWorkbook workbook, CellStyle style, String dateFormat) {
        CellStyle cs = null;
        DataFormat dataFormat = workbook.createDataFormat();
        if (style == null) {
            cs = workbook.createCellStyle();
        } else {
            cs = style;
        }
        cs.setDataFormat(dataFormat.getFormat(dateFormat));
        return cs;
    }

    public static CellStyle cellStyleFormatNumber(SXSSFWorkbook workbook, CellStyle style) {
        CellStyle cs = null;
        DataFormat dataFormat = workbook.createDataFormat();
        if (style == null) {
            cs = workbook.createCellStyle();
        } else {
            cs = style;
        }
        cs.setDataFormat(dataFormat.getFormat("#,##0.00"));
        return  cs;
    }

    public static CellStyle cellStyleFormatNumberInteger(SXSSFWorkbook workbook, CellStyle style) {
        CellStyle cs = null;
        DataFormat dataFormat = workbook.createDataFormat();
        if (style == null) {
            cs = workbook.createCellStyle();
        } else {
            cs = style;
        }
        cs.setDataFormat(dataFormat.getFormat("#,##0"));
        return  cs;
    }

    public static CellStyle cellStyleTableHeader(SXSSFWorkbook workbook) {
        CellStyle cs = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        cs.setFont(font);
        cs.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
        cs.setFillPattern(CellStyle.SOLID_FOREGROUND);
        return cs;
    }






    public static String getCellString(Cell cell) {
        if (cell != null) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            return cell.getStringCellValue().toUpperCase();
        } else {
            return "";
        }
    }

    public static BigDecimal getCellBigDecimal(Cell cell) {
        if (cell != null) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            String s = StringUtils.checkNull(cell.getStringCellValue());
            if (s.length() == 0) {
                s = "0";
            }
            return new BigDecimal(s);
        } else {
            return BigDecimal.ZERO;
        }
    }

    public static Integer getCellInteger(Cell cell) {
        if (cell != null) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            return new Integer(cell.getStringCellValue());
        } else {
            return 0;
        }
    }

    public static Date getCellDate(Cell cell) {
        if (cell != null) {
            return cell.getDateCellValue();
        } else {
            return null;
        }
    }



}

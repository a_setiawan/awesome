package com.rintis.marketing.core.utils;

public class EisUtils {

    public static String getTransactionName(String transactionId) {
        String s = "";
        if (transactionId == null) {
            return "";
        }
        if (transactionId.equalsIgnoreCase("R")) {
            s = "Withdrawal and Balance Inquiry";
        } else if (transactionId.equalsIgnoreCase("X")) {
            s = "Transfer";
        } else if (transactionId.equalsIgnoreCase("D")) {
            s = "Debit";
        }
        return s;
    }
}

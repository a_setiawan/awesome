package com.rintis.marketing.core;

public class SpringBeanConstant {
    public static final String SPRING_BEAN_COMMON_DAO = "commonDao";
    public static final String SPRING_BEAN_USERLOGIN_DAO = "userLoginDao";
    public static final String SPRING_BEAN_MENU_DAO = "menuDao";
    public static final String SPRING_BEAN_TRANSLOG_DAO = "translogDao";

    public static final String SPRING_BEAN_PAN_DAO = "panDao";
    public static final String SPRING_BEAN_MCC_DAO = "mccDao";
    public static final String SPRING_BEAN_MODULE_FACTORY = "mf";

    public static final String SPRING_BEAN_SEQUENTIAL_DAO = "sequentialDao";
    
    public static final String SPRING_BEAN_COMMON_SERVICE = "commonService";
    public static final String SPRING_BEAN_LOGIN_SERVICE = "loginService";
    public static final String SPRING_BEAN_MENU_SERVICE = "menuService";
    public static final String SPRING_BEAN_TRANSLOG_SERVICE = "translogService";

    public static final String SPRING_BEAN_PAN_SERVICE = "panService";
    public static final String SPRING_BEAN_MCC_SERVICE = "mccService";
    public static final String SPRING_BEAN_SEQUENTIAL_SERVICE = "sequentialService";

    public static final String SPRING_BEAN_REPORT_DAO = "reportDao";
    public static final String SPRING_BEAN_REPORT_SERVICE = "reportService";
    public static final String SPRING_BEAN_REPORT_QUERY_DAO = "reportQueryDao";
    public static final String SPRING_BEAN_REPORT_QUERY_AMOUNT_DAO = "reportQueryAmountDao";
    public static final String SPRING_BEAN_REPORT_QUERY_FEE_DAO = "reportQueryFeeDao";
    public static final String SPRING_BEAN_REPORT_QUERY_SERVICE = "reportQueryService";
    public static final String SPRING_BEAN_REPORT_QUERY_AMOUNT_SERVICE = "reportQueryAmountService";
    public static final String SPRING_BEAN_REPORT_QUERY_FEE_SERVICE = "reportQueryFeeService";
    public static final String SPRING_BEAN_REPORT_QUERY_BILLER_SERVICE = "reportQueryBillerService";
    public static final String SPRING_BEAN_REPORT_QUERY_BILLER_DAO = "reportQueryBillerDao";
    public static final String SPRING_BEAN_REPORT_QUERY_EIS_DAO = "reportQueryEisDao";

    public static final String SPRING_BEAN_ACCOUNT_DAO = "accountDao";
    public static final String SPRING_BEAN_ACCOUNT_SERVICE = "accountService";
    public static final String SPRING_BEAN_MARKETING_DAO = "marketingDao";
    public static final String SPRING_BEAN_MARKETING_SERVICE = "marketingService";
    public static final String SPRING_BEAN_BI_DAO = "biDao";
    public static final String SPRING_BEAN_BI_SERVICE = "biService";

    public static final String SPRING_BEAN_MASTER_DAO = "masterDao";
    public static final String SPRING_BEAN_MASTER_SERVICE = "masterService";

    public static final String SPRING_BEAN_AGGREGATE_DAO = "aggregateDao";
    public static final String SPRING_BEAN_AGGREGATE_SERVICE = "aggregateServices";

    public static final String SPRING_BEAN_ALERT_DAO = "alertDao";
    public static final String SPRING_BEAN_ALERT_SERVICE = "alertServices";

    /*Add by aaw 28/06/2021*/
    public static final String SPRING_BEAN_QR_DAO = "qrDao";
    public static final String SPRING_BEAN_QR_SERVICE = "qrServices";

    public static final String SPRING_BEAN_MPAN_TRESHOLD_SERVICE = "mPanService";
    public static final String SPRING_BEAN_MPAN_TRESHOLD_DAO = "mPanDao";

    public static final String SPRING_BEAN_BILLER_SERVICE = "billerService";
    public static final String SPRING_BEAN_BILLERD_DAO = "billerDao";
    /*End add*/
}

package com.rintis.marketing.config.spring;

import com.rintis.marketing.core.app.main.module.ModuleFactory;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class StartupApp {
    private Logger log = Logger.getLogger(StartupApp.class);
    private ModuleFactory moduleFactory;
    
    public StartupApp() {
    }
    
    public StartupApp(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
        //log.info(moduleFactory);
    }
    
    //@PostConstruct
    public void init() {
        //log.info("startup init");
        try {
            //stop, start job
            //ScheduleTaskController.stopScheduler();
            //ScheduleTaskController.startScheduler();
        } catch(Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
}

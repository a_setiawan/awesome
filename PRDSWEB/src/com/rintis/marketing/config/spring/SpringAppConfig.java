package com.rintis.marketing.config.spring;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;


@Configuration
@EnableTransactionManagement
@ComponentScan(
        basePackages={"com.rintis.marketing.config.spring",
                "com.rintis.marketing.core.app",
                "com.rintis.marketing.web.jsf"}
        )
@PropertySource("classpath:jdbc.properties")
@ImportResource({"/WEB-INF/applicationContext.xml"})
//@ImportResource({"classpath:net/bull/javamelody/monitoring-spring-datasource.xml", "classpath:applicationContext.xml"})
public class SpringAppConfig {
    private Logger log = Logger.getLogger(SpringAppConfig.class);
    @Autowired
    private Environment env;

    @Bean
    public DataSource dataSource() {
        ComboPooledDataSource cp = new ComboPooledDataSource();
        try {
            /*cp.setDriverClass(EncDecUtils.decrypt(env.getProperty("jdbc.driverClassName"), DataUtils.s1 ));
            cp.setJdbcUrl(EncDecUtils.decrypt(env.getProperty("jdbc.url"), DataUtils.s1 ));
            cp.setUser(EncDecUtils.decrypt(env.getProperty("jdbc.username"), DataUtils.s1 ));
            cp.setPassword(EncDecUtils.decrypt(env.getProperty("jdbc.password"), DataUtils.s1 ));*/
            
            cp.setDriverClass(env.getProperty("jdbc.driverClassName"));
            cp.setJdbcUrl(env.getProperty("jdbc.url"));
            cp.setUser(env.getProperty("jdbc.username"));
            cp.setPassword(env.getProperty("jdbc.password"));
        } catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return cp;
    }
    
    @Bean
    public PlatformTransactionManager transactionManager() {
        return new HibernateTransactionManager(sessionFactory());
    }
    
    @Bean
    public SessionFactory sessionFactory() {
        LocalSessionFactoryBuilder ls = new LocalSessionFactoryBuilder(dataSource())
            .scanPackages("com.rintis.marketing.beans.entity");
        ls.setProperty("hibernate.jdbc.batch_size", "0");
        ls.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
        ls.setProperty("hibernate.show_sql", "false");
        ls.setProperty("hibernate.format_sql", "true");
        ls.setProperty("hibernate.generate_statistics", "false");
        ls.setProperty("hibernate.jdbc.batch_versioned_data", "false");
        ls.setProperty("hibernate.jdbc.use_streams_for_binary", "false");
        ls.setProperty("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.EhCacheRegionFactory");
        ls.setProperty("hibernate.cache.use_second_level_cache", "true");
        ls.setProperty("hibernate.cache.use_query_cache", "true");
        ls.setProperty("hibernate.c3p0.min_size", "5");
        ls.setProperty("hibernate.c3p0.max_size", "20");
        ls.setProperty("hibernate.c3p0.timeout", "300");
        ls.setProperty("hibernate.c3p0.max_statements", "50");
        ls.setProperty("hibernate.c3p0.idle_test_period", "3000");
        ls.setProperty("hibernate.c3p0.acquire_retry_attempts ", "0");
        return ls.buildSessionFactory();
    }
    
    
    //@Bean
    //public StartupApp startupApp() {
    //    return new StartupApp(moduleFactory);
    //}

}

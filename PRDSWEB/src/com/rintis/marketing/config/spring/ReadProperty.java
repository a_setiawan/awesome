package com.rintis.marketing.config.spring;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/*Add by ait 17/05/21, read config file for linkdb*/
public class ReadProperty {
    String linkDB = "";
    InputStream inputStream;

    public String getPropValues() throws IOException {

        try {
            Properties prop = new Properties();
            String propFileName = "setting.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

            String profileActive = prop.getProperty("profile.active");
           if (profileActive != null) {
               if (profileActive.equalsIgnoreCase("development")) {
                   linkDB = prop.getProperty("linkdb.development");
               } else {
                   linkDB = prop.getProperty("linkdb.production");
               }
           }

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }
        return linkDB;
    }

    public String getLinkActive() throws IOException {
        Properties props = PropertiesLoaderUtils.loadAllProperties("setting.properties");
        PropertyPlaceholderConfigurer props2 = new PropertyPlaceholderConfigurer();
        props2.setProperties(props);
        String profile = props.getProperty("profile.active");
        if (profile != null) {
            if (profile.equalsIgnoreCase("development")) {
                linkDB = props.getProperty("linkdb.development");
            } else {
                linkDB = props.getProperty("linkdb.production");
            }
        }
        return linkDB;
    }

    public static class dbLink {
        private String linkPos;

        private String linkAtm;

        public String getLinkPos() {
            return linkPos;
        }

        public void setLinkPos(String linkPos) {
            this.linkPos = linkPos;
        }

        public void setLinkAtm(String linkAtm) {
            this.linkAtm = linkAtm;
        }

        public String getLinkAtm() {
            return linkAtm;
        }
    }

    public static dbLink dbLinkActive() throws IOException {
        dbLink db = new dbLink();
        Properties props = PropertiesLoaderUtils.loadAllProperties("setting.properties");
        PropertyPlaceholderConfigurer props2 = new PropertyPlaceholderConfigurer();
        props2.setProperties(props);
        String profile = props.getProperty("profile.active");
        if (profile != null) {
            if (profile.equalsIgnoreCase("development")) {
                db.setLinkPos(props.getProperty("linkdb.pos.development"));
                db.setLinkAtm(props.getProperty("linkdb.development"));
            } else {
                db.setLinkPos(props.getProperty("linkdb.pos.production"));
                db.setLinkAtm(props.getProperty("linkdb.production"));
            }
        }
        return db;
    }

    public static class email {
        private String mailHost;
        private String mailPort;
        private String mailFrom;
        private String mailSubjectBlock;
        private String mailSubjectUnblock;
        private String mailSubjectBlockApprove;
        private String mailSubjectUnblockApprove;
        private String mailSubjectBlockApproveCancel;
        private String mailSubjectUnblockApproveCancel;

        public String getMailHost() {
            return mailHost;
        }

        public void setMailHost(String mailHost) {
            this.mailHost = mailHost;
        }

        public String getMailPort() {
            return mailPort;
        }

        public void setMailPort(String mailPort) {
            this.mailPort = mailPort;
        }

        public String getMailFrom() {
            return mailFrom;
        }

        public void setMailFrom(String mailFrom) {
            this.mailFrom = mailFrom;
        }

        public String getMailSubjectBlock() {
            return mailSubjectBlock;
        }

        public void setMailSubjectBlock(String mailSubjectBlock) {
            this.mailSubjectBlock = mailSubjectBlock;
        }

        public String getMailSubjectUnblock() {
            return mailSubjectUnblock;
        }

        public void setMailSubjectUnblock(String mailSubjectUnblock) {
            this.mailSubjectUnblock = mailSubjectUnblock;
        }

        public String getMailSubjectBlockApprove() {
            return mailSubjectBlockApprove;
        }

        public void setMailSubjectBlockApprove(String mailSubjectBlockApprove) {
            this.mailSubjectBlockApprove = mailSubjectBlockApprove;
        }

        public String getMailSubjectUnblockApprove() {
            return mailSubjectUnblockApprove;
        }

        public void setMailSubjectUnblockApprove(String mailSubjectUnblockApprove) {
            this.mailSubjectUnblockApprove = mailSubjectUnblockApprove;
        }

        public String getMailSubjectBlockApproveCancel() {
            return mailSubjectBlockApproveCancel;
        }

        public void setMailSubjectBlockApproveCancel(String mailSubjectBlockApproveCancel) {
            this.mailSubjectBlockApproveCancel = mailSubjectBlockApproveCancel;
        }

        public String getMailSubjectUnblockApproveCancel() {
            return mailSubjectUnblockApproveCancel;
        }

        public void setMailSubjectUnblockApproveCancel(String mailSubjectUnblockApproveCancel) {
            this.mailSubjectUnblockApproveCancel = mailSubjectUnblockApproveCancel;
        }
    }

    public static email mailProperty() throws IOException {
        email email = new email();
        Properties props = PropertiesLoaderUtils.loadAllProperties("setting.properties");
        PropertyPlaceholderConfigurer prop = new PropertyPlaceholderConfigurer();
        prop.setProperties(props);
        email.setMailHost(props.getProperty("mail_host"));
        email.setMailPort(props.getProperty("mail_port"));
        email.setMailFrom(props.getProperty("mail_from"));
        email.setMailSubjectBlock(props.getProperty("mail_subject_blocked"));
        email.setMailSubjectUnblock(props.getProperty("mail_subject_unblocked"));
        email.setMailSubjectBlockApprove(props.getProperty("mail_subject_blocked_approve"));
        email.setMailSubjectUnblockApprove(props.getProperty("mail_subject_unblocked_approve"));
        email.setMailSubjectBlockApproveCancel(props.getProperty("mail_subject_blocked_approve_cancel"));
        email.setMailSubjectUnblockApproveCancel(props.getProperty("mail_subject_unblocked_approve_cancel"));
        return email;
    }
}
package com.rintis.marketing.test;


import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.StringUtils;
import org.apache.log4j.Logger;

import java.util.Calendar;
import java.util.Date;

public class TestMain1 {
    static Logger log = Logger.getLogger(TestMain1.class);

    public static void main(String[] args) {
        try {
            Date nowDate = new Date();
            Date nowDateStart = DateUtils.getStartOfDay(nowDate);
            Date nowDateEnd = DateUtils.getDateRound5Min(nowDate);

            Date prevDateStart = DateUtils.addDate(Calendar.DAY_OF_MONTH, nowDateStart, -1);
            Date prevDateEnd = DateUtils.addDate(Calendar.DAY_OF_MONTH, nowDateEnd, -1);

            log.info(nowDateStart);
            log.info(nowDateEnd);
            log.info(prevDateStart);
            log.info(prevDateEnd);

            String period = DateUtils.convertDate(nowDateStart, DateUtils.DATE_YYMMDD);
            String shourFrom = "00:00";

            int hourThru = DateUtils.getHour(nowDateEnd);
            String shourThru = StringUtils.formatFixLength(Integer.toString(hourThru), "0", 2) + ":00";

            String stimeFrom = "00:00";
            String stimeFrom2 = "";
            String stimeThru = "";
            int minute = DateUtils.getMinute(nowDateEnd);
            log.info(minute);
            if (minute > 0) {
                stimeFrom2 = "00:" + StringUtils.formatFixLength(Integer.toString(minute-5), "0", 2);
                stimeThru = "00:" + StringUtils.formatFixLength(Integer.toString(minute), "0", 2);
            }

            log.info(period);
            log.info(shourFrom);
            log.info(shourThru);
            log.info(stimeFrom);
            log.info(stimeFrom2);
            log.info(stimeThru);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

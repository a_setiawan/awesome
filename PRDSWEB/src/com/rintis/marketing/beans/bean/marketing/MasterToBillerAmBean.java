package com.rintis.marketing.beans.bean.marketing;

import java.math.BigDecimal;

public class MasterToBillerAmBean {
    private BigDecimal year;
    private String billerId;
    private String billerName;
    private String groupId;

    private String transactionId;
    private String subTransactionId;
    private String transactionName;
    private String subTransactionName;

    private String accountId1;
    private String accountId2;
    private String accountName1;
    private String accountName2;

    public String getBillerId() {
        return billerId;
    }

    public void setBillerId(String billerId) {
        this.billerId = billerId;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getSubTransactionId() {
        return subTransactionId;
    }

    public void setSubTransactionId(String subTransactionId) {
        this.subTransactionId = subTransactionId;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public String getSubTransactionName() {
        return subTransactionName;
    }

    public void setSubTransactionName(String subTransactionName) {
        this.subTransactionName = subTransactionName;
    }

    public String getAccountId1() {
        return accountId1;
    }

    public void setAccountId1(String accountId1) {
        this.accountId1 = accountId1;
    }

    public String getAccountId2() {
        return accountId2;
    }

    public void setAccountId2(String accountId2) {
        this.accountId2 = accountId2;
    }

    public BigDecimal getYear() {
        return year;
    }

    public void setYear(BigDecimal year) {
        this.year = year;
    }

    public String getAccountName1() {
        return accountName1;
    }

    public void setAccountName1(String accountName1) {
        this.accountName1 = accountName1;
    }

    public String getAccountName2() {
        return accountName2;
    }

    public void setAccountName2(String accountName2) {
        this.accountName2 = accountName2;
    }
}

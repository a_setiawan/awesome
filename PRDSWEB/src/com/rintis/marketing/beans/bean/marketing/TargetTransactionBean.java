package com.rintis.marketing.beans.bean.marketing;

import java.math.BigDecimal;

public class TargetTransactionBean {
    private String accountid;
    private BigDecimal targettransactiontypeid;
    private BigDecimal years;
    private String tipe;
    private String accountname;
    private String transactiontypename;

    public String getAccountid() {
        return accountid;
    }

    public void setAccountid(String accountid) {
        this.accountid = accountid;
    }

    public BigDecimal getTargettransactiontypeid() {
        return targettransactiontypeid;
    }

    public void setTargettransactiontypeid(BigDecimal targettransactiontypeid) {
        this.targettransactiontypeid = targettransactiontypeid;
    }

    public BigDecimal getYears() {
        return years;
    }

    public void setYears(BigDecimal years) {
        this.years = years;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getTransactiontypename() {
        return transactiontypename;
    }

    public void setTransactiontypename(String transactiontypename) {
        this.transactiontypename = transactiontypename;
    }
}

package com.rintis.marketing.beans.bean.marketing;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class MasterToBankBean {
    private BigDecimal year;
    private String bankId;
    private String bankName;
    private String groupId;

    private String targetId;
    private String subTransactionId;
    private String targetName;
    private String subTransactionName;
    private int m1;
    private int m2;
    private int m3;
    private int m4;
    private int m5;
    private int m6;
    private int m7;
    private int m8;
    private int m9;
    private int m10;
    private int m11;
    private int m12;

    private BigDecimal mbd1;
    private BigDecimal mbd2;
    private BigDecimal mbd3;
    private BigDecimal mbd4;
    private BigDecimal mbd5;
    private BigDecimal mbd6;
    private BigDecimal mbd7;
    private BigDecimal mbd8;
    private BigDecimal mbd9;
    private BigDecimal mbd10;
    private BigDecimal mbd11;
    private BigDecimal mbd12;

    private Map<String, Object> mapTransactionEnable;
    private Map<String, Object> mapValue;
    private Map<String, Object> mapAccountManager;

    private Map<String, Object> mapValue1;
    private Map<String, Object> mapValue2;
    private Map<String, Object> mapValue3;
    private Map<String, Object> mapValue4;
    private Map<String, Object> mapValue5;
    private Map<String, Object> mapValue6;
    private Map<String, Object> mapValue7;
    private Map<String, Object> mapValue8;
    private Map<String, Object> mapValue9;
    private Map<String, Object> mapValue10;
    private Map<String, Object> mapValue11;
    private Map<String, Object> mapValue12;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Map<String, Object> getMapTransactionEnable() {
        return mapTransactionEnable;
    }

    public void setMapTransactionEnable(Map<String, Object> mapTransactionEnable) {
        this.mapTransactionEnable = mapTransactionEnable;
    }

    public Map<String, Object> getMapValue() {
        return mapValue;
    }

    public void setMapValue(Map<String, Object> mapValue) {
        this.mapValue = mapValue;
    }

    public Map<String, Object> getMapAccountManager() {
        return mapAccountManager;
    }

    public void setMapAccountManager(Map<String, Object> mapAccountManager) {
        this.mapAccountManager = mapAccountManager;
    }

    public Map<String, Object> getMapValue1() {
        return mapValue1;
    }

    public void setMapValue1(Map<String, Object> mapValue1) {
        this.mapValue1 = mapValue1;
    }

    public Map<String, Object> getMapValue2() {
        return mapValue2;
    }

    public void setMapValue2(Map<String, Object> mapValue2) {
        this.mapValue2 = mapValue2;
    }

    public Map<String, Object> getMapValue3() {
        return mapValue3;
    }

    public void setMapValue3(Map<String, Object> mapValue3) {
        this.mapValue3 = mapValue3;
    }

    public Map<String, Object> getMapValue4() {
        return mapValue4;
    }

    public void setMapValue4(Map<String, Object> mapValue4) {
        this.mapValue4 = mapValue4;
    }

    public Map<String, Object> getMapValue5() {
        return mapValue5;
    }

    public void setMapValue5(Map<String, Object> mapValue5) {
        this.mapValue5 = mapValue5;
    }

    public Map<String, Object> getMapValue6() {
        return mapValue6;
    }

    public void setMapValue6(Map<String, Object> mapValue6) {
        this.mapValue6 = mapValue6;
    }

    public Map<String, Object> getMapValue7() {
        return mapValue7;
    }

    public void setMapValue7(Map<String, Object> mapValue7) {
        this.mapValue7 = mapValue7;
    }

    public Map<String, Object> getMapValue8() {
        return mapValue8;
    }

    public void setMapValue8(Map<String, Object> mapValue8) {
        this.mapValue8 = mapValue8;
    }

    public Map<String, Object> getMapValue9() {
        return mapValue9;
    }

    public void setMapValue9(Map<String, Object> mapValue9) {
        this.mapValue9 = mapValue9;
    }

    public Map<String, Object> getMapValue10() {
        return mapValue10;
    }

    public void setMapValue10(Map<String, Object> mapValue10) {
        this.mapValue10 = mapValue10;
    }

    public Map<String, Object> getMapValue11() {
        return mapValue11;
    }

    public void setMapValue11(Map<String, Object> mapValue11) {
        this.mapValue11 = mapValue11;
    }

    public Map<String, Object> getMapValue12() {
        return mapValue12;
    }

    public void setMapValue12(Map<String, Object> mapValue12) {
        this.mapValue12 = mapValue12;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getSubTransactionId() {
        return subTransactionId;
    }

    public void setSubTransactionId(String subTransactionId) {
        this.subTransactionId = subTransactionId;
    }

    public int getM1() {
        return m1;
    }

    public void setM1(int m1) {
        this.m1 = m1;
    }

    public int getM2() {
        return m2;
    }

    public void setM2(int m2) {
        this.m2 = m2;
    }

    public int getM3() {
        return m3;
    }

    public void setM3(int m3) {
        this.m3 = m3;
    }

    public int getM4() {
        return m4;
    }

    public void setM4(int m4) {
        this.m4 = m4;
    }

    public int getM5() {
        return m5;
    }

    public void setM5(int m5) {
        this.m5 = m5;
    }

    public int getM6() {
        return m6;
    }

    public void setM6(int m6) {
        this.m6 = m6;
    }

    public int getM7() {
        return m7;
    }

    public void setM7(int m7) {
        this.m7 = m7;
    }

    public int getM8() {
        return m8;
    }

    public void setM8(int m8) {
        this.m8 = m8;
    }

    public int getM9() {
        return m9;
    }

    public void setM9(int m9) {
        this.m9 = m9;
    }

    public int getM10() {
        return m10;
    }

    public void setM10(int m10) {
        this.m10 = m10;
    }

    public int getM11() {
        return m11;
    }

    public void setM11(int m11) {
        this.m11 = m11;
    }

    public int getM12() {
        return m12;
    }

    public void setM12(int m12) {
        this.m12 = m12;
    }

    public String getTargetName() {
        return targetName;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    public String getSubTransactionName() {
        return subTransactionName;
    }

    public void setSubTransactionName(String subTransactionName) {
        this.subTransactionName = subTransactionName;
    }

    public BigDecimal getYear() {
        return year;
    }

    public void setYear(BigDecimal year) {
        this.year = year;
    }

    public BigDecimal getMbd1() {
        return mbd1;
    }

    public void setMbd1(BigDecimal mbd1) {
        this.mbd1 = mbd1;
    }

    public BigDecimal getMbd2() {
        return mbd2;
    }

    public void setMbd2(BigDecimal mbd2) {
        this.mbd2 = mbd2;
    }

    public BigDecimal getMbd3() {
        return mbd3;
    }

    public void setMbd3(BigDecimal mbd3) {
        this.mbd3 = mbd3;
    }

    public BigDecimal getMbd4() {
        return mbd4;
    }

    public void setMbd4(BigDecimal mbd4) {
        this.mbd4 = mbd4;
    }

    public BigDecimal getMbd5() {
        return mbd5;
    }

    public void setMbd5(BigDecimal mbd5) {
        this.mbd5 = mbd5;
    }

    public BigDecimal getMbd6() {
        return mbd6;
    }

    public void setMbd6(BigDecimal mbd6) {
        this.mbd6 = mbd6;
    }

    public BigDecimal getMbd7() {
        return mbd7;
    }

    public void setMbd7(BigDecimal mbd7) {
        this.mbd7 = mbd7;
    }

    public BigDecimal getMbd8() {
        return mbd8;
    }

    public void setMbd8(BigDecimal mbd8) {
        this.mbd8 = mbd8;
    }

    public BigDecimal getMbd9() {
        return mbd9;
    }

    public void setMbd9(BigDecimal mbd9) {
        this.mbd9 = mbd9;
    }

    public BigDecimal getMbd10() {
        return mbd10;
    }

    public void setMbd10(BigDecimal mbd10) {
        this.mbd10 = mbd10;
    }

    public BigDecimal getMbd11() {
        return mbd11;
    }

    public void setMbd11(BigDecimal mbd11) {
        this.mbd11 = mbd11;
    }

    public BigDecimal getMbd12() {
        return mbd12;
    }

    public void setMbd12(BigDecimal mbd12) {
        this.mbd12 = mbd12;
    }
}

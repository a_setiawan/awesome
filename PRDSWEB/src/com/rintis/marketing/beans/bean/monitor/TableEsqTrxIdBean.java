package com.rintis.marketing.beans.bean.monitor;


import com.rintis.marketing.beans.bean.bi.EsqTrx5MinBean;
import com.rintis.marketing.beans.bean.bi.UserDashboardDetailBean;

import java.util.List;

public class TableEsqTrxIdBean {
    private String typeInterval;
    private UserDashboardDetailBean userDashboardDetailBean;
    private List<EsqTrx5MinBean> listData;

    private String renderAcqOnly;
    private String renderIssOnly;
    private String renderBnfOnly;
    private String renderAcqIss;
    private String renderAcqBnf;
    private String renderIssBnf;

    public UserDashboardDetailBean getUserDashboardDetailBean() {
        return userDashboardDetailBean;
    }

    public void setUserDashboardDetailBean(UserDashboardDetailBean userDashboardDetailBean) {
        this.userDashboardDetailBean = userDashboardDetailBean;
    }

    public String getTypeInterval() {
        return typeInterval;
    }

    public void setTypeInterval(String typeInterval) {
        this.typeInterval = typeInterval;
    }

    public List<EsqTrx5MinBean> getListData() {
        return listData;
    }

    public void setListData(List<EsqTrx5MinBean> listData) {
        this.listData = listData;
    }

    public String getRenderAcqOnly() {
        return renderAcqOnly;
    }

    public void setRenderAcqOnly(String renderAcqOnly) {
        this.renderAcqOnly = renderAcqOnly;
    }

    public String getRenderIssOnly() {
        return renderIssOnly;
    }

    public void setRenderIssOnly(String renderIssOnly) {
        this.renderIssOnly = renderIssOnly;
    }

    public String getRenderBnfOnly() {
        return renderBnfOnly;
    }

    public void setRenderBnfOnly(String renderBnfOnly) {
        this.renderBnfOnly = renderBnfOnly;
    }

    public String getRenderAcqIss() {
        return renderAcqIss;
    }

    public void setRenderAcqIss(String renderAcqIss) {
        this.renderAcqIss = renderAcqIss;
    }

    public String getRenderAcqBnf() {
        return renderAcqBnf;
    }

    public void setRenderAcqBnf(String renderAcqBnf) {
        this.renderAcqBnf = renderAcqBnf;
    }

    public String getRenderIssBnf() {
        return renderIssBnf;
    }

    public void setRenderIssBnf(String renderIssBnf) {
        this.renderIssBnf = renderIssBnf;
    }
}

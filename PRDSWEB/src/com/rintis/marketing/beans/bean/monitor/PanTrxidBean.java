package com.rintis.marketing.beans.bean.monitor;

/**
 * Created by erichie on 09/12/2019.
 */
public class PanTrxidBean {

    private String trxid;
    private String trxname;
    private String trxidindicator;
    private String trxnameindicator;

    public String getTrxid() {
        return trxid;
    }

    public void setTrxid(String trxid) {
        this.trxid = trxid;
    }

    public String getTrxname() {
        return trxname;
    }

    public void setTrxname(String trxname) {
        this.trxname = trxname;
    }

    public String getTrxidindicator() {
        return trxidindicator;
    }

    public void setTrxidindicator(String trxidindicator) {
        this.trxidindicator = trxidindicator;
    }

    public String getTrxnameindicator() {
        return trxnameindicator;
    }

    public void setTrxnameindicator(String trxnameindicator) {
        this.trxnameindicator = trxnameindicator;
    }

}

package com.rintis.marketing.beans.bean.blocked;


public class BlockedAdministrationDetail {
    private String docId;
    private String pan;
    private String bankId;
    private String bankName;
    private String blockedTo;
    private String notes;

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBlockedTo() {
        return blockedTo;
    }

    public void setBlockedTo(String blockedTo) {
        this.blockedTo = blockedTo;
    }
}

package com.rintis.marketing.beans.bean.biller;

import java.io.Serializable;
import java.math.BigDecimal;

/*Add by aaw 20211110*/

public class BillerAnomalyBean implements Serializable {
    private String period;
    private String periodDisplay;
    private String timeFr;
    private String timeTo;
    private String trxType;
    private String tranType;
    private BigDecimal freqApp;
    private BigDecimal freqDc;
    private BigDecimal freqDcFree;
    private BigDecimal totalAmtApp;
    private BigDecimal totalAmtDc;
    private BigDecimal totalAmtDcFree;
    private BigDecimal totalAmt;
    private String chk;
    private String chkColor;
    private String messageType;
    private String responseCode;
    private BigDecimal amount;
    private String description;
    private String instCd;
    private String instName;
    private String periodFrom;
    private String periodTo;
    private String custNo;
    private String custName;

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPeriodDisplay() {
        return periodDisplay;
    }

    public void setPeriodDisplay(String periodDisplay) {
        this.periodDisplay = periodDisplay;
    }

    public String getTimeFr() {
        return timeFr;
    }

    public void setTimeFr(String timeFr) {
        this.timeFr = timeFr;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public String getTranType() {
        return tranType;
    }

    public void setTranType(String tranType) {
        this.tranType = tranType;
    }

    public BigDecimal getFreqApp() {
        return freqApp;
    }

    public void setFreqApp(BigDecimal freqApp) {
        this.freqApp = freqApp;
    }

    public BigDecimal getFreqDcFree() {
        return freqDcFree;
    }

    public void setFreqDcFree(BigDecimal freqDcFree) {
        this.freqDcFree = freqDcFree;
    }

    public BigDecimal getTotalAmtApp() {
        return totalAmtApp;
    }

    public void setTotalAmtApp(BigDecimal totalAmtApp) {
        this.totalAmtApp = totalAmtApp;
    }

    public BigDecimal getTotalAmtDcFree() {
        return totalAmtDcFree;
    }

    public void setTotalAmtDcFree(BigDecimal totalAmtDcFree) {
        this.totalAmtDcFree = totalAmtDcFree;
    }

    public BigDecimal getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getChk() {
        return chk;
    }

    public void setChk(String chk) {
        this.chk = chk;
    }

    public String getChkColor() {
        return chkColor;
    }

    public void setChkColor(String chkColor) {
        this.chkColor = chkColor;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInstCd() {
        return instCd;
    }

    public void setInstCd(String instCd) {
        this.instCd = instCd;
    }

    public String getPeriodFrom() {
        return periodFrom;
    }

    public void setPeriodFrom(String periodFrom) {
        this.periodFrom = periodFrom;
    }

    public String getPeriodTo() {
        return periodTo;
    }

    public void setPeriodTo(String periodTo) {
        this.periodTo = periodTo;
    }

    public String getCustNo() {
        return custNo;
    }

    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public BigDecimal getFreqDc() {
        return freqDc;
    }

    public void setFreqDc(BigDecimal freqDc) {
        this.freqDc = freqDc;
    }

    public BigDecimal getTotalAmtDc() {
        return totalAmtDc;
    }

    public void setTotalAmtDc(BigDecimal totalAmtDc) {
        this.totalAmtDc = totalAmtDc;
    }

    public String getInstName() {
        return instName;
    }

    public void setInstName(String instName) {
        this.instName = instName;
    }
}
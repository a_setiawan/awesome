package com.rintis.marketing.beans.bean.biller;

public class TmsBillerBean {
    private String billerId;
    private String billerFiid;
    private String billerName;
    private String activeStatus;

    public String getBillerId() {
        return billerId;
    }

    public void setBillerId(String billerId) {
        this.billerId = billerId;
    }

    public String getBillerFiid() {
        return billerFiid;
    }

    public void setBillerFiid(String billerFiid) {
        this.billerFiid = billerFiid;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }
}
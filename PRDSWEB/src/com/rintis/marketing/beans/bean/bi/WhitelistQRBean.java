package com.rintis.marketing.beans.bean.bi;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by aaw on 28/06/21.
 */
public class WhitelistQRBean implements Serializable {
    private String pan;
    private String acquirerId;
    private String acquirerBank;
    private BigDecimal freqApp;
    private BigDecimal freqDc;
    private BigDecimal freqDcFree;
    private BigDecimal freqRvsl;
    private BigDecimal totalAmtApp;
    private BigDecimal totalAmtDc;
    private BigDecimal totalAmtDcFree;
    private BigDecimal totalAmtRvsl;
    private BigDecimal totalAmt;
    private String active;

    public String getPan() {
        return pan;
    }
    public void setPan(String pan) {
        this.pan = pan;
    }
    public String getAcquirerId() {
        return acquirerId;
    }
    public void setAcquirerId(String acquirerId) {
        this.acquirerId = acquirerId;
    }
    public BigDecimal getFreqApp() {
        return freqApp;
    }
    public void setFreqApp(BigDecimal FreqApp) {
        this.freqApp = FreqApp;
    }
    public BigDecimal getFreqDc() {
        return freqDc;
    }
    public void setFreqDc(BigDecimal FreqDc) {
        this.freqDc = FreqDc;
    }
    public BigDecimal getFreqDcFree() {
        return freqDcFree;
    }
    public void setFreqDcFree(BigDecimal FreqDcFree) {
        this.freqDcFree = FreqDcFree;
    }
    public BigDecimal getFreqRvsl() {
        return freqRvsl;
    }
    public void setFreqRvsl(BigDecimal FreqRvsl) {
        this.freqRvsl = FreqRvsl;
    }
    public BigDecimal getTotalAmtApp() {
        return totalAmtApp;
    }
    public void setTotalAmtApp(BigDecimal TotalAmtApp) {
        this.totalAmtApp = TotalAmtApp;
    }
    public BigDecimal getTotalAmtDc() {
        return totalAmtDc;
    }
    public void setTotalAmtDc(BigDecimal TotalAmtDc) {
        this.totalAmtDc = TotalAmtDc;
    }
    public BigDecimal getTotalAmtDcFree() {
        return totalAmtDcFree;
    }
    public void setTotalAmtDcFree(BigDecimal TotalAmtDcFree) {
        this.totalAmtDcFree = TotalAmtDcFree;
    }
    public BigDecimal getTotalAmtRvsl() {
        return totalAmtRvsl;
    }
    public void setTotalAmtRvsl(BigDecimal TotalAmtRvsl) {
        this.totalAmtRvsl = TotalAmtRvsl;
    }
    public BigDecimal getTotalAmt() {
        return totalAmt;
    }
    public void setTotalAmt(BigDecimal TotalAmt) {
        this.totalAmt = TotalAmt;
    }
    public String getActive() {
        return active;
    }
    public void setActive(String Active) {
        this.active = Active;
    }
    public String getAcquirerBank() {
        return acquirerBank;
    }
    public void setAcquirerBank(String acquirerBank) {
        this.acquirerBank = acquirerBank;
    }
}

package com.rintis.marketing.beans.bean.bi;

import java.math.BigDecimal;

/**
 * Created by aabraham on 25/02/2019.
 */
public class DetailPanAnomalyBean {
    private String period;
    private String trxtime;
    private String terminal;
    private String termid;
    private String bankbene;
    private String namebene;
    private String toaccount;
    private BigDecimal app;
    private BigDecimal dc;
    private BigDecimal dcfree;
    private BigDecimal rvsl;
    private BigDecimal amount;
    private String rc;
    private String msgtype;
    private String posentrymode;
    private String bankacq;
    private String description;

    public String getPosentrymode() {
        return posentrymode;
    }
    public void setPosentrymode(String posentrymode) {
        this.posentrymode = posentrymode;
    }

    public String getBankacq() {
        return bankacq;
    }
    public void setBankacq(String bankacq) {
        this.bankacq = bankacq;
    }

    public String getPeriod() {
        return period;
    }
    public void setPeriod(String period) {
        this.period = period;
    }

    public String getTrxTime() {
        return trxtime;
    }
    public void setTrxTime(String trxtime) {
        this.trxtime = trxtime;
    }

    public String getTerminal() {
        return terminal;
    }
    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTermId() {
        return termid;
    }
    public void setTermId(String termid) {
        this.termid = termid;
    }

    public String getBankBene() {
        return bankbene;
    }
    public void setBankBene(String bankbene) {
        this.bankbene = bankbene;
    }

    public String getNameBene() {
        return namebene;
    }
    public void setNameBene(String namebene) {
        this.namebene = namebene;
    }

    public String getToAccount() {
        return toaccount;
    }
    public void setToAccount(String toaccount) {
        this.toaccount = toaccount;
    }

    public BigDecimal getApp() {
        return app;
    }
    public void setApp(BigDecimal app) {
        this.app = app;
    }

    public BigDecimal getDc() {
        return dc;
    }
    public void setDc(BigDecimal dc) {
        this.dc = dc;
    }

    public BigDecimal getDcFree() {
        return dcfree;
    }
    public void setDcFree(BigDecimal dcfree) {
        this.dcfree = dcfree;
    }

    public BigDecimal getRvsl() {
        return rvsl;
    }
    public void setRvsl(BigDecimal rvsl) {
        this.rvsl = rvsl;
    }

    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getRc() {
        return rc;
    }
    public void setRc(String rc) {
        this.rc = rc;
    }

    public String getMsgtype() {
        return msgtype;
    }
    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}

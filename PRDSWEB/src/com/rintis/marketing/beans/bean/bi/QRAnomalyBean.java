package com.rintis.marketing.beans.bean.bi;

import java.io.Serializable;
import java.math.BigDecimal;

/*Created by aaw*/

public class QRAnomalyBean implements Serializable {
    private String period;
    private String perioddisplay;
    private String timefr;
    private String timeto;
    private String trxtype;
    private String trantype;
    private String pan;
    private BigDecimal freqapp;
    private BigDecimal freqdc;
    private BigDecimal freqdcfree;
    private BigDecimal freqrvsl;
    private BigDecimal totalamtapp;
    private BigDecimal totalamtdc;
    private BigDecimal totalamtdcfree;
    private BigDecimal totalamtrvsl;
    private BigDecimal totalamt;
    private String chk;
    private String chkcolor;
    private String messagetype;
    private String responsecode;
    private BigDecimal amount;
    private String description;
    private String termid;
    private String terminal;
    private String termownername;
    private String posentrymode;
    private String status;
    private String acquirerBank;
    private String acquirerId;
    private String qrType;
    private String periodFrom;
    private String periodTo;

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getPeriod() {
        return period;
    }
    public void setPeriod(String period) {
        this.period = period;
    }
    public String getPeriodDisplay() {
        return perioddisplay;
    }
    public void setPeriodDisplay(String perioddisplay) {
        this.perioddisplay = perioddisplay;
    }
    public String getTimeFr() {
        return timefr;
    }
    public void setTimeFr(String timefr) {
        this.timefr = timefr;
    }
    public String getTimeTo() {
        return timeto;
    }
    public void setTimeTo(String timeto) {
        this.timeto = timeto;
    }
    public String getTrxType() {
        return trxtype;
    }
    public void setTrxType(String trx_type) {
        this.trxtype = trx_type;
    }
    public String getTranType() {
        return trantype;
    }
    public void setTranType(String tran_type) {
        this.trantype = tran_type;
    }
    public String getPan() {
        return pan;
    }
    public void setPan(String pan) {
        this.pan = pan;
    }
    public BigDecimal getFreqApp() {
        return freqapp;
    }
    public void setFreqApp(BigDecimal freq_app) {
        this.freqapp = freq_app;
    }
    public BigDecimal getFreqDc() {
        return freqdc;
    }
    public void setFreqDc(BigDecimal freq_dc) {
        this.freqdc = freq_dc;
    }
    public BigDecimal getFreqDcFree() {
        return freqdcfree;
    }
    public void setFreqDcFree(BigDecimal freq_dc_free) {
        this.freqdcfree = freq_dc_free;
    }
    public BigDecimal getFreqRvsl() {
        return freqrvsl;
    }
    public void setFreqRvsl(BigDecimal freq_rvsl) {
        this.freqrvsl = freq_rvsl;
    }
    public BigDecimal getAmtApp() {
        return totalamtapp;
    }
    public void setAmtApp(BigDecimal totalamtapp) {
        this.totalamtapp = totalamtapp;
    }
    public BigDecimal getAmtDc() {
        return totalamtdc;
    }
    public void setAmtDc(BigDecimal totalamtdc) {
        this.totalamtdc = totalamtdc;
    }
    public BigDecimal getAmtDcFree() {
        return totalamtdcfree;
    }
    public void setAmtDcFree(BigDecimal totalamtdcfree) {
        this.totalamtdcfree = totalamtdcfree;
    }
    public BigDecimal getAmtRvsl() {
        return totalamtrvsl;
    }
    public void setAmtRvsl(BigDecimal totalamtrvsl) {
        this.totalamtrvsl = totalamtrvsl;
    }
    public BigDecimal getAmt() {
        return totalamt;
    }
    public void setAmt(BigDecimal totalamt) {
        this.totalamt = totalamt;
    }
    public String getChk() {
        return chk;
    }
    public void setChk(String chk) {
        this.chk = chk;
    }
    public String getChkColor() {
        return chkcolor;
    }
    public void setChkColor(String chkcolor) {
        this.chkcolor = chkcolor;
    }
    public String getMessageType() {
        return messagetype;
    }
    public void setMessageType(String messagetype) {
        this.messagetype = messagetype;
    }
    public String getResponseCode() {
        return responsecode;
    }
    public void setResponseCode(String responsecode) {
        this.responsecode = responsecode;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public String getTermId() {
        return termid;
    }
    public void setTermId(String termid) {
        this.termid = termid;
    }
    public String getTermOwnerName() {
        return termownername;
    }
    public void setTermOwnerName(String termownername) {
        this.termownername = termownername;
    }
    public String getPosEntryMode() {
        return posentrymode;
    }
    public void setPosEntryMode(String posentrymode) {
        this.posentrymode = posentrymode;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getAcquirerBank() {
        return acquirerBank;
    }

    public void setAcquirerBank(String acquirerBank) {
        this.acquirerBank = acquirerBank;
    }

    public String getAcquirerId() {
        return acquirerId;
    }

    public void setAcquirerId(String acquirerId) {
        this.acquirerId = acquirerId;
    }

    public String getQrType() {
        return qrType;
    }

    public void setQrType(String qrType) {
        this.qrType = qrType;
    }

    public String getPeriodFrom() {
        return periodFrom;
    }

    public void setPeriodFrom(String periodFrom) {
        this.periodFrom = periodFrom;
    }

    public String getPeriodTo() {
        return periodTo;
    }

    public void setPeriodTo(String periodTo) {
        this.periodTo = periodTo;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }
}

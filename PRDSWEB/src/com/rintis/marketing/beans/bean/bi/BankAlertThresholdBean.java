package com.rintis.marketing.beans.bean.bi;

import java.math.BigDecimal;

public class BankAlertThresholdBean {
    private BigDecimal active;
    private Boolean activeBool;
    private String bankid;
    private String bankname;
    private String transactionid;
    private String transactionname;
    private BigDecimal thresholdm1;
    private BigDecimal threshold5minwdwd;
    private BigDecimal threshold5minwdhd;
    private BigDecimal thresholdhourwdwd;
    private BigDecimal thresholdhourwdhd;
    private String starthour;
    private String startminute;
    private String endhour;
    private String endminute;
    private BigDecimal comparemethod1int;
    private BigDecimal comparemethod2int;
    private BigDecimal comparemethod3int;
    private BigDecimal comparemethod4int;
    private Boolean comparemethod1;
    private Boolean comparemethod2;
    private Boolean comparemethod3;
    private Boolean comparemethod4;
    private String limitintervalminute;
    private BigDecimal total;


    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public String getTransactionname() {
        return transactionname;
    }

    public void setTransactionname(String transactionname) {
        this.transactionname = transactionname;
    }

    public BigDecimal getThreshold5minwdwd() {
        return threshold5minwdwd;
    }

    public void setThreshold5minwdwd(BigDecimal threshold5minwdwd) {
        this.threshold5minwdwd = threshold5minwdwd;
    }

    public BigDecimal getThreshold5minwdhd() {
        return threshold5minwdhd;
    }

    public void setThreshold5minwdhd(BigDecimal threshold5minwdhd) {
        this.threshold5minwdhd = threshold5minwdhd;
    }

    public BigDecimal getThresholdhourwdwd() {
        return thresholdhourwdwd;
    }

    public void setThresholdhourwdwd(BigDecimal thresholdhourwdwd) {
        this.thresholdhourwdwd = thresholdhourwdwd;
    }

    public BigDecimal getThresholdhourwdhd() {
        return thresholdhourwdhd;
    }

    public void setThresholdhourwdhd(BigDecimal thresholdhourwdhd) {
        this.thresholdhourwdhd = thresholdhourwdhd;
    }

    public String getStarthour() {
        return starthour;
    }

    public void setStarthour(String starthour) {
        this.starthour = starthour;
    }

    public String getStartminute() {
        return startminute;
    }

    public void setStartminute(String startminute) {
        this.startminute = startminute;
    }

    public String getEndhour() {
        return endhour;
    }

    public void setEndhour(String endhour) {
        this.endhour = endhour;
    }

    public String getEndminute() {
        return endminute;
    }

    public void setEndminute(String endminute) {
        this.endminute = endminute;
    }

    public BigDecimal getComparemethod1int() {
        return comparemethod1int;
    }

    public void setComparemethod1int(BigDecimal comparemethod1int) {
        this.comparemethod1int = comparemethod1int;
    }

    public BigDecimal getComparemethod2int() {
        return comparemethod2int;
    }

    public void setComparemethod2int(BigDecimal comparemethod2int) {
        this.comparemethod2int = comparemethod2int;
    }

    public Boolean getComparemethod1() {
        return comparemethod1;
    }

    public void setComparemethod1(Boolean comparemethod1) {
        this.comparemethod1 = comparemethod1;
    }

    public Boolean getComparemethod2() {
        return comparemethod2;
    }

    public void setComparemethod2(Boolean comparemethod2) {
        this.comparemethod2 = comparemethod2;
    }

    public String getLimitintervalminute() {
        return limitintervalminute;
    }

    public void setLimitintervalminute(String limitintervalminute) {
        this.limitintervalminute = limitintervalminute;
    }

    public BigDecimal getComparemethod3int() {
        return comparemethod3int;
    }

    public void setComparemethod3int(BigDecimal comparemethod3int) {
        this.comparemethod3int = comparemethod3int;
    }

    public BigDecimal getComparemethod4int() {
        return comparemethod4int;
    }

    public void setComparemethod4int(BigDecimal comparemethod4int) {
        this.comparemethod4int = comparemethod4int;
    }

    public Boolean getComparemethod3() {
        return comparemethod3;
    }

    public void setComparemethod3(Boolean comparemethod3) {
        this.comparemethod3 = comparemethod3;
    }

    public Boolean getComparemethod4() {
        return comparemethod4;
    }

    public void setComparemethod4(Boolean comparemethod4) {
        this.comparemethod4 = comparemethod4;
    }

    public BigDecimal getActive() {
        return active;
    }

    public void setActive(BigDecimal active) {
        this.active = active;
    }

    public Boolean getActiveBool() {
        return activeBool;
    }

    public void setActiveBool(Boolean activeBool) {
        this.activeBool = activeBool;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getThresholdm1() {
        return thresholdm1;
    }

    public void setThresholdm1(BigDecimal thresholdm1) {
        this.thresholdm1 = thresholdm1;
    }
}

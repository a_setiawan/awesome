package com.rintis.marketing.beans.bean.bi;

import java.io.Serializable;
import java.math.BigDecimal;

public class MCCAnomalyBean implements Serializable {
    private String period;
    private String perioddisplay;
    private String trxtype;
    private String trantype;
    private String pan;
    private String issuerbank;
    private String mcc;
    private String description;
    private BigDecimal freqapp;
    private BigDecimal freqtot;
    private BigDecimal totalamtapp;
    private BigDecimal totalamttot;

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPerioddisplay() {
        return perioddisplay;
    }

    public void setPerioddisplay(String perioddisplay) {
        this.perioddisplay = perioddisplay;
    }

    public String getTrxtype() {
        return trxtype;
    }

    public void setTrxtype(String trxtype) {
        this.trxtype = trxtype;
    }

    public String getTrantype() {
        return trantype;
    }

    public void setTrantype(String trantype) {
        this.trantype = trantype;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getIssuerbank() {
        return issuerbank;
    }

    public void setIssuerbank(String issuerbank) {
        this.issuerbank = issuerbank;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public BigDecimal getFreqapp() {
        return freqapp;
    }

    public void setFreqapp(BigDecimal freqapp) {
        this.freqapp = freqapp;
    }

    public BigDecimal getFreqtot() {
        return freqtot;
    }

    public void setFreqtot(BigDecimal freqtot) {
        this.freqtot = freqtot;
    }

    public BigDecimal getTotalamtapp() {
        return totalamtapp;
    }

    public void setTotalamtapp(BigDecimal totalamtapp) {
        this.totalamtapp = totalamtapp;
    }

    public BigDecimal getTotalamttot() {
        return totalamttot;
    }

    public void setTotalamttot(BigDecimal totalamttot) {
        this.totalamttot = totalamttot;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

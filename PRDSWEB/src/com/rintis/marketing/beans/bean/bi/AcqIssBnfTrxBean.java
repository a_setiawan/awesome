package com.rintis.marketing.beans.bean.bi;


import java.math.BigDecimal;

public class AcqIssBnfTrxBean {
    private String acqid;
    private String issid;
    private String bnfid;
    private String trxid;
    private BigDecimal totalprev;
    private BigDecimal totalcurr;

    public String getAcqid() {
        return acqid;
    }

    public void setAcqid(String acqid) {
        this.acqid = acqid;
    }

    public String getIssid() {
        return issid;
    }

    public void setIssid(String issid) {
        this.issid = issid;
    }

    public String getBnfid() {
        return bnfid;
    }

    public void setBnfid(String bnfid) {
        this.bnfid = bnfid;
    }

    public String getTrxid() {
        return trxid;
    }

    public void setTrxid(String trxid) {
        this.trxid = trxid;
    }

    public BigDecimal getTotalprev() {
        return totalprev;
    }

    public void setTotalprev(BigDecimal totalprev) {
        this.totalprev = totalprev;
    }

    public BigDecimal getTotalcurr() {
        return totalcurr;
    }

    public void setTotalcurr(BigDecimal totalcurr) {
        this.totalcurr = totalcurr;
    }
}

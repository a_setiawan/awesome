package com.rintis.marketing.beans.bean.bi;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by aabraham on 11/02/2019.
 */
/*Modify by aww 21/07/2021, add implements serializable to fixing when sorting data*/
public class WhitelistBean implements Serializable {
    private String pan;
    private String pengirim;
    private BigDecimal freqApp;
    private BigDecimal freqDc;
    private BigDecimal freqDcFree;
    private BigDecimal freqRvsl;
    private BigDecimal totalAmtApp;
    private BigDecimal totalAmtDc;
    private BigDecimal totalAmtDcFree;
    private BigDecimal totalAmtRvsl;
    private BigDecimal totalAmt;
    private String active;

    public String getPan() {
        return pan;
    }
    public void setPan(String Pan) {
        this.pan = Pan;
    }
    public String getPengirim() {
        return pengirim;
    }
    public void setPengirim(String Pengirim) {
        this.pengirim = Pengirim;
    }
    public BigDecimal getFreqApp() {
        return freqApp;
    }
    public void setFreqApp(BigDecimal FreqApp) {
        this.freqApp = FreqApp;
    }
    public BigDecimal getFreqDc() {
        return freqDc;
    }
    public void setFreqDc(BigDecimal FreqDc) {
        this.freqDc = FreqDc;
    }
    public BigDecimal getFreqDcFree() {
        return freqDcFree;
    }
    public void setFreqDcFree(BigDecimal FreqDcFree) {
        this.freqDcFree = FreqDcFree;
    }
    public BigDecimal getFreqRvsl() {
        return freqRvsl;
    }
    public void setFreqRvsl(BigDecimal FreqRvsl) {
        this.freqRvsl = FreqRvsl;
    }
    public BigDecimal getTotalAmtApp() {
        return totalAmtApp;
    }
    public void setTotalAmtApp(BigDecimal TotalAmtApp) {
        this.totalAmtApp = TotalAmtApp;
    }
    public BigDecimal getTotalAmtDc() {
        return totalAmtDc;
    }
    public void setTotalAmtDc(BigDecimal TotalAmtDc) {
        this.totalAmtDc = TotalAmtDc;
    }
    public BigDecimal getTotalAmtDcFree() {
        return totalAmtDcFree;
    }
    public void setTotalAmtDcFree(BigDecimal TotalAmtDcFree) {
        this.totalAmtDcFree = TotalAmtDcFree;
    }
    public BigDecimal getTotalAmtRvsl() {
        return totalAmtRvsl;
    }
    public void setTotalAmtRvsl(BigDecimal TotalAmtRvsl) {
        this.totalAmtRvsl = TotalAmtRvsl;
    }
    public BigDecimal getTotalAmt() {
        return totalAmt;
    }
    public void setTotalAmt(BigDecimal TotalAmt) {
        this.totalAmt = TotalAmt;
    }
    public String getActive() {
        return active;
    }
    public void setActive(String Active) {
        this.active = Active;
    }
}

package com.rintis.marketing.beans.bean.bi;

import java.math.BigDecimal;

public class DashboardRoleBean {
    private BigDecimal dashboardid;
    private String menuname;
    private String description;
    private BigDecimal roletypeid;
    private boolean selected;

    public BigDecimal getDashboardid() {
        return dashboardid;
    }

    public void setDashboardid(BigDecimal dashboardid) {
        this.dashboardid = dashboardid;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getRoletypeid() {
        return roletypeid;
    }

    public void setRoletypeid(BigDecimal roletypeid) {
        this.roletypeid = roletypeid;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}

package com.rintis.marketing.beans.bean.bi;

/**
 * Created by aabraham on 01/03/2019.
 */

/**
 * modify by erichie
 */
public class PanAnomalyEmailBean {
    private String useremail;
    private String gender;
    private String username;
    //start modify
    private String alert1h;
    private String alert3h;
    private String alert6h;
    private String alert1d;
    private String alert3d;
    private String alertMaxAmt;
    private String alertMccVelocity;
    private String alertSeqVelocity;
    //end modify
    /*Add by aaw 02/07/2021*/
    private String alertMpanAcq;
    /*End add*/

    public String getUserEmail() {
        return useremail;
    }
    public void setUserEmail(String useremail) {
        this.useremail = useremail;
    }

    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserName() {
        return username;
    }
    public void setUserName(String username) {
        this.username = username;
    }

    //start modify
    public String getAlert1H() {
        return alert1h;
    }
    public void setAlert1H(String alert1h) {
        this.alert1h = alert1h;
    }

    public String getAlert3H() {
        return alert3h;
    }
    public void setAlert3H(String alert3h) {
        this.alert3h = alert3h;
    }

    public String getAlert6H(){
        return alert6h;
    }
    public void setAlert6H(String alert6h) { this.alert6h = alert6h;}

    public String getAlert1D() {
        return alert1d;
    }
    public void setAlert1D(String alert1d) {
        this.alert1d = alert1d;
    }

    public String getAlert3D() {
        return alert3d;
    }
    public void setAlert3D(String alert3d) {
        this.alert3d = alert3d;
    }

    public String getAlertMaxAmt() {
        return alertMaxAmt;
    }
    public void setAlertMaxAmt(String alertMaxAmt) {
        this.alertMaxAmt = alertMaxAmt;
    }

    public String getAlertMccVelocity() {
        return alertMccVelocity;
    }

    public void setAlertMccVelocity(String alertMccVelocity) {
        this.alertMccVelocity = alertMccVelocity;
    }

    public String getAlertSeqVelocity() {
        return alertSeqVelocity;
    }

    public void setAlertSeqVelocity(String alertSeqVelocity) {
        this.alertSeqVelocity = alertSeqVelocity;
    }
    //end modify

    /*Add by aaw 02/07/2021*/
    public String getAlertMpanAcq() {
        return alertMpanAcq;
    }

    public void setAlertMpanAcq(String alertMpanAcq) {
        this.alertMpanAcq = alertMpanAcq;
    }
    /*End add*/
}

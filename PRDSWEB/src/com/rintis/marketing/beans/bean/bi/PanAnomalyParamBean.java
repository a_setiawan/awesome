package com.rintis.marketing.beans.bean.bi;

import java.math.BigDecimal;

/**
 * Created by aabraham on 04/03/2019.
 */
public class PanAnomalyParamBean {
    private String trx;
    private String trxid;
    private BigDecimal mintrxapp1h;
    private BigDecimal minamtapp1h;
    private BigDecimal mintrxdc1h;
    private BigDecimal minamtdc1h;
    private BigDecimal mintrxdcfree1h;
    private BigDecimal minamtdcfree1h;
    private BigDecimal mintrxrvsl1h;
    private BigDecimal minamtrvsl1h;
    private BigDecimal minamttot1h;
    private BigDecimal mintrxapp3h;
    private BigDecimal minamtapp3h;
    private BigDecimal mintrxdc3h;
    private BigDecimal minamtdc3h;
    private BigDecimal mintrxdcfree3h;
    private BigDecimal minamtdcfree3h;
    private BigDecimal mintrxrvsl3h;
    private BigDecimal minamtrvsl3h;
    private BigDecimal minamttot3h;
    private BigDecimal mintrxapp6h;
    private BigDecimal minamtapp6h;
    private BigDecimal mintrxdc6h;
    private BigDecimal minamtdc6h;
    private BigDecimal mintrxdcfree6h;
    private BigDecimal minamtdcfree6h;
    private BigDecimal mintrxrvsl6h;
    private BigDecimal minamtrvsl6h;
    private BigDecimal minamttot6h;
    private BigDecimal mintrxapp1d;
    private BigDecimal minamtapp1d;
    private BigDecimal mintrxdc1d;
    private BigDecimal minamtdc1d;
    private BigDecimal mintrxdcfree1d;
    private BigDecimal minamtdcfree1d;
    private BigDecimal mintrxrvsl1d;
    private BigDecimal minamtrvsl1d;
    private BigDecimal minamttot1d;
    private BigDecimal mintrxapp3d;
    private BigDecimal minamtapp3d;
    private BigDecimal mintrxdc3d;
    private BigDecimal minamtdc3d;
    private BigDecimal mintrxdcfree3d;
    private BigDecimal minamtdcfree3d;
    private BigDecimal mintrxrvsl3d;
    private BigDecimal minamtrvsl3d;
    private BigDecimal minamttot3d;
    private BigDecimal maxamt1h;

    public String getTrx() {
        return trx;
    }
    public void setTrx(String trx) {
        this.trx = trx;
    }

    public String getTrxid() {
        return trxid;
    }

    public void setTrxid(String trxid) {
        this.trxid = trxid;
    }

    public BigDecimal getTrxApp1h() {
        return mintrxapp1h;
    }
    public void setTrxApp1h(BigDecimal mintrxapp1h) {
        this.mintrxapp1h = mintrxapp1h;
    }

    public BigDecimal getTrxApp3h() {
        return mintrxapp3h;
    }
    public void setTrxApp3h(BigDecimal mintrxapp3h) {
        this.mintrxapp3h = mintrxapp3h;
    }

    public BigDecimal getTrxApp6h() {
        return mintrxapp6h;
    }
    public void setTrxApp6h(BigDecimal mintrxapp6h) {
        this.mintrxapp6h = mintrxapp6h;
    }

    public BigDecimal getTrxDc1h() {
        return mintrxdc1h;
    }
    public void setTrxDc1h(BigDecimal mintrxdc1h) {
        this.mintrxdc1h = mintrxdc1h;
    }

    public BigDecimal getTrxDc3h() {
        return mintrxdc3h;
    }
    public void setTrxDc3h(BigDecimal mintrxdc3h) {
        this.mintrxdc3h = mintrxdc3h;
    }

    public BigDecimal getTrxDc6h() {
        return mintrxdc6h;
    }
    public void setTrxDc6h(BigDecimal mintrxdc6h) {
        this.mintrxdc6h = mintrxdc6h;
    }

    public BigDecimal getTrxDcFree1h() {
        return mintrxdcfree1h;
    }
    public void setTrxDcFree1h(BigDecimal mintrxdcfree1h) {
        this.mintrxdcfree1h = mintrxdcfree1h;
    }

    public BigDecimal getTrxDcFree3h() {
        return mintrxdcfree3h;
    }
    public void setTrxDcFree3h(BigDecimal mintrxdcfree3h) {
        this.mintrxdcfree3h = mintrxdcfree3h;
    }

    public BigDecimal getTrxDcFree6h() {
        return mintrxdcfree6h;
    }
    public void setTrxDcFree6h(BigDecimal mintrxdcfree6h) {
        this.mintrxdcfree6h = mintrxdcfree6h;
    }

    public BigDecimal getTrxRvsl1h() {
        return mintrxrvsl1h;
    }
    public void setTrxRvsl1h(BigDecimal mintrxrvsl1h) {
        this.mintrxrvsl1h = mintrxrvsl1h;
    }

    public BigDecimal getTrxRvsl3h() {
        return mintrxrvsl3h;
    }
    public void setTrxRvsl3h(BigDecimal mintrxrvsl3h) {
        this.mintrxrvsl3h = mintrxrvsl3h;
    }

    public BigDecimal getTrxRvsl6h() {
        return mintrxrvsl6h;
    }
    public void setTrxRvsl6h(BigDecimal mintrxrvsl6h) {
        this.mintrxrvsl6h = mintrxrvsl6h;
    }

    public BigDecimal getAmtApp1h() {
        return minamtapp1h;
    }
    public void setAmtApp1h(BigDecimal minamtapp1h) {
        this.minamtapp1h = minamtapp1h;
    }

    public BigDecimal getAmtApp3h() {
        return minamtapp3h;
    }
    public void setAmtApp3h(BigDecimal minamtapp3h) {
        this.minamtapp3h = minamtapp3h;
    }

    public BigDecimal getAmtApp6h() {
        return minamtapp6h;
    }
    public void setAmtApp6h(BigDecimal minamtapp6h) {
        this.minamtapp6h = minamtapp6h;
    }

    public BigDecimal getAmtDc1h() {
        return minamtdc1h;
    }
    public void setAmtDc1h(BigDecimal minamtdc1h) {
        this.minamtdc1h = minamtdc1h;
    }

    public BigDecimal getAmtDc3h() {
        return minamtdc3h;
    }
    public void setAmtDc3h(BigDecimal minamtdc3h) {
        this.minamtdc3h = minamtdc3h;
    }

    public BigDecimal getAmtDc6h() {
        return minamtdc6h;
    }
    public void setAmtDc6h(BigDecimal minamtdc6h) {
        this.minamtdc6h = minamtdc6h;
    }

    public BigDecimal getAmtDcFree1h() {
        return minamtdcfree1h;
    }
    public void setAmtDcFree1h(BigDecimal minamtdcfree1h) {
        this.minamtdcfree1h = minamtdcfree1h;
    }

    public BigDecimal getAmtDcFree3h() {
        return minamtdcfree3h;
    }
    public void setAmtDcFree3h(BigDecimal minamtdcfree3h) {
        this.minamtdcfree3h = minamtdcfree3h;
    }

    public BigDecimal getAmtDcFree6h() {
        return minamtdcfree6h;
    }
    public void setAmtDcFree6h(BigDecimal minamtdcfree6h) {
        this.minamtdcfree6h = minamtdcfree6h;
    }

    public BigDecimal getAmtRvsl1h() {
        return minamtrvsl1h;
    }
    public void setAmtRvsl1h(BigDecimal minamtrvsl1h) {
        this.minamtrvsl1h = minamtrvsl1h;
    }

    public BigDecimal getAmtRvsl3h() {
        return minamtrvsl3h;
    }
    public void setAmtRvsl3h(BigDecimal minamtrvsl3h) {
        this.minamtrvsl3h = minamtrvsl3h;
    }

    public BigDecimal getAmtRvsl6h() {
        return minamtrvsl6h;
    }
    public void setAmtRvsl6h(BigDecimal minamtrvsl6h) {
        this.minamtrvsl6h = minamtrvsl6h;
    }

    public BigDecimal getAmtTot1h() {
        return minamttot1h;
    }
    public void setAmtTot1h(BigDecimal minamttot1h) {
        this.minamttot1h = minamttot1h;
    }

    public BigDecimal getAmtTot3h() {
        return minamttot3h;
    }
    public void setAmtTot3h(BigDecimal minamttot3h) {
        this.minamttot3h = minamttot3h;
    }

    public BigDecimal getAmtTot6h() {
        return minamttot6h;
    }
    public void setAmtTot6h(BigDecimal minamttot6h) {
        this.minamttot6h = minamttot6h;
    }

    public BigDecimal getTrxApp1d() {
        return mintrxapp1d;
    }
    public void setTrxApp1d(BigDecimal mintrxapp1d) {
        this.mintrxapp1d = mintrxapp1d;
    }

    public BigDecimal getAmtApp1d() {
        return minamtapp1d;
    }
    public void setAmtApp1d(BigDecimal minamtapp1d) {
        this.minamtapp1d = minamtapp1d;
    }

    public BigDecimal getTrxDc1d() {
        return mintrxdc1d;
    }
    public void setTrxDc1d(BigDecimal mintrxdc1d) {
        this.mintrxdc1d = mintrxdc1d;
    }

    public BigDecimal getAmtDc1d() {
        return minamtdc1d;
    }
    public void setAmtDc1d(BigDecimal minamtdc1d) {
        this.minamtdc1d = minamtdc1d;
    }

    public BigDecimal getTrxDcFree1d() {
        return mintrxdcfree1d;
    }
    public void setTrxDcFree1d(BigDecimal mintrxdcfree1d) {
        this.mintrxdcfree1d = mintrxdcfree1d;
    }

    public BigDecimal getAmtDcFree1d() {
        return minamtdcfree1d;
    }
    public void setAmtDcFree1d(BigDecimal minamtdcfree1d) {
        this.minamtdcfree1d = minamtdcfree1d;
    }

    public BigDecimal getTrxRvsl1d() {
        return mintrxrvsl1d;
    }
    public void setTrxRvsl1d(BigDecimal mintrxrvsl1d) {
        this.mintrxrvsl1d = mintrxrvsl1d;
    }

    public BigDecimal getAmtRvsl1d() {
        return minamtrvsl1d;
    }
    public void setAmtRvsl1d(BigDecimal minamtrvsl1d) {
        this.minamtrvsl1d = minamtrvsl1d;
    }

    public BigDecimal getAmtTot1d() {
        return minamttot1d;
    }
    public void setAmtTot1d(BigDecimal minamttot1d) {
        this.minamttot1d = minamttot1d;
    }

    public BigDecimal getTrxApp3d() {
        return mintrxapp3d;
    }
    public void setTrxApp3d(BigDecimal mintrxapp3d) {
        this.mintrxapp3d = mintrxapp3d;
    }

    public BigDecimal getAmtApp3d() {
        return minamtapp3d;
    }
    public void setAmtApp3d(BigDecimal minamtapp3d) {
        this.minamtapp3d = minamtapp3d;
    }

    public BigDecimal getTrxDc3d() {
        return mintrxdc3d;
    }
    public void setTrxDc3d(BigDecimal mintrxdc3d) {
        this.mintrxdc3d = mintrxdc3d;
    }

    public BigDecimal getAmtDc3d() {
        return minamtdc3d;
    }
    public void setAmtDc3d(BigDecimal minamtdc3d) {
        this.minamtdc3d = minamtdc3d;
    }

    public BigDecimal getTrxDcFree3d() {
        return mintrxdcfree3d;
    }
    public void setTrxDcFree3d(BigDecimal mintrxdcfree3d) {
        this.mintrxdcfree3d = mintrxdcfree3d;
    }

    public BigDecimal getAmtDcFree3d() {
        return minamtdcfree3d;
    }
    public void setAmtDcFree3d(BigDecimal minamtdcfree3d) {
        this.minamtdcfree3d = minamtdcfree3d;
    }

    public BigDecimal getTrxRvsl3d() {
        return mintrxrvsl3d;
    }
    public void setTrxRvsl3d(BigDecimal mintrxrvsl3d) {
        this.mintrxrvsl3d = mintrxrvsl3d;
    }

    public BigDecimal getAmtRvsl3d() {
        return minamtrvsl3d;
    }
    public void setAmtRvsl3d(BigDecimal minamtrvsl3d) {
        this.minamtrvsl3d = minamtrvsl3d;
    }

    public BigDecimal getAmtTot3d() {
        return minamttot3d;
    }
    public void setAmtTot3d(BigDecimal minamttot3d) {
        this.minamttot3d = minamttot3d;
    }

    public BigDecimal getMaxAmt1h() {
        return maxamt1h;
    }
    public void setMaxAmt1h(BigDecimal maxamt1h) {
        this.maxamt1h = maxamt1h;
    }

}

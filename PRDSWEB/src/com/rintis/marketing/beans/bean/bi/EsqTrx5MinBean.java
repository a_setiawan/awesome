package com.rintis.marketing.beans.bean.bi;

import com.rintis.marketing.core.utils.MathUtils;

import java.math.BigDecimal;

public class EsqTrx5MinBean {
    private String bankid;
    private String period;
    private String trxid;
    //private String hourfr;
    //private String hourto;
    private String timefr;
    private String timeto;
    private BigDecimal freq_acq_app;
    private BigDecimal freq_acq_dc ;
    private BigDecimal freq_acq_df ;
    private BigDecimal freq_acq_rvsl;
    private BigDecimal freq_iss_app;
    private BigDecimal freq_iss_dc ;
    private BigDecimal freq_iss_df ;
    private BigDecimal freq_iss_rvsl;
    private BigDecimal freq_bnf_app;
    private BigDecimal freq_bnf_dc ;
    private BigDecimal freq_bnf_df ;
    private BigDecimal freq_bnf_rvsl;
    private BigDecimal freq_acqBnf_app;
    private BigDecimal freq_acqBnf_dc;
    private BigDecimal freq_acqBnf_df;
    private BigDecimal freq_acqBnf_rvsl;
    private BigDecimal freq_acqIss_app;
    private BigDecimal freq_acqIss_dc;
    private BigDecimal freq_acqIss_df;
    private BigDecimal freq_acqIss_rvsl;
    private BigDecimal freq_issBnf_app;
    private BigDecimal freq_issBnf_dc;
    private BigDecimal freq_issBnf_df;
    private BigDecimal freq_issBnf_rvsl;

    private String prevperiod;
    private BigDecimal freq_acq_app_prev;
    private BigDecimal freq_acq_dc_prev ;
    private BigDecimal freq_acq_df_prev ;
    private BigDecimal freq_acq_rvsl_prev;
    private BigDecimal freq_iss_app_prev;
    private BigDecimal freq_iss_dc_prev ;
    private BigDecimal freq_iss_df_prev ;
    private BigDecimal freq_iss_rvsl_prev;
    private BigDecimal freq_bnf_app_prev;
    private BigDecimal freq_bnf_dc_prev ;
    private BigDecimal freq_bnf_df_prev ;
    private BigDecimal freq_bnf_rvsl_prev;
    private BigDecimal freq_acqBnf_app_prev;
    private BigDecimal freq_acqBnf_dc_prev;
    private BigDecimal freq_acqBnf_df_prev;
    private BigDecimal freq_acqBnf_rvsl_prev;
    private BigDecimal freq_acqIss_app_prev;
    private BigDecimal freq_acqIss_dc_prev;
    private BigDecimal freq_acqIss_df_prev;
    private BigDecimal freq_acqIss_rvsl_prev;
    private BigDecimal freq_issBnf_app_prev;
    private BigDecimal freq_issBnf_dc_prev;
    private BigDecimal freq_issBnf_df_prev;
    private BigDecimal freq_issBnf_rvsl_prev;

    private BigDecimal freq_app;
    private BigDecimal freq_dc;
    private BigDecimal freq_df;

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getTrxid() {
        return trxid;
    }

    public void setTrxid(String trxid) {
        this.trxid = trxid;
    }

    /*public String getHourfr() {
        return hourfr;
    }

    public void setHourfr(String hourfr) {
        this.hourfr = hourfr;
    }

    public String getHourto() {
        return hourto;
    }

    public void setHourto(String hourto) {
        this.hourto = hourto;
    }*/

    public String getTimefr() {
        return timefr;
    }

    public void setTimefr(String timefr) {
        this.timefr = timefr;
    }

    public String getTimeto() {
        return timeto;
    }

    public void setTimeto(String timeto) {
        this.timeto = timeto;
    }

    public BigDecimal getFreq_acq_app() {
        return MathUtils.checkNull(freq_acq_app);
    }

    public void setFreq_acq_app(BigDecimal freq_acq_app) {
        this.freq_acq_app = freq_acq_app;
    }

    public BigDecimal getFreq_acq_dc() {
        return MathUtils.checkNull(freq_acq_dc);
    }

    public void setFreq_acq_dc(BigDecimal freq_acq_dc) {
        this.freq_acq_dc = freq_acq_dc;
    }

    public BigDecimal getFreq_acq_df() {
        return MathUtils.checkNull(freq_acq_df);
    }

    public void setFreq_acq_df(BigDecimal freq_acq_df) {
        this.freq_acq_df = freq_acq_df;
    }

    public BigDecimal getFreq_acq_rvsl() {
        return MathUtils.checkNull(freq_acq_rvsl);
    }

    public void setFreq_acq_rvsl(BigDecimal freq_acq_rvsl) {
        this.freq_acq_rvsl = freq_acq_rvsl;
    }

    public BigDecimal getFreq_iss_app() {
        return MathUtils.checkNull(freq_iss_app);
    }

    public void setFreq_iss_app(BigDecimal freq_iss_app) {
        this.freq_iss_app = freq_iss_app;
    }

    public BigDecimal getFreq_iss_dc() {
        return MathUtils.checkNull(freq_iss_dc);
    }

    public void setFreq_iss_dc(BigDecimal freq_iss_dc) {
        this.freq_iss_dc = freq_iss_dc;
    }

    public BigDecimal getFreq_iss_df() {
        return MathUtils.checkNull(freq_iss_df);
    }

    public void setFreq_iss_df(BigDecimal freq_iss_df) {
        this.freq_iss_df = freq_iss_df;
    }

    public BigDecimal getFreq_iss_rvsl() {
        return MathUtils.checkNull(freq_iss_rvsl);
    }

    public void setFreq_iss_rvsl(BigDecimal freq_iss_rvsl) {
        this.freq_iss_rvsl = freq_iss_rvsl;
    }

    public BigDecimal getFreq_bnf_app() {
        return MathUtils.checkNull(freq_bnf_app);
    }

    public void setFreq_bnf_app(BigDecimal freq_bnf_app) {
        this.freq_bnf_app = freq_bnf_app;
    }

    public BigDecimal getFreq_bnf_dc() {
        return MathUtils.checkNull(freq_bnf_dc);
    }

    public void setFreq_bnf_dc(BigDecimal freq_bnf_dc) {
        this.freq_bnf_dc = freq_bnf_dc;
    }

    public BigDecimal getFreq_bnf_df() {
        return MathUtils.checkNull(freq_bnf_df);
    }

    public void setFreq_bnf_df(BigDecimal freq_bnf_df) {
        this.freq_bnf_df = freq_bnf_df;
    }

    public BigDecimal getFreq_bnf_rvsl() {
        return MathUtils.checkNull(freq_bnf_rvsl);
    }

    public void setFreq_bnf_rvsl(BigDecimal freq_bnf_rvsl) {
        this.freq_bnf_rvsl = freq_bnf_rvsl;
    }

    public BigDecimal getFreq_acqBnf_app() {
        return MathUtils.checkNull(freq_acqBnf_app);
    }

    public void setFreq_acqBnf_app(BigDecimal freq_acqBnf_app) {
        this.freq_acqBnf_app = freq_acqBnf_app;
    }

    public BigDecimal getFreq_acqBnf_dc() {
        return MathUtils.checkNull(freq_acqBnf_dc);
    }

    public void setFreq_acqBnf_dc(BigDecimal freq_acqBnf_dc) {
        this.freq_acqBnf_dc = freq_acqBnf_dc;
    }

    public BigDecimal getFreq_acqBnf_df() {
        return MathUtils.checkNull(freq_acqBnf_df);
    }

    public void setFreq_acqBnf_df(BigDecimal freq_acqBnf_df) {
        this.freq_acqBnf_df = freq_acqBnf_df;
    }

    public BigDecimal getFreq_acqBnf_rvsl() {
        return MathUtils.checkNull(freq_acqBnf_rvsl);
    }

    public void setFreq_acqBnf_rvsl(BigDecimal freq_acqBnf_rvsl) {
        this.freq_acqBnf_rvsl = freq_acqBnf_rvsl;
    }

    public BigDecimal getFreq_acqIss_app() {
        return MathUtils.checkNull(freq_acqIss_app);
    }

    public void setFreq_acqIss_app(BigDecimal freq_acqIss_app) {
        this.freq_acqIss_app = freq_acqIss_app;
    }

    public BigDecimal getFreq_acqIss_dc() {
        return MathUtils.checkNull(freq_acqIss_dc);
    }

    public void setFreq_acqIss_dc(BigDecimal freq_acqIss_dc) {
        this.freq_acqIss_dc = freq_acqIss_dc;
    }

    public BigDecimal getFreq_acqIss_df() {
        return MathUtils.checkNull(freq_acqIss_df);
    }

    public void setFreq_acqIss_df(BigDecimal freq_acqIss_df) {
        this.freq_acqIss_df = freq_acqIss_df;
    }

    public BigDecimal getFreq_acqIss_rvsl() {
        return MathUtils.checkNull(freq_acqIss_rvsl);
    }

    public void setFreq_acqIss_rvsl(BigDecimal freq_acqIss_rvsl) {
        this.freq_acqIss_rvsl = freq_acqIss_rvsl;
    }

    public BigDecimal getFreq_issBnf_app() {
        return MathUtils.checkNull(freq_issBnf_app);
    }

    public void setFreq_issBnf_app(BigDecimal freq_issBnf_app) {
        this.freq_issBnf_app = freq_issBnf_app;
    }

    public BigDecimal getFreq_issBnf_dc() {
        return MathUtils.checkNull(freq_issBnf_dc);
    }

    public void setFreq_issBnf_dc(BigDecimal freq_issBnf_dc) {
        this.freq_issBnf_dc = freq_issBnf_dc;
    }

    public BigDecimal getFreq_issBnf_df() {
        return MathUtils.checkNull(freq_issBnf_df);
    }

    public void setFreq_issBnf_df(BigDecimal freq_issBnf_df) {
        this.freq_issBnf_df = freq_issBnf_df;
    }

    public BigDecimal getFreq_issBnf_rvsl() {
        return MathUtils.checkNull(freq_issBnf_rvsl);
    }

    public void setFreq_issBnf_rvsl(BigDecimal freq_issBnf_rvsl) {
        this.freq_issBnf_rvsl = freq_issBnf_rvsl;
    }

    public String getPrevperiod() {
        return prevperiod;
    }

    public void setPrevperiod(String prevperiod) {
        this.prevperiod = prevperiod;
    }

    public BigDecimal getFreq_acq_app_prev() {
        return freq_acq_app_prev;
    }

    public void setFreq_acq_app_prev(BigDecimal freq_acq_app_prev) {
        this.freq_acq_app_prev = freq_acq_app_prev;
    }

    public BigDecimal getFreq_acq_dc_prev() {
        return freq_acq_dc_prev;
    }

    public void setFreq_acq_dc_prev(BigDecimal freq_acq_dc_prev) {
        this.freq_acq_dc_prev = freq_acq_dc_prev;
    }

    public BigDecimal getFreq_acq_df_prev() {
        return freq_acq_df_prev;
    }

    public void setFreq_acq_df_prev(BigDecimal freq_acq_df_prev) {
        this.freq_acq_df_prev = freq_acq_df_prev;
    }

    public BigDecimal getFreq_acq_rvsl_prev() {
        return freq_acq_rvsl_prev;
    }

    public void setFreq_acq_rvsl_prev(BigDecimal freq_acq_rvsl_prev) {
        this.freq_acq_rvsl_prev = freq_acq_rvsl_prev;
    }

    public BigDecimal getFreq_iss_app_prev() {
        return freq_iss_app_prev;
    }

    public void setFreq_iss_app_prev(BigDecimal freq_iss_app_prev) {
        this.freq_iss_app_prev = freq_iss_app_prev;
    }

    public BigDecimal getFreq_iss_dc_prev() {
        return freq_iss_dc_prev;
    }

    public void setFreq_iss_dc_prev(BigDecimal freq_iss_dc_prev) {
        this.freq_iss_dc_prev = freq_iss_dc_prev;
    }

    public BigDecimal getFreq_iss_df_prev() {
        return freq_iss_df_prev;
    }

    public void setFreq_iss_df_prev(BigDecimal freq_iss_df_prev) {
        this.freq_iss_df_prev = freq_iss_df_prev;
    }

    public BigDecimal getFreq_iss_rvsl_prev() {
        return freq_iss_rvsl_prev;
    }

    public void setFreq_iss_rvsl_prev(BigDecimal freq_iss_rvsl_prev) {
        this.freq_iss_rvsl_prev = freq_iss_rvsl_prev;
    }

    public BigDecimal getFreq_bnf_app_prev() {
        return freq_bnf_app_prev;
    }

    public void setFreq_bnf_app_prev(BigDecimal freq_bnf_app_prev) {
        this.freq_bnf_app_prev = freq_bnf_app_prev;
    }

    public BigDecimal getFreq_bnf_dc_prev() {
        return freq_bnf_dc_prev;
    }

    public void setFreq_bnf_dc_prev(BigDecimal freq_bnf_dc_prev) {
        this.freq_bnf_dc_prev = freq_bnf_dc_prev;
    }

    public BigDecimal getFreq_bnf_df_prev() {
        return freq_bnf_df_prev;
    }

    public void setFreq_bnf_df_prev(BigDecimal freq_bnf_df_prev) {
        this.freq_bnf_df_prev = freq_bnf_df_prev;
    }

    public BigDecimal getFreq_bnf_rvsl_prev() {
        return freq_bnf_rvsl_prev;
    }

    public void setFreq_bnf_rvsl_prev(BigDecimal freq_bnf_rvsl_prev) {
        this.freq_bnf_rvsl_prev = freq_bnf_rvsl_prev;
    }

    public BigDecimal getFreq_acqBnf_app_prev() {
        return freq_acqBnf_app_prev;
    }

    public void setFreq_acqBnf_app_prev(BigDecimal freq_acqBnf_app_prev) {
        this.freq_acqBnf_app_prev = freq_acqBnf_app_prev;
    }

    public BigDecimal getFreq_acqBnf_dc_prev() {
        return freq_acqBnf_dc_prev;
    }

    public void setFreq_acqBnf_dc_prev(BigDecimal freq_acqBnf_dc_prev) {
        this.freq_acqBnf_dc_prev = freq_acqBnf_dc_prev;
    }

    public BigDecimal getFreq_acqBnf_df_prev() {
        return freq_acqBnf_df_prev;
    }

    public void setFreq_acqBnf_df_prev(BigDecimal freq_acqBnf_df_prev) {
        this.freq_acqBnf_df_prev = freq_acqBnf_df_prev;
    }

    public BigDecimal getFreq_acqBnf_rvsl_prev() {
        return freq_acqBnf_rvsl_prev;
    }

    public void setFreq_acqBnf_rvsl_prev(BigDecimal freq_acqBnf_rvsl_prev) {
        this.freq_acqBnf_rvsl_prev = freq_acqBnf_rvsl_prev;
    }

    public BigDecimal getFreq_acqIss_app_prev() {
        return freq_acqIss_app_prev;
    }

    public void setFreq_acqIss_app_prev(BigDecimal freq_acqIss_app_prev) {
        this.freq_acqIss_app_prev = freq_acqIss_app_prev;
    }

    public BigDecimal getFreq_acqIss_dc_prev() {
        return freq_acqIss_dc_prev;
    }

    public void setFreq_acqIss_dc_prev(BigDecimal freq_acqIss_dc_prev) {
        this.freq_acqIss_dc_prev = freq_acqIss_dc_prev;
    }

    public BigDecimal getFreq_acqIss_df_prev() {
        return freq_acqIss_df_prev;
    }

    public void setFreq_acqIss_df_prev(BigDecimal freq_acqIss_df_prev) {
        this.freq_acqIss_df_prev = freq_acqIss_df_prev;
    }

    public BigDecimal getFreq_acqIss_rvsl_prev() {
        return freq_acqIss_rvsl_prev;
    }

    public void setFreq_acqIss_rvsl_prev(BigDecimal freq_acqIss_rvsl_prev) {
        this.freq_acqIss_rvsl_prev = freq_acqIss_rvsl_prev;
    }

    public BigDecimal getFreq_issBnf_app_prev() {
        return freq_issBnf_app_prev;
    }

    public void setFreq_issBnf_app_prev(BigDecimal freq_issBnf_app_prev) {
        this.freq_issBnf_app_prev = freq_issBnf_app_prev;
    }

    public BigDecimal getFreq_issBnf_dc_prev() {
        return freq_issBnf_dc_prev;
    }

    public void setFreq_issBnf_dc_prev(BigDecimal freq_issBnf_dc_prev) {
        this.freq_issBnf_dc_prev = freq_issBnf_dc_prev;
    }

    public BigDecimal getFreq_issBnf_df_prev() {
        return freq_issBnf_df_prev;
    }

    public void setFreq_issBnf_df_prev(BigDecimal freq_issBnf_df_prev) {
        this.freq_issBnf_df_prev = freq_issBnf_df_prev;
    }

    public BigDecimal getFreq_issBnf_rvsl_prev() {
        return freq_issBnf_rvsl_prev;
    }

    public void setFreq_issBnf_rvsl_prev(BigDecimal freq_issBnf_rvsl_prev) {
        this.freq_issBnf_rvsl_prev = freq_issBnf_rvsl_prev;
    }

    public BigDecimal getFreq_app() {
        return freq_app;
    }

    public void setFreq_app(BigDecimal freq_app) {
        this.freq_app = freq_app;
    }

    public BigDecimal getFreq_dc() {
        return freq_dc;
    }

    public void setFreq_dc(BigDecimal freq_dc) {
        this.freq_dc = freq_dc;
    }

    public BigDecimal getFreq_df() {
        return freq_df;
    }

    public void setFreq_df(BigDecimal freq_df) {
        this.freq_df = freq_df;
    }
}

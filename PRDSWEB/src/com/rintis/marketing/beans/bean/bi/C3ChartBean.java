package com.rintis.marketing.beans.bean.bi;

import com.martinlinha.c3faces.script.property.Axis;
import com.martinlinha.c3faces.script.property.Data;
import com.martinlinha.c3faces.script.property.Padding;
import com.martinlinha.c3faces.script.property.Point;

public class C3ChartBean {
    private Data data;
    private Axis axis;
    private Point point;
    private String title;
    private Padding padding;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Axis getAxis() {
        return axis;
    }

    public void setAxis(Axis axis) {
        this.axis = axis;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public Padding getPadding() {
        return padding;
    }

    public void setPadding(Padding padding) {
        this.padding = padding;
    }
}

package com.rintis.marketing.beans.bean.bi;

import com.rintis.marketing.core.constant.DataConstant;

import java.math.BigDecimal;

public class UserDashboardDetailBean {
    private BigDecimal id;
    private BigDecimal seq;
    private String bankid;
    private String transactionid;
    private String transactionindicatorid;
    private BigDecimal sortidx;
    private String monitoringinterval;
    private String monitoringintervalname;
    private String bankname;
    private String transactionname;
    private String transactionindicatorname;

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getSeq() {
        return seq;
    }

    public void setSeq(BigDecimal seq) {
        this.seq = seq;
    }

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public String getTransactionindicatorid() {
        return transactionindicatorid;
    }

    public void setTransactionindicatorid(String transactionindicatorid) {
        this.transactionindicatorid = transactionindicatorid;
    }

    public BigDecimal getSortidx() {
        return sortidx;
    }

    public void setSortidx(BigDecimal sortidx) {
        this.sortidx = sortidx;
    }

    public String getMonitoringinterval() {
        return monitoringinterval;
    }

    public void setMonitoringinterval(String monitoringinterval) {
        this.monitoringinterval = monitoringinterval;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getTransactionname() {
        return transactionname;
    }

    public void setTransactionname(String transactionname) {
        this.transactionname = transactionname;
    }

    public String getTransactionindicatorname() {
        return transactionindicatorname;
    }

    public void setTransactionindicatorname(String transactionindicatorname) {
        this.transactionindicatorname = transactionindicatorname;
    }

    public String getMonitoringintervalname() {
        return DataConstant.getTimeFrameName(monitoringinterval);
    }

    public void setMonitoringintervalname(String monitoringintervalname) {
        this.monitoringintervalname = monitoringintervalname;
    }
}

package com.rintis.marketing.beans.bean.bi;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by aabraham on 30/01/2019.
 */
public class PanAnomalyBean implements Serializable {
    private String period;
    private String perioddisplay;
    private String timefr;
    private String timeto;
    private String trxtype;
    private String trantype;
    private String pan;
    private String issuerbank;
    private String pengirim;
    private BigDecimal freqapp;
    private BigDecimal freqdc;
    private BigDecimal freqdcfree;
    private BigDecimal freqrvsl;
    private BigDecimal totalamtapp;
    private BigDecimal totalamtdc;
    private BigDecimal totalamtdcfree;
    private BigDecimal totalamtrvsl;
    private BigDecimal totalamt;
    private String chk;
    private String chkcolor;
    private String messagetype;
    private String responsecode;
    private String atmseqnum;
    private String posinvoicenum;
    private BigDecimal amount;
    private String description;
    private String toacct;
    private String beneficiarybank;
    private String beneficiaryname;
    private String pcode;
    private String tracenum;
    private String acquirerbank;
    private String termid;
    private String termownername;
    private String posentrymode;
    private String status;
    /*Add by aaw, 2/6/21, fixing in search beneficiary name in header*/
    private String beneficiarynameTrim;
    /*End add*/

    /*Add by aaw 13/07/2021, fixing in detail beneficiary for 3D*/
    private String periodFrom;
    private String periodTo;
    /*End add*/

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getPeriod() {
        return period;
    }
    public void setPeriod(String period) {
        this.period = period;
    }
    public String getPeriodDisplay() {
        return perioddisplay;
    }
    public void setPeriodDisplay(String perioddisplay) {
        this.perioddisplay = perioddisplay;
    }
    public String getTimeFr() {
        return timefr;
    }
    public void setTimeFr(String timefr) {
        this.timefr = timefr;
    }
    public String getTimeTo() {
        return timeto;
    }
    public void setTimeTo(String timeto) {
        this.timeto = timeto;
    }
    public String getTrxType() {
        return trxtype;
    }
    public void setTrxType(String trx_type) {
        this.trxtype = trx_type;
    }
    public String getTranType() {
        return trantype;
    }
    public void setTranType(String tran_type) {
        this.trantype = tran_type;
    }
    public String getPan() {
        return pan;
    }
    public void setPan(String pan) {
        this.pan = pan;
    }
    public String getIssuerBank() {
        return issuerbank;
    }
    public void setIssuerBank(String issuer_bank) {
            this.issuerbank = issuer_bank;
    }
    public String getPengirim() {
        return pengirim;
    }
    public void setPengirim(String pengirim) {
        this.pengirim = pengirim;
    }
    public BigDecimal getFreqApp() {
        return freqapp;
    }
    public void setFreqApp(BigDecimal freq_app) {
        this.freqapp = freq_app;
    }
    public BigDecimal getFreqDc() {
        return freqdc;
    }
    public void setFreqDc(BigDecimal freq_dc) {
        this.freqdc = freq_dc;
    }
    public BigDecimal getFreqDcFree() {
        return freqdcfree;
    }
    public void setFreqDcFree(BigDecimal freq_dc_free) {
        this.freqdcfree = freq_dc_free;
    }
    public BigDecimal getFreqRvsl() {
        return freqrvsl;
    }
    public void setFreqRvsl(BigDecimal freq_rvsl) {
        this.freqrvsl = freq_rvsl;
    }
    public BigDecimal getAmtApp() {
        return totalamtapp;
    }
    public void setAmtApp(BigDecimal totalamtapp) {
        this.totalamtapp = totalamtapp;
    }
    public BigDecimal getAmtDc() {
        return totalamtdc;
    }
    public void setAmtDc(BigDecimal totalamtdc) {
        this.totalamtdc = totalamtdc;
    }
    public BigDecimal getAmtDcFree() {
        return totalamtdcfree;
    }
    public void setAmtDcFree(BigDecimal totalamtdcfree) {
        this.totalamtdcfree = totalamtdcfree;
    }
    public BigDecimal getAmtRvsl() {
        return totalamtrvsl;
    }
    public void setAmtRvsl(BigDecimal totalamtrvsl) {
        this.totalamtrvsl = totalamtrvsl;
    }
    public BigDecimal getAmt() {
        return totalamt;
    }
    public void setAmt(BigDecimal totalamt) {
        this.totalamt = totalamt;
    }
    public String getChk() {
        return chk;
    }
    public void setChk(String chk) {
        this.chk = chk;
    }
    public String getChkColor() {
        return chkcolor;
    }
    public void setChkColor(String chkcolor) {
        this.chkcolor = chkcolor;
    }
    public String getMessageType() {
        return messagetype;
    }
    public void setMessageType(String messagetype) {
        this.messagetype = messagetype;
    }
    public String getResponseCode() {
        return responsecode;
    }
    public void setResponseCode(String responsecode) {
        this.responsecode = responsecode;
    }
    public String getAtmSeqNum() {
        return atmseqnum;
    }
    public void setAtmSeqNum(String atmseqnum) {
        this.atmseqnum = atmseqnum;
    }
    public String getPosInvoiceNum() {
        return posinvoicenum;
    }
    public void setPosInvoiceNum(String posinvoicenum) {
        this.posinvoicenum = posinvoicenum;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getToAcct() {
        return toacct;
    }
    public void setToAcct(String toacct) {
        this.toacct = toacct;
    }
    public String getBeneficiaryBank() {
        return beneficiarybank;
    }
    public void setBeneficiaryBank(String beneficiarybank) {
        this.beneficiarybank = beneficiarybank;
    }
    public String getBeneficiaryName() {
        return beneficiaryname;
    }
    public void setBeneficiaryName(String beneficiaryname) {
        this.beneficiaryname = beneficiaryname;
    }
    public String getPcode() {
        return pcode;
    }
    public void setPcode(String pcode) {
        this.pcode = pcode;
    }
    public String getTraceNum() {
        return tracenum;
    }
    public void setTraceNum(String tracenum) {
        this.tracenum = tracenum;
    }
    public String getAcquirerBank() {
        return acquirerbank;
    }
    public void setAcquirerBank(String acquirerbank) {
        this.acquirerbank = acquirerbank;
    }
    public String getTermId() {
        return termid;
    }
    public void setTermId(String termid) {
        this.termid = termid;
    }
    public String getTermOwnerName() {
        return termownername;
    }
    public void setTermOwnerName(String termownername) {
        this.termownername = termownername;
    }
    public String getPosEntryMode() {
        return posentrymode;
    }
    public void setPosEntryMode(String posentrymode) {
        this.posentrymode = posentrymode;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    /*Add by aaw, 2/6/21, setter getter*/
    public String getBeneficiarynameTrim() {
        return new String(beneficiaryname).trim().replaceAll("\\s{2,}", " ");
    }
    public void setBeneficiarynameTrim(String beneficiarynameTrim) {
        this.beneficiarynameTrim = beneficiarynameTrim;
    }
    /*End add*/

    public String getPeriodFrom() {
        return periodFrom;
    }

    public void setPeriodFrom(String periodFrom) {
        this.periodFrom = periodFrom;
    }

    public String getPeriodTo() {
        return periodTo;
    }

    public void setPeriodTo(String periodTo) {
        this.periodTo = periodTo;
    }
}

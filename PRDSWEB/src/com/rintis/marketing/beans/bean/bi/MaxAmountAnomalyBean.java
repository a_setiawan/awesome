package com.rintis.marketing.beans.bean.bi;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by aabraham on 31/01/2020.
 */
public class MaxAmountAnomalyBean implements Serializable {
    private String period;
    private String perioddisplay;
    private String time;
    private String trxtype;
    private String trantype;
    private String pan;
    private String issuerbank;
    private String pengirim;
    private String messagetype;
    private String responsecode;
    private String atmseqnum;
    private String posinvoicenum;
    private BigDecimal amount;
    private String description;

    public String getPeriod() {
        return period;
    }
    public void setPeriod(String period) {
        this.period = period;
    }
    public String getPeriodDisplay() {
        return perioddisplay;
    }
    public void setPeriodDisplay(String perioddisplay) {
        this.perioddisplay = perioddisplay;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public String getTrxType() {
        return trxtype;
    }
    public void setTrxType(String trx_type) {
        this.trxtype = trx_type;
    }
    public String getTranType() {
        return trantype;
    }
    public void setTranType(String tran_type) {
        this.trantype = tran_type;
    }
    public String getPan() {
        return pan;
    }
    public void setPan(String pan) {
        this.pan = pan;
    }
    public String getIssuerBank() {
        return issuerbank;
    }
    public void setIssuerBank(String issuer_bank) {
        this.issuerbank = issuer_bank;
    }
    public String getPengirim() {
        return pengirim;
    }
    public void setPengirim(String pengirim) {
        this.pengirim = pengirim;
    }
    public String getMessageType() {
        return messagetype;
    }
    public void setMessageType(String messagetype) {
        this.messagetype = messagetype;
    }
    public String getResponseCode() {
        return responsecode;
    }
    public void setResponseCode(String responsecode) {
        this.responsecode = responsecode;
    }
    public String getAtmSeqNum() {
        return atmseqnum;
    }
    public void setAtmSeqNum(String atmseqnum) {
        this.atmseqnum = atmseqnum;
    }
    public String getPosInvoiceNum() {
        return posinvoicenum;
    }
    public void setPosInvoiceNum(String posinvoicenum) {
        this.posinvoicenum = posinvoicenum;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}

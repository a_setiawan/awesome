package com.rintis.marketing.beans.bean.bi;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by aaw on 30/06/2021.
 */

public class DetailMPanAnomalyBean implements Serializable {
    private String period;
    private String trxtime;
    private String terminal;
    private String termid;
    private BigDecimal app;
    private BigDecimal dc;
    private BigDecimal dcfree;
    private BigDecimal rvsl;
    private BigDecimal amount;
    private String rc;
    private String msgtype;
    private String posentrymode;
    private String acquirerBank;
    private String acquirerId;
    private String description;
    private String pan;
    private String qrType;
    private String cardNo;
    private String issuerBank;

    public String getPosentrymode() {
        return posentrymode;
    }
    public void setPosentrymode(String posentrymode) {
        this.posentrymode = posentrymode;
    }
    public String getPeriod() {
        return period;
    }
    public void setPeriod(String period) {
        this.period = period;
    }
    public String getTrxTime() {
        return trxtime;
    }
    public void setTrxTime(String trxtime) {
        this.trxtime = trxtime;
    }
    public String getTerminal() {
        return terminal;
    }
    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }
    public String getTermId() {
        return termid;
    }
    public void setTermId(String termid) {
        this.termid = termid;
    }
    public BigDecimal getApp() {
        return app;
    }
    public void setApp(BigDecimal app) {
        this.app = app;
    }
    public BigDecimal getDc() {
        return dc;
    }
    public void setDc(BigDecimal dc) {
        this.dc = dc;
    }
    public BigDecimal getDcFree() {
        return dcfree;
    }
    public void setDcFree(BigDecimal dcfree) {
        this.dcfree = dcfree;
    }
    public BigDecimal getRvsl() {
        return rvsl;
    }
    public void setRvsl(BigDecimal rvsl) {
        this.rvsl = rvsl;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public String getRc() {
        return rc;
    }
    public void setRc(String rc) {
        this.rc = rc;
    }
    public String getMsgtype() {
        return msgtype;
    }
    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getPan() {
        return pan;
    }
    public void setPan(String pan) {
        this.pan = pan;
    }
    public String getTrxtime() {
        return trxtime;
    }
    public void setTrxtime(String trxtime) {
        this.trxtime = trxtime;
    }
    public String getTermid() {
        return termid;
    }
    public void setTermid(String termid) {
        this.termid = termid;
    }
    public String getAcquirerBank() {
        return acquirerBank;
    }
    public void setAcquirerBank(String acquirerBank) {
        this.acquirerBank = acquirerBank;
    }
    public String getAcquirerId() {
        return acquirerId;
    }
    public void setAcquirerId(String acquirerId) {
        this.acquirerId = acquirerId;
    }
    public BigDecimal getDcfree() {
        return dcfree;
    }
    public void setDcfree(BigDecimal dcfree) {
        this.dcfree = dcfree;
    }
    public String getQrType() {
        return qrType;
    }
    public void setQrType(String qrType) {
        this.qrType = qrType;
    }
    public String getCardNo() {
        return cardNo;
    }
    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }
    public String getIssuerBank() {
        return issuerBank;
    }
    public void setIssuerBank(String issuerBank) {
        this.issuerBank = issuerBank;
    }

    public String toString() {
        return "DetailMPANAnomalyBean{" +
                "period='" + period + '\'' +
                ", trxtime='" + trxtime + '\'' +
                ", terminal='" + terminal + '\'' +
                ", termid='" + termid + '\'' +
                ", app=" + app +
                ", dc=" + dc +
                ", dcfree=" + dcfree +
                ", rvsl=" + rvsl +
                ", amount=" + amount +
                ", rc='" + rc + '\'' +
                ", msgtype='" + msgtype + '\'' +
                ", posentrymode='" + posentrymode + '\'' +
                ", acquirerId='" + acquirerId + '\'' +
                ", acquirerBank='" + acquirerBank + '\'' +
                ", description='" + description + '\'' +
                ", pan='" + pan + '\'' +
                ", qrType='" + qrType + '\'' +
                ", cardNo='" + cardNo + '\'' +
                ", issuerBank='" + issuerBank + '\'' +
                '}';
    }
}

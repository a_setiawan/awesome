package com.rintis.marketing.beans.bean.bi;

public class DistinctAcqIssBnfBean {
    private String acqid;
    private String issid;
    private String bnfid;
    private String trxid;

    public String getAcqid() {
        return acqid;
    }

    public void setAcqid(String acqid) {
        this.acqid = acqid;
    }

    public String getIssid() {
        return issid;
    }

    public void setIssid(String issid) {
        this.issid = issid;
    }

    public String getBnfid() {
        return bnfid;
    }

    public void setBnfid(String bnfid) {
        this.bnfid = bnfid;
    }

    public String getTrxid() {
        return trxid;
    }

    public void setTrxid(String trxid) {
        this.trxid = trxid;
    }
}

package com.rintis.marketing.beans.bean.master;

import java.math.BigDecimal;

/**
 * Created by aabraham on 22/04/2021.
 */
public class MCCParameterBean {
    private String mcc;
    private BigDecimal min_trx_app_1d;
    private BigDecimal min_trx_tot_1d;
    private BigDecimal min_amt_app_1d;
    private BigDecimal min_amt_tot_1d;

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public BigDecimal getMin_trx_app_1d() {
        return min_trx_app_1d;
    }

    public void setMin_trx_app_1d(BigDecimal min_trx_app_1d) {
        this.min_trx_app_1d = min_trx_app_1d;
    }

    public BigDecimal getMin_trx_tot_1d() {
        return min_trx_tot_1d;
    }

    public void setMin_trx_tot_1d(BigDecimal min_trx_tot_1d) {
        this.min_trx_tot_1d = min_trx_tot_1d;
    }

    public BigDecimal getMin_amt_app_1d() {
        return min_amt_app_1d;
    }

    public void setMin_amt_app_1d(BigDecimal min_amt_app_1d) {
        this.min_amt_app_1d = min_amt_app_1d;
    }

    public BigDecimal getMin_amt_tot_1d() {
        return min_amt_tot_1d;
    }

    public void setMin_amt_tot_1d(BigDecimal min_amt_tot_1d) {
        this.min_amt_tot_1d = min_amt_tot_1d;
    }
}

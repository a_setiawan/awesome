package com.rintis.marketing.beans.bean.master;

/**
 * Created by aabraham on 19/04/2021.
 */
public class MCCBean {
    private String mcc;
    private String description;

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

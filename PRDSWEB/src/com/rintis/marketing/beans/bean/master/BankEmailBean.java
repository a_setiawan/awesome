package com.rintis.marketing.beans.bean.master;

import java.io.Serializable;

public class BankEmailBean implements Serializable {
    private String bankid;
    private String bankname;
    private String email;
    private String alertMccVelocity;
    private String alertSeqVelocity;
    /*Add by aaw 02/07/2021*/
    private String alertMpanAcq;
    /*End add*/

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlertMccVelocity() {
        return alertMccVelocity;
    }

    public void setAlertMccVelocity(String alertMccVelocity) {
        this.alertMccVelocity = alertMccVelocity;
    }

    public String getAlertSeqVelocity() {
        return alertSeqVelocity;
    }

    public void setAlertSeqVelocity(String alertSeqVelocity) {
        this.alertSeqVelocity = alertSeqVelocity;
    }

    public String getAlertMpanAcq() {
        return alertMpanAcq;
    }

    public void setAlertMpanAcq(String alertMpanAcq) {
        this.alertMpanAcq = alertMpanAcq;
    }
}

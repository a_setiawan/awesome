package com.rintis.marketing.beans.bean.master;

/**
 * Created by erichie on 09/11/2019.
 */
public class BankBean {

    private String bankID;
    private String bankname;

    public String getBankID() {
        return bankID;
    }

    public void setBankID(String bankID) {
        this.bankID = bankID;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

}

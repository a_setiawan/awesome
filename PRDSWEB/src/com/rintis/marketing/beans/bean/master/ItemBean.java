package com.rintis.marketing.beans.bean.master;

public class ItemBean {
    private String id;
    private String name;
    private boolean check;
    private String operator;
    private boolean iss;
    private boolean acq;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public boolean isIss() {
        return iss;
    }

    public void setIss(boolean iss) {
        this.iss = iss;
    }

    public boolean isAcq() {
        return acq;
    }

    public void setAcq(boolean acq) {
        this.acq = acq;
    }
}

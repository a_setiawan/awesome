package com.rintis.marketing.beans.bean.master;

import java.io.Serializable;
import java.math.BigDecimal;

public class BinBean implements Serializable {
    private String bankId;
    private String bankName;
    private String binNo;
    private BigDecimal binLength;

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }

    public BigDecimal getBinLength() {
        return binLength;
    }

    public void setBinLength(BigDecimal binLength) {
        this.binLength = binLength;
    }
}

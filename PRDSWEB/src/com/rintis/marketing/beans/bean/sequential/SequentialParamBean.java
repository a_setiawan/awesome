package com.rintis.marketing.beans.bean.sequential;

import java.math.BigDecimal;

/**
 * Created by aabraham on 20/04/2021.
 */
public class SequentialParamBean {
    private String trxid;
    private String trx;
    private BigDecimal mintrxapp1d;
    private BigDecimal minamtapp1d;
    private BigDecimal mintrxtot1d;
    private BigDecimal minamttot1d;

    public String getTrx() {
        return trx;
    }

    public void setTrx(String trx) {
        this.trx = trx;
    }

    public String getTrxid() {
        return trxid;
    }

    public void setTrxid(String trxid) {
        this.trxid = trxid;
    }

    public BigDecimal getMintrxapp1d() {
        return mintrxapp1d;
    }

    public void setMintrxapp1d(BigDecimal mintrxapp1d) {
        this.mintrxapp1d = mintrxapp1d;
    }

    public BigDecimal getMinamtapp1d() {
        return minamtapp1d;
    }

    public void setMinamtapp1d(BigDecimal minamtapp1d) {
        this.minamtapp1d = minamtapp1d;
    }

    public BigDecimal getMintrxtot1d() {
        return mintrxtot1d;
    }

    public void setMintrxtot1d(BigDecimal mintrxtot1d) {
        this.mintrxtot1d = mintrxtot1d;
    }

    public BigDecimal getMinamttot1d() {
        return minamttot1d;
    }

    public void setMinamttot1d(BigDecimal minamttot1d) {
        this.minamttot1d = minamttot1d;
    }
}

package com.rintis.marketing.beans.bean.pan;

import com.rintis.marketing.beans.entity.AbstractLastUpdatedCreated;
import com.rintis.marketing.beans.entity.EsqTrxid;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

/**
 * Created by aabraham on 30/01/2019.
 */
@Entity
@Table(name = "tms_trxanmly3hour")
public class PanAnomaly3H extends AbstractLastUpdatedCreated {
    @Column(name = "period")
    private String period;
    @Column(name = "time_fr_3h")
    private String time_fr_3h;
    @Column(name = "time_to_3h")
    private String time_to_3h;
    @Column(name = "trx_type")
    private String trx_type;
    @Column(name = "tran_type")
    private String tran_type;
    @Column(name = "pan")
    private String pan;
    @Column(name = "issuer_bank")
    private String issuer_bank;
    @Column(name = "pengirim")
    private String pengirim;
    @Column(name = "freq_app")
    private Integer freq_app;
    @Column(name = "freq_dc")
    private Integer freq_dc;
    @Column(name = "freq_rvsl")
    private Integer freq_rvsl;
    @Column(name = "total_amt_app")
    private Integer total_amt_app;
    @Column(name = "total_amt_dc")
    private Integer total_amt_dc;
    @Column(name = "total_amt_rvsl")
    private Integer total_amt_rvsl;

    @OneToOne
    @JoinColumn(name="tran_type", referencedColumnName = "trx_id", insertable=false, updatable=false)
    @NotFound(action = NotFoundAction.IGNORE)
    private EsqTrxid esqTrxid;

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getTimeFr() {
        return time_fr_3h;
    }

    public void setTimeFr(String time_fr_3h) {
        this.time_fr_3h = time_fr_3h;
    }

    public String getTimeTo() {
        return time_to_3h;
    }

    public void setTimeTo(String time_to_3h) {
        this.time_to_3h = time_to_3h;
    }

    public String getTrxType() {
        return trx_type;
    }

    public void setTrxType(String trx_type) {
        this.trx_type = trx_type;
    }

    public String getTranType() {
        return tran_type;
    }

    public void setTranType(String tran_type) {
        this.tran_type = tran_type;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getIssuerBank() {
        return issuer_bank;
    }

    public void setIssuerBank(String issuer_bank) {
        this.issuer_bank = issuer_bank;
    }

    public String getPengirim() {
        return pengirim;
    }

    public void setPengirim(String pengirim) {
        this.pengirim = pengirim;
    }

    public Integer getFreqApp() {
        return freq_app;
    }

    public void setFreqApp(Integer freq_app) {
        this.freq_app = freq_app;
    }

    public Integer getFreqDc() {
        return freq_dc;
    }

    public void setFreqDc(Integer freq_dc) {
        this.freq_dc = freq_dc;
    }

    public Integer getFreqRvsl() {
        return freq_rvsl;
    }

    public void setFreqRvsl(Integer freq_rvsl) {
        this.freq_rvsl = freq_rvsl;
    }

    public Integer getAmtApp() {
        return total_amt_app;
    }

    public void setAmtApp(Integer total_amt_app) {
        this.total_amt_app = total_amt_app;
    }

    public Integer getAmtDc() {
        return total_amt_dc;
    }

    public void setAmtDc(Integer total_amt_dc) {
        this.total_amt_dc = total_amt_dc;
    }

    public Integer getAmtRvsl() {
        return total_amt_rvsl;
    }

    public void setAmtRvsl(Integer total_amt_rvsl) {
        this.total_amt_rvsl = total_amt_rvsl;
    }

    public EsqTrxid getEsqTrxid() {
        return esqTrxid;
    }

    public void setEsqTrxid(EsqTrxid esqTrxid) {
        this.esqTrxid = esqTrxid;
    }
}

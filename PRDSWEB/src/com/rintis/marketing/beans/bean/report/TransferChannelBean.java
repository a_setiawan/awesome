package com.rintis.marketing.beans.bean.report;

import java.math.BigDecimal;

public class TransferChannelBean implements Comparable<TransferChannelBean> {
    private Integer day;
    private String bankid;
    private String bankname;
    private BigDecimal atm;
    private BigDecimal internetbanking;
    private BigDecimal mobilebanking;
    private BigDecimal remittance;
    private BigDecimal phonebanking;
    private BigDecimal smsbanking;
    private BigDecimal decline;
    private BigDecimal edc;
    private BigDecimal total;
    private BigDecimal contribution;


    @Override
    public int compareTo(TransferChannelBean comparestu) {
        BigDecimal compareage = ((TransferChannelBean)comparestu).getTotal();
        /* For Ascending order*/
        //return this.pct.compareTo(compareage);

        /* For Descending order do like this */
        return compareage.compareTo(total);
    }


    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public BigDecimal getAtm() {
        return atm;
    }

    public void setAtm(BigDecimal atm) {
        this.atm = atm;
    }

    public BigDecimal getInternetbanking() {
        return internetbanking;
    }

    public void setInternetbanking(BigDecimal internetbanking) {
        this.internetbanking = internetbanking;
    }

    public BigDecimal getMobilebanking() {
        return mobilebanking;
    }

    public void setMobilebanking(BigDecimal mobilebanking) {
        this.mobilebanking = mobilebanking;
    }

    public BigDecimal getRemittance() {
        return remittance;
    }

    public void setRemittance(BigDecimal remittance) {
        this.remittance = remittance;
    }

    public BigDecimal getPhonebanking() {
        return phonebanking;
    }

    public void setPhonebanking(BigDecimal phonebanking) {
        this.phonebanking = phonebanking;
    }

    public BigDecimal getSmsbanking() {
        return smsbanking;
    }

    public void setSmsbanking(BigDecimal smsbanking) {
        this.smsbanking = smsbanking;
    }

    public BigDecimal getDecline() {
        return decline;
    }

    public void setDecline(BigDecimal decline) {
        this.decline = decline;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getContribution() {
        return contribution;
    }

    public void setContribution(BigDecimal contribution) {
        this.contribution = contribution;
    }

    public BigDecimal getEdc() {
        return edc;
    }

    public void setEdc(BigDecimal edc) {
        this.edc = edc;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }
}

package com.rintis.marketing.beans.bean.report;

import java.math.BigDecimal;

public class AllChannelBean {
    private String channelid;
    private String channelname;
    private BigDecimal withdrawal;
    private BigDecimal inquiry;
    private BigDecimal transfer;
    private BigDecimal payment;
    private BigDecimal topup;
    private BigDecimal decline;
    private BigDecimal primadebit;
    private BigDecimal declinepos;
    private BigDecimal total;
    private BigDecimal contribution1;
    private BigDecimal contribution2;


    public String getChannelname() {
        return channelname;
    }

    public void setChannelname(String channelname) {
        this.channelname = channelname;
    }

    public BigDecimal getWithdrawal() {
        return withdrawal;
    }

    public void setWithdrawal(BigDecimal withdrawal) {
        this.withdrawal = withdrawal;
    }

    public BigDecimal getInquiry() {
        return inquiry;
    }

    public void setInquiry(BigDecimal inquiry) {
        this.inquiry = inquiry;
    }

    public BigDecimal getTransfer() {
        return transfer;
    }

    public void setTransfer(BigDecimal transfer) {
        this.transfer = transfer;
    }

    public BigDecimal getDecline() {
        return decline;
    }

    public void setDecline(BigDecimal decline) {
        this.decline = decline;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getContribution1() {
        return contribution1;
    }

    public void setContribution1(BigDecimal contribution1) {
        this.contribution1 = contribution1;
    }

    public BigDecimal getContribution2() {
        return contribution2;
    }

    public void setContribution2(BigDecimal contribution2) {
        this.contribution2 = contribution2;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public BigDecimal getTopup() {
        return topup;
    }

    public void setTopup(BigDecimal topup) {
        this.topup = topup;
    }

    public String getChannelid() {
        return channelid;
    }

    public void setChannelid(String channelid) {
        this.channelid = channelid;
    }

    public BigDecimal getPrimadebit() {
        return primadebit;
    }

    public void setPrimadebit(BigDecimal primadebit) {
        this.primadebit = primadebit;
    }

    public BigDecimal getDeclinepos() {
        return declinepos;
    }

    public void setDeclinepos(BigDecimal declinepos) {
        this.declinepos = declinepos;
    }
}

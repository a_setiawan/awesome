package com.rintis.marketing.beans.bean.report;

import com.rintis.marketing.core.utils.MathUtils;

import java.math.BigDecimal;

public class DailyTransactionBean {
    private Integer day;
    private BigDecimal withdrawal;
    private BigDecimal balanceinquiry;
    private BigDecimal transfer;
    private BigDecimal transferLocal;
    private BigDecimal transferIntercon;
    private BigDecimal topup;
    private BigDecimal payment;
    private BigDecimal decline;
    private BigDecimal posappr;
    private BigDecimal posdec;
    private BigDecimal total;

    private BigDecimal withR2;
    private BigDecimal withC2;
    private BigDecimal inqR2;
    private BigDecimal inqC2;
    private BigDecimal transfX2;
    private BigDecimal pmtP2;
    private BigDecimal topU210;
    private BigDecimal topU211;
    private BigDecimal topU220;
    private BigDecimal topU230;

    private BigDecimal withDecR2;
    private BigDecimal withDecC2;
    private BigDecimal inqDecR2;
    private BigDecimal inqDecC2;
    private BigDecimal transfDecX2;
    private BigDecimal pmtDecP2;
    private BigDecimal topDecU210;
    private BigDecimal topDecU211;
    private BigDecimal topDecpU220;
    private BigDecimal topDecpU230;

    private BigDecimal debApprD2Sales;
    private BigDecimal debApprD2Void;
    private BigDecimal debDeclD2Sales;
    private BigDecimal debDeclD2Void;

    private BigDecimal cup;
    private BigDecimal apn;


    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public BigDecimal getWithdrawal() {
        return withdrawal;
    }

    public void setWithdrawal(BigDecimal withdrawal) {
        this.withdrawal = withdrawal;
    }

    public BigDecimal getBalanceinquiry() {
        return balanceinquiry;
    }

    public void setBalanceinquiry(BigDecimal balanceinquiry) {
        this.balanceinquiry = balanceinquiry;
    }

    public BigDecimal getTransfer() {
        return transfer;
    }

    public void setTransfer(BigDecimal transfer) {
        this.transfer = transfer;
    }

    public BigDecimal getTopup() {
        return topup;
    }

    public void setTopup(BigDecimal topup) {
        this.topup = topup;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public BigDecimal getDecline() {
        return decline;
    }

    public void setDecline(BigDecimal decline) {
        this.decline = decline;
    }

    public BigDecimal getPosappr() {
        return posappr;
    }

    public void setPosappr(BigDecimal posappr) {
        this.posappr = posappr;
    }

    public BigDecimal getPosdec() {
        return posdec;
    }

    public void setPosdec(BigDecimal posdec) {
        this.posdec = posdec;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getWithR2() {
        return withR2;
    }

    public void setWithR2(BigDecimal withR2) {
        this.withR2 = withR2;
    }

    public BigDecimal getWithC2() {
        return MathUtils.checkNull(withC2);
    }

    public void setWithC2(BigDecimal withC2) {
        this.withC2 = withC2;
    }

    public BigDecimal getInqR2() {
        return inqR2;
    }

    public void setInqR2(BigDecimal inqR2) {
        this.inqR2 = inqR2;
    }

    public BigDecimal getInqC2() {
        return MathUtils.checkNull(inqC2);
    }

    public void setInqC2(BigDecimal inqC2) {
        this.inqC2 = inqC2;
    }

    public BigDecimal getTransfX2() {
        return transfX2;
    }

    public void setTransfX2(BigDecimal transfX2) {
        this.transfX2 = transfX2;
    }

    public BigDecimal getPmtP2() {
        return pmtP2;
    }

    public void setPmtP2(BigDecimal pmtP2) {
        this.pmtP2 = pmtP2;
    }

    public BigDecimal getTopU210() {
        return topU210;
    }

    public void setTopU210(BigDecimal topU210) {
        this.topU210 = topU210;
    }

    public BigDecimal getTopU211() {
        return topU211;
    }

    public void setTopU211(BigDecimal topU211) {
        this.topU211 = topU211;
    }

    public BigDecimal getTopU220() {
        return topU220;
    }

    public void setTopU220(BigDecimal topU220) {
        this.topU220 = topU220;
    }

    public BigDecimal getTopU230() {
        return topU230;
    }

    public void setTopU230(BigDecimal topU230) {
        this.topU230 = topU230;
    }

    public BigDecimal getWithDecR2() {
        return withDecR2;
    }

    public void setWithDecR2(BigDecimal withDecR2) {
        this.withDecR2 = withDecR2;
    }

    public BigDecimal getWithDecC2() {
        return withDecC2;
    }

    public void setWithDecC2(BigDecimal withDecC2) {
        this.withDecC2 = withDecC2;
    }

    public BigDecimal getInqDecR2() {
        return inqDecR2;
    }

    public void setInqDecR2(BigDecimal inqDecR2) {
        this.inqDecR2 = inqDecR2;
    }

    public BigDecimal getInqDecC2() {
        return inqDecC2;
    }

    public void setInqDecC2(BigDecimal inqDecC2) {
        this.inqDecC2 = inqDecC2;
    }

    public BigDecimal getTransfDecX2() {
        return transfDecX2;
    }

    public void setTransfDecX2(BigDecimal transfDecX2) {
        this.transfDecX2 = transfDecX2;
    }

    public BigDecimal getPmtDecP2() {
        return pmtDecP2;
    }

    public void setPmtDecP2(BigDecimal pmtDecP2) {
        this.pmtDecP2 = pmtDecP2;
    }

    public BigDecimal getTopDecU210() {
        return topDecU210;
    }

    public void setTopDecU210(BigDecimal topDecU210) {
        this.topDecU210 = topDecU210;
    }

    public BigDecimal getTopDecU211() {
        return topDecU211;
    }

    public void setTopDecU211(BigDecimal topDecU211) {
        this.topDecU211 = topDecU211;
    }

    public BigDecimal getTopDecpU220() {
        return topDecpU220;
    }

    public void setTopDecpU220(BigDecimal topDecpU220) {
        this.topDecpU220 = topDecpU220;
    }

    public BigDecimal getTopDecpU230() {
        return topDecpU230;
    }

    public void setTopDecpU230(BigDecimal topDecpU230) {
        this.topDecpU230 = topDecpU230;
    }

    public BigDecimal getDebApprD2Sales() {
        return debApprD2Sales;
    }

    public void setDebApprD2Sales(BigDecimal debApprD2Sales) {
        this.debApprD2Sales = debApprD2Sales;
    }

    public BigDecimal getDebApprD2Void() {
        return debApprD2Void;
    }

    public void setDebApprD2Void(BigDecimal debApprD2Void) {
        this.debApprD2Void = debApprD2Void;
    }

    public BigDecimal getDebDeclD2Sales() {
        return debDeclD2Sales;
    }

    public void setDebDeclD2Sales(BigDecimal debDeclD2Sales) {
        this.debDeclD2Sales = debDeclD2Sales;
    }

    public BigDecimal getDebDeclD2Void() {
        return debDeclD2Void;
    }

    public void setDebDeclD2Void(BigDecimal debDeclD2Void) {
        this.debDeclD2Void = debDeclD2Void;
    }

    public BigDecimal getCup() {
        return cup;
    }

    public void setCup(BigDecimal cup) {
        this.cup = cup;
    }

    public BigDecimal getApn() {
        return apn;
    }

    public void setApn(BigDecimal apn) {
        this.apn = apn;
    }

    public BigDecimal getTransferLocal() {
        return transferLocal;
    }

    public void setTransferLocal(BigDecimal transferLocal) {
        this.transferLocal = transferLocal;
    }

    public BigDecimal getTransferIntercon() {
        return transferIntercon;
    }

    public void setTransferIntercon(BigDecimal transferIntercon) {
        this.transferIntercon = transferIntercon;
    }
}

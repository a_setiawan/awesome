package com.rintis.marketing.beans.bean.report;

import org.primefaces.model.chart.LineChartModel;

import java.math.BigDecimal;

public class GeneralSummaryBean implements Comparable<GeneralSummaryBean> {
    private String transactionid;
    private String transactionname;
    private BigDecimal actualytd1;
    private BigDecimal actualytd2;
    private BigDecimal actualytd3;
    private BigDecimal actualytd4;
    private BigDecimal actualytd5;
    private BigDecimal actualytd6;
    private BigDecimal actualytd7;
    private BigDecimal actualytd8;
    private BigDecimal actualytd9;
    private BigDecimal actualytd10;
    private BigDecimal actualytd11;
    private BigDecimal actualytd12;

    private BigDecimal actualytdallmonth1;
    private BigDecimal actualytdallmonth2;
    private BigDecimal actualytdallmonth3;
    private BigDecimal actualytdallmonth4;
    private BigDecimal actualytdallmonth5;
    private BigDecimal actualytdallmonth6;
    private BigDecimal actualytdallmonth7;
    private BigDecimal actualytdallmonth8;
    private BigDecimal actualytdallmonth9;
    private BigDecimal actualytdallmonth10;
    private BigDecimal actualytdallmonth11;
    private BigDecimal actualytdallmonth12;

    private BigDecimal target1;
    private BigDecimal target2;
    private BigDecimal target3;
    private BigDecimal target4;
    private BigDecimal target5;
    private BigDecimal target6;
    private BigDecimal target7;
    private BigDecimal target8;
    private BigDecimal target9;
    private BigDecimal target10;
    private BigDecimal target11;
    private BigDecimal target12;

    private BigDecimal actualmtd;
    private BigDecimal targetmtd;
    private BigDecimal targetyear;
    private BigDecimal actualMtdVsTargetMtd;
    private BigDecimal actualMtdVsTargetYear;
    private BigDecimal pctContribution;

    private BigDecimal actualq1;
    private BigDecimal actualq2;
    private BigDecimal actualq3;
    private BigDecimal actualq4;
    private BigDecimal targetq1;
    private BigDecimal targetq2;
    private BigDecimal targetq3;
    private BigDecimal targetq4;
    private BigDecimal actualqmtd;
    private BigDecimal targetqmtd;
    private BigDecimal actualqMtdVsTargetqMtd;
    private BigDecimal actualqMtdVsTargetYear;
    private BigDecimal pctContributionQ;

    private String color;

    @Override
    public int compareTo(GeneralSummaryBean comparestu) {
        //BigDecimal compareage = ((GeneralSummaryBean)comparestu).getTotal();
        /* For Ascending order*/
        //return this.pct.compareTo(compareage);

        /* For Descending order do like this */
        //return compareage.compareTo(total);
        return 0;
    }

    public String getTransactionname() {
        return transactionname;
    }

    public void setTransactionname(String transactionname) {
        this.transactionname = transactionname;
    }

    public BigDecimal getActualytd1() {
        return actualytd1;
    }

    public void setActualytd1(BigDecimal actualytd1) {
        this.actualytd1 = actualytd1;
    }

    public BigDecimal getActualytd2() {
        return actualytd2;
    }

    public void setActualytd2(BigDecimal actualytd2) {
        this.actualytd2 = actualytd2;
    }

    public BigDecimal getActualytd3() {
        return actualytd3;
    }

    public void setActualytd3(BigDecimal actualytd3) {
        this.actualytd3 = actualytd3;
    }

    public BigDecimal getActualytd4() {
        return actualytd4;
    }

    public void setActualytd4(BigDecimal actualytd4) {
        this.actualytd4 = actualytd4;
    }

    public BigDecimal getActualytd5() {
        return actualytd5;
    }

    public void setActualytd5(BigDecimal actualytd5) {
        this.actualytd5 = actualytd5;
    }

    public BigDecimal getActualytd6() {
        return actualytd6;
    }

    public void setActualytd6(BigDecimal actualytd6) {
        this.actualytd6 = actualytd6;
    }

    public BigDecimal getActualytd7() {
        return actualytd7;
    }

    public void setActualytd7(BigDecimal actualytd7) {
        this.actualytd7 = actualytd7;
    }

    public BigDecimal getActualytd8() {
        return actualytd8;
    }

    public void setActualytd8(BigDecimal actualytd8) {
        this.actualytd8 = actualytd8;
    }

    public BigDecimal getActualytd9() {
        return actualytd9;
    }

    public void setActualytd9(BigDecimal actualytd9) {
        this.actualytd9 = actualytd9;
    }

    public BigDecimal getActualytd10() {
        return actualytd10;
    }

    public void setActualytd10(BigDecimal actualytd10) {
        this.actualytd10 = actualytd10;
    }

    public BigDecimal getActualytd11() {
        return actualytd11;
    }

    public void setActualytd11(BigDecimal actualytd11) {
        this.actualytd11 = actualytd11;
    }

    public BigDecimal getActualytd12() {
        return actualytd12;
    }

    public void setActualytd12(BigDecimal actualytd12) {
        this.actualytd12 = actualytd12;
    }

    public BigDecimal getActualmtd() {
        return actualmtd;
    }

    public void setActualmtd(BigDecimal actualmtd) {
        this.actualmtd = actualmtd;
    }

    public BigDecimal getTargetmtd() {
        return targetmtd;
    }

    public void setTargetmtd(BigDecimal targetmtd) {
        this.targetmtd = targetmtd;
    }

    public BigDecimal getTargetyear() {
        return targetyear;
    }

    public void setTargetyear(BigDecimal targetyear) {
        this.targetyear = targetyear;
    }

    public BigDecimal getActualMtdVsTargetMtd() {
        return actualMtdVsTargetMtd;
    }

    public void setActualMtdVsTargetMtd(BigDecimal actualMtdVsTargetMtd) {
        this.actualMtdVsTargetMtd = actualMtdVsTargetMtd;
    }

    public BigDecimal getActualMtdVsTargetYear() {
        return actualMtdVsTargetYear;
    }

    public void setActualMtdVsTargetYear(BigDecimal actualMtdVsTargetYear) {
        this.actualMtdVsTargetYear = actualMtdVsTargetYear;
    }

    public BigDecimal getTarget1() {
        return target1;
    }

    public void setTarget1(BigDecimal target1) {
        this.target1 = target1;
    }

    public BigDecimal getTarget2() {
        return target2;
    }

    public void setTarget2(BigDecimal target2) {
        this.target2 = target2;
    }

    public BigDecimal getTarget3() {
        return target3;
    }

    public void setTarget3(BigDecimal target3) {
        this.target3 = target3;
    }

    public BigDecimal getTarget4() {
        return target4;
    }

    public void setTarget4(BigDecimal target4) {
        this.target4 = target4;
    }

    public BigDecimal getTarget5() {
        return target5;
    }

    public void setTarget5(BigDecimal target5) {
        this.target5 = target5;
    }

    public BigDecimal getTarget6() {
        return target6;
    }

    public void setTarget6(BigDecimal target6) {
        this.target6 = target6;
    }

    public BigDecimal getTarget7() {
        return target7;
    }

    public void setTarget7(BigDecimal target7) {
        this.target7 = target7;
    }

    public BigDecimal getTarget8() {
        return target8;
    }

    public void setTarget8(BigDecimal target8) {
        this.target8 = target8;
    }

    public BigDecimal getTarget9() {
        return target9;
    }

    public void setTarget9(BigDecimal target9) {
        this.target9 = target9;
    }

    public BigDecimal getTarget10() {
        return target10;
    }

    public void setTarget10(BigDecimal target10) {
        this.target10 = target10;
    }

    public BigDecimal getTarget11() {
        return target11;
    }

    public void setTarget11(BigDecimal target11) {
        this.target11 = target11;
    }

    public BigDecimal getTarget12() {
        return target12;
    }

    public void setTarget12(BigDecimal target12) {
        this.target12 = target12;
    }

    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public BigDecimal getPctContribution() {
        return pctContribution;
    }

    public void setPctContribution(BigDecimal pctContribution) {
        this.pctContribution = pctContribution;
    }

    public BigDecimal getActualq1() {
        return actualq1;
    }

    public void setActualq1(BigDecimal actualq1) {
        this.actualq1 = actualq1;
    }

    public BigDecimal getActualq2() {
        return actualq2;
    }

    public void setActualq2(BigDecimal actualq2) {
        this.actualq2 = actualq2;
    }

    public BigDecimal getActualq3() {
        return actualq3;
    }

    public void setActualq3(BigDecimal actualq3) {
        this.actualq3 = actualq3;
    }

    public BigDecimal getActualq4() {
        return actualq4;
    }

    public void setActualq4(BigDecimal actualq4) {
        this.actualq4 = actualq4;
    }

    public BigDecimal getTargetq1() {
        return targetq1;
    }

    public void setTargetq1(BigDecimal targetq1) {
        this.targetq1 = targetq1;
    }

    public BigDecimal getTargetq2() {
        return targetq2;
    }

    public void setTargetq2(BigDecimal targetq2) {
        this.targetq2 = targetq2;
    }

    public BigDecimal getTargetq3() {
        return targetq3;
    }

    public void setTargetq3(BigDecimal targetq3) {
        this.targetq3 = targetq3;
    }

    public BigDecimal getTargetq4() {
        return targetq4;
    }

    public void setTargetq4(BigDecimal targetq4) {
        this.targetq4 = targetq4;
    }

    public BigDecimal getActualqmtd() {
        return actualqmtd;
    }

    public void setActualqmtd(BigDecimal actualqmtd) {
        this.actualqmtd = actualqmtd;
    }

    public BigDecimal getTargetqmtd() {
        return targetqmtd;
    }

    public void setTargetqmtd(BigDecimal targetqmtd) {
        this.targetqmtd = targetqmtd;
    }

    public BigDecimal getActualqMtdVsTargetqMtd() {
        return actualqMtdVsTargetqMtd;
    }

    public void setActualqMtdVsTargetqMtd(BigDecimal actualqMtdVsTargetqMtd) {
        this.actualqMtdVsTargetqMtd = actualqMtdVsTargetqMtd;
    }

    public BigDecimal getActualqMtdVsTargetYear() {
        return actualqMtdVsTargetYear;
    }

    public void setActualqMtdVsTargetYear(BigDecimal actualqMtdVsTargetYear) {
        this.actualqMtdVsTargetYear = actualqMtdVsTargetYear;
    }

    public BigDecimal getPctContributionQ() {
        return pctContributionQ;
    }

    public void setPctContributionQ(BigDecimal pctContributionQ) {
        this.pctContributionQ = pctContributionQ;
    }

    public BigDecimal getActualytdallmonth1() {
        return actualytdallmonth1;
    }

    public void setActualytdallmonth1(BigDecimal actualytdallmonth1) {
        this.actualytdallmonth1 = actualytdallmonth1;
    }

    public BigDecimal getActualytdallmonth2() {
        return actualytdallmonth2;
    }

    public void setActualytdallmonth2(BigDecimal actualytdallmonth2) {
        this.actualytdallmonth2 = actualytdallmonth2;
    }

    public BigDecimal getActualytdallmonth3() {
        return actualytdallmonth3;
    }

    public void setActualytdallmonth3(BigDecimal actualytdallmonth3) {
        this.actualytdallmonth3 = actualytdallmonth3;
    }

    public BigDecimal getActualytdallmonth4() {
        return actualytdallmonth4;
    }

    public void setActualytdallmonth4(BigDecimal actualytdallmonth4) {
        this.actualytdallmonth4 = actualytdallmonth4;
    }

    public BigDecimal getActualytdallmonth5() {
        return actualytdallmonth5;
    }

    public void setActualytdallmonth5(BigDecimal actualytdallmonth5) {
        this.actualytdallmonth5 = actualytdallmonth5;
    }

    public BigDecimal getActualytdallmonth6() {
        return actualytdallmonth6;
    }

    public void setActualytdallmonth6(BigDecimal actualytdallmonth6) {
        this.actualytdallmonth6 = actualytdallmonth6;
    }

    public BigDecimal getActualytdallmonth7() {
        return actualytdallmonth7;
    }

    public void setActualytdallmonth7(BigDecimal actualytdallmonth7) {
        this.actualytdallmonth7 = actualytdallmonth7;
    }

    public BigDecimal getActualytdallmonth8() {
        return actualytdallmonth8;
    }

    public void setActualytdallmonth8(BigDecimal actualytdallmonth8) {
        this.actualytdallmonth8 = actualytdallmonth8;
    }

    public BigDecimal getActualytdallmonth9() {
        return actualytdallmonth9;
    }

    public void setActualytdallmonth9(BigDecimal actualytdallmonth9) {
        this.actualytdallmonth9 = actualytdallmonth9;
    }

    public BigDecimal getActualytdallmonth10() {
        return actualytdallmonth10;
    }

    public void setActualytdallmonth10(BigDecimal actualytdallmonth10) {
        this.actualytdallmonth10 = actualytdallmonth10;
    }

    public BigDecimal getActualytdallmonth11() {
        return actualytdallmonth11;
    }

    public void setActualytdallmonth11(BigDecimal actualytdallmonth11) {
        this.actualytdallmonth11 = actualytdallmonth11;
    }

    public BigDecimal getActualytdallmonth12() {
        return actualytdallmonth12;
    }

    public void setActualytdallmonth12(BigDecimal actualytdallmonth12) {
        this.actualytdallmonth12 = actualytdallmonth12;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}

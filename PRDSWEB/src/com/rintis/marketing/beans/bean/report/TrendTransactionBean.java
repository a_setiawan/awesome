package com.rintis.marketing.beans.bean.report;

import java.math.BigDecimal;

public class TrendTransactionBean {
    private int month;
    private String smonth;
    private BigDecimal prevYear;
    private BigDecimal currYear;
    private BigDecimal avgPerDay;
    private BigDecimal percentGrowth;

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getSmonth() {
        return smonth;
    }

    public void setSmonth(String smonth) {
        this.smonth = smonth;
    }

    public BigDecimal getPrevYear() {
        return prevYear;
    }

    public void setPrevYear(BigDecimal prevYear) {
        this.prevYear = prevYear;
    }

    public BigDecimal getCurrYear() {
        return currYear;
    }

    public void setCurrYear(BigDecimal currYear) {
        this.currYear = currYear;
    }

    public BigDecimal getAvgPerDay() {
        return avgPerDay;
    }

    public void setAvgPerDay(BigDecimal avgPerDay) {
        this.avgPerDay = avgPerDay;
    }

    public BigDecimal getPercentGrowth() {
        return percentGrowth;
    }

    public void setPercentGrowth(BigDecimal percentGrowth) {
        this.percentGrowth = percentGrowth;
    }
}

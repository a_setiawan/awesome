package com.rintis.marketing.beans.bean.report;

import java.math.BigDecimal;

public class TransContributionBean {
    private String bankid;
    private String bankname;
    private String billerid;
    private String billername;
    private BigDecimal prevyear;
    private BigDecimal curryear;
    private BigDecimal txgrowth;
    private BigDecimal pctxgrowth;
    private BigDecimal pctcontribution;

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getBillerid() {
        return billerid;
    }

    public void setBillerid(String billerid) {
        this.billerid = billerid;
    }

    public String getBillername() {
        return billername;
    }

    public void setBillername(String billername) {
        this.billername = billername;
    }

    public BigDecimal getPrevyear() {
        return prevyear;
    }

    public void setPrevyear(BigDecimal prevyear) {
        this.prevyear = prevyear;
    }

    public BigDecimal getCurryear() {
        return curryear;
    }

    public void setCurryear(BigDecimal curryear) {
        this.curryear = curryear;
    }

    public BigDecimal getTxgrowth() {
        return txgrowth;
    }

    public void setTxgrowth(BigDecimal txgrowth) {
        this.txgrowth = txgrowth;
    }

    public BigDecimal getPctxgrowth() {
        return pctxgrowth;
    }

    public void setPctxgrowth(BigDecimal pctxgrowth) {
        this.pctxgrowth = pctxgrowth;
    }

    public BigDecimal getPctcontribution() {
        return pctcontribution;
    }

    public void setPctcontribution(BigDecimal pctcontribution) {
        this.pctcontribution = pctcontribution;
    }
}

package com.rintis.marketing.beans.bean.report;

import java.math.BigDecimal;

public class SwtDeclineBean {
    private String acqid;
    private String acqname;
    private String issid;
    private String issname;
    private BigDecimal declwdw;
    private BigDecimal declinq;
    private BigDecimal decltrf;
    private BigDecimal declpmt;
    private BigDecimal decltop;
    private BigDecimal apprwdw;
    private BigDecimal apprinq;
    private BigDecimal apprtrf;
    private BigDecimal apprpmt;
    private BigDecimal apprtop;
    private BigDecimal total;
    private Integer month;
    private String smonth;

    public String getAcqid() {
        return acqid;
    }

    public void setAcqid(String acqid) {
        this.acqid = acqid;
    }

    public String getAcqname() {
        return acqname;
    }

    public void setAcqname(String acqname) {
        this.acqname = acqname;
    }

    public String getIssid() {
        return issid;
    }

    public void setIssid(String issid) {
        this.issid = issid;
    }

    public String getIssname() {
        return issname;
    }

    public void setIssname(String issname) {
        this.issname = issname;
    }

    public BigDecimal getDeclwdw() {
        return declwdw;
    }

    public void setDeclwdw(BigDecimal declwdw) {
        this.declwdw = declwdw;
    }

    public BigDecimal getDeclinq() {
        return declinq;
    }

    public void setDeclinq(BigDecimal declinq) {
        this.declinq = declinq;
    }

    public BigDecimal getDecltrf() {
        return decltrf;
    }

    public void setDecltrf(BigDecimal decltrf) {
        this.decltrf = decltrf;
    }

    public BigDecimal getDeclpmt() {
        return declpmt;
    }

    public void setDeclpmt(BigDecimal declpmt) {
        this.declpmt = declpmt;
    }

    public BigDecimal getDecltop() {
        return decltop;
    }

    public void setDecltop(BigDecimal decltop) {
        this.decltop = decltop;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public String getSmonth() {
        return smonth;
    }

    public void setSmonth(String smonth) {
        this.smonth = smonth;
    }

    public BigDecimal getApprwdw() {
        return apprwdw;
    }

    public void setApprwdw(BigDecimal apprwdw) {
        this.apprwdw = apprwdw;
    }

    public BigDecimal getApprinq() {
        return apprinq;
    }

    public void setApprinq(BigDecimal apprinq) {
        this.apprinq = apprinq;
    }

    public BigDecimal getApprtrf() {
        return apprtrf;
    }

    public void setApprtrf(BigDecimal apprtrf) {
        this.apprtrf = apprtrf;
    }

    public BigDecimal getApprpmt() {
        return apprpmt;
    }

    public void setApprpmt(BigDecimal apprpmt) {
        this.apprpmt = apprpmt;
    }

    public BigDecimal getApprtop() {
        return apprtop;
    }

    public void setApprtop(BigDecimal apprtop) {
        this.apprtop = apprtop;
    }
}

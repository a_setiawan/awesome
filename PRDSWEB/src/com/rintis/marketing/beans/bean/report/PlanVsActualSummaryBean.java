package com.rintis.marketing.beans.bean.report;

import java.math.BigDecimal;

public class PlanVsActualSummaryBean {
    private BigDecimal totalActPrevYear;
    private BigDecimal totalActCurrYear;
    private BigDecimal totalTargetCurrYear;
    private BigDecimal totalActCurrYTd;
    private BigDecimal totalActPrevYTd;
    private BigDecimal totalTargetCurrYtd;
    private BigDecimal totalKjp;
    private BigDecimal totalNonKjp;
    private BigDecimal pctKjp;
    private BigDecimal pctNonKjp;


    private BigDecimal pctGrowthYtd;
    private BigDecimal pctYtd;
    private BigDecimal pctYear;
    private String smonth;

    public BigDecimal getTotalActPrevYear() {
        return totalActPrevYear;
    }

    public void setTotalActPrevYear(BigDecimal totalActPrevYear) {
        this.totalActPrevYear = totalActPrevYear;
    }

    public BigDecimal getTotalActCurrYear() {
        return totalActCurrYear;
    }

    public void setTotalActCurrYear(BigDecimal totalActCurrYear) {
        this.totalActCurrYear = totalActCurrYear;
    }

    public BigDecimal getTotalTargetCurrYear() {
        return totalTargetCurrYear;
    }

    public void setTotalTargetCurrYear(BigDecimal totalTargetCurrYear) {
        this.totalTargetCurrYear = totalTargetCurrYear;
    }

    public BigDecimal getTotalActCurrYTd() {
        return totalActCurrYTd;
    }

    public void setTotalActCurrYTd(BigDecimal totalActCurrYTd) {
        this.totalActCurrYTd = totalActCurrYTd;
    }

    public BigDecimal getTotalTargetCurrYtd() {
        return totalTargetCurrYtd;
    }

    public void setTotalTargetCurrYtd(BigDecimal totalTargetCurrYtd) {
        this.totalTargetCurrYtd = totalTargetCurrYtd;
    }

    public BigDecimal getPctGrowthYtd() {
        return pctGrowthYtd;
    }

    public void setPctGrowthYtd(BigDecimal pctGrowthYtd) {
        this.pctGrowthYtd = pctGrowthYtd;
    }

    public BigDecimal getPctYtd() {
        return pctYtd;
    }

    public void setPctYtd(BigDecimal pctYtd) {
        this.pctYtd = pctYtd;
    }

    public BigDecimal getPctYear() {
        return pctYear;
    }

    public void setPctYear(BigDecimal pctYear) {
        this.pctYear = pctYear;
    }

    public BigDecimal getTotalActPrevYTd() {
        return totalActPrevYTd;
    }

    public void setTotalActPrevYTd(BigDecimal totalActPrevYTd) {
        this.totalActPrevYTd = totalActPrevYTd;
    }

    public String getSmonth() {
        return smonth;
    }

    public void setSmonth(String smonth) {
        this.smonth = smonth;
    }

    public BigDecimal getTotalKjp() {
        return totalKjp;
    }

    public void setTotalKjp(BigDecimal totalKjp) {
        this.totalKjp = totalKjp;
    }

    public BigDecimal getPctKjp() {
        return pctKjp;
    }

    public void setPctKjp(BigDecimal pctKjp) {
        this.pctKjp = pctKjp;
    }

    public BigDecimal getTotalNonKjp() {
        return totalNonKjp;
    }

    public void setTotalNonKjp(BigDecimal totalNonKjp) {
        this.totalNonKjp = totalNonKjp;
    }

    public BigDecimal getPctNonKjp() {
        return pctNonKjp;
    }

    public void setPctNonKjp(BigDecimal pctNonKjp) {
        this.pctNonKjp = pctNonKjp;
    }
}

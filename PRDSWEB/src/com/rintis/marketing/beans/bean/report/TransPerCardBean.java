package com.rintis.marketing.beans.bean.report;


import java.math.BigDecimal;

public class TransPerCardBean {
    private String bankid;
    private String bankname;
    private BigDecimal totalcardholder;
    private BigDecimal totalatm;
    private BigDecimal totaltrxbycardholder;
    private BigDecimal ibft;
    private BigDecimal wdw;
    private BigDecimal balinq;
    private BigDecimal payment;
    private BigDecimal debit;
    private BigDecimal topup;
    private BigDecimal pctTotalTrxByCard;
    private BigDecimal pctIbft;
    private BigDecimal pctWdw;
    private BigDecimal pctBalInq;
    private BigDecimal pctPmt;
    private BigDecimal pctTopup;
    private BigDecimal pctDebit;

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public BigDecimal getTotalcardholder() {
        return totalcardholder;
    }

    public void setTotalcardholder(BigDecimal totalcardholder) {
        this.totalcardholder = totalcardholder;
    }

    public BigDecimal getTotalatm() {
        return totalatm;
    }

    public void setTotalatm(BigDecimal totalatm) {
        this.totalatm = totalatm;
    }

    public BigDecimal getTotaltrxbycardholder() {
        return totaltrxbycardholder;
    }

    public void setTotaltrxbycardholder(BigDecimal totaltrxbycardholder) {
        this.totaltrxbycardholder = totaltrxbycardholder;
    }

    public BigDecimal getIbft() {
        return ibft;
    }

    public void setIbft(BigDecimal ibft) {
        this.ibft = ibft;
    }

    public BigDecimal getWdw() {
        return wdw;
    }

    public void setWdw(BigDecimal wdw) {
        this.wdw = wdw;
    }

    public BigDecimal getBalinq() {
        return balinq;
    }

    public void setBalinq(BigDecimal balinq) {
        this.balinq = balinq;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public BigDecimal getPctTotalTrxByCard() {
        return pctTotalTrxByCard;
    }

    public void setPctTotalTrxByCard(BigDecimal pctTotalTrxByCard) {
        this.pctTotalTrxByCard = pctTotalTrxByCard;
    }

    public BigDecimal getPctIbft() {
        return pctIbft;
    }

    public void setPctIbft(BigDecimal pctIbft) {
        this.pctIbft = pctIbft;
    }

    public BigDecimal getPctWdw() {
        return pctWdw;
    }

    public void setPctWdw(BigDecimal pctWdw) {
        this.pctWdw = pctWdw;
    }

    public BigDecimal getPctBalInq() {
        return pctBalInq;
    }

    public void setPctBalInq(BigDecimal pctBalInq) {
        this.pctBalInq = pctBalInq;
    }

    public BigDecimal getPctPmt() {
        return pctPmt;
    }

    public void setPctPmt(BigDecimal pctPmt) {
        this.pctPmt = pctPmt;
    }

    public BigDecimal getDebit() {
        return debit;
    }

    public void setDebit(BigDecimal debit) {
        this.debit = debit;
    }

    public BigDecimal getTopup() {
        return topup;
    }

    public void setTopup(BigDecimal topup) {
        this.topup = topup;
    }

    public BigDecimal getPctTopup() {
        return pctTopup;
    }

    public void setPctTopup(BigDecimal pctTopup) {
        this.pctTopup = pctTopup;
    }

    public BigDecimal getPctDebit() {
        return pctDebit;
    }

    public void setPctDebit(BigDecimal pctDebit) {
        this.pctDebit = pctDebit;
    }
}

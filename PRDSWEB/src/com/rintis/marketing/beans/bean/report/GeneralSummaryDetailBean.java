package com.rintis.marketing.beans.bean.report;

import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.MathUtils;

import java.math.BigDecimal;

public class GeneralSummaryDetailBean implements Comparable<GeneralSummaryDetailBean> {
    private String bankid;
    private String bankname;
    private String bankidiss;
    private String banknameiss;
    private String bankidaq;
    private String banknameacq;
    private BigDecimal wdwlocalfreq;
    private BigDecimal inqlocalfreq;
    private BigDecimal pmtlocalfreq;
    private BigDecimal poslocalfreq;
    private BigDecimal trflocalfreq;
    private BigDecimal trfinterfreq;
    private BigDecimal toplocalfreq;
    private BigDecimal declatmfreq;
    private BigDecimal declposfreq;
    private BigDecimal inqcupfreq;
    private BigDecimal wdwcupfreq;
    private BigDecimal inqapnfreq;
    private BigDecimal wdwapnfreq;

    private BigDecimal totalother;

    private BigDecimal wdwlocaltarget;
    private BigDecimal inqlocaltarget;
    private BigDecimal pmtlocaltarget;
    private BigDecimal poslocaltarget;
    private BigDecimal trflocaltarget;
    private BigDecimal trfintertarget;
    private BigDecimal toplocaltarget;
    private BigDecimal declatmtarget;
    private BigDecimal declpostarget;
    private BigDecimal inqcuptarget;
    private BigDecimal wdwcuptarget;
    private BigDecimal inqapntarget;
    private BigDecimal wdwapntarget;

    private BigDecimal totalothertarget;

    private BigDecimal wdwlocaltargetyear;
    private BigDecimal inqlocaltargetyear;
    private BigDecimal pmtlocaltargetyear;
    private BigDecimal poslocaltargetyear;
    private BigDecimal trflocaltargetyear;
    private BigDecimal trfintertargetyear;
    private BigDecimal toplocaltargetyear;
    private BigDecimal declatmtargetyear;
    private BigDecimal declpostargetyear;
    private BigDecimal inqcuptargetyear;
    private BigDecimal wdwcuptargetyear;
    private BigDecimal inqapntargetyear;
    private BigDecimal wdwapntargetyear;

    private BigDecimal totalothertargetyear;
    private BigDecimal totalgrowth;
    private BigDecimal totalachievement;
    private BigDecimal pct;

    private BigDecimal totalActual;
    private BigDecimal totalTarget;
    private BigDecimal totalTargetYear;

    public BigDecimal frequency(String generalId) {
        if (true) return totalActual;

        if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_WDW_APP)) {
            return wdwlocalfreq;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_INQ_APP)) {
            return inqlocalfreq;
        }  else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_PMT_APP)) {
            return pmtlocalfreq;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_POS_APP)) {
            return poslocalfreq;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TRF_APP)) {
            return trflocalfreq.subtract(trfinterfreq);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TRF_INTER_APP)) {
            return trfinterfreq;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TOP_APP)) {
            return toplocalfreq;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_ATM_DEC)) {
            return declatmfreq;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_POS_DEC)) {
            return declposfreq;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_CUP_APP)) {
            return wdwcupfreq.add(inqcupfreq);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_APN_APP)) {
            return wdwapnfreq.add(inqapnfreq);
        } else if (generalId.equalsIgnoreCase(DataConstant.ALL)) {
            return wdwlocalfreq.add(inqlocalfreq).add(pmtlocalfreq).add(poslocalfreq)
                    .add(trflocalfreq).subtract(trfinterfreq)
                .add(trfinterfreq).add(toplocalfreq).add(declatmfreq).add(declposfreq)
                .add(wdwcupfreq).add(inqcupfreq).add(wdwapnfreq).add(inqapnfreq);
        } else {
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal target(String generalId) {
        if (true) return totalTarget;

        if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_WDW_APP)) {
            return wdwlocaltarget;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_INQ_APP)) {
            return inqlocaltarget;
        }  else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_PMT_APP)) {
            return pmtlocaltarget;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_POS_APP)) {
            return poslocaltarget;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TRF_APP)) {
            return MathUtils.checkNull(trflocaltarget).subtract(MathUtils.checkNull(trfintertarget));
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TRF_INTER_APP)) {
            return trfintertarget;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TOP_APP)) {
            return toplocaltarget;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_ATM_DEC)) {
            return declatmtarget;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_POS_DEC)) {
            return declpostarget;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_CUP_APP)) {
            return MathUtils.checkNull(wdwcuptarget).add(MathUtils.checkNull(inqcuptarget));
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_APN_APP)) {
            return MathUtils.checkNull(wdwapntarget).add(MathUtils.checkNull(inqapntarget));
        } else if (generalId.equalsIgnoreCase(DataConstant.ALL)) {
            return MathUtils.checkNull(wdwlocaltarget)
                    .add(MathUtils.checkNull(inqlocaltarget))
                    .add(MathUtils.checkNull(pmtlocaltarget))
                    .add(MathUtils.checkNull(poslocaltarget))
                    .add(MathUtils.checkNull(trflocaltarget))
                    .subtract(MathUtils.checkNull(trfintertarget))
                    .add(MathUtils.checkNull(trfintertarget))
                    .add(MathUtils.checkNull(toplocaltarget))
                    .add(MathUtils.checkNull(declatmtarget))
                    .add(MathUtils.checkNull(declpostarget))
                    .add(MathUtils.checkNull(wdwcuptarget))
                    .add(MathUtils.checkNull(inqcuptarget))
                    .add(MathUtils.checkNull(wdwapntarget))
                    .add(MathUtils.checkNull(inqapntarget));
        } else {
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal targetYear(String generalId) {
        if (true) return totalTargetYear;

        if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_WDW_APP)) {
            return MathUtils.checkNull(wdwlocaltargetyear);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_INQ_APP)) {
            return MathUtils.checkNull(inqlocaltargetyear);
        }  else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_PMT_APP)) {
            return MathUtils.checkNull(pmtlocaltargetyear);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_POS_APP)) {
            return MathUtils.checkNull(poslocaltargetyear);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TRF_APP)) {
            return MathUtils.checkNull(trflocaltargetyear).subtract(MathUtils.checkNull(trfintertargetyear));
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TRF_INTER_APP)) {
            return MathUtils.checkNull(trfintertargetyear);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TOP_APP)) {
            return MathUtils.checkNull(toplocaltargetyear);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_ATM_DEC)) {
            return MathUtils.checkNull(declatmtargetyear);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_POS_DEC)) {
            return MathUtils.checkNull(declpostargetyear);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_CUP_APP)) {
            return MathUtils.checkNull(wdwcuptargetyear)
                    .add(MathUtils.checkNull(inqcuptargetyear));
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_APN_APP)) {
            return MathUtils.checkNull(wdwapntargetyear)
                    .add(MathUtils.checkNull(inqapntargetyear));
        } else if (generalId.equalsIgnoreCase(DataConstant.ALL)) {
            return MathUtils.checkNull(wdwlocaltargetyear)
                    .add(MathUtils.checkNull(inqlocaltargetyear))
                    .add(MathUtils.checkNull(pmtlocaltargetyear))
                    .add(MathUtils.checkNull(poslocaltargetyear))
                    .add(MathUtils.checkNull(trflocaltargetyear))
                    .subtract(MathUtils.checkNull(trfintertargetyear))
                    .add(MathUtils.checkNull(trfintertargetyear))
                    .add(MathUtils.checkNull(toplocaltarget))
                    .add(MathUtils.checkNull(declatmtargetyear))
                    .add(MathUtils.checkNull(declpostargetyear))
                    .add(MathUtils.checkNull(wdwcuptargetyear))
                    .add(MathUtils.checkNull(inqcuptargetyear))
                    .add(MathUtils.checkNull(wdwapntargetyear))
                    .add(MathUtils.checkNull(inqapntargetyear));
        } else {
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal growth(String generalId) {
        if (true) {
            return MathUtils.calcPercentComparison(totalActual, totalTarget);
        }

        if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_WDW_APP)) {
            return MathUtils.calcPercentComparison(wdwlocalfreq, wdwlocaltarget);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_INQ_APP)) {
            return MathUtils.calcPercentComparison(inqlocalfreq, inqlocaltarget);
        }  else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_PMT_APP)) {
            return MathUtils.calcPercentComparison(pmtlocalfreq, pmtlocaltarget);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_POS_APP)) {
            return MathUtils.calcPercentComparison(poslocalfreq, poslocaltarget);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TRF_APP)) {
            return MathUtils.calcPercentComparison(trflocalfreq.subtract(trfinterfreq), trflocaltarget);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TRF_INTER_APP)) {
            return MathUtils.calcPercentComparison(trfinterfreq, trfintertarget);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TOP_APP)) {
            return MathUtils.calcPercentComparison(toplocalfreq, toplocaltarget);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_ATM_DEC)) {
            return MathUtils.calcPercentComparison(declatmfreq, declatmtarget);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_POS_DEC)) {
            return MathUtils.calcPercentComparison(declposfreq, declpostarget);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_CUP_APP)) {
            return MathUtils.calcPercentComparison(MathUtils.checkNull(wdwcupfreq).add(MathUtils.checkNull(inqcupfreq)), MathUtils.checkNull(wdwcuptarget).add(MathUtils.checkNull(inqcuptarget)));
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_APN_APP)) {
            return MathUtils.calcPercentComparison(MathUtils.checkNull(wdwapnfreq).add(MathUtils.checkNull(inqapnfreq)), MathUtils.checkNull(wdwapntarget).add(MathUtils.checkNull(inqapntarget)));
        } else if (generalId.equalsIgnoreCase(DataConstant.ALL)) {
            BigDecimal b1 = MathUtils.checkNull(wdwlocalfreq)
                    .add(MathUtils.checkNull(inqlocalfreq))
                    .add(MathUtils.checkNull(pmtlocalfreq))
                    .add(MathUtils.checkNull(poslocalfreq))
                    .add(MathUtils.checkNull(trflocalfreq))
                    .subtract(MathUtils.checkNull(trfinterfreq))
                    .add(MathUtils.checkNull(trfinterfreq))
                    .add(MathUtils.checkNull(toplocalfreq))
                    .add(MathUtils.checkNull(declatmfreq))
                    .add(MathUtils.checkNull(declposfreq))
                    .add(MathUtils.checkNull(wdwcupfreq))
                    .add(MathUtils.checkNull(inqcupfreq))
                    .add(MathUtils.checkNull(wdwapnfreq))
                    .add(MathUtils.checkNull(inqapnfreq));

            BigDecimal b2 = MathUtils.checkNull(wdwlocaltarget)
                    .add(MathUtils.checkNull(inqlocaltarget))
                    .add(MathUtils.checkNull(pmtlocaltarget))
                    .add(MathUtils.checkNull(poslocaltarget))
                    .add(MathUtils.checkNull(trflocaltarget))
                    .add(MathUtils.checkNull(trfintertarget))
                    .add(MathUtils.checkNull(toplocaltarget))
                    .add(MathUtils.checkNull(declatmtarget))
                    .add(MathUtils.checkNull(declpostarget))
                    .add(MathUtils.checkNull(wdwcuptarget))
                    .add(MathUtils.checkNull(inqcuptarget))
                    .add(MathUtils.checkNull(wdwapntarget))
                    .add(MathUtils.checkNull(inqapntarget));

            return MathUtils.calcPercentComparison(b1, b2);
        } else {
            return BigDecimal.ZERO;
        }
    }


    public BigDecimal achievement(String generalId) {
        if (true) {
            return MathUtils.calcPercentComparison(totalActual, totalTargetYear);
        }

        if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_WDW_APP)) {
            return MathUtils.calcPercentComparison(wdwlocalfreq, wdwlocaltargetyear);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_INQ_APP)) {
            return MathUtils.calcPercentComparison(inqlocalfreq, inqlocaltargetyear);
        }  else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_PMT_APP)) {
            return MathUtils.calcPercentComparison(pmtlocalfreq, pmtlocaltargetyear);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_POS_APP)) {
            return MathUtils.calcPercentComparison(poslocalfreq, poslocaltargetyear);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TRF_APP)) {
            return MathUtils.calcPercentComparison(
                    MathUtils.checkNull(trflocalfreq).subtract(MathUtils.checkNull(trfinterfreq)), MathUtils.checkNull(trflocaltargetyear));
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TRF_INTER_APP)) {
            return MathUtils.calcPercentComparison(trfinterfreq, trfintertargetyear);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TOP_APP)) {
            return MathUtils.calcPercentComparison(toplocalfreq, toplocaltargetyear);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_ATM_DEC)) {
            return MathUtils.calcPercentComparison(declatmfreq, declatmtargetyear);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_POS_DEC)) {
            return MathUtils.calcPercentComparison(declposfreq, declpostargetyear);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_CUP_APP)) {
            return MathUtils.calcPercentComparison(
                    MathUtils.checkNull(wdwcupfreq).add(MathUtils.checkNull(inqcupfreq)),
                    MathUtils.checkNull(wdwcuptargetyear).add(MathUtils.checkNull(inqcuptargetyear)));
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_APN_APP)) {
            return MathUtils.calcPercentComparison(
                    MathUtils.checkNull(wdwapnfreq).add(MathUtils.checkNull(inqapnfreq)),
                    MathUtils.checkNull(wdwapntargetyear).add(MathUtils.checkNull(inqapntargetyear)));
        } else if (generalId.equalsIgnoreCase(DataConstant.ALL)) {
            BigDecimal b1 = MathUtils.checkNull(wdwlocalfreq)
                    .add(MathUtils.checkNull(inqlocalfreq))
                    .add(MathUtils.checkNull(pmtlocalfreq))
                    .add(MathUtils.checkNull(poslocalfreq))
                    .add(MathUtils.checkNull(trflocalfreq))
                    .subtract(MathUtils.checkNull(trfinterfreq))
                    .add(MathUtils.checkNull(trfinterfreq))
                    .add(MathUtils.checkNull(toplocalfreq))
                    .add(MathUtils.checkNull(declatmfreq))
                    .add(MathUtils.checkNull(declposfreq))
                    .add(MathUtils.checkNull(wdwcupfreq))
                    .add(MathUtils.checkNull(inqcupfreq))
                    .add(MathUtils.checkNull(wdwapnfreq))
                    .add(MathUtils.checkNull(inqapnfreq));

            BigDecimal b2 = MathUtils.checkNull(wdwlocaltargetyear)
                    .add(MathUtils.checkNull(inqlocaltargetyear))
                    .add(MathUtils.checkNull(pmtlocaltargetyear))
                    .add(MathUtils.checkNull(poslocaltargetyear))
                    .add(MathUtils.checkNull(trflocaltargetyear))
                    .add(MathUtils.checkNull(trfintertargetyear))
                    .add(MathUtils.checkNull(toplocaltargetyear))
                    .add(MathUtils.checkNull(declatmtargetyear))
                    .add(MathUtils.checkNull(declpostargetyear))
                    .add(MathUtils.checkNull(wdwcuptargetyear))
                    .add(MathUtils.checkNull(inqcuptargetyear))
                    .add(MathUtils.checkNull(wdwapntargetyear))
                    .add(MathUtils.checkNull(inqapntargetyear));

            return MathUtils.calcPercentComparison(b1, b2);
        } else {
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal frequencyDetailBank(String generalId) {
        if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_WDW_APP)) {
            return wdwlocalfreq;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_INQ_APP)) {
            return inqlocalfreq;
        }  else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_PMT_APP)) {
            return pmtlocalfreq;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_POS_APP)) {
            return poslocalfreq;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TRF_APP)) {
            return trflocalfreq.subtract(trfinterfreq);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TRF_INTER_APP)) {
            return trfinterfreq;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_TOP_APP)) {
            return toplocalfreq;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_ATM_DEC)) {
            return declatmfreq;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_POS_DEC)) {
            return declposfreq;
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_CUP_APP)) {
            return wdwcupfreq.add(inqcupfreq);
        } else if (generalId.equalsIgnoreCase(DataConstant.GLOBAL_APN_APP)) {
            return wdwapnfreq.add(inqapnfreq);
        } else if (generalId.equalsIgnoreCase(DataConstant.ALL)) {
            return wdwlocalfreq.add(inqlocalfreq).add(pmtlocalfreq).add(poslocalfreq)
                    .add(trflocalfreq).subtract(trfinterfreq)
                    .add(trfinterfreq).add(toplocalfreq).add(declatmfreq).add(declposfreq)
                    .add(wdwcupfreq).add(inqcupfreq).add(wdwapnfreq).add(inqapnfreq);
        } else {
            return BigDecimal.ZERO;
        }
    }

    @Override
    public int compareTo(GeneralSummaryDetailBean comparestu) {
        BigDecimal compareage = ((GeneralSummaryDetailBean)comparestu).getPct();
        /* For Ascending order*/
        //return this.pct.compareTo(compareage);

        /* For Descending order do like this */
        return compareage.compareTo(pct);
    }


    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public BigDecimal getWdwlocalfreq() {
        return wdwlocalfreq;
    }

    public void setWdwlocalfreq(BigDecimal wdwlocalfreq) {
        this.wdwlocalfreq = wdwlocalfreq;
    }

    public BigDecimal getInqlocalfreq() {
        return inqlocalfreq;
    }

    public void setInqlocalfreq(BigDecimal inqlocalfreq) {
        this.inqlocalfreq = inqlocalfreq;
    }

    public BigDecimal getPmtlocalfreq() {
        return pmtlocalfreq;
    }

    public void setPmtlocalfreq(BigDecimal pmtlocalfreq) {
        this.pmtlocalfreq = pmtlocalfreq;
    }

    public BigDecimal getPoslocalfreq() {
        return poslocalfreq;
    }

    public void setPoslocalfreq(BigDecimal poslocalfreq) {
        this.poslocalfreq = poslocalfreq;
    }

    public BigDecimal getTrflocalfreq() {
        return trflocalfreq;
    }

    public void setTrflocalfreq(BigDecimal trflocalfreq) {
        this.trflocalfreq = trflocalfreq;
    }

    public BigDecimal getTrfinterfreq() {
        return trfinterfreq;
    }

    public void setTrfinterfreq(BigDecimal trfinterfreq) {
        this.trfinterfreq = trfinterfreq;
    }

    public BigDecimal getToplocalfreq() {
        return toplocalfreq;
    }

    public void setToplocalfreq(BigDecimal toplocalfreq) {
        this.toplocalfreq = toplocalfreq;
    }

    public BigDecimal getDeclatmfreq() {
        return declatmfreq;
    }

    public void setDeclatmfreq(BigDecimal declatmfreq) {
        this.declatmfreq = declatmfreq;
    }

    public BigDecimal getDeclposfreq() {
        return declposfreq;
    }

    public void setDeclposfreq(BigDecimal declposfreq) {
        this.declposfreq = declposfreq;
    }

    public BigDecimal getInqcupfreq() {
        return inqcupfreq;
    }

    public void setInqcupfreq(BigDecimal inqcupfreq) {
        this.inqcupfreq = inqcupfreq;
    }

    public BigDecimal getWdwcupfreq() {
        return wdwcupfreq;
    }

    public void setWdwcupfreq(BigDecimal wdwcupfreq) {
        this.wdwcupfreq = wdwcupfreq;
    }

    public BigDecimal getInqapnfreq() {
        return inqapnfreq;
    }

    public void setInqapnfreq(BigDecimal inqapnfreq) {
        this.inqapnfreq = inqapnfreq;
    }

    public BigDecimal getWdwapnfreq() {
        return wdwapnfreq;
    }

    public void setWdwapnfreq(BigDecimal wdwapnfreq) {
        this.wdwapnfreq = wdwapnfreq;
    }

    public BigDecimal getTotalother() {
        return totalother;
    }

    public void setTotalother(BigDecimal totalother) {
        this.totalother = totalother;
    }

    public BigDecimal getWdwlocaltarget() {
        return wdwlocaltarget;
    }

    public void setWdwlocaltarget(BigDecimal wdwlocaltarget) {
        this.wdwlocaltarget = wdwlocaltarget;
    }

    public BigDecimal getInqlocaltarget() {
        return inqlocaltarget;
    }

    public void setInqlocaltarget(BigDecimal inqlocaltarget) {
        this.inqlocaltarget = inqlocaltarget;
    }

    public BigDecimal getPmtlocaltarget() {
        return pmtlocaltarget;
    }

    public void setPmtlocaltarget(BigDecimal pmtlocaltarget) {
        this.pmtlocaltarget = pmtlocaltarget;
    }

    public BigDecimal getPoslocaltarget() {
        return poslocaltarget;
    }

    public void setPoslocaltarget(BigDecimal poslocaltarget) {
        this.poslocaltarget = poslocaltarget;
    }

    public BigDecimal getTrflocaltarget() {
        return trflocaltarget;
    }

    public void setTrflocaltarget(BigDecimal trflocaltarget) {
        this.trflocaltarget = trflocaltarget;
    }

    public BigDecimal getTrfintertarget() {
        return trfintertarget;
    }

    public void setTrfintertarget(BigDecimal trfintertarget) {
        this.trfintertarget = trfintertarget;
    }

    public BigDecimal getToplocaltarget() {
        return toplocaltarget;
    }

    public void setToplocaltarget(BigDecimal toplocaltarget) {
        this.toplocaltarget = toplocaltarget;
    }

    public BigDecimal getDeclatmtarget() {
        return declatmtarget;
    }

    public void setDeclatmtarget(BigDecimal declatmtarget) {
        this.declatmtarget = declatmtarget;
    }

    public BigDecimal getDeclpostarget() {
        return declpostarget;
    }

    public void setDeclpostarget(BigDecimal declpostarget) {
        this.declpostarget = declpostarget;
    }

    public BigDecimal getInqcuptarget() {
        return inqcuptarget;
    }

    public void setInqcuptarget(BigDecimal inqcuptarget) {
        this.inqcuptarget = inqcuptarget;
    }

    public BigDecimal getWdwcuptarget() {
        return wdwcuptarget;
    }

    public void setWdwcuptarget(BigDecimal wdwcuptarget) {
        this.wdwcuptarget = wdwcuptarget;
    }

    public BigDecimal getInqapntarget() {
        return inqapntarget;
    }

    public void setInqapntarget(BigDecimal inqapntarget) {
        this.inqapntarget = inqapntarget;
    }

    public BigDecimal getWdwapntarget() {
        return wdwapntarget;
    }

    public void setWdwapntarget(BigDecimal wdwapntarget) {
        this.wdwapntarget = wdwapntarget;
    }

    public BigDecimal getTotalothertarget() {
        return totalothertarget;
    }

    public void setTotalothertarget(BigDecimal totalothertarget) {
        this.totalothertarget = totalothertarget;
    }

    public BigDecimal getWdwlocaltargetyear() {
        return wdwlocaltargetyear;
    }

    public void setWdwlocaltargetyear(BigDecimal wdwlocaltargetyear) {
        this.wdwlocaltargetyear = wdwlocaltargetyear;
    }

    public BigDecimal getInqlocaltargetyear() {
        return inqlocaltargetyear;
    }

    public void setInqlocaltargetyear(BigDecimal inqlocaltargetyear) {
        this.inqlocaltargetyear = inqlocaltargetyear;
    }

    public BigDecimal getPmtlocaltargetyear() {
        return pmtlocaltargetyear;
    }

    public void setPmtlocaltargetyear(BigDecimal pmtlocaltargetyear) {
        this.pmtlocaltargetyear = pmtlocaltargetyear;
    }

    public BigDecimal getPoslocaltargetyear() {
        return poslocaltargetyear;
    }

    public void setPoslocaltargetyear(BigDecimal poslocaltargetyear) {
        this.poslocaltargetyear = poslocaltargetyear;
    }

    public BigDecimal getTrflocaltargetyear() {
        return trflocaltargetyear;
    }

    public void setTrflocaltargetyear(BigDecimal trflocaltargetyear) {
        this.trflocaltargetyear = trflocaltargetyear;
    }

    public BigDecimal getTrfintertargetyear() {
        return trfintertargetyear;
    }

    public void setTrfintertargetyear(BigDecimal trfintertargetyear) {
        this.trfintertargetyear = trfintertargetyear;
    }

    public BigDecimal getToplocaltargetyear() {
        return toplocaltargetyear;
    }

    public void setToplocaltargetyear(BigDecimal toplocaltargetyear) {
        this.toplocaltargetyear = toplocaltargetyear;
    }

    public BigDecimal getDeclatmtargetyear() {
        return declatmtargetyear;
    }

    public void setDeclatmtargetyear(BigDecimal declatmtargetyear) {
        this.declatmtargetyear = declatmtargetyear;
    }

    public BigDecimal getDeclpostargetyear() {
        return declpostargetyear;
    }

    public void setDeclpostargetyear(BigDecimal declpostargetyear) {
        this.declpostargetyear = declpostargetyear;
    }

    public BigDecimal getInqcuptargetyear() {
        return inqcuptargetyear;
    }

    public void setInqcuptargetyear(BigDecimal inqcuptargetyear) {
        this.inqcuptargetyear = inqcuptargetyear;
    }

    public BigDecimal getWdwcuptargetyear() {
        return wdwcuptargetyear;
    }

    public void setWdwcuptargetyear(BigDecimal wdwcuptargetyear) {
        this.wdwcuptargetyear = wdwcuptargetyear;
    }

    public BigDecimal getInqapntargetyear() {
        return inqapntargetyear;
    }

    public void setInqapntargetyear(BigDecimal inqapntargetyear) {
        this.inqapntargetyear = inqapntargetyear;
    }

    public BigDecimal getWdwapntargetyear() {
        return wdwapntargetyear;
    }

    public void setWdwapntargetyear(BigDecimal wdwapntargetyear) {
        this.wdwapntargetyear = wdwapntargetyear;
    }

    public BigDecimal getTotalothertargetyear() {
        return totalothertargetyear;
    }

    public void setTotalothertargetyear(BigDecimal totalothertargetyear) {
        this.totalothertargetyear = totalothertargetyear;
    }

    public BigDecimal getTotalgrowth() {
        return totalgrowth;
    }

    public void setTotalgrowth(BigDecimal totalgrowth) {
        this.totalgrowth = totalgrowth;
    }

    public BigDecimal getTotalachievement() {
        return totalachievement;
    }

    public void setTotalachievement(BigDecimal totalachievement) {
        this.totalachievement = totalachievement;
    }

    public BigDecimal getPct() {
        return pct;
    }

    public void setPct(BigDecimal pct) {
        this.pct = pct;
    }

    public String getBankidiss() {
        return bankidiss;
    }

    public void setBankidiss(String bankidiss) {
        this.bankidiss = bankidiss;
    }

    public String getBanknameiss() {
        return banknameiss;
    }

    public void setBanknameiss(String banknameiss) {
        this.banknameiss = banknameiss;
    }

    public String getBankidaq() {
        return bankidaq;
    }

    public void setBankidaq(String bankidaq) {
        this.bankidaq = bankidaq;
    }

    public String getBanknameacq() {
        return banknameacq;
    }

    public void setBanknameacq(String banknameacq) {
        this.banknameacq = banknameacq;
    }

    public BigDecimal getTotalActual() {
        return totalActual;
    }

    public void setTotalActual(BigDecimal totalActual) {
        this.totalActual = totalActual;
    }

    public BigDecimal getTotalTarget() {
        return totalTarget;
    }

    public void setTotalTarget(BigDecimal totalTarget) {
        this.totalTarget = totalTarget;
    }

    public BigDecimal getTotalTargetYear() {
        return totalTargetYear;
    }

    public void setTotalTargetYear(BigDecimal totalTargetYear) {
        this.totalTargetYear = totalTargetYear;
    }
}

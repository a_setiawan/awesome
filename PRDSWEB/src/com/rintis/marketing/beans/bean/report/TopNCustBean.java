package com.rintis.marketing.beans.bean.report;


import java.math.BigDecimal;
import java.util.Comparator;

public class TopNCustBean {
    private String bankname;
    private String bankid;
    private String billername;
    private String billerid;

    private BigDecimal actualm1;
    private BigDecimal actualm2;
    private BigDecimal actualm3;
    private BigDecimal actualm4;
    private BigDecimal actualm5;
    private BigDecimal actualm6;
    private BigDecimal actualm7;
    private BigDecimal actualm8;
    private BigDecimal actualm9;
    private BigDecimal actualm10;
    private BigDecimal actualm11;
    private BigDecimal actualm12;

    private BigDecimal targetm1;
    private BigDecimal targetm2;
    private BigDecimal targetm3;
    private BigDecimal targetm4;
    private BigDecimal targetm5;
    private BigDecimal targetm6;
    private BigDecimal targetm7;
    private BigDecimal targetm8;
    private BigDecimal targetm9;
    private BigDecimal targetm10;
    private BigDecimal targetm11;
    private BigDecimal targetm12;

    private BigDecimal actualCurrMonthVsActualPrevMonth;
    private BigDecimal actualMonthVsTargetMonth;
    private BigDecimal actualMtdVsTargetMtd;
    private BigDecimal actualMtdVsTotalTarget;







    public BigDecimal getActualm1() {
        return actualm1;
    }

    public void setActualm1(BigDecimal actualm1) {
        this.actualm1 = actualm1;
    }

    public BigDecimal getActualm2() {
        return actualm2;
    }

    public void setActualm2(BigDecimal actualm2) {
        this.actualm2 = actualm2;
    }

    public BigDecimal getActualm3() {
        return actualm3;
    }

    public void setActualm3(BigDecimal actualm3) {
        this.actualm3 = actualm3;
    }

    public BigDecimal getActualm4() {
        return actualm4;
    }

    public void setActualm4(BigDecimal actualm4) {
        this.actualm4 = actualm4;
    }

    public BigDecimal getActualm5() {
        return actualm5;
    }

    public void setActualm5(BigDecimal actualm5) {
        this.actualm5 = actualm5;
    }

    public BigDecimal getActualm6() {
        return actualm6;
    }

    public void setActualm6(BigDecimal actualm6) {
        this.actualm6 = actualm6;
    }

    public BigDecimal getActualm7() {
        return actualm7;
    }

    public void setActualm7(BigDecimal actualm7) {
        this.actualm7 = actualm7;
    }

    public BigDecimal getActualm8() {
        return actualm8;
    }

    public void setActualm8(BigDecimal actualm8) {
        this.actualm8 = actualm8;
    }

    public BigDecimal getActualm9() {
        return actualm9;
    }

    public void setActualm9(BigDecimal actualm9) {
        this.actualm9 = actualm9;
    }

    public BigDecimal getActualm10() {
        return actualm10;
    }

    public void setActualm10(BigDecimal actualm10) {
        this.actualm10 = actualm10;
    }

    public BigDecimal getActualm11() {
        return actualm11;
    }

    public void setActualm11(BigDecimal actualm11) {
        this.actualm11 = actualm11;
    }

    public BigDecimal getActualm12() {
        return actualm12;
    }

    public void setActualm12(BigDecimal actualm12) {
        this.actualm12 = actualm12;
    }

    public BigDecimal getTargetm1() {
        return targetm1;
    }

    public void setTargetm1(BigDecimal targetm1) {
        this.targetm1 = targetm1;
    }

    public BigDecimal getTargetm2() {
        return targetm2;
    }

    public void setTargetm2(BigDecimal targetm2) {
        this.targetm2 = targetm2;
    }

    public BigDecimal getTargetm3() {
        return targetm3;
    }

    public void setTargetm3(BigDecimal targetm3) {
        this.targetm3 = targetm3;
    }

    public BigDecimal getTargetm4() {
        return targetm4;
    }

    public void setTargetm4(BigDecimal targetm4) {
        this.targetm4 = targetm4;
    }

    public BigDecimal getTargetm5() {
        return targetm5;
    }

    public void setTargetm5(BigDecimal targetm5) {
        this.targetm5 = targetm5;
    }

    public BigDecimal getTargetm6() {
        return targetm6;
    }

    public void setTargetm6(BigDecimal targetm6) {
        this.targetm6 = targetm6;
    }

    public BigDecimal getTargetm7() {
        return targetm7;
    }

    public void setTargetm7(BigDecimal targetm7) {
        this.targetm7 = targetm7;
    }

    public BigDecimal getTargetm8() {
        return targetm8;
    }

    public void setTargetm8(BigDecimal targetm8) {
        this.targetm8 = targetm8;
    }

    public BigDecimal getTargetm9() {
        return targetm9;
    }

    public void setTargetm9(BigDecimal targetm9) {
        this.targetm9 = targetm9;
    }

    public BigDecimal getTargetm10() {
        return targetm10;
    }

    public void setTargetm10(BigDecimal targetm10) {
        this.targetm10 = targetm10;
    }

    public BigDecimal getTargetm11() {
        return targetm11;
    }

    public void setTargetm11(BigDecimal targetm11) {
        this.targetm11 = targetm11;
    }

    public BigDecimal getTargetm12() {
        return targetm12;
    }

    public void setTargetm12(BigDecimal targetm12) {
        this.targetm12 = targetm12;
    }

    public BigDecimal getActualCurrMonthVsActualPrevMonth() {
        return actualCurrMonthVsActualPrevMonth;
    }

    public void setActualCurrMonthVsActualPrevMonth(BigDecimal actualCurrMonthVsActualPrevMonth) {
        this.actualCurrMonthVsActualPrevMonth = actualCurrMonthVsActualPrevMonth;
    }

    public BigDecimal getActualMonthVsTargetMonth() {
        return actualMonthVsTargetMonth;
    }

    public void setActualMonthVsTargetMonth(BigDecimal actualMonthVsTargetMonth) {
        this.actualMonthVsTargetMonth = actualMonthVsTargetMonth;
    }

    public BigDecimal getActualMtdVsTargetMtd() {
        return actualMtdVsTargetMtd;
    }

    public void setActualMtdVsTargetMtd(BigDecimal actualMtdVsTargetMtd) {
        this.actualMtdVsTargetMtd = actualMtdVsTargetMtd;
    }

    public BigDecimal getActualMtdVsTotalTarget() {
        return actualMtdVsTotalTarget;
    }

    public void setActualMtdVsTotalTarget(BigDecimal actualMtdVsTotalTarget) {
        this.actualMtdVsTotalTarget = actualMtdVsTotalTarget;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getBillername() {
        return billername;
    }

    public void setBillername(String billername) {
        this.billername = billername;
    }

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getBillerid() {
        return billerid;
    }

    public void setBillerid(String billerid) {
        this.billerid = billerid;
    }
}

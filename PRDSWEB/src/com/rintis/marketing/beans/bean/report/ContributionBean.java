package com.rintis.marketing.beans.bean.report;

import java.math.BigDecimal;

public class ContributionBean implements Comparable<ContributionBean> {
    private String bankid;
    private String bankname;
    private BigDecimal withdrawal;
    private BigDecimal balinquiry;
    private BigDecimal transfer;
    private BigDecimal payment;
    private BigDecimal topup;
    private BigDecimal decline;
    private BigDecimal debitappr;
    private BigDecimal debitdecl;

    private BigDecimal totalatm;
    private BigDecimal total;

    @Override
    public int compareTo(ContributionBean comparestu) {
        BigDecimal compareage = ((ContributionBean)comparestu).getTotal();
        /* For Ascending order*/
        //return this.pct.compareTo(compareage);

        /* For Descending order do like this */
        return compareage.compareTo(total);
    }


    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public BigDecimal getWithdrawal() {
        return withdrawal;
    }

    public void setWithdrawal(BigDecimal withdrawal) {
        this.withdrawal = withdrawal;
    }

    public BigDecimal getBalinquiry() {
        return balinquiry;
    }

    public void setBalinquiry(BigDecimal balinquiry) {
        this.balinquiry = balinquiry;
    }

    public BigDecimal getTransfer() {
        return transfer;
    }

    public void setTransfer(BigDecimal transfer) {
        this.transfer = transfer;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public BigDecimal getTopup() {
        return topup;
    }

    public void setTopup(BigDecimal topup) {
        this.topup = topup;
    }

    public BigDecimal getDecline() {
        return decline;
    }

    public void setDecline(BigDecimal decline) {
        this.decline = decline;
    }

    public BigDecimal getDebitappr() {
        return debitappr;
    }

    public void setDebitappr(BigDecimal debitappr) {
        this.debitappr = debitappr;
    }

    public BigDecimal getDebitdecl() {
        return debitdecl;
    }

    public void setDebitdecl(BigDecimal debitdecl) {
        this.debitdecl = debitdecl;
    }

    public BigDecimal getTotalatm() {
        return totalatm;
    }

    public void setTotalatm(BigDecimal totalatm) {
        this.totalatm = totalatm;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}

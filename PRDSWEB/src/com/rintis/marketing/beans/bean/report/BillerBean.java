package com.rintis.marketing.beans.bean.report;


public class BillerBean {
    private String billerid;
    private String billername;
    private String billerfolder;
    private String billeractivestatus;
    private String billerfiid;

    public String getBillerid() {
        return billerid;
    }

    public void setBillerid(String billerid) {
        this.billerid = billerid;
    }

    public String getBillername() {
        return billername;
    }

    public void setBillername(String billername) {
        this.billername = billername;
    }

    public String getBillerfolder() {
        return billerfolder;
    }

    public void setBillerfolder(String billerfolder) {
        this.billerfolder = billerfolder;
    }

    public String getBilleractivestatus() {
        return billeractivestatus;
    }

    public void setBilleractivestatus(String billeractivestatus) {
        this.billeractivestatus = billeractivestatus;
    }

    public String getBillerfiid() {
        return billerfiid;
    }

    public void setBillerfiid(String billerfiid) {
        this.billerfiid = billerfiid;
    }
}

package com.rintis.marketing.beans.bean.report;

import java.math.BigDecimal;

public class PlanVsActualBean {
    private String transactionid;
    private String transactionname;
    private int year;
    private String smonth;
    private int month;
    private BigDecimal actualPrevYear;
    private BigDecimal actual;
    private BigDecimal target;
    private BigDecimal diff;
    private BigDecimal pct;
    private BigDecimal pctGrowth;

    private String q;

    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public String getTransactionname() {
        return transactionname;
    }

    public void setTransactionname(String transactionname) {
        this.transactionname = transactionname;
    }

    public String getSmonth() {
        return smonth;
    }

    public void setSmonth(String smonth) {
        this.smonth = smonth;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public BigDecimal getActual() {
        return actual;
    }

    public void setActual(BigDecimal actual) {
        this.actual = actual;
    }

    public BigDecimal getTarget() {
        return target;
    }

    public void setTarget(BigDecimal target) {
        this.target = target;
    }

    public BigDecimal getDiff() {
        return diff;
    }

    public void setDiff(BigDecimal diff) {
        this.diff = diff;
    }

    public BigDecimal getPct() {
        return pct;
    }

    public void setPct(BigDecimal pct) {
        this.pct = pct;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public BigDecimal getActualPrevYear() {
        return actualPrevYear;
    }

    public void setActualPrevYear(BigDecimal actualPrevYear) {
        this.actualPrevYear = actualPrevYear;
    }

    public BigDecimal getPctGrowth() {
        return pctGrowth;
    }

    public void setPctGrowth(BigDecimal pctGrowth) {
        this.pctGrowth = pctGrowth;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }
}

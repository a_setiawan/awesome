package com.rintis.marketing.beans.bean.report;


import java.math.BigDecimal;
import java.util.Date;

public class ReportBean {
    private String syear;
    private Integer year;
    private String smonth;
    private String smonthtext;
    private BigDecimal totalfreq;
    private BigDecimal totalamount;
    private BigDecimal totalfee;
    private BigDecimal totalpmt;

    private BigDecimal prevTotalfreq;
    private BigDecimal prevTotalfee;

    private BigDecimal percentGrowthFreq;
    private BigDecimal percentGrowthFee;
    private BigDecimal percentGrowthFreqPrevYr;
    private BigDecimal percentGrowthFeePrevYr;

    private BigDecimal totalfreqAtm;
    private BigDecimal totalfreqPos;
    private BigDecimal totalfreqPmt;

    private BigDecimal totalfreqpmtappr;
    private BigDecimal totalfreqpmtdecl;

    private BigDecimal prevTotalfreqAtm;
    private BigDecimal prevTotalfreqPos;
    private BigDecimal prevTotalfreqPmt;

    private BigDecimal prevTotalfreqWith;
    private BigDecimal prevTotalfreqDebit;
    private BigDecimal prevTotalfreqTransf;

    private BigDecimal percentGrowthFreqAtm;
    private BigDecimal percentGrowthFreqPos;
    private BigDecimal percentGrowthFreqPmt;
    private BigDecimal percentGrowthFreqAtmPrevYr;
    private BigDecimal percentGrowthFreqPosPrevYr;
    private BigDecimal percentGrowthFreqPmtPrevYr;

    private BigDecimal totalamountAtm;
    private BigDecimal totalamountPos;
    private BigDecimal totalamountPmt;
    private BigDecimal totalamountWithdrawal;
    private BigDecimal totalamountTransfer;

    private BigDecimal percentGrowthAtm;
    private BigDecimal percentGrowthPos;
    private BigDecimal percentGrowthPmt;
    private BigDecimal percentGrowthAtmPrevYr;
    private BigDecimal percentGrowthPosPrevYr;
    private BigDecimal percentGrowthPmtPrevYr;

    private BigDecimal totalfeeAtm;
    private BigDecimal totalfeePos;
    private BigDecimal totalfeePmt;

    private BigDecimal prevTotalfeeAtm;
    private BigDecimal prevTotalfeePos;
    private BigDecimal prevTotalfeePmt;

    private String bankname;
    private String bankid;

    private BigDecimal totalfreqwith;
    private BigDecimal totalfreqbalinq;
    private BigDecimal totalfreqtransf;
    private BigDecimal totalfreqdecl;
    private BigDecimal totalfreqpaym;
    private BigDecimal totalfreqapprv;
    private BigDecimal totalfreqdebit;

    private BigDecimal totalfeewith;
    private BigDecimal totalfeebalinq;
    private BigDecimal totalfeetransf;

    private BigDecimal totalfeeappr;
    private BigDecimal totalfeeapprvoid;
    private BigDecimal totalfeedecl;
    private BigDecimal totalfeedeclvoid;
    private BigDecimal totalfeeposappr;
    private BigDecimal totalfeeposdecl;

    private BigDecimal percentslabank;
    private BigDecimal percentslarintis;

    private String periodyear;
    private String periodmonth;
    private String groupid;
    private String groupname;
    private String problemno;
    private String bankfolder;
    private String typeid;
    private String typename;
    private String listid;
    private String listname;
    private Date begindate;
    private Date enddate;
    private BigDecimal duration;
    private String description;
    private String transaction;
    private String declinecode;
    private BigDecimal frequency;
    private String rtcd;

    private String billerid;
    private String billername;
    private BigDecimal totalapprpmtamount;
    private BigDecimal totaldeclpmtamount;

    private String bankidacq;
    private String bankidiss;
    private String bankidbnf;
    private String banknameacq;
    private String banknameiss;
    private String banknamebnf;
    private String acqcode;
    private String isscode;
    private String bnfcode;
    private String banknameintacq;
    private String banknameintiss;
    private String banknameintbnf;

    private BigDecimal percentGrowthFreqWith;
    private BigDecimal percentGrowthFreqDebit;
    private BigDecimal percentGrowthFreqTransf;

    private String intercon;


    public String getDurationTimeFormat() {
        long totalSecs = 0;
        String timeString = "";
        try {
            totalSecs = duration.longValue();
            long hours = totalSecs / 3600;
            long minutes = (totalSecs % 3600) / 60;
            long seconds = totalSecs % 60;

            timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return timeString;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public BigDecimal getTotalfreq() {
        return totalfreq;
    }

    public void setTotalfreq(BigDecimal totalfreq) {
        this.totalfreq = totalfreq;
    }

    public BigDecimal getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(BigDecimal totalamount) {
        this.totalamount = totalamount;
    }

    public BigDecimal getTotalfee() {
        return totalfee;
    }

    public void setTotalfee(BigDecimal totalfee) {
        this.totalfee = totalfee;
    }

    public BigDecimal getPercentGrowthFreq() {
        return percentGrowthFreq;
    }

    public void setPercentGrowthFreq(BigDecimal percentGrowthFreq) {
        this.percentGrowthFreq = percentGrowthFreq;
    }

    public BigDecimal getPercentGrowthFee() {
        return percentGrowthFee;
    }

    public void setPercentGrowthFee(BigDecimal percentGrowthFee) {
        this.percentGrowthFee = percentGrowthFee;
    }

    public String getSyear() {
        return syear;
    }

    public void setSyear(String syear) {
        this.syear = syear;
    }

    public String getSmonth() {
        return smonth;
    }

    public void setSmonth(String smonth) {
        this.smonth = smonth;
    }

    public BigDecimal getTotalfreqAtm() {
        return totalfreqAtm;
    }

    public void setTotalfreqAtm(BigDecimal totalfreqAtm) {
        this.totalfreqAtm = totalfreqAtm;
    }

    public BigDecimal getTotalfreqPos() {
        return totalfreqPos;
    }

    public void setTotalfreqPos(BigDecimal totalfreqPos) {
        this.totalfreqPos = totalfreqPos;
    }

    public BigDecimal getTotalfreqPmt() {
        return totalfreqPmt;
    }

    public void setTotalfreqPmt(BigDecimal totalfreqPmt) {
        this.totalfreqPmt = totalfreqPmt;
    }

    public BigDecimal getPercentGrowthFreqAtm() {
        return percentGrowthFreqAtm;
    }

    public void setPercentGrowthFreqAtm(BigDecimal percentGrowthFreqAtm) {
        this.percentGrowthFreqAtm = percentGrowthFreqAtm;
    }

    public BigDecimal getPercentGrowthFreqPos() {
        return percentGrowthFreqPos;
    }

    public void setPercentGrowthFreqPos(BigDecimal percentGrowthFreqPos) {
        this.percentGrowthFreqPos = percentGrowthFreqPos;
    }

    public BigDecimal getPercentGrowthFreqPmt() {
        return percentGrowthFreqPmt;
    }

    public void setPercentGrowthFreqPmt(BigDecimal percentGrowthFreqPmt) {
        this.percentGrowthFreqPmt = percentGrowthFreqPmt;
    }

    public BigDecimal getPercentGrowthPmt() {
        return percentGrowthPmt;
    }

    public void setPercentGrowthPmt(BigDecimal percentGrowthPmt) {
        this.percentGrowthPmt = percentGrowthPmt;
    }

    public BigDecimal getTotalpmt() {
        return totalpmt;
    }

    public void setTotalpmt(BigDecimal totalpmt) {
        this.totalpmt = totalpmt;
    }

    public BigDecimal getTotalamountAtm() {
        return totalamountAtm;
    }

    public void setTotalamountAtm(BigDecimal totalamountAtm) {
        this.totalamountAtm = totalamountAtm;
    }

    public BigDecimal getTotalamountPos() {
        return totalamountPos;
    }

    public void setTotalamountPos(BigDecimal totalamountPos) {
        this.totalamountPos = totalamountPos;
    }

    public BigDecimal getTotalamountPmt() {
        return totalamountPmt;
    }

    public void setTotalamountPmt(BigDecimal totalamountPmt) {
        this.totalamountPmt = totalamountPmt;
    }

    public BigDecimal getPercentGrowthAtm() {
        return percentGrowthAtm;
    }

    public void setPercentGrowthAtm(BigDecimal percentGrowthAtm) {
        this.percentGrowthAtm = percentGrowthAtm;
    }

    public BigDecimal getPercentGrowthPos() {
        return percentGrowthPos;
    }

    public void setPercentGrowthPos(BigDecimal percentGrowthPos) {
        this.percentGrowthPos = percentGrowthPos;
    }

    public BigDecimal getTotalfeeAtm() {
        return totalfeeAtm;
    }

    public void setTotalfeeAtm(BigDecimal totalfeeAtm) {
        this.totalfeeAtm = totalfeeAtm;
    }

    public BigDecimal getTotalfeePos() {
        return totalfeePos;
    }

    public void setTotalfeePos(BigDecimal totalfeePos) {
        this.totalfeePos = totalfeePos;
    }

    public BigDecimal getTotalfeePmt() {
        return totalfeePmt;
    }

    public void setTotalfeePmt(BigDecimal totalfeePmt) {
        this.totalfeePmt = totalfeePmt;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public BigDecimal getTotalfreqwith() {
        return totalfreqwith;
    }

    public void setTotalfreqwith(BigDecimal totalfreqwith) {
        this.totalfreqwith = totalfreqwith;
    }

    public BigDecimal getTotalfreqbalinq() {
        return totalfreqbalinq;
    }

    public void setTotalfreqbalinq(BigDecimal totalfreqbalinq) {
        this.totalfreqbalinq = totalfreqbalinq;
    }

    public BigDecimal getTotalfreqtransf() {
        return totalfreqtransf;
    }

    public void setTotalfreqtransf(BigDecimal totalfreqtransf) {
        this.totalfreqtransf = totalfreqtransf;
    }

    public BigDecimal getTotalfreqdecl() {
        return totalfreqdecl;
    }

    public void setTotalfreqdecl(BigDecimal totalfreqdecl) {
        this.totalfreqdecl = totalfreqdecl;
    }

    public BigDecimal getTotalfeeappr() {
        return totalfeeappr;
    }

    public void setTotalfeeappr(BigDecimal totalfeeappr) {
        this.totalfeeappr = totalfeeappr;
    }

    public BigDecimal getTotalfeeapprvoid() {
        return totalfeeapprvoid;
    }

    public void setTotalfeeapprvoid(BigDecimal totalfeeapprvoid) {
        this.totalfeeapprvoid = totalfeeapprvoid;
    }

    public BigDecimal getTotalfeedecl() {
        return totalfeedecl;
    }

    public void setTotalfeedecl(BigDecimal totalfeedecl) {
        this.totalfeedecl = totalfeedecl;
    }

    public BigDecimal getTotalfeedeclvoid() {
        return totalfeedeclvoid;
    }

    public void setTotalfeedeclvoid(BigDecimal totalfeedeclvoid) {
        this.totalfeedeclvoid = totalfeedeclvoid;
    }

    public BigDecimal getTotalfeewith() {
        return totalfeewith;
    }

    public void setTotalfeewith(BigDecimal totalfeewith) {
        this.totalfeewith = totalfeewith;
    }

    public BigDecimal getTotalfeebalinq() {
        return totalfeebalinq;
    }

    public void setTotalfeebalinq(BigDecimal totalfeebalinq) {
        this.totalfeebalinq = totalfeebalinq;
    }

    public BigDecimal getTotalfeetransf() {
        return totalfeetransf;
    }

    public void setTotalfeetransf(BigDecimal totalfeetransf) {
        this.totalfeetransf = totalfeetransf;
    }

    public BigDecimal getTotalfreqpaym() {
        return totalfreqpaym;
    }

    public void setTotalfreqpaym(BigDecimal totalfreqpaym) {
        this.totalfreqpaym = totalfreqpaym;
    }

    public BigDecimal getPercentGrowthFreqPrevYr() {
        return percentGrowthFreqPrevYr;
    }

    public void setPercentGrowthFreqPrevYr(BigDecimal percentGrowthFreqPrevYr) {
        this.percentGrowthFreqPrevYr = percentGrowthFreqPrevYr;
    }

    public BigDecimal getPercentGrowthFeePrevYr() {
        return percentGrowthFeePrevYr;
    }

    public void setPercentGrowthFeePrevYr(BigDecimal percentGrowthFeePrevYr) {
        this.percentGrowthFeePrevYr = percentGrowthFeePrevYr;
    }

    public BigDecimal getPercentGrowthFreqAtmPrevYr() {
        return percentGrowthFreqAtmPrevYr;
    }

    public void setPercentGrowthFreqAtmPrevYr(BigDecimal percentGrowthFreqAtmPrevYr) {
        this.percentGrowthFreqAtmPrevYr = percentGrowthFreqAtmPrevYr;
    }

    public BigDecimal getPercentGrowthFreqPosPrevYr() {
        return percentGrowthFreqPosPrevYr;
    }

    public void setPercentGrowthFreqPosPrevYr(BigDecimal percentGrowthFreqPosPrevYr) {
        this.percentGrowthFreqPosPrevYr = percentGrowthFreqPosPrevYr;
    }

    public BigDecimal getPercentGrowthFreqPmtPrevYr() {
        return percentGrowthFreqPmtPrevYr;
    }

    public void setPercentGrowthFreqPmtPrevYr(BigDecimal percentGrowthFreqPmtPrevYr) {
        this.percentGrowthFreqPmtPrevYr = percentGrowthFreqPmtPrevYr;
    }

    public BigDecimal getPercentGrowthAtmPrevYr() {
        return percentGrowthAtmPrevYr;
    }

    public void setPercentGrowthAtmPrevYr(BigDecimal percentGrowthAtmPrevYr) {
        this.percentGrowthAtmPrevYr = percentGrowthAtmPrevYr;
    }

    public BigDecimal getPercentGrowthPosPrevYr() {
        return percentGrowthPosPrevYr;
    }

    public void setPercentGrowthPosPrevYr(BigDecimal percentGrowthPosPrevYr) {
        this.percentGrowthPosPrevYr = percentGrowthPosPrevYr;
    }

    public BigDecimal getPercentGrowthPmtPrevYr() {
        return percentGrowthPmtPrevYr;
    }

    public void setPercentGrowthPmtPrevYr(BigDecimal percentGrowthPmtPrevYr) {
        this.percentGrowthPmtPrevYr = percentGrowthPmtPrevYr;
    }

    public String getSmonthtext() {
        return smonthtext;
    }

    public void setSmonthtext(String smonthtext) {
        this.smonthtext = smonthtext;
    }

    public BigDecimal getPercentslabank() {
        return percentslabank;
    }

    public void setPercentslabank(BigDecimal percentslabank) {
        this.percentslabank = percentslabank;
    }

    public BigDecimal getPercentslarintis() {
        return percentslarintis;
    }

    public void setPercentslarintis(BigDecimal percentslarintis) {
        this.percentslarintis = percentslarintis;
    }

    public BigDecimal getTotalfreqapprv() {
        return totalfreqapprv;
    }

    public void setTotalfreqapprv(BigDecimal totalfreqapprv) {
        this.totalfreqapprv = totalfreqapprv;
    }

    public BigDecimal getPrevTotalfreq() {
        return prevTotalfreq;
    }

    public void setPrevTotalfreq(BigDecimal prevTotalfreq) {
        this.prevTotalfreq = prevTotalfreq;
    }

    public BigDecimal getPrevTotalfreqAtm() {
        return prevTotalfreqAtm;
    }

    public void setPrevTotalfreqAtm(BigDecimal prevTotalfreqAtm) {
        this.prevTotalfreqAtm = prevTotalfreqAtm;
    }

    public BigDecimal getPrevTotalfreqPos() {
        return prevTotalfreqPos;
    }

    public void setPrevTotalfreqPos(BigDecimal prevTotalfreqPos) {
        this.prevTotalfreqPos = prevTotalfreqPos;
    }

    public BigDecimal getPrevTotalfreqPmt() {
        return prevTotalfreqPmt;
    }

    public void setPrevTotalfreqPmt(BigDecimal prevTotalfreqPmt) {
        this.prevTotalfreqPmt = prevTotalfreqPmt;
    }

    public BigDecimal getPrevTotalfee() {
        return prevTotalfee;
    }

    public void setPrevTotalfee(BigDecimal prevTotalfee) {
        this.prevTotalfee = prevTotalfee;
    }

    public BigDecimal getPrevTotalfeeAtm() {
        return prevTotalfeeAtm;
    }

    public void setPrevTotalfeeAtm(BigDecimal prevTotalfeeAtm) {
        this.prevTotalfeeAtm = prevTotalfeeAtm;
    }

    public BigDecimal getPrevTotalfeePos() {
        return prevTotalfeePos;
    }

    public void setPrevTotalfeePos(BigDecimal prevTotalfeePos) {
        this.prevTotalfeePos = prevTotalfeePos;
    }

    public BigDecimal getPrevTotalfeePmt() {
        return prevTotalfeePmt;
    }

    public void setPrevTotalfeePmt(BigDecimal prevTotalfeePmt) {
        this.prevTotalfeePmt = prevTotalfeePmt;
    }

    public String getPeriodyear() {
        return periodyear;
    }

    public void setPeriodyear(String periodyear) {
        this.periodyear = periodyear;
    }

    public String getPeriodmonth() {
        return periodmonth;
    }

    public void setPeriodmonth(String periodmonth) {
        this.periodmonth = periodmonth;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getProblemno() {
        return problemno;
    }

    public void setProblemno(String problemno) {
        this.problemno = problemno;
    }

    public String getBankfolder() {
        return bankfolder;
    }

    public void setBankfolder(String bankfolder) {
        this.bankfolder = bankfolder;
    }

    public String getTypeid() {
        return typeid;
    }

    public void setTypeid(String typeid) {
        this.typeid = typeid;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public String getListid() {
        return listid;
    }

    public void setListid(String listid) {
        this.listid = listid;
    }

    public String getListname() {
        return listname;
    }

    public void setListname(String listname) {
        this.listname = listname;
    }

    public Date getBegindate() {
        return begindate;
    }

    public void setBegindate(Date begindate) {
        this.begindate = begindate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public BigDecimal getDuration() {
        return duration;
    }

    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getTotalamountWithdrawal() {
        return totalamountWithdrawal;
    }

    public void setTotalamountWithdrawal(BigDecimal totalamountWithdrawal) {
        this.totalamountWithdrawal = totalamountWithdrawal;
    }

    public BigDecimal getTotalamountTransfer() {
        return totalamountTransfer;
    }

    public void setTotalamountTransfer(BigDecimal totalamountTransfer) {
        this.totalamountTransfer = totalamountTransfer;
    }

    public BigDecimal getTotalfeeposappr() {
        return totalfeeposappr;
    }

    public void setTotalfeeposappr(BigDecimal totalfeeposappr) {
        this.totalfeeposappr = totalfeeposappr;
    }

    public BigDecimal getTotalfeeposdecl() {
        return totalfeeposdecl;
    }

    public void setTotalfeeposdecl(BigDecimal totalfeeposdecl) {
        this.totalfeeposdecl = totalfeeposdecl;
    }

    public String getBillerid() {
        return billerid;
    }

    public void setBillerid(String billerid) {
        this.billerid = billerid;
    }

    public String getBillername() {
        return billername;
    }

    public void setBillername(String billername) {
        this.billername = billername;
    }

    public BigDecimal getTotalfreqpmtappr() {
        return totalfreqpmtappr;
    }

    public void setTotalfreqpmtappr(BigDecimal totalfreqpmtappr) {
        this.totalfreqpmtappr = totalfreqpmtappr;
    }

    public BigDecimal getTotalfreqpmtdecl() {
        return totalfreqpmtdecl;
    }

    public void setTotalfreqpmtdecl(BigDecimal totalfreqpmtdecl) {
        this.totalfreqpmtdecl = totalfreqpmtdecl;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getDeclinecode() {
        return declinecode;
    }

    public void setDeclinecode(String declinecode) {
        this.declinecode = declinecode;
    }

    public BigDecimal getFrequency() {
        return frequency;
    }

    public void setFrequency(BigDecimal frequency) {
        this.frequency = frequency;
    }

    public String getRtcd() {
        return rtcd;
    }

    public void setRtcd(String rtcd) {
        this.rtcd = rtcd;
    }

    public String getBankidacq() {
        return bankidacq;
    }

    public void setBankidacq(String bankidacq) {
        this.bankidacq = bankidacq;
    }

    public String getBankidiss() {
        return bankidiss;
    }

    public void setBankidiss(String bankidiss) {
        this.bankidiss = bankidiss;
    }

    public String getBankidbnf() {
        return bankidbnf;
    }

    public void setBankidbnf(String bankidbnf) {
        this.bankidbnf = bankidbnf;
    }

    public String getBanknameacq() {
        return banknameacq;
    }

    public void setBanknameacq(String banknameacq) {
        this.banknameacq = banknameacq;
    }

    public String getBanknameiss() {
        return banknameiss;
    }

    public void setBanknameiss(String banknameiss) {
        this.banknameiss = banknameiss;
    }

    public String getBanknamebnf() {
        return banknamebnf;
    }

    public void setBanknamebnf(String banknamebnf) {
        this.banknamebnf = banknamebnf;
    }

    public BigDecimal getTotalfreqdebit() {
        return totalfreqdebit;
    }

    public void setTotalfreqdebit(BigDecimal totalfreqdebit) {
        this.totalfreqdebit = totalfreqdebit;
    }

    public BigDecimal getPercentGrowthFreqWith() {
        return percentGrowthFreqWith;
    }

    public void setPercentGrowthFreqWith(BigDecimal percentGrowthFreqWith) {
        this.percentGrowthFreqWith = percentGrowthFreqWith;
    }

    public BigDecimal getPercentGrowthFreqDebit() {
        return percentGrowthFreqDebit;
    }

    public void setPercentGrowthFreqDebit(BigDecimal percentGrowthFreqDebit) {
        this.percentGrowthFreqDebit = percentGrowthFreqDebit;
    }

    public BigDecimal getPercentGrowthFreqTransf() {
        return percentGrowthFreqTransf;
    }

    public void setPercentGrowthFreqTransf(BigDecimal percentGrowthFreqTransf) {
        this.percentGrowthFreqTransf = percentGrowthFreqTransf;
    }

    public BigDecimal getPrevTotalfreqWith() {
        return prevTotalfreqWith;
    }

    public void setPrevTotalfreqWith(BigDecimal prevTotalfreqWith) {
        this.prevTotalfreqWith = prevTotalfreqWith;
    }

    public BigDecimal getPrevTotalfreqDebit() {
        return prevTotalfreqDebit;
    }

    public void setPrevTotalfreqDebit(BigDecimal prevTotalfreqDebit) {
        this.prevTotalfreqDebit = prevTotalfreqDebit;
    }

    public BigDecimal getPrevTotalfreqTransf() {
        return prevTotalfreqTransf;
    }

    public void setPrevTotalfreqTransf(BigDecimal prevTotalfreqTransf) {
        this.prevTotalfreqTransf = prevTotalfreqTransf;
    }

    public String getIntercon() {
        return intercon;
    }

    public void setIntercon(String intercon) {
        this.intercon = intercon;
    }

    public String getAcqcode() {
        return acqcode;
    }

    public void setAcqcode(String acqcode) {
        this.acqcode = acqcode;
    }

    public String getIsscode() {
        return isscode;
    }

    public void setIsscode(String isscode) {
        this.isscode = isscode;
    }

    public String getBnfcode() {
        return bnfcode;
    }

    public void setBnfcode(String bnfcode) {
        this.bnfcode = bnfcode;
    }

    public String getBanknameintacq() {
        return banknameintacq;
    }

    public void setBanknameintacq(String banknameintacq) {
        this.banknameintacq = banknameintacq;
    }

    public String getBanknameintiss() {
        return banknameintiss;
    }

    public void setBanknameintiss(String banknameintiss) {
        this.banknameintiss = banknameintiss;
    }

    public String getBanknameintbnf() {
        return banknameintbnf;
    }

    public void setBanknameintbnf(String banknameintbnf) {
        this.banknameintbnf = banknameintbnf;
    }

    public BigDecimal getTotalapprpmtamount() {
        return totalapprpmtamount;
    }

    public void setTotalapprpmtamount(BigDecimal totalapprpmtamount) {
        this.totalapprpmtamount = totalapprpmtamount;
    }

    public BigDecimal getTotaldeclpmtamount() {
        return totaldeclpmtamount;
    }

    public void setTotaldeclpmtamount(BigDecimal totaldeclpmtamount) {
        this.totaldeclpmtamount = totaldeclpmtamount;
    }
}

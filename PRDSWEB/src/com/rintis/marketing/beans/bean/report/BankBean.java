package com.rintis.marketing.beans.bean.report;


import java.math.BigDecimal;

public class BankBean {
    private String bankid;
    private String bankname;
    private String bankshortname;
    private String bankissatmstatus;
    private String bankacqatmstatus;
    private String bankissposstatus;
    private String bankcode;
    private BigDecimal bankatmqty;
    private BigDecimal bankedcqty;
    private String bankpayment;
    private String bankapn;
    private String bankcup;
    private String bankcnet;
    private BigDecimal bankcardholderqty;
    private Boolean bankcupbool;
    private Boolean bankapnbool;
    private Boolean bankpaymentbool;
    /*add by aaw 02/07/2021*/
    private String bankFiid;
    /*End add*/

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getBankshortname() {
        return bankshortname;
    }

    public void setBankshortname(String bankshortname) {
        this.bankshortname = bankshortname;
    }

    public String getBankissatmstatus() {
        return bankissatmstatus;
    }

    public void setBankissatmstatus(String bankissatmstatus) {
        this.bankissatmstatus = bankissatmstatus;
    }

    public String getBankacqatmstatus() {
        return bankacqatmstatus;
    }

    public void setBankacqatmstatus(String bankacqatmstatus) {
        this.bankacqatmstatus = bankacqatmstatus;
    }

    public String getBankissposstatus() {
        return bankissposstatus;
    }

    public void setBankissposstatus(String bankissposstatus) {
        this.bankissposstatus = bankissposstatus;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public BigDecimal getBankatmqty() {
        return bankatmqty;
    }

    public void setBankatmqty(BigDecimal bankatmqty) {
        this.bankatmqty = bankatmqty;
    }

    public BigDecimal getBankedcqty() {
        return bankedcqty;
    }

    public void setBankedcqty(BigDecimal bankedcqty) {
        this.bankedcqty = bankedcqty;
    }

    public String getBankpayment() {
        return bankpayment;
    }

    public void setBankpayment(String bankpayment) {
        this.bankpayment = bankpayment;
    }

    public String getBankapn() {
        return bankapn;
    }

    public void setBankapn(String bankapn) {
        this.bankapn = bankapn;
    }

    public String getBankcup() {
        return bankcup;
    }

    public void setBankcup(String bankcup) {
        this.bankcup = bankcup;
    }

    public BigDecimal getBankcardholderqty() {
        return bankcardholderqty;
    }

    public void setBankcardholderqty(BigDecimal bankcardholderqty) {
        this.bankcardholderqty = bankcardholderqty;
    }

    public Boolean getBankcupbool() {
        return bankcupbool;
    }

    public void setBankcupbool(Boolean bankcupbool) {
        this.bankcupbool = bankcupbool;
    }

    public Boolean getBankapnbool() {
        return bankapnbool;
    }

    public void setBankapnbool(Boolean bankapnbool) {
        this.bankapnbool = bankapnbool;
    }

    public Boolean getBankpaymentbool() {
        return bankpaymentbool;
    }

    public void setBankpaymentbool(Boolean bankpaymentbool) {
        this.bankpaymentbool = bankpaymentbool;
    }

    public String getBankcnet() {
        return bankcnet;
    }

    public void setBankcnet(String bankcnet) {
        this.bankcnet = bankcnet;
    }

    /*add by aaw 02/07/2021*/
    public String getBankFiid() {
        return bankFiid;
    }

    public void setBankFiid(String bankFiid) {
        this.bankFiid = bankFiid;
    }
    /*End add*/
}

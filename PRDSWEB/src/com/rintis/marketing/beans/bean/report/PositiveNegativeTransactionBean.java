package com.rintis.marketing.beans.bean.report;

import java.math.BigDecimal;

public class PositiveNegativeTransactionBean {
    private String bankname;
    private String bankid;
    private String billername;
    private String billerid;

    private BigDecimal prevmonth;
    private BigDecimal currmonth;
    private BigDecimal txn;
    private BigDecimal pct;

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getBillername() {
        return billername;
    }

    public void setBillername(String billername) {
        this.billername = billername;
    }

    public String getBillerid() {
        return billerid;
    }

    public void setBillerid(String billerid) {
        this.billerid = billerid;
    }

    public BigDecimal getPrevmonth() {
        return prevmonth;
    }

    public void setPrevmonth(BigDecimal prevmonth) {
        this.prevmonth = prevmonth;
    }

    public BigDecimal getCurrmonth() {
        return currmonth;
    }

    public void setCurrmonth(BigDecimal currmonth) {
        this.currmonth = currmonth;
    }

    public BigDecimal getTxn() {
        return txn;
    }

    public void setTxn(BigDecimal txn) {
        this.txn = txn;
    }

    public BigDecimal getPct() {
        return pct;
    }

    public void setPct(BigDecimal pct) {
        this.pct = pct;
    }
}

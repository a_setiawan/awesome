package com.rintis.marketing.beans.bean.report;

import com.rintis.marketing.beans.bean.marketing.ColumnModel;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class DailyTransBillerBean {
    private int day;
    private List<BillerDetailBean> listDetail;
    private BigDecimal total;
    private Map<String, BigDecimal> mapFrequency;
    private Map<String, BigDecimal> mapTotal;
    private String bankId;
    private String bankName;

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public List<BillerDetailBean> getListDetail() {
        return listDetail;
    }

    public void setListDetail(List<BillerDetailBean> listDetail) {
        this.listDetail = listDetail;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Map<String, BigDecimal> getMapFrequency() {
        return mapFrequency;
    }

    public void setMapFrequency(Map<String, BigDecimal> mapFrequency) {
        this.mapFrequency = mapFrequency;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Map<String, BigDecimal> getMapTotal() {
        return mapTotal;
    }

    public void setMapTotal(Map<String, BigDecimal> mapTotal) {
        this.mapTotal = mapTotal;
    }
}

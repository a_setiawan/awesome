package com.rintis.marketing.beans.bean.menu;


import java.io.Serializable;
import java.math.BigDecimal;

public class UserMenuBean implements Serializable {
    private String menuid;
    private BigDecimal menutype;
    private String menuname;
    private String menuurl;
    private String parentmenuid;
    private BigDecimal sequenceid;
    private BigDecimal active;
    private String userloginid;
    private Boolean statusmenu;
    private BigDecimal roletypeid;

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public BigDecimal getMenutype() {
        return menutype;
    }

    public void setMenutype(BigDecimal menutype) {
        this.menutype = menutype;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public String getMenuurl() {
        return menuurl;
    }

    public void setMenuurl(String menuurl) {
        this.menuurl = menuurl;
    }

    public String getParentmenuid() {
        return parentmenuid;
    }

    public void setParentmenuid(String parentmenuid) {
        this.parentmenuid = parentmenuid;
    }

    public BigDecimal getSequenceid() {
        return sequenceid;
    }

    public void setSequenceid(BigDecimal sequenceid) {
        this.sequenceid = sequenceid;
    }

    public BigDecimal getActive() {
        return active;
    }

    public void setActive(BigDecimal active) {
        this.active = active;
    }

    public String getUserloginid() {
        return userloginid;
    }

    public void setUserloginid(String userloginid) {
        this.userloginid = userloginid;
    }

    public Boolean getStatusmenu() {
        return statusmenu;
    }

    public void setStatusmenu(Boolean statusmenu) {
        this.statusmenu = statusmenu;
    }

    public BigDecimal getRoletypeid() {
        return roletypeid;
    }

    public void setRoletypeid(BigDecimal roletypeid) {
        this.roletypeid = roletypeid;
    }
}

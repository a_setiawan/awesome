package com.rintis.marketing.beans.bean;

import java.io.Serializable;
import java.util.List;

import com.rintis.marketing.beans.entity.MenuRole;
import com.rintis.marketing.beans.entity.MenuRoleType;
import com.rintis.marketing.beans.entity.UserLogin;
import com.rintis.marketing.beans.entity.UserLoginBank;
import com.rintis.marketing.core.constant.DataConstant;

public class UserInfoBean implements Serializable {
    private String userId;
    private String companyEmail;
    private UserLogin userLogin;
    private UserLoginBank userLoginBank;
    private List<MenuRole> listMenuRole;
    private String branchName;
    private String branchId;

    public boolean checkRoleAdmin() {
        boolean result = false;
        if (userLogin != null) {
            MenuRoleType mrt = userLogin.getMenuRoleType();
            if (mrt != null) {
                if (mrt.getMenuRoleName().equalsIgnoreCase(DataConstant.MENU_ROLE_NAME_ADMIN)) {
                    result = true;
                }
            }
        }
        return result;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public UserLogin getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }

    public List<MenuRole> getListMenuRole() {
        return listMenuRole;
    }

    public void setListMenuRole(List<MenuRole> listMenuRole) {
        this.listMenuRole = listMenuRole;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public UserLoginBank getUserLoginBank() {
        return userLoginBank;
    }

    public void setUserLoginBank(UserLoginBank userLoginBank) {
        this.userLoginBank = userLoginBank;
    }
}

package com.rintis.marketing.beans.entity;

import javax.persistence.*;

@Entity
@Table(name = "tms_bank_email")
public class BankEmail extends AbstractLastUpdatedCreated {
    @Id
    @Column(name = "bank_id")
    private String bankId;
    @Column(name = "email")
    private String email;
    @Column(name = "alert_mcc_status")
    private String alert_mcc_status;
    @Column(name = "alert_seq_status")
    private String alert_seq_status;

    @OneToOne
    @JoinColumn(name="bank_id", insertable=false, updatable=false)
    private EisBank bank;

    /*Add by aaw 02/07/2021*/
    @Column(name = "alert_mpan_acq")
    private String alert_mpan_acq;
    /*End add*/

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public EisBank getBank() {
        return bank;
    }

    public void setBank(EisBank bank) {
        this.bank = bank;
    }

    public String getAlert_mcc_status() {
        return alert_mcc_status;
    }

    public void setAlert_mcc_status(String alert_mcc_status) {
        this.alert_mcc_status = alert_mcc_status;
    }

    public String getAlert_seq_status() {
        return alert_seq_status;
    }

    public void setAlert_seq_status(String alert_seq_status) {
        this.alert_seq_status = alert_seq_status;
    }

    /*Add by aaw 02/07/2021*/
    public String getAlert_mpan_acq() {
        return alert_mpan_acq;
    }

    public void setAlert_mpan_acq(String alert_mpan_acq) {
        this.alert_mpan_acq = alert_mpan_acq;
    }
    /*End  add*/
}

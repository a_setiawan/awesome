package com.rintis.marketing.beans.entity;

import com.rintis.marketing.core.utils.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMS_MENU")
public class Menu extends AbstractLastUpdatedCreated {
    @Id
    @Column(name = "MENU_ID")
    private String menuId;
    @Column(name = "MENU_TYPE")
    private Integer menuType;
    @Column(name = "MENU_NAME")
    private String menuName;
    @Column(name = "MENU_URL")
    private String menuUrl;
    @Column(name = "MENU_TITLE")
    private String menuTitle;
    @Column(name = "MENU_ICON")
    private String menuIcon;
    @Column(name = "PARENT_MENU_ID")
    private String parentMenuId;
    @Column(name = "SEQUENCE_ID")
    private Integer sequenceId;
    @Column(name = "ACTIVE")
    private Integer active;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = StringUtils.upperCaseNull(menuId);
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getParentMenuId() {
        return parentMenuId;
    }

    public void setParentMenuId(String parentMenuId) {
        this.parentMenuId = StringUtils.upperCaseNull(parentMenuId);
    }

    public Integer getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId) {
        this.sequenceId = sequenceId;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getMenuType() {
        return menuType;
    }

    public void setMenuType(Integer menuType) {
        this.menuType = menuType;
    }

}

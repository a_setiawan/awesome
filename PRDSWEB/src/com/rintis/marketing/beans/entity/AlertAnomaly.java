package com.rintis.marketing.beans.entity;

import com.rintis.marketing.core.constant.DataConstant;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table(name = "tms_alert_anomaly")
public class AlertAnomaly extends AbstractLastUpdatedCreated {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "TMS_ALERT_ANOMALY_SEQ")
    @SequenceGenerator(name = "TMS_ALERT_ANOMALY_SEQ", sequenceName = "TMS_ALERT_ANOMALY_SEQ")
    @Column(name = "id")
    private BigInteger id;
    @Column(name = "bank_id")
    private String bankId;
    @Column(name = "transaction_id")
    private String transactionId;
    @Column(name = "transaction_indicator_id")
    private String transactionIndicatorId;
    @Column(name = "alert_date")
    private Date alertDate;
    @Column(name = "description")
    private String description;
    @Column(name = "solve_status")
    private Integer solveStatus;
    @Column(name = "threshold")
    private BigDecimal threshold;
    @Column(name = "prev_value")
    private BigDecimal prevValue;
    @Column(name = "curr_value")
    private BigDecimal currValue;
    @Column(name = "type_alert")
    private String typeAlert;
    @Column(name = "alert_method")
    private String alertMethod;
    @Column(name = "alert_period")
    private String alertPeriod;

    @Column(name = "group_bank_id")
    private String groupBankId;
    @Column(name = "type_col")
    private String typeCol;
    @Column(name = "limit_hi")
    private BigDecimal limitHi;
    @Column(name = "limit_lo")
    private BigDecimal limitLo;
    @Column(name = "period")
    private String period;
    @Column(name = "time_fr")
    private String timeFr;
    @Column(name = "time_to")
    private String timeTo;
    @Column(name = "period_prev")
    private String periodPrev;
    @Column(name = "time_fr_prev")
    private String timeFrPrev;
    @Column(name = "time_to_prev")
    private String timeToPrev;

    @Column(name = "min_trx")
    private BigDecimal minTrx;
    @Column(name = "pct_decline")
    private BigDecimal pctDecline;
    @Column(name = "pct_approve")
    private BigDecimal pctApprove;

    @OneToOne
    @JoinColumn(name="bank_id", insertable=false, updatable=false)
    private EisBank eisBank;

    @OneToOne
    @JoinColumn(name="transaction_id", referencedColumnName = "trx_id", insertable=false, updatable=false)
    @NotFound(action = NotFoundAction.IGNORE)
    private EsqTrxid esqTrxid;

    @OneToOne
    @JoinColumn(name="transaction_indicator_id", referencedColumnName = "trx_id", insertable=false, updatable=false)
    @NotFound(action = NotFoundAction.IGNORE)
    private EsqTrxIndicator esqTrxIndicator;

    @Transient
    private Boolean ack;

    public String getTimeframeName() {
        return DataConstant.getTimeFrameName(alertPeriod);
    }

    public String getTypeColName() {
        return DataConstant.getColTypeName(typeCol);
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Date getAlertDate() {
        return alertDate;
    }

    public void setAlertDate(Date alertDate) {
        this.alertDate = alertDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSolveStatus() {
        return solveStatus;
    }

    public void setSolveStatus(Integer solveStatus) {
        this.solveStatus = solveStatus;
    }

    public BigDecimal getThreshold() {
        return threshold;
    }

    public void setThreshold(BigDecimal threshold) {
        this.threshold = threshold;
    }

    public BigDecimal getPrevValue() {
        return prevValue;
    }

    public void setPrevValue(BigDecimal prevValue) {
        this.prevValue = prevValue;
    }

    public BigDecimal getCurrValue() {
        return currValue;
    }

    public void setCurrValue(BigDecimal currValue) {
        this.currValue = currValue;
    }

    public String getTypeAlert() {
        return typeAlert;
    }

    public void setTypeAlert(String typeAlert) {
        this.typeAlert = typeAlert;
    }

    public EisBank getEisBank() {
        return eisBank;
    }

    public void setEisBank(EisBank eisBank) {
        this.eisBank = eisBank;
    }

    public EsqTrxid getEsqTrxid() {
        return esqTrxid;
    }

    public void setEsqTrxid(EsqTrxid esqTrxid) {
        this.esqTrxid = esqTrxid;
    }

    public Boolean getAck() {
        return ack;
    }

    public void setAck(Boolean ack) {
        this.ack = ack;
    }

    public String getAlertMethod() {
        return alertMethod;
    }

    public void setAlertMethod(String alertMethod) {
        this.alertMethod = alertMethod;
    }

    public String getAlertPeriod() {
        return alertPeriod;
    }

    public void setAlertPeriod(String alertPeriod) {
        this.alertPeriod = alertPeriod;
    }

    public String getTransactionIndicatorId() {
        return transactionIndicatorId;
    }

    public void setTransactionIndicatorId(String transactionIndicatorId) {
        this.transactionIndicatorId = transactionIndicatorId;
    }

    public EsqTrxIndicator getEsqTrxIndicator() {
        return esqTrxIndicator;
    }

    public void setEsqTrxIndicator(EsqTrxIndicator esqTrxIndicator) {
        this.esqTrxIndicator = esqTrxIndicator;
    }

    public BigDecimal getMinTrx() {
        return minTrx;
    }

    public void setMinTrx(BigDecimal minTrx) {
        this.minTrx = minTrx;
    }

    public BigDecimal getPctDecline() {
        return pctDecline;
    }

    public void setPctDecline(BigDecimal pctDecline) {
        this.pctDecline = pctDecline;
    }

    public BigDecimal getPctApprove() {
        return pctApprove;
    }

    public void setPctApprove(BigDecimal pctApprove) {
        this.pctApprove = pctApprove;
    }

    public String getGroupBankId() {
        return groupBankId;
    }

    public void setGroupBankId(String groupBankId) {
        this.groupBankId = groupBankId;
    }

    public String getTypeCol() {
        return typeCol;
    }

    public void setTypeCol(String typeCol) {
        this.typeCol = typeCol;
    }

    public BigDecimal getLimitHi() {
        return limitHi;
    }

    public void setLimitHi(BigDecimal limitHi) {
        this.limitHi = limitHi;
    }

    public BigDecimal getLimitLo() {
        return limitLo;
    }

    public void setLimitLo(BigDecimal limitLo) {
        this.limitLo = limitLo;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getTimeFr() {
        return timeFr;
    }

    public void setTimeFr(String timeFr) {
        this.timeFr = timeFr;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public String getPeriodPrev() {
        return periodPrev;
    }

    public void setPeriodPrev(String periodPrev) {
        this.periodPrev = periodPrev;
    }

    public String getTimeFrPrev() {
        return timeFrPrev;
    }

    public void setTimeFrPrev(String timeFrPrev) {
        this.timeFrPrev = timeFrPrev;
    }

    public String getTimeToPrev() {
        return timeToPrev;
    }

    public void setTimeToPrev(String timeToPrev) {
        this.timeToPrev = timeToPrev;
    }
}

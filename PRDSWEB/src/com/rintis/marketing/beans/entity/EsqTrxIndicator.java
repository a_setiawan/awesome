package com.rintis.marketing.beans.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tms_trxindicator")
public class EsqTrxIndicator extends AbstractLastUpdatedCreated {
    @Id
    @Column(name = "trx_id")
    private String trxId;
    @Column(name = "trx_name")
    private String trxName;

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getTrxName() {
        return trxName;
    }

    public void setTrxName(String trxName) {
        this.trxName = trxName;
    }
}

package com.rintis.marketing.beans.entity;

import com.rintis.marketing.core.utils.StringUtils;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.math.BigDecimal;

@Embeddable
public class DashboardRolePk implements Serializable {
    @Column(name = "role_type_id")
    private Integer roleTypeId;
    @Column(name = "dashboard_id")
    private BigDecimal dashboardId;

    

    public Integer getRoleTypeId() {
        return roleTypeId;
    }

    public void setRoleTypeId(Integer roleTypeId) {
        this.roleTypeId = roleTypeId;
    }

    public BigDecimal getDashboardId() {
        return dashboardId;
    }

    public void setDashboardId(BigDecimal dashboardId) {
        this.dashboardId = dashboardId;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (!(other instanceof DashboardRolePk))
            return false;

        final DashboardRolePk pk = (DashboardRolePk) other;

        if (!pk.getRoleTypeId().equals(getRoleTypeId()))
            return false;
        if (!pk.getDashboardId().equals(getDashboardId()))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = getRoleTypeId().hashCode();
        result = 29 * result + getDashboardId().hashCode();
        return result;
    }
}

package com.rintis.marketing.beans.entity;

import com.rintis.marketing.core.utils.StringUtils;

import javax.persistence.*;

@Entity
@Table(name = "tms_menu_role_type")
public class MenuRoleType extends AbstractLastUpdatedCreated {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "TMS_MENU_ROLE_TYPE_SEQ")
    @SequenceGenerator(name = "TMS_MENU_ROLE_TYPE_SEQ", sequenceName = "TMS_MENU_ROLE_TYPE_SEQ")
    @Column(name = "menu_role_type_id")
    private Integer menuRoleTypeId;
    @Column(name = "menu_role_name")
    private String menuRoleName;
    @Column(name = "hidden")
    private Integer hidden;

    public Integer getMenuRoleTypeId() {
        return menuRoleTypeId;
    }

    public void setMenuRoleTypeId(Integer menuRoleTypeId) {
        this.menuRoleTypeId = menuRoleTypeId;
    }

    public String getMenuRoleName() {
        return menuRoleName;
    }

    public void setMenuRoleName(String menuRoleName) {
        this.menuRoleName = StringUtils.upperCaseNull(menuRoleName);
    }

    public Integer getHidden() {
        return hidden;
    }

    public void setHidden(Integer hidden) {
        this.hidden = hidden;
    }

}

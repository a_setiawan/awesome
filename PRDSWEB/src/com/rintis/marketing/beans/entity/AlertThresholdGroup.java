package com.rintis.marketing.beans.entity;

import com.rintis.marketing.core.constant.DataConstant;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "tms_alert_th_group")
public class AlertThresholdGroup extends AbstractLastUpdatedCreated {
    @EmbeddedId
    private AlertThresholdGroupPk alertThresholdGroupPk;
    @Column(name = "start_time")
    private String startTime;
    @Column(name = "end_time")
    private String endTime;
    @Column(name = "prev_n_tt")
    private Integer prevNTt;
    @Column(name = "prev_n_ap")
    private Integer prevNAp;
    @Column(name = "prev_n_dc")
    private Integer prevNDc;
    @Column(name = "prev_n_df")
    private Integer prevNDf;
    @Column(name = "prev_day")
    private Integer prevDay;
    @Column(name = "trh_trx_period_pct_tt")
    private BigDecimal trhTrxPeriodPctTt;
    @Column(name = "trh_trx_period_pct_ap")
    private BigDecimal trhTrxPeriodPctAp;
    @Column(name = "trh_trx_period_pct_dc")
    private BigDecimal trhTrxPeriodPctDc;
    @Column(name = "trh_trx_period_pct_df")
    private BigDecimal trhTrxPeriodPctDf;

    @Column(name = "trh_trx_day_pct_tt")
    private BigDecimal trhTrxDayPctTt;
    @Column(name = "trh_trx_day_pct_ap")
    private BigDecimal trhTrxDayPctAp;
    @Column(name = "trh_trx_day_pct_dc")
    private BigDecimal trhTrxDayPctDc;
    @Column(name = "trh_trx_day_pct_df")
    private BigDecimal trhTrxDayPctDf;

    @Column(name = "trh_trx_day_pct_tt_h")
    private BigDecimal trhTrxDayPctTtHoliday;
    @Column(name = "trh_trx_day_pct_ap_h")
    private BigDecimal trhTrxDayPctApHoliday;
    @Column(name = "trh_trx_day_pct_dc_h")
    private BigDecimal trhTrxDayPctDcHoliday;
    @Column(name = "trh_trx_day_pct_df_h")
    private BigDecimal trhTrxDayPctDfHoliday;

    @Column(name = "active_m1")
    private Integer activeM1;
    @Column(name = "active_m2")
    private Integer activeM2;
    @Column(name = "active_m3")
    private Integer activeM3;
    @Column(name = "active_m4")
    private Integer activeM4;
    @Column(name = "trh_pct_dc")
    private BigDecimal trhPctDc;
    @Column(name = "trh_pct_df")
    private BigDecimal trhPctDf;

    @Column(name = "min_trx_period_tt")
    private BigDecimal minTrxPeriodTt;
    @Column(name = "min_trx_period_ap")
    private BigDecimal minTrxPeriodAp;
    @Column(name = "min_trx_period_dc")
    private BigDecimal minTrxPeriodDc;
    @Column(name = "min_trx_period_df")
    private BigDecimal minTrxPeriodDf;

    @Column(name = "min_trx_day_tt")
    private BigDecimal minTrxDayTt;
    @Column(name = "min_trx_day_ap")
    private BigDecimal minTrxDayAp;
    @Column(name = "min_trx_day_dc")
    private BigDecimal minTrxDayDc;
    @Column(name = "min_trx_day_df")
    private BigDecimal minTrxDayDf;

    @Column(name = "min_trx_pct_dc")
    private BigDecimal minTrxPctDc;
    @Column(name = "min_trx_pct_df")
    private BigDecimal minTrxPctDf;

    @Column(name = "trh_trx_day_pct_tt_dec")
    private BigDecimal trhTrxDayPctTtDec;
    @Column(name = "trh_trx_day_pct_tt_inc")
    private BigDecimal trhTrxDayPctTtInc;
    @Column(name = "trh_trx_day_pct_ap_dec")
    private BigDecimal trhTrxDayPctApDec;
    @Column(name = "trh_trx_day_pct_ap_inc")
    private BigDecimal trhTrxDayPctApInc;
    @Column(name = "trh_trx_day_pct_dc_dec")
    private BigDecimal trhTrxDayPctDcDec;
    @Column(name = "trh_trx_day_pct_dc_inc")
    private BigDecimal trhTrxDayPctDcInc;
    @Column(name = "trh_trx_day_pct_df_dec")
    private BigDecimal trhTrxDayPctDfDec;
    @Column(name = "trh_trx_day_pct_df_inc")
    private BigDecimal trhTrxDayPctDfInc;
    @Column(name = "trh_trx_day_pct_tt_wh_dec")
    private BigDecimal trhTrxDayPctTtWhDec;
    @Column(name = "trh_trx_day_pct_tt_wh_inc")
    private BigDecimal trhTrxDayPctTtWhInc;
    @Column(name = "trh_trx_day_pct_tt_hw_dec")
    private BigDecimal trhTrxDayPctTtHwDec;
    @Column(name = "trh_trx_day_pct_tt_hw_inc")
    private BigDecimal trhTrxDayPctTtHwInc;
    @Column(name = "trh_trx_day_pct_ap_wh_dec")
    private BigDecimal trhTrxDayPctApWhDec;
    @Column(name = "trh_trx_day_pct_ap_wh_inc")
    private BigDecimal trhTrxDayPctApWhInc;
    @Column(name = "trh_trx_day_pct_ap_hw_dec")
    private BigDecimal trhTrxDayPctApHwDec;
    @Column(name = "trh_trx_day_pct_ap_hw_inc")
    private BigDecimal trhTrxDayPctApHwInc;
    @Column(name = "trh_trx_day_pct_dc_wh_dec")
    private BigDecimal trhTrxDayPctDcWhDec;
    @Column(name = "trh_trx_day_pct_dc_wh_inc")
    private BigDecimal trhTrxDayPctDcWhInc;
    @Column(name = "trh_trx_day_pct_dc_hw_dec")
    private BigDecimal trhTrxDayPctDcHwDec;
    @Column(name = "trh_trx_day_pct_dc_hw_inc")
    private BigDecimal trhTrxDayPctDcHwInc;
    @Column(name = "trh_trx_day_pct_df_wh_dec")
    private BigDecimal trhTrxDayPctDfWhDec;
    @Column(name = "trh_trx_day_pct_df_wh_inc")
    private BigDecimal trhTrxDayPctDfWhInc;
    @Column(name = "trh_trx_day_pct_df_hw_dec")
    private BigDecimal trhTrxDayPctDfHwDec;
    @Column(name = "trh_trx_day_pct_df_hw_inc")
    private BigDecimal trhTrxDayPctDfHwInc;

    @Transient
    private String transactionIndicatorName;
    @Transient
    private Boolean activeM1Bool;
    @Transient
    private Boolean activeM2Bool;
    @Transient
    private Boolean activeM3Bool;
    @Transient
    private Boolean activeM4Bool;
    @Transient
    private String startHour;
    @Transient
    private String endHour;
    @Transient
    private String startMinute;
    @Transient
    private String endMinute;

    /*@OneToOne
    @JoinColumn(name="timeframe", referencedColumnName="tf_id", insertable=false, updatable=false)
    private TmsTimeFrame tmeTimeframe;*/



    public String getTimeFrameName() {
        return DataConstant.getTimeFrameName(alertThresholdGroupPk.getTimeframe());
    }



    public AlertThresholdGroupPk getAlertThresholdGroupPk() {
        return alertThresholdGroupPk;
    }

    public void setAlertThresholdGroupPk(AlertThresholdGroupPk alertThresholdGroupPk) {
        this.alertThresholdGroupPk = alertThresholdGroupPk;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getPrevNTt() {
        return prevNTt;
    }

    public void setPrevNTt(Integer prevNTt) {
        this.prevNTt = prevNTt;
    }

    public Integer getPrevNAp() {
        return prevNAp;
    }

    public void setPrevNAp(Integer prevNAp) {
        this.prevNAp = prevNAp;
    }

    public Integer getPrevNDc() {
        return prevNDc;
    }

    public void setPrevNDc(Integer prevNDc) {
        this.prevNDc = prevNDc;
    }

    public Integer getPrevNDf() {
        return prevNDf;
    }

    public void setPrevNDf(Integer prevNDf) {
        this.prevNDf = prevNDf;
    }

    public Integer getPrevDay() {
        return prevDay;
    }

    public void setPrevDay(Integer prevDay) {
        this.prevDay = prevDay;
    }

    public BigDecimal getTrhTrxPeriodPctTt() {
        return trhTrxPeriodPctTt;
    }

    public void setTrhTrxPeriodPctTt(BigDecimal trhTrxPeriodPctTt) {
        this.trhTrxPeriodPctTt = trhTrxPeriodPctTt;
    }

    public BigDecimal getTrhTrxPeriodPctAp() {
        return trhTrxPeriodPctAp;
    }

    public void setTrhTrxPeriodPctAp(BigDecimal trhTrxPeriodPctAp) {
        this.trhTrxPeriodPctAp = trhTrxPeriodPctAp;
    }

    public BigDecimal getTrhTrxPeriodPctDc() {
        return trhTrxPeriodPctDc;
    }

    public void setTrhTrxPeriodPctDc(BigDecimal trhTrxPeriodPctDc) {
        this.trhTrxPeriodPctDc = trhTrxPeriodPctDc;
    }

    public BigDecimal getTrhTrxPeriodPctDf() {
        return trhTrxPeriodPctDf;
    }

    public void setTrhTrxPeriodPctDf(BigDecimal trhTrxPeriodPctDf) {
        this.trhTrxPeriodPctDf = trhTrxPeriodPctDf;
    }

    public BigDecimal getTrhTrxDayPctTt() {
        return trhTrxDayPctTt;
    }

    public void setTrhTrxDayPctTt(BigDecimal trhTrxDayPctTt) {
        this.trhTrxDayPctTt = trhTrxDayPctTt;
    }

    public BigDecimal getTrhTrxDayPctAp() {
        return trhTrxDayPctAp;
    }

    public void setTrhTrxDayPctAp(BigDecimal trhTrxDayPctAp) {
        this.trhTrxDayPctAp = trhTrxDayPctAp;
    }

    public BigDecimal getTrhTrxDayPctDc() {
        return trhTrxDayPctDc;
    }

    public void setTrhTrxDayPctDc(BigDecimal trhTrxDayPctDc) {
        this.trhTrxDayPctDc = trhTrxDayPctDc;
    }

    public BigDecimal getTrhTrxDayPctDf() {
        return trhTrxDayPctDf;
    }

    public void setTrhTrxDayPctDf(BigDecimal trhTrxDayPctDf) {
        this.trhTrxDayPctDf = trhTrxDayPctDf;
    }

    public Integer getActiveM1() {
        return activeM1;
    }

    public void setActiveM1(Integer activeM1) {
        this.activeM1 = activeM1;
    }

    public Integer getActiveM2() {
        return activeM2;
    }

    public void setActiveM2(Integer activeM2) {
        this.activeM2 = activeM2;
    }

    public Integer getActiveM3() {
        return activeM3;
    }

    public void setActiveM3(Integer activeM3) {
        this.activeM3 = activeM3;
    }

    public Integer getActiveM4() {
        return activeM4;
    }

    public void setActiveM4(Integer activeM4) {
        this.activeM4 = activeM4;
    }

    public BigDecimal getTrhPctDc() {
        return trhPctDc;
    }

    public void setTrhPctDc(BigDecimal trhPctDc) {
        this.trhPctDc = trhPctDc;
    }

    public BigDecimal getTrhPctDf() {
        return trhPctDf;
    }

    public void setTrhPctDf(BigDecimal trhPctDf) {
        this.trhPctDf = trhPctDf;
    }

    public String getTransactionIndicatorName() {
        return transactionIndicatorName;
    }

    public void setTransactionIndicatorName(String transactionIndicatorName) {
        this.transactionIndicatorName = transactionIndicatorName;
    }

    public Boolean getActiveM1Bool() {
        return activeM1Bool;
    }

    public void setActiveM1Bool(Boolean activeM1Bool) {
        this.activeM1Bool = activeM1Bool;
    }

    public Boolean getActiveM2Bool() {
        return activeM2Bool;
    }

    public void setActiveM2Bool(Boolean activeM2Bool) {
        this.activeM2Bool = activeM2Bool;
    }

    public Boolean getActiveM3Bool() {
        return activeM3Bool;
    }

    public void setActiveM3Bool(Boolean activeM3Bool) {
        this.activeM3Bool = activeM3Bool;
    }

    public Boolean getActiveM4Bool() {
        return activeM4Bool;
    }

    public void setActiveM4Bool(Boolean activeM4Bool) {
        this.activeM4Bool = activeM4Bool;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getEndHour() {
        return endHour;
    }

    public void setEndHour(String endHour) {
        this.endHour = endHour;
    }

    public String getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(String startMinute) {
        this.startMinute = startMinute;
    }

    public String getEndMinute() {
        return endMinute;
    }

    public void setEndMinute(String endMinute) {
        this.endMinute = endMinute;
    }

    public BigDecimal getMinTrxPeriodTt() {
        return minTrxPeriodTt;
    }

    public void setMinTrxPeriodTt(BigDecimal minTrxPeriodTt) {
        this.minTrxPeriodTt = minTrxPeriodTt;
    }

    public BigDecimal getMinTrxPeriodAp() {
        return minTrxPeriodAp;
    }

    public void setMinTrxPeriodAp(BigDecimal minTrxPeriodAp) {
        this.minTrxPeriodAp = minTrxPeriodAp;
    }

    public BigDecimal getMinTrxPeriodDc() {
        return minTrxPeriodDc;
    }

    public void setMinTrxPeriodDc(BigDecimal minTrxPeriodDc) {
        this.minTrxPeriodDc = minTrxPeriodDc;
    }

    public BigDecimal getMinTrxPeriodDf() {
        return minTrxPeriodDf;
    }

    public void setMinTrxPeriodDf(BigDecimal minTrxPeriodDf) {
        this.minTrxPeriodDf = minTrxPeriodDf;
    }

    public BigDecimal getMinTrxDayTt() {
        return minTrxDayTt;
    }

    public void setMinTrxDayTt(BigDecimal minTrxDayTt) {
        this.minTrxDayTt = minTrxDayTt;
    }

    public BigDecimal getMinTrxDayAp() {
        return minTrxDayAp;
    }

    public void setMinTrxDayAp(BigDecimal minTrxDayAp) {
        this.minTrxDayAp = minTrxDayAp;
    }

    public BigDecimal getMinTrxDayDc() {
        return minTrxDayDc;
    }

    public void setMinTrxDayDc(BigDecimal minTrxDayDc) {
        this.minTrxDayDc = minTrxDayDc;
    }

    public BigDecimal getMinTrxDayDf() {
        return minTrxDayDf;
    }

    public void setMinTrxDayDf(BigDecimal minTrxDayDf) {
        this.minTrxDayDf = minTrxDayDf;
    }

    public BigDecimal getTrhTrxDayPctTtHoliday() {
        return trhTrxDayPctTtHoliday;
    }

    public void setTrhTrxDayPctTtHoliday(BigDecimal trhTrxDayPctTtHoliday) {
        this.trhTrxDayPctTtHoliday = trhTrxDayPctTtHoliday;
    }

    public BigDecimal getTrhTrxDayPctApHoliday() {
        return trhTrxDayPctApHoliday;
    }

    public void setTrhTrxDayPctApHoliday(BigDecimal trhTrxDayPctApHoliday) {
        this.trhTrxDayPctApHoliday = trhTrxDayPctApHoliday;
    }

    public BigDecimal getTrhTrxDayPctDcHoliday() {
        return trhTrxDayPctDcHoliday;
    }

    public void setTrhTrxDayPctDcHoliday(BigDecimal trhTrxDayPctDcHoliday) {
        this.trhTrxDayPctDcHoliday = trhTrxDayPctDcHoliday;
    }

    public BigDecimal getTrhTrxDayPctDfHoliday() {
        return trhTrxDayPctDfHoliday;
    }

    public void setTrhTrxDayPctDfHoliday(BigDecimal trhTrxDayPctDfHoliday) {
        this.trhTrxDayPctDfHoliday = trhTrxDayPctDfHoliday;
    }

    /*public TmsTimeFrame getTmeTimeframe() {
        return tmeTimeframe;
    }

    public void setTmeTimeframe(TmsTimeFrame tmeTimeframe) {
        this.tmeTimeframe = tmeTimeframe;
    }*/

    public BigDecimal getMinTrxPctDc() {
        return minTrxPctDc;
    }

    public void setMinTrxPctDc(BigDecimal minTrxPctDc) {
        this.minTrxPctDc = minTrxPctDc;
    }

    public BigDecimal getMinTrxPctDf() {
        return minTrxPctDf;
    }

    public void setMinTrxPctDf(BigDecimal minTrxPctDf) {
        this.minTrxPctDf = minTrxPctDf;
    }

    public BigDecimal getTrhTrxDayPctTtWhDec() {
        return trhTrxDayPctTtWhDec;
    }

    public void setTrhTrxDayPctTtWhDec(BigDecimal trhTrxDayPctTtWhDec) {
        this.trhTrxDayPctTtWhDec = trhTrxDayPctTtWhDec;
    }

    public BigDecimal getTrhTrxDayPctTtWhInc() {
        return trhTrxDayPctTtWhInc;
    }

    public void setTrhTrxDayPctTtWhInc(BigDecimal trhTrxDayPctTtWhInc) {
        this.trhTrxDayPctTtWhInc = trhTrxDayPctTtWhInc;
    }

    public BigDecimal getTrhTrxDayPctTtHwDec() {
        return trhTrxDayPctTtHwDec;
    }

    public void setTrhTrxDayPctTtHwDec(BigDecimal trhTrxDayPctTtHwDec) {
        this.trhTrxDayPctTtHwDec = trhTrxDayPctTtHwDec;
    }

    public BigDecimal getTrhTrxDayPctTtHwInc() {
        return trhTrxDayPctTtHwInc;
    }

    public void setTrhTrxDayPctTtHwInc(BigDecimal trhTrxDayPctTtHwInc) {
        this.trhTrxDayPctTtHwInc = trhTrxDayPctTtHwInc;
    }

    public BigDecimal getTrhTrxDayPctApWhDec() {
        return trhTrxDayPctApWhDec;
    }

    public void setTrhTrxDayPctApWhDec(BigDecimal trhTrxDayPctApWhDec) {
        this.trhTrxDayPctApWhDec = trhTrxDayPctApWhDec;
    }

    public BigDecimal getTrhTrxDayPctApWhInc() {
        return trhTrxDayPctApWhInc;
    }

    public void setTrhTrxDayPctApWhInc(BigDecimal trhTrxDayPctApWhInc) {
        this.trhTrxDayPctApWhInc = trhTrxDayPctApWhInc;
    }

    public BigDecimal getTrhTrxDayPctApHwDec() {
        return trhTrxDayPctApHwDec;
    }

    public void setTrhTrxDayPctApHwDec(BigDecimal trhTrxDayPctApHwDec) {
        this.trhTrxDayPctApHwDec = trhTrxDayPctApHwDec;
    }

    public BigDecimal getTrhTrxDayPctApHwInc() {
        return trhTrxDayPctApHwInc;
    }

    public void setTrhTrxDayPctApHwInc(BigDecimal trhTrxDayPctApHwInc) {
        this.trhTrxDayPctApHwInc = trhTrxDayPctApHwInc;
    }

    public BigDecimal getTrhTrxDayPctDcWhDec() {
        return trhTrxDayPctDcWhDec;
    }

    public void setTrhTrxDayPctDcWhDec(BigDecimal trhTrxDayPctDcWhDec) {
        this.trhTrxDayPctDcWhDec = trhTrxDayPctDcWhDec;
    }

    public BigDecimal getTrhTrxDayPctDcWhInc() {
        return trhTrxDayPctDcWhInc;
    }

    public void setTrhTrxDayPctDcWhInc(BigDecimal trhTrxDayPctDcWhInc) {
        this.trhTrxDayPctDcWhInc = trhTrxDayPctDcWhInc;
    }

    public BigDecimal getTrhTrxDayPctDcHwDec() {
        return trhTrxDayPctDcHwDec;
    }

    public void setTrhTrxDayPctDcHwDec(BigDecimal trhTrxDayPctDcHwDec) {
        this.trhTrxDayPctDcHwDec = trhTrxDayPctDcHwDec;
    }

    public BigDecimal getTrhTrxDayPctDcHwInc() {
        return trhTrxDayPctDcHwInc;
    }

    public void setTrhTrxDayPctDcHwInc(BigDecimal trhTrxDayPctDcHwInc) {
        this.trhTrxDayPctDcHwInc = trhTrxDayPctDcHwInc;
    }

    public BigDecimal getTrhTrxDayPctDfWhDec() {
        return trhTrxDayPctDfWhDec;
    }

    public void setTrhTrxDayPctDfWhDec(BigDecimal trhTrxDayPctDfWhDec) {
        this.trhTrxDayPctDfWhDec = trhTrxDayPctDfWhDec;
    }

    public BigDecimal getTrhTrxDayPctDfWhInc() {
        return trhTrxDayPctDfWhInc;
    }

    public void setTrhTrxDayPctDfWhInc(BigDecimal trhTrxDayPctDfWhInc) {
        this.trhTrxDayPctDfWhInc = trhTrxDayPctDfWhInc;
    }

    public BigDecimal getTrhTrxDayPctDfHwDec() {
        return trhTrxDayPctDfHwDec;
    }

    public void setTrhTrxDayPctDfHwDec(BigDecimal trhTrxDayPctDfHwDec) {
        this.trhTrxDayPctDfHwDec = trhTrxDayPctDfHwDec;
    }

    public BigDecimal getTrhTrxDayPctDfHwInc() {
        return trhTrxDayPctDfHwInc;
    }

    public void setTrhTrxDayPctDfHwInc(BigDecimal trhTrxDayPctDfHwInc) {
        this.trhTrxDayPctDfHwInc = trhTrxDayPctDfHwInc;
    }

    public BigDecimal getTrhTrxDayPctTtDec() {
        return trhTrxDayPctTtDec;
    }

    public void setTrhTrxDayPctTtDec(BigDecimal trhTrxDayPctTtDec) {
        this.trhTrxDayPctTtDec = trhTrxDayPctTtDec;
    }

    public BigDecimal getTrhTrxDayPctTtInc() {
        return trhTrxDayPctTtInc;
    }

    public void setTrhTrxDayPctTtInc(BigDecimal trhTrxDayPctTtInc) {
        this.trhTrxDayPctTtInc = trhTrxDayPctTtInc;
    }

    public BigDecimal getTrhTrxDayPctApDec() {
        return trhTrxDayPctApDec;
    }

    public void setTrhTrxDayPctApDec(BigDecimal trhTrxDayPctApDec) {
        this.trhTrxDayPctApDec = trhTrxDayPctApDec;
    }

    public BigDecimal getTrhTrxDayPctApInc() {
        return trhTrxDayPctApInc;
    }

    public void setTrhTrxDayPctApInc(BigDecimal trhTrxDayPctApInc) {
        this.trhTrxDayPctApInc = trhTrxDayPctApInc;
    }

    public BigDecimal getTrhTrxDayPctDcDec() {
        return trhTrxDayPctDcDec;
    }

    public void setTrhTrxDayPctDcDec(BigDecimal trhTrxDayPctDcDec) {
        this.trhTrxDayPctDcDec = trhTrxDayPctDcDec;
    }

    public BigDecimal getTrhTrxDayPctDcInc() {
        return trhTrxDayPctDcInc;
    }

    public void setTrhTrxDayPctDcInc(BigDecimal trhTrxDayPctDcInc) {
        this.trhTrxDayPctDcInc = trhTrxDayPctDcInc;
    }

    public BigDecimal getTrhTrxDayPctDfDec() {
        return trhTrxDayPctDfDec;
    }

    public void setTrhTrxDayPctDfDec(BigDecimal trhTrxDayPctDfDec) {
        this.trhTrxDayPctDfDec = trhTrxDayPctDfDec;
    }

    public BigDecimal getTrhTrxDayPctDfInc() {
        return trhTrxDayPctDfInc;
    }

    public void setTrhTrxDayPctDfInc(BigDecimal trhTrxDayPctDfInc) {
        this.trhTrxDayPctDfInc = trhTrxDayPctDfInc;
    }
}

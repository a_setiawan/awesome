package com.rintis.marketing.beans.entity;

import javax.persistence.*;

/**
 * Created by aabraham on 01/02/2021.
 */
@Entity
@Table(name = "tms_binlist")
public class BinList {
    @Id
    @Column(name = "list_id")
    private String listId;
    @Column(name = "bank_id")
    private String bankId;
    @Column(name = "bin")
    private String bin;

    @OneToOne
    @JoinColumn(name="bank_id", insertable=false, updatable=false)
    private TmsBankCa bank;

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public TmsBankCa getBank() {
        return bank;
    }

    public void setBank(TmsBankCa bank) {
        this.bank = bank;
    }

}

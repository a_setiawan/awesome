package com.rintis.marketing.beans.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * Created by aabraham on 22/02/2021.
 */
@Entity
@Table(name = "V_TMS_BANK_CA_BILLER")
public class TmsBankCaBiller extends AbstractLastUpdatedCreated {
    @Id
    @Column(name = "bank_id")
    private String bankId;
    @Column(name = "bank_name")
    private String bankName;
    @Column(name = "bank_short_name")
    private String bankShortName;
    @Column(name = "bank_atm_qty")
    private BigDecimal bankAtmQty;
    @Column(name = "bank_edc_qty")
    private BigDecimal bankEdcQty;
    @Column(name = "bank_payment")
    private String bankPayment;
    @Column(name = "bank_apn")
    private String bankApn;
    @Column(name = "bank_cup")
    private String bankCup;
    @Column(name = "bank_cardholder_qty")
    private BigDecimal bankCardholderQty;

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankShortName() {
        return bankShortName;
    }

    public void setBankShortName(String bankShortName) {
        this.bankShortName = bankShortName;
    }

    public BigDecimal getBankAtmQty() {
        return bankAtmQty;
    }

    public void setBankAtmQty(BigDecimal bankAtmQty) {
        this.bankAtmQty = bankAtmQty;
    }

    public BigDecimal getBankEdcQty() {
        return bankEdcQty;
    }

    public void setBankEdcQty(BigDecimal bankEdcQty) {
        this.bankEdcQty = bankEdcQty;
    }

    public String getBankPayment() {
        return bankPayment;
    }

    public void setBankPayment(String bankPayment) {
        this.bankPayment = bankPayment;
    }

    public String getBankApn() {
        return bankApn;
    }

    public void setBankApn(String bankApn) {
        this.bankApn = bankApn;
    }

    public String getBankCup() {
        return bankCup;
    }

    public void setBankCup(String bankCup) {
        this.bankCup = bankCup;
    }

    public BigDecimal getBankCardholderQty() {
        return bankCardholderQty;
    }

    public void setBankCardholderQty(BigDecimal bankCardholderQty) {
        this.bankCardholderQty = bankCardholderQty;
    }
}

package com.rintis.marketing.beans.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tms_group_bank_member")
public class GroupBankMember {
    @EmbeddedId
    private GroupBankMemberPk groupBankMemberPk;

    public GroupBankMemberPk getGroupBankMemberPk() {
        return groupBankMemberPk;
    }

    public void setGroupBankMemberPk(GroupBankMemberPk groupBankMemberPk) {
        this.groupBankMemberPk = groupBankMemberPk;
    }
}

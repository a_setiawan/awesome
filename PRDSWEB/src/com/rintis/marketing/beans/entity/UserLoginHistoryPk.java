package com.rintis.marketing.beans.entity;

import com.rintis.marketing.core.utils.StringUtils;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserLoginHistoryPk implements Serializable {
    @Column(name = "USER_LOGIN_ID")
    private String userLoginId;
    @Column(name = "FROM_DATE")
    private Date fromDate;

    public String getUserLoginId() {
        return userLoginId;
    }

    public void setUserLoginId(String userLoginId) {
        this.userLoginId = StringUtils.upperCaseNull(userLoginId);
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (!(other instanceof UserLoginHistoryPk))
            return false;

        final UserLoginHistoryPk pk = (UserLoginHistoryPk) other;

        if (!pk.getUserLoginId().equals(getUserLoginId()))
            return false;
        if (!pk.getFromDate().equals(getFromDate()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = getUserLoginId().hashCode();
        result = 29 * result + getFromDate().hashCode();
        return result;
    }
}

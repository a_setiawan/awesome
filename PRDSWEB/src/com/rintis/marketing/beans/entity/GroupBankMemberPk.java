package com.rintis.marketing.beans.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class GroupBankMemberPk implements Serializable {
    @Column(name = "group_id")
    private String groupId;
    @Column(name = "bank_id")
    private String bankId;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (!(other instanceof GroupBankMemberPk))
            return false;

        final GroupBankMemberPk pk = (GroupBankMemberPk) other;

        if (!pk.getGroupId().equals(getGroupId()))
            return false;
        if (!pk.getBankId().equals(getBankId()))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = getGroupId().hashCode();
        result = 29 * result + getBankId().hashCode();
        return result;
    }
}

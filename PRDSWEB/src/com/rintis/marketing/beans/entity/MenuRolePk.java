package com.rintis.marketing.beans.entity;

import com.rintis.marketing.core.utils.StringUtils;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MenuRolePk implements Serializable {
    @Column(name = "role_type_id")
    private Integer roleTypeId;
    @Column(name = "menu_id")
    private String menuId;

    

    public Integer getRoleTypeId() {
        return roleTypeId;
    }

    public void setRoleTypeId(Integer roleTypeId) {
        this.roleTypeId = roleTypeId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = StringUtils.upperCaseNull(menuId);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (!(other instanceof MenuRolePk))
            return false;

        final MenuRolePk pk = (MenuRolePk) other;

        if (!pk.getRoleTypeId().equals(getRoleTypeId()))
            return false;
        if (!pk.getMenuId().equals(getMenuId()))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = getRoleTypeId().hashCode();
        result = 29 * result + getMenuId().hashCode();
        return result;
    }
}

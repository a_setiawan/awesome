package com.rintis.marketing.beans.entity;

import com.rintis.marketing.core.utils.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MRKT_ENUMERATION_TYPE")
public class EnumerationType extends AbstractLastUpdatedCreated {
    @Id
    @Column(name = "enum_type_id")
    private String enumTypeId;
    @Column(name = "parent_type_id")
    private String parentTypeId;
    @Column(name = "description")
    private String description;

    public String getEnumTypeId() {
        return enumTypeId;
    }

    public void setEnumTypeId(String enumTypeId) {
        this.enumTypeId = StringUtils.upperCaseNull(enumTypeId);
    }

    public String getParentTypeId() {
        return parentTypeId;
    }

    public void setParentTypeId(String parentTypeId) {
        this.parentTypeId = StringUtils.upperCaseNull(parentTypeId);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = StringUtils.upperCaseNull(description);
    }
}

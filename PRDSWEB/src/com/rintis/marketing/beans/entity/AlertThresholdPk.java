package com.rintis.marketing.beans.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class AlertThresholdPk implements Serializable {
    @Column(name = "transaction_id")
    private String transactionId;
    @Column(name = "bank_id")
    private String bankId;
    @Column(name = "TRANSACTION_INDICATOR_ID")
    private String transactionIndicatorId;


    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getTransactionIndicatorId() {
        return transactionIndicatorId;
    }

    public void setTransactionIndicatorId(String transactionIndicatorId) {
        this.transactionIndicatorId = transactionIndicatorId;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (!(other instanceof AlertThresholdPk))
            return false;

        final AlertThresholdPk pk = (AlertThresholdPk) other;


        if (!pk.getTransactionId().equals(getTransactionId()))
            return false;
        if (!pk.getBankId().equals(getBankId()))
            return false;
        if (!pk.getTransactionIndicatorId().equals(getTransactionIndicatorId()))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = getTransactionId().hashCode();
        result = 29 * result + getBankId().hashCode();
        result = 29 * result + getTransactionIndicatorId().hashCode();
        return result;
    }


}

package com.rintis.marketing.beans.entity;

import javax.persistence.*;

@Entity
@Table(name = "tms_bin")
public class Bin extends AbstractLastUpdatedCreated{
    @Id
    @Column(name = "bank_id")
    private String bankId;
    @Column(name = "bin_no")
    private String binNo;
    @Column(name = "bin_length")
    private String binLength;

    @ManyToOne
    @JoinColumn(name="bank_id", insertable=false, updatable=false)
    private TmsBankCa bank;

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }

    public String getBinLength() {
        return binLength;
    }

    public void setBinLength(String binLength) {
        this.binLength = binLength;
    }

    public TmsBankCa getBank() {
        return bank;
    }

    public void setBank(TmsBankCa bank) {
        this.bank = bank;
    }

}

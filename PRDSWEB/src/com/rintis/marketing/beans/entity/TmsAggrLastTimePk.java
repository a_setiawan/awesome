package com.rintis.marketing.beans.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class TmsAggrLastTimePk implements Serializable {
    @Column(name = "group_id")
    private String groupId;
    @Column(name = "aggr_id")
    private String aggrId;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getAggrId() {
        return aggrId;
    }

    public void setAggrId(String aggrId) {
        this.aggrId = aggrId;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (!(other instanceof TmsAggrLastTimePk))
            return false;

        final TmsAggrLastTimePk pk = (TmsAggrLastTimePk) other;


        if (!pk.getGroupId().equals(getGroupId()))
            return false;
        if (!pk.getAggrId().equals(getAggrId()))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = getGroupId().hashCode();
        result = 29 * result + getAggrId().hashCode();
        return result;
    }
}

package com.rintis.marketing.beans.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class AlertThresholdGroupPk implements Serializable {
    @Column(name = "group_bank_id")
    private String groupBankId;
    @Column(name = "trx_id")
    private String trxId;
    @Column(name = "trx_indicator_id")
    private String trxIndicatorId;
    @Column(name = "timeframe")
    private String timeframe;

    public String getGroupBankId() {
        return groupBankId;
    }

    public void setGroupBankId(String groupBankId) {
        this.groupBankId = groupBankId;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getTrxIndicatorId() {
        return trxIndicatorId;
    }

    public void setTrxIndicatorId(String trxIndicatorId) {
        this.trxIndicatorId = trxIndicatorId;
    }

    public String getTimeframe() {
        return timeframe;
    }

    public void setTimeframe(String timeframe) {
        this.timeframe = timeframe;
    }


    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (!(other instanceof AlertThresholdGroupPk))
            return false;

        final AlertThresholdGroupPk pk = (AlertThresholdGroupPk) other;


        if (!pk.getGroupBankId().equals(getGroupBankId()))
            return false;
        if (!pk.getTrxId().equals(getTrxId()))
            return false;
        if (!pk.getTrxIndicatorId().equals(getTrxIndicatorId()))
            return false;
        if (!pk.getTimeframe().equals(getTimeframe()))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = getGroupBankId().hashCode();
        result = 29 * result + getTrxId().hashCode();
        result = 29 * result + getTrxIndicatorId().hashCode();
        result = 29 * result + getTimeframe().hashCode();
        return result;
    }


}

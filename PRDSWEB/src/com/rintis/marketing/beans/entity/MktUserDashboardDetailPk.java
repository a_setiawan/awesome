package com.rintis.marketing.beans.entity;

import com.rintis.marketing.core.utils.StringUtils;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.math.BigDecimal;

@Embeddable
public class MktUserDashboardDetailPk implements Serializable {
    @Column(name = "id")
    private BigDecimal id;
    @Column(name = "seq")
    private BigDecimal seq;


    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getSeq() {
        return seq;
    }

    public void setSeq(BigDecimal seq) {
        this.seq = seq;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (!(other instanceof MktUserDashboardDetailPk))
            return false;

        final MktUserDashboardDetailPk pk = (MktUserDashboardDetailPk) other;
        System.out.println(">>>>>>>>>>>>>>> ID : " + pk.getId());
        System.out.println(">>>>>>>>>>>>>>> SEQ : " + pk.getSeq());
        if (!pk.getId().equals(getId()))
            return false;
        if (!pk.getSeq().equals(getSeq()))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = getId().hashCode();
        result = 29 * result + getSeq().hashCode();
        System.out.println(">>>>>>>>>>>>>>> RESULT : " + result);
        return result;
    }
}

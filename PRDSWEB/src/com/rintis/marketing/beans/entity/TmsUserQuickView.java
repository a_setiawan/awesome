package com.rintis.marketing.beans.entity;

import com.rintis.marketing.core.constant.DataConstant;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tms_user_quickview")
public class TmsUserQuickView {
    @EmbeddedId
    private TmsUserQuickViewPk tmsUserQuickViewPk;
    @Column(name = "quick_date")
    private Date quickDate;
    @Column(name = "cbtot")
    private Integer cbTot;
    @Column(name = "cbapp")
    private Integer cbApp;
    @Column(name = "cbdc")
    private Integer cbDc;
    @Column(name = "cbdf")
    private Integer cbDf;

    @OneToOne
    @JoinColumn(name="bank_id", insertable=false, updatable=false)
    private TmsBankCaBiller bank;

    //@OneToOne
    //@JoinColumn(name="trx_id", referencedColumnName = "trx_id", insertable=false, updatable=false)
    //private EsqTrxid trx;
    @Transient
    private String trxName;
    @Transient
    private String intervalName;

    public String getTimeframeName() {
        return DataConstant.getTimeFrameName(tmsUserQuickViewPk.getTimeframe());
    }

    public TmsUserQuickViewPk getTmsUserQuickViewPk() {
        return tmsUserQuickViewPk;
    }

    public void setTmsUserQuickViewPk(TmsUserQuickViewPk tmsUserQuickViewPk) {
        this.tmsUserQuickViewPk = tmsUserQuickViewPk;
    }

    public Date getQuickDate() {
        return quickDate;
    }

    public void setQuickDate(Date quickDate) {
        this.quickDate = quickDate;
    }

    public TmsBankCaBiller getBank() {
        return bank;
    }

    public void setBank(TmsBankCaBiller bank) {
        this.bank = bank;
    }

    /*public EsqTrxid getTrx() {
        return trx;
    }

    public void setTrx(EsqTrxid trx) {
        this.trx = trx;
    }*/

    public String getTrxName() {
        return trxName;
    }

    public void setTrxName(String trxName) {
        this.trxName = trxName;
    }

    public String getIntervalName() {
        return intervalName;
    }

    public void setIntervalName(String intervalName) {
        this.intervalName = intervalName;
    }

    public Integer getCbTot() {
        return cbTot;
    }

    public void setCbTot(Integer cbTot) {
        this.cbTot = cbTot;
    }

    public Integer getCbApp() {
        return cbApp;
    }

    public void setCbApp(Integer cbApp) {
        this.cbApp = cbApp;
    }

    public Integer getCbDc() {
        return cbDc;
    }

    public void setCbDc(Integer cbDc) {
        this.cbDc = cbDc;
    }

    public Integer getCbDf() {
        return cbDf;
    }

    public void setCbDf(Integer cbDf) {
        this.cbDf = cbDf;
    }
}

package com.rintis.marketing.beans.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "tms_bank_alert_threshold")
public class BankAlertThreshold extends AbstractLastUpdatedCreated {
    @EmbeddedId
    private BankAlertThresholdPk bankAlertThresholdPk;
    @Column(name = "threshold_5min_wdwd")
    private BigDecimal threshold5MinWdWd;
    @Column(name = "threshold_5min_wdhd")
    private BigDecimal threshold5MinWdHd;
    @Column(name = "threshold_hour_wdwd")
    private BigDecimal thresholdHourWdWd;
    @Column(name = "threshold_hour_wdhd")
    private BigDecimal thresholdHourWdHd;
    @Column(name = "start_hour")
    private String startHour;
    @Column(name = "start_minute")
    private String startMinute;
    @Column(name = "end_hour")
    private String endHour;
    @Column(name = "end_minute")
    private String endMinute;
    @Column(name = "compare_method1")
    private BigDecimal compareMethod1;
    @Column(name = "compare_method2")
    private BigDecimal compareMethod2;
    @Column(name = "compare_method3")
    private BigDecimal compareMethod3;
    @Column(name = "compare_method4")
    private BigDecimal compareMethod4;
    @Column(name = "limit_interval_minute")
    private String limitIntervalMinute;
    @Column(name = "active")
    private BigDecimal active;
    @Column(name = "threshold_m1")
    private BigDecimal thresholdM1;

    public BankAlertThresholdPk getBankAlertThresholdPk() {
        return bankAlertThresholdPk;
    }

    public void setBankAlertThresholdPk(BankAlertThresholdPk bankAlertThresholdPk) {
        this.bankAlertThresholdPk = bankAlertThresholdPk;
    }

    public BigDecimal getThreshold5MinWdWd() {
        return threshold5MinWdWd;
    }

    public void setThreshold5MinWdWd(BigDecimal threshold5MinWdWd) {
        this.threshold5MinWdWd = threshold5MinWdWd;
    }

    public BigDecimal getThreshold5MinWdHd() {
        return threshold5MinWdHd;
    }

    public void setThreshold5MinWdHd(BigDecimal threshold5MinWdHd) {
        this.threshold5MinWdHd = threshold5MinWdHd;
    }

    public BigDecimal getThresholdHourWdWd() {
        return thresholdHourWdWd;
    }

    public void setThresholdHourWdWd(BigDecimal thresholdHourWdWd) {
        this.thresholdHourWdWd = thresholdHourWdWd;
    }

    public BigDecimal getThresholdHourWdHd() {
        return thresholdHourWdHd;
    }

    public void setThresholdHourWdHd(BigDecimal thresholdHourWdHd) {
        this.thresholdHourWdHd = thresholdHourWdHd;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(String startMinute) {
        this.startMinute = startMinute;
    }

    public String getEndHour() {
        return endHour;
    }

    public void setEndHour(String endHour) {
        this.endHour = endHour;
    }

    public String getEndMinute() {
        return endMinute;
    }

    public void setEndMinute(String endMinute) {
        this.endMinute = endMinute;
    }

    public BigDecimal getCompareMethod1() {
        return compareMethod1;
    }

    public void setCompareMethod1(BigDecimal compareMethod1) {
        this.compareMethod1 = compareMethod1;
    }

    public BigDecimal getCompareMethod2() {
        return compareMethod2;
    }

    public void setCompareMethod2(BigDecimal compareMethod2) {
        this.compareMethod2 = compareMethod2;
    }

    public String getLimitIntervalMinute() {
        return limitIntervalMinute;
    }

    public void setLimitIntervalMinute(String limitIntervalMinute) {
        this.limitIntervalMinute = limitIntervalMinute;
    }

    public BigDecimal getCompareMethod3() {
        return compareMethod3;
    }

    public void setCompareMethod3(BigDecimal compareMethod3) {
        this.compareMethod3 = compareMethod3;
    }

    public BigDecimal getCompareMethod4() {
        return compareMethod4;
    }

    public void setCompareMethod4(BigDecimal compareMethod4) {
        this.compareMethod4 = compareMethod4;
    }

    public BigDecimal getActive() {
        return active;
    }

    public void setActive(BigDecimal active) {
        this.active = active;
    }

    public BigDecimal getThresholdM1() {
        return thresholdM1;
    }

    public void setThresholdM1(BigDecimal thresholdM1) {
        this.thresholdM1 = thresholdM1;
    }

}

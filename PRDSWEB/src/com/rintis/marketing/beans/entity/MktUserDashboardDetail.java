package com.rintis.marketing.beans.entity;

import javax.persistence.*;

@Entity
@Table(name = "tms_user_dashboard_detail")
public class MktUserDashboardDetail extends AbstractLastUpdatedCreated {
    @EmbeddedId
    private MktUserDashboardDetailPk mktUserDashboardDetailPk;
    @Column(name = "bank_id")
    private String bankId;
    @Column(name = "transaction_id")
    private String transactionId;
    @Column(name = "transaction_indicator_id")
    private String transactionIndicatorId;
    @Column(name = "sortidx")
    private Integer sortIdx;
    @Column(name = "monitoring_interval")
    private String monitoringInterval;

    @Transient
    private String bankName;
    @Transient
    private String transactionName;
    @Transient
    private String transactionIndicatorName;


    public MktUserDashboardDetailPk getMktUserDashboardDetailPk() {
        return mktUserDashboardDetailPk;
    }

    public void setMktUserDashboardDetailPk(MktUserDashboardDetailPk mktUserDashboardDetailPk) {
        this.mktUserDashboardDetailPk = mktUserDashboardDetailPk;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionIndicatorId() {
        return transactionIndicatorId;
    }

    public void setTransactionIndicatorId(String transactionIndicatorId) {
        this.transactionIndicatorId = transactionIndicatorId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public String getTransactionIndicatorName() {
        return transactionIndicatorName;
    }

    public void setTransactionIndicatorName(String transactionIndicatorName) {
        this.transactionIndicatorName = transactionIndicatorName;
    }

    public Integer getSortIdx() {
        return sortIdx;
    }

    public void setSortIdx(Integer sortIdx) {
        this.sortIdx = sortIdx;
    }

    public String getMonitoringInterval() {
        return monitoringInterval;
    }

    public void setMonitoringInterval(String monitoringInterval) {
        this.monitoringInterval = monitoringInterval;
    }
}

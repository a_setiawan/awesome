package com.rintis.marketing.beans.entity;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * Created by aabraham on 01/02/2021.
 */
public class BinListPk implements Serializable {
    @Column(name = "list_id")
    private String listId;
    @Column(name = "trx_id")
    private String trxId;

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (!(other instanceof BinListPk))
            return false;

        final BinListPk pk = (BinListPk) other;

        if (!pk.getListId().equals(getListId()))
            return false;
        if (!pk.getTrxId().equals(getTrxId()))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = getListId().hashCode();
        result = 29 * result + getTrxId().hashCode();
        return result;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }
}

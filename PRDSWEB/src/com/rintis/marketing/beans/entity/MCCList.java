package com.rintis.marketing.beans.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by aabraham on 29/03/2021.
 */
@Entity
@Table(name = "tms_mcc")
public class  MCCList {
    @Id
    @Column(name = "mcc")
    private String mcc;
    @Column(name = "description")
    private String description;

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}


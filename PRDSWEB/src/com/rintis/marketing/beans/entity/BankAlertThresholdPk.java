package com.rintis.marketing.beans.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class BankAlertThresholdPk implements Serializable {
    @Column(name = "transaction_id")
    private String transactionId;
    @Column(name = "bank_id")
    private String bankId;


    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (!(other instanceof BankAlertThresholdPk))
            return false;

        final BankAlertThresholdPk pk = (BankAlertThresholdPk) other;


        if (!pk.getTransactionId().equals(getTransactionId()))
            return false;
        if (!pk.getBankId().equals(getBankId()))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = getTransactionId().hashCode();
        result = 29 * result + getBankId().hashCode();
        return result;
    }


}

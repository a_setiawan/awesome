package com.rintis.marketing.beans.entity;

import javax.persistence.*;

@Entity
@Table(name = "TMS_SYSTEM_PROPERTY_USER")
public class SystemPropertyUser extends AbstractLastUpdatedCreated {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "TMS_SYSTEMPROPERTYUSER_SEQ")
    @SequenceGenerator(name = "TMS_SYSTEMPROPERTYUSER_SEQ", sequenceName = "TMS_SYSTEMPROPERTYUSER_SEQ")
    @Column(name = "id")
    private Integer id;
	@Column(name = "user_login_id")
    private String userLoginId;
    @Column(name = "system_name")
    private String systemName;
    @Column(name = "system_value")
    private String systemValue;

    @Transient
    private String tempValue;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserLoginId() {
        return userLoginId;
    }

    public void setUserLoginId(String userLoginId) {
        this.userLoginId = userLoginId;
    }
	
	public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getSystemValue() {
        return systemValue;
    }

    public void setSystemValue(String systemValue) {
        this.systemValue = systemValue;
    }

    public String getTempValue() {
        return tempValue;
    }

    public void setTempValue(String tempValue) {
        this.tempValue = tempValue;
    }
}

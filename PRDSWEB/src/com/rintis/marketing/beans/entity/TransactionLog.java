package com.rintis.marketing.beans.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "TMS_TRANSACTION_LOG")
public class TransactionLog extends AbstractLastUpdatedCreated {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "TMS_TRANSLOG_SEQ")
    @SequenceGenerator(name = "TMS_TRANSLOG_SEQ", sequenceName = "TMS_TRANSLOG_SEQ")
    @Column(name = "LOG_ID")
    private Long logId;
    @Column(name = "TRANSACTION_ID")
    private String transactionId;
    @Column(name = "LOG_DATE")
    private Date logDate;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "USER_ID")
    private String userId;

    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

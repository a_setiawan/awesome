package com.rintis.marketing.beans.entity;

import com.rintis.marketing.core.utils.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MRKT_ENUMERATION")
public class Enumeration extends AbstractLastUpdatedCreated {
    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "enum_id")
    private String enumId;
    @Column(name = "enum_type_id")
    private String enumTypeId;
    @Column(name = "seq")
    private Integer seq;
    @Column(name = "description")
    private String description;

    public String getEnumId() {
        return enumId;
    }

    public void setEnumId(String enumId) {
        this.enumId = StringUtils.upperCaseNull(enumId);
    }

    public String getEnumTypeId() {
        return enumTypeId;
    }

    public void setEnumTypeId(String enumTypeId) {
        this.enumTypeId = StringUtils.upperCaseNull(enumTypeId);
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = StringUtils.upperCaseNull(description);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}

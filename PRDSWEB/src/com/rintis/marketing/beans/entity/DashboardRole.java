package com.rintis.marketing.beans.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "tms_dashboard_role")
public class DashboardRole extends AbstractLastUpdatedCreated {
    @EmbeddedId
    private DashboardRolePk dashboardRolePk;


    public DashboardRolePk getDashboardRolePk() {
        return dashboardRolePk;
    }

    public void setDashboardRolePk(DashboardRolePk dashboardRolePk) {
        this.dashboardRolePk = dashboardRolePk;
    }
}

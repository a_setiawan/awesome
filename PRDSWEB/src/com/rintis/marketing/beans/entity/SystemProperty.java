package com.rintis.marketing.beans.entity;

import javax.persistence.*;

@Entity
@Table(name = "tms_system_property")
public class SystemProperty extends AbstractLastUpdatedCreated {
    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "system_name")
    private String systemName;
    @Column(name = "system_value")
    private String systemValue;
    @Column(name = "per_user")
    private Integer perUser;
    @Column(name = "description")
    private String description;

    @Transient
    private String tempValue;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getSystemValue() {
        return systemValue;
    }

    public void setSystemValue(String systemValue) {
        this.systemValue = systemValue;
    }

    public Integer getPerUser() {
        return perUser;
    }

    public void setPerUser(Integer perUser) {
        this.perUser = perUser;
    }

    public String getTempValue() {
        return tempValue;
    }

    public void setTempValue(String tempValue) {
        this.tempValue = tempValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

package com.rintis.marketing.beans.entity;

import com.rintis.marketing.core.utils.StringUtils;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "tms_user_login")
public class UserLogin extends AbstractLastUpdatedCreated {
    @Id
    @Column(name = "user_login_id")
    private String userLoginId;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "current_password")
    private String currentPassword;
    @Column(name = "enabled")
    private Integer enabled;
    @Column(name = "last_login")
    private Date lastLogin;
    @Column(name = "role_type_id")
    private Integer roleTypeId;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "email")
    private String email;
    @Column(name = "validation_type")
    private String validationType;
    @Column(name = "user_bank")
    private Integer userBank;
    @Column(name = "bank_id")
    private String bankId;

    @OneToOne
    @JoinColumn(name="role_type_id", insertable=false, updatable=false)
    private MenuRoleType menuRoleType;

    @OneToOne
    @JoinColumn(name="bank_id", insertable=false, updatable=false)
    private EisBank bank;

    public String getUserLoginId() {
        return userLoginId;
    }

    public void setUserLoginId(String userLoginId) {
        this.userLoginId = StringUtils.upperCaseNull(userLoginId);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = StringUtils.upperCaseNull(userName);
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Integer getRoleTypeId() {
        return roleTypeId;
    }

    public void setRoleTypeId(Integer roleTypeId) {
        this.roleTypeId = roleTypeId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = StringUtils.upperCaseNull(mobile);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = StringUtils.upperCaseNull(email);
    }

    public MenuRoleType getMenuRoleType() {
        return menuRoleType;
    }

    public void setMenuRoleType(MenuRoleType menuRoleType) {
        this.menuRoleType = menuRoleType;
    }

    public String getValidationType() {
        return validationType;
    }

    public void setValidationType(String validationType) {
        this.validationType = validationType;
    }

    public Integer getUserBank() {
        return userBank;
    }

    public void setUserBank(Integer userBank) {
        this.userBank = userBank;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public EisBank getBank() {
        return bank;
    }

    public void setBank(EisBank bank) {
        this.bank = bank;
    }
}

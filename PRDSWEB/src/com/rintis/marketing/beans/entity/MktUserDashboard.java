package com.rintis.marketing.beans.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "tms_user_dashboard")
public class MktUserDashboard extends AbstractLastUpdatedCreated {
    @Id
    @Column(name = "id")
    private BigDecimal id;
    @Column(name = "user_id")
    private String userId;
    @Column(name = "menu_name")
    private String menuName;
    @Column(name = "description")
    private String description;

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

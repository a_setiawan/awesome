package com.rintis.marketing.beans.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by aabraham on 01/02/2021.
 */
@Entity
@Table(name = "tms_binlist_trxid")
public class BinListTrxId {
    @EmbeddedId
    private BinListPk binListPk;

    public BinListPk getBinListPk() {
        return binListPk;
    }

    public void setBinListPk(BinListPk binListPk) {
        this.binListPk = binListPk;
    }
}

package com.rintis.marketing.beans.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/*Add by aaw 20211112*/
@Entity
@Table(name = "tms_billerdummy")
public class BillerDummy extends AbstractLastUpdatedCreated {
    @Id
    @Column(name = "inst_cd")
    private String instCd;
    @Id
    @Column(name = "cust_no")
    private String custNo;
    @Column(name = "cust_name")
    private String custName;
    @Column(name = "freq_app")
    private BigDecimal freqApp;
    @Column(name = "freq_dc")
    private BigDecimal freqDc;
    @Column(name = "freq_dc_free")
    private BigDecimal freqDcFree;
    @Column(name = "total_amt_app")
    private BigDecimal totalAmtApp;
    @Column(name = "total_amt_dc")
    private BigDecimal totalAmtDc;
    @Column(name = "total_amt_dc_free")
    private BigDecimal totalAmtDcFree;
    @Column(name = "total_amt")
    private BigDecimal totalAmt;
    @Column(name = "active")
    private String active;

    public String getInstCd() {
        return instCd;
    }

    public void setInstCd(String instCd) {
        this.instCd = instCd;
    }

    public String getCustNo() {
        return custNo;
    }

    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public BigDecimal getFreqApp() {
        return freqApp;
    }

    public void setFreqApp(BigDecimal freqApp) {
        this.freqApp = freqApp;
    }

    public BigDecimal getFreqDc() {
        return freqDc;
    }

    public void setFreqDc(BigDecimal freqDc) {
        this.freqDc = freqDc;
    }

    public BigDecimal getFreqDcFree() {
        return freqDcFree;
    }

    public void setFreqDcFree(BigDecimal freqDcFree) {
        this.freqDcFree = freqDcFree;
    }

    public BigDecimal getTotalAmtApp() {
        return totalAmtApp;
    }

    public void setTotalAmtApp(BigDecimal totalAmtApp) {
        this.totalAmtApp = totalAmtApp;
    }

    public BigDecimal getTotalAmtDc() {
        return totalAmtDc;
    }

    public void setTotalAmtDc(BigDecimal totalAmtDc) {
        this.totalAmtDc = totalAmtDc;
    }

    public BigDecimal getTotalAmtDcFree() {
        return totalAmtDcFree;
    }

    public void setTotalAmtDcFree(BigDecimal totalAmtDcFree) {
        this.totalAmtDcFree = totalAmtDcFree;
    }

    public BigDecimal getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
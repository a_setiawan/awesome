package com.rintis.marketing.beans.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "tms_menu_role")
public class MenuRole extends AbstractLastUpdatedCreated {
    @EmbeddedId
    private MenuRolePk menuRolePk;



    public MenuRolePk getMenuRolePk() {
        return menuRolePk;
    }

    public void setMenuRolePk(MenuRolePk menuRolePk) {
        this.menuRolePk = menuRolePk;
    }




    

    
}

package com.rintis.marketing.beans.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "tms_master_holiday")
public class TmsMasterHoliday extends AbstractLastUpdatedCreated {
    @Id
    @Column(name = "id")
    private BigDecimal id;
    @Column(name = "year")
    private Integer year;
    @Column(name = "holiday_date")
    private Date holidayDate;
    @Column(name = "description")
    private String description;

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Date getHolidayDate() {
        return holidayDate;
    }

    public void setHolidayDate(Date holidayDate) {
        this.holidayDate = holidayDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

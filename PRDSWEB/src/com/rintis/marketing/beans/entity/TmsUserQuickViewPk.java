package com.rintis.marketing.beans.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class TmsUserQuickViewPk implements Serializable {
    @Column(name = "user_id")
    private String userId;
    @Column(name = "bank_id")
    private String bankId;
    @Column(name = "trx_id")
    private String trxId;
    @Column(name = "timeframe")
    private String timeframe;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getTimeframe() {
        return timeframe;
    }

    public void setTimeframe(String timeframe) {
        this.timeframe = timeframe;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (!(other instanceof TmsUserQuickViewPk))
            return false;

        final TmsUserQuickViewPk pk = (TmsUserQuickViewPk) other;


        if (!pk.getUserId().equals(getUserId()))
            return false;
        if (!pk.getBankId().equals(getBankId()))
            return false;
        if (!pk.getTrxId().equals(getTrxId()))
            return false;
        if (!pk.getTimeframe().equals(getTimeframe()))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = getUserId().hashCode();
        result = 29 * result + getBankId().hashCode();
        result = 29 * result + getTrxId().hashCode();
        result = 29 * result + getTimeframe().hashCode();
        return result;
    }
}

package com.rintis.marketing.beans.entity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by aabraham on 22/04/2021.
 */

@Entity
@Table(name = "tms_param_mcc")
public class MCCParameter {

    @Id
    @Column(name = "mcc")
    private String mcc;
    @Column(name = "min_trx_app_1d")
    private BigDecimal min_trx_app_1d;
    @Column(name = "min_trx_tot_1d")
    private BigDecimal min_trx_tot_1d;
    @Column(name = "min_amt_app_1d")
    private BigDecimal min_amt_app_1d;
    @Column(name = "min_amt_tot_1d")
    private BigDecimal min_amt_tot_1d;

    @OneToOne
    @JoinColumn(name="mcc", insertable=false, updatable=false)
    private MCCList mccList;

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public BigDecimal getMin_trx_app_1d() {
        return min_trx_app_1d;
    }

    public void setMin_trx_app_1d(BigDecimal min_trx_app_1d) {
        this.min_trx_app_1d = min_trx_app_1d;
    }

    public BigDecimal getMin_trx_tot_1d() {
        return min_trx_tot_1d;
    }

    public void setMin_trx_tot_1d(BigDecimal min_trx_tot_1d) {
        this.min_trx_tot_1d = min_trx_tot_1d;
    }

    public BigDecimal getMin_amt_app_1d() {
        return min_amt_app_1d;
    }

    public void setMin_amt_app_1d(BigDecimal min_amt_app_1d) {
        this.min_amt_app_1d = min_amt_app_1d;
    }

    public BigDecimal getMin_amt_tot_1d() {
        return min_amt_tot_1d;
    }

    public void setMin_amt_tot_1d(BigDecimal min_amt_tot_1d) {
        this.min_amt_tot_1d = min_amt_tot_1d;
    }

    public MCCList getMccList() {
        return mccList;
    }

    public void setMccList(MCCList mccList) {
        this.mccList = mccList;
    }
}

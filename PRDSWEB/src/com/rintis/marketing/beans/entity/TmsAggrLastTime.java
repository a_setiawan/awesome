package com.rintis.marketing.beans.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "TMS_AGGR_LASTTIME")
public class TmsAggrLastTime {
    @EmbeddedId
    private TmsAggrLastTimePk tmsAggrLastTimePk;
    @Column(name = "last_time")
    private Date lastTime;
    @Column(name = "last_time_rnd")
    private String lastTimeRnd;

    public TmsAggrLastTimePk getTmsAggrLastTimePk() {
        return tmsAggrLastTimePk;
    }

    public void setTmsAggrLastTimePk(TmsAggrLastTimePk tmsAggrLastTimePk) {
        this.tmsAggrLastTimePk = tmsAggrLastTimePk;
    }

    public Date getLastTime() {
        return lastTime;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }

    public String getLastTimeRnd() {
        return lastTimeRnd;
    }

    public void setLastTimeRnd(String lastTimeRnd) {
        this.lastTimeRnd = lastTimeRnd;
    }
}

package com.rintis.marketing.beans.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by aaw 20211115
 */

@Entity
@Table(name = "tms_param_anmly_biller")
public class BillerAnomalyParam extends AbstractLastUpdatedCreated {
    @Id
    @Column(name = "trx_id")
    private String trxId;

    @Column(name = "min_trx_app_1h")
    private BigDecimal minTrxApp1h;

    @Column(name = "min_amt_app_1h")
    private BigDecimal minAmtApp1h;

    @Column(name = "min_trx_dc_1h")
    private BigDecimal minTrxDc1h;

    @Column(name = "min_amt_dc_1h")
    private BigDecimal minAmtDc1h;

    @Column(name = "min_trx_dc_free_1h")
    private BigDecimal minTrxDcFree1h;

    @Column(name = "min_amt_dc_free_1h")
    private BigDecimal minAmtDcFree1h;

    @Column(name = "min_trx_app_3h")
    private BigDecimal minTrxApp3h;

    @Column(name = "min_amt_app_3h")
    private BigDecimal minAmtApp3h;

    @Column(name = "min_trx_dc_3h")
    private BigDecimal minTrxDc3h;

    @Column(name = "min_amt_dc_3h")
    private BigDecimal minAmtDc3h;

    @Column(name = "min_trx_dc_free_3h")
    private BigDecimal minTrxDcFree3h;

    @Column(name = "min_amt_dc_free_3h")
    private BigDecimal minAmtDcFree3h;

    @Column(name = "min_trx_app_6h")
    private BigDecimal minTrxApp6h;

    @Column(name = "min_amt_app_6h")
    private BigDecimal minAmtApp6h;

    @Column(name = "min_trx_dc_6h")
    private BigDecimal minTrxDc6h;

    @Column(name = "min_amt_dc_6h")
    private BigDecimal minAmtDc6h;

    @Column(name = "min_trx_dc_free_6h")
    private BigDecimal minTrxDcFree6h;

    @Column(name = "min_amt_dc_free_6h")
    private BigDecimal minAmtDcFree6h;

    @Column(name = "min_trx_app_1d")
    private BigDecimal minTrxApp1d;

    @Column(name = "min_amt_app_1d")
    private BigDecimal minAmtApp1d;

    @Column(name = "min_trx_dc_1d")
    private BigDecimal minTrxDc1d;

    @Column(name = "min_amt_dc_1d")
    private BigDecimal minAmtDc1d;

    @Column(name = "min_trx_dc_free_1d")
    private BigDecimal minTrxDcFree1d;

    @Column(name = "min_amt_dc_free_1d")
    private BigDecimal minAmtDcFree1d;

    @Column(name = "min_trx_app_3d")
    private BigDecimal minTrxApp3d;

    @Column(name = "min_amt_app_3d")
    private BigDecimal minAmtApp3d;

    @Column(name = "min_trx_dc_3d")
    private BigDecimal minTrxDc3d;

    @Column(name = "min_amt_dc_3d")
    private BigDecimal minAmtDc3d;

    @Column(name = "min_trx_dc_free_3d")
    private BigDecimal minTrxDcFree3d;

    @Column(name = "min_amt_dc_free_3d")
    private BigDecimal minAmtDcFree3d;

    @OneToOne
    @JoinColumn(name="trx_id", referencedColumnName = "trx_id", insertable=false, updatable=false)
    @NotFound(action = NotFoundAction.IGNORE)
    private EsqTrxid esqTrxid;

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public BigDecimal getMinTrxApp1h() {
        return minTrxApp1h;
    }

    public void setMinTrxApp1h(BigDecimal minTrxApp1h) {
        this.minTrxApp1h = minTrxApp1h;
    }

    public BigDecimal getMinAmtApp1h() {
        return minAmtApp1h;
    }

    public void setMinAmtApp1h(BigDecimal minAmtApp1h) {
        this.minAmtApp1h = minAmtApp1h;
    }

    public BigDecimal getMinTrxDc1h() {
        return minTrxDc1h;
    }

    public void setMinTrxDc1h(BigDecimal minTrxDc1h) {
        this.minTrxDc1h = minTrxDc1h;
    }

    public BigDecimal getMinAmtDc1h() {
        return minAmtDc1h;
    }

    public void setMinAmtDc1h(BigDecimal minAmtDc1h) {
        this.minAmtDc1h = minAmtDc1h;
    }

    public BigDecimal getMinTrxDcFree1h() {
        return minTrxDcFree1h;
    }

    public void setMinTrxDcFree1h(BigDecimal minTrxDcFree1h) {
        this.minTrxDcFree1h = minTrxDcFree1h;
    }

    public BigDecimal getMinAmtDcFree1h() {
        return minAmtDcFree1h;
    }

    public void setMinAmtDcFree1h(BigDecimal minAmtDcFree1h) {
        this.minAmtDcFree1h = minAmtDcFree1h;
    }

    public BigDecimal getMinTrxApp3h() {
        return minTrxApp3h;
    }

    public void setMinTrxApp3h(BigDecimal minTrxApp3h) {
        this.minTrxApp3h = minTrxApp3h;
    }

    public BigDecimal getMinAmtApp3h() {
        return minAmtApp3h;
    }

    public void setMinAmtApp3h(BigDecimal minAmtApp3h) {
        this.minAmtApp3h = minAmtApp3h;
    }

    public BigDecimal getMinTrxDc3h() {
        return minTrxDc3h;
    }

    public void setMinTrxDc3h(BigDecimal minTrxDc3h) {
        this.minTrxDc3h = minTrxDc3h;
    }

    public BigDecimal getMinAmtDc3h() {
        return minAmtDc3h;
    }

    public void setMinAmtDc3h(BigDecimal minAmtDc3h) {
        this.minAmtDc3h = minAmtDc3h;
    }

    public BigDecimal getMinTrxDcFree3h() {
        return minTrxDcFree3h;
    }

    public void setMinTrxDcFree3h(BigDecimal minTrxDcFree3h) {
        this.minTrxDcFree3h = minTrxDcFree3h;
    }

    public BigDecimal getMinAmtDcFree3h() {
        return minAmtDcFree3h;
    }

    public void setMinAmtDcFree3h(BigDecimal minAmtDcFree3h) {
        this.minAmtDcFree3h = minAmtDcFree3h;
    }

    public BigDecimal getMinTrxApp6h() {
        return minTrxApp6h;
    }

    public void setMinTrxApp6h(BigDecimal minTrxApp6h) {
        this.minTrxApp6h = minTrxApp6h;
    }

    public BigDecimal getMinAmtApp6h() {
        return minAmtApp6h;
    }

    public void setMinAmtApp6h(BigDecimal minAmtApp6h) {
        this.minAmtApp6h = minAmtApp6h;
    }

    public BigDecimal getMinTrxDc6h() {
        return minTrxDc6h;
    }

    public void setMinTrxDc6h(BigDecimal minTrxDc6h) {
        this.minTrxDc6h = minTrxDc6h;
    }

    public BigDecimal getMinAmtDc6h() {
        return minAmtDc6h;
    }

    public void setMinAmtDc6h(BigDecimal minAmtDc6h) {
        this.minAmtDc6h = minAmtDc6h;
    }

    public BigDecimal getMinTrxDcFree6h() {
        return minTrxDcFree6h;
    }

    public void setMinTrxDcFree6h(BigDecimal minTrxDcFree6h) {
        this.minTrxDcFree6h = minTrxDcFree6h;
    }

    public BigDecimal getMinAmtDcFree6h() {
        return minAmtDcFree6h;
    }

    public void setMinAmtDcFree6h(BigDecimal minAmtDcFree6h) {
        this.minAmtDcFree6h = minAmtDcFree6h;
    }

    public BigDecimal getMinTrxApp1d() {
        return minTrxApp1d;
    }

    public void setMinTrxApp1d(BigDecimal minTrxApp1d) {
        this.minTrxApp1d = minTrxApp1d;
    }

    public BigDecimal getMinAmtApp1d() {
        return minAmtApp1d;
    }

    public void setMinAmtApp1d(BigDecimal minAmtApp1d) {
        this.minAmtApp1d = minAmtApp1d;
    }

    public BigDecimal getMinTrxDc1d() {
        return minTrxDc1d;
    }

    public void setMinTrxDc1d(BigDecimal minTrxDc1d) {
        this.minTrxDc1d = minTrxDc1d;
    }

    public BigDecimal getMinAmtDc1d() {
        return minAmtDc1d;
    }

    public void setMinAmtDc1d(BigDecimal minAmtDc1d) {
        this.minAmtDc1d = minAmtDc1d;
    }

    public BigDecimal getMinTrxDcFree1d() {
        return minTrxDcFree1d;
    }

    public void setMinTrxDcFree1d(BigDecimal minTrxDcFree1d) {
        this.minTrxDcFree1d = minTrxDcFree1d;
    }

    public BigDecimal getMinAmtDcFree1d() {
        return minAmtDcFree1d;
    }

    public void setMinAmtDcFree1d(BigDecimal minAmtDcFree1d) {
        this.minAmtDcFree1d = minAmtDcFree1d;
    }

    public BigDecimal getMinTrxApp3d() {
        return minTrxApp3d;
    }

    public void setMinTrxApp3d(BigDecimal minTrxApp3d) {
        this.minTrxApp3d = minTrxApp3d;
    }

    public BigDecimal getMinAmtApp3d() {
        return minAmtApp3d;
    }

    public void setMinAmtApp3d(BigDecimal minAmtApp3d) {
        this.minAmtApp3d = minAmtApp3d;
    }

    public BigDecimal getMinTrxDc3d() {
        return minTrxDc3d;
    }

    public void setMinTrxDc3d(BigDecimal minTrxDc3d) {
        this.minTrxDc3d = minTrxDc3d;
    }

    public BigDecimal getMinAmtDc3d() {
        return minAmtDc3d;
    }

    public void setMinAmtDc3d(BigDecimal minAmtDc3d) {
        this.minAmtDc3d = minAmtDc3d;
    }

    public BigDecimal getMinTrxDcFree3d() {
        return minTrxDcFree3d;
    }

    public void setMinTrxDcFree3d(BigDecimal minTrxDcFree3d) {
        this.minTrxDcFree3d = minTrxDcFree3d;
    }

    public BigDecimal getMinAmtDcFree3d() {
        return minAmtDcFree3d;
    }

    public void setMinAmtDcFree3d(BigDecimal minAmtDcFree3d) {
        this.minAmtDcFree3d = minAmtDcFree3d;
    }

    public EsqTrxid getEsqTrxid() {
        return esqTrxid;
    }

    public void setEsqTrxid(EsqTrxid esqTrxid) {
        this.esqTrxid = esqTrxid;
    }
}

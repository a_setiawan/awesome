package com.rintis.marketing.beans.entity;

import com.rintis.marketing.core.utils.StringUtils;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tms_user_login_history")
public class UserLoginHistory extends AbstractLastUpdatedCreated {
    @EmbeddedId
    private UserLoginHistoryPk userLoginHistoryPk;
    @Column(name = "thru_date")
    private Date thruDate;
    @Column(name = "ip_address")
    private String ipAddress;

    public UserLoginHistoryPk getUserLoginHistoryPk() {
        return userLoginHistoryPk;
    }

    public void setUserLoginHistoryPk(UserLoginHistoryPk userLoginHistoryPk) {
        this.userLoginHistoryPk = userLoginHistoryPk;
    }

    public Date getThruDate() {
        return thruDate;
    }

    public void setThruDate(Date thruDate) {
        this.thruDate = thruDate;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = StringUtils.upperCaseNull(ipAddress);
    }

}

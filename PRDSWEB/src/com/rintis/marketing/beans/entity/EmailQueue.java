package com.rintis.marketing.beans.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tms_email_queue")
public class EmailQueue {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "TMS_EMAILQUEUE_SEQ")
    @SequenceGenerator(name = "TMS_EMAILQUEUE_SEQ", sequenceName = "TMS_EMAILQUEUE_SEQ")
    @Column(name = "id")
    private Integer id;
    @Column(name = "email_to")
    private String emailTo;
    @Column(name = "email_subject")
    private String emailSubject;
    @Column(name = "email_message")
    private String emailMessage;
    @Column(name = "queue_date")
    private Date queueDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmailTo() {
        return emailTo;
    }

    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailMessage() {
        return emailMessage;
    }

    public void setEmailMessage(String emailMessage) {
        this.emailMessage = emailMessage;
    }

    public Date getQueueDate() {
        return queueDate;
    }

    public void setQueueDate(Date queueDate) {
        this.queueDate = queueDate;
    }
}

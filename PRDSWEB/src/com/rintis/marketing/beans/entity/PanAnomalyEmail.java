package com.rintis.marketing.beans.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "tms_anmly_email")
public class PanAnomalyEmail extends AbstractLastUpdatedCreated implements Serializable {
    @Id
    @Column(name = "user_email")
    private String useremail;
    @Column(name = "gender")
    private String gender;
    @Column(name = "user_name")
    private String username;
    @Column(name = "alert_1h_status")
    private String alert1h;
    @Column(name = "alert_3h_status")
    private String alert3h;
    @Column(name = "alert_6h_status")
    private String alert6h;
    @Column(name = "alert_1d_status")
    private String alert1d;
    @Column(name = "alert_3d_status")
    private String alert3d;
    @Column(name = "alert_maxamt_status")
    private String alertMaxAmt;
    @Column(name = "alert_mcc_status")
    private String alertMccVelocity;
    @Column(name = "alert_seq_status")
    private String alertSeqVelocity;
    @Column(name = "alert_mpan_acq")
    private String alertMpanAcq;

    public String getUserEmail() {
        return useremail;
    }
    public void setUserEmail(String useremail) {
        this.useremail = useremail;
    }

    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserName() {
        return username;
    }
    public void setUserName(String username) {
        this.username = username;
    }

    public String getAlert1H() {
        return alert1h;
    }
    public void setAlert1H(String alert1h) {
        this.alert1h = alert1h;
    }

    public String getAlert3H() {
        return alert3h;
    }
    public void setAlert3H(String alert3h) {
        this.alert3h = alert3h;
    }

    public String getAlert6H(){
        return alert6h;
    }
    public void setAlert6H(String alert6h) { this.alert6h = alert6h;}

    public String getAlert1D() {
        return alert1d;
    }
    public void setAlert1D(String alert1d) {
        this.alert1d = alert1d;
    }

    public String getAlert3D() {
        return alert3d;
    }
    public void setAlert3D(String alert3d) {
        this.alert3d = alert3d;
    }

    public String getAlertMaxAmt() {
        return alertMaxAmt;
    }
    public void setAlertMaxAmt(String alertMaxAmt) {
        this.alertMaxAmt = alertMaxAmt;
    }

    public String getAlertMccVelocity() {
        return alertMccVelocity;
    }

    public void setAlertMccVelocity(String alertMccVelocity) {
        this.alertMccVelocity = alertMccVelocity;
    }

    public String getAlertSeqVelocity() {
        return alertSeqVelocity;
    }

    public void setAlertSeqVelocity(String alertSeqVelocity) {
        this.alertSeqVelocity = alertSeqVelocity;
    }

    public String getAlertMpanAcq() {
        return alertMpanAcq;
    }

    public void setAlertMpanAcq(String alertMpanAcq) {
        this.alertMpanAcq = alertMpanAcq;
    }
}

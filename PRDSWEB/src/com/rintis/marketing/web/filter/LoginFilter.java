package com.rintis.marketing.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.rintis.marketing.web.common.SessionConstant;

@WebFilter(filterName="LoginFilter", urlPatterns="/apps/*")
public class LoginFilter implements Filter {

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)req;
        HttpServletResponse response = (HttpServletResponse)res;
        HttpSession ses = request.getSession();

        String userInfo = (String)ses.getAttribute(SessionConstant.SESSION_USERINFO);
        //System.out.println(userInfo);
        if (userInfo == null) {
            String x = Long.toString(System.currentTimeMillis()).substring(5);
            response.sendRedirect(request.getContextPath() + "/login.do?x="+x);
            return;
        } else {

            //protek akses ke menu yg tidak mempunyai hak akses
            String uri = request.getRequestURI();
            int i = uri.indexOf("/apps/");
            uri = uri.substring(i, uri.length());
            String menu = (String)ses.getAttribute(SessionConstant.SESSION_ACCESS_MENU);
            if (menu == null) {
                menu = "";
            }
            if (menu == null) {
                response.sendRedirect(request.getContextPath() + "/apps/welcome.do");
                return;
            }
            if (menu.indexOf(uri) > 0) {
                response.sendRedirect(request.getContextPath() + "/apps/welcome.do");
                return;
            }
        }
        
        chain.doFilter(req, res);
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
    }

}

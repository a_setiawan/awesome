package com.rintis.marketing.web.filter;


import org.apache.log4j.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import java.util.Iterator;

public class FacesExceptionHandler extends ExceptionHandlerWrapper {

    private ExceptionHandler wrapped;

    Logger log;
    FacesContext fc;

    public FacesExceptionHandler(ExceptionHandler exceptionHandler) {
        wrapped = exceptionHandler;
        log = Logger.getLogger(this.getClass().getName());
        fc = FacesContext.getCurrentInstance();
    }

    @Override
    public ExceptionHandler getWrapped() {
        return wrapped;
    }

    @Override
    public void handle() {

        Iterator<ExceptionQueuedEvent> i = super.getUnhandledExceptionQueuedEvents().iterator();

        while (i.hasNext()) {

            Throwable t = i.next().getContext().getException();

            if (t instanceof ViewExpiredException) {
                try {
                    // Handle the exception, for example:
                    log.error("ViewExpiredException occurred!");
                    //not working with c3faces
                    //fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "An exception occurred",
                    //        "ViewExpiredException happened!"));
                    //fc.getApplication().getNavigationHandler().handleNavigation(fc, null, "/expired");
                }
                finally {
                    i.remove();
                }
            }

            /*
            else if (t instanceof SomeOtherException) {
                // handle SomeOtherException
            }
            */

        }

        // let the parent handle the rest
        getWrapped().handle();
    }
}

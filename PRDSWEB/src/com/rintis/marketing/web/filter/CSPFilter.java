package com.rintis.marketing.web.filter;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CSPFilter implements Filter {
    private Logger log = Logger.getLogger(CSPFilter.class);

    public static final String CONTENT_SECURITY_POLICY_HEADER = "Content-Security-Policy";

    public static final String SANDBOX = "sandbox";
    /** The default policy for loading content such as JavaScript, Images, CSS, Font's, AJAX requests, Frames, HTML5 Media */
    public static final String DEFAULT_SRC = "default-src";
    /** Defines valid sources of images */
    public static final String IMG_SRC = "img-src";
    /** Defines valid sources of JavaScript  */
    public static final String SCRIPT_SRC = "script-src";
    /** Defines valid sources of stylesheets */
    public static final String STYLE_SRC = "style-src";
    /** Defines valid sources of fonts */
    public static final String FONT_SRC = "font-src";
    /** Applies to XMLHttpRequest (AJAX), WebSocket or EventSource */
    public static final String CONNECT_SRC = "connect-src";
    /** Defines valid sources of plugins, eg <object>, <embed> or <applet>.  */
    public static final String OBJECT_SRC = "object-src";
    /** Defines valid sources of audio and video, eg HTML5 <audio>, <video> elements */
    public static final String MEDIA_SRC = "media-src";
    /** Defines valid sources for loading frames */
    public static final String FRAME_SRC = "frame-src";

    public static final String KEYWORD_NONE = "'none'";
    public static final String KEYWORD_SELF = "'self'";



    public void init(FilterConfig filterConfig) {
    }


    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String contentSecurityPolicyHeaderName = CONTENT_SECURITY_POLICY_HEADER;
        String contentSecurityPolicy = getContentSecurityPolicy();

        //log.info("Adding Header " + contentSecurityPolicyHeaderName + " = " + contentSecurityPolicy);
        httpResponse.addHeader(contentSecurityPolicyHeaderName, contentSecurityPolicy);

        chain.doFilter(request, response);
    }

    private String getContentSecurityPolicy() {
        StringBuilder contentSecurityPolicy = new StringBuilder("");
        /*contentSecurityPolicy.append("default-src").append(" ").append("none").append("; ");
        contentSecurityPolicy.append(FRAME_SRC).append(" ").append(KEYWORD_NONE).append("; ");
        contentSecurityPolicy.append(MEDIA_SRC).append(" ").append(KEYWORD_SELF).append("; ");
        contentSecurityPolicy.append(OBJECT_SRC).append(" ").append(KEYWORD_SELF).append("; ");
        contentSecurityPolicy.append(CONNECT_SRC).append(" ").append(KEYWORD_SELF).append("; ");
        contentSecurityPolicy.append(SCRIPT_SRC).append(" ").append(KEYWORD_SELF).append("; ");
        contentSecurityPolicy.append(STYLE_SRC).append(" ").append(KEYWORD_SELF).append("; ");
        contentSecurityPolicy.append(IMG_SRC).append(" ").append(KEYWORD_SELF).append("; ");
        contentSecurityPolicy.append("frame-ancestors").append(" ").append("'none'");*/
        //contentSecurityPolicy.append("default-src *; object-src 'none'");

        contentSecurityPolicy.append(FRAME_SRC).append(" ").append(KEYWORD_NONE).append("; ");
        contentSecurityPolicy.append(MEDIA_SRC).append(" ").append(KEYWORD_SELF).append("; ");
        contentSecurityPolicy.append(OBJECT_SRC).append(" ").append(KEYWORD_NONE).append("; ");
        //contentSecurityPolicy.append("form-action").append(" ").append("'self' 'unsafe-inline' 'unsafe-eval'").append("; ");
        contentSecurityPolicy.append("base-uri").append(" ").append("'self'").append("; ");
        //contentSecurityPolicy.append(SCRIPT_SRC).append(" ").append("'self' 'unsafe-inline'").append("; ");
        contentSecurityPolicy.append("frame-ancestors").append(" ").append("'none'").append("; ");

        //addDirectiveToContentSecurityPolicy(contentSecurityPolicy, IMG_SRC, imgSrc);
        //addDirectiveToContentSecurityPolicy(contentSecurityPolicy, SCRIPT_SRC, scriptSrc);
        //addDirectiveToContentSecurityPolicy(contentSecurityPolicy, STYLE_SRC, styleSrc);
        //addDirectiveToContentSecurityPolicy(contentSecurityPolicy, FONT_SRC, fontSrc);
        //addDirectiveToContentSecurityPolicy(contentSecurityPolicy, CONNECT_SRC, connectSrc);
        //addDirectiveToContentSecurityPolicy(contentSecurityPolicy, OBJECT_SRC, objectSrc);
        //addDirectiveToContentSecurityPolicy(contentSecurityPolicy, MEDIA_SRC, mediaSrc);
        //addDirectiveToContentSecurityPolicy(contentSecurityPolicy, FRAME_SRC, frameSrc);
        //addDirectiveToContentSecurityPolicy(contentSecurityPolicy, REPORT_URI, reportUri);
        //addSandoxDirectiveToContentSecurityPolicy(contentSecurityPolicy, sandbox);

        return contentSecurityPolicy.toString();
    }



    public void destroy() {
    }
}


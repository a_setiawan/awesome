package com.rintis.marketing.web.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ReferrerFeatureFilter implements Filter {

    private Logger log = Logger.getLogger(ReferrerFeatureFilter.class);

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse httpServletResponse = ((HttpServletResponse) res);
        httpServletResponse.addHeader("Referrer-Policy", "same-origin");
        httpServletResponse.addHeader("Feature-Policy", "camera none;geolocation none;");
        //httpServletResponse.addHeader("X-Frame-Options", "DENY");
        httpServletResponse.addHeader("Strict-Transport-Security", "max-age=63072000; includeSubDomains; preload");
        chain.doFilter(req, res);
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
    }

}


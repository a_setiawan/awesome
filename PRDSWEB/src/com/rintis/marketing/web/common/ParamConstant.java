package com.rintis.marketing.web.common;

public class ParamConstant {
    public static final String GLOW_MESSAGE = "glowmsg";

    public static final String PARAM_ID = "id";
    public static final String PARAM_STAGE = "stage";
    public static final String PARAM_STEP = "step";
    public static final String PARAM_USERID = "uid";
    public static final String PARAM_BANKID = "bid";
    public static final String PARAM_MENU_ROLE_TYPE_ID = "mrtid";
    public static final String PARAM_PAN = "wlPan";
    public static final String PARAM_PENGIRIM = "wlPengirim";
    
}

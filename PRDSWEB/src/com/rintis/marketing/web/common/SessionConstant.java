package com.rintis.marketing.web.common;

public class SessionConstant {
    
    public static final String SESSION_USERINFO = "sesUserInfo";
    public static final String SESSION_USERINFOBEAN = "sesUserInfoBean";
    public static final String SESSION_ACCESS_MENU = "sesAccessMenu";
    public static final String SESSION_FORCE_CHANGE_PASS = "sesforcechangepass";

}

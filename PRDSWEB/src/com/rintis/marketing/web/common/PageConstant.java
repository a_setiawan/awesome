package com.rintis.marketing.web.common;

public class PageConstant {

    public static final String PAGE_INDEX = "/index.jsp";
    public static final String PAGE_LOGIN = "/login";
    public static final String PAGE_WELCOME = "/apps/welcome";
    public static final String PAGE_WELCOME_BOD = "/apps/welcome_bod";
    public static final String PAGE_FORCE_CHANGE_PASS = "/apps/forcechangepass";
    public static final String PAGE_LIST_USER = "/apps/user/listUser";
    public static final String PAGE_MENU_ROLE_TYPE_LIST_MENU_ROLE_TYPE = "/apps/menu/listMenuRoleType";
    public static final String PAGE_WHITELIST_PAN_WHITELIST_PAN = "/apps/pan/listWhitelistPAN";
    public static final String PAGE_EMAIL_PAN_EMAIL_PAN = "/apps/pan/listPanAnomalyEmail";
    public static final String PAGE_PARAM_PAN_PARAM_PAN = "/apps/pan/listPanAnomalyAggregateParameter";
    public static final String PAGE_PARAM_MAX_AMOUNT_PARAM = "/apps/pan/listAnomalyMaxAmountParameter";
    public static final String PAGE_PARAM_PER_BANK_PARAM = "/apps/pan/listPanAnomalyAggregatePerBankParameter";
    public static final String PAGE_PARAM_BANK_BENE_PARAM = "/apps/pan/listPanAnomalyAggregateParameterBankBeneficiary";
    public static final String PAGE_MASTER_MCC = "/apps/mcc/listMCC";
    public static final String PAGE_MASTER_MCC_PARAMETER = "/apps/mcc/listMCCParameter";
    public static final String PAGE_PARAM_SEQUENTIAL = "/apps/sequential/listSequentialParameter";
    public static final String PAGE_LIST_REPORT_SETTING = "/apps/report/setting/listReportSetting";

    public static final String PAGE_LIST_ACCOUNT_NAME = "/apps/master/listAccountName";

    //public static final String PAGE_REPORT_A = "/apps/report/reporta";
    //public static final String PAGE_REPORT_A1 = "/apps/report/reporta1";
    //public static final String PAGE_REPORT_A11 = "/apps/report/reporta11";

    /*Add by aaw 20/05/21*/
    public static final String PAGE_WHITELIST_BENE_WHITELIST_BENE = "/apps/pan/listWhitelistBeneficiary";
    /*End add*/
    /*Add by aaw 20/05/21*/
    public static final String PAGE_WHITELIST_MPAN = "/apps/qr/listWhitelistMPan";
     /*End add*/

    public static final String PAGE_BLOCKED_ADMINISTRATION_DETAIL = "/apps/blocked/blockedAdministrationDetail";
    public static final String PAGE_BLOCKED_ADMINISTRATION_DETAIL_MANUAL = "/apps/blocked/blockedAdministrationDetailManual";
    public static final String PAGE_BLOCKED_ADMINISTRATION = "/apps/blocked/blockedAdministration";
    public static final String PAGE_UNBLOCKED_ADMINISTRATION_DETAIL = "/apps/blocked/unblockedAdministrationDetail";
    public static final String PAGE_UNBLOCKED_ADMINISTRATION = "/apps/blocked/unblockedAdministration";
    public static final String PAGE_BLOCKED_DOCUMENT_ADMINISTRATION = "/apps/blocked/listDocumentAdministration";
}

package com.rintis.marketing.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.web.schedule.JobRegisterTaskController;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;


@WebServlet(name="taskServlet", urlPatterns="/taskServlet", loadOnStartup=1)
public class TaskServlet extends HttpServlet {
    private Logger log = Logger.getLogger(TaskServlet.class);
    
    @Override
    public void init() throws ServletException {
        super.init();
        //log.info("init");
        try {
            ApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
            ModuleFactory moduleFactory = (ModuleFactory)appContext.getBean(SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY);

            JobRegisterTaskController.stopTask();
            JobRegisterTaskController.startTask(moduleFactory.getApplicationContext());

            //stop, start job
            //DownloadUploadTaskController.stopTask();
            //DownloadUploadTaskController.startTask(moduleFactory.getApplicationContext());
        } catch(Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //log.info("get");
        /*String cmd = (String) req.getParameter("id");
        
        if (cmd != null) {
            if (cmd.equalsIgnoreCase("1")) {
                try {
                    ScheduleTaskController.stopScheduler();
                    return;
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
        
        if (cmd != null) {
            if (cmd.equalsIgnoreCase("2")) {
                try {
                    ScheduleTaskController.initScheduler();
                    ScheduleTaskController.startScheduler();
                    
                    
                    ApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
                    
                    ScheduleTaskController.runJob(appContext, "job1", "group1", "trigger", "0/10 * * * * ?", DownloadSalesmanJob.class);
                    
                    return;
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
        
        try {
        //log.info(taskScheduler. );
        JobKey jobKeyA = new JobKey("jobA", "group1");
        JobDetail jobA = JobBuilder.newJob(DownloadJob.class)
        .withIdentity(jobKeyA).build();
        
        Trigger trigger1 = TriggerBuilder
                .newTrigger()
                .withIdentity("dummyTriggerName1", "group1")
                .withSchedule(
                    CronScheduleBuilder.cronSchedule("0/5 * * * * ?"))
                .build();
        
        //sch = new StdSchedulerFactory().getScheduler();
        
        //sch.start();
        ScheduleTaskController.initScheduler();
        ScheduleTaskController.startScheduler();        
        
        ScheduleTaskController.getScheduler().scheduleJob(jobA, trigger1);

        } catch(Exception e) {
            log.error(e.getMessage(), e);
        }*/
        
    }

}

package com.rintis.marketing.web.schedule;

import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.context.ApplicationContext;

public class ScheduleTaskController {
    private static Logger log = Logger.getLogger(ScheduleTaskController.class);
    private static Scheduler scheduler;
    
    public static final String SPRING_CONTEXT = "springContext";
    
    public static void initScheduler() {
        try {
            scheduler = new StdSchedulerFactory().getScheduler();
        } catch (SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public static void stopScheduler() {
        try {
            if (scheduler == null) {
                return;
            }
            scheduler.shutdown();
        } catch (SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public static void startScheduler() {
        try {
            scheduler.start();
        } catch (SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public static Scheduler getScheduler() {
        return scheduler;
    }
    
    public static void runJob(ApplicationContext appContext, String jobName, String jobGroup, String triggerName, String cron, Class<? extends Job> jobClass ) {
        try {
            JobKey jobKeyA = new JobKey(jobName, jobGroup);
            JobDetail jobA = JobBuilder.newJob(jobClass).withIdentity(jobKeyA).build();
            jobA.getJobDataMap().put(SPRING_CONTEXT, appContext);
            Trigger trigger1 = TriggerBuilder
                    .newTrigger()
                    .withIdentity(triggerName, jobGroup)
                    .withSchedule(CronScheduleBuilder.cronSchedule(cron))
                    .build();

            scheduler.scheduleJob(jobA, trigger1);
        } catch (SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }
    
}

package com.rintis.marketing.web.schedule;


import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

public class JobRegisterTaskController extends ScheduleTaskController {
    private static Logger log = Logger.getLogger(JobRegisterTaskController.class);

    public static void startTask(ApplicationContext appContext) {
        try {
            initScheduler();
            startScheduler();

            ModuleFactory moduleFactory = (ModuleFactory)appContext.getBean(SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY);
            String jobSchedule = "";

            /*jobSchedule = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_SCHEDULE_ALERT_ANOMALY_5MIN);
            if (jobSchedule.length() > 0) {
                runJob(appContext, "alertAnomaly5MinJob", "alertAnomaly5MinGroup",
                        "alertAnomaly5MinTrigger", jobSchedule, AlertAnomaly5MinJob.class);
            }*/

            /*jobSchedule = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_SCHEDULE_ALERT_ANOMALY_HOURLY);
            if (jobSchedule.length() > 0) {
                runJob(appContext, "alertAnomalyHourlyJob", "alertAnomalyHourlyGroup",
                        "alertAnomalyHourlyTrigger", jobSchedule, AlertAnomalyHourlyJob.class);
            }*/

            /*jobSchedule = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_SEND_EMAIL_INTERVAL);
            if (jobSchedule.length() > 0) {
                runJob(appContext, "sendEmailJob", "sendEmailGroup",
                        "sendEmailTrigger", jobSchedule, SendEmailJob.class);
            }*/

            /*jobSchedule = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_SCHEDULE_DISTINCT_ACQ_ISS_BNF);
            if (jobSchedule.length() > 0) {
                runJob(appContext, "distinctAcqIssBnfJob", "distinctAcqIssBnfGroup",
                        "distinctAcqIssBnfTrigger", jobSchedule, DistinctAcqIssBnfJob.class);
            }

            jobSchedule = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_SCHEDULE_CHECK_ACQ_ISS_BNF_TRX);
            if (jobSchedule.length() > 0) {
                runJob(appContext, "checkAcqIssBnfTrxJob", "checkAcqIssBnfTrxGroup",
                        "checkAcqIssBnfTrxTrigger", jobSchedule, CheckAcqIssBnfTrxJob.class);
            }*/

            /*jobSchedule = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_SCHEDULE_ETECT_ROUTING);
            if (jobSchedule.length() > 0) {
                runJob(appContext, "detectRoutingJob", "detectRoutingGroup",
                        "detectRoutingTrigger", jobSchedule, DetectRoutingJob.class);
            }*/

        } catch(Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public static void stopTask() {
        stopScheduler();
    }
}

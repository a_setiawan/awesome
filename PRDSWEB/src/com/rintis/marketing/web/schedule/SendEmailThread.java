package com.rintis.marketing.web.schedule;


import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import jodd.mail.Email;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import jodd.mail.SmtpSslServer;
import org.apache.log4j.Logger;

import java.util.StringTokenizer;

public class SendEmailThread implements Runnable {
    private Logger log = Logger.getLogger(SendEmailThread.class);
    private ModuleFactory moduleFactory;
    private String emailto;
    private String emailSubject;
    private String emailMessage;
    private Integer emailQueueId;

    public SendEmailThread(int id, String emailto, String emailSubject, String emailMessage, ModuleFactory moduleFactory) {
        this.emailQueueId = id;
        this.emailto = emailto;
        this.emailSubject = emailSubject;
        this.emailMessage = emailMessage;
        this.moduleFactory = moduleFactory;
    }

    @Override
    public void run() {
        sendEmail();
    }


    private void sendEmail() {
        log.info("id = " + emailQueueId + ", " + "start thread " + emailto + " ");
        if (emailto == null) return;
        if (emailto.length() == 0) return;

        try {

            String fromEmail = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_DEFAULT_FROM_EMAIL);
            String smtpUser = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_SMTP_USER);
            String smtp = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_SMTP);
            String smtpPassword = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_SMTP_PASSWORD);
            String smtpSsl = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_SMTP_SSL);


            Email email = Email.create()
                    .from(fromEmail)
                    .subject(emailSubject)
                    .addText(emailMessage);

            if (emailto.indexOf(",") > 0) {
                StringTokenizer st = new StringTokenizer(emailto, ",");
                while (st.hasMoreTokens()) {
                    String e = st.nextToken();
                    email.to(e);
                }
            } else {
                email.to(emailto);
            }

            //EmailMessage htmlMessage = new EmailMessage(emailMessage, MimeTypes.MIME_TEXT_PLAIN);

            SmtpServer smtpServer = null;
            if (smtpSsl.equalsIgnoreCase("1")) {
                smtpServer = SmtpSslServer.create(smtp).authenticateWith(smtpUser, smtpPassword);
            } else {
                smtpServer = SmtpServer.create(smtp);//.authenticateWith(smtpUser, smtpPassword);
            }

            SendMailSession session = smtpServer.createSession();
            session.open();
            session.sendMail(email);
            session.close();

            moduleFactory.getCommonService().deleteEmailQueue(emailQueueId);

            log.info("id = " + emailQueueId + ", " + "email send " + emailto);
        } catch (Exception e) {
            log.error("id = " + emailQueueId + ", " + e.getMessage(), e);
        } finally {
        }
        log.info("id = " + emailQueueId + ", " + "end thread " + emailto);
    }

}


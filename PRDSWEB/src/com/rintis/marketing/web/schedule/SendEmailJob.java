package com.rintis.marketing.web.schedule;

import com.rintis.marketing.beans.entity.EmailQueue;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@DisallowConcurrentExecution
public class SendEmailJob implements Job {
    private Logger log = Logger.getLogger(SendEmailJob.class);

    @Override
    public void execute(JobExecutionContext ctx) throws JobExecutionException {
        try {
            log.info("send email job");

            ApplicationContext appContext = (ApplicationContext) ctx.getJobDetail().getJobDataMap().get(ScheduleTaskController.SPRING_CONTEXT);
            ModuleFactory moduleFactory = (ModuleFactory) appContext.getBean(SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY);

            List<EmailQueue> list = moduleFactory.getCommonService().listEmailQueue();
            ExecutorService executor = Executors.newFixedThreadPool(5);
            for(EmailQueue cmsEmailQueue : list) {
                SendEmailThread sendEmailThread = new SendEmailThread(
                        cmsEmailQueue.getId(),
                        cmsEmailQueue.getEmailTo(),
                        cmsEmailQueue.getEmailSubject(),
                        cmsEmailQueue.getEmailMessage(),
                        moduleFactory
                );
                executor.execute(sendEmailThread);
                Thread.sleep(20);
                break;
            }
            executor.shutdown();
            while (!executor.isTerminated()) {
                Thread.sleep(40);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}

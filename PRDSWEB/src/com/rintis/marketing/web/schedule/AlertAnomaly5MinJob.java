package com.rintis.marketing.web.schedule;

import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

@DisallowConcurrentExecution
public class AlertAnomaly5MinJob implements Job {
    private Logger log = Logger.getLogger(AlertAnomaly5MinJob.class);

    @Override
    public void execute(JobExecutionContext ctx) throws JobExecutionException {
        try {
            log.info("alert anomaly 5 min job");

            ApplicationContext appContext = (ApplicationContext) ctx.getJobDetail().getJobDataMap().get(ScheduleTaskController.SPRING_CONTEXT);
            ModuleFactory moduleFactory = (ModuleFactory) appContext.getBean(SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY);

            //moduleFactory.getBiService().checkAlertAnomaly5MinAlertThreshold();


        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


}

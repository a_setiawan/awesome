package com.rintis.marketing.web.jsf.main.sequential;

import com.rintis.marketing.beans.bean.sequential.SequentialParamBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by aabraham on 22/04/2021.
 */

@ManagedBean
@ViewScoped
public class EditSequentialParameterManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<SequentialParamBean> listSequentialParam;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        String trx = (String)getRequestParameterMap().get("wlTrx");

        if(trx.equals("POS/Debit")){
            trx = "01";
        }else if(trx.equals("Cash Withdrawal")){
            trx = "10";
        }else if(trx.equals("Payment")){
            trx = "17";
        }else if(trx.equals("Transfer Debet/Kredit")){
            trx = "40";
        }else if(trx.equals("Top-up E-Money")){
            trx = "85";
        }else if(trx.equals("QR")){
            trx = "29";
        }

        if (trx != null) {
            try {
                listSequentialParam = moduleFactory.getSequentialService().getParam(trx);
                if (!isPostback()) {
                    createTlog(moduleFactory);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public String editAction() {
        try {
            String trx = listSequentialParam.get(0).getTrx();

            BigDecimal mintrxapp1d = listSequentialParam.get(0).getMintrxapp1d();
            BigDecimal minamtapp1d = listSequentialParam.get(0).getMinamtapp1d();
            BigDecimal mintrxtot1d = listSequentialParam.get(0).getMintrxtot1d();
            BigDecimal minamttot1d = listSequentialParam.get(0).getMinamttot1d();

            moduleFactory.getSequentialService().updateParam(trx, mintrxapp1d, minamtapp1d, mintrxtot1d, minamttot1d);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_PARAM_SEQUENTIAL + "?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<SequentialParamBean> getListSequentialParam() {
        return listSequentialParam;
    }

    public void setListSequentialParam(List<SequentialParamBean> listSequentialParam) {
        this.listSequentialParam = listSequentialParam;
    }
}

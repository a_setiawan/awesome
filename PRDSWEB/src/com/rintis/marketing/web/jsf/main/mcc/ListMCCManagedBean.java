package com.rintis.marketing.web.jsf.main.mcc;

import com.rintis.marketing.beans.entity.MCCList;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.util.List;

/**
 * Created by aabraham on 29/03/2021.
 */
@ManagedBean
@ViewScoped
public class ListMCCManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;

    private List<MCCList> listMCC;
    private String selectedDeleteItem;
    private String selectedMCC;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listMCC = moduleFactory.getMasterService().listMcc("-1");

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    public void deleteMccActionListener(ActionEvent event) {
        try {
            moduleFactory.getMasterService().deleteMcc(selectedDeleteItem);
            listMCC = moduleFactory.getMasterService().listMcc("-1");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String searchAction() {
        try {
            listMCC = moduleFactory.getMasterService().listMcc(selectedMCC);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<MCCList> getListMCC() {
        return listMCC;
    }

    public void setListMCC(List<MCCList> listMCC) {
        this.listMCC = listMCC;
    }

    public String getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(String selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }

    public String getSelectedMCC() {
        return selectedMCC;
    }

    public void setSelectedMCC(String selectedMCC) {
        this.selectedMCC = selectedMCC;
    }
}

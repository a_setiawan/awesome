package com.rintis.marketing.web.jsf.main.sequential;

import com.rintis.marketing.beans.bean.bi.PanAnomalyParamBean;
import com.rintis.marketing.beans.bean.sequential.SequentialParamBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * Created by aabraham on 20/04/2021.
 */
@ManagedBean
@ViewScoped
public class ListSequentialParameterManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;

    private List<SequentialParamBean> listSequentialParam;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listSequentialParam = moduleFactory.getSequentialService().listParam();
            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public List<SequentialParamBean> getListSequentialParam() {
        return listSequentialParam;
    }

    public void setListSequentialParam(List<SequentialParamBean> listSequentialParam) {
        this.listSequentialParam = listSequentialParam;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }
}

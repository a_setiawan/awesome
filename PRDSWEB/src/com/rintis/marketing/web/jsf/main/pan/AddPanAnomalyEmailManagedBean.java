package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.PanAnomalyEmailBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 * Created by aabraham on 01/03/2019.
 */
@ManagedBean
@ViewScoped
public class AddPanAnomalyEmailManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private PanAnomalyEmailBean panAnomalyEmailBean;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        panAnomalyEmailBean = new PanAnomalyEmailBean();
    }

    public String saveAction() {
        try {
            if (panAnomalyEmailBean.getUserEmail().length() == 0) {
                glowMessageError("Enter User Email", "");
                return "";
            }

            if (panAnomalyEmailBean.getGender().length() == 0) {
                glowMessageError("Enter Gender", "");
                return "";
            }

            if (panAnomalyEmailBean.getUserName().length() == 0) {
                glowMessageError("Enter Username", "");
                return "";
            }

            boolean chk = moduleFactory.getPanService().checkEmailUsed(panAnomalyEmailBean.getUserEmail());
            if (chk) {
                glowMessageError("User Email already exist", "");
                return "";
            }

            //start modify by erichie
            String actAlert1H = panAnomalyEmailBean.getAlert1H().toString();
            String actAlert3H = panAnomalyEmailBean.getAlert3H().toString();
            String actAlert6H = panAnomalyEmailBean.getAlert6H().toString();
            String actAlert1D = panAnomalyEmailBean.getAlert1D().toString();
            String actAlert3D = panAnomalyEmailBean.getAlert3D().toString();
            String actAlertMaxAmt = panAnomalyEmailBean.getAlertMaxAmt().toString();
            String actAlertMccVelocity = panAnomalyEmailBean.getAlertMccVelocity().toString();
            String actAlertSeqVelocity = panAnomalyEmailBean.getAlertSeqVelocity().toString();
            /*Add by aaw 02/07/2021*/
            String actAlertMpanAcq = panAnomalyEmailBean.getAlertMpanAcq().toString();
            /*End add*/

            moduleFactory.getPanService().saveEmail(panAnomalyEmailBean.getUserEmail(), panAnomalyEmailBean.getGender(), panAnomalyEmailBean.getUserName(), actAlert1H, actAlert3H, actAlert6H, actAlert1D, actAlert3D, actAlertMaxAmt, actAlertMccVelocity, actAlertSeqVelocity,
                actAlertMpanAcq);
            //end modify by erichie

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_EMAIL_PAN_EMAIL_PAN + "?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public PanAnomalyEmailBean getPanAnomalyEmail() {
        return panAnomalyEmailBean;
    }

    public void setPanAnomalyEmail(PanAnomalyEmailBean panAnomalyEmailBean) {
        this.panAnomalyEmailBean = panAnomalyEmailBean;
    }
}

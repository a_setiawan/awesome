package com.rintis.marketing.web.jsf.main.common;



import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.beans.bean.bi.DashboardRoleBean;
import com.rintis.marketing.beans.entity.DashboardRole;
import com.rintis.marketing.beans.entity.Menu;
import com.rintis.marketing.beans.entity.MktUserDashboard;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import com.rintis.marketing.core.constant.DataConstant;
import org.primefaces.model.menu.*;
import org.springframework.context.support.AbstractApplicationContext;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@SessionScoped
public class PrivateHeaderManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{"+ SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY+"}")
    private ModuleFactory moduleFactory;
    
    private String name;
    private String branchId;
    private String branchName;
    private MenuModel menuModel;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        UserInfoBean userInfoBean = getUserInfoBean();
        //log.info(userInfoBean.getUser());
        if (userInfoBean.getUserLogin() != null) {
            name = userInfoBean.getUserLogin().getUserName();
        } else if (userInfoBean.getUserLoginBank() != null) {
            name = userInfoBean.getUserLoginBank().getUserName();
        }
        buildMenubar(userInfoBean.getUserId(), userInfoBean);
    }

    private void buildMenubar(String userId, UserInfoBean userInfoBean) {
        //log.info("build menu bar");
        //log.info(userId);
        try {
            menuModel = new DefaultMenuModel();
            List<Menu> listMenu = new ArrayList<>();
            if (userInfoBean.getUserLogin() != null) {
                listMenu = moduleFactory.getMenuService().listMenuByRole(userId, null);
            } else if (userInfoBean.getUserLoginBank() != null) {
                listMenu = moduleFactory.getMenuService().listMenuByRoleBank(userId, null);
            }
            //log.info(listMenu.size());
            for(int i=0; i<listMenu.size(); i++) {
                Menu menu = listMenu.get(i);
                DefaultSubMenu submenu = new DefaultSubMenu();
                //submenu.setTransient(true);
                submenu.setLabel(menu.getMenuName());
                //if (menu.getMenuIcon().length() == 0) {
                if (menu.getMenuIcon() == null) {
                    submenu.setIcon(null);
                } else {
                    submenu.setIcon(menu.getMenuIcon());
                }
                menuModel.addElement(submenu);
                buildMenuItem(userId, submenu, menu.getMenuId(), userInfoBean);
            }


            List<DashboardRoleBean> listRf = null;
            if (getUserInfoBean().getUserLogin() != null) {
                listRf = moduleFactory.getBiService().listDashboardRoleNew(getUserInfoBean().getUserLogin().getRoleTypeId());
            } else if (getUserInfoBean().getUserLoginBank() != null) {
                listRf = moduleFactory.getBiService().listDashboardRoleNew(getUserInfoBean().getUserLoginBank().getRoleTypeId());
            }

            if (listRf.size() > 0) {
                //add menu user dashboard
                DefaultSubMenu submenu = new DefaultSubMenu();
                submenu.setLabel("User Dashboard");
                submenu.setIcon(null);
                submenu.setStyleClass("menudashboard");
                menuModel.addElement(submenu);

            /*List<MktUserDashboard> listRf = moduleFactory.getBiService().listUserDashboard(userId);
            for(MktUserDashboard rf : listRf) {
                //Menu menu = moduleFactory.getMenuService().getMenu(rf.getReportFavoritePk().getMenuId());
                DefaultMenuItem menuItem = new DefaultMenuItem();
                //menuItem.setTransient(true);
                menuItem.setValue(rf.getMenuName());
                menuItem.setIcon(null);
                menuItem.setUrl("/apps/dashboard/userDashboard.do?faces-redirect=true&id=" + rf.getId().toPlainString());
                menuItem.setTitle(null);

                submenu.getElements().add(menuItem);
            }*/

                for (DashboardRoleBean rf : listRf) {
                    //190524 Remarked and Modify by AAH
                    //MktUserDashboard userDashboard = moduleFactory.getBiService().getUserDashboard(rf.getDashboardRolePk().getDashboardId());
                    MktUserDashboard userDashboard = moduleFactory.getBiService().getUserDashboard(rf.getDashboardid());
                    if (userDashboard == null) continue;

                    //encrypt param
                    String urlParam = "id=" + userDashboard.getId().toPlainString();
                    urlParam = HttpUtils.decodeUrlParam(urlParam, getUserInfoBean().getUserId(), getHttpSession().getId());
                    urlParam = "xp=" + urlParam;

                    //Menu menu = moduleFactory.getMenuService().getMenu(rf.getReportFavoritePk().getMenuId());
                    DefaultMenuItem menuItem = new DefaultMenuItem();
                    //menuItem.setTransient(true);
                    menuItem.setValue(userDashboard.getMenuName());
                    menuItem.setIcon(null);
                    menuItem.setUrl("/apps/dashboard/userDashboard.do?faces-redirect=true&" + urlParam);
                    menuItem.setTitle(null);

                    submenu.getElements().add(menuItem);
                }

            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    private void buildMenuItem(String userId, Submenu submenu, String parentId, UserInfoBean userInfoBean) {
        try {
            List<Menu> listMenu = new ArrayList<>();
            if (userInfoBean.getUserLogin() != null) {
                listMenu = moduleFactory.getMenuService().listMenuByRole(userId, parentId);
            } else if (userInfoBean.getUserLoginBank() != null) {
                listMenu = moduleFactory.getMenuService().listMenuByRoleBank(userId, parentId);
            }
            for(int i=0; i<listMenu.size(); i++) {
                Menu menu = listMenu.get(i);
                //log.info(parentId + " - " + menu.getMenuName());
                if (menu.getMenuType() == DataConstant.MENU_TYPE_SUB) {
                    DefaultSubMenu newSubmenu = new DefaultSubMenu();
                    //newSubmenu.setTransient(true);
                    newSubmenu.setLabel(menu.getMenuName());
                    //if (menu.getMenuIcon().length() == 0) {
                    if (menu.getMenuIcon() == null) {
                        newSubmenu.setIcon(null);
                    } else {
                        newSubmenu.setIcon(menu.getMenuIcon());
                    }
                    submenu.getElements().add(newSubmenu);
                    buildMenuItem(userId, newSubmenu, menu.getMenuId(), userInfoBean);
                } else {
                    DefaultMenuItem menuItem = new DefaultMenuItem();
                    //menuItem.setTransient(true);
                    menuItem.setValue(menu.getMenuName());
                    //if (menu.getMenuIcon().length() == 0) {
                    if (menu.getMenuIcon() == null) {
                        menuItem.setIcon(null);
                    } else {
                        menuItem.setIcon(menu.getMenuIcon());
                    }
                    //if (menu.getMenuUrl().length() == 0) {
                    if (menu.getMenuUrl() == null) {
                        menuItem.setUrl(null);
                    } else {
                        menuItem.setUrl(menu.getMenuUrl());
                    }
                    //if (menu.getMenuTitle().length() == 0) {
                    if (menu.getMenuTitle() == null) {
                        menuItem.setTitle(null);
                    } else {
                        menuItem.setTitle(menu.getMenuTitle());
                    }
                    submenu.getElements().add(menuItem);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            if (e.getMessage().indexOf("Closed Connection") > -1) {
                ((AbstractApplicationContext)moduleFactory.getApplicationContext()).refresh();
            }
        }
    }

    public String getMenuHtml() {
        StringBuilder sb = new StringBuilder("\n");

        try {
            String userId = getUserInfoBean().getUserId();

            List<Menu> listMenu = moduleFactory.getMenuService().listMenuByRole(userId, null);
            //log.info(listMenu.size());
            for(int i=0; i<listMenu.size(); i++) {
                Menu menu = listMenu.get(i);

                sb.append("<li class=\"dropdown\"> \n");
                sb.append("<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"> \n");
                sb.append(menu.getMenuName());
                sb.append("<b class=\"caret\"></b></a> \n");

                buildSubMenuHtml(userId, sb, menu.getMenuId());

                sb.append("</li> \n");

                sb.append("\n");
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        //log.info(sb.toString());
        return sb.toString();
    }

    private String buildSubMenuHtml(String userId, StringBuilder sb, String parentId) {
        try {
            String contextPath = getFacesContext().getExternalContext().getRequestContextPath();
            List<Menu> listMenu = moduleFactory.getMenuService().listMenuByRole(userId, parentId);
            sb.append("<ul class=\"dropdown-menu\" > \n");
            for(int i=0; i<listMenu.size(); i++) {
                Menu menu = listMenu.get(i);
                if (menu.getMenuType() == DataConstant.MENU_TYPE_SUB) {
                    sb.append("<li class=\"dropdown dropdown-submenu\"> \n");
                    sb.append("<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">" + menu.getMenuName() + "</a> \n");
                    buildSubMenuHtml(userId, sb, menu.getMenuId());
                    sb.append("</li> \n");
                } else {
                    sb.append("<li><a href=\"" + contextPath + "/" + menu.getMenuUrl() + "\"> \n");
                    sb.append(menu.getMenuName());
                    sb.append("</a></li> \n");
                }
            }
            sb.append("</ul>\n");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MenuModel getMenuModel() {
        return menuModel;
    }

    public void setMenuModel(MenuModel menuModel) {
        this.menuModel = menuModel;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

}

package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.PanAnomalyParamBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by aabraham on 04/03/2019.
 */
@ManagedBean
@ViewScoped
public class EditAnomalyMaxAmountParameterManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<PanAnomalyParamBean> listMaxAmtAnomalyParam;
    private String trx_id;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        String trx = (String)getRequestParameterMap().get("wlTrx");

//        System.out.println("trx_id nama : " + trx);

        if(trx.equals("POS/Debit")){

            trx = "01";

        }else if(trx.equals("Cash Withdrawal")){

            trx = "10";

        }else if(trx.equals("Payment")){

            trx = "17";

        }else if(trx.equals("Transfer Debet/Kredit")){

            trx = "40";

        }else if(trx.equals("Top-up E-Money")){

            trx = "85";

        }else if(trx.equals("QR")){

            trx = "29";

        }

//        System.out.println("trx_id angka : " + trx);

        if (trx != null) {
            trx_id = trx;
            try {
                listMaxAmtAnomalyParam = moduleFactory.getPanService().getParamMaxAmount(trx);
                if (!isPostback()) {
                    createTlog(moduleFactory);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    //start modify by erichie
    public String editAction() {
        try {
            //String trx = listMaxAmtAnomalyParam.get(0).getTrx();
            String trx = trx_id;
            BigDecimal maxamt1h = listMaxAmtAnomalyParam.get(0).getMaxAmt1h();

            moduleFactory.getPanService().updateMaxAmtParam(trx, maxamt1h);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_PARAM_MAX_AMOUNT_PARAM + "?faces-redirect=true";
    }
    //end modify by erichie

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<PanAnomalyParamBean> getPanAnomalyParam() {
        return listMaxAmtAnomalyParam;
    }

    public void setPanAnomalyParam(List<PanAnomalyParamBean> listMaxAmtAnomalyParam) {
        this.listMaxAmtAnomalyParam = listMaxAmtAnomalyParam;
    }
}

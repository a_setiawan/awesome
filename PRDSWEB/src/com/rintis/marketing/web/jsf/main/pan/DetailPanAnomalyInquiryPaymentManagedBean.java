package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.DetailPanAnomalyBean;
import com.rintis.marketing.beans.bean.bi.DetailPanAnomalyPaymentBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by aabraham on 04/08/2021.
 */
@ManagedBean
@ViewScoped
public class DetailPanAnomalyInquiryPaymentManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<DetailPanAnomalyPaymentBean> list;
    private String pPeriod;
    private String pTimeFr;
    private String pTimeTo;
    private String pPan;
    private String pPengirim;
    private String pIssuerBank;
    private String pTranType;
    private String pTranTypeId;
    private String pGroupId;
    private String pStatus;
    private String totalAmt;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            //190926 Modify by AAH encode param
            pPeriod = paramEncode(getRequestParameterMap().get("pPeriod").toString());
            pTimeFr = paramEncode(getRequestParameterMap().get("pTimeFr").toString().replace(":", ""));
            pTimeTo = paramEncode(getRequestParameterMap().get("pTimeTo").toString().replace(":", ""));
            pPan = paramEncode(getRequestParameterMap().get("pPan").toString());
            pPengirim = paramEncode(getRequestParameterMap().get("pPengirim").toString());
            pIssuerBank = paramEncode(getRequestParameterMap().get("pIssuerBank").toString());
            pTranType = paramEncode(getRequestParameterMap().get("pTranType").toString());
            pStatus = paramEncode(getRequestParameterMap().get("pStatus").toString());
            pGroupId = moduleFactory.getPanService().getGroupIdFromTrxName(pTranType);
            pTranTypeId = moduleFactory.getPanService().getTrxIdFromTrxName(pTranType);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            System.out.println("PAN Anomaly Inquiry Detail - " + getUserInfoBean().getUserId() + " - PGW - " +pPeriod + " - " + pTimeFr + " - " + pTimeTo + " - " + pPan + " - " + pTranTypeId);
            list = moduleFactory.getPanService().listDetailPgw(pPeriod, pTimeFr, pTimeTo, pPan, pTranTypeId, pPengirim);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String paramEncode(String data){
        //encrypt param
        if(!data.equals(null)){
            data = HttpUtils.encodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }

    public String getValueAmtTotal() {
        Double totAmt = 0.00;

        if(list != null){
            for(DetailPanAnomalyPaymentBean l : list) {
                totAmt += Double.parseDouble(l.getAmount().toString());
            }
        }

        DecimalFormat formatter = new DecimalFormat("###,###,###.00");
        totalAmt = formatter.format(totAmt);

        return totalAmt;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<DetailPanAnomalyPaymentBean> getList() {
        return list;
    }

    public void setList(List<DetailPanAnomalyPaymentBean> list) {
        this.list = list;
    }

    public String getpPeriod() {
        return pPeriod;
    }

    public void setpPeriod(String pPeriod) {
        this.pPeriod = pPeriod;
    }

    public String getpTimeFr() {
        return pTimeFr;
    }

    public void setpTimeFr(String pTimeFr) {
        this.pTimeFr = pTimeFr;
    }

    public String getpTimeTo() {
        return pTimeTo;
    }

    public void setpTimeTo(String pTimeTo) {
        this.pTimeTo = pTimeTo;
    }

    public String getpPan() {
        return pPan;
    }

    public void setpPan(String pPan) {
        this.pPan = pPan;
    }

    public String getpPengirim() {
        return pPengirim;
    }

    public void setpPengirim(String pPengirim) {
        this.pPengirim = pPengirim;
    }

    public String getpIssuerBank() {
        return pIssuerBank;
    }

    public void setpIssuerBank(String pIssuerBank) {
        this.pIssuerBank = pIssuerBank;
    }

    public String getpTranType() {
        return pTranType;
    }

    public void setpTranType(String pTranType) {
        this.pTranType = pTranType;
    }

    public String getpTranTypeId() {
        return pTranTypeId;
    }

    public void setpTranTypeId(String pTranTypeId) {
        this.pTranTypeId = pTranTypeId;
    }

    public String getpGroupId() {
        return pGroupId;
    }

    public void setpGroupId(String pGroupId) {
        this.pGroupId = pGroupId;
    }

    public String getpStatus() {
        return pStatus;
    }

    public void setpStatus(String pStatus) {
        this.pStatus = pStatus;
    }
}

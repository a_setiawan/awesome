package com.rintis.marketing.web.jsf.main.biller;

import com.rintis.marketing.beans.entity.BillerAnomalyParam;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 * Created by aaw 20211115
 */

@ManagedBean
@ViewScoped
public class EditBillerAnomalyParameterManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private BillerAnomalyParam billerAnomalyParam;
    private String trxName = "";
    private String trxId = "";

    @Override
    @PostConstruct
    protected void initManagedBean() {
        trxId = paramDecodeUrl((String)getRequestParameterMap().get("pTrxId"));

        if (!"".equals(trxId)) {
            try {
                billerAnomalyParam = moduleFactory.getBillerService().fetchBillerParamByTrxId(trxId);
                if (!isPostback()) {
                    createTlog(moduleFactory);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public String editAction() {
        try {

            moduleFactory.getBillerService().updateBillerAnomalyParam(billerAnomalyParam);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "/apps/biller/billerAnomalyParameter?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public BillerAnomalyParam getBillerAnomalyParam() {
        return billerAnomalyParam;
    }

    public void setBillerAnomalyParam(BillerAnomalyParam billerAnomalyParam) {
        this.billerAnomalyParam = billerAnomalyParam;
    }

    public String getTrxName() {
        return trxName;
    }

    public void setTrxName(String trxName) {
        this.trxName = trxName;
    }
}

package com.rintis.marketing.web.jsf.main.alert;

import com.rintis.marketing.beans.bean.bi.BankAlertThresholdBean;
import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.BankAlertThreshold;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.MathUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import jodd.util.MathUtil;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

@ManagedBean
@ViewScoped
public class EditBankAlertThresholdManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<EsqTrxIdBean> listTrxId;
    private List<BankAlertThresholdBean> list;
    private String selectedTrxId;
    private String selectedTransactionName;
    private boolean activeCheck;
    private String selectedOrder;


    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0503";
            pageTitle = getMenuName(moduleFactory, menuId);

            selectedOrder = "NAME";
            selectedTrxId = "";
            listTrxId = moduleFactory.getMasterService().listEsqTrxId();

            activeCheck = false;

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            if (selectedTrxId.length() == 0) {
                glowMessageError("Select Transaction", "");
                return "";
            }

            EsqTrxIdBean trx = moduleFactory.getMasterService().getEsqTrxId(selectedTrxId);
            selectedTransactionName = trx.getTrxname();

            list = moduleFactory.getBiService().listBankAlertThreshold(selectedTrxId, selectedTransactionName, selectedOrder);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String saveAction() {
        try {
            if (list.size() == 0) {
                glowMessageError("No Bank", "");
                return "";
            }
            for(BankAlertThresholdBean ba : list) {
                if (!MathUtils.validatePercentValue(ba.getThreshold5minwdwd()) ||
                        !MathUtils.validatePercentValue(ba.getThreshold5minwdhd()) //||
                        //!MathUtils.validatePercentValue(ba.getThresholdhourwdwd()) ||
                        //!MathUtils.validatePercentValue(ba.getThresholdhourwdhd())
                        ) {
                    glowMessageError("Percent value must be between 0 - 100", "");
                    return "";
                }
            }

            moduleFactory.getBiService().updateBankAlertThreshold(selectedTrxId, list);

            glowMessageInfo("Update Success", "");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            glowMessageError("Update failed", "");
        }
        return "";
    }

    public void checkboxActiveChangeListener() {
        for(BankAlertThresholdBean bat : list) {
            if (activeCheck) {
                bat.setActive(BigDecimal.ONE);
                bat.setActiveBool(true);
            } else {
                bat.setActive(BigDecimal.ZERO);
                bat.setActiveBool(false);
            }
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<EsqTrxIdBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<EsqTrxIdBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

    public List<BankAlertThresholdBean> getList() {
        return list;
    }

    public void setList(List<BankAlertThresholdBean> list) {
        this.list = list;
    }

    public String getSelectedTrxId() {
        return selectedTrxId;
    }

    public void setSelectedTrxId(String selectedTrxId) {
        this.selectedTrxId = selectedTrxId;
    }

    public String getSelectedTransactionName() {
        return selectedTransactionName;
    }

    public void setSelectedTransactionName(String selectedTransactionName) {
        this.selectedTransactionName = selectedTransactionName;
    }

    public boolean isActiveCheck() {
        return activeCheck;
    }

    public void setActiveCheck(boolean activeCheck) {
        this.activeCheck = activeCheck;
    }

    public String getSelectedOrder() {
        return selectedOrder;
    }

    public void setSelectedOrder(String selectedOrder) {
        this.selectedOrder = selectedOrder;
    }
}

package com.rintis.marketing.web.jsf.main.groupbank;

import com.rintis.marketing.beans.bean.master.BankEmailBean;
import com.rintis.marketing.beans.entity.BankEmail;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
public class BankEmailManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<BankEmailBean> listBankEmail;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0404";
            pageTitle = getMenuName(moduleFactory, menuId);

            listBankEmail = moduleFactory.getMasterService().listBankEmailBean();

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String saveAction() {
        try {
            //validation email

            for(BankEmailBean beb : listBankEmail) {
                //if (StringUtils.checkNull(beb.getEmail()).trim().length() > 0) {
                    BankEmail be = moduleFactory.getMasterService().getBankEmail(beb.getBankid());
                    if (be != null) {
                        be.setEmail(StringUtils.checkNull(beb.getEmail()));
                        moduleFactory.getMasterService().updateBankEmail(be);
                    } else {
                        be = new BankEmail();
                        be.setBankId(beb.getBankid());
                        be.setEmail(StringUtils.checkNull(beb.getEmail()));
                        moduleFactory.getMasterService().addBankEmail(be);
                    }
                //}
            }

            glowMessageInfo("Update Success", "");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            glowMessageError("Update Fail", "");
        }
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<BankEmailBean> getListBankEmail() {
        return listBankEmail;
    }

    public void setListBankEmail(List<BankEmailBean> listBankEmail) {
        this.listBankEmail = listBankEmail;
    }


}

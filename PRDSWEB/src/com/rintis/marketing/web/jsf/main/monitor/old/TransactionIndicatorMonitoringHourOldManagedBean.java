package com.rintis.marketing.web.jsf.main.monitor.old;

import com.martinlinha.c3faces.model.C3DataSet;
import com.martinlinha.c3faces.model.C3DataSetString;
import com.martinlinha.c3faces.model.C3ViewDataSet;
import com.martinlinha.c3faces.model.C3ViewDataSetString;
import com.martinlinha.c3faces.script.property.*;
import com.rintis.marketing.beans.bean.bi.C3ChartBean;
import com.rintis.marketing.beans.bean.bi.EsqTrx5MinBean;
import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.monitor.TransactionIndicatorMonitoringBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.EisBank;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.constant.ParameterMapName;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.MathUtils;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import org.apache.commons.collections.map.HashedMap;
import org.apache.poi.ss.usermodel.DateUtil;
import org.primefaces.model.chart.*;
import org.primefaces.model.chart.Axis;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.*;

@ManagedBean
@ViewScoped
public class TransactionIndicatorMonitoringHourOldManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<BankBean> listBank;
    private String maxDate;
    private int defaultRangeInHour;
    private Date selectedEndDate;
    private Integer selectedEndHour;
    private Date selectedStartDate;
    private Integer selectedStartHour;
    private String selectedBankId;
    private String selectedBankName;
    private String render;
    private List<EsqTrxIdBean> listTrxId;
    private String selectedTrxId;
    private List<EsqTrx5MinBean>  list;
    private List<EsqTrxIdBean> listTrxIdIndicator;
    private String[] selectedTransactionIndicatorId;
    private String selectedTrxName;
    private String selectedTransactionIndicatorName;
    private String graphRefreshInterval;
    private boolean showTable;
    private boolean disableAutoRefresh;
    private String disableAutoRefreshString;
    private String renderResult;
    private boolean showPercent;
    private String showPercentButtonText;

    private String renderAcqOnly;
    private String renderIssOnly;
    private String renderBnfOnly;
    private String renderAcqIss;
    private String renderAcqBnf;
    private String renderIssBnf;

    /*private LineChartModel dataReportAcqOnly;
    private LineChartModel dataReportIssOnly;
    private LineChartModel dataReportBnfOnly;
    private LineChartModel dataReportAcqIss;
    private LineChartModel dataReportAcqBnf;
    private LineChartModel dataReportIssBnf;*/

    private Data dataAcqOnly;
    private com.martinlinha.c3faces.script.property.Axis axisAcqOnly;
    private Point pointAcqOnly;

    private Data dataIssOnly;
    private com.martinlinha.c3faces.script.property.Axis axisIssOnly;
    private Point pointIssOnly;

    private Data dataBnfOnly;
    private com.martinlinha.c3faces.script.property.Axis axisBnfOnly;
    private Point pointBnfOnly;

    private Data dataAcqIss;
    private com.martinlinha.c3faces.script.property.Axis axisAcqIss;
    private Point pointAcqIss;

    private Data dataAcqBnf;
    private com.martinlinha.c3faces.script.property.Axis axisAcqBnf;
    private Point pointAcqBnf;

    private Data dataIssBnf;
    private com.martinlinha.c3faces.script.property.Axis axisIssBnf;
    private Point pointIssBnf;

    C3ChartBean c3ChartBeanAcqOnly;
    C3ChartBean c3ChartBeanIssOnly;
    C3ChartBean c3ChartBeanBnfOnly;

    C3ChartBean c3ChartBeanAcqIss;
    C3ChartBean c3ChartBeanAcqBnf;
    C3ChartBean c3ChartBeanIssBnf;


    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0502";
            pageTitle = getMenuName(moduleFactory, menuId);

            String s = StringUtils.checkNull((String)getRequestParameterMap().get("bid"));
            if (s.length() > 0) {
                selectedBankId = s;
            }

            s = StringUtils.checkNull((String)getRequestParameterMap().get("trxid"));
            if (s.length() > 0) {
                selectedTrxId = s;
            }

            renderResult = "false";
            disableAutoRefresh = false;
            disableAutoRefreshString = "true";
            showTable = false;
            graphRefreshInterval = DataConstant.DEFAULT_GRAPH_REFRESH_INTERVAL;
            defaultRangeInHour = Integer.parseInt(
                    moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(), DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_HOURLY)
            );
            render = "false";
            renderAcqOnly = "false";
            renderIssOnly = "false";
            renderBnfOnly = "false";
            renderAcqIss = "false";
            renderAcqBnf = "false";
            renderIssBnf = "false";
            showPercent = false;
            showPercentButtonText = "Show Percent";
            s = StringUtils.checkNull( (String)getRequestParameterMap().get("sp"));
            if (s.equalsIgnoreCase("true")) {
                showPercent = true;
                showPercentButtonText = "Show Percent";
            } else if (s.equalsIgnoreCase("false")) {
                showPercent = false;
                showPercentButtonText = "Show Total";
            }

            maxDate = DateUtils.convertDate(selectedEndDate, DateUtils.DATE_DDMMYYYY_HHMMSS);

            Map<String, Object> paramBank = new HashMap<>();
            listBank = moduleFactory.getMasterService().listBank(paramBank);

            listTrxId = moduleFactory.getMasterService().listEsqTrxId();


            Date now = new Date();
            selectedEndDate = DateUtils.setDate(Calendar.HOUR_OF_DAY, now, DateUtils.getHour(now));
            selectedEndDate = DateUtils.setDate(Calendar.MINUTE, now, DateUtils.getMinute(now));
            Date thdate = selectedEndDate;
            if (selectedEndDate.getTime() < DateUtils.getStartOfDay(now).getTime()  ) {
                thdate = DateUtils.getEndOfDay235959(thdate);
                thdate = DateUtils.addDate(Calendar.SECOND, thdate, 1);
            } else {
                //log.info(thdate);
                //int thminute = DateUtils.getMinute(thdate) % 5;
                //log.info(thminute);
                thdate = DateUtils.setDate(Calendar.MINUTE, thdate, 59);
                thdate = DateUtils.setDate(Calendar.SECOND, thdate, 59);
                //log.info(thdate);
            }
            selectedEndDate = thdate;
            selectedEndHour = DateUtils.getHour(thdate);


            Date frdate = DateUtils.addDate(Calendar.HOUR_OF_DAY, thdate, defaultRangeInHour*-1);
            //log.info(frdate);
            //int frminute = DateUtils.getMinute(frdate) % 5;
            //log.info(frminute);
            frdate = DateUtils.addDate(Calendar.MINUTE, frdate, 0);
            frdate = DateUtils.setDate(Calendar.SECOND, frdate, 0);
            //log.info(frdate);
            selectedStartDate = frdate;
            selectedStartHour = DateUtils.getHour(frdate);

            if (StringUtils.checkNull(selectedBankId).length() > 0 && StringUtils.checkNull(selectedTrxId).length() > 0) {
                submitAction();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void disableAutoRefreshChangeListener() {
        if (disableAutoRefresh) {
            disableAutoRefreshString = "false";
        } else {
            disableAutoRefreshString = "true";
        }
    }

    public String submitAction() {
        try {
            trxIdChangeListener();

            if (showPercent) {
                showPercentButtonText = "Show Total";
            } else {
                showPercentButtonText = "Show Percent";
            }

            if (selectedBankId.length() == 0) {
                glowMessageError("Select Bank", "");
                return "";
            }
            if (selectedTrxId.length() == 0) {
                glowMessageError("Select Transaction", "");
                return "";
            }
            if (selectedTransactionIndicatorId == null) {
                glowMessageError("Select Transaction Indicator", "");
                return "";
            }
            if (selectedTransactionIndicatorId.length == 0) {
                glowMessageError("Select Transaction Indicator", "");
                return "";
            }


            EisBank bank = moduleFactory.getMasterService().getBank(selectedBankId);
            selectedBankName = bank.getBankName();

            if (selectedTrxId.equalsIgnoreCase(DataConstant.ALL)) {
                selectedTrxName = DataConstant.STR_ALL_TRANSACTION;
            } else {
                EsqTrxIdBean trxid = moduleFactory.getMasterService().getEsqTrxId(selectedTrxId);
                selectedTrxName = trxid.getTrxname();
            }

            renderAcqOnly = "false";
            renderIssOnly = "false";
            renderBnfOnly = "false";
            renderAcqIss = "false";
            renderAcqBnf = "false";
            renderIssBnf = "false";

            renderResult = "true";

            selectedTransactionIndicatorName = "";

            if (selectedTrxId.equalsIgnoreCase(DataConstant.ALL)) {
                selectedTransactionIndicatorName = "All";
                renderAcqOnly = "true";
                renderIssOnly = "true";
                renderBnfOnly = "true";
                renderAcqIss = "true";
                renderAcqBnf = "true";
                renderIssBnf = "true";

            } else {
                for (String id : selectedTransactionIndicatorId) {
                    EsqTrxIdBean trxIndicator = moduleFactory.getMasterService().getEsqTrxIdIndicator(id);
                    if (selectedTransactionIndicatorName.length() == 0) {
                        selectedTransactionIndicatorName = trxIndicator.getTrxnameindicator();
                    } else {
                        selectedTransactionIndicatorName = selectedTransactionIndicatorName + ", " + trxIndicator.getTrxnameindicator();
                    }

                    if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ONLY)) {
                        renderAcqOnly = "true";
                    }
                    if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_ONLY)) {
                        renderIssOnly = "true";
                    }
                    if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__BNF_ONLY)) {
                        renderBnfOnly = "true";
                    }
                    if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ISS)) {
                        renderAcqIss = "true";
                    }
                    if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_BNF)) {
                        renderAcqBnf = "true";
                    }
                    if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_BNF)) {
                        renderIssBnf = "true";
                    }
                }
            }



            Date thdate = selectedEndDate;
            Date frdate = selectedStartDate;

            if (disableAutoRefresh) {
                thdate = selectedEndDate;
                //round minute/second to zero
                thdate = DateUtils.setMinute(thdate, 0);
                thdate = DateUtils.setSecond(thdate, 0);
                frdate = selectedStartDate;

                thdate = DateUtils.setDate(Calendar.HOUR_OF_DAY, thdate, selectedEndHour);
                frdate = DateUtils.setDate(Calendar.HOUR_OF_DAY, frdate, selectedStartHour);
            } else {
                //thdate = new Date();
                //round minute/second to zero
                //thdate = DateUtils.setMinute(thdate, 0);
                //thdate = DateUtils.setSecond(thdate, 0);
                String s = moduleFactory.getAggregateService().getAggrLastTime(selectedTrxId, DataConstant.T1H);
                thdate = DateUtils.convertDate(s, DateUtils.DATE_YYMMDD_HHMM);
                frdate = DateUtils.addDate(Calendar.HOUR, thdate, -1 * defaultRangeInHour);

            }


            list = listDataTrx(frdate, thdate, selectedBankId, selectedTrxId);




            List<EsqTrx5MinBean> listAcqOnly = new ArrayList<>();
            List<EsqTrx5MinBean> listIssOnly = new ArrayList<>();
            List<EsqTrx5MinBean> listBnfOnly = new ArrayList<>();
            List<EsqTrx5MinBean> listAcqIss = new ArrayList<>();
            List<EsqTrx5MinBean> listAcqBnf = new ArrayList<>();
            List<EsqTrx5MinBean> listIssBnf = new ArrayList<>();


            List<String> listDateStringAcqOnly = new ArrayList<>();
            List<Integer> listDataTotalTransaksiAcqOnly = new ArrayList<>();
            List<Integer> listDataTotalApproveAcqOnly = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeAcqOnly = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeAcqOnly = new ArrayList<>();

            List<String> listDateStringIssOnly = new ArrayList<>();
            List<Integer> listDataTotalTransaksiIssOnly = new ArrayList<>();
            List<Integer> listDataTotalApproveIssOnly = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeIssOnly = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeIssOnly = new ArrayList<>();

            List<String> listDateStringBnfOnly = new ArrayList<>();
            List<Integer> listDataTotalTransaksiBnfOnly = new ArrayList<>();
            List<Integer> listDataTotalApproveBnfOnly = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeBnfOnly = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeBnfOnly = new ArrayList<>();

            List<String> listDateStringAcqIss = new ArrayList<>();
            List<Integer> listDataTotalTransaksiAcqIss = new ArrayList<>();
            List<Integer> listDataTotalApproveAcqIss = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeAcqIss = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeAcqIss = new ArrayList<>();

            List<String> listDateStringAcqBnf = new ArrayList<>();
            List<Integer> listDataTotalTransaksiAcqBnf = new ArrayList<>();
            List<Integer> listDataTotalApproveAcqBnf = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeAcqBnf = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeAcqBnf = new ArrayList<>();

            List<String> listDateStringIssBnf = new ArrayList<>();
            List<Integer> listDataTotalTransaksiIssBnf = new ArrayList<>();
            List<Integer> listDataTotalApproveIssBnf = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeIssBnf = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeIssBnf = new ArrayList<>();


            List<BigDecimal> listDataPercentApprAcqOnly = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCAcqOnly = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFAcqOnly = new ArrayList<>();

            List<BigDecimal> listDataPercentApprIssOnly = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCIssOnly = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFIssOnly = new ArrayList<>();

            List<BigDecimal> listDataPercentApprBnfOnly = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCBnfOnly = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFBnfOnly = new ArrayList<>();

            List<BigDecimal> listDataPercentApprAcqIss = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCAcqIss = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFAcqIss = new ArrayList<>();

            List<BigDecimal> listDataPercentApprAcqBnf = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCAcqBnf = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFAcqBnf = new ArrayList<>();

            List<BigDecimal> listDataPercentApprIssBnf = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCIssBnf = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFIssBnf = new ArrayList<>();



            for(int i=0; i < list.size() - 1; i++) {
                EsqTrx5MinBean esq = list.get(i);
                Date d = DateUtils.convertDate(esq.getPeriod() + " " + esq.getTimeto(), DateUtils.DATE_YYMMDD_HHMM);
                listDateStringAcqOnly.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringIssOnly.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringBnfOnly.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringAcqIss.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringAcqBnf.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringIssBnf.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));

                for(String trxindid : selectedTransactionIndicatorId) {
                    //log.info(trxindid);
                    if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ONLY)) {
                        listAcqOnly.add(esq);
                        BigDecimal b = MathUtils.checkNull(esq.getFreq_acq_app())
                                .add(MathUtils.checkNull(esq.getFreq_acq_dc()))
                                .add(MathUtils.checkNull(esq.getFreq_acq_df()));
                        BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acq_app()), b);
                        BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acq_dc(), b);
                        BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acq_df(), b);
                        //csAcqOnlyTotal.set(esq.getPeriod() + " " + esq.getHourfr(), b);
                        //csAcqOnlyAppr.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acq_app()));
                        //csAcqOnlyDc.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acq_dc()));
                        //csAcqOnlyDf.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acq_df()));
                        listDataTotalTransaksiAcqOnly.add(b.intValue());
                        listDataTotalApproveAcqOnly.add(MathUtils.checkNull(esq.getFreq_acq_app()).intValue());
                        listDataTotalDeclineChargeAcqOnly.add(MathUtils.checkNull(esq.getFreq_acq_dc()).intValue());
                        listDataTotalDeclineFreeAcqOnly.add(MathUtils.checkNull(esq.getFreq_acq_df()).intValue());
                        listDataPercentApprAcqOnly.add(pctAppr.multiply(new BigDecimal("100")));
                        listDataPercentDeclCAcqOnly.add(pctDeclC.multiply(new BigDecimal("100")));
                        listDataPercentDeclFAcqOnly.add(pctDeclF.multiply(new BigDecimal("100")));
                    }

                    if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_ONLY)) {
                        listIssOnly.add(esq);
                        BigDecimal b = MathUtils.checkNull(esq.getFreq_iss_app())
                                .add(MathUtils.checkNull(esq.getFreq_iss_dc()))
                                .add(MathUtils.checkNull(esq.getFreq_iss_df()));
                        BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_iss_app()), b);
                        BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_iss_dc(), b);
                        BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_iss_df(), b);
                        //csIssOnlyTotal.set(esq.getPeriod() + " " + esq.getHourfr(), b);
                        //csIssOnlyAppr.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_iss_app()));
                        //csIssOnlyDc.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_iss_dc()));
                        //csIssOnlyDf.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_iss_df()));
                        listDataTotalTransaksiIssOnly.add(b.intValue());
                        listDataTotalApproveIssOnly.add(MathUtils.checkNull(esq.getFreq_iss_app()).intValue());
                        listDataTotalDeclineChargeIssOnly.add(MathUtils.checkNull(esq.getFreq_iss_dc()).intValue());
                        listDataTotalDeclineFreeIssOnly.add(MathUtils.checkNull(esq.getFreq_iss_df()).intValue());
                        listDataPercentApprIssOnly.add(pctAppr.multiply(new BigDecimal("100")));
                        listDataPercentDeclCIssOnly.add(pctDeclC.multiply(new BigDecimal("100")));
                        listDataPercentDeclFIssOnly.add(pctDeclF.multiply(new BigDecimal("100")));
                    }

                    if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__BNF_ONLY)) {
                        listBnfOnly.add(esq);
                        BigDecimal b = MathUtils.checkNull(esq.getFreq_bnf_app())
                                .add(MathUtils.checkNull(esq.getFreq_bnf_dc()))
                                .add(MathUtils.checkNull(esq.getFreq_bnf_df()));
                        BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_bnf_app()), b);
                        BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_bnf_dc(), b);
                        BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_bnf_df(), b);
                        //csBnfOnlyTotal.set(esq.getPeriod() + " " + esq.getHourfr(), b);
                        //csBnfOnlyAppr.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_bnf_app()));
                        //csBnfOnlyDc.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_bnf_dc()));
                        //csBnfOnlyDf.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_bnf_df()));
                        listDataTotalTransaksiBnfOnly.add(b.intValue());
                        listDataTotalApproveBnfOnly.add(MathUtils.checkNull(esq.getFreq_bnf_app()).intValue());
                        listDataTotalDeclineChargeBnfOnly.add(MathUtils.checkNull(esq.getFreq_bnf_dc()).intValue());
                        listDataTotalDeclineFreeBnfOnly.add(MathUtils.checkNull(esq.getFreq_bnf_df()).intValue());
                        listDataPercentApprBnfOnly.add(pctAppr.multiply(new BigDecimal("100")));
                        listDataPercentDeclCBnfOnly.add(pctDeclC.multiply(new BigDecimal("100")));
                        listDataPercentDeclFBnfOnly.add(pctDeclF.multiply(new BigDecimal("100")));
                    }

                    if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ISS)) {
                        listAcqIss.add(esq);
                        BigDecimal b = MathUtils.checkNull(esq.getFreq_acqIss_app())
                                .add(MathUtils.checkNull(esq.getFreq_acqIss_dc()))
                                .add(MathUtils.checkNull(esq.getFreq_acqIss_df()));
                        BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqIss_app()), b);
                        BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acqIss_dc(), b);
                        BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acqIss_df(), b);
                        //csAcqIssTotal.set(esq.getPeriod() + " " + esq.getHourfr(), b);
                        //csAcqIssAppr.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acqIss_app()));
                        //csAcqIssDc.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acqIss_dc()));
                        //csAcqIssDf.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acqIss_df()));
                        listDataTotalTransaksiAcqIss.add(b.intValue());
                        listDataTotalApproveAcqIss.add(MathUtils.checkNull(esq.getFreq_acqIss_app()).intValue());
                        listDataTotalDeclineChargeAcqIss.add(MathUtils.checkNull(esq.getFreq_acqIss_dc()).intValue());
                        listDataTotalDeclineFreeAcqIss.add(MathUtils.checkNull(esq.getFreq_acqIss_df()).intValue());
                        listDataPercentApprAcqIss.add(pctAppr.multiply(new BigDecimal("100")));
                        listDataPercentDeclCAcqIss.add(pctDeclC.multiply(new BigDecimal("100")));
                        listDataPercentDeclFAcqIss.add(pctDeclF.multiply(new BigDecimal("100")));
                    }

                    if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_BNF)) {
                        listAcqBnf.add(esq);
                        BigDecimal b = MathUtils.checkNull(esq.getFreq_acqBnf_app())
                                .add(MathUtils.checkNull(esq.getFreq_acqBnf_dc()))
                                .add(MathUtils.checkNull(esq.getFreq_acqBnf_df()));
                        BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqBnf_app()), b);
                        BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_dc(), b);
                        BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_df(), b);
                        //csAcqBnfTotal.set(esq.getPeriod() + " " + esq.getHourfr(), b);
                        //csAcqBnfAppr.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acqBnf_app()));
                        //csAcqBnfDc.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acqBnf_dc()));
                        //csAcqBnfDf.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acqBnf_df()));
                        listDataTotalTransaksiAcqBnf.add(b.intValue());
                        listDataTotalApproveAcqBnf.add(MathUtils.checkNull(esq.getFreq_acqBnf_app()).intValue());
                        listDataTotalDeclineChargeAcqBnf.add(MathUtils.checkNull(esq.getFreq_acqBnf_dc()).intValue());
                        listDataTotalDeclineFreeAcqBnf.add(MathUtils.checkNull(esq.getFreq_acqBnf_df()).intValue());
                        listDataPercentApprAcqBnf.add(pctAppr.multiply(new BigDecimal("100")));
                        listDataPercentDeclCAcqBnf.add(pctDeclC.multiply(new BigDecimal("100")));
                        listDataPercentDeclFAcqBnf.add(pctDeclF.multiply(new BigDecimal("100")));
                    }

                    if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_BNF)) {
                        listIssBnf.add(esq);
                        BigDecimal b = MathUtils.checkNull(esq.getFreq_issBnf_app())
                                .add(MathUtils.checkNull(esq.getFreq_issBnf_dc()))
                                .add(MathUtils.checkNull(esq.getFreq_issBnf_df()));
                        BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_issBnf_app()), b);
                        BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_issBnf_dc(), b);
                        BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_issBnf_df(), b);
                        //csIssBnfTotal.set(esq.getPeriod() + " " + esq.getHourfr(), b);
                        //csIssBnfAppr.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_issBnf_app()));
                        //csIssBnfDc.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_issBnf_dc()));
                        //csIssBnfDf.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_issBnf_df()));
                        listDataTotalTransaksiIssBnf.add(b.intValue());
                        listDataTotalApproveIssBnf.add(MathUtils.checkNull(esq.getFreq_issBnf_app()).intValue());
                        listDataTotalDeclineChargeIssBnf.add(MathUtils.checkNull(esq.getFreq_issBnf_dc()).intValue());
                        listDataTotalDeclineFreeIssBnf.add(MathUtils.checkNull(esq.getFreq_issBnf_df()).intValue());
                        listDataPercentApprIssBnf.add(pctAppr.multiply(new BigDecimal("100")));
                        listDataPercentDeclCIssBnf.add(pctDeclC.multiply(new BigDecimal("100")));
                        listDataPercentDeclFIssBnf.add(pctDeclF.multiply(new BigDecimal("100")));
                    }
                }
            }






            String title = selectedBankName + " - " + selectedTrxName + " - " + selectedTransactionIndicatorName + " - 5 Min";


            String colorTotal = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_TOTAL);
            String colorApprove = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_APPR);
            String colorDc = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_DC);
            String colorDf = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_DF);

            if (!showPercent) {
                c3ChartBeanAcqOnly = new C3ChartBean();
                generateChart(listDataTotalTransaksiAcqOnly,
                        listDataTotalApproveAcqOnly,
                        listDataTotalDeclineChargeAcqOnly,
                        listDataTotalDeclineFreeAcqOnly,
                        listDateStringAcqOnly,
                        colorTotal, colorApprove, colorDc, colorDf,
                        title,
                        DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                        DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                        c3ChartBeanAcqOnly);

                c3ChartBeanIssOnly = new C3ChartBean();
                generateChart(listDataTotalTransaksiIssOnly,
                        listDataTotalApproveIssOnly,
                        listDataTotalDeclineChargeIssOnly,
                        listDataTotalDeclineFreeIssOnly,
                        listDateStringIssOnly,
                        colorTotal, colorApprove, colorDc, colorDf,
                        title,
                        DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                        DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                        c3ChartBeanIssOnly);

                c3ChartBeanBnfOnly = new C3ChartBean();
                generateChart(listDataTotalTransaksiBnfOnly,
                        listDataTotalApproveBnfOnly,
                        listDataTotalDeclineChargeBnfOnly,
                        listDataTotalDeclineFreeBnfOnly,
                        listDateStringBnfOnly,
                        colorTotal, colorApprove, colorDc, colorDf,
                        title,
                        DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                        DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                        c3ChartBeanBnfOnly);

                c3ChartBeanAcqIss = new C3ChartBean();
                generateChart(listDataTotalTransaksiAcqIss,
                        listDataTotalApproveAcqIss,
                        listDataTotalDeclineChargeAcqIss,
                        listDataTotalDeclineFreeAcqIss,
                        listDateStringAcqIss,
                        colorTotal, colorApprove, colorDc, colorDf,
                        title,
                        DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                        DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                        c3ChartBeanAcqIss);

                c3ChartBeanAcqBnf = new C3ChartBean();
                generateChart(listDataTotalTransaksiAcqBnf,
                        listDataTotalApproveAcqBnf,
                        listDataTotalDeclineChargeAcqBnf,
                        listDataTotalDeclineFreeAcqBnf,
                        listDateStringAcqBnf,
                        colorTotal, colorApprove, colorDc, colorDf,
                        title,
                        DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                        DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                        c3ChartBeanAcqBnf);

                c3ChartBeanIssBnf = new C3ChartBean();
                generateChart(listDataTotalTransaksiIssBnf,
                        listDataTotalApproveIssBnf,
                        listDataTotalDeclineChargeIssBnf,
                        listDataTotalDeclineFreeIssBnf,
                        listDateStringIssBnf,
                        colorTotal, colorApprove, colorDc, colorDf,
                        title,
                        DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                        DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                        c3ChartBeanIssBnf);

            } else {
                List<DsBean> listDsBean = new ArrayList<>();

                DsBean dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprAcqOnly);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCAcqOnly);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFAcqOnly);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                c3ChartBeanAcqOnly = new C3ChartBean();
                generateChartMultiData(c3ChartBeanAcqOnly, listDsBean, listDateStringAcqOnly, title);


                //iss only
                listDsBean = new ArrayList<>();
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprIssOnly);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCIssOnly);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFIssOnly);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                c3ChartBeanIssOnly = new C3ChartBean();
                generateChartMultiData(c3ChartBeanIssOnly, listDsBean, listDateStringIssOnly, title);


                //bnf only
                listDsBean = new ArrayList<>();
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprBnfOnly);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCBnfOnly);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFBnfOnly);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                c3ChartBeanBnfOnly = new C3ChartBean();
                generateChartMultiData(c3ChartBeanBnfOnly, listDsBean, listDateStringBnfOnly, title);


                //acq iss
                listDsBean = new ArrayList<>();
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprAcqIss);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCAcqIss);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFAcqIss);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                c3ChartBeanAcqIss = new C3ChartBean();
                generateChartMultiData(c3ChartBeanAcqIss, listDsBean, listDateStringAcqIss, title);



                //acq bnf
                listDsBean = new ArrayList<>();
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprAcqBnf);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCAcqBnf);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFAcqBnf);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                c3ChartBeanAcqBnf = new C3ChartBean();
                generateChartMultiData(c3ChartBeanAcqBnf, listDsBean, listDateStringAcqBnf, title);


                //iss bnf
                listDsBean = new ArrayList<>();
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprIssBnf);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCIssBnf);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFIssBnf);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                c3ChartBeanIssBnf = new C3ChartBean();
                generateChartMultiData(c3ChartBeanIssBnf, listDsBean, listDateStringIssBnf, title);


            }



            render = "true";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String showPercentChangeListener() {
        /*showPercent = !showPercent;
        if (showPercent) {
            showPercentButtonText = "Show Total";
        } else {
            showPercentButtonText = "Show Percent";
        }*/
        //disableAutoRefreshChangeListener();
        //poolAction();
        return "transactionIndicatorMonitoringHourPercent?faces-redirect=true&bid=" +
                StringUtils.checkNull(selectedBankId) + "&trxid=" + StringUtils.checkNull(selectedTrxId);
    }

    private List<EsqTrx5MinBean> listDataTrx(Date frdate, Date thdate, String bankId, String trxId) throws Exception {
        List<EsqTrx5MinBean> list = new ArrayList<>();
        Date sd = frdate;
        //Date ed = thdate;
        int idx = 0;
        while (true) {
            if (idx == 287) {
                //log.info(idx);
            }
            String period = DateUtils.convertDate(sd, DateUtils.DATE_YYMMDD);
            int ihourfr = DateUtils.getHour(sd);
            Date ed = DateUtils.addDate(Calendar.MINUTE, sd, 5);
            int ihourto = DateUtils.getHour(ed);
            //int iminfr= DateUtils.getMinute(sd);
            //int iminto = DateUtils.getMinute(ed);
            //if (ihourto == 0 && iminto == 0) {
            //    ihourto = 24;
            //}
            ihourto++;
            //String periodto = DateUtils.convertDate(ed, DateUtils.DATE_YYMMDD);
            //log.info(period + " " + ihourfr + ":" + ihourto );

            String shourfr = StringUtils.formatFixLength(Integer.toString(ihourfr), "0", 2) + ":00";
            String shourto = StringUtils.formatFixLength(Integer.toString(ihourto), "0", 2) + ":00";
            //String sminfr = StringUtils.formatFixLength(Integer.toString(ihourfr), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(iminfr), "0", 2);
            //String sminto = StringUtils.formatFixLength(Integer.toString(ihourfr), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(iminto), "0", 2);
            if (shourto.equalsIgnoreCase("00:00")) {
                shourto = "24:00";
            }
            EsqTrx5MinBean esqTrx5MinBean = moduleFactory.getBiService().getEsqTrx(DataConstant.TABLE_TRX1HOUR,
                    selectedBankId, selectedTrxId,
                    period, shourfr, shourto);
            if (esqTrx5MinBean == null) {
                esqTrx5MinBean = new EsqTrx5MinBean();
                esqTrx5MinBean.setPeriod(period);
                esqTrx5MinBean.setBankid(selectedBankId);
                esqTrx5MinBean.setTrxid(selectedTrxId);
                esqTrx5MinBean.setTimefr(shourfr);
                esqTrx5MinBean.setTimeto(shourto);

            }
            list.add(esqTrx5MinBean);

            if (ed.getTime() >= thdate.getTime()) {
                break;
            }

            sd = DateUtils.addDate(Calendar.HOUR_OF_DAY, sd, 1);
            idx++;
        };

        return list;
    }


    private void generateChart(List<Integer> listDataTotalTransaksi,
                               List<Integer> listDataTotalApprove,
                               List<Integer> listDataTotalDeclineCharge,
                               List<Integer> listDataTotalDeclineFree,
                               List<String> listDateString,
                               String colorTotal, String colorApprove,
                               String colorDc, String colorDf, String title,
                               String dsNameTotal, String dsNameAppr, String dsNameDc, String dsNameDf,
                               C3ChartBean c3ChartBean) {
        C3DataSet dsTotal = new C3DataSet(listDataTotalTransaksi);
        C3ViewDataSet dataSetTotal = new C3ViewDataSet(dsNameTotal, dsTotal, "#"+colorTotal);

        C3DataSet dsAppr = new C3DataSet(listDataTotalApprove);
        C3ViewDataSet dataSetAppr = new C3ViewDataSet(dsNameAppr, dsAppr, "#"+colorApprove);

        C3DataSet dsDc = new C3DataSet(listDataTotalDeclineCharge);
        C3ViewDataSet dataSetDc = new C3ViewDataSet(dsNameDc, dsDc, "#"+colorDc);

        C3DataSet dsDf = new C3DataSet(listDataTotalDeclineFree);
        C3ViewDataSet dataSetDf = new C3ViewDataSet(dsNameDf, dsDf, "#"+colorDf);

        C3DataSetString dsDateString = new C3DataSetString(listDateString);
        C3ViewDataSetString dataSetDateString = new C3ViewDataSetString("x", dsDateString, false);

        Data data = new Data();
        data.getDataSetsString().add(dataSetDateString);
        data.getDataSets().add(dataSetTotal);
        data.getDataSets().add(dataSetAppr);
        data.getDataSets().add(dataSetDc);
        data.getDataSets().add(dataSetDf);

        data.setX("z1");
        data.setxFormat("%Y-%m-%d %H:%M");

        com.martinlinha.c3faces.script.property.Axis axis = new com.martinlinha.c3faces.script.property.Axis();
        axis.setHeightX(70);
        axis.setTypeX(com.martinlinha.c3faces.script.property.Axis.Type.TIMESERIES);
        axis.setTickX("fit: true,\n" +
                "format: '%d/%m %H:%M',\n" +
                "rotate: -45,\n" +
                "multiline: false,\n" +
                "culling: true");
        axis.setMinY(0);

        Point point = new Point();
        point.setShow(true);

        c3ChartBean.setData(data);
        c3ChartBean.setAxis(axis);
        c3ChartBean.setPoint(point);
        //c3ChartBean.setPadding(padding);
        c3ChartBean.setTitle(title);
    }

    public String showHistory() {
        return "transactionIndicatorMonitoringHourHistory?faces-redirect=true&bid=" +
                StringUtils.checkNull(selectedBankId) + "&trxid=" + StringUtils.checkNull(selectedTrxId);
    }



    public void onPoolListener() {
        try {
            if (disableAutoRefresh) {
                submitAction();
                return;
            }

            submitAction();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void trxIdChangeListener() {
        try {
            if (selectedTrxId.equalsIgnoreCase(DataConstant.ALL)) {
                listTrxIdIndicator = new ArrayList<>();
                selectedTransactionIndicatorId = new String[6];
                selectedTransactionIndicatorId[0] = "01";
                selectedTransactionIndicatorId[1] = "02";
                selectedTransactionIndicatorId[2] = "03";
                selectedTransactionIndicatorId[3] = "04";
                selectedTransactionIndicatorId[4] = "05";
                selectedTransactionIndicatorId[5] = "06";
                return;
            } else {
                listTrxIdIndicator = moduleFactory.getMasterService().listEsqTrxIdTrxIndicator(selectedTrxId);
            }
            //listTrxIdIndicator = moduleFactory.getMasterService().listEsqTrxIdIndicator();

            selectedTransactionIndicatorId = new String[listTrxIdIndicator.size()];
            for(int i = 0; i < listTrxIdIndicator.size(); i++) {
                EsqTrxIdBean id = listTrxIdIndicator.get(i);
                selectedTransactionIndicatorId[i] = id.getTrxidindicator();
            }

            //selectedTransactionIndicatorId = null;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void generateChartMultiData(C3ChartBean c3ChartBean, List<DsBean> listData, List<String> listDateString,
                                        String title) {
        List<C3ViewDataSet> listDataSet = new ArrayList<>();

        for(DsBean dsBean : listData) {
            C3DataSet ds = new C3DataSet(dsBean.getListData());
            C3ViewDataSet dataSet = new C3ViewDataSet(dsBean.getName(), ds, "#"+dsBean.getColor());
            listDataSet.add(dataSet);
        }

        C3DataSetString dsDateString = new C3DataSetString(listDateString);
        C3ViewDataSetString dataSetDateString = new C3ViewDataSetString("x", dsDateString, false);

        Data data = new Data();
        data.getDataSetsString().add(dataSetDateString);
        for(C3ViewDataSet ds : listDataSet) {
            data.getDataSets().add(ds);
        }

        data.setX("z1");
        data.setxFormat("%Y-%m-%d %H:%M");

        com.martinlinha.c3faces.script.property.Axis axis = new com.martinlinha.c3faces.script.property.Axis();
        axis.setHeightX(70);
        axis.setTypeX(com.martinlinha.c3faces.script.property.Axis.Type.TIMESERIES);
        axis.setTickX("fit: true,\n" +
                "format: '%d/%m %H:%M',\n" +
                "rotate: -45,\n" +
                "multiline: false,\n" +
                "culling: true");
        axis.setMinY(0);

        Point point = new Point();
        point.setShow(true);

        //Padding padding = new Padding();

        c3ChartBean.setData(data);
        c3ChartBean.setAxis(axis);
        c3ChartBean.setPoint(point);
        //c3ChartBean.setPadding(padding);
        c3ChartBean.setTitle(title);
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<BankBean> getListBank() {
        return listBank;
    }

    public void setListBank(List<BankBean> listBank) {
        this.listBank = listBank;
    }

    public String getSelectedBankId() {
        return selectedBankId;
    }

    public void setSelectedBankId(String selectedBankId) {
        this.selectedBankId = selectedBankId;
    }

    public String getRender() {
        return render;
    }

    public void setRender(String render) {
        this.render = render;
    }

    public List<EsqTrx5MinBean>  getList() {
        return list;
    }

    public void setList(List<EsqTrx5MinBean>  list) {
        this.list = list;
    }

    public String getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(String maxDate) {
        this.maxDate = maxDate;
    }

    public List<EsqTrxIdBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<EsqTrxIdBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

    public String getSelectedTrxId() {
        return selectedTrxId;
    }

    public void setSelectedTrxId(String selectedTrxId) {
        this.selectedTrxId = selectedTrxId;
    }

    public List<EsqTrxIdBean> getListTrxIdIndicator() {
        return listTrxIdIndicator;
    }

    public void setListTrxIdIndicator(List<EsqTrxIdBean> listTrxIdIndicator) {
        this.listTrxIdIndicator = listTrxIdIndicator;
    }

    public String getSelectedBankName() {
        return selectedBankName;
    }

    public void setSelectedBankName(String selectedBankName) {
        this.selectedBankName = selectedBankName;
    }

    public String[] getSelectedTransactionIndicatorId() {
        return selectedTransactionIndicatorId;
    }

    public void setSelectedTransactionIndicatorId(String[] selectedTransactionIndicatorId) {
        this.selectedTransactionIndicatorId = selectedTransactionIndicatorId;
    }

    public String getSelectedTrxName() {
        return selectedTrxName;
    }

    public void setSelectedTrxName(String selectedTrxName) {
        this.selectedTrxName = selectedTrxName;
    }

    public String getSelectedTransactionIndicatorName() {
        return selectedTransactionIndicatorName;
    }

    public void setSelectedTransactionIndicatorName(String selectedTransactionIndicatorName) {
        this.selectedTransactionIndicatorName = selectedTransactionIndicatorName;
    }

    public String getRenderAcqOnly() {
        return renderAcqOnly;
    }

    public void setRenderAcqOnly(String renderAcqOnly) {
        this.renderAcqOnly = renderAcqOnly;
    }

    public String getRenderIssOnly() {
        return renderIssOnly;
    }

    public void setRenderIssOnly(String renderIssOnly) {
        this.renderIssOnly = renderIssOnly;
    }

    public String getRenderBnfOnly() {
        return renderBnfOnly;
    }

    public void setRenderBnfOnly(String renderBnfOnly) {
        this.renderBnfOnly = renderBnfOnly;
    }

    public String getRenderAcqIss() {
        return renderAcqIss;
    }

    public void setRenderAcqIss(String renderAcqIss) {
        this.renderAcqIss = renderAcqIss;
    }

    public String getRenderAcqBnf() {
        return renderAcqBnf;
    }

    public void setRenderAcqBnf(String renderAcqBnf) {
        this.renderAcqBnf = renderAcqBnf;
    }

    public String getRenderIssBnf() {
        return renderIssBnf;
    }

    public void setRenderIssBnf(String renderIssBnf) {
        this.renderIssBnf = renderIssBnf;
    }

    /*public LineChartModel getDataReportAcqOnly() {
        return dataReportAcqOnly;
    }

    public void setDataReportAcqOnly(LineChartModel dataReportAcqOnly) {
        this.dataReportAcqOnly = dataReportAcqOnly;
    }

    public LineChartModel getDataReportIssOnly() {
        return dataReportIssOnly;
    }

    public void setDataReportIssOnly(LineChartModel dataReportIssOnly) {
        this.dataReportIssOnly = dataReportIssOnly;
    }

    public LineChartModel getDataReportBnfOnly() {
        return dataReportBnfOnly;
    }

    public void setDataReportBnfOnly(LineChartModel dataReportBnfOnly) {
        this.dataReportBnfOnly = dataReportBnfOnly;
    }

    public LineChartModel getDataReportAcqIss() {
        return dataReportAcqIss;
    }

    public void setDataReportAcqIss(LineChartModel dataReportAcqIss) {
        this.dataReportAcqIss = dataReportAcqIss;
    }

    public LineChartModel getDataReportAcqBnf() {
        return dataReportAcqBnf;
    }

    public void setDataReportAcqBnf(LineChartModel dataReportAcqBnf) {
        this.dataReportAcqBnf = dataReportAcqBnf;
    }

    public LineChartModel getDataReportIssBnf() {
        return dataReportIssBnf;
    }

    public void setDataReportIssBnf(LineChartModel dataReportIssBnf) {
        this.dataReportIssBnf = dataReportIssBnf;
    }*/

    public int getDefaultRangeInHour() {
        return defaultRangeInHour;
    }

    public void setDefaultRangeInHour(int defaultRangeInHour) {
        this.defaultRangeInHour = defaultRangeInHour;
    }

    public Date getSelectedEndDate() {
        return selectedEndDate;
    }

    public void setSelectedEndDate(Date selectedEndDate) {
        this.selectedEndDate = selectedEndDate;
    }

    public Integer getSelectedEndHour() {
        return selectedEndHour;
    }

    public void setSelectedEndHour(Integer selectedEndHour) {
        this.selectedEndHour = selectedEndHour;
    }

    public Date getSelectedStartDate() {
        return selectedStartDate;
    }

    public void setSelectedStartDate(Date selectedStartDate) {
        this.selectedStartDate = selectedStartDate;
    }

    public Integer getSelectedStartHour() {
        return selectedStartHour;
    }

    public void setSelectedStartHour(Integer selectedStartHour) {
        this.selectedStartHour = selectedStartHour;
    }

    public String getGraphRefreshInterval() {
        return graphRefreshInterval;
    }

    public void setGraphRefreshInterval(String graphRefreshInterval) {
        this.graphRefreshInterval = graphRefreshInterval;
    }

    public boolean isShowTable() {
        return showTable;
    }

    public void setShowTable(boolean showTable) {
        this.showTable = showTable;
    }

    public boolean isDisableAutoRefresh() {
        return disableAutoRefresh;
    }

    public void setDisableAutoRefresh(boolean disableAutoRefresh) {
        this.disableAutoRefresh = disableAutoRefresh;
    }

    public String getDisableAutoRefreshString() {
        return disableAutoRefreshString;
    }

    public void setDisableAutoRefreshString(String disableAutoRefreshString) {
        this.disableAutoRefreshString = disableAutoRefreshString;
    }

    public Data getDataAcqOnly() {
        return dataAcqOnly;
    }

    public void setDataAcqOnly(Data dataAcqOnly) {
        this.dataAcqOnly = dataAcqOnly;
    }

    public com.martinlinha.c3faces.script.property.Axis getAxisAcqOnly() {
        return axisAcqOnly;
    }

    public void setAxisAcqOnly(com.martinlinha.c3faces.script.property.Axis axisAcqOnly) {
        this.axisAcqOnly = axisAcqOnly;
    }

    public Point getPointAcqOnly() {
        return pointAcqOnly;
    }

    public void setPointAcqOnly(Point pointAcqOnly) {
        this.pointAcqOnly = pointAcqOnly;
    }

    public Data getDataIssOnly() {
        return dataIssOnly;
    }

    public void setDataIssOnly(Data dataIssOnly) {
        this.dataIssOnly = dataIssOnly;
    }

    public com.martinlinha.c3faces.script.property.Axis getAxisIssOnly() {
        return axisIssOnly;
    }

    public void setAxisIssOnly(com.martinlinha.c3faces.script.property.Axis axisIssOnly) {
        this.axisIssOnly = axisIssOnly;
    }

    public Point getPointIssOnly() {
        return pointIssOnly;
    }

    public void setPointIssOnly(Point pointIssOnly) {
        this.pointIssOnly = pointIssOnly;
    }

    public Data getDataBnfOnly() {
        return dataBnfOnly;
    }

    public void setDataBnfOnly(Data dataBnfOnly) {
        this.dataBnfOnly = dataBnfOnly;
    }

    public com.martinlinha.c3faces.script.property.Axis getAxisBnfOnly() {
        return axisBnfOnly;
    }

    public void setAxisBnfOnly(com.martinlinha.c3faces.script.property.Axis axisBnfOnly) {
        this.axisBnfOnly = axisBnfOnly;
    }

    public Point getPointBnfOnly() {
        return pointBnfOnly;
    }

    public void setPointBnfOnly(Point pointBnfOnly) {
        this.pointBnfOnly = pointBnfOnly;
    }

    public Data getDataAcqIss() {
        return dataAcqIss;
    }

    public void setDataAcqIss(Data dataAcqIss) {
        this.dataAcqIss = dataAcqIss;
    }

    public com.martinlinha.c3faces.script.property.Axis getAxisAcqIss() {
        return axisAcqIss;
    }

    public void setAxisAcqIss(com.martinlinha.c3faces.script.property.Axis axisAcqIss) {
        this.axisAcqIss = axisAcqIss;
    }

    public Point getPointAcqIss() {
        return pointAcqIss;
    }

    public void setPointAcqIss(Point pointAcqIss) {
        this.pointAcqIss = pointAcqIss;
    }

    public Data getDataAcqBnf() {
        return dataAcqBnf;
    }

    public void setDataAcqBnf(Data dataAcqBnf) {
        this.dataAcqBnf = dataAcqBnf;
    }

    public com.martinlinha.c3faces.script.property.Axis getAxisAcqBnf() {
        return axisAcqBnf;
    }

    public void setAxisAcqBnf(com.martinlinha.c3faces.script.property.Axis axisAcqBnf) {
        this.axisAcqBnf = axisAcqBnf;
    }

    public Point getPointAcqBnf() {
        return pointAcqBnf;
    }

    public void setPointAcqBnf(Point pointAcqBnf) {
        this.pointAcqBnf = pointAcqBnf;
    }

    public Data getDataIssBnf() {
        return dataIssBnf;
    }

    public void setDataIssBnf(Data dataIssBnf) {
        this.dataIssBnf = dataIssBnf;
    }

    public com.martinlinha.c3faces.script.property.Axis getAxisIssBnf() {
        return axisIssBnf;
    }

    public void setAxisIssBnf(com.martinlinha.c3faces.script.property.Axis axisIssBnf) {
        this.axisIssBnf = axisIssBnf;
    }

    public Point getPointIssBnf() {
        return pointIssBnf;
    }

    public void setPointIssBnf(Point pointIssBnf) {
        this.pointIssBnf = pointIssBnf;
    }

    public C3ChartBean getC3ChartBeanAcqOnly() {
        return c3ChartBeanAcqOnly;
    }

    public void setC3ChartBeanAcqOnly(C3ChartBean c3ChartBeanAcqOnly) {
        this.c3ChartBeanAcqOnly = c3ChartBeanAcqOnly;
    }

    public C3ChartBean getC3ChartBeanIssOnly() {
        return c3ChartBeanIssOnly;
    }

    public void setC3ChartBeanIssOnly(C3ChartBean c3ChartBeanIssOnly) {
        this.c3ChartBeanIssOnly = c3ChartBeanIssOnly;
    }

    public C3ChartBean getC3ChartBeanBnfOnly() {
        return c3ChartBeanBnfOnly;
    }

    public void setC3ChartBeanBnfOnly(C3ChartBean c3ChartBeanBnfOnly) {
        this.c3ChartBeanBnfOnly = c3ChartBeanBnfOnly;
    }

    public C3ChartBean getC3ChartBeanAcqIss() {
        return c3ChartBeanAcqIss;
    }

    public void setC3ChartBeanAcqIss(C3ChartBean c3ChartBeanAcqIss) {
        this.c3ChartBeanAcqIss = c3ChartBeanAcqIss;
    }

    public C3ChartBean getC3ChartBeanAcqBnf() {
        return c3ChartBeanAcqBnf;
    }

    public void setC3ChartBeanAcqBnf(C3ChartBean c3ChartBeanAcqBnf) {
        this.c3ChartBeanAcqBnf = c3ChartBeanAcqBnf;
    }

    public C3ChartBean getC3ChartBeanIssBnf() {
        return c3ChartBeanIssBnf;
    }

    public void setC3ChartBeanIssBnf(C3ChartBean c3ChartBeanIssBnf) {
        this.c3ChartBeanIssBnf = c3ChartBeanIssBnf;
    }

    public String getRenderResult() {
        return renderResult;
    }

    public void setRenderResult(String renderResult) {
        this.renderResult = renderResult;
    }

    public boolean isShowPercent() {
        return showPercent;
    }

    public void setShowPercent(boolean showPercent) {
        this.showPercent = showPercent;
    }

    public String getShowPercentButtonText() {
        return showPercentButtonText;
    }

    public void setShowPercentButtonText(String showPercentButtonText) {
        this.showPercentButtonText = showPercentButtonText;
    }

    public class DsBean {
        private List<? extends Number> listData;
        private String color;
        private String name;

        public List<? extends Number> getListData() {
            return listData;
        }

        public void setListData(List<? extends Number> listData) {
            this.listData = listData;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}

package com.rintis.marketing.web.jsf.main.common;

import com.rintis.marketing.core.utils.MessagesResourceBundle;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;



@ManagedBean
@RequestScoped
public class MessageManagedBean extends AbstractManagedBean {
    private String msg;
    
    @Override
    @PostConstruct
    protected void initManagedBean() {
        String id = (String)getRequestParameterMap().get("id");
        msg = MessagesResourceBundle.getMessage(id);
        if (msg == null) msg = "";
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}

package com.rintis.marketing.web.jsf.main.setting;

import com.rintis.marketing.beans.entity.SystemProperty;
import com.rintis.marketing.beans.entity.SystemPropertyUser;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
public class EditSettingUserManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<SystemPropertyUser> listSystemProperty;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listSystemProperty = moduleFactory.getCommonService().listSystemPropertyUser(getUserInfoBean().getUserId());

            for(SystemPropertyUser sp : listSystemProperty) {
                sp.setTempValue(sp.getSystemValue());
            }

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String saveAction() {
        try {
            //validation
            for(SystemPropertyUser sp : listSystemProperty) {
                if (sp.getSystemName().equalsIgnoreCase("limit_year")) {
                    try {
                        Integer.parseInt(sp.getSystemValue());
                    } catch (Exception ex) {
                        glowMessageError("LIMIT_YEAR must be numeric", "");
                        return "";
                    }
                }

                if (sp.getSystemName().equalsIgnoreCase("top_n")) {
                    try {
                        Integer.parseInt(sp.getSystemValue());
                    } catch (Exception ex) {
                        glowMessageError("TOP_N must be numeric", "");
                        return "";
                    }
                }
            }

            moduleFactory.getCommonService().editSystemPropertyUser(getUserInfoBean().getUserId(), listSystemProperty);
            glowMessageInfo("Update Success", "");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            glowMessageInfo("Update Failed", "");
        }
        return "";
    }

    public String updateColor(SystemPropertyUser item) {
        item.setTempValue(item.getSystemValue());
        return "";
    }

    public String copyColorToPicker(SystemPropertyUser item) {
        item.setSystemValue(item.getTempValue());
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<SystemPropertyUser> getListSystemProperty() {
        return listSystemProperty;
    }

    public void setListSystemProperty(List<SystemPropertyUser> listSystemProperty) {
        this.listSystemProperty = listSystemProperty;
    }

}

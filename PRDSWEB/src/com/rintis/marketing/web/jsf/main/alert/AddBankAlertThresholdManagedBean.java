package com.rintis.marketing.web.jsf.main.alert;

import com.rintis.marketing.beans.bean.bi.AlertThresholdBean;
import com.rintis.marketing.beans.bean.bi.BankAlertThresholdBean;
import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.AlertThreshold;
import com.rintis.marketing.beans.entity.AlertThresholdPk;
import com.rintis.marketing.beans.entity.BankAlertThreshold;
import com.rintis.marketing.beans.entity.EisBank;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.MathUtils;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import jodd.util.MathUtil;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ManagedBean
@ViewScoped
public class AddBankAlertThresholdManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<BankBean> listBank;
    private List<EsqTrxIdBean> listTrxId;
    private List<AlertThreshold> list;
    private List<EsqTrxIdBean> listTrxIdIndicator;
    private String selectedBankId;
    private String selectedBankName;
    private String selectedTrxId;
    private String selectedTransactionName;
    private String selectedTrxIndicatorId;
    private String selectedTrxIndicatorName;
    private boolean activeCheck;
    private String selectedOrder;


    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0503";
            pageTitle = getMenuName(moduleFactory, menuId);

            list = new ArrayList<>();
            selectedOrder = "NAME";
            selectedTrxId = "";
            listTrxId = moduleFactory.getMasterService().listEsqTrxId();

            activeCheck = false;

            Map<String, Object> paramBank = new HashMap<>();
            listBank = moduleFactory.getMasterService().listBank(paramBank);

            listTrxId = moduleFactory.getMasterService().listEsqTrxId();

            String bid = StringUtils.checkNull((String)getRequestParameterMap().get("bid"));
            String trxid = StringUtils.checkNull((String)getRequestParameterMap().get("trxid"));
            if (bid.length() > 0 && trxid.length() > 0) {
                selectedBankId = bid;
                selectedTrxId = trxid;
                submitAction();
            }


            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    public String submitAction() {
        try {
            if (selectedBankId.length() == 0) {
                glowMessageError("Select Bank", "");
                return "";
            }

            if (selectedTrxId.length() == 0) {
                glowMessageError("Select Transaction", "");
                return "";
            }

            EisBank bank = moduleFactory.getMasterService().getBank(selectedBankId);
            selectedBankName = bank.getBankName();
            if (selectedTrxId.equalsIgnoreCase(DataConstant.ALL)) {
                selectedTransactionName = "All";
                listTrxIdIndicator = moduleFactory.getMasterService().listEsqTrxIdTrxIndicator();
            } else {
                EsqTrxIdBean trx = moduleFactory.getMasterService().getEsqTrxId(selectedTrxId);
                selectedTransactionName = trx.getTrxname();
                listTrxIdIndicator = moduleFactory.getMasterService().listEsqTrxIdTrxIndicator(selectedTrxId);
            }



            list = new ArrayList<>();
            for(EsqTrxIdBean trxIndic : listTrxIdIndicator) {
                AlertThreshold alertThreshold = null;

                //get old data
                alertThreshold = moduleFactory.getBiService().getAlertThreshold(selectedBankId, selectedTrxId, trxIndic.getTrxidindicator());
                if (alertThreshold == null) {
                    AlertThresholdPk pk = new AlertThresholdPk();
                    pk.setBankId(selectedBankId);
                    pk.setTransactionId(selectedTrxId);
                    pk.setTransactionIndicatorId(trxIndic.getTrxidindicator());

                    alertThreshold = new AlertThreshold();
                    alertThreshold.setAlertThresholdPk(pk);
                    alertThreshold.setTransactionIndicatorName(trxIndic.getTrxnameindicator());
                } else {
                    alertThreshold.setTransactionIndicatorName(trxIndic.getTrxnameindicator());

                    alertThreshold.setActiveBool(StringUtils.bigdecimal2bool(alertThreshold.getActive()) );
                    alertThreshold.setCompareMethod1Bool(StringUtils.bigdecimal2bool(alertThreshold.getCompareMethod1()) );
                    alertThreshold.setCompareMethod2Bool(StringUtils.bigdecimal2bool(alertThreshold.getCompareMethod2()) );
                    alertThreshold.setCompareMethod3Bool(StringUtils.bigdecimal2bool(alertThreshold.getCompareMethod3()) );
                    alertThreshold.setCompareMethod4Bool(StringUtils.bigdecimal2bool(alertThreshold.getCompareMethod4()) );
                }

                list.add(alertThreshold);
            }


         } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String saveAction() {
        try {
            if (selectedBankId.length() == 0) {
                glowMessageError("Select Bank", "");
                return "";
            }

            if (selectedTrxId.length() == 0) {
                glowMessageError("Select Transaction", "");
                return "";
            }

            if (list.size() == 0) {
                glowMessageError("No Indicator selected", "");
                return "";
            }

            boolean find = false;
            for(AlertThreshold ba : list) {
                if (ba.getActiveBool()) {
                    find = true;
                }
            }
            if (!find) {
                glowMessageError("No Indicator selected", "");
                return "";
            }

            for(AlertThreshold ba : list) {
                if (ba.getActiveBool()) {
                    if (ba.getStartHour().equalsIgnoreCase("00") &&
                            ba.getStartMinute().equalsIgnoreCase("00") &&
                            ba.getEndHour().equalsIgnoreCase("00") &&
                            ba.getEndMinute().equalsIgnoreCase("00") ) {
                        glowMessageError("Select Active Time", "");
                        return "";
                    }

                    if (ba.getCompareMethod1Bool()) {
                        if (ba.getThresholdM1() == null) {
                            glowMessageError("Enter Threshold M1", "");
                            return "";
                        }
                    }

                    if (ba.getCompareMethod2Bool()) {
                        if (ba.getThreshold5MinWdHd() == null || ba.getThreshold5MinWdWd() == null) {
                            glowMessageError("Enter Threshold M2", "");
                            return "";
                        }
                    }
                }

                if (!MathUtils.validatePercentValue(ba.getThresholdM1()) ) {
                    glowMessageError("Threshold M1 (percent) value must be between 0 - 100", "");
                    return "";
                }
                if (!MathUtils.validatePercentValue(ba.getThreshold5MinWdWd()) ||
                        !MathUtils.validatePercentValue(ba.getThreshold5MinWdHd()) //||
                        //!MathUtils.validatePercentValue(ba.getThresholdhourwdwd()) ||
                        //!MathUtils.validatePercentValue(ba.getThresholdhourwdhd())
                        ) {
                    glowMessageError("Threshold M2 (percent) value must be between 0 - 100", "");
                    return "";
                }
            }

            moduleFactory.getBiService().editAlertThreshold(list);

            glowMessageInfo("Update Success", "");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            glowMessageError("Update failed", "");
        }
        return "";
    }


    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<EsqTrxIdBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<EsqTrxIdBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

    public String getSelectedTrxId() {
        return selectedTrxId;
    }

    public void setSelectedTrxId(String selectedTrxId) {
        this.selectedTrxId = selectedTrxId;
    }

    public String getSelectedTransactionName() {
        return selectedTransactionName;
    }

    public void setSelectedTransactionName(String selectedTransactionName) {
        this.selectedTransactionName = selectedTransactionName;
    }

    public boolean isActiveCheck() {
        return activeCheck;
    }

    public void setActiveCheck(boolean activeCheck) {
        this.activeCheck = activeCheck;
    }

    public String getSelectedOrder() {
        return selectedOrder;
    }

    public void setSelectedOrder(String selectedOrder) {
        this.selectedOrder = selectedOrder;
    }

    public List<BankBean> getListBank() {
        return listBank;
    }

    public void setListBank(List<BankBean> listBank) {
        this.listBank = listBank;
    }

    public List<EsqTrxIdBean> getListTrxIdIndicator() {
        return listTrxIdIndicator;
    }

    public void setListTrxIdIndicator(List<EsqTrxIdBean> listTrxIdIndicator) {
        this.listTrxIdIndicator = listTrxIdIndicator;
    }

    public String getSelectedBankId() {
        return selectedBankId;
    }

    public void setSelectedBankId(String selectedBankId) {
        this.selectedBankId = selectedBankId;
    }

    public String getSelectedBankName() {
        return selectedBankName;
    }

    public void setSelectedBankName(String selectedBankName) {
        this.selectedBankName = selectedBankName;
    }

    public String getSelectedTrxIndicatorId() {
        return selectedTrxIndicatorId;
    }

    public void setSelectedTrxIndicatorId(String selectedTrxIndicatorId) {
        this.selectedTrxIndicatorId = selectedTrxIndicatorId;
    }

    public String getSelectedTrxIndicatorName() {
        return selectedTrxIndicatorName;
    }

    public void setSelectedTrxIndicatorName(String selectedTrxIndicatorName) {
        this.selectedTrxIndicatorName = selectedTrxIndicatorName;
    }

    public void setList(List<AlertThreshold> list) {
        this.list = list;
    }

    public List<AlertThreshold> getList() {
        return list;
    }
}

package com.rintis.marketing.web.jsf.main.dashboard;

import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.*;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@ManagedBean
@ViewScoped
public class AddUserDashboardManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private MktUserDashboard mktUserDashboard;
    private List<MktUserDashboardDetail> listDetail;
    private List<BankBean> listBank;
    private List<EsqTrxIdBean> listTrxId;
    private List<EsqTrxIdBean> listTrxIdIndicator;
    private String selectedBankId;
    private String selectedTrxId;
    private String selectedTransactionIndicatorId;
    private String selectedMonitoringInterval;
    private MktUserDashboardDetail selectedDeleteItem;
    private Integer sortidx;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            sortidx = 1;

            mktUserDashboard = new MktUserDashboard();
            listDetail = new ArrayList<>();

            listBank = moduleFactory.getMasterService().listBankCA(new HashMap<>());
            listTrxId = moduleFactory.getMasterService().listEsqTrxId();


            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void trxIdChangeListener() {
        try {
            if (selectedTrxId.equalsIgnoreCase(DataConstant.ALL)) {
                listTrxIdIndicator = moduleFactory.getMasterService().listEsqTrxIdTrxIndicator();
            } else {
                listTrxIdIndicator = moduleFactory.getMasterService().listEsqTrxIdTrxIndicator(selectedTrxId);
            }
            //listTrxIdIndicator = moduleFactory.getMasterService().listEsqTrxIdIndicator();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String addAction() {
        try {
            if (listDetail.size() == DataConstant.USER_DASHBOARD_MAX_CHART) {
                glowMessageError("Max " + Integer.toString(DataConstant.USER_DASHBOARD_MAX_CHART) + " charts", "");
                return "";
            }

            //validation
            if (selectedTrxId.length() == 0) {
                glowMessageError("Select Transaction", "");
                return "";
            }
            if (selectedTransactionIndicatorId.length() == 0) {
                glowMessageError("Select Transaction Indicator", "");
                return "";
            }

            //check already exist
            for(MktUserDashboardDetail d : listDetail) {
                if (d.getBankId().equalsIgnoreCase(selectedBankId) &&
                        d.getTransactionId().equalsIgnoreCase(selectedTrxId) &&
                        d.getTransactionIndicatorId().equalsIgnoreCase(selectedTransactionIndicatorId) &&
                        d.getMonitoringInterval().equalsIgnoreCase(selectedMonitoringInterval) ) {
                    glowMessageError("Item already exist", "");
                    return "";
                }
            }

            sortidx++;

            MktUserDashboardDetail detail = new MktUserDashboardDetail();
            detail.setBankId(selectedBankId);
            detail.setTransactionId(selectedTrxId);
            detail.setTransactionIndicatorId(selectedTransactionIndicatorId);
            detail.setSortIdx(sortidx);
            detail.setMonitoringInterval(selectedMonitoringInterval);

            //EisBank bank = moduleFactory.getMasterService().getBank(selectedBankId);
            //Modify by AAH 210222
            BankBean bank = moduleFactory.getMasterService().getBankCA(selectedBankId);
            detail.setBankName(bank.getBankname());

            if (selectedTrxId.equalsIgnoreCase(DataConstant.ALL)) {
                detail.setTransactionName("All");
            } else {
                EsqTrxIdBean tr = moduleFactory.getMasterService().getEsqTrxId(selectedTrxId);
                detail.setTransactionName(tr.getTrxname());
            }

            EsqTrxIdBean trind = moduleFactory.getMasterService().getEsqTrxIdIndicator(selectedTransactionIndicatorId);
            detail.setTransactionIndicatorName(trind.getTrxnameindicator());

            listDetail.add(detail);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String removeItemAction(int idx) {
        listDetail.remove(idx);
        return "";
    }

    public String saveAction() {
        try {
            //validation
            if (mktUserDashboard.getMenuName().length() == 0) {
                glowMessageError("Enter Menu Name", "");
                return "";
            }

            if (mktUserDashboard.getDescription().length() == 0) {
                glowMessageError("Enter Description", "");
                return "";
            }

            if (listDetail.size() == 0) {
                glowMessageError("Empty Detail", "");
                return "";
            }

            mktUserDashboard.setUserId(getUserInfoBean().getUserId());
            moduleFactory.getBiService().createUserDashboard(mktUserDashboard, listDetail, getUserInfoBean());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            glowMessageError("Save Dashboard Failed", "");
            return "";
        }
        return "listUserDashboard?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public MktUserDashboard getMktUserDashboard() {
        return mktUserDashboard;
    }

    public void setMktUserDashboard(MktUserDashboard mktUserDashboard) {
        this.mktUserDashboard = mktUserDashboard;
    }

    public List<MktUserDashboardDetail> getListDetail() {
        return listDetail;
    }

    public void setListDetail(List<MktUserDashboardDetail> listDetail) {
        this.listDetail = listDetail;
    }

    public List<BankBean> getListBank() {
        return listBank;
    }

    public void setListBank(List<BankBean> listBank) {
        this.listBank = listBank;
    }

    public List<EsqTrxIdBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<EsqTrxIdBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

    public List<EsqTrxIdBean> getListTrxIdIndicator() {
        return listTrxIdIndicator;
    }

    public void setListTrxIdIndicator(List<EsqTrxIdBean> listTrxIdIndicator) {
        this.listTrxIdIndicator = listTrxIdIndicator;
    }

    public String getSelectedBankId() {
        return selectedBankId;
    }

    public void setSelectedBankId(String selectedBankId) {
        this.selectedBankId = selectedBankId;
    }

    public String getSelectedTrxId() {
        return selectedTrxId;
    }

    public void setSelectedTrxId(String selectedTrxId) {
        this.selectedTrxId = selectedTrxId;
    }

    public String getSelectedTransactionIndicatorId() {
        return selectedTransactionIndicatorId;
    }

    public void setSelectedTransactionIndicatorId(String selectedTransactionIndicatorId) {
        this.selectedTransactionIndicatorId = selectedTransactionIndicatorId;
    }

    public MktUserDashboardDetail getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(MktUserDashboardDetail selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }

    public String getSelectedMonitoringInterval() {
        return selectedMonitoringInterval;
    }

    public void setSelectedMonitoringInterval(String selectedMonitoringInterval) {
        this.selectedMonitoringInterval = selectedMonitoringInterval;
    }

    public Integer getSortidx() {
        return sortidx;
    }

    public void setSortidx(Integer sortidx) {
        this.sortidx = sortidx;
    }
}

package com.rintis.marketing.web.jsf.main.tlog;

import com.rintis.marketing.beans.entity.UserLogin;
import com.rintis.marketing.beans.entity.UserLoginBank;
import com.rintis.marketing.beans.entity.UserLoginHistory;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.ParameterMapName;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ManagedBean
@ViewScoped
public class SearchUserLoginLogManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;

    private Date startDate;
    private Date endDate;
    private String userId;
    private List<UserLoginHistory> list;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        startDate = new Date();
        endDate = new Date();
        try {


            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    public String searchAction() {
        try {
            //check user
            //Remarked by AAH 191011
            /*UserLogin ul = moduleFactory.getLoginService().getUserLogin(userId.toUpperCase());
            if (ul == null) {
                UserLoginBank ulb = moduleFactory.getLoginService().getUserLoginBank(userId.toUpperCase());
                if (ulb == null) {
                    glowMessageError("UserId doesn't exist", "");
                    return "";
                }
            }*/

            Map<String, Object> param = new HashMap<>();
            param.put(ParameterMapName.PARAM_USERID, userId.toUpperCase());
            param.put(ParameterMapName.PARAM_STARTDATE, startDate);
            param.put(ParameterMapName.PARAM_ENDDATE, endDate);
            list = moduleFactory.getTransLogService().searchUserLoginLog(param);
            //log.info(list.size());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return "";
    }



    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<UserLoginHistory> getList() {
        return list;
    }

    public void setList(List<UserLoginHistory> list) {
        this.list = list;
    }
}


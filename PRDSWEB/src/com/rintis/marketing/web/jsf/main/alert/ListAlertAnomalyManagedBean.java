package com.rintis.marketing.web.jsf.main.alert;

import com.rintis.marketing.beans.entity.AlertAnomaly;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.constant.ParameterMapName;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ManagedBean
@ViewScoped
public class ListAlertAnomalyManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<AlertAnomaly> list;
    private String graphRefreshInterval;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            graphRefreshInterval = DataConstant.DEFAULT_GRAPH_REFRESH_INTERVAL;

            Map<String, Object> param = new HashMap<>();
            param.put(ParameterMapName.PARAM_STATUSID, DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
            list = moduleFactory.getBiService().searchAlertAnomaly(param);


            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void onPoolListener() {
        try {
            Map<String, Object> param = new HashMap<>();
            param.put(ParameterMapName.PARAM_STATUSID, DataConstant.ALERT_ANOMALY_STATUS__NOTSET);
            list = moduleFactory.getBiService().searchAlertAnomaly(param);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<AlertAnomaly> getList() {
        return list;
    }

    public void setList(List<AlertAnomaly> list) {
        this.list = list;
    }

    public String getGraphRefreshInterval() {
        return graphRefreshInterval;
    }

    public void setGraphRefreshInterval(String graphRefreshInterval) {
        this.graphRefreshInterval = graphRefreshInterval;
    }
}

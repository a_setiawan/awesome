package com.rintis.marketing.web.jsf.main.biller;

import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.beans.bean.biller.BillerAnomalyBean;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.beans.entity.BillerDummy;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.app.main.module.login.LoginService;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import org.apache.commons.lang.time.DateUtils;
import org.primefaces.model.SelectableDataModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.ListDataModel;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@ManagedBean
@ViewScoped
public class BillerInquiryManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<PanTrxidBean> listTrxId;
    private List<BillerAnomalyBean> list;
    private BillerAnomalyDataModel listBillerAnomalyDataModel;
    private List<BillerAnomalyBean> selectedAlertAnomaly;
    private BillerAnomalyBean[] selectedItems;
    private Date startDate;
    private Date endDate;
    private Date startTime;
    private Date endTime;
    private Date nowTime;
    private String transaction;
    private String periodRange;
    private String periodFr;
    private String periodTo;
    private String timeFr;
    private String timeTo;
    private String pan;
    private String roleID;
    private Boolean sender;
    private String period;
    private Boolean whitelist;
    private Boolean role;
    private Boolean roleBank;
    private Boolean disable;
    private Boolean enable3D;
    private Boolean disable3D;
    private boolean isUpdate;
    private String transTypeName = "";
    private String transType = "";
    private String periodType = "";
    private String periodF;
    private String periodT;

    public BillerInquiryManagedBean() {}

    @Override
    @PostConstruct
    protected void initManagedBean() {
        roleID = LoginService.getRoleID;
        try {
            menuId = "0527";
            pageTitle = getMenuName(moduleFactory, menuId);

            startDate = new Date();
            endDate = new Date();
            startTime = new Date();
            endTime = new Date();
            nowTime = new Date();

            startTime = DateUtils.setMinutes(startTime, 0);
            startTime = DateUtils.setHours(startTime, startTime.getHours() - 1);
            endTime = DateUtils.setMinutes(endTime, 0);

            SimpleDateFormat datefmt = new SimpleDateFormat("yyMMdd");
            periodRange = datefmt.format(startDate);

            period = "1H";
            transaction = "17";
            transType = transaction;
            disable = false;
            whitelist = false;

            listTrxId = moduleFactory.getBillerService().listTrxIdBiller("17, 37");

            if(Integer.parseInt(this.getRoleID()) == 99){
                role = true;
            } else {
                role = false;
            }

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            UserInfoBean userInfoBean = getUserInfoBean();
            String bank_id = "";
            if (userInfoBean.getUserLoginBank() != null) {
                bank_id = userInfoBean.getUserLoginBank().getBankId();
            }

            if (period.equals("1D") || period.equals("3D")){
                disable = false;
            } else {
                disable = true;
            }

            if(period.equals("3D")){
                enable3D = true;
                disable3D = false;
            }else{
                enable3D = false;
                disable3D = true;
            }

            SimpleDateFormat datefmt = new SimpleDateFormat("yyMMdd");
            SimpleDateFormat timefmt = new SimpleDateFormat("HHmm");
            periodFr = datefmt.format(startDate);
            periodTo = datefmt.format(endDate);
            timeFr = timefmt.format(startTime);
            timeTo = timefmt.format(endTime);

            if(timeTo.substring(0,2).equals("00")){
                timeTo = "24" + timeTo.substring(2,4);
            }

            sender = false;

            long numOfDays = getDifferenceDays(startDate, nowTime) + 1;
            long days = Integer.parseInt(periodRange) - Integer.parseInt(periodFr);

            boolean isValid = true;
            if (new SimpleDateFormat("ddMMyyyy").parse(periodFr).after(new SimpleDateFormat("ddMMyyyy").parse(periodTo))
                    || new SimpleDateFormat("HHmm").parse(timeFr).after(new SimpleDateFormat("HHmm").parse(timeTo))) {
                isValid = false;
            }

            if (numOfDays < 30 && isValid) {
                if(period.equals("1H")){
                    list = moduleFactory.getBillerService().listAnomalyBiller1H(periodFr, periodTo, timeFr, timeTo, transaction, whitelist, bank_id);
                    listBillerAnomalyDataModel = new BillerAnomalyDataModel(list);
                }else if(period.equals("3H")){
                    list = moduleFactory.getBillerService().listAnomalyBiller3H(periodFr, periodTo, timeFr, timeTo, transaction, whitelist, bank_id);
                    listBillerAnomalyDataModel = new BillerAnomalyDataModel(list);
                }else if(period.equals("6H")){
                    list = moduleFactory.getBillerService().listAnomalyBiller6H(periodFr, periodTo, timeFr, timeTo, transaction, whitelist, bank_id);
                    listBillerAnomalyDataModel = new BillerAnomalyDataModel(list);
                }else if(period.equals("1D")){
                    list = moduleFactory.getBillerService().listAnomalyBiller1D(periodFr, periodTo, transaction, whitelist, bank_id);
                    listBillerAnomalyDataModel = new BillerAnomalyDataModel(list);
                }else if(period.equals("3D")){
                    list = moduleFactory.getBillerService().listAnomalyBiller3D(periodFr, periodTo, transaction, whitelist, bank_id);
                    listBillerAnomalyDataModel = new BillerAnomalyDataModel(list);
                }

                if (listTrxId != null) {
                    List<PanTrxidBean> listTemp = listTrxId.stream()
                            .filter(x -> x.getTrxid().equals(transaction))
                            .collect(Collectors.toList());
                    if (listTemp != null) {
                        transTypeName = listTemp.get(0).getTrxname();
                    }
                }
                transType = transaction;
                periodType = period;
                periodF = periodFr;
                periodT = periodTo;
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String errorMsg() throws ParseException {
        long numOfDays = getDifferenceDays(startDate, nowTime) + 1;
        if (numOfDays > 7) {
            glowMessageError("Max 7 Days", "");
        }

        if (periodFr != null && periodTo != null) {
            if (new SimpleDateFormat("ddMMyyyy").parse(periodFr).after(new SimpleDateFormat("ddMMyyyy").parse(periodTo))) {
                glowMessageError("Tanggal awal harus lebih kecil dari tanggal akhir !", "");
            }
        } else if (timeFr != null && timeTo != null){
            if(new SimpleDateFormat("HHmm").parse(timeFr).after(new SimpleDateFormat("HHmm").parse(timeTo))) {
                glowMessageError("Jam awal harus lebih kecil dari jam akhir !", "");
            }
        }
        return "";
    }

    public void popupMsg() {
        if (isUpdate) {
            glowMessageInfo("Biller Special Treatment Has Been Saved", "");
        }
    }

    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public void onPoolAction() {
        submitAction();
    }

    public String saveItemAction() {
        try {
            String act = "true";
            isUpdate = false;
            BigDecimal freq = BigDecimal.valueOf(0);
            if (selectedItems == null) return "";
            for(BillerAnomalyBean aa : selectedItems) {
                boolean chk = moduleFactory.getBillerService().checkTresholdExist(aa.getInstCd().trim(), aa.getCustNo().trim());
                if (!chk) {
                    BillerDummy billerDummy = new BillerDummy();
                    billerDummy.setInstCd(aa.getInstCd());
                    billerDummy.setCustNo(aa.getCustNo());
                    billerDummy.setCustName(aa.getCustName());
                    billerDummy.setFreqApp(freq);
                    billerDummy.setFreqDc(freq);
                    billerDummy.setFreqDcFree(freq);
                    billerDummy.setTotalAmtApp(freq);
                    billerDummy.setTotalAmtDc(freq);
                    billerDummy.setTotalAmtDcFree(freq);
                    billerDummy.setTotalAmt(freq);
                    billerDummy.setActive(act);

                    moduleFactory.getBillerService().saveBillerTreshold(billerDummy);
                    isUpdate = true;
                }
            }

            if(isUpdate){
                submitAction();
            }

            selectedItems = null;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String paramEncode(String data){
        if(!data.equals(null)){
            data = HttpUtils.decodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<BillerAnomalyBean> getList() {
        return list;
    }

    public void setList(List<BillerAnomalyBean> list) {
        this.list = list;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public Boolean getWhitelist() {
        return whitelist;
    }

    public void setWhitelist(Boolean whitelist) {
        this.whitelist = whitelist;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPeriodRange() {
        return periodRange;
    }

    public void setPeriodRange(String periodRange) {
        this.periodRange = periodRange;
    }

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    public Boolean getRole(){
        return role;
    }

    public Boolean getSender(){
        return sender;
    }

    public void setRole(Boolean role){
        this.role = role;
    }

    public Boolean getRoleBank() {
        return roleBank;
    }

    public void setRoleBank(Boolean roleBank) {
        this.roleBank = roleBank;
    }

    public Boolean getDisable() {
        return disable;
    }

    public void setDisable(Boolean disable) {
        this.disable = disable;
    }

    public Boolean getEnable3D() {
        return enable3D;
    }

    public void setEnable3D(Boolean enable3D) {
        this.enable3D = enable3D;
    }

    public Boolean getDisable3D() {
        return disable3D;
    }

    public void setDisable3D(Boolean disable3D) {
        this.disable3D = disable3D;
    }

    public class BillerAnomalyDataModel extends ListDataModel<BillerAnomalyBean> implements SelectableDataModel<BillerAnomalyBean> {

        public BillerAnomalyDataModel(List<BillerAnomalyBean> list) {
            super(list);
        }

        @Override
        public BillerAnomalyBean getRowData(String rowKey) {
            List<BillerAnomalyBean> list = (List<BillerAnomalyBean>)getWrappedData();
            for(BillerAnomalyBean billerAnomaly : list) {
                String pp = billerAnomaly.getInstCd() + billerAnomaly.getCustNo() + billerAnomaly.getCustName();
                if (pp.equals(rowKey) ) {
                    return billerAnomaly;
                }
            }
            return null;
        }

        @Override
        public Object getRowKey(BillerAnomalyBean billerAnomaly) {
            return billerAnomaly.getInstCd() + billerAnomaly.getCustNo() + billerAnomaly.getCustName();
        }
    }

    public List<BillerAnomalyBean> getSelectedAnomaly() {
        return selectedAlertAnomaly;
    }

    public void setSelectedAnomaly(List<BillerAnomalyBean> selectedAlertAnomaly) {
        this.selectedAlertAnomaly = selectedAlertAnomaly;
    }

    public BillerAnomalyBean[] getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(BillerAnomalyBean[] selectedItems) {
        this.selectedItems = selectedItems;
    }

    public List<PanTrxidBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<PanTrxidBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

    public BillerAnomalyDataModel getListBillerAnomalyDataModel() {
        return listBillerAnomalyDataModel;
    }

    public void setListBillerAnomalyDataModel(BillerAnomalyDataModel listBillerAnomalyDataModel) {
        this.listBillerAnomalyDataModel = listBillerAnomalyDataModel;
    }

    public String getTransTypeName() {
        return transTypeName;
    }

    public void setTransTypeName(String transTypeName) {
        this.transTypeName = transTypeName;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getPeriodType() {
        return periodType;
    }

    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    public String getPeriodF() {
        return periodF;
    }

    public void setPeriodF(String periodF) {
        this.periodF = periodF;
    }

    public String getPeriodT() {
        return periodT;
    }

    public void setPeriodT(String periodT) {
        this.periodT = periodT;
    }
}
package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.PanAnomalyParamBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * Created by aabraham on 01/03/2019.
 */
@ManagedBean
@ViewScoped
public class AddPanAnomalyAggregatePerBankManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private String checkBankID;
    private PanAnomalyParamBean panAnomalyParamBean;
    private List<PanAnomalyParamBean> listParam;
    private ListPanAnomalyAggregatePerBankParameterManagedBean listPanAnomalyAggregatePerBankParameterManagedBean;
    String pBankId;
    @Override
    @PostConstruct
    protected void initManagedBean() {

        try {
            pBankId = getRequestParameterMap().get("pBankId").toString();
            listParam = moduleFactory.getPanService().listParamBank();
            checkBankID = null;

            if (checkBankID == null) {
                glowMessageError("Input bank terlebih dahulu sebelum add parameter", "");
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }


    }

    public String saveAction() {
        try {
            System.out.println(pBankId);
            String bank_id = pBankId;
            moduleFactory.getPanService().saveParamBank(listParam, bank_id);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_PARAM_PER_BANK_PARAM + "?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }
    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public PanAnomalyParamBean getPanAnomalyParamBean() {
        return panAnomalyParamBean;
    }
    public void setPanAnomalyParamBean(PanAnomalyParamBean panAnomalyParamBean) {
        this.panAnomalyParamBean = panAnomalyParamBean;
    }

    public List<PanAnomalyParamBean> getListParam() {
        return listParam;
    }

    public void setListParam(List<PanAnomalyParamBean> listParam) {
        this.listParam = listParam;
    }

    public void setListPanAnomalyAggregatePerBankParameterManagedBean(ListPanAnomalyAggregatePerBankParameterManagedBean listPanAnomalyAggregatePerBankParameterManagedBean) {
        this.listPanAnomalyAggregatePerBankParameterManagedBean = listPanAnomalyAggregatePerBankParameterManagedBean;
    }
}

package com.rintis.marketing.web.jsf.main.mcc;

import com.rintis.marketing.beans.bean.master.MCCParameterBean;
import com.rintis.marketing.beans.entity.MCCList;
import com.rintis.marketing.beans.entity.MCCParameter;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by aabraham on 22/04/2021.
 */
@ManagedBean
@ViewScoped
public class AddMCCParameterManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private MCCParameterBean mccParameterBean;

    private List<MCCParameter> listMCCParameter;
    private List<MCCList> listMCC;
    private String selectedMCCParameter;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listMCC = moduleFactory.getMasterService().listMcc("-1");
            listMCCParameter = moduleFactory.getMasterService().listMccParameter("-1");
            mccParameterBean = new MCCParameterBean();
            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String saveAction() {
        try {
            if (mccParameterBean.getMcc().equals("-1")) {
                glowMessageError("Enter MCC", "");
                return "";
            }

            boolean chk = moduleFactory.getMasterService().checkMccParameterUsed(mccParameterBean.getMcc());
            if (chk) {
                glowMessageError("MCC Parameter already exist", "");
                return "";
            }

            String mcc = mccParameterBean.getMcc();
            BigDecimal mintrxapp1d = mccParameterBean.getMin_trx_app_1d();
            BigDecimal minamtapp1d = mccParameterBean.getMin_amt_app_1d();
            BigDecimal mintrxtot1d = mccParameterBean.getMin_trx_tot_1d();
            BigDecimal minamttot1d = mccParameterBean.getMin_amt_tot_1d();
            moduleFactory.getMasterService().saveMccParameter(mcc, mintrxapp1d, mintrxtot1d, minamtapp1d, minamttot1d);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_MASTER_MCC_PARAMETER + "?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public MCCParameterBean getMccParameterBean() {
        return mccParameterBean;
    }

    public void setMccParameterBean(MCCParameterBean mccParameterBean) {
        this.mccParameterBean = mccParameterBean;
    }

    public List<MCCParameter> getListMCCParameter() {
        return listMCCParameter;
    }

    public void setListMCCParameter(List<MCCParameter> listMCCParameter) {
        this.listMCCParameter = listMCCParameter;
    }

    public String getSelectedMCCParameter() {
        return selectedMCCParameter;
    }

    public void setSelectedMCCParameter(String selectedMCCParameter) {
        this.selectedMCCParameter = selectedMCCParameter;
    }

    public List<MCCList> getListMCC() {
        return listMCC;
    }

    public void setListMCC(List<MCCList> listMCC) {
        this.listMCC = listMCC;
    }
}

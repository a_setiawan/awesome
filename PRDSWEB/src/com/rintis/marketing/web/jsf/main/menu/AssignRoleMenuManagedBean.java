package com.rintis.marketing.web.jsf.main.menu;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.rintis.marketing.beans.bean.menu.UserMenuBean;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;


import com.rintis.marketing.beans.entity.MenuRoleType;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.constant.DataConstant;

@ManagedBean
@ViewScoped
public class AssignRoleMenuManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private TreeNode rootMenu;
    private UserMenuBean selectedMenu;
    private List<MenuRoleType> listMenuRoleType;
    private Integer roleTypeId;
    private Integer selectedRoleTypeId;
    private String selectedRoleTypeName;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listMenuRoleType = moduleFactory.getMenuService().listMenuRoleType(null);
            for(int i=0; i<listMenuRoleType.size(); i++ ) {
                MenuRoleType mrt = (MenuRoleType)listMenuRoleType.get(i);
                if (mrt.getMenuRoleTypeId() == -1) {
                    listMenuRoleType.remove(i);
                    break;
                }
            }
            rootMenu = new DefaultTreeNode("root", null);


            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private TreeNode recurseMenu(TreeNode rootNode, String parentId) {
        try {
            List<UserMenuBean> listMenu = moduleFactory.getMenuService().listRoleAllMenu(roleTypeId, parentId, DataConstant.ACTIVE);
            //log.info(listMenu);
            for (UserMenuBean menu : listMenu) {
                TreeNode node = new DefaultTreeNode(menu, rootNode);
                node.setExpanded(true);
                recurseMenu(node, menu.getMenuid());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return rootNode;
    }
    
    public String submitAction() {
        try {
            selectedRoleTypeId = roleTypeId;
            MenuRoleType rt = moduleFactory.getMenuService().getMenuRoleType(roleTypeId);
            selectedRoleTypeName = rt.getMenuRoleName();
            rootMenu = new DefaultTreeNode("root", null);
            rootMenu = recurseMenu(rootMenu, null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }
    
    public void changeUserMenu(UserMenuBean userMenuBean) {
//        log.info(userMenuBean.getStatusMenu());
//        log.info(userMenuBean.getMenuId());
//        log.info(userMenuBean.getUserLoginId());
//        log.info(userMenuBean.isStatus());
        try {
            moduleFactory.getMenuService().updateRoleMenu(roleTypeId, userMenuBean.getMenuid(), userMenuBean.getStatusmenu());
            
            rootMenu = new DefaultTreeNode("root", null);
            rootMenu = recurseMenu(rootMenu, null);
        } catch(Exception e) {
            log.error(e.getMessage(), e);
        }
    } 
    

    
    public String selectAllAction() {
        try {
            if (rootMenu.getChildCount() == 0) {
                return "";
            }
            moduleFactory.getMenuService().selectAllRoleMenu(selectedRoleTypeId);
            rootMenu = new DefaultTreeNode("root", null);
            rootMenu = recurseMenu(rootMenu, null);
        } catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }
    
    public String clearAllAction() {
        try {
            if (rootMenu.getChildCount() == 0) {
                return "";
            }
            moduleFactory.getMenuService().clearAllRoleMenu(selectedRoleTypeId);
            rootMenu = new DefaultTreeNode("root", null);
            rootMenu = recurseMenu(rootMenu, null);
        } catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }
    

    
    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public TreeNode getRootMenu() {
        return rootMenu;
    }

    public void setRootMenu(TreeNode rootMenu) {
        this.rootMenu = rootMenu;
    }

    public UserMenuBean getSelectedMenu() {
        return selectedMenu;
    }

    public void setSelectedMenu(UserMenuBean selectedMenu) {
        this.selectedMenu = selectedMenu;
    }

    public Integer getRoleTypeId() {
        return roleTypeId;
    }

    public void setRoleTypeId(Integer roleTypeId) {
        this.roleTypeId = roleTypeId;
    }

    public Integer getSelectedRoleTypeId() {
        return selectedRoleTypeId;
    }

    public void setSelectedRoleTypeId(Integer selectedRoleTypeId) {
        this.selectedRoleTypeId = selectedRoleTypeId;
    }

    public String getSelectedRoleTypeName() {
        return selectedRoleTypeName;
    }

    public void setSelectedRoleTypeName(String selectedRoleTypeName) {
        this.selectedRoleTypeName = selectedRoleTypeName;
    }

    public List<MenuRoleType> getListMenuRoleType() {
        return listMenuRoleType;
    }

    public void setListMenuRoleType(List<MenuRoleType> listMenuRoleType) {
        this.listMenuRoleType = listMenuRoleType;
    }




}

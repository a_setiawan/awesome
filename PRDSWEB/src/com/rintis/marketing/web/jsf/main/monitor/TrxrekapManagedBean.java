package com.rintis.marketing.web.jsf.main.monitor;

import com.martinlinha.c3faces.model.C3DataSet;
import com.martinlinha.c3faces.model.C3DataSetString;
import com.martinlinha.c3faces.model.C3ViewDataSet;
import com.martinlinha.c3faces.model.C3ViewDataSetString;
import com.martinlinha.c3faces.script.property.Data;
import com.martinlinha.c3faces.script.property.Point;
import com.rintis.marketing.beans.bean.bi.C3ChartBean;
import com.rintis.marketing.beans.bean.bi.EsqTrx5MinBean;
import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.EisBank;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.MathUtils;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.*;

@ManagedBean
@ViewScoped
public class TrxrekapManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<BankBean> listBank;
    private List<EsqTrxIdBean> listTrxId;
    private String selectedAcqId;
    private String selectedIssId;
    private String selectedBnfId;
    private String selectedTrxId;
    private String selectedAcqName;
    private String selectedIssName;
    private String selectedBnfName;
    private String selectedTrxName;
    private String styleBnf;

    private String render;
    private boolean showTable;
    private String graphRefreshInterval;
    private int defaultRangeInHour;
    private Date thdate;
    private Date frdate;
    private Boolean showPercent;

    private List<EsqTrx5MinBean> list;

    C3ChartBean c3ChartBean;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0510";
            pageTitle = getMenuName(moduleFactory, menuId);

            styleBnf = "visibility: hidden;";
            Map<String, Object> paramBank = new HashMap<>();
            listBank = moduleFactory.getMasterService().listBank(paramBank);

            listTrxId = moduleFactory.getMasterService().listEsqTrxId();

            showPercent = false;
            showTable = false;
            render = "false";
            graphRefreshInterval = DataConstant.DEFAULT_GRAPH_REFRESH_INTERVAL;
            defaultRangeInHour = Integer.parseInt(
                    moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(), DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_HOURLY)
            );

            Date now = new Date();
            thdate = now;
            thdate = DateUtils.setDate(Calendar.MINUTE, thdate, 59);
            thdate = DateUtils.setDate(Calendar.SECOND, thdate, 59);

            frdate = DateUtils.addDate(Calendar.HOUR_OF_DAY, thdate, defaultRangeInHour*-1);
            frdate = DateUtils.addDate(Calendar.MINUTE, frdate, 0);
            frdate = DateUtils.setDate(Calendar.SECOND, frdate, 0);


        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            if (selectedTrxId.length() == 0) {
                glowMessageError("Select Transaction", "");
                return "";
            }

            if (selectedAcqId.length() == 0) {
                glowMessageError("Select Acquirer", "");
                return "";
            }

            if (selectedIssId.length() == 0) {
                glowMessageError("Select Issuer", "");
                return "";
            }

            if (selectedTrxId.equalsIgnoreCase("40") || selectedTrxId.equalsIgnoreCase("32")) {
                if (selectedBnfId.length() == 0) {
                    glowMessageError("Select Beneficiary", "");
                    return "";
                }
            }

            transactionChangeListener();

            EsqTrxIdBean trx = moduleFactory.getMasterService().getEsqTrxId(selectedTrxId);
            if (trx != null) {
                selectedTrxName = trx.getTrxname();
            }

            EisBank bank = moduleFactory.getMasterService().getBank(selectedAcqId);
            if (bank != null) {
                selectedAcqName = bank.getBankName();
            }
            bank = moduleFactory.getMasterService().getBank(selectedIssId);
            if (bank != null) {
                selectedIssName = bank.getBankName();
            }
            if (selectedTrxId.equalsIgnoreCase("40") || selectedTrxId.equalsIgnoreCase("32")) {
                bank = moduleFactory.getMasterService().getBank(selectedBnfId);
                if (bank != null) {
                    selectedBnfName = bank.getBankName();
                }
            } else {
                selectedBnfName = "-";
            }

            Date now = new Date();
            thdate = now;
            thdate = DateUtils.setDate(Calendar.MINUTE, thdate, 59);
            thdate = DateUtils.setDate(Calendar.SECOND, thdate, 59);

            frdate = DateUtils.addDate(Calendar.HOUR_OF_DAY, thdate, defaultRangeInHour*-1);
            frdate = DateUtils.addDate(Calendar.MINUTE, frdate, 0);
            frdate = DateUtils.setDate(Calendar.SECOND, frdate, 0);


            list = listDataTrx(frdate, thdate, selectedAcqId, selectedIssId, selectedBnfId, selectedTrxId);

            List<String> listDateString = new ArrayList<>();
            List<Integer> listDataTotalTransaksi = new ArrayList<>();
            List<Integer> listDataTotalApprove = new ArrayList<>();
            List<Integer> listDataTotalDeclineCharge = new ArrayList<>();
            List<Integer> listDataTotalDeclineFree = new ArrayList<>();

            List<BigDecimal> listDataPercentAppr = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclC = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclF = new ArrayList<>();

            for(int i=0; i < list.size() - 1; i++) {
                EsqTrx5MinBean esq = list.get(i);
                Date d = DateUtils.convertDate(esq.getPeriod() + " " + esq.getTimeto(), DateUtils.DATE_YYMMDD_HHMM);
                listDateString.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));

                BigDecimal b = MathUtils.checkNull(esq.getFreq_app())
                        .add(MathUtils.checkNull(esq.getFreq_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_df()));

                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_app()), b);
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_df(), b);

                listDataTotalTransaksi.add(b.intValue());
                listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_app()).intValue());
                listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_dc()).intValue());
                listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_df()).intValue());

                listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));
            }


            String title = "";//selectedBankName + " - " + selectedTrxName + " - " + selectedTransactionIndicatorName;


            String colorTotal = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_TOTAL);
            String colorApprove = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_APPR);
            String colorDc = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_DC);
            String colorDf = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_DF);

            if (!showPercent) {
                c3ChartBean = new C3ChartBean();
                generateChart(listDataTotalTransaksi,
                        listDataTotalApprove,
                        listDataTotalDeclineCharge,
                        listDataTotalDeclineFree,
                        listDateString,
                        colorTotal, colorApprove, colorDc, colorDf,
                        title,
                        DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                        DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                        c3ChartBean);


            } else {
                List<DsBean> listDsBean = new ArrayList<>();

                DsBean dsBean = new DsBean();
                dsBean.setListData(listDataPercentAppr);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclC);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclF);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                c3ChartBean = new C3ChartBean();
                generateChartMultiData(c3ChartBean, listDsBean, listDateString, title);

            }



            render = "true";


        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public void onPoolListener() {
        submitAction();
    }

    private List<EsqTrx5MinBean> listDataTrx(Date frdate, Date thdate, String acqId,
            String issId, String bnfId, String trxId) throws Exception {

        if (selectedTrxId.equalsIgnoreCase("40") || selectedTrxId.equalsIgnoreCase("32")) {

        } else {
            bnfId = "00";
        }

        List<EsqTrx5MinBean> list = new ArrayList<>();
        Date sd = frdate;
        //Date ed = thdate;
        int idx = 0;
        while (true) {
            if (idx == 287) {
                //log.info(idx);
            }
            String period = DateUtils.convertDate(sd, DateUtils.DATE_YYMMDD);
            int ihourfr = DateUtils.getHour(sd);
            Date ed = DateUtils.addDate(Calendar.HOUR_OF_DAY, sd, 1);
            int ihourto = DateUtils.getHour(ed);
            //int iminfr= DateUtils.getMinute(sd);
            //int iminto = DateUtils.getMinute(ed);
            //if (ihourto == 0 && iminto == 0) {
            //    ihourto = 24;
            //}
            //ihourto++;
            //String periodto = DateUtils.convertDate(ed, DateUtils.DATE_YYMMDD);
            //log.info(period + " " + ihourfr + ":" + ihourto );

            String shourfr = StringUtils.formatFixLength(Integer.toString(ihourfr), "0", 2) + ":00";
            String shourto = StringUtils.formatFixLength(Integer.toString(ihourto), "0", 2) + ":00";
            //String sminfr = StringUtils.formatFixLength(Integer.toString(ihourfr), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(iminfr), "0", 2);
            //String sminto = StringUtils.formatFixLength(Integer.toString(ihourfr), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(iminto), "0", 2);
            if (shourto.equalsIgnoreCase("00:00")) {
                shourto = "24:00";
            }
            EsqTrx5MinBean esqTrx5MinBean = moduleFactory.getBiService().getEsqTrxRekap(acqId, issId, bnfId, selectedTrxId,
                    period, shourfr, shourto);
            if (esqTrx5MinBean == null) {
                esqTrx5MinBean = new EsqTrx5MinBean();
                esqTrx5MinBean.setPeriod(period);
                esqTrx5MinBean.setTrxid(selectedTrxId);
                esqTrx5MinBean.setTimefr(shourfr);
                esqTrx5MinBean.setTimeto(shourto);

            }
            list.add(esqTrx5MinBean);

            if (ed.getTime() >= thdate.getTime()) {
                break;
            }

            sd = DateUtils.addDate(Calendar.HOUR_OF_DAY, sd, 1);
            idx++;
        };

        return list;
    }

    private void generateChart(List<Integer> listDataTotalTransaksi,
                               List<Integer> listDataTotalApprove,
                               List<Integer> listDataTotalDeclineCharge,
                               List<Integer> listDataTotalDeclineFree,
                               List<String> listDateString,
                               String colorTotal, String colorApprove,
                               String colorDc, String colorDf, String title,
                               String dsNameTotal, String dsNameAppr, String dsNameDc, String dsNameDf,
                               C3ChartBean c3ChartBean) {
        C3DataSet dsTotal = new C3DataSet(listDataTotalTransaksi);
        C3ViewDataSet dataSetTotal = new C3ViewDataSet(dsNameTotal, dsTotal, "#"+colorTotal);

        C3DataSet dsAppr = new C3DataSet(listDataTotalApprove);
        C3ViewDataSet dataSetAppr = new C3ViewDataSet(dsNameAppr, dsAppr, "#"+colorApprove);

        C3DataSet dsDc = new C3DataSet(listDataTotalDeclineCharge);
        C3ViewDataSet dataSetDc = new C3ViewDataSet(dsNameDc, dsDc, "#"+colorDc);

        C3DataSet dsDf = new C3DataSet(listDataTotalDeclineFree);
        C3ViewDataSet dataSetDf = new C3ViewDataSet(dsNameDf, dsDf, "#"+colorDf);

        C3DataSetString dsDateString = new C3DataSetString(listDateString);
        C3ViewDataSetString dataSetDateString = new C3ViewDataSetString("x", dsDateString, false);

        Data data = new Data();
        data.getDataSetsString().add(dataSetDateString);
        data.getDataSets().add(dataSetTotal);
        data.getDataSets().add(dataSetAppr);
        data.getDataSets().add(dataSetDc);
        data.getDataSets().add(dataSetDf);

        data.setX("z1");
        data.setxFormat("%Y-%m-%d %H:%M");

        com.martinlinha.c3faces.script.property.Axis axis = new com.martinlinha.c3faces.script.property.Axis();
        axis.setHeightX(70);
        axis.setTypeX(com.martinlinha.c3faces.script.property.Axis.Type.TIMESERIES);
        axis.setTickX("fit: true,\n" +
                "format: '%d/%m %H:%M',\n" +
                "rotate: -45,\n" +
                "multiline: false,\n" +
                "culling: true");
        axis.setMinY(0);

        Point point = new Point();
        point.setShow(true);

        c3ChartBean.setData(data);
        c3ChartBean.setAxis(axis);
        c3ChartBean.setPoint(point);
        //c3ChartBean.setPadding(padding);
        c3ChartBean.setTitle(title);
    }

    private void generateChartMultiData(C3ChartBean c3ChartBean, List<DsBean> listData, List<String> listDateString,
                                        String title) {
        List<C3ViewDataSet> listDataSet = new ArrayList<>();

        for(DsBean dsBean : listData) {
            C3DataSet ds = new C3DataSet(dsBean.getListData());
            C3ViewDataSet dataSet = new C3ViewDataSet(dsBean.getName(), ds, "#"+dsBean.getColor());
            listDataSet.add(dataSet);
        }

        C3DataSetString dsDateString = new C3DataSetString(listDateString);
        C3ViewDataSetString dataSetDateString = new C3ViewDataSetString("x", dsDateString, false);

        Data data = new Data();
        data.getDataSetsString().add(dataSetDateString);
        for(C3ViewDataSet ds : listDataSet) {
            data.getDataSets().add(ds);
        }

        data.setX("z1");
        data.setxFormat("%Y-%m-%d %H:%M");

        com.martinlinha.c3faces.script.property.Axis axis = new com.martinlinha.c3faces.script.property.Axis();
        axis.setHeightX(70);
        axis.setTypeX(com.martinlinha.c3faces.script.property.Axis.Type.TIMESERIES);
        axis.setTickX("fit: true,\n" +
                "format: '%d/%m %H:%M',\n" +
                "rotate: -45,\n" +
                "multiline: false,\n" +
                "culling: true");
        axis.setMinY(0);

        Point point = new Point();
        point.setShow(true);

        //Padding padding = new Padding();

        c3ChartBean.setData(data);
        c3ChartBean.setAxis(axis);
        c3ChartBean.setPoint(point);
        //c3ChartBean.setPadding(padding);
        c3ChartBean.setTitle(title);
    }

    public void transactionChangeListener() {
        if (selectedTrxId.equalsIgnoreCase("40") || selectedTrxId.equalsIgnoreCase("32")) {
            styleBnf = "visibility: visible;";
        } else {
            styleBnf = "visibility: hidden;";
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<BankBean> getListBank() {
        return listBank;
    }

    public void setListBank(List<BankBean> listBank) {
        this.listBank = listBank;
    }

    public List<EsqTrxIdBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<EsqTrxIdBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

    public String getSelectedAcqId() {
        return selectedAcqId;
    }

    public void setSelectedAcqId(String selectedAcqId) {
        this.selectedAcqId = selectedAcqId;
    }

    public String getSelectedIssId() {
        return selectedIssId;
    }

    public void setSelectedIssId(String selectedIssId) {
        this.selectedIssId = selectedIssId;
    }

    public String getSelectedBnfId() {
        return selectedBnfId;
    }

    public void setSelectedBnfId(String selectedBnfId) {
        this.selectedBnfId = selectedBnfId;
    }

    public String getSelectedTrxId() {
        return selectedTrxId;
    }

    public void setSelectedTrxId(String selectedTrxId) {
        this.selectedTrxId = selectedTrxId;
    }

    public String getSelectedAcqName() {
        return selectedAcqName;
    }

    public void setSelectedAcqName(String selectedAcqName) {
        this.selectedAcqName = selectedAcqName;
    }

    public String getSelectedIssName() {
        return selectedIssName;
    }

    public void setSelectedIssName(String selectedIssName) {
        this.selectedIssName = selectedIssName;
    }

    public String getSelectedBnfName() {
        return selectedBnfName;
    }

    public void setSelectedBnfName(String selectedBnfName) {
        this.selectedBnfName = selectedBnfName;
    }

    public String getStyleBnf() {
        return styleBnf;
    }

    public void setStyleBnf(String styleBnf) {
        this.styleBnf = styleBnf;
    }

    public String getRender() {
        return render;
    }

    public void setRender(String render) {
        this.render = render;
    }

    public boolean isShowTable() {
        return showTable;
    }

    public void setShowTable(boolean showTable) {
        this.showTable = showTable;
    }

    public String getGraphRefreshInterval() {
        return graphRefreshInterval;
    }

    public void setGraphRefreshInterval(String graphRefreshInterval) {
        this.graphRefreshInterval = graphRefreshInterval;
    }

    public int getDefaultRangeInHour() {
        return defaultRangeInHour;
    }

    public void setDefaultRangeInHour(int defaultRangeInHour) {
        this.defaultRangeInHour = defaultRangeInHour;
    }

    public Date getThdate() {
        return thdate;
    }

    public void setThdate(Date thdate) {
        this.thdate = thdate;
    }

    public Date getFrdate() {
        return frdate;
    }

    public void setFrdate(Date frdate) {
        this.frdate = frdate;
    }

    public Boolean getShowPercent() {
        return showPercent;
    }

    public void setShowPercent(Boolean showPercent) {
        this.showPercent = showPercent;
    }

    public List<EsqTrx5MinBean> getList() {
        return list;
    }

    public void setList(List<EsqTrx5MinBean> list) {
        this.list = list;
    }

    public C3ChartBean getC3ChartBean() {
        return c3ChartBean;
    }

    public void setC3ChartBean(C3ChartBean c3ChartBean) {
        this.c3ChartBean = c3ChartBean;
    }

    public String getSelectedTrxName() {
        return selectedTrxName;
    }

    public void setSelectedTrxName(String selectedTrxName) {
        this.selectedTrxName = selectedTrxName;
    }

    public class DsBean {
        private List<? extends Number> listData;
        private String color;
        private String name;

        public List<? extends Number> getListData() {
            return listData;
        }

        public void setListData(List<? extends Number> listData) {
            this.listData = listData;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }


}

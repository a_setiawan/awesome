package com.rintis.marketing.web.jsf.main.user;

import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.MenuRoleType;
import com.rintis.marketing.beans.entity.UserLogin;
import com.rintis.marketing.beans.entity.UserLoginBank;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.common.ParamConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ManagedBean
@ViewScoped
public class EditUserBankManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<BankBean> listBank;
    private List<MenuRoleType> listRole;
    private UserLoginBank userLogin;

    private String message;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        message = "";
        String userId = (String)getRequestParameterMap().get(ParamConstant.PARAM_USERID);
        //log.info(userId);
        try {
            listRole = moduleFactory.getMenuService().listMenuRoleType(DataConstant.MENU_ROLE_TYPE_NOT_HIDDEN);
            Map<String, Object> paramBank = new HashMap<>();
            listBank = moduleFactory.getMasterService().listBankCA(paramBank);



            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        if (userId != null) {
            try {
                userLogin = moduleFactory.getLoginService().getUserLoginBank(userId);
                //log.info(userLogin);
                if (userLogin == null) {
                    message = "Data not available";
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public String editUserAction() {
        try {
            if (userLogin.getUserLoginId().length() == 0) {
                glowMessageError("Enter User Id", "");
                return "";
            }
            if (userLogin.getUserName().length() == 0) {
                glowMessageError("Enter Name", "");
                return "";
            }
            if (userLogin.getRoleTypeId() == -1) {
                glowMessageError("Select Role Access", "");
                return "";
            }
            if (userLogin.getBankId().length() == 0) {
                glowMessageError("Select Bank", "");
                return "";
            }
            if (userLogin.getCurrentPassword().length() == 0) {
                glowMessageError("Enter Password", "");
                return "";
            }

            moduleFactory.getLoginService().updateUserLoginBank(userLogin);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "/apps/user/listUserBank?faces-redirect=true";
    }

    public String resetPasswordAction() {
        try {
            String s = moduleFactory.getLoginService().resetPasswordBank(userLogin.getUserLoginId(), getUserInfoBean().getUserId());

            glowMessageInfo("Reset Password success<br/>New Password " + s , "");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            glowMessageError("Reset Password failed", "");
        }
        return "";//PageConstant.PAGE_LIST_USER + "?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<MenuRoleType> getListRole() {
        return listRole;
    }

    public void setListRole(List<MenuRoleType> listRole) {
        this.listRole = listRole;
    }

    public UserLoginBank getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(UserLoginBank userLogin) {
        this.userLogin = userLogin;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<BankBean> getListBank() {
        return listBank;
    }

    public void setListBank(List<BankBean> listBank) {
        this.listBank = listBank;
    }
}

package com.rintis.marketing.web.jsf.main.bin;

import com.rintis.marketing.beans.bean.master.BinBean;
import com.rintis.marketing.beans.entity.Bin;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import org.hibernate.SessionFactory;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.util.List;

@ManagedBean
@ViewScoped
public class ListBinManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private SessionFactory sessionFactory;
    private List<BinBean> listBin;
    private BinBean selectedDeleteItem;

    public ListBinManagedBean() {}

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0413";
            pageTitle = getMenuName(moduleFactory, menuId);
            listBin = moduleFactory.getMasterService().binList();

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    public void deleteMccActionListener(ActionEvent event) {
        try {
            moduleFactory.getMasterService().deleteBin(selectedDeleteItem.getBankId(), selectedDeleteItem.getBinNo(), selectedDeleteItem.getBinLength());
            listBin = moduleFactory.getMasterService().binList();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<BinBean> getListBin() {
        return listBin;
    }

    public void setListBin(List<BinBean> listBin) {
        this.listBin = listBin;
    }

    public BinBean getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(BinBean selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }
}

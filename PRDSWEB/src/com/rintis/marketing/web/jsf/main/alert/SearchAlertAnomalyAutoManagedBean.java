package com.rintis.marketing.web.jsf.main.alert;

import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.AlertAnomaly;
import com.rintis.marketing.beans.entity.EisBank;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.constant.ParameterMapName;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import org.primefaces.model.SelectableDataModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.ListDataModel;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ManagedBean
@ViewScoped
public class SearchAlertAnomalyAutoManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<AlertAnomaly> list;
    private AlertAnomalyDataModel listAlertAnomalyDataModel;
    private AlertAnomaly selectedAlertAnomaly;
    private List<BankBean> listBank;
    private List<EsqTrxIdBean> listTransaction;
    private String selectedBankId;
    private String selectedBankName;
    private String selectedTransactionId;
    private String selectedTransactionName;
    private Date startDate;
    private Date endDate;
    private Integer selectedStatusId;
    private AlertAnomaly[] selectedItems;
    private String renderAckAll;
    private String refreshInterval;


    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0504";
            pageTitle = getMenuName(moduleFactory, menuId);

            selectedStatusId = 0;
            startDate = new Date();
            endDate = new Date();
            renderAckAll = "false";

            if (getUserInfoBean().checkRoleAdmin()) {
                renderAckAll = "true";
            }

            listBank = moduleFactory.getMasterService().listBank(new HashMap<>());
            listTransaction = moduleFactory.getMasterService().listEsqTrxId();

            refreshInterval = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_REFRESH_INTERVAL_TABLE_ALERT);

            submitAction();

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            if (StringUtils.checkNull(selectedBankId).length() > 0) {
                EisBank bank = moduleFactory.getMasterService().getBank(selectedBankId);
                selectedBankName = bank.getBankName();
            } else {
                selectedBankName = "All";
            }

            if (StringUtils.checkNull(selectedTransactionId).length() > 0) {
                EsqTrxIdBean trx = moduleFactory.getMasterService().getEsqTrxId(selectedTransactionId);
                selectedTransactionName = trx.getTrxname();
            } else {
                selectedTransactionName = "All";
            }

            startDate = DateUtils.getStartOfDay(startDate);
            endDate = DateUtils.getEndOfDay235959(endDate);

            Map<String, Object> param = new HashMap<>();
            param.put(ParameterMapName.PARAM_BANKID, selectedBankId);
            param.put(ParameterMapName.PARAM_TRANSACTIONID, selectedTransactionId );
            param.put(ParameterMapName.PARAM_STARTDATE, startDate);
            param.put(ParameterMapName.PARAM_ENDDATE, endDate);
            param.put(ParameterMapName.PARAM_STATUSID, selectedStatusId);
            list = moduleFactory.getBiService().searchAlertAnomaly(param);

            listAlertAnomalyDataModel = new AlertAnomalyDataModel(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public void onPoolAction() {
        submitAction();
    }

    public String normalAction() {
        try {
            moduleFactory.getBiService().alertAnomalySetStatus(selectedAlertAnomaly.getId(), DataConstant.ALERT_ANOMALY_STATUS__NORMAL);
            submitAction();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String notNormalAction() {
        try {
            moduleFactory.getBiService().alertAnomalySetStatus(selectedAlertAnomaly.getId(), DataConstant.ALERT_ANOMALY_STATUS__ABNORMAL);
            submitAction();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String removeAllItemAction() {
        try {
            startDate = DateUtils.getStartOfDay(startDate);
            endDate = DateUtils.getEndOfDay235959(endDate);

            Map<String, Object> param = new HashMap<>();
            param.put(ParameterMapName.PARAM_BANKID, selectedBankId);
            param.put(ParameterMapName.PARAM_TRANSACTIONID, selectedTransactionId );
            param.put(ParameterMapName.PARAM_STARTDATE, startDate);
            param.put(ParameterMapName.PARAM_ENDDATE, endDate);
            param.put(ParameterMapName.PARAM_STATUSID, selectedStatusId);
            list = moduleFactory.getBiService().searchAlertAnomaly(param);

            for(AlertAnomaly aa : list) {
                moduleFactory.getBiService().alertAnomalySetStatus(aa.getId(), DataConstant.ALERT_ANOMALY_STATUS__ABNORMAL);
            }
            submitAction();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String removeItemAction() {
        try {
            if (selectedItems == null) return "";
            for(AlertAnomaly aa : selectedItems) {
                moduleFactory.getBiService().alertAnomalySetStatus(aa.getId(), DataConstant.ALERT_ANOMALY_STATUS__ABNORMAL);
            }
            submitAction();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<AlertAnomaly> getList() {
        return list;
    }

    public void setList(List<AlertAnomaly> list) {
        this.list = list;
    }

    public List<BankBean> getListBank() {
        return listBank;
    }

    public void setListBank(List<BankBean> listBank) {
        this.listBank = listBank;
    }

    public List<EsqTrxIdBean> getListTransaction() {
        return listTransaction;
    }

    public void setListTransaction(List<EsqTrxIdBean> listTransaction) {
        this.listTransaction = listTransaction;
    }

    public String getSelectedBankId() {
        return selectedBankId;
    }

    public void setSelectedBankId(String selectedBankId) {
        this.selectedBankId = selectedBankId;
    }

    public String getSelectedBankName() {
        return selectedBankName;
    }

    public void setSelectedBankName(String selectedBankName) {
        this.selectedBankName = selectedBankName;
    }

    public String getSelectedTransactionId() {
        return selectedTransactionId;
    }

    public void setSelectedTransactionId(String selectedTransactionId) {
        this.selectedTransactionId = selectedTransactionId;
    }

    public String getSelectedTransactionName() {
        return selectedTransactionName;
    }

    public void setSelectedTransactionName(String selectedTransactionName) {
        this.selectedTransactionName = selectedTransactionName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public AlertAnomaly getSelectedAlertAnomaly() {
        return selectedAlertAnomaly;
    }

    public void setSelectedAlertAnomaly(AlertAnomaly selectedAlertAnomaly) {
        this.selectedAlertAnomaly = selectedAlertAnomaly;
    }

    public Integer getSelectedStatusId() {
        return selectedStatusId;
    }

    public void setSelectedStatusId(Integer selectedStatusId) {
        this.selectedStatusId = selectedStatusId;
    }

    public AlertAnomaly[] getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(AlertAnomaly[] selectedItems) {
        this.selectedItems = selectedItems;
    }

    public AlertAnomalyDataModel getListAlertAnomalyDataModel() {
        return listAlertAnomalyDataModel;
    }

    public void setListAlertAnomalyDataModel(AlertAnomalyDataModel listAlertAnomalyDataModel) {
        this.listAlertAnomalyDataModel = listAlertAnomalyDataModel;
    }

    public String getRenderAckAll() {
        return renderAckAll;
    }

    public void setRenderAckAll(String renderAckAll) {
        this.renderAckAll = renderAckAll;
    }

    public String getRefreshInterval() {
        return refreshInterval;
    }

    public void setRefreshInterval(String refreshInterval) {
        this.refreshInterval = refreshInterval;
    }

    public class AlertAnomalyDataModel extends ListDataModel<AlertAnomaly> implements SelectableDataModel<AlertAnomaly> {

        public AlertAnomalyDataModel(List<AlertAnomaly> list) {
            super(list);
        }

        @Override
        public AlertAnomaly getRowData(String rowKey) {
            List<AlertAnomaly> list = (List<AlertAnomaly>)getWrappedData();
            for(AlertAnomaly alertAnomaly : list) {
                if (alertAnomaly.getId().toString().equals(rowKey) ) {
                    return alertAnomaly;
                }
            }
            return null;
        }

        @Override
        public Object getRowKey(AlertAnomaly alertAnomaly) {
            return alertAnomaly.getId();
        }

    }

}

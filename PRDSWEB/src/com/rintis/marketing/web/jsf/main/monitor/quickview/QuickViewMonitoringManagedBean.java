package com.rintis.marketing.web.jsf.main.monitor.quickview;

import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.TmsUserQuickView;
import com.rintis.marketing.beans.entity.TmsUserQuickViewPk;
import com.rintis.marketing.core.app.main.dao.MasterDao;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.jsf.main.monitor.AbstractQuickViewMonitoring;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;

@ManagedBean
@ViewScoped
public class QuickViewMonitoringManagedBean extends AbstractQuickViewMonitoring {

    private List<BankBean> listBank;
    private String render;
    private List<EsqTrxIdBean> listTrxId;
    private String graphRefreshInterval;
    private boolean showTable;
    private String showPercentButtonText;
    private boolean saveSelection;
    private MasterDao masterDao;

    private boolean triggerFromPoll;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0517";
            pageTitle = getMenuName(moduleFactory, menuId);

            triggerFromPoll = false;
            saveSelection = false;

            String s = StringUtils.checkNull((String)getRequestParameterMap().get("bid"));
            if (s.length() > 0) {
                selectedBankId = s;
            }

            s = StringUtils.checkNull((String)getRequestParameterMap().get("trxid"));
            if (s.length() > 0) {
                selectedTrxId = s;
            }

            s = StringUtils.checkNull((String)getRequestParameterMap().get("tf"));
            if (s.length() > 0) {
                aggregateTimeFrame = s;
                if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T5M)) {
                    timeFrame = TIMEFRAME_5M;
                    defaultRangeInHour = Integer.parseInt(
                            moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                    DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_5M)
                    );
                } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T15M)) {
                    timeFrame = TIMEFRAME_15M;
                    defaultRangeInHour = Integer.parseInt(
                            moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                    DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_15M)
                    );
                } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T30M)) {
                    timeFrame = TIMEFRAME_30M;
                    defaultRangeInHour = Integer.parseInt(
                            moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                    DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_30M)
                    );
                } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T1H)) {
                    timeFrame = TIMEFRAME_1H;
                    defaultRangeInHour = Integer.parseInt(
                            moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                    DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_HOURLY)
                    );
                } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T1D)) {
                    timeFrame = TIMEFRAME_1D;
                    defaultRangeInHour = Integer.parseInt(
                            moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                    DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_1D)
                    );
                }
            } else {
                aggregateTimeFrame = DataConstant.T5M;
                timeFrame = TIMEFRAME_5M;
                defaultRangeInHour = Integer.parseInt(
                        moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_5M)
                );
            }

            //activeCurr = new Boolean[4];
            activePrev = new Boolean[4];
            for(int i=0; i<4; i++) {
                //activeCurr[i] = true;
                activePrev[i] = true;
            }
            renderPrevDate = "";

            //checkAllCurr = false;
            checkAllPrev = false;



            history = false;
            renderResult = "false";
            showTable = false;
            graphRefreshInterval = DataConstant.DEFAULT_GRAPH_REFRESH_INTERVAL;

            render = "false";
            renderAcqOnly = "false";
            renderIssOnly = "false";
            renderBnfOnly = "false";
            renderAcqIss = "false";
            renderAcqBnf = "false";
            renderIssBnf = "false";
            showPercent = false;
            showPercentButtonText = "Show Percent";


            Map<String, Object> paramBank = new HashMap<>();
            listBank = moduleFactory.getMasterService().listBankCA(paramBank);

            listTrxId = moduleFactory.getMasterService().listEsqTrxId();

            /*if (StringUtils.checkNull(selectedBankId).length() > 0 &&
                    StringUtils.checkNull(selectedTrxId).length() > 0 &&
                    StringUtils.checkNull(aggregateTimeFrame).length() > 0
                    ) {
                submitAction();
            }*/

            if (!isPostback()) {
                selectedPrevDate = DateUtils.addDayDate(new Date(), -1);
                createTlog(moduleFactory);
            }

            if (StringUtils.checkNull(selectedBankId).length() > 0 &&
                    StringUtils.checkNull(selectedTrxId).length() > 0 &&
                    StringUtils.checkNull(aggregateTimeFrame).length() > 0
                    ) {
                submitAction();
            } /*else {
                TmsUserQuickViewPk pk = new TmsUserQuickViewPk();
                pk.setUserId(getUserInfoBean().getUserId());
                pk.setBankId(selectedBankId);
                pk.setTrxId(selectedTrxId);
                pk.setTimeframe(aggregateTimeFrame);
                TmsUserQuickView uq = moduleFactory.getMasterService().getUserQuickview(pk);
                if (uq != null) {
                    selectedBankId = uq.getBankId();
                    selectedTrxId = uq.getTrxId();
                    aggregateTimeFrame = uq.getTimeframe();
                }
            }*/

            listUserQuickView(getUserInfoBean().getUserId());

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }



    public String submitAction() {
        try {
            trxIdChangeListener();

            if (selectedBankId.length() == 0) {
                glowMessageError("Select Bank", "");
                return "";
            }
            if (selectedTrxId.length() == 0) {
                glowMessageError("Select Transaction", "");
                return "";
            }
            if (selectedTransactionIndicatorId == null) {
                glowMessageError("Select Transaction Indicator", "");
                return "";
            }
            if (selectedTransactionIndicatorId.length == 0) {
                glowMessageError("Select Transaction Indicator", "");
                return "";
            }

            if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T5M)) {
                timeFrame = TIMEFRAME_5M;
                defaultRangeInHour = Integer.parseInt(
                        moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_5M)
                );
            } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T15M)) {
                timeFrame = TIMEFRAME_15M;
                defaultRangeInHour = Integer.parseInt(
                        moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_15M)
                );
            } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T30M)) {
                timeFrame = TIMEFRAME_30M;
                defaultRangeInHour = Integer.parseInt(
                        moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_30M)
                );
            } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T1H)) {
                timeFrame = TIMEFRAME_1H;
                defaultRangeInHour = Integer.parseInt(
                        moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_HOURLY)
                );
            } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T1D)) {
                timeFrame = TIMEFRAME_1D;
                defaultRangeInHour = Integer.parseInt(
                        moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_1D)
                );
            }

            if (saveSelection) {
                TmsUserQuickViewPk pk = new TmsUserQuickViewPk();
                pk.setUserId(getUserInfoBean().getUserId());
                pk.setBankId(selectedBankId);
                pk.setTrxId(selectedTrxId);
                pk.setTimeframe(aggregateTimeFrame);
                TmsUserQuickView uq = new TmsUserQuickView();
                uq.setTmsUserQuickViewPk(pk);
                uq.setQuickDate(new Date());
                if (activePrev[0]) {
                    uq.setCbTot(1);
                } else {
                    uq.setCbTot(0);
                }
                if (activePrev[1]) {
                    uq.setCbApp(1);
                } else {
                    uq.setCbApp(0);
                }
                if (activePrev[2]) {
                    uq.setCbDc(1);
                } else {
                    uq.setCbDc(0);
                }
                if (activePrev[3]) {
                    uq.setCbDf(1);
                } else {
                    uq.setCbDf(0);
                }
                boolean result = moduleFactory.getMasterService().updateUserQuickView(uq);
                if (result) {
                    //log.info(result);
                    //show message box
                    if (!triggerFromPoll) {
                        log.info("show box");
                        glowMessageInfo("Current selection is already in selection history", "");
                    }
                }
            }

            processSubmit();

            render = "true";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String submitAction2() {
        try {
            processSubmit();

            render = "true";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String showPercentChangeListener() {
        return "quickViewMonitoringPercent?faces-redirect=true&bid=" +
                StringUtils.checkNull(selectedBankId) + "&trxid=" + StringUtils.checkNull(selectedTrxId) +
                "&tf=" + StringUtils.checkNull(aggregateTimeFrame);
    }

    public String showHistory() {
        return "quickViewMonitoringHistory?faces-redirect=true&bid=" +
                StringUtils.checkNull(selectedBankId) + "&trxid=" + StringUtils.checkNull(selectedTrxId) +
                "&tf=" + StringUtils.checkNull(aggregateTimeFrame);
    }





    public void onPoolListener() {
        try {
            triggerFromPoll = true;

            String s = moduleFactory.getAggregateService().getAggrLastTime(selectedTrxId, aggregateTimeFrame);
            Date thdate = DateUtils.convertDate(s, DateUtils.DATE_YYMMDD_HHMM);
            selectedEndDate = thdate;

            selectedEndHour = DateUtils.getHour(thdate);
            selectedEndMinute = DateUtils.getMinute(thdate);

            Date frdate = DateUtils.addDate(Calendar.HOUR_OF_DAY, thdate, defaultRangeInHour*-1);

            selectedStartDate = frdate;
            selectedStartHour = DateUtils.getHour(frdate);
            selectedStartMinute = DateUtils.getMinute(frdate);


            submitAction();

            triggerFromPoll = false;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }







    /*public void checkAllCurrListener() {
        for(int i=0; i < activeCurr.length; i++) {
            if (checkAllCurr) {
                activeCurr[i] = true;
            } else {
                activeCurr[i] = false;
            }
        }
    }

    public void checkAllPrevListener() {
        for(int i=0; i < activePrev.length; i++) {
            if (checkAllPrev) {
                activePrev[i] = true;
            } else {
                activePrev[i] = false;
            }
        }
    }*/

    public void checkPrevListener() {
        /*if (activePrev[0] || activePrev[1] || activePrev[2] || activePrev[3]) {
            renderPrevDate = "";
        } else {
            renderPrevDate = "visibility: hidden;";
        }*/
    }


    public void quickViewChangeListener() {
        StringTokenizer st = new StringTokenizer(selectedUserQuickView, ",");
        selectedBankId = st.nextToken();
        selectedTrxId = st.nextToken();
        aggregateTimeFrame = st.nextToken();
        String cbtot = st.nextToken();
        String cbapp = st.nextToken();
        String cbdc = st.nextToken();
        String cbdf = st.nextToken();

        if (cbtot.equalsIgnoreCase("1")) {
            activePrev[0] = true;
        } else {
            activePrev[0] = false;
        }

        if (cbapp.equalsIgnoreCase("1")) {
            activePrev[1] = true;
        } else {
            activePrev[1] = false;
        }

        if (cbdc.equalsIgnoreCase("1")) {
            activePrev[2] = true;
        } else {
            activePrev[2] = false;
        }

        if (cbdf.equalsIgnoreCase("1")) {
            activePrev[3] = true;
        } else {
            activePrev[3] = false;
        }

        //log.info(selectedPrevDate);
        ///if (selectedPrevDate == null) {
        //    selectedPrevDate = DateUtils.addDayDate(new Date(), -1);
        //}

        //submitAction();

    }

    public List<BankBean> getListBank() {
        return listBank;
    }

    public void setListBank(List<BankBean> listBank) {
        this.listBank = listBank;
    }

    public String getRender() {
        return render;
    }

    public void setRender(String render) {
        this.render = render;
    }

    public List<EsqTrxIdBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<EsqTrxIdBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

    public String getGraphRefreshInterval() {
        return graphRefreshInterval;
    }

    public void setGraphRefreshInterval(String graphRefreshInterval) {
        this.graphRefreshInterval = graphRefreshInterval;
    }

    public boolean isShowTable() {
        return showTable;
    }

    public void setShowTable(boolean showTable) {
        this.showTable = showTable;
    }

    public String getShowPercentButtonText() {
        return showPercentButtonText;
    }

    public void setShowPercentButtonText(String showPercentButtonText) {
        this.showPercentButtonText = showPercentButtonText;
    }

    public boolean isSaveSelection() {
        return saveSelection;
    }

    public void setSaveSelection(boolean saveSelection) {
        this.saveSelection = saveSelection;
    }
}


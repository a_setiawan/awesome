package com.rintis.marketing.web.jsf.main.monitor.quickview;

import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.entity.TmsUserQuickView;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import com.rintis.marketing.web.jsf.main.alert.SearchAlertAnomalyHistoryManagedBean;
import org.primefaces.model.SelectableDataModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.ListDataModel;
import java.util.List;

@ManagedBean
@ViewScoped
public class UserQuickViewHistoryManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private UserQuickViewDataModel userQuickViewDataModel;
    private TmsUserQuickView[] selectedItems;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0520";
            pageTitle = getMenuName(moduleFactory, menuId);

            refreshData();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void refreshData() {
        try {
            List<TmsUserQuickView> listTmsUserQuickView = moduleFactory.getMasterService().listUserQuickview(getUserInfoBean().getUserId());

            for (TmsUserQuickView uqv : listTmsUserQuickView) {
                if (uqv.getTmsUserQuickViewPk().getTrxId().equalsIgnoreCase(DataConstant.ALL)) {
                    uqv.setTrxName("ALL");
                } else {
                    EsqTrxIdBean trx = moduleFactory.getMasterService().getEsqTrxId(uqv.getTmsUserQuickViewPk().getTrxId());
                    uqv.setTrxName(trx.getTrxname());
                }
                uqv.setIntervalName(DataConstant.getTimeFrameName(uqv.getTmsUserQuickViewPk().getTimeframe()));
            }

            userQuickViewDataModel = new UserQuickViewDataModel(listTmsUserQuickView);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String deleteAction() {
        try {
            if (selectedItems == null) {
                glowMessageError("Select Item to delete", "");
                return "";
            }
            if (selectedItems.length == 0) {
                glowMessageError("Select Item to delete", "");
                return "";
            }

            moduleFactory.getMasterService().deleteUserQuickView(selectedItems);

            refreshData();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String deletaAllAction() {
        try {
            moduleFactory.getMasterService().deleteAllUserQuickView(getUserInfoBean().getUserId());

            refreshData();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public UserQuickViewDataModel getUserQuickViewDataModel() {
        return userQuickViewDataModel;
    }

    public void setUserQuickViewDataModel(UserQuickViewDataModel userQuickViewDataModel) {
        this.userQuickViewDataModel = userQuickViewDataModel;
    }

    public TmsUserQuickView[] getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(TmsUserQuickView[] selectedItems) {
        this.selectedItems = selectedItems;
    }

    public class UserQuickViewDataModel extends ListDataModel<TmsUserQuickView> implements SelectableDataModel<TmsUserQuickView> {

        public UserQuickViewDataModel(List<TmsUserQuickView> list) {
            super(list);
        }

        @Override
        public TmsUserQuickView getRowData(String rowKey) {
            List<TmsUserQuickView> list = (List<TmsUserQuickView>)getWrappedData();
            for(TmsUserQuickView userQuickView : list) {
                String key = userQuickView.getTmsUserQuickViewPk().getUserId() +
                        userQuickView.getTmsUserQuickViewPk().getBankId() +
                        userQuickView.getTmsUserQuickViewPk().getTrxId() +
                        userQuickView.getTmsUserQuickViewPk().getTimeframe();
                if (key.equals(rowKey) ) {
                    return userQuickView;
                }
            }
            return null;
        }

        @Override
        public Object getRowKey(TmsUserQuickView userQuickView) {
            String key = userQuickView.getTmsUserQuickViewPk().getUserId() +
                    userQuickView.getTmsUserQuickViewPk().getBankId() +
                    userQuickView.getTmsUserQuickViewPk().getTrxId() +
                    userQuickView.getTmsUserQuickViewPk().getTimeframe();
            return key;
        }

    }


}

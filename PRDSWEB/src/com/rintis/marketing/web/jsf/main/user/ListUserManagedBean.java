package com.rintis.marketing.web.jsf.main.user;

import com.rintis.marketing.beans.entity.MenuRoleType;
import com.rintis.marketing.beans.entity.UserLogin;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.util.List;

@ManagedBean
@ViewScoped
public class ListUserManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;

    private List<MenuRoleType> listMenuRoleType;
    private List<UserLogin> listUserLogin;
    private UserLogin selectedDeleteItem;
    private Integer selectedRole;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listMenuRoleType = moduleFactory.getMenuService().listMenuRoleType(DataConstant.MENU_ROLE_TYPE_NOT_HIDDEN);
            listUserLogin = moduleFactory.getLoginService().listUserLoginNonBank(-1);



            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }



    public void deleteUserActionListener(ActionEvent event) {
        try {
            moduleFactory.getLoginService().deleteUserLogin(selectedDeleteItem.getUserLoginId());
            listUserLogin = moduleFactory.getLoginService().listUserLoginNonBank(selectedRole);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String searchAction() {
        try {
            listUserLogin = moduleFactory.getLoginService().listUserLoginNonBank(selectedRole);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }
    


    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<UserLogin> getListUserLogin() {
        return listUserLogin;
    }

    public void setListUserLogin(List<UserLogin> listUserLogin) {
        this.listUserLogin = listUserLogin;
    }

    public UserLogin getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(UserLogin selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }

    public List<MenuRoleType> getListMenuRoleType() {
        return listMenuRoleType;
    }

    public void setListMenuRoleType(List<MenuRoleType> listMenuRoleType) {
        this.listMenuRoleType = listMenuRoleType;
    }

    public Integer getSelectedRole() {
        return selectedRole;
    }

    public void setSelectedRole(Integer selectedRole) {
        this.selectedRole = selectedRole;
    }
}

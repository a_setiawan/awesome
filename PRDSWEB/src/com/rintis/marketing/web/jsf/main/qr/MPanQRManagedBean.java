
package com.rintis.marketing.web.jsf.main.qr;

import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.beans.bean.bi.QRAnomalyBean;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.app.main.module.login.LoginService;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.SessionFactory;
import org.primefaces.model.SelectableDataModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.ListDataModel;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * Created by aaw on 25/06/2021.
 */

@ManagedBean
@ViewScoped
public class MPanQRManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private SessionFactory sessionFactory;
    private List<PanTrxidBean> listTrxId;
    private List<QRAnomalyBean> list;
    private QRAnomalyDataModel listQRAnomalyDataModel;
    private List<QRAnomalyBean> selectedAlertAnomaly;
    private QRAnomalyBean[] selectedItems;
    private Date startDate;
    private Date endDate;
    private Date startTime;
    private Date endTime;
    private Date nowTime;
    private String transaction;
    private String selectedTrxId;
    private String periodRange;
    private String periodFr;
    private String periodTo;
    private String timeFr;
    private String timeTo;
    private String pan;
    private String roleID;
    private String period;
    private Boolean whitelist;
    private Boolean role;
    private Boolean roleBank;
    private Boolean disable;
    private Boolean enable3D;
    private Boolean disable3D;
    private boolean isUpdate;

    public MPanQRManagedBean() {
    }

    @Override
    @PostConstruct
    protected void initManagedBean() {

        roleID = LoginService.getRoleID;
        try {
            menuId = "0525";
            pageTitle = getMenuName(moduleFactory, menuId);

            startDate = new Date();
            endDate = new Date();
            startTime = new Date();
            endTime = new Date();
            nowTime = new Date();

            startTime = DateUtils.setMinutes(startTime, 0);
            startTime = DateUtils.setHours(startTime, startTime.getHours() - 1);
            endTime = DateUtils.setMinutes(endTime, 0);

            SimpleDateFormat datefmt = new SimpleDateFormat("yyMMdd");
            periodRange = datefmt.format(startDate);

            period = "60M";
            transaction = "QR";
            disable = false;
            whitelist = false;

            /*"29" = QR*/
            listTrxId = moduleFactory.getQRService().mPanListTrxId("29");
            /*Add by aaw 14/09/2021*/
            /* Find data from list in java 8
            List<PanTrxidBean> result = listTrxId.stream()
                    .filter(item -> item.getTrxname().equalsIgnoreCase("QR"))
                    .collect(Collectors.toList());
            */
            for(PanTrxidBean p : listTrxId) {
                if ("QR".equalsIgnoreCase(p.getTrxname())) {
                    p.setTrxname("QR (MPAN)");
                }
            }
            /*End add*/

            if(Integer.parseInt(this.getRoleID()) == 99){
                role = true;
            } else {
                role = false;
            }

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            UserInfoBean userInfoBean = getUserInfoBean();
            String bank_id = "";
            if (userInfoBean.getUserLoginBank() != null) {
                bank_id = userInfoBean.getUserLoginBank().getBankId();
                roleBank = false;
            } else {
                roleBank = true;
            }

            if (period.equals("1D") || period.equals("3D")){
                disable = false;
            } else {
                disable = true;
            }

            if(period.equals("3D")){
                enable3D = true;
                disable3D = false;
            }else{
                enable3D = false;
                disable3D = true;
            }

            SimpleDateFormat datefmt = new SimpleDateFormat("yyMMdd");
            SimpleDateFormat timefmt = new SimpleDateFormat("HHmm");
            periodFr = datefmt.format(startDate);
            periodTo = datefmt.format(endDate);
            timeFr = timefmt.format(startTime);
            timeTo = timefmt.format(endTime);

            if(timeTo.substring(0,2).equals("00")){
                timeTo = "24" + timeTo.substring(2,4);
            }

            long numOfDays = getDifferenceDays(startDate, nowTime) + 1;
            //long days = Integer.parseInt(periodRange) - Integer.parseInt(periodFr);

            boolean isValid = true;
            if (new SimpleDateFormat("ddMMyyyy").parse(periodFr).after(new SimpleDateFormat("ddMMyyyy").parse(periodTo))
                    || new SimpleDateFormat("HHmm").parse(timeFr).after(new SimpleDateFormat("HHmm").parse(timeTo))) {
                isValid = false;
                System.out.println("Start date or time start is bigger than date to or time to !");
            }

            if (numOfDays < 30 && isValid) {
                if(period.equals("1H")){
                    list = moduleFactory.getQRService().listAnomalyQRAcq1H(periodFr, periodTo, timeFr, timeTo, transaction, whitelist, bank_id);
                    listQRAnomalyDataModel = new QRAnomalyDataModel(list);
                }else if(period.equals("3H")){
                    list = moduleFactory.getQRService().listAnomalyQRAcq3H(periodFr, periodTo, timeFr, timeTo, transaction, whitelist, bank_id);
                    listQRAnomalyDataModel = new QRAnomalyDataModel(list);
                }else if(period.equals("6H")){
                    list = moduleFactory.getQRService().listAnomalyQRAcq6H(periodFr, periodTo, timeFr, timeTo, transaction, whitelist, bank_id);
                    listQRAnomalyDataModel = new QRAnomalyDataModel(list);
                }else if(period.equals("1D")){
                    list = moduleFactory.getQRService().listAnomalyQRAcq1D(periodFr, periodTo, transaction, whitelist, bank_id);
                    listQRAnomalyDataModel = new QRAnomalyDataModel(list);
                }else if(period.equals("3D")){
                    list = moduleFactory.getQRService().listAnomalyQRAcq3D(periodFr, periodTo, transaction, whitelist, bank_id);
                    listQRAnomalyDataModel = new QRAnomalyDataModel(list);
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String errorMsg() throws ParseException {
        long numOfDays = getDifferenceDays(startDate, nowTime) + 1;
        if (numOfDays > 7) {
            glowMessageError("Max 7 Days", "");
        }

        if (periodFr != null && periodTo != null) {
            if (new SimpleDateFormat("ddMMyyyy").parse(periodFr).after(new SimpleDateFormat("ddMMyyyy").parse(periodTo))) {
                glowMessageError("Tanggal awal harus lebih kecil dari tanggal akhir !", "");
            }
        } else if (timeFr != null && timeTo != null){
            if(new SimpleDateFormat("HHmm").parse(timeFr).after(new SimpleDateFormat("HHmm").parse(timeTo))) {
                glowMessageError("Jam awal harus lebih kecil dari jam akhir !", "");
            }
        }
        return "";
    }

    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public void onPoolAction() {
        submitAction();
    }

    public String saveItemAction() {
        try {
            String act = "true";
            isUpdate = false;
            BigDecimal freq = BigDecimal.valueOf(0);
            if (selectedItems == null) return "";
            for(QRAnomalyBean aa : selectedItems) {
                boolean chk = moduleFactory.getQRService().checkQRAcqUsed(aa.getPan().trim(), aa.getAcquirerId().trim());
                if (!chk) {
                    moduleFactory.getQRService().saveMPanTreshold(aa.getPan().trim(), aa.getAcquirerId().trim(), freq, freq, freq, freq, freq, freq, freq, freq, freq, act);
                    isUpdate = true;
                }
            }

            if(isUpdate){
                submitAction();
                //glowMessageInfo("Merchant PAN has been Saved", "");
            }

            selectedItems = null;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public void popupMsg() {
        if (isUpdate) {
            glowMessageInfo("Merchant PAN has been Saved", "");
        }
    }

    public String paramDecode(String data){
        //encrypt param
        if(!data.equals(null)){
            data = HttpUtils.decodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<QRAnomalyBean> getList() {
        return list;
    }

    public void setList(List<QRAnomalyBean> list) {
        this.list = list;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public Boolean getWhitelist() {
        return whitelist;
    }

    public void setWhitelist(Boolean whitelist) {
        this.whitelist = whitelist;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPeriodRange() {
        return periodRange;
    }

    public void setPeriodRange(String periodRange) {
        this.periodRange = periodRange;
    }

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    public Boolean getRole(){
        return role;
    }

    public void setRole(Boolean role){
        this.role = role;
    }

    public Boolean getRoleBank() {
        return roleBank;
    }

    public void setRoleBank(Boolean roleBank) {
        this.roleBank = roleBank;
    }

    public Boolean getDisable() {
        return disable;
    }

    public void setDisable(Boolean disable) {
        this.disable = disable;
    }

    public Boolean getEnable3D() {
        return enable3D;
    }

    public void setEnable3D(Boolean enable3D) {
        this.enable3D = enable3D;
    }

    public Boolean getDisable3D() {
        return disable3D;
    }

    public void setDisable3D(Boolean disable3D) {
        this.disable3D = disable3D;
    }

    public class QRAnomalyDataModel extends ListDataModel<QRAnomalyBean> implements SelectableDataModel<QRAnomalyBean> {

        public QRAnomalyDataModel(List<QRAnomalyBean> list) {
            super(list);
        }

        @Override
        public QRAnomalyBean getRowData(String rowKey) {
            List<QRAnomalyBean> list = (List<QRAnomalyBean>)getWrappedData();
            for(QRAnomalyBean qrAnomaly : list) {
                String pp = qrAnomaly.getPan().toString() + qrAnomaly.getAcquirerId().toString();
                if (pp.equals(rowKey) ) {
                    return qrAnomaly;
                }
            }
            return null;
        }

        @Override
        public Object getRowKey(QRAnomalyBean panAnomaly) {
            return panAnomaly.getPan() + panAnomaly.getAcquirerId();
        }

    }

    public QRAnomalyDataModel getListQRAnomalyDataModel() {
        return listQRAnomalyDataModel;
    }

    public void setListQRAnomalyDataModel(QRAnomalyDataModel listQRAnomalyDataModel) {
        this.listQRAnomalyDataModel = listQRAnomalyDataModel;
    }

    public List<QRAnomalyBean> getSelectedAnomaly() {
        return selectedAlertAnomaly;
    }

    public void setSelectedAnomaly(List<QRAnomalyBean> selectedAlertAnomaly) {
        this.selectedAlertAnomaly = selectedAlertAnomaly;
    }

    public QRAnomalyBean[] getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(QRAnomalyBean[] selectedItems) {
        this.selectedItems = selectedItems;
    }

    public List<PanTrxidBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<PanTrxidBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

 }

package com.rintis.marketing.web.jsf.main.groupbank;

import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.BankEmail;
import com.rintis.marketing.beans.entity.EisBank;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.common.ParamConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

@ManagedBean
@ViewScoped
public class EditEmailBankManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private String bankId;
    private String bankName;
    private String alertMccVelocity;
    private String alertSeqVelocity;
    private BankEmail bankEmail;
    private List<String> listEmail;
    private String newEmail;
    /*Add by aaw 02/07/2021*/
    private String alertMpanAcq;
    /*End add*/

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0404";
            pageTitle = getMenuName(moduleFactory, menuId);

            bankId = (String)getRequestParameterMap().get(ParamConstant.PARAM_BANKID);
            BankBean bank = moduleFactory.getMasterService().getBankCA(bankId);
            bankName = bank.getBankname();

            newEmail = "";
            bankEmail = moduleFactory.getMasterService().getBankEmail(bankId);

            alertMccVelocity = bankEmail.getAlert_mcc_status().replace("YES", "true").replace("NO", "false");
            alertSeqVelocity = bankEmail.getAlert_seq_status().replace("YES", "true").replace("NO", "false");
            /*Add by aaw 02/07/2021*/
            alertMpanAcq = bankEmail.getAlert_mpan_acq().replace("YES", "true").replace("NO", "false");
            /*End add*/

            listEmail = new ArrayList<>();
            if (bankEmail != null) {
                StringTokenizer st = new StringTokenizer(StringUtils.checkNull(bankEmail.getEmail()), ",");
                while (st.hasMoreTokens()) {
                    String s = st.nextToken();
                    listEmail.add(s);
                }
            }

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String saveAction() {
        try {
            for(String s : listEmail) {
                if (StringUtils.checkNull(s).length() == 0) {
                    glowMessageError("Email blank", "");
                    return "";
                }
            }

            StringBuffer sb = new StringBuffer("");
            int idx = 0;
            for(String s : listEmail) {
                sb.append(s);
                if (idx < listEmail.size() - 1 ) {
                    sb.append(",");
                }
                idx++;
            }

            alertMccVelocity = alertMccVelocity.replace("true", "YES").replace("false", "NO");
            alertSeqVelocity = alertSeqVelocity.replace("true", "YES").replace("false", "NO");
            /*Add by aaw 02/07/2021*/
            alertMpanAcq = alertMpanAcq.replace("true", "YES").replace("false", "NO");
            /*End add*/

            bankEmail.setEmail(sb.toString());
            bankEmail.setAlert_mcc_status(alertMccVelocity);
            bankEmail.setAlert_seq_status(alertSeqVelocity);
            /*Add by aaw 02/07/2021*/
            bankEmail.setAlert_mpan_acq(alertMpanAcq);
            /*End add*/
            moduleFactory.getMasterService().updateBankEmail(bankEmail);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "/apps/groupbank/bankEmail?faces-redirect=true";
    }

    public String deleteAction(String idx) {
        try {
            listEmail.remove(Integer.parseInt(idx));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String updateAction() {
        return "";
    }

    public String addAction() {
        if (StringUtils.checkNull(newEmail).trim().length() == 0) {
            glowMessageError("Enter Email", "");
            return "";
        }
        listEmail.add(newEmail);
        newEmail = "";
        return "";
    }


    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<String> getListEmail() {
        return listEmail;
    }

    public void setListEmail(List<String> listEmail) {
        this.listEmail = listEmail;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public BankEmail getBankEmail() {
        return bankEmail;
    }

    public void setBankEmail(BankEmail bankEmail) {
        this.bankEmail = bankEmail;
    }

    public String getAlertMccVelocity() {
        return alertMccVelocity;
    }

    public void setAlertMccVelocity(String alertMccVelocity) {
        this.alertMccVelocity = alertMccVelocity;
    }

    public String getAlertSeqVelocity() {
        return alertSeqVelocity;
    }

    public void setAlertSeqVelocity(String alertSeqVelocity) {
        this.alertSeqVelocity = alertSeqVelocity;
    }

    /*Ad by aaw 02/07/2021*/
    public String getAlertMpanAcq() {
        return alertMpanAcq;
    }

    public void setAlertMpanAcq(String alertMpanAcq) {
        this.alertMpanAcq = alertMpanAcq;
    }
    /*End add*/
}

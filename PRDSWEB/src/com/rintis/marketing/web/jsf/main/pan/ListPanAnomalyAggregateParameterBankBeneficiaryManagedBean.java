package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.PanAnomalyParamBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * Created by aabraham on 04/03/2019.
 */
@ManagedBean
@ViewScoped
public class ListPanAnomalyAggregateParameterBankBeneficiaryManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;

    private List<PanAnomalyParamBean> listPanAnomalyParamBankBeneficiary;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listPanAnomalyParamBankBeneficiary = moduleFactory.getPanService().listParamBankBene();
            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<PanAnomalyParamBean> getListPanAnomalyParamBankBeneficiary() {
        return listPanAnomalyParamBankBeneficiary;
    }

    public void setListPanAnomalyParamBankBeneficiary(List<PanAnomalyParamBean> listPanAnomalyParamBankBeneficiary) {
        this.listPanAnomalyParamBankBeneficiary = listPanAnomalyParamBankBeneficiary;
    }
}

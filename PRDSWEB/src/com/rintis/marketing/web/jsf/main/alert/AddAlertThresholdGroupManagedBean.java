package com.rintis.marketing.web.jsf.main.alert;

import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.entity.*;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.MathUtils;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class AddAlertThresholdGroupManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private String selectedGroupId;
    private String selectedGroupName;
    private List<GroupBank> listGroupBank;
    private String selectedTrxId;
    private String selectedTransactionName;
    private List<EsqTrxIdBean> listTrxId;
    private List<AlertThresholdGroup> list;
    private List<EsqTrxIdBean> listTrxIdIndicator;

	private String m1Description;
	private String m2Description;
	private String m3Description;
	private String m4Description;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0407";
            pageTitle = getMenuName(moduleFactory, menuId);

            listGroupBank = moduleFactory.getMasterService().listGroupBank();

            selectedTrxId = "";
            listTrxId = moduleFactory.getMasterService().listEsqTrxId();
			
			m1Description = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_M1_DESCRIPTION);
			m2Description = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_M2_DESCRIPTION);
			m3Description = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_M3_DESCRIPTION);
			m4Description = moduleFactory.getCommonService().getSystemProperty(DataConstant.SYSTEM_PROPERTY_M4_DESCRIPTION);
			

            String gid = StringUtils.checkNull((String)getRequestParameterMap().get("gid"));
            String trxid = StringUtils.checkNull((String)getRequestParameterMap().get("trxid"));
            if (gid.length() > 0 && trxid.length() > 0) {
                selectedGroupId = gid;
                selectedTrxId = trxid;
                submitAction();
            }

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            if (selectedGroupId.length() == 0) {
                glowMessageError("Select Group Bank", "");
                return "";
            }

            if (selectedTrxId.length() == 0) {
                glowMessageError("Select Transaction", "");
                return "";
            }

            GroupBank groupBank = moduleFactory.getMasterService().getGroupBank(selectedGroupId);
            selectedGroupName = groupBank.getGroupName();
            if (selectedTrxId.equalsIgnoreCase(DataConstant.ALL)) {
                selectedTransactionName = "All";
                listTrxIdIndicator = moduleFactory.getMasterService().listEsqTrxIdTrxIndicator();
            } else {
                EsqTrxIdBean trx = moduleFactory.getMasterService().getEsqTrxId(selectedTrxId);
                selectedTransactionName = trx.getTrxname();
                listTrxIdIndicator = moduleFactory.getMasterService().listEsqTrxIdTrxIndicator(selectedTrxId);
            }



            list = new ArrayList<>();
            for(int i = 0; i < DataConstant.TIMEFRAME_ARRAY.length - 1; i++) {
                String timeframe = DataConstant.TIMEFRAME_ARRAY[i];

                for (EsqTrxIdBean trxIndic : listTrxIdIndicator) {
                    AlertThresholdGroup alertThresholdGroup = null;

                    //get old data
                    alertThresholdGroup = moduleFactory.getAlertService().getAlertThresholdGroup(selectedGroupId, selectedTrxId, trxIndic.getTrxidindicator(), timeframe);
                    if (alertThresholdGroup == null) {
                        AlertThresholdGroupPk pk = new AlertThresholdGroupPk();
                        pk.setGroupBankId(selectedGroupId);
                        pk.setTrxId(selectedTrxId);
                        pk.setTrxIndicatorId(trxIndic.getTrxidindicator());
                        pk.setTimeframe(timeframe);

                        alertThresholdGroup = new AlertThresholdGroup();
                        alertThresholdGroup.setAlertThresholdGroupPk(pk);
                        alertThresholdGroup.setTransactionIndicatorName(trxIndic.getTrxnameindicator());
                        alertThresholdGroup.setActiveM1Bool(false);
                        alertThresholdGroup.setActiveM2Bool(false);
                        alertThresholdGroup.setActiveM3Bool(false);
                        alertThresholdGroup.setActiveM4Bool(false);
                    } else {
                        alertThresholdGroup.setTransactionIndicatorName(trxIndic.getTrxnameindicator());

                        alertThresholdGroup.setActiveM1Bool(StringUtils.int2bool(alertThresholdGroup.getActiveM1()));
                        alertThresholdGroup.setActiveM2Bool(StringUtils.int2bool(alertThresholdGroup.getActiveM2()));
                        alertThresholdGroup.setActiveM3Bool(StringUtils.int2bool(alertThresholdGroup.getActiveM3()));
                        alertThresholdGroup.setActiveM4Bool(StringUtils.int2bool(alertThresholdGroup.getActiveM4()));
                    }

                    list.add(alertThresholdGroup);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String saveAction() {
        String MESSAGE_TRX_NEGATIVE = "Min Trx is negative";
        String MESSAGE_NEGATIVE = "Threshold is negative";
        String MESSAGE_MORE_100 = "Threshold is greater than 999";
        try {
            //validasi threshold percent
            for(AlertThresholdGroup atg : list) {
                //check m1 *****
                //check negative
                if (atg.getMinTrxPeriodTt().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_TRX_NEGATIVE, "");
                    return "";
                }
                if (atg.getMinTrxPeriodAp().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_TRX_NEGATIVE, "");
                    return "";
                }
                if (atg.getMinTrxPeriodDc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_TRX_NEGATIVE, "");
                    return "";
                }
                if (atg.getMinTrxPeriodDf().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_TRX_NEGATIVE, "");
                    return "";
                }
                //check negative
                if (atg.getTrhTrxPeriodPctTt().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxPeriodPctAp().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxPeriodPctDc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxPeriodPctDf().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }

                //check > 100
                //Modify by AAH check > 999
                if (atg.getTrhTrxPeriodPctTt().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxPeriodPctAp().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxPeriodPctDc().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxPeriodPctDf().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }


                //check m2 *****
                //check negative
                if (atg.getMinTrxDayTt().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_TRX_NEGATIVE, "");
                    return "";
                }
                if (atg.getMinTrxDayAp().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_TRX_NEGATIVE, "");
                    return "";
                }
                if (atg.getMinTrxDayDc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_TRX_NEGATIVE, "");
                    return "";
                }
                if (atg.getMinTrxDayDf().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_TRX_NEGATIVE, "");
                    return "";
                }
                //check negative
                //total
                if (atg.getTrhTrxDayPctTtDec().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctTtInc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctTtHwDec().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctTtHwInc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctTtWhDec().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctTtWhInc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                //approve
                if (atg.getTrhTrxDayPctApDec().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctApInc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctApHwDec().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctApHwInc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctApWhDec().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctApWhInc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                //decline charge
                if (atg.getTrhTrxDayPctDcDec().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDcInc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDcHwDec().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDcHwInc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDcWhDec().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDcWhInc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                //decline free
                if (atg.getTrhTrxDayPctDfDec().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDfInc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDfHwDec().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDfHwInc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDfWhDec().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDfWhInc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }

                //check > 100
                //Modify by AAH check > 999
                //total
                if (atg.getTrhTrxDayPctTtDec().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctTtInc().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctTtHwDec().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctTtHwInc().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctTtWhDec().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctTtWhInc().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                //approve
                if (atg.getTrhTrxDayPctApDec().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctApInc().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctApHwDec().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctApHwInc().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctApWhDec().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctApWhInc().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                //decline charge
                if (atg.getTrhTrxDayPctDcDec().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDcInc().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDcHwDec().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDcHwInc().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDcWhDec().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDcWhInc().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                //decline free
                if (atg.getTrhTrxDayPctDfDec().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDfInc().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDfHwDec().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDfHwInc().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDfWhDec().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
                if (atg.getTrhTrxDayPctDfWhInc().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }

                //check m3 *****
                //check negative
                if (atg.getMinTrxPctDc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_TRX_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhPctDc().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }
                //check > 100
                //Modify by AAH check > 999
                if (atg.getTrhPctDc().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }

                //check m4 *****
                //check negative
                if (atg.getMinTrxPctDf().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_TRX_NEGATIVE, "");
                    return "";
                }
                if (atg.getTrhPctDf().compareTo(BigDecimal.ZERO) < 0) {
                    glowMessageError(MESSAGE_NEGATIVE, "");
                    return "";
                }

                //check > 100
                //Modify by AAH check > 999
                if (atg.getTrhPctDf().compareTo(new BigDecimal("999")) > 0) {
                    glowMessageError(MESSAGE_MORE_100, "");
                    return "";
                }
            }

            moduleFactory.getAlertService().updateAlertThresholdGroupV2(list);

            submitAction();

            glowMessageInfo("Update Success", "");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            glowMessageError("Update Failed", "");
        }
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public String getSelectedGroupId() {
        return selectedGroupId;
    }

    public void setSelectedGroupId(String selectedGroupId) {
        this.selectedGroupId = selectedGroupId;
    }

    public List<GroupBank> getListGroupBank() {
        return listGroupBank;
    }

    public void setListGroupBank(List<GroupBank> listGroupBank) {
        this.listGroupBank = listGroupBank;
    }

    public String getSelectedTrxId() {
        return selectedTrxId;
    }

    public void setSelectedTrxId(String selectedTrxId) {
        this.selectedTrxId = selectedTrxId;
    }

    public List<EsqTrxIdBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<EsqTrxIdBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

    public String getSelectedGroupName() {
        return selectedGroupName;
    }

    public void setSelectedGroupName(String selectedGroupName) {
        this.selectedGroupName = selectedGroupName;
    }

    public String getSelectedTransactionName() {
        return selectedTransactionName;
    }

    public void setSelectedTransactionName(String selectedTransactionName) {
        this.selectedTransactionName = selectedTransactionName;
    }

    public List<AlertThresholdGroup> getList() {
        return list;
    }

    public void setList(List<AlertThresholdGroup> list) {
        this.list = list;
    }

    public List<EsqTrxIdBean> getListTrxIdIndicator() {
        return listTrxIdIndicator;
    }

    public void setListTrxIdIndicator(List<EsqTrxIdBean> listTrxIdIndicator) {
        this.listTrxIdIndicator = listTrxIdIndicator;
    }

    public String getM1Description() {
        return m1Description;
    }

    public void setM1Description(String m1Description) {
        this.m1Description = m1Description;
    }

    public String getM2Description() {
        return m2Description;
    }

    public void setM2Description(String m2Description) {
        this.m2Description = m2Description;
    }

    public String getM3Description() {
        return m3Description;
    }

    public void setM3Description(String m3Description) {
        this.m3Description = m3Description;
    }

    public String getM4Description() {
        return m4Description;
    }

    public void setM4Description(String m4Description) {
        this.m4Description = m4Description;
    }
}

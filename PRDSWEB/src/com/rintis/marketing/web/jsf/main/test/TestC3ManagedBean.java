package com.rintis.marketing.web.jsf.main.test;

import com.martinlinha.c3faces.component.C3Chart;
import com.martinlinha.c3faces.model.C3DataSet;
import com.martinlinha.c3faces.model.C3DataSetString;
import com.martinlinha.c3faces.model.C3ViewDataSet;
import com.martinlinha.c3faces.model.C3ViewDataSetString;
import com.martinlinha.c3faces.script.property.Axis;
import com.martinlinha.c3faces.script.property.Data;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.*;

@ManagedBean
@ViewScoped
public class TestC3ManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private Data datac3;
    private Axis axis;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            saveAction();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String saveAction() {
        try {
            int randmax = 200;
            int randmin = 0;
            Random r = new Random();

            int y2 = r.nextInt((randmax - randmin) + 1) + randmin;

            List<Integer> listData = new ArrayList<>();
            List<Long> listDate = new ArrayList<>();
            List<String> listDateString = new ArrayList<>();

            C3DataSet ds = null;
            Date now = new Date();
            now = DateUtils.addDate(Calendar.DAY_OF_MONTH, now, -14);

            listDate = new ArrayList<>();
            listData = new ArrayList<>();
            for(int i=1; i <=10; i++) {
                int y1 = r.nextInt((randmax - randmin) + 1) + randmin;
                listData.add(y1);

                listDateString.add(DateUtils.convertDate(now, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDate.add(now.getTime());
                now = DateUtils.addDate(Calendar.DAY_OF_MONTH, now, 1);
            }
            ds = new C3DataSet(listData);
            C3ViewDataSet dataSet1 = new C3ViewDataSet("Data sample 1",
                    ds, "#FF0000");

            C3DataSetString dsDateString = new C3DataSetString(listDateString);
            C3ViewDataSetString dataSetDateString = new C3ViewDataSetString("x", dsDateString, false);

            listData = new ArrayList<>();
            for(int i=1; i <=10; i++) {
                int y1 = r.nextInt((randmax - randmin) + 1) + randmin;
                listData.add(y1);
            }
            ds = new C3DataSet(listData);
            C3ViewDataSet dataSet2 = new C3ViewDataSet("Data sample 2",
                    ds, "#0000FF");

            datac3 = new Data();
            datac3.getDataSetsString().add(dataSetDateString);
            datac3.getDataSets().add(dataSet1);
            datac3.getDataSets().add(dataSet2);

            datac3.setX("z1");
            datac3.setxFormat("%Y-%m-%d %H:%M");

            axis = new Axis();
            axis.setHeightX(70);
            axis.setTypeX(Axis.Type.TIMESERIES);
            axis.setTickX("fit: true,\n" +
                    "                            format: '%d/%m %H:%M',\n" +
                    "                            rotate: -45,\n" +
                    "                            multiline: false,\n" +
                    "                            culling: true");

            //C3Chart chart;
            //chart.

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public void clickTest() {
        log.info("click");
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public Data getDatac3() {
        return datac3;
    }

    public void setDatac3(Data datac3) {
        this.datac3 = datac3;
    }

    public Axis getAxis() {
        return axis;
    }

    public void setAxis(Axis axis) {
        this.axis = axis;
    }
}

package com.rintis.marketing.web.jsf.main.qr;

import com.rintis.marketing.beans.bean.bi.WhitelistQRBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.HashMap;
import java.util.List;

/**
 * Created by aaw on 28/06/21.
 */

@ManagedBean
@ViewScoped
public class AddWhitelistMPanManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private WhitelistQRBean whitelistQRBean;
    private List<com.rintis.marketing.beans.bean.report.BankBean> listBankReport;
    private String selectedBankId;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        whitelistQRBean = new WhitelistQRBean();
        try {
            listBankReport = moduleFactory.getMasterService().listBank(new HashMap<>());
        }catch(Exception e){
            log.error(e.getMessage(), e);
        }
    }

    public boolean isAlpha(String acct) {
        boolean res = false;
        for (char c : acct.toCharArray()) {
            if((Character.isLetter(c))) {
                System.out.println("Is a character! " + c);
                res = true;
                break;
            }
        }
        return res;
    }

    public boolean containsDigit(String acct) {
        boolean res = false;
        if (acct != null && !acct.isEmpty()) {
            for (char c :  acct.toCharArray()) {
                if(Character.isDigit(c)) {
                    res = true;
                    break;
                }
            }
        }
        return res;
    }

    public String saveAction() {
        try {
            if (whitelistQRBean.getPan().length() == 0) {
                glowMessageError("Enter PAN", "");
                return "";
            }

            if (isAlpha(whitelistQRBean.getPan().trim()) || !containsDigit(whitelistQRBean.getPan().trim())) {
                glowMessageError("'PAN' Not Valid", "Please Chek PAN Number!");
                return "";
            }

            if (selectedBankId == null || selectedBankId == "") {
                glowMessageError("Acquirer Bank Is Required!", "");
                return "";
            }

            boolean chk = moduleFactory.getMPanTresholdService().checkMPanUsed(whitelistQRBean.getPan().trim(), selectedBankId);
            if (chk) {
                glowMessageError("PAN And Acquirer Bank Already Exist", "");
                return "";
            }

            String act = whitelistQRBean.getActive().toString();

            whitelistQRBean.setPan(whitelistQRBean.getPan().trim());
            whitelistQRBean.setAcquirerId(selectedBankId);
            whitelistQRBean.setFreqApp(whitelistQRBean.getFreqApp());
            whitelistQRBean.setFreqDc(whitelistQRBean.getFreqDc());
            whitelistQRBean.setFreqDcFree(whitelistQRBean.getFreqDcFree());
            whitelistQRBean.setFreqRvsl(whitelistQRBean.getFreqRvsl());
            whitelistQRBean.setTotalAmtApp(whitelistQRBean.getTotalAmtApp());
            whitelistQRBean.setTotalAmtDc(whitelistQRBean.getTotalAmtDc());
            whitelistQRBean.setTotalAmtDcFree(whitelistQRBean.getTotalAmtDcFree());
            whitelistQRBean.setTotalAmtRvsl(whitelistQRBean.getTotalAmtRvsl());
            whitelistQRBean.setTotalAmt(whitelistQRBean.getTotalAmt());
            whitelistQRBean.setActive(act);

            moduleFactory.getMPanTresholdService().saveMPanTreshold(whitelistQRBean);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_WHITELIST_MPAN + "?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public WhitelistQRBean getWhitelistQRBean() {
        return whitelistQRBean;
    }

    public void setWhitelistQRBean(WhitelistQRBean whitelistQRBeann) {
        this.whitelistQRBean = whitelistQRBean;
    }

    public List<com.rintis.marketing.beans.bean.report.BankBean> getListBankReport() {
        return listBankReport;
    }

    public void setListBankReport(List<com.rintis.marketing.beans.bean.report.BankBean> listBankReport) {
        this.listBankReport = listBankReport;
    }

    public String getSelectedBankId() {
        return selectedBankId;
    }

    public void setSelectedBankId(String selectedBankId) {
        this.selectedBankId = selectedBankId;
    }
}

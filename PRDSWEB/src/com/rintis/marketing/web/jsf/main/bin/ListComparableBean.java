package com.rintis.marketing.web.jsf.main.bin;

import com.rintis.marketing.beans.entity.EsqTrxid;

import java.io.Serializable;

/**
 * Created by aabraham on 01/02/2021.
 */
public class ListComparableBean implements Serializable, Comparable<ListComparableBean> {

    private EsqTrxid esqTrxid;

    public EsqTrxid getEsqTrxid() {
        return esqTrxid;
    }

    public void setEsqTrxid(EsqTrxid esqTrxid) {
        this.esqTrxid = esqTrxid;
    }

    @Override
    public int compareTo(ListComparableBean comparestu) {
        String compareage = ((ListComparableBean)comparestu).esqTrxid.getTrxName();
        /* For Ascending order*/
        return this.esqTrxid.getTrxName().compareTo(compareage);
    }
}

package com.rintis.marketing.web.jsf.main.blocked;

import com.rintis.marketing.beans.bean.bi.PanAnomalyEmailBean;
import com.rintis.marketing.beans.bean.blocked.BlockedAdministrationDetail;
import com.rintis.marketing.beans.bean.blocked.BlockedAdministrationHeader;
import com.rintis.marketing.config.spring.ReadProperty;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import org.hibernate.SessionFactory;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.faces.event.ValueChangeEvent;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


@ManagedBean
@ViewScoped
public class blockedApprovalManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private SessionFactory sessionFactory;
    private String status;
    private String docId;
    private List<BlockedAdministrationHeader> listDoc;
    private List<BlockedAdministrationDetail> listDetail;
    private List<BlockedAdministrationHeader> listHeader;
    private String pDocId, pDateCreated, pCreatedBy, pStatus, pStatusDate, pApprovedBy;

    public blockedApprovalManagedBean() {}

    @Override
    @PostConstruct
    protected void initManagedBean() {

        try {
            menuId = "060201";
            pageTitle = getMenuName(moduleFactory, menuId);

            listDoc = moduleFactory.getSequentialService().listDocBlocked(status);
            listDetail = moduleFactory.getSequentialService().listBlockedAdministrationDetail("");

            pDocId = "-";
            pDateCreated = "-";
            pCreatedBy = "-";
            pStatus = "-";
            pStatusDate = "-";
            pApprovedBy = "-";

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            if(!docId.equals("")){
                listDetail = moduleFactory.getSequentialService().listBlockedAdministrationDetail(docId);
                listHeader = moduleFactory.getSequentialService().listBlockedAdministrationHeader(docId);
                if(!listHeader.equals(null)){
                    pDocId = docId;
                    pDateCreated = listHeader.get(0).getCreatedDate();
                    pCreatedBy = listHeader.get(0).getCreatedBy();
                    pStatus = listHeader.get(0).getStatus();
                    pStatusDate = listHeader.get(0).getStatusDate();
                    pApprovedBy = moduleFactory.getSequentialService().getApprovedBy(docId);
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return "";
    }

    public void statusChangeListener() {
        try {
            listDoc = moduleFactory.getSequentialService().listDocBlocked(status);
            listDetail = moduleFactory.getSequentialService().listBlockedAdministrationDetail("");

            pDocId = "-";
            pDateCreated = "-";
            pCreatedBy = "-";
            pStatus = "-";
            pStatusDate = "-";
            pApprovedBy = "-";

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String approveAction() {
        if(!status.equals("Created")){
            glowMessageInfo("Approval Failed", "");
        }else{
            try {
                if(listDetail.size() > 0){
                    String emailTo = moduleFactory.getSequentialService().getEmailDocId(docId);
                    String userId = getUserInfoBean().getUserId();
                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat datefmt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    Date date = calendar.getTime();
                    String datetime = datefmt.format(date);
                    moduleFactory.getSequentialService().updatePanDetail(docId, "Blocked");
                    moduleFactory.getSequentialService().updatePanDetailManual(docId, "Blocked");
                    moduleFactory.getSequentialService().addPanBlockedHistory(docId, String.valueOf(datetime), userId, "Blocked");
                    moduleFactory.getSequentialService().updateBlockedHeader(docId, String.valueOf(datetime), "Approved");
                    moduleFactory.getSequentialService().addPanBlockedApproval(docId, String.valueOf(datetime), userId, "Approved");
                    moduleFactory.getSequentialService().insertPgw(docId);
                    moduleFactory.getSequentialService().insertB24(docId);
                    listDoc = moduleFactory.getSequentialService().listDocBlocked(status);
                    listDetail = moduleFactory.getSequentialService().listBlockedAdministrationDetail("");
                    glowMessageInfo("PAN Block Approved, Success", "");

                    String MAIL_FROM = ReadProperty.mailProperty().getMailFrom();
                    String MAIL_HOST = ReadProperty.mailProperty().getMailHost();
                    int MAIL_PORT = Integer.parseInt(ReadProperty.mailProperty().getMailPort());
                    String MAIL_SUBJECT = ReadProperty.mailProperty().getMailSubjectBlockApprove();

                    Properties prop = System.getProperties();
                    prop.put("mail.smtp.host", MAIL_HOST);
                    prop.put("mail.smtp.port", MAIL_PORT);
                    prop.put("mail.debug", "false");
                    prop.put("mail.transport.protocol", "smtp");

                    Session session = Session.getDefaultInstance(prop);
                    MimeMessage msg = new MimeMessage(session);
                    msg.setFrom(new InternetAddress(MAIL_FROM));

                    msg.addRecipients(Message.RecipientType.TO, emailTo);

                    List <PanAnomalyEmailBean> recipientListCC = moduleFactory.getSequentialService().getEmail();
                    for (PanAnomalyEmailBean recipient : recipientListCC) {
                        msg.addRecipient(Message.RecipientType.CC, new InternetAddress(recipient.getUserEmail()));
                    }

                    msg.setContent(getMailBody(docId, userId), "text/html");
                    msg.setSubject(MAIL_SUBJECT + " - " + docId);
                    msg.setHeader("X-Priority", "1");
                    Transport transport = session.getTransport();
                    transport.send(msg);
                    log.info("send email block approval response");

                    listDoc = moduleFactory.getSequentialService().listDocBlocked(status);
                    listDetail = moduleFactory.getSequentialService().listBlockedAdministrationDetail("");

                }else{
                    glowMessageInfo("List Empty", "");
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                glowMessageInfo("Approval Failed", "");
            }
        }

        return "";
    }

    public String getMailBody(String pDocId, String pUserId){
        try {
            listHeader = moduleFactory.getSequentialService().listBlockedAdministrationHeader(pDocId);
            if(!listHeader.equals(null)){
                pDateCreated = listHeader.get(0).getCreatedDate();
                pCreatedBy = listHeader.get(0).getCreatedBy();
                pStatus = listHeader.get(0).getStatus();
                pStatusDate = listHeader.get(0).getStatusDate();
            }
        }catch (Exception e) {
            log.error(e.getMessage(), e);
        }


        String result = "<html>"
                + "<body style='font-family:Calibri; font-size:12pt'>"
                + "<div>"
                + "<label>Dear All, </label>"
                + "</div>"
                + "<div style='margin-top:20px; margin-bottom:10px;'>"
                + "<label> The following are PAN block list :</label>"
                + "</div>"
                + "<div>"
                + "<table style='width:500px; font-family:Calibri; font-size:10pt; color:black;'>"
                + "<tr>"
                + "<td>Document# :</td>"
                + "<td>" + pDocId + "</td>"
                + "<td></td>"
                + "<td>Status :</td>"
                + "<td>" + pStatus + "</td>"
                + "</tr>"
                + "<tr>"
                + "<td>Date Created :</td>"
                + "<td>" + pDateCreated + "</td>"
                + "<td></td>"
                + "<td>Status Date :</td>"
                + "<td>" + pStatusDate + "</td>"
                + "</tr>"
                + "<tr>"
                + "<td>Created By :</td>"
                + "<td>" + pCreatedBy + "</td>"
                + "<td></td>"
                + "<td>Approved By :</td>"
                + "<td>" + pUserId + "</td>"
                + "</tr>"
                + "<tr></tr>"
                + "</table>"
                + "</div>"
                + "<div>"
                + "<table style='width:500px; font-family:Calibri; font-size:10pt; color:black; border: 1px solid #778899;padding: 1px;'>"
                + "<tr style='border-top: 1px thin #FFFFFF; border-left: 1px thin #FFFFFF; border-right: 1px #FFFFFF; background-color:#2F4F4F; color:#F0FFFF; text-align:center; font-size:11pt'>"
                + "<td style='padding-left:1px;padding-right:1px; border: 1px solid #778899;padding: 1px;'> No </td>"
                + "<td style='padding-left:1px;padding-right:1px; border: 1px solid #778899;padding: 1px;'> PAN </td>"
                + "<td style='padding-left:1px;padding-right:1px; border: 1px solid #778899;padding: 1px;'> Institution </td>"
                + "</tr>";

        try {
            listDetail = moduleFactory.getSequentialService().listBlockedAdministrationDetail(docId);
            int number = 1;
            for (BlockedAdministrationDetail list : listDetail) {
                result = result
                        + "<tr style='border-bottom: 1px solid #ddd; border-top: 1px solid #ddd; border-collapse: collapse;'>"
                        + "<td style='text-align:center; border: 1px solid #778899;padding: 1px;'>"
                        + number
                        + "</td><td style='text-align:left; border: 1px solid #778899;padding: 1px;'>"
                        + list.getPan()
                        + "</td><td style='text-align:left; border: 1px solid #778899;padding: 1px;'>"
                        + list.getBankName()
                        + "</td></tr>";
                number++;
            }

            result = result
                    + "</table>"
                    + "</div>"
                    + "<div style='margin-top:50px; margin-bottom:10px;'>";

            result = result
                    + "<label style='text-decoration: underline;'>Potential Risk Detection System</label><br/>"
                    + "<label>PT. Rintis Sejahtera</label><br/>"
                    + "<label>Gedung Prima Sejahtera, 8th Floor</label><br/>"
                    + "<label>Jalur Sutera Street Kav 5A, Alam Sutera</label><br/>"
                    + "<label>Kunciran - Pinang, Tangerang City, Banten</label><br/>"
                    + "<label>Telp +62 21 5208776 | Fax +62 21 5710288</label><br/>"
                    + "</div>"
                    + "<div style='margin-top:30px; margin-bottom:10px; color:#A9A9A9;'>"
                    + "<hr width=100% style='color:#d4d4d4;'>"
                    + "<label>NOTICE OF CONFIDENTIALITY: This email, and any attachments thereto, is intended for use only by authorized officer of the member bank - the addressee(s) named herein and may contain confidential information, legally privileged information and attorney-client work product. If you are not the intended recipient of this email, you are hereby notified that any dissemination, distribution or copying of this email, and any attachments thereto, is strictly prohibited. If you have received this email in error, please notify the sender by email, telephone or fax, and permanently delete the original and any of any email and printout thereof. Thank you. This is an auto-generated information email and no reply needed.</label><br/>"
                    + "</div>"
                    + "</body>"
                    + "</html>";

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public String cancelAction() {
        if(!status.equals("Created")){
            glowMessageInfo("Cancel Failed", "");
        }else{
            try {
                if(listDetail.size() > 0){
                    String emailTo = moduleFactory.getSequentialService().getEmailDocId(docId);
                    String userId = getUserInfoBean().getUserId();
                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat datefmt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    Date date = calendar.getTime();
                    String datetime = datefmt.format(date);
                    moduleFactory.getSequentialService().updatePanDetail(docId, "");
                    moduleFactory.getSequentialService().updatePanDetailManual(docId, "");
                    moduleFactory.getSequentialService().addPanBlockedHistory(docId, String.valueOf(datetime), userId, "Blocked Cancelled");
                    moduleFactory.getSequentialService().updateBlockedHeader(docId, String.valueOf(datetime), "Cancelled");
                    moduleFactory.getSequentialService().addPanBlockedApproval(docId, String.valueOf(datetime), userId, "Cancelled");
                    listDoc = moduleFactory.getSequentialService().listDocBlocked(status);
                    listDetail = moduleFactory.getSequentialService().listBlockedAdministrationDetail("");

                    glowMessageInfo("PAN Block Cancelled, Success", "");

                    String MAIL_FROM = ReadProperty.mailProperty().getMailFrom();
                    String MAIL_HOST = ReadProperty.mailProperty().getMailHost();
                    int MAIL_PORT = Integer.parseInt(ReadProperty.mailProperty().getMailPort());
                    String MAIL_SUBJECT = ReadProperty.mailProperty().getMailSubjectBlockApproveCancel();

                    Properties prop = System.getProperties();
                    prop.put("mail.smtp.host", MAIL_HOST);
                    prop.put("mail.smtp.port", MAIL_PORT);
                    prop.put("mail.debug", "false");
                    prop.put("mail.transport.protocol", "smtp");

                    Session session = Session.getDefaultInstance(prop);
                    MimeMessage msg = new MimeMessage(session);
                    msg.setFrom(new InternetAddress(MAIL_FROM));

                    msg.addRecipients(Message.RecipientType.TO, emailTo);

                    List <PanAnomalyEmailBean> recipientListCC = moduleFactory.getSequentialService().getEmail();
                    for (PanAnomalyEmailBean recipient : recipientListCC) {
                        msg.addRecipient(Message.RecipientType.CC, new InternetAddress(recipient.getUserEmail()));
                    }

                    msg.setContent(getMailBody(docId, userId), "text/html");
                    msg.setSubject(MAIL_SUBJECT + " - " + docId);
                    msg.setHeader("X-Priority", "1");
                    Transport transport = session.getTransport();
                    transport.send(msg);
                    log.info("send email block approval response");

                    listDoc = moduleFactory.getSequentialService().listDocBlocked(status);
                    listDetail = moduleFactory.getSequentialService().listBlockedAdministrationDetail("");

                }else{
                    glowMessageInfo("List Empty", "");
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                glowMessageInfo("Cancel Failed", "");
            }
        }
        return "";
    }

    public List<BlockedAdministrationHeader> getListHeader() {
        return listHeader;
    }

    public void setListHeader(List<BlockedAdministrationHeader> listHeader) {
        this.listHeader = listHeader;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public List<BlockedAdministrationHeader> getListDoc() {
        return listDoc;
    }

    public void setListDoc(List<BlockedAdministrationHeader> listDoc) {
        this.listDoc = listDoc;
    }

    public List<BlockedAdministrationDetail> getListDetail() {
        return listDetail;
    }

    public void setListDetail(List<BlockedAdministrationDetail> listDetail) {
        this.listDetail = listDetail;
    }

    public String getpDateCreated() {
        return pDateCreated;
    }

    public void setpDateCreated(String pDateCreated) {
        this.pDateCreated = pDateCreated;
    }

    public String getpCreatedBy() {
        return pCreatedBy;
    }

    public void setpCreatedBy(String pCreatedBy) {
        this.pCreatedBy = pCreatedBy;
    }

    public String getpStatus() {
        return pStatus;
    }

    public void setpStatus(String pStatus) {
        this.pStatus = pStatus;
    }

    public String getpStatusDate() {
        return pStatusDate;
    }

    public void setpStatusDate(String pStatusDate) {
        this.pStatusDate = pStatusDate;
    }

    public String getpApprovedBy() {
        return pApprovedBy;
    }

    public void setpApprovedBy(String pApprovedBy) {
        this.pApprovedBy = pApprovedBy;
    }

    public String getpDocId() {
        return pDocId;
    }

    public void setpDocId(String pDocId) {
        this.pDocId = pDocId;
    }
}

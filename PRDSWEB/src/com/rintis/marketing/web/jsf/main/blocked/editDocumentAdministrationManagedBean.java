package com.rintis.marketing.web.jsf.main.blocked;

import com.rintis.marketing.beans.bean.bi.PanAnomalyEmailBean;
import com.rintis.marketing.beans.bean.blocked.BlockedAdministrationDetail;
import com.rintis.marketing.beans.bean.blocked.BlockedAdministrationHeader;
import com.rintis.marketing.config.spring.ReadProperty;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import org.hibernate.SessionFactory;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Properties;

@ManagedBean
@ViewScoped
public class editDocumentAdministrationManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private SessionFactory sessionFactory;
    private List<BlockedAdministrationHeader> listHeader;
    private List<BlockedAdministrationDetail> listDetail;
    private BlockedAdministrationDetail selectedDeleteItem;
    private String pDocId, pDateCreated,pCreatedBy,pStatus, pStatusDate;

    public editDocumentAdministrationManagedBean() {}

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "060103";
            pageTitle = getMenuName(moduleFactory, menuId);
            pDocId = paramEncode(getRequestParameterMap().get("pDocId").toString());
            listHeader = moduleFactory.getSequentialService().listBlockedAdministrationHeader(pDocId);
            if(!listHeader.isEmpty()){
                pDateCreated = listHeader.get(0).getCreatedDate();
                pCreatedBy = listHeader.get(0).getCreatedBy();
                pStatus = listHeader.get(0).getStatus();
                pStatusDate = listHeader.get(0).getStatusDate();
            }

            listDetail = moduleFactory.getSequentialService().listBlockedAdministrationDetail(pDocId);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    public String paramEncode(String data){
        //encrypt param
        if(!data.equals(null)){
            data = HttpUtils.encodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }

    public void deleteActionListener(ActionEvent event) {
        try {
            moduleFactory.getSequentialService().deleteBlockedAdministrationDetail(selectedDeleteItem.getDocId(), selectedDeleteItem.getPan());
            listDetail = moduleFactory.getSequentialService().listBlockedAdministrationDetail(pDocId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String approveAction() {
        try {
            if(listDetail.size() > 0){
                String userId = getUserInfoBean().getUserId();
                String emailCC = moduleFactory.getLoginService().getEmail(userId);
                moduleFactory.getSequentialService().addPanBlockedHistory(pDocId, pDateCreated, userId, "Blocked Selected");
                moduleFactory.getSequentialService().updateBlockedNotesHeader(pDocId, "Request");

                String MAIL_FROM = ReadProperty.mailProperty().getMailFrom();
                String MAIL_HOST = ReadProperty.mailProperty().getMailHost();
                int MAIL_PORT = Integer.parseInt(ReadProperty.mailProperty().getMailPort());
                String MAIL_SUBJECT = ReadProperty.mailProperty().getMailSubjectBlock();

                Properties prop = System.getProperties();
                prop.put("mail.smtp.host", MAIL_HOST);
                prop.put("mail.smtp.port", MAIL_PORT);
                prop.put("mail.debug", "false");
                prop.put("mail.transport.protocol", "smtp");

                Session session = Session.getDefaultInstance(prop);
                MimeMessage msg = new MimeMessage(session);
                msg.setFrom(new InternetAddress(MAIL_FROM));

                List <PanAnomalyEmailBean> recipientListTO = moduleFactory.getSequentialService().getEmail();
                for (PanAnomalyEmailBean recipient : recipientListTO) {
                    msg.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient.getUserEmail()));
                }

                msg.addRecipients(Message.RecipientType.CC, emailCC);
                msg.setContent(getMailBody(), "text/html");
                msg.setSubject(MAIL_SUBJECT + " - " + pDocId);
                msg.setHeader("X-Priority", "1");
                Transport transport = session.getTransport();
                transport.send(msg);
                log.info("send email approval request");

            }else{
                glowMessageInfo("List Empty", "");
                return "";
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            glowMessageInfo("Approval Request Failed", "");
        }

        return PageConstant.PAGE_BLOCKED_DOCUMENT_ADMINISTRATION + "?faces-redirect=true";
    }

    public String cancelAction() {
        try {
            moduleFactory.getSequentialService().updatePanDetail(pDocId, "");
            moduleFactory.getSequentialService().deleteBlockedAdministrationDetail(pDocId);
            moduleFactory.getSequentialService().deleteBlockedAdministrationHeader(pDocId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            glowMessageInfo("Approval Request Failed", "");
        }

        return PageConstant.PAGE_BLOCKED_DOCUMENT_ADMINISTRATION + "?faces-redirect=true";
    }

    public String exitAction() {
        return PageConstant.PAGE_BLOCKED_DOCUMENT_ADMINISTRATION + "?faces-redirect=true";
    }

    public String getMailBody(){

        String result = "<html>"
                + "<body style='font-family:Calibri; font-size:12pt'>"
                + "<div>"
                + "<label>Dear All, </label>"
                + "</div>"
                + "<div style='margin-top:20px; margin-bottom:10px;'>"
                + "<label> The following are PAN block list :</label>"
                + "</div>"
                + "<div>"
                + "<table style='width:500px; font-family:Calibri; font-size:10pt; color:black;'>"
                + "<tr>"
                + "<td>Document# :</td>"
                + "<td>" + pDocId + "</td>"
                + "<td></td>"
                + "<td>Status :</td>"
                + "<td>" + pStatus + "</td>"
                + "</tr>"
                + "<tr>"
                + "<td>Date Created :</td>"
                + "<td>" + pDateCreated + "</td>"
                + "<td></td>"
                + "<td>Status Date :</td>"
                + "<td>" + pStatusDate + "</td>"
                + "</tr>"
                + "<tr>"
                + "<td>Created By :</td>"
                + "<td>" + pCreatedBy + "</td>"
                + "<td></td>"
                + "<td></td>"
                + "<td></td>"
                + "</tr>"
                + "<tr></tr>"
                + "</table>"
                + "</div>"
                + "<div>"
                + "<table style='width:500px; font-family:Calibri; font-size:10pt; color:black; border: 1px solid #778899;padding: 1px;'>"
                + "<tr style='border-top: 1px thin #FFFFFF; border-left: 1px thin #FFFFFF; border-right: 1px #FFFFFF; background-color:#2F4F4F; color:#F0FFFF; text-align:center; font-size:11pt'>"
                + "<td style='padding-left:1px;padding-right:1px; border: 1px solid #778899;padding: 1px;'> No </td>"
                + "<td style='padding-left:1px;padding-right:1px; border: 1px solid #778899;padding: 1px;'> PAN </td>"
                + "<td style='padding-left:1px;padding-right:1px; border: 1px solid #778899;padding: 1px;'> Institution </td>"
                + "</tr>";

        try {
            listDetail = moduleFactory.getSequentialService().listBlockedAdministrationDetail(pDocId);
            int number = 1;
            for (BlockedAdministrationDetail list : listDetail) {
                result = result
                        + "<tr style='border-bottom: 1px solid #ddd; border-top: 1px solid #ddd; border-collapse: collapse;'>"
                        + "<td style='text-align:center; border: 1px solid #778899;padding: 1px;'>"
                        + number
                        + "</td><td style='text-align:left; border: 1px solid #778899;padding: 1px;'>"
                        + list.getPan()
                        + "</td><td style='text-align:left; border: 1px solid #778899;padding: 1px;'>"
                        + list.getBankName()
                        + "</td></tr>";
                number++;
            }

            result = result
                    + "</table>"
                    + "</div>"
                    + "<div style='margin-top:50px; margin-bottom:10px;'>";

            result = result
                    + "<label style='text-decoration: underline;'>Potential Risk Detection System</label><br/>"
                    + "<label>PT. Rintis Sejahtera</label><br/>"
                    + "<label>Gedung Prima Sejahtera, 8th Floor</label><br/>"
                    + "<label>Jalur Sutera Street Kav 5A, Alam Sutera</label><br/>"
                    + "<label>Kunciran - Pinang, Tangerang City, Banten</label><br/>"
                    + "<label>Telp +62 21 5208776 | Fax +62 21 5710288</label><br/>"
                    + "</div>"
                    + "<div style='margin-top:30px; margin-bottom:10px; color:#A9A9A9;'>"
                    + "<hr width=100% style='color:#d4d4d4;'>"
                    + "<label>NOTICE OF CONFIDENTIALITY: This email, and any attachments thereto, is intended for use only by authorized officer of the member bank - the addressee(s) named herein and may contain confidential information, legally privileged information and attorney-client work product. If you are not the intended recipient of this email, you are hereby notified that any dissemination, distribution or copying of this email, and any attachments thereto, is strictly prohibited. If you have received this email in error, please notify the sender by email, telephone or fax, and permanently delete the original and any of any email and printout thereof. Thank you. This is an auto-generated information email and no reply needed.</label><br/>"
                    + "</div>"
                    + "</body>"
                    + "</html>";

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<BlockedAdministrationHeader> getListHeader() {
        return listHeader;
    }

    public void setListHeader(List<BlockedAdministrationHeader> listHeader) {
        this.listHeader = listHeader;
    }

    public List<BlockedAdministrationDetail> getListDetail() {
        return listDetail;
    }

    public void setListDetail(List<BlockedAdministrationDetail> listDetail) {
        this.listDetail = listDetail;
    }

    public BlockedAdministrationDetail getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(BlockedAdministrationDetail selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }

    public String getpDocId() {
        return pDocId;
    }

    public void setpDocId(String pDocId) {
        this.pDocId = pDocId;
    }

    public String getpDateCreated() {
        return pDateCreated;
    }

    public void setpDateCreated(String pDateCreated) {
        this.pDateCreated = pDateCreated;
    }

    public String getpCreatedBy() {
        return pCreatedBy;
    }

    public void setpCreatedBy(String pCreatedBy) {
        this.pCreatedBy = pCreatedBy;
    }

    public String getpStatus() {
        return pStatus;
    }

    public void setpStatus(String pStatus) {
        this.pStatus = pStatus;
    }

    public String getpStatusDate() {
        return pStatusDate;
    }

    public void setpStatusDate(String pStatusDate) {
        this.pStatusDate = pStatusDate;
    }
}

package com.rintis.marketing.web.jsf.main.dashboard;

import com.martinlinha.c3faces.model.C3DataSet;
import com.martinlinha.c3faces.model.C3DataSetString;
import com.martinlinha.c3faces.model.C3ViewDataSet;
import com.martinlinha.c3faces.model.C3ViewDataSetString;
import com.martinlinha.c3faces.script.property.Axis;
import com.martinlinha.c3faces.script.property.Data;
import com.martinlinha.c3faces.script.property.Point;
import com.rintis.marketing.beans.bean.bi.C3ChartBean;
import com.rintis.marketing.beans.bean.bi.EsqTrx5MinBean;
import com.rintis.marketing.beans.bean.bi.UserDashboardDetailBean;
import com.rintis.marketing.beans.bean.monitor.TableEsqTrxIdBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.MathUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.faces.bean.ManagedProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public abstract class AbstractUserDashboardHistory extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    protected ModuleFactory moduleFactory;

    protected final int TIMEFRAME_5M = 5;
    protected final int TIMEFRAME_15M = 15;
    protected final int TIMEFRAME_30M = 30;
    protected final int TIMEFRAME_1H = 60;
    protected final int TIMEFRAME_1D = 1440;

    protected int defaultRangeInHour;
    protected Date selectedEndDate;
    protected Integer selectedEndHour;
    protected Integer selectedEndMinute;
    protected Date selectedStartDate;
    protected Integer selectedStartHour;
    protected Integer selectedStartMinute;
    protected List<C3ChartBean> listC3Chart;
    protected Boolean[] activePrev;
    protected Boolean[] activePercentPrev;
    protected boolean showPercent;
    protected String showPercentButtonText;
    protected boolean showPrevData;
    protected Date selectedPrevDate;
    protected List<TableEsqTrxIdBean> listTableData;
    protected String maxDate;


    protected String[] renderChartArray;
    protected C3ChartBean[] chartBeanArray;




    protected void renderChart(UserDashboardDetailBean detail) throws Exception {
        String selectedBankId = detail.getBankid();
        String selectedTrxId = detail.getTransactionid();
        String selectedTransactionIndicatorId = detail.getTransactionindicatorid();
        String aggregateTimeFrame = detail.getMonitoringinterval();

        String renderAcqOnly = "false";
        String renderIssOnly = "false";
        String renderBnfOnly = "false";
        String renderAcqIss = "false";
        String renderAcqBnf = "false";
        String renderIssBnf = "false";

        if (selectedTrxId.equalsIgnoreCase(DataConstant.ALL)) {
            renderAcqOnly = "true";
            renderIssOnly = "true";
            renderBnfOnly = "true";
            renderAcqIss = "true";
            renderAcqBnf = "true";
            renderIssBnf = "true";

        } else {
            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ONLY)) {
                renderAcqOnly = "true";
            }
            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_ONLY)) {
                renderIssOnly = "true";
            }
            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__BNF_ONLY)) {
                renderBnfOnly = "true";
            }
            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ISS)) {
                renderAcqIss = "true";
            }
            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_BNF)) {
                renderAcqBnf = "true";
            }
            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_BNF)) {
                renderIssBnf = "true";
            }
        }


        Date thdate = null;
        Date frdate = null;
        String sminref = "";
        String tableName = "";
        Date rSelectedEndDate = selectedEndDate;
        Date rSelectedStartDate = selectedStartDate;
        String lastAggr = "";
        int timeFrame = 0;
        //log.info("log-01");
        if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T5M)) {
            timeFrame = TIMEFRAME_5M;
            sminref = "23:55";
            tableName = DataConstant.TABLE_TRX5MIN;
            defaultRangeInHour = Integer.parseInt(moduleFactory.getCommonService().getSystemPropertyUserDefault(
                    getUserInfoBean().getUserId(), DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_5M));

        } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T15M)) {
            timeFrame = TIMEFRAME_15M;
            sminref = "23:45";
            tableName = DataConstant.TABLE_TRX15MIN;
            defaultRangeInHour = Integer.parseInt(moduleFactory.getCommonService().getSystemPropertyUserDefault(
                    getUserInfoBean().getUserId(), DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_15M));

        } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T30M)) {
            timeFrame = TIMEFRAME_30M;
            sminref = "23:30";
            tableName = DataConstant.TABLE_TRX30MIN;
            defaultRangeInHour = Integer.parseInt(moduleFactory.getCommonService().getSystemPropertyUserDefault(
                    getUserInfoBean().getUserId(), DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_30M));

        } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T1H)) {
            timeFrame = TIMEFRAME_1H;
            sminref = "23:00";
            tableName = DataConstant.TABLE_TRX1HOUR;
            defaultRangeInHour = Integer.parseInt(moduleFactory.getCommonService().getSystemPropertyUserDefault(
                    getUserInfoBean().getUserId(), DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_HOURLY));

        } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T1D)) {
            timeFrame = TIMEFRAME_1D;
            sminref = "00:00";
            tableName = DataConstant.TABLE_TRX1DAY;
            defaultRangeInHour = Integer.parseInt(moduleFactory.getCommonService().getSystemPropertyUserDefault(
                    getUserInfoBean().getUserId(), DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_1D));

        }
        //log.info("log-02");

        lastAggr = moduleFactory.getAggregateService().getAggrLastTime(selectedTrxId, aggregateTimeFrame);
        /*thdate = DateUtils.convertDate(lastAggr, DateUtils.DATE_YYMMDD_HHMM);
        frdate = DateUtils.addDate(Calendar.HOUR_OF_DAY, thdate, defaultRangeInHour*-1);
        //log.info("log-03");

        selectedStartDate = frdate;
        selectedStartHour = DateUtils.getHour(frdate);
        selectedStartMinute = DateUtils.getMinute(frdate);

        selectedEndDate = thdate;
        selectedEndHour = DateUtils.getHour(thdate);
        selectedEndMinute = DateUtils.getMinute(thdate);

        if (selectedPrevDate == null) {
            selectedPrevDate = DateUtils.addDate(Calendar.DAY_OF_MONTH, selectedEndDate, -1);
        } else {
            selectedPrevDate = DateUtils.setDate(Calendar.HOUR_OF_DAY, selectedPrevDate, DateUtils.getHour(thdate));
            selectedPrevDate = DateUtils.setDate(Calendar.MINUTE, selectedPrevDate, DateUtils.getMinute(thdate));
            selectedPrevDate = DateUtils.setDate(Calendar.SECOND, selectedPrevDate, 0);
        }*/
        rSelectedEndDate = DateUtils.setHour(rSelectedEndDate, 0);
        rSelectedEndDate = DateUtils.setMinute(rSelectedEndDate, 0);
        rSelectedEndDate = DateUtils.setSecond(rSelectedEndDate, 0);
        if (timeFrame == TIMEFRAME_1D) {
            rSelectedEndDate = DateUtils.setHour(rSelectedEndDate, 0);
            rSelectedEndDate = DateUtils.setMinute(rSelectedEndDate, 0);
        } else {
            rSelectedEndDate = DateUtils.setHour(rSelectedEndDate, selectedEndHour);
            rSelectedEndDate = DateUtils.setMinute(rSelectedEndDate, selectedEndMinute);
        }
        thdate = rSelectedEndDate;

        rSelectedStartDate = rSelectedEndDate;
        rSelectedStartDate = DateUtils.setHour(rSelectedStartDate, 0);
        rSelectedStartDate = DateUtils.setMinute(rSelectedStartDate, 0);
        rSelectedStartDate = DateUtils.setSecond(rSelectedStartDate, 0);
        if (timeFrame == TIMEFRAME_1D) {
            rSelectedStartDate = DateUtils.setHour(rSelectedStartDate, 0);
            rSelectedStartDate = DateUtils.setMinute(rSelectedStartDate, 0);
            rSelectedStartDate = DateUtils.addDate(Calendar.HOUR_OF_DAY, rSelectedStartDate, defaultRangeInHour * -1);
        } else {
            rSelectedStartDate = DateUtils.setHour(rSelectedStartDate, selectedStartHour);
            rSelectedStartDate = DateUtils.setMinute(rSelectedStartDate, selectedStartMinute);
        }
        frdate = rSelectedStartDate;

        Date lastthdate = DateUtils.convertDate(lastAggr, DateUtils.DATE_YYMMDD_HHMM);
        if (lastthdate.getTime() < thdate.getTime()) {
            rSelectedEndDate = lastthdate;
            thdate = rSelectedEndDate;
            //selectedEndHour = DateUtils.getHour(thdate);
            //selectedEndMinute = DateUtils.getMinute(thdate);
        }
        //log.info("log-04");


        //EisBank bank = moduleFactory.getMasterService().getBank(selectedBankId);
        String selectedBankName = detail.getBankname();
        //EsqTrxIdBean trx = moduleFactory.getMasterService().getEsqTrxId(selectedTrxId);
        String selectedTransactionName = detail.getTransactionname();
        if (selectedTransactionName == null) {
            selectedTransactionName = "All";
        }
        //EsqTrxIdBean trxIndicator = moduleFactory.getMasterService().getEsqTrxIdIndicator(selectedTransactionIndicatorId);
        String selectedTransactionIndicatorName = detail.getTransactionindicatorname();
        String monitoringInterval = detail.getMonitoringinterval();

        //String seriesColor = "";

        //ChartSeries cs = new ChartSeries();
        //ChartSeries csPctAppr = new ChartSeries();
        //ChartSeries csPsDecl = new ChartSeries();

        Data data = new Data();
        List<String> listDateString = new ArrayList<>();
        List<Integer> listDataTotalTransaksi = new ArrayList<>();
        List<Integer> listData = new ArrayList<>();

        List<Integer> listDataTotalApprove = new ArrayList<>();
        List<Integer> listDataTotalDeclineCharge = new ArrayList<>();
        List<Integer> listDataTotalDeclineFree = new ArrayList<>();
        List<BigDecimal> listDataPercentAppr = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclC = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclF = new ArrayList<>();

        List<Integer> listDataTotalTransaksiPrev = new ArrayList<>();
        List<Integer> listDataTotalApprovePrev = new ArrayList<>();
        List<Integer> listDataTotalDeclineChargePrev = new ArrayList<>();
        List<Integer> listDataTotalDeclineFreePrev = new ArrayList<>();
        List<BigDecimal> listDataPercentApprPrev = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclCPrev = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclFPrev = new ArrayList<>();


        /*Date thdate = selectedEndDate;
        thdate = DateUtils.setDate(Calendar.HOUR_OF_DAY, thdate, selectedEndHour);
        thdate = DateUtils.setDate(Calendar.MINUTE, thdate, selectedEndMinute);

        Date frdate = selectedStartDate;
        frdate = DateUtils.setDate(Calendar.HOUR_OF_DAY, frdate, selectedStartHour);
        frdate = DateUtils.setDate(Calendar.MINUTE, frdate, selectedStartMinute);
        frdate = DateUtils.addDate(Calendar.MINUTE, frdate, -5);*/

        int prevDay = (int)DateUtils.getDifferenceDays(selectedPrevDate, thdate);
        prevDay = Math.abs(prevDay);
        //log.info("log-06");

        List<EsqTrx5MinBean> list = new ArrayList<>();
        Date sd = frdate;
        //int idx = 0;
        while (true) {
            //if (idx == 287) {
            //    log.info(idx);
            //}
            String period = DateUtils.convertDate(sd, DateUtils.DATE_YYMMDD);
            String periodPrev = DateUtils.convertDate(DateUtils.addDate(Calendar.DAY_OF_MONTH, sd, -1 * prevDay) , DateUtils.DATE_YYMMDD);
            Date ed = DateUtils.addDate(Calendar.MINUTE, sd, timeFrame);
            String sminfr = DateUtils.convertDate(sd, DateUtils.DATE_HHMM);
            String sminto = DateUtils.convertDate(ed, DateUtils.DATE_HHMM);

            if (sminfr.equalsIgnoreCase(sminref)) {
                sminto = "24:00";
            }
            EsqTrx5MinBean esqTrx5MinBean = null;
            //log.info("log-07");
            if (showPrevData) {
                esqTrx5MinBean = moduleFactory.getBiService().getEsqTrxWithPrev(tableName, selectedBankId, selectedTrxId,
                        period, periodPrev, sminfr, sminto);
            } else {
                esqTrx5MinBean = moduleFactory.getBiService().getEsqTrx(tableName, selectedBankId, selectedTrxId,
                        period, sminfr, sminto);
            }
            //log.info("log-08");
            if (esqTrx5MinBean == null) {
                esqTrx5MinBean = new EsqTrx5MinBean();
                esqTrx5MinBean.setPeriod(period);
                esqTrx5MinBean.setBankid(selectedBankId);
                esqTrx5MinBean.setTrxid(selectedTrxId);
                esqTrx5MinBean.setTimefr(sminfr);
                esqTrx5MinBean.setTimeto(sminto);

            }
            list.add(esqTrx5MinBean);
            //}

            if (ed.getTime() >= thdate.getTime()) {
                break;
            }

            sd = DateUtils.addDate(Calendar.MINUTE, sd, timeFrame);
            //idx++;
        };

        //log.info("log-09");
        TableEsqTrxIdBean tableEsqTrxIdBean = new TableEsqTrxIdBean();
        tableEsqTrxIdBean.setTypeInterval(DataConstant.MONITORING_INTERVAL_5MIN);
        tableEsqTrxIdBean.setUserDashboardDetailBean(detail);
        tableEsqTrxIdBean.setListData(list);
        tableEsqTrxIdBean.setRenderAcqOnly(renderAcqOnly);
        tableEsqTrxIdBean.setRenderIssOnly(renderIssOnly);
        tableEsqTrxIdBean.setRenderBnfOnly(renderBnfOnly);
        tableEsqTrxIdBean.setRenderAcqIss(renderAcqIss);
        tableEsqTrxIdBean.setRenderAcqBnf(renderAcqBnf);
        tableEsqTrxIdBean.setRenderIssBnf(renderIssBnf);
        listTableData.add(tableEsqTrxIdBean);
        //log.info("log-10");

        boolean toggle = true;
        for(EsqTrx5MinBean esq : list) {
            Date d = DateUtils.convertDate(esq.getPeriod() + " " + esq.getTimeto(), DateUtils.DATE_YYMMDD_HHMM);

            if (timeFrame == TIMEFRAME_1D) {
                listDateString.add(DateUtils.convertDate(d, DateUtils.DATE_DDMMM));
            } else {
                listDateString.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ONLY)) {
                BigDecimal b = MathUtils.checkNull(esq.getFreq_acq_app())
                        .add(MathUtils.checkNull(esq.getFreq_acq_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_acq_df()));

                BigDecimal bPrev = MathUtils.checkNull(esq.getFreq_acq_app_prev())
                        .add(MathUtils.checkNull(esq.getFreq_acq_dc_prev()))
                        .add(MathUtils.checkNull(esq.getFreq_acq_df_prev()));

                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acq_app()), b);
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acq_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acq_df(), b);

                BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acq_app_prev()), bPrev);
                BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_acq_dc_prev(), bPrev);
                BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_acq_df_prev(), bPrev);

                if (toggle) {
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_acq_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_acq_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_acq_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));

                    listDataTotalTransaksiPrev.add(bPrev.intValue());
                    listDataTotalApprovePrev.add(MathUtils.checkNull(esq.getFreq_acq_app_prev()).intValue());
                    listDataTotalDeclineChargePrev.add(MathUtils.checkNull(esq.getFreq_acq_dc_prev()).intValue());
                    listDataTotalDeclineFreePrev.add(MathUtils.checkNull(esq.getFreq_acq_df_prev()).intValue());
                    listDataPercentApprPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclCPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclFPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));
                } else {
                    listDataTotalTransaksi.add(0);
                }
            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_ONLY)) {
                BigDecimal b = MathUtils.checkNull(esq.getFreq_iss_app())
                        .add(MathUtils.checkNull(esq.getFreq_iss_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_iss_df()));

                BigDecimal bPrev = MathUtils.checkNull(esq.getFreq_iss_app_prev())
                        .add(MathUtils.checkNull(esq.getFreq_iss_dc_prev()))
                        .add(MathUtils.checkNull(esq.getFreq_iss_df_prev()));

                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_iss_app()), b);
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_iss_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_iss_df(), b);

                BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_iss_app_prev()), bPrev);
                BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_iss_dc_prev(), bPrev);
                BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_iss_df_prev(), bPrev);

                if (toggle) {
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_iss_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_iss_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_iss_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));

                    listDataTotalTransaksiPrev.add(bPrev.intValue());
                    listDataTotalApprovePrev.add(MathUtils.checkNull(esq.getFreq_iss_app_prev()).intValue());
                    listDataTotalDeclineChargePrev.add(MathUtils.checkNull(esq.getFreq_iss_dc_prev()).intValue());
                    listDataTotalDeclineFreePrev.add(MathUtils.checkNull(esq.getFreq_iss_df_prev()).intValue());
                    listDataPercentApprPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclCPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclFPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));
                } else {
                    listDataTotalTransaksi.add(0);
                }

            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__BNF_ONLY)) {
                BigDecimal b = MathUtils.checkNull(esq.getFreq_bnf_app())
                        .add(MathUtils.checkNull(esq.getFreq_bnf_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_bnf_df()));

                BigDecimal bPrev = MathUtils.checkNull(esq.getFreq_bnf_app_prev())
                        .add(MathUtils.checkNull(esq.getFreq_bnf_dc_prev()))
                        .add(MathUtils.checkNull(esq.getFreq_bnf_df_prev()));

                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_bnf_app()), b);
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_bnf_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_bnf_df(), b);

                BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_bnf_app_prev()), bPrev);
                BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_bnf_dc_prev(), bPrev);
                BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_bnf_df_prev(), bPrev);

                if (toggle) {
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_bnf_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_bnf_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_bnf_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));

                    listDataTotalTransaksiPrev.add(bPrev.intValue());
                    listDataTotalApprovePrev.add(MathUtils.checkNull(esq.getFreq_bnf_app_prev()).intValue());
                    listDataTotalDeclineChargePrev.add(MathUtils.checkNull(esq.getFreq_bnf_dc_prev()).intValue());
                    listDataTotalDeclineFreePrev.add(MathUtils.checkNull(esq.getFreq_bnf_df_prev()).intValue());
                    listDataPercentApprPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclCPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclFPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));

                } else {
                    listDataTotalTransaksi.add(0);
                }

            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ISS)) {
                BigDecimal b = MathUtils.checkNull(esq.getFreq_acqIss_app())
                        .add(MathUtils.checkNull(esq.getFreq_acqIss_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_acqIss_df()));

                BigDecimal bPrev = MathUtils.checkNull(esq.getFreq_acqIss_app_prev())
                        .add(MathUtils.checkNull(esq.getFreq_acqIss_dc_prev()))
                        .add(MathUtils.checkNull(esq.getFreq_acqIss_df_prev()));

                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqIss_app()), b);
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acqIss_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acqIss_df(), b);

                BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqIss_app_prev()), bPrev);
                BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_acqIss_dc_prev(), bPrev);
                BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_acqIss_df_prev(), bPrev);

                if (toggle) {
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_acqIss_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_acqIss_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_acqIss_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));

                    listDataTotalTransaksiPrev.add(bPrev.intValue());
                    listDataTotalApprovePrev.add(MathUtils.checkNull(esq.getFreq_acqIss_app_prev()).intValue());
                    listDataTotalDeclineChargePrev.add(MathUtils.checkNull(esq.getFreq_acqIss_dc_prev()).intValue());
                    listDataTotalDeclineFreePrev.add(MathUtils.checkNull(esq.getFreq_acqIss_df_prev()).intValue());
                    listDataPercentApprPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclCPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclFPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));

                } else {
                    listDataTotalTransaksi.add(0);
                }

            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_BNF)) {
                BigDecimal b = MathUtils.checkNull(esq.getFreq_acqBnf_app())
                        .add(MathUtils.checkNull(esq.getFreq_acqBnf_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_acqBnf_df()));

                BigDecimal bPrev = MathUtils.checkNull(esq.getFreq_acqBnf_app_prev())
                        .add(MathUtils.checkNull(esq.getFreq_acqBnf_dc_prev()))
                        .add(MathUtils.checkNull(esq.getFreq_acqBnf_df_prev()));

                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqBnf_app()), b);
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_df(), b);

                BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqBnf_app_prev()), bPrev);
                BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_dc_prev(), bPrev);
                BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_df_prev(), bPrev);

                if (toggle) {
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_acqBnf_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_acqBnf_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_acqBnf_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));

                    listDataTotalTransaksiPrev.add(bPrev.intValue());
                    listDataTotalApprovePrev.add(MathUtils.checkNull(esq.getFreq_acqBnf_app_prev()).intValue());
                    listDataTotalDeclineChargePrev.add(MathUtils.checkNull(esq.getFreq_acqBnf_dc_prev()).intValue());
                    listDataTotalDeclineFreePrev.add(MathUtils.checkNull(esq.getFreq_acqBnf_df_prev()).intValue());
                    listDataPercentApprPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclCPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclFPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));

                } else {
                    listDataTotalTransaksi.add(0);
                }

            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_BNF)) {
                BigDecimal b = MathUtils.checkNull(esq.getFreq_issBnf_app())
                        .add(MathUtils.checkNull(esq.getFreq_issBnf_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_issBnf_df()));

                BigDecimal bPrev = MathUtils.checkNull(esq.getFreq_issBnf_app_prev())
                        .add(MathUtils.checkNull(esq.getFreq_issBnf_dc_prev()))
                        .add(MathUtils.checkNull(esq.getFreq_issBnf_df_prev()));

                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_issBnf_app()), b);
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_issBnf_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_issBnf_df(), b);

                BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_issBnf_app_prev()), bPrev);
                BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_issBnf_dc_prev(), bPrev);
                BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_issBnf_df_prev(), bPrev);

                if (toggle) {
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_issBnf_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_issBnf_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_issBnf_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));

                    listDataTotalTransaksiPrev.add(bPrev.intValue());
                    listDataTotalApprovePrev.add(MathUtils.checkNull(esq.getFreq_issBnf_app_prev()).intValue());
                    listDataTotalDeclineChargePrev.add(MathUtils.checkNull(esq.getFreq_issBnf_dc_prev()).intValue());
                    listDataTotalDeclineFreePrev.add(MathUtils.checkNull(esq.getFreq_issBnf_df_prev()).intValue());
                    listDataPercentApprPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclCPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclFPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));

                } else {
                    listDataTotalTransaksi.add(0);
                }

            }
        }
        //log.info("log-11");

        //LineChartModel dataReport = new LineChartModel();
        String title = selectedBankName + " - " + selectedTransactionName + " - " +
                selectedTransactionIndicatorName + " - " + DataConstant.getTimeFrameName(monitoringInterval);
        //generateChart(title, dataReport, cs, seriesColor);

        //listLineChardModel.add(dataReport);


        String colorTotal = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_TOTAL);
        String colorApprove = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_APPR);
        String colorDc = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_DC);
        String colorDf = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_DF);

        String colorTotalPrev = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_TOTAL_PREV);
        String colorApprovePrev = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_APPR_PREV);
        String colorDcPrev = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_DC_PREV);
        String colorDfPrev = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_DF_PREV);

        //String colorPctAppr = "0000F0";
        //String colorPctDecl = "00FF0F";
        //log.info("log-12");

        if (!showPercent) {
            generateChart(listDataTotalTransaksi,
                    listDataTotalApprove,
                    listDataTotalDeclineCharge,
                    listDataTotalDeclineFree,
                    listDataTotalTransaksiPrev,
                    listDataTotalApprovePrev,
                    listDataTotalDeclineChargePrev,
                    listDataTotalDeclineFreePrev,
                    listDateString,
                    colorTotal, colorApprove, colorDc, colorDf,
                    colorTotalPrev, colorApprovePrev, colorDcPrev, colorDfPrev,
                    title,
                    DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                    DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                    timeFrame);
            //log.info("log-13");
        } else {
            List<DsBean> listDsBean = new ArrayList<>();

            if (activePercentPrev[0]) {
                DsBean dsBean = new DsBean();
                dsBean.setListData(listDataPercentAppr);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[1]) {
                DsBean dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclC);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[2]) {
                DsBean dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclF);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[0]) {
                DsBean dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprPrev);
                dsBean.setColor(colorApprovePrev);
                dsBean.setName("% Approval Prev");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[1]) {
                DsBean dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCPrev);
                dsBean.setColor(colorDcPrev);
                dsBean.setName("% Decline Charge Prev ");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[2]) {
                DsBean dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFPrev);
                dsBean.setColor(colorDfPrev);
                dsBean.setName("% Decline Free Prev ");
                listDsBean.add(dsBean);
            }
            //log.info("log-14");

            generateChartMultiData(listDsBean, listDateString, title, timeFrame);
            //log.info("log-15");
        }
    }


    protected void generateChart(List<Integer> listDataTotalTransaksi,
                               List<Integer> listDataTotalApprove,
                               List<Integer> listDataTotalDeclineCharge,
                               List<Integer> listDataTotalDeclineFree,
                               List<Integer> listDataTotalTransaksiPrev,
                               List<Integer> listDataTotalApprovePrev,
                               List<Integer> listDataTotalDeclineChargePrev,
                               List<Integer> listDataTotalDeclineFreePrev,
                               List<String> listDateString,
                               String colorTotal, String colorApprove, String colorDc, String colorDf,
                               String colorTotalPrev, String colorApprovePrev, String colorDcPrev, String colorDfPrev,
                               String title,
                               String dsNameTotal, String dsNameAppr, String dsNameDc, String dsNameDf,
                               int timeframe) {
        C3DataSet dsTotal = new C3DataSet(listDataTotalTransaksi);
        C3ViewDataSet dataSetTotal = new C3ViewDataSet(dsNameTotal, dsTotal, "#"+colorTotal);

        C3DataSet dsAppr = new C3DataSet(listDataTotalApprove);
        C3ViewDataSet dataSetAppr = new C3ViewDataSet(dsNameAppr, dsAppr, "#"+colorApprove);

        C3DataSet dsDc = new C3DataSet(listDataTotalDeclineCharge);
        C3ViewDataSet dataSetDc = new C3ViewDataSet(dsNameDc, dsDc, "#"+colorDc);

        C3DataSet dsDf = new C3DataSet(listDataTotalDeclineFree);
        C3ViewDataSet dataSetDf = new C3ViewDataSet(dsNameDf, dsDf, "#"+colorDf);

        C3DataSet dsTotalPrev = new C3DataSet(listDataTotalTransaksiPrev);
        C3ViewDataSet dataSetTotalPrev = new C3ViewDataSet(dsNameTotal + " Prev", dsTotalPrev, "#"+colorTotalPrev);

        C3DataSet dsApprPrev = new C3DataSet(listDataTotalApprovePrev);
        C3ViewDataSet dataSetApprPrev = new C3ViewDataSet(dsNameAppr + " Prev", dsApprPrev, "#"+colorApprovePrev);

        C3DataSet dsDcPrev = new C3DataSet(listDataTotalDeclineChargePrev);
        C3ViewDataSet dataSetDcPrev = new C3ViewDataSet(dsNameDc + " Prev", dsDcPrev, "#"+colorDcPrev);

        C3DataSet dsDfPrev = new C3DataSet(listDataTotalDeclineFreePrev);
        C3ViewDataSet dataSetDfPrev = new C3ViewDataSet(dsNameDf  + " Prev", dsDfPrev, "#"+colorDfPrev);

        C3DataSetString dsDateString = new C3DataSetString(listDateString);
        C3ViewDataSetString dataSetDateString = new C3ViewDataSetString("x", dsDateString, false);

        Data data = new Data();
        data.getDataSetsString().add(dataSetDateString);

        if (activePrev[0]) {
            data.getDataSets().add(dataSetTotal);
        }
        if (activePrev[1]) {
            data.getDataSets().add(dataSetAppr);
        }
        if (activePrev[2]) {
            data.getDataSets().add(dataSetDc);
        }
        if (activePrev[3]) {
            data.getDataSets().add(dataSetDf);
        }
        if (activePrev[0]) {
            data.getDataSets().add(dataSetTotalPrev);
        }
        if (activePrev[1]) {
            data.getDataSets().add(dataSetApprPrev);
        }
        if (activePrev[2]) {
            data.getDataSets().add(dataSetDcPrev);
        }
        if (activePrev[3]) {
            data.getDataSets().add(dataSetDfPrev);
        }

        data.setX("z1");
        //if (timeframe == TIMEFRAME_1D) {
        //    data.setxFormat("%Y-%m-%d");
        //} else {
            data.setxFormat("%Y-%m-%d %H:%M");
        //}

        com.martinlinha.c3faces.script.property.Axis axis = new com.martinlinha.c3faces.script.property.Axis();
        axis.setHeightX(70);
        if (timeframe == TIMEFRAME_1D) {
            axis.setTypeX(Axis.Type.CATEGORY);
            axis.setTickX("fit: true,\n" +
                    "format: '%d/%m',\n" +
                    "rotate: -45,\n" +
                    "multiline: false,\n" +
                    "culling: true");
        } else {
            axis.setTypeX(com.martinlinha.c3faces.script.property.Axis.Type.TIMESERIES);
            axis.setTickX("fit: true,\n" +
                    "format: '%d/%m %H:%M',\n" +
                    "rotate: -45,\n" +
                    "multiline: false,\n" +
                    "culling: true");
        }
        axis.setMinY(0);

        Point point = new Point();
        point.setShow(true);

        //Padding padding = new Padding();

        C3ChartBean c3ChartBean = new C3ChartBean();
        c3ChartBean.setData(data);
        c3ChartBean.setAxis(axis);
        c3ChartBean.setPoint(point);
        //c3ChartBean.setPadding(padding);
        c3ChartBean.setTitle(title);
        listC3Chart.add(c3ChartBean);
    }

    protected void generateChartMultiData(List<DsBean> listData, List<String> listDateString,
                                        String title, int timeframe) {
        List<C3ViewDataSet> listDataSet = new ArrayList<>();

        for(DsBean dsBean : listData) {
            C3DataSet ds = new C3DataSet(dsBean.getListData());
            C3ViewDataSet dataSet = new C3ViewDataSet(dsBean.getName(), ds, "#"+dsBean.getColor());
            listDataSet.add(dataSet);
        }

        C3DataSetString dsDateString = new C3DataSetString(listDateString);
        C3ViewDataSetString dataSetDateString = new C3ViewDataSetString("x", dsDateString, false);

        Data data = new Data();
        data.getDataSetsString().add(dataSetDateString);
        for(C3ViewDataSet ds : listDataSet) {
            data.getDataSets().add(ds);
        }

        data.setX("z1");
        if (timeframe == TIMEFRAME_1D) {
            data.setxFormat("%Y-%m-%d");
        } else {
            data.setxFormat("%Y-%m-%d %H:%M");
        }

        com.martinlinha.c3faces.script.property.Axis axis = new com.martinlinha.c3faces.script.property.Axis();
        axis.setHeightX(70);
        if (timeframe == TIMEFRAME_1D) {
            axis.setTypeX(Axis.Type.CATEGORY);
            axis.setTickX("fit: true,\n" +
                    "format: '%d/%m',\n" +
                    "rotate: -45,\n" +
                    "multiline: false,\n" +
                    "culling: true");
        } else {
            axis.setTypeX(com.martinlinha.c3faces.script.property.Axis.Type.TIMESERIES);
            axis.setTickX("fit: true,\n" +
                    "format: '%d/%m %H:%M',\n" +
                    "rotate: -45,\n" +
                    "multiline: false,\n" +
                    "culling: true");
        }
        axis.setMinY(0);

        Point point = new Point();
        point.setShow(true);

        //Padding padding = new Padding();

        C3ChartBean c3ChartBean = new C3ChartBean();
        c3ChartBean.setData(data);
        c3ChartBean.setAxis(axis);
        c3ChartBean.setPoint(point);
        //c3ChartBean.setPadding(padding);
        c3ChartBean.setTitle(title);
        listC3Chart.add(c3ChartBean);
    }


    public Boolean[] getActivePercentPrev() {
        return activePercentPrev;
    }

    public void setActivePercentPrev(Boolean[] activePercentPrev) {
        this.activePercentPrev = activePercentPrev;
    }

    public boolean isShowPercent() {
        return showPercent;
    }

    public void setShowPercent(boolean showPercent) {
        this.showPercent = showPercent;
    }

    public String getShowPercentButtonText() {
        return showPercentButtonText;
    }

    public void setShowPercentButtonText(String showPercentButtonText) {
        this.showPercentButtonText = showPercentButtonText;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<C3ChartBean> getListC3Chart() {
        return listC3Chart;
    }

    public void setListC3Chart(List<C3ChartBean> listC3Chart) {
        this.listC3Chart = listC3Chart;
    }

    public Boolean[] getActivePrev() {
        return activePrev;
    }

    public void setActivePrev(Boolean[] activePrev) {
        this.activePrev = activePrev;
    }

    public Date getSelectedEndDate() {
        return selectedEndDate;
    }

    public void setSelectedEndDate(Date selectedEndDate) {
        this.selectedEndDate = selectedEndDate;
    }

    public Integer getSelectedEndHour() {
        return selectedEndHour;
    }

    public void setSelectedEndHour(Integer selectedEndHour) {
        this.selectedEndHour = selectedEndHour;
    }

    public Integer getSelectedEndMinute() {
        return selectedEndMinute;
    }

    public void setSelectedEndMinute(Integer selectedEndMinute) {
        this.selectedEndMinute = selectedEndMinute;
    }

    public Date getSelectedStartDate() {
        return selectedStartDate;
    }

    public void setSelectedStartDate(Date selectedStartDate) {
        this.selectedStartDate = selectedStartDate;
    }

    public Integer getSelectedStartHour() {
        return selectedStartHour;
    }

    public void setSelectedStartHour(Integer selectedStartHour) {
        this.selectedStartHour = selectedStartHour;
    }

    public Integer getSelectedStartMinute() {
        return selectedStartMinute;
    }

    public void setSelectedStartMinute(Integer selectedStartMinute) {
        this.selectedStartMinute = selectedStartMinute;
    }

    public int getDefaultRangeInHour() {
        return defaultRangeInHour;
    }

    public void setDefaultRangeInHour(int defaultRangeInHour) {
        this.defaultRangeInHour = defaultRangeInHour;
    }

    public Date getSelectedPrevDate() {
        return selectedPrevDate;
    }

    public void setSelectedPrevDate(Date selectedPrevDate) {
        this.selectedPrevDate = selectedPrevDate;
    }

    public boolean isShowPrevData() {
        return showPrevData;
    }

    public void setShowPrevData(boolean showPrevData) {
        this.showPrevData = showPrevData;
    }

    public List<TableEsqTrxIdBean> getListTableData() {
        return listTableData;
    }

    public void setListTableData(List<TableEsqTrxIdBean> listTableData) {
        this.listTableData = listTableData;
    }

    public String getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(String maxDate) {
        this.maxDate = maxDate;
    }

    public String[] getRenderChartArray() {
        return renderChartArray;
    }

    public void setRenderChartArray(String[] renderChartArray) {
        this.renderChartArray = renderChartArray;
    }

    public C3ChartBean[] getChartBeanArray() {
        return chartBeanArray;
    }

    public void setChartBeanArray(C3ChartBean[] chartBeanArray) {
        this.chartBeanArray = chartBeanArray;
    }

    public class DsBean {
        private List<? extends Number> listData;
        private String color;
        private String name;

        public List<? extends Number> getListData() {
            return listData;
        }

        public void setListData(List<? extends Number> listData) {
            this.listData = listData;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}

package com.rintis.marketing.web.jsf.main.biller;

import com.rintis.marketing.beans.entity.BillerAnomalyInstitusiParam;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aaw 20211115
 */

@ManagedBean
@ViewScoped
public class AddBillerAnomalyInstitutionManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private BillerAnomalyInstitusiParam billerAnomalyInstitusiParam;
    private List<BillerAnomalyInstitusiParam> listParam = new ArrayList<>();
    String pBillerId = "";
    String pBillerName = "";

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            pBillerId = paramDecodeUrl(getRequestParameterMap().get("billerId").toString());
            pBillerName = paramDecodeUrl(getRequestParameterMap().get("billerName").toString());
            listParam = moduleFactory.getBillerService().listBillerInstitutionParam();

            if ("".equals(pBillerId)) {
                glowMessageError("Institution not valid !", "");
            }

            createTlog(moduleFactory);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String saveAction() {
        try {
            moduleFactory.getBillerService().saveMultipleBillerAnomalyinstitution(listParam);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            glowMessageError("Something went wrong !", "");
            return "";
        }
        return "billerAnomalyInstitusiParameter?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public BillerAnomalyInstitusiParam getBillerAnomalyInstitusiParam() {
        return billerAnomalyInstitusiParam;
    }

    public void setBillerAnomalyInstitusiParam(BillerAnomalyInstitusiParam billerAnomalyInstitusiParam) {
        this.billerAnomalyInstitusiParam = billerAnomalyInstitusiParam;
    }

    public List<BillerAnomalyInstitusiParam> getListParam() {
        return listParam;
    }

    public void setListParam(List<BillerAnomalyInstitusiParam> listParam) {
        this.listParam = listParam;
    }

    public String getpBillerId() {
        return pBillerId;
    }

    public void setpBillerId(String pBillerId) {
        this.pBillerId = pBillerId;
    }

    public String getpBillerName() {
        return pBillerName;
    }

    public void setpBillerName(String pBillerName) {
        this.pBillerName = pBillerName;
    }
}

package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.WhitelistBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 * Created by aabraham on 12/02/2019.
 */
@ManagedBean
@ViewScoped
public class AddWhitelistPANManagedBean  extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private WhitelistBean whitelistBean;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        whitelistBean = new WhitelistBean();
    }

    public String saveAction() {
        try {
            if (whitelistBean.getPan().length() == 0) {
                glowMessageError("Enter PAN", "");
                return "";
            }

            if (whitelistBean.getPengirim().length() == 0) {
                whitelistBean.setPengirim(" ");
            }

            boolean chk = moduleFactory.getPanService().checkPanSenderUsed(whitelistBean.getPan().trim(), whitelistBean.getPengirim());
            if (chk) {
                glowMessageError("PAN And Sender Already Exist", "");
                return "";
            }

            /*Add validation by aaw 10/08/2021*/
            if (!isValid(whitelistBean.getPan())) {
                glowMessageError("PAN Not Valid", "PAN Can't Start With '-' ");
                return "";
            }
            /*End add*/

            String act = whitelistBean.getActive().toString();

            moduleFactory.getPanService().savePan(whitelistBean.getPan(), whitelistBean.getPengirim(), whitelistBean.getFreqApp(), whitelistBean.getFreqDc(), whitelistBean.getFreqDcFree(), whitelistBean.getFreqRvsl(), whitelistBean.getTotalAmtApp(), whitelistBean.getTotalAmtDc(), whitelistBean.getTotalAmtDcFree(), whitelistBean.getTotalAmtRvsl(), whitelistBean.getTotalAmt(), act);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_WHITELIST_PAN_WHITELIST_PAN + "?faces-redirect=true";
    }

    /*Add new method by aaw 10/08/2021*/
    public boolean isValid(String input) {
        boolean result = true;
        if (!"".equals(input.trim())) {
            if ((input.trim()).startsWith("-")) {
                result = false;
            }
        }

        return result;
    }
    /*End add*/

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public WhitelistBean getWhitelistPan() {
        return whitelistBean;
    }

    public void setWhitelistPan(WhitelistBean whitelistBean) {
        this.whitelistBean = whitelistBean;
    }
}

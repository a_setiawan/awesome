package com.rintis.marketing.web.jsf.main.alert;

import com.rintis.marketing.beans.bean.master.HolidayBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import com.rintis.marketing.web.jsf.main.test.TestDateBean;
import jodd.madvoc.meta.In;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@ManagedBean
@ViewScoped
public class HolidayManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<HolidayBean> listDate;
    private List<List<HolidayBean>> listMonth;
    private Integer selectedYear;
    private List<Integer> listYear;

    @Override
    protected void initManagedBean() {
        try {
            menuId = "0408";
            pageTitle = getMenuName(moduleFactory, menuId);

            selectedYear = DateUtils.getYear(new Date());
            listYear = DateUtils.listYear(1);
            listDate = new ArrayList<>();


            submitAction();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            String[] daystr = {"", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
            listMonth = new ArrayList<>();

            for(int month=1; month <= 12; month++) {
                Date date = new Date();
                date = DateUtils.setDay(date, 1);
                date = DateUtils.setYear(date, selectedYear);
                date = DateUtils.setMonth(date, month);

                listDate = new ArrayList<>();
                Date selectedDate = date;

                Calendar cal = Calendar.getInstance();
                cal.setTime(selectedDate);
                int maxday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
                //log.info(maxday);

                for (int i = 1; i <= 7; i++) {
                    HolidayBean db = new HolidayBean();
                    db.setHeader(1);
                    db.setDayOfWeekString(daystr[i]);
                    listDate.add(db);
                }

                for (int i = 1; i <= maxday; i++) {
                    cal = Calendar.getInstance();
                    cal.setTime(selectedDate);
                    cal.set(Calendar.DAY_OF_MONTH, i);
                    int dayofweek = cal.get(Calendar.DAY_OF_WEEK);

                    if (i == 1) {
                        if (dayofweek > 1) {
                            for (int x = 1; x <= dayofweek - 1; x++) {
                                HolidayBean db = new HolidayBean();
                                db.setHeader(0);
                                listDate.add(db);
                            }
                        }
                    }

                    HolidayBean db = new HolidayBean();
                    db.setDay(i);
                    db.setMonth(cal.get(Calendar.MONTH));
                    db.setYear(cal.get(Calendar.YEAR));
                    db.setHoliday(false);
                    db.setDayOfWeek(dayofweek);
                    db.setHeader(0);
                    db.setColor("color: black;");
                    if (dayofweek == 1) {
                        db.setColor("color: red;");
                        db.setHoliday(true);
                    }
                    if (dayofweek == 7) {
                        db.setHoliday(true);
                    }

                    Date d = new Date();
                    d= DateUtils.setHour(d, 0);
                    d= DateUtils.setMinute(d, 0);
                    d= DateUtils.setSecond(d, 0);
                    d= DateUtils.setMonth(d, 1);
                    d= DateUtils.setDay(d, 1);
                    d= DateUtils.setMonth(d, db.getMonth() +1);
                    d= DateUtils.setDay(d, db.getDay());
                    d= DateUtils.setYear(d, db.getYear());
                    /*String s = Integer.toString(db.getYear()) + "-" +
                            StringUtils.formatFixLength(Integer.toString(db.getMonth()), "0",2) + "-" +
                            StringUtils.formatFixLength(Integer.toString(db.getDay()), "0", 2) + " 00:00:00";
                    Date d = DateUtils.convertDate(s, DateUtils.DATE_YYYYMMDD_HHMMSS);*/

                    boolean holdy = moduleFactory.getMasterService().checkHoliday(d);
                    if (holdy) {
                        db.setHoliday(true);
                    }
                    listDate.add(db);

                    if (i == maxday) {
                        if (dayofweek < 7) {
                            for (int x = 1; x <= (7 - maxday); x++) {
                                db = new HolidayBean();
                                db.setHeader(0);
                                listDate.add(db);
                            }
                        }
                    }
                }

                //log.info(month + " " + listDate.size());
                listMonth.add(listDate);

            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String saveAction() {
        try {
            moduleFactory.getMasterService().updateMasterHoliday2(selectedYear, listMonth);

            glowMessageInfo("Update Success", "");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            glowMessageError("Update Failed", "");
        }
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<HolidayBean> getListDate() {
        return listDate;
    }

    public void setListDate(List<HolidayBean> listDate) {
        this.listDate = listDate;
    }

    public List<List<HolidayBean>> getListMonth() {
        return listMonth;
    }

    public void setListMonth(List<List<HolidayBean>> listMonth) {
        this.listMonth = listMonth;
    }

    public Integer getSelectedYear() {
        return selectedYear;
    }

    public void setSelectedYear(Integer selectedYear) {
        this.selectedYear = selectedYear;
    }

    public List<Integer> getListYear() {
        return listYear;
    }

    public void setListYear(List<Integer> listYear) {
        this.listYear = listYear;
    }
}

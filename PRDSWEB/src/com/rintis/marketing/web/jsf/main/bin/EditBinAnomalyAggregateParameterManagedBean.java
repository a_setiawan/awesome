package com.rintis.marketing.web.jsf.main.bin;

import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.*;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.ParamConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import com.rintis.marketing.web.jsf.main.groupbank.BankComparableBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.*;

/**
 * Created by aabraham on 01/02/2021.
 */
@ManagedBean
@ViewScoped
public class EditBinAnomalyAggregateParameterManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private BinList listBin;
    private List<TmsBankCa> listBank;

    private List<ListComparableBean> listTrxSource;
    private List<ListComparableBean> listTrxSelected;
    private List<String> selectedOptionSource;
    private List<String> selectedOptionTarget;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            String listId = (String)getRequestParameterMap().get(ParamConstant.PARAM_ID);
            listBin = moduleFactory.getMasterService().getBinList(listId);
            listBank = moduleFactory.getMasterService().listBankBIN();

            listTrxSource = new ArrayList<>();
            List<EsqTrxid> list = moduleFactory.getMasterService().listTrx();
            String [] id = {"30", "32", "37"};

            for(EsqTrxid trx : list) {
                if(!Arrays.asList(id).contains(trx.getTrxId())){
                    ListComparableBean lcb = new ListComparableBean();
                    lcb.setEsqTrxid(trx);
                    listTrxSource.add(lcb);
                }
            }

            String trxstring = "";
            listTrxSelected = new ArrayList<>();

            List<BinListTrxId> listBinTrxId = moduleFactory.getMasterService().listBinTrxId(listId);
            for(BinListTrxId gbm : listBinTrxId) {
                EsqTrxid eb = moduleFactory.getMasterService().getTrx(gbm.getBinListPk().getTrxId());
                ListComparableBean lcb = new ListComparableBean();
                lcb.setEsqTrxid(eb);
                listTrxSelected.add(lcb);
                trxstring = trxstring + "," + gbm.getBinListPk().getTrxId();
            }
            Collections.sort(listTrxSelected);

            int size = listTrxSource.size();
            for(int i = size - 1; i >= 0; i--) {
                ListComparableBean lcb = listTrxSource.get(i);
                if (trxstring.indexOf(lcb.getEsqTrxid().getTrxId()) > -1) {
                    listTrxSource.remove(i);
                }
            }

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String updateAction() {
        try {
            List<String> list  = new ArrayList<>();
            for(ListComparableBean lcb : listTrxSelected) {
                list.add(lcb.getEsqTrxid().getTrxId());
            }

            moduleFactory.getMasterService().updateListBin(listBin, list);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "/apps/bin/listBinAnomalyAggregateParameter?faces-redirect=true";
    }

    public String addSelectedTrxAction() {
        try {
            String trxstring = "";
            for (String s : selectedOptionSource) {
                //log.info(s);
                EsqTrxid bb = moduleFactory.getMasterService().getTrx(s);
                ListComparableBean lcb = new ListComparableBean();
                lcb.setEsqTrxid(bb);
                listTrxSelected.add(lcb);
                trxstring = trxstring + "," + s;
            }
            Collections.sort(listTrxSelected);

            int size = listTrxSource.size();
            for(int i = size - 1; i >= 0; i--) {
                ListComparableBean lcb = listTrxSource.get(i);
                if (trxstring.indexOf(lcb.getEsqTrxid().getTrxId()) > -1) {
                    listTrxSource.remove(i);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String removeSelectedTrxAction() {
        try {
            String trxstring = "";
            for (String s : selectedOptionTarget) {
                //log.info(s);
                EsqTrxid bb = moduleFactory.getMasterService().getTrx(s);
                ListComparableBean lcb = new ListComparableBean();
                lcb.setEsqTrxid(bb);
                listTrxSource.add(lcb);
                trxstring = trxstring + "," + s;
            }
            Collections.sort(listTrxSource);

            int size = listTrxSelected.size();
            for(int i = size - 1; i >= 0; i--) {
                ListComparableBean lcb = listTrxSelected.get(i);
                if (trxstring.indexOf(lcb.getEsqTrxid().getTrxId()) > -1) {
                    listTrxSelected.remove(i);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public BinList getListBin() {
        return listBin;
    }

    public void setListBin(BinList listBin) {
        this.listBin = listBin;
    }

    public List<TmsBankCa> getListBank() {
        return listBank;
    }

    public void setListBank(List<TmsBankCa> listBank) {
        this.listBank = listBank;
    }

    public List<ListComparableBean> getListTrxSource() {
        return listTrxSource;
    }

    public void setListTrxSource(List<ListComparableBean> listTrxSource) {
        this.listTrxSource = listTrxSource;
    }

    public List<ListComparableBean> getListTrxSelected() {
        return listTrxSelected;
    }

    public void setListTrxSelected(List<ListComparableBean> listTrxSelected) {
        this.listTrxSelected = listTrxSelected;
    }

    public List<String> getSelectedOptionSource() {
        return selectedOptionSource;
    }

    public void setSelectedOptionSource(List<String> selectedOptionSource) {
        this.selectedOptionSource = selectedOptionSource;
    }

    public List<String> getSelectedOptionTarget() {
        return selectedOptionTarget;
    }

    public void setSelectedOptionTarget(List<String> selectedOptionTarget) {
        this.selectedOptionTarget = selectedOptionTarget;
    }
}

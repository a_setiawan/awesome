package com.rintis.marketing.web.jsf.main.biller;

import com.rintis.marketing.beans.bean.biller.TmsBillerBean;
import com.rintis.marketing.beans.entity.BillerDummy;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aaw 20211112
 */

@ManagedBean
@ViewScoped
public class AddWhitelistBillerManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private BillerDummy billerDummy;
    private List<TmsBillerBean> listBiller = new ArrayList<>();
    private String billerId = "";

    @Override
    @PostConstruct
    protected void initManagedBean() {
        billerDummy = new BillerDummy();
        try {
            listBiller = moduleFactory.getBillerService().fetchAllActiveBiller();
            createTlog(moduleFactory);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public boolean isAlpha(String acct) {
        boolean res = false;
        for (char c : acct.toCharArray()) {
            if((Character.isLetter(c))) {
                System.out.println("Is a character! " + c);
                res = true;
                break;
            }
        }
        return res;
    }

    public boolean containsDigit(String acct) {
        boolean res = false;
        if (acct != null && !acct.isEmpty()) {
            for (char c :  acct.toCharArray()) {
                if(Character.isDigit(c)) {
                    res = true;
                    break;
                }
            }
        }
        return res;
    }

    public String saveAction() {
        try {
            if (billerId.trim().length() == 0) {
                glowMessageError("Institution Code Is Required !", "");
                return "";
            }

            /*if (isAlpha(billerDummy.getInstCd().trim()) || !containsDigit(billerDummy.getInstCd().trim())) {
                glowMessageError("'Institution Code' Must Consist Of 0-9 and -", "Sample : 014-XXX");
                return "";
            }*/

            if (billerDummy.getCustNo().length() == 0) {
                glowMessageError("Customer No Is Required !", "");
                return "";
            }

            boolean chk = moduleFactory.getBillerService().checkTresholdExist(billerId, billerDummy.getCustNo().trim());
            if (chk) {
                glowMessageError("Institution Code And Customer No Is Already Exist !", "");
                return "";
            }

            /*if (!isValid(billerDummy.getCustNo())) {
                glowMessageError("Customer No Is Not Valid", "Customer No Can't Start With '-' ");
                return "";
            }*/

            billerDummy.setInstCd(billerId);

            moduleFactory.getBillerService().saveBillerTreshold(billerDummy);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "/apps/biller/listWhitelistBiller.do?faces-redirect=true";
    }

    /*Add new method by aaw 10/08/2021*/
    public boolean isValid(String input) {
        boolean result = true;
        if (!"".equals(input.trim())) {
            if ((input.trim()).startsWith("-")) {
                result = false;
            }
        }

        return result;
    }
    /*End add*/

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public BillerDummy getBillerDummy() {
        return billerDummy;
    }

    public void setBillerDummy(BillerDummy billerDummy) {
        this.billerDummy = billerDummy;
    }

    public List<TmsBillerBean> getListBiller() {
        return listBiller;
    }

    public void setListBiller(List<TmsBillerBean> listBiller) {
        this.listBiller = listBiller;
    }

    public String getBillerId() {
        return billerId;
    }

    public void setBillerId(String billerId) {
        this.billerId = billerId;
    }
}

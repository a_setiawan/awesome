package com.rintis.marketing.web.jsf.main.biller;

import com.rintis.marketing.beans.entity.BillerDummy;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.util.List;

/**
 * Created by aaw 20211112
 */

@ManagedBean
@ViewScoped
public class WhitelistBillerManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;

    private List<BillerDummy> listWhitelistBiller;
    private BillerDummy selectedDeleteItem;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listWhitelistBiller = moduleFactory.getBillerService().listWhitelistBiller();
            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    public void deleteActionListener(ActionEvent event) {
        try {
            moduleFactory.getBillerService().deleteWhitelistBiller(selectedDeleteItem.getInstCd(), selectedDeleteItem.getCustNo());
            listWhitelistBiller = moduleFactory.getBillerService().listWhitelistBiller();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<BillerDummy> getListWhitelistBiller() {
        return listWhitelistBiller;
    }

    public void setListWhitelistBiller(List<BillerDummy> listWhitelistBiller) {
        this.listWhitelistBiller = listWhitelistBiller;
    }

    public BillerDummy getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(BillerDummy selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }
}

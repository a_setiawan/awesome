package com.rintis.marketing.web.jsf.main.mcc;

import com.rintis.marketing.beans.entity.MCCParameter;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by aabraham on 26/04/2021.
 */

@ManagedBean
@ViewScoped
public class EditMCCParameterManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;

    private List<MCCParameter> listMCCParameter;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        String mcc = (String)getRequestParameterMap().get("uid");
        if (mcc != null) {
            try {
                listMCCParameter = moduleFactory.getMasterService().listMccParameter(mcc);
                if (!isPostback()) {
                    createTlog(moduleFactory);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public String editAction() {
        try {
            String mcc = listMCCParameter.get(0).getMcc();
            BigDecimal freqApp = listMCCParameter.get(0).getMin_trx_app_1d();
            BigDecimal freqTot = listMCCParameter.get(0).getMin_trx_tot_1d();
            BigDecimal amtApp = listMCCParameter.get(0).getMin_amt_app_1d();
            BigDecimal amtTot = listMCCParameter.get(0).getMin_amt_tot_1d();
            moduleFactory.getMasterService().updateMccParameter(mcc, freqApp, freqTot, amtApp, amtTot);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_MASTER_MCC_PARAMETER + "?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<MCCParameter> getListMCCParameter() {
        return listMCCParameter;
    }

    public void setListMCCParameter(List<MCCParameter> listMCCParameter) {
        this.listMCCParameter = listMCCParameter;
    }
}

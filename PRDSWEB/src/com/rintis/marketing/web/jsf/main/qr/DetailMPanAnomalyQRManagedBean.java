package com.rintis.marketing.web.jsf.main.qr;

import com.google.common.collect.ComparisonChain;
import com.rintis.marketing.beans.bean.bi.DetailMPanAnomalyBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by aaw on 30/06/2021.
 */

@ManagedBean
@ViewScoped
public class DetailMPanAnomalyQRManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<DetailMPanAnomalyBean> list;
    private String pPeriod;
    private String pPeriodTo;
    private String pPeriodType;
    private String pTimeFr;
    private String pTimeTo;
    private String pPan;
    private String pAcquirerBank;
    private String pAcquirerId;
    private String pTranType;
    private String pTranTypeId;
    private String pGroupId;
    private String pQrType;
    private int colspan;
    private String totalAmt;
    private String pTermId;
    private String pTermName;

    @Override
    @PostConstruct
    protected void initManagedBean() {

        try {
            pPeriodType = paramEncode(getRequestParameterMap().get("pPeriodType").toString());
            if ("3D".equalsIgnoreCase(pPeriodType)) {
                pPeriod = paramEncode(getRequestParameterMap().get("pPeriodFrom").toString());
            } else {
                pPeriod = paramEncode(getRequestParameterMap().get("pPeriod").toString());
            }
            pPeriodTo = paramEncode(getRequestParameterMap().get("pPeriodTo").toString());
            pTimeFr = paramEncode(getRequestParameterMap().get("pTimeFr").toString().replace(":", ""));
            pTimeTo = paramEncode(getRequestParameterMap().get("pTimeTo").toString().replace(":", ""));
            pPan = paramEncode(getRequestParameterMap().get("pPan").toString());
            pAcquirerId = paramEncode(getRequestParameterMap().get("pAcqId").toString());
            pAcquirerBank = paramEncode(getRequestParameterMap().get("pAcqBank").toString());
            pTranType = "QR";
            pQrType = paramEncode(getRequestParameterMap().get("pQrType").toString());
            pGroupId = moduleFactory.getPanService().getGroupIdFromTrxName(pTranType);
            pTranTypeId = moduleFactory.getPanService().getTrxIdFromTrxName(pTranType);
            pTermId = paramEncode(getRequestParameterMap().get("pTermId").toString());
            pTermName = paramEncode(getRequestParameterMap().get("pTermName").toString());

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            List<DetailMPanAnomalyBean> def = new ArrayList<>();
            def = moduleFactory.getQRService().listDetailMPanQR(pPeriod, pTimeFr, pTimeTo, pPan, pQrType, pAcquirerId, pPeriodTo, pPeriodType, pTermId, pTermName);
            if(!def.isEmpty()){
                list = sortingListMpan(def);
            }

            colspan = 9;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public List<DetailMPanAnomalyBean> sortingListMpan (List<DetailMPanAnomalyBean> in) {

        Collections.sort(in, new Comparator<DetailMPanAnomalyBean>() {
            @Override
            public int compare(DetailMPanAnomalyBean o1, DetailMPanAnomalyBean o2) {
                return ComparisonChain.start()
                    .compare(o1.getPeriod(), o2.getPeriod())
                    .compare(o1.getTrxTime(), o2.getTrxTime())
                    .result();
                /*Above is sorting ascending, for descending use :
                .compare(o2.getPeriod(), o1.getPeriod()) */
            }
        });
        System.out.println("Result Sort : " + in);
        return in;
    }

    public String paramEncode(String data){
        if(!data.equals(null)){
            data = HttpUtils.encodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }

    public String getValueAmtTotal() {
        Double totAmt = 0.00;

        if(list != null){
            for(DetailMPanAnomalyBean l : list) {
                totAmt += Double.parseDouble(l.getAmount().toString());
            }
        }

        DecimalFormat formatter = new DecimalFormat("###,###,###.00");
        totalAmt = formatter.format(totAmt);

        return totalAmt;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<DetailMPanAnomalyBean> getList() {
        return list;
    }

    public void setList(List<DetailMPanAnomalyBean> list) {
        this.list = list;
    }

    public String getpPeriod() {
        return pPeriod;
    }

    public void setpPeriod(String pPeriod) {
        this.pPeriod = pPeriod;
    }

    public String getpTimeFr() {
        return pTimeFr;
    }

    public void setpTimeFr(String pTimeFr) {
        this.pTimeFr = pTimeFr;
    }

    public String getpTimeTo() {
        return pTimeTo;
    }

    public void setpTimeTo(String pTimeTo) {
        this.pTimeTo = pTimeTo;
    }

    public String getpPan() {
        return pPan;
    }

    public void setpPan(String pPan) {
        this.pPan = pPan;
    }

    public String getpTranType() {
        return pTranType;
    }

    public void setpTranType(String pTranType) {
        this.pTranType = pTranType;
    }

    public String getpTranTypeId() {
        return pTranTypeId;
    }

    public void setpTranTypeId(String pTranTypeId) {
        this.pTranTypeId = pTranTypeId;
    }

    public String getpGroupId() {
        return pGroupId;
    }

    public void setpGroupId(String pGroupId) {
        this.pGroupId = pGroupId;
    }

    public int getColspan() {
        return colspan;
    }

    public void setColspan(int colspan) {
        this.colspan = colspan;
    }


    public String getpAcquirerId() {
        return pAcquirerId;
    }

    public void setpAcquirerId(String pAcquirerId) {
        this.pAcquirerId = pAcquirerId;
    }

    public String getpQrType() {
        return pQrType;
    }

    public void setpQrType(String pQrType) {
        this.pQrType = pQrType;
    }

    public String getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(String totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getpAcquirerBank() {
        return pAcquirerBank;
    }

    public void setpAcquirerBank(String pAcquirerBank) {
        this.pAcquirerBank = pAcquirerBank;
    }

    public String getpPeriodTo() {
        return pPeriodTo;
    }

    public void setpPeriodTo(String pPeriodTo) {
        this.pPeriodTo = pPeriodTo;
    }

    public String getpPeriodType() {
        return pPeriodType;
    }

    public void setpPeriodType(String pPeriodType) {
        this.pPeriodType = pPeriodType;
    }

    public String getpTermId() {
        return pTermId;
    }

    public void setpTermId(String pTermId) {
        this.pTermId = pTermId;
    }

    public String getpTermName() {
        return pTermName;
    }

    public void setpTermName(String pTermName) {
        this.pTermName = pTermName;
    }
}

package com.rintis.marketing.web.jsf.main.user;

import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.EisBank;
import com.rintis.marketing.beans.entity.MenuRoleType;
import com.rintis.marketing.beans.entity.UserLogin;
import com.rintis.marketing.beans.entity.UserLoginBank;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ManagedBean
@ViewScoped
public class ListUserBankManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;

    private List<MenuRoleType> listMenuRoleType;
    private List<UserLoginBank> listUserLogin;
    private UserLoginBank selectedDeleteItem;
    private Integer selectedRole;
    private String selectedBankId;
    private List<BankBean> listBank;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listMenuRoleType = moduleFactory.getMenuService().listMenuRoleType(DataConstant.MENU_ROLE_TYPE_NOT_HIDDEN);
            listUserLogin = moduleFactory.getLoginService().listUserLoginBank(-1);

            Map<String, Object> paramBank = new HashMap<>();
            listBank = moduleFactory.getMasterService().listBankCA(paramBank);



            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }



    public void deleteUserActionListener(ActionEvent event) {
        try {
            moduleFactory.getLoginService().deleteUserLoginBank(selectedDeleteItem.getUserLoginId());
            listUserLogin = moduleFactory.getLoginService().listUserLoginBank(selectedRole, selectedBankId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String searchAction() {
        try {
            listUserLogin = moduleFactory.getLoginService().listUserLoginBank(selectedRole, selectedBankId);
            //log.info(listUserLogin.size());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }
    


    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<UserLoginBank> getListUserLogin() {
        return listUserLogin;
    }

    public void setListUserLogin(List<UserLoginBank> listUserLogin) {
        this.listUserLogin = listUserLogin;
    }

    public UserLoginBank getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(UserLoginBank selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }

    public List<MenuRoleType> getListMenuRoleType() {
        return listMenuRoleType;
    }

    public void setListMenuRoleType(List<MenuRoleType> listMenuRoleType) {
        this.listMenuRoleType = listMenuRoleType;
    }

    public Integer getSelectedRole() {
        return selectedRole;
    }

    public void setSelectedRole(Integer selectedRole) {
        this.selectedRole = selectedRole;
    }

    public String getSelectedBankId() {
        return selectedBankId;
    }

    public void setSelectedBankId(String selectedBankId) {
        this.selectedBankId = selectedBankId;
    }

    public List<BankBean> getListBank() {
        return listBank;
    }

    public void setListBank(List<BankBean> listBank) {
        this.listBank = listBank;
    }
}

package com.rintis.marketing.web.jsf.main.qr;

import com.rintis.marketing.beans.bean.bi.WhitelistQRBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.util.List;

/**
 * Created by aaw on 28/06/21.
 */

@ManagedBean
@ViewScoped
public class ListWhitelistMPanManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;

    private List<WhitelistQRBean> listWhitelistMPanQR;
    private WhitelistQRBean selectedDeleteItem;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listWhitelistMPanQR = moduleFactory.getMPanTresholdService().listMPanQR();
            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    public void deleteActionListener(ActionEvent event) {
        try {
            moduleFactory.getMPanTresholdService().deleteMPanTreshold(selectedDeleteItem.getPan().toString(), selectedDeleteItem.getAcquirerId().toString());
            listWhitelistMPanQR = moduleFactory.getMPanTresholdService().listMPanQR();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<WhitelistQRBean> getListWhitelistMPanQR() {
        return listWhitelistMPanQR;
    }

    public void setListWhitelistMPanQR(List<WhitelistQRBean> listWhitelistMPan) {
        this.listWhitelistMPanQR = listWhitelistMPan;
    }

    public WhitelistQRBean getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(WhitelistQRBean selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }
}

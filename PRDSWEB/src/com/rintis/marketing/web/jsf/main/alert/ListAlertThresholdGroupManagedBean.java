package com.rintis.marketing.web.jsf.main.alert;

import com.rintis.marketing.beans.bean.bi.AlertThresholdBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.util.List;

@ManagedBean
@ViewScoped
public class ListAlertThresholdGroupManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<AlertThresholdBean> listAlertThreshold;
    private AlertThresholdBean selectedDeleteItem;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0407";
            pageTitle = getMenuName(moduleFactory, menuId);

            listAlertThreshold = moduleFactory.getAlertService().listAlertThresholdGroupHeader();


            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String editAction() {
        String bid = (String)getRequestParameterMap().get("gid");
        String trxid = (String)getRequestParameterMap().get("trxid");

        return "addAlertThresholdGroup?faces-redirect=true&gid=" + bid + "&trxid=" + trxid;
    }

    public void deleteActionListener(ActionEvent event) {
        try {
            if (selectedDeleteItem == null) return;

            moduleFactory.getAlertService().deleteAlertThresholdGroup(selectedDeleteItem.getGroupid(), selectedDeleteItem.getTransactionid());

            listAlertThreshold = moduleFactory.getAlertService().listAlertThresholdGroupHeader();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<AlertThresholdBean> getListAlertThreshold() {
        return listAlertThreshold;
    }

    public void setListAlertThreshold(List<AlertThresholdBean> listAlertThreshold) {
        this.listAlertThreshold = listAlertThreshold;
    }

    public AlertThresholdBean getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(AlertThresholdBean selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }
}

package com.rintis.marketing.web.jsf.main.biller;

import com.rintis.marketing.beans.entity.BillerAnomalyParam;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * Created by aaw 20211115
 */

@ManagedBean
@ViewScoped
public class BillerAnomalyParameterManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;

    private List<BillerAnomalyParam> listBillerAnomalyParam;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listBillerAnomalyParam = moduleFactory.getBillerService().fetchBillerAnomalyParam();
            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<BillerAnomalyParam> getListBillerAnomalyParam() {
        return listBillerAnomalyParam;
    }

    public void setListBillerAnomalyParam(List<BillerAnomalyParam> listBillerAnomalyParam) {
        this.listBillerAnomalyParam = listBillerAnomalyParam;
    }
}

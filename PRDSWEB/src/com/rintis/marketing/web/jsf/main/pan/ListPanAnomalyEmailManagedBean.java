package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.PanAnomalyEmailBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.util.List;

/**
 * Created by aabraham on 01/03/2019.
 */
@ManagedBean
@ViewScoped
public class ListPanAnomalyEmailManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;

    private List<PanAnomalyEmailBean> listPanAnomalyEmail;
    private PanAnomalyEmailBean selectedDeleteItem;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listPanAnomalyEmail = moduleFactory.getPanService().listEmail();
            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void deleteActionListener(ActionEvent event) {
        try {
            moduleFactory.getPanService().deleteEmail(selectedDeleteItem.getUserEmail().toString());
            listPanAnomalyEmail = moduleFactory.getPanService().listEmail();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<PanAnomalyEmailBean> getListPanAnomalyEmail() {
        return listPanAnomalyEmail;
    }

    public void setListPanAnomalyEmail(List<PanAnomalyEmailBean> listPanAnomalyEmail) {
        this.listPanAnomalyEmail = listPanAnomalyEmail;
    }

    public PanAnomalyEmailBean getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(PanAnomalyEmailBean selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }
}

package com.rintis.marketing.web.jsf.main.user;

import com.rintis.marketing.beans.entity.MenuRoleType;
import com.rintis.marketing.beans.entity.UserLogin;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import com.rintis.marketing.web.common.PageConstant;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
public class AddUserManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private String disablePassword;
    private String passwordCss;

    private List<MenuRoleType> listRole;
    private UserLogin userLogin;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        userLogin = new UserLogin();
        userLogin.setEnabled(DataConstant.ACTIVE);
        userLogin.setUserBank(DataConstant.USER_NONBANK);
        userLogin.setValidationType(DataConstant.USER_VALIDATION_TYPE_LDAP);
        userLogin.setCurrentPassword("");

        disablePassword ="true";
        passwordCss = "visibility:hidden;";
        try {
            listRole = moduleFactory.getMenuService().listMenuRoleType(DataConstant.MENU_ROLE_TYPE_NOT_HIDDEN);




            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public String addUserAction() {
        try {
            if (userLogin.getUserLoginId().length() == 0) {
                glowMessageError("Enter User Id", "");
                return "";
            }
            if (userLogin.getUserName().length() == 0) {
                glowMessageError("Enter Name", "");
                return "";
            }
            if (userLogin.getValidationType().equalsIgnoreCase(DataConstant.USER_VALIDATION_TYPE_INTERNAL)) {
                if (userLogin.getCurrentPassword().length() == 0) {
                    glowMessageError("Enter Password", "");
                    return "";
                }
            }
            if (userLogin.getEmail().length() == 0) {
                glowMessageError("Enter Email", "");
                return "";
            }

            if (!StringUtils.validateEmail(userLogin.getEmail())) {
                glowMessageError("Invalid Email", "");
                return "";
            }

            if (moduleFactory.getLoginService().getUserLogin(userLogin.getUserLoginId()) != null) {
                glowMessageError("User Id already exist", "");
                return "";
            }
            if (moduleFactory.getLoginService().getUserLoginBank(userLogin.getUserLoginId()) != null) {
                glowMessageError("User Id already exist", "");
                return "";
            }


            moduleFactory.getLoginService().addUserLogin(userLogin);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_LIST_USER + "?faces-redirect=true";
    }

    public void validationChangeListener() {
        if (userLogin.getValidationType().equalsIgnoreCase(DataConstant.USER_VALIDATION_TYPE_LDAP)) {
            disablePassword = "true";
            userLogin.setCurrentPassword("");
            passwordCss = "visibility:hidden;";
        } else {
            disablePassword = "false";
            passwordCss = "";
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<MenuRoleType> getListRole() {
        return listRole;
    }

    public void setListRole(List<MenuRoleType> listRole) {
        this.listRole = listRole;
    }

    public UserLogin getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }

    public String getDisablePassword() {
        return disablePassword;
    }

    public void setDisablePassword(String disablePassword) {
        this.disablePassword = disablePassword;
    }

    public String getPasswordCss() {
        return passwordCss;
    }

    public void setPasswordCss(String passwordCss) {
        this.passwordCss = passwordCss;
    }
}

package com.rintis.marketing.web.jsf.main.qr;

import com.rintis.marketing.beans.bean.bi.WhitelistQRBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by aaw on 28/06/2021.
 */

@ManagedBean
@ViewScoped
public class EditWhitelistMPanManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<WhitelistQRBean> listWhitelistMPan;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        String pan = (String)getRequestParameterMap().get("wlPan");
        String acquirerId = (String)getRequestParameterMap().get("wlAcqId");

        if (pan != null && pan.trim() != "") {
            try {
                listWhitelistMPan = moduleFactory.getMPanTresholdService().getWhitelistMPan(pan.trim(), acquirerId);
                if (!isPostback()) {
                    createTlog(moduleFactory);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public String editAction() {
        try {
            if (listWhitelistMPan.get(0).getPan().length() == 0) {
                glowMessageError("Data Not Valid, 'PAN' Is Blank!", "");
                return "";
            }
            String mpan = listWhitelistMPan.get(0).getPan().trim();
            String acqId = listWhitelistMPan.get(0).getAcquirerId();
            BigDecimal freqApp = listWhitelistMPan.get(0).getFreqApp();
            BigDecimal freqDc = listWhitelistMPan.get(0).getFreqDc();
            BigDecimal freqDcFree = listWhitelistMPan.get(0).getFreqDcFree();
            BigDecimal freqRvsl = listWhitelistMPan.get(0).getFreqRvsl();
            BigDecimal totalAmtApp = listWhitelistMPan.get(0).getTotalAmtApp();
            BigDecimal totalAmtDc = listWhitelistMPan.get(0).getTotalAmtDc();
            BigDecimal totalAmtDcFree = listWhitelistMPan.get(0).getTotalAmtDcFree();
            BigDecimal totalAmtRvsl = listWhitelistMPan.get(0).getTotalAmtRvsl();
            BigDecimal totalAmt = listWhitelistMPan.get(0).getTotalAmt();
            String active = listWhitelistMPan.get(0).getActive();

            moduleFactory.getMPanTresholdService().updateMPan(mpan, acqId, freqApp, freqDc, freqDcFree, freqRvsl, totalAmtApp, totalAmtDc, totalAmtDcFree, totalAmtRvsl, totalAmt, active);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_WHITELIST_MPAN + "?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<WhitelistQRBean> getListWhitelistMPan() {
        return listWhitelistMPan;
    }

    public void setListWhitelistMPan(List<WhitelistQRBean> listWhitelistMPan) {
        this.listWhitelistMPan = listWhitelistMPan;
    }
}

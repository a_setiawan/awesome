package com.rintis.marketing.web.jsf.main.pan;

import com.google.common.collect.ComparisonChain;
import com.rintis.marketing.beans.bean.bi.DetailPanAnomalyBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by aabraham on 26/02/2019.
 */
@ManagedBean
@ViewScoped
public class DetailPanAnomalyInquiryManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<DetailPanAnomalyBean> list;
    private String pPeriod;
    private String pTimeFr;
    private String pTimeTo;
    private String pPan;
    private String pPengirim;
    private String pIssuerBank;
    private String pTranType;
    private String pTranTypeId;
    private String pGroupId;
    private String pStatus;
    private int totalApp;
    private int totalDc;
    private int totalDcFree;
    private int totalRvsl;
    private int colspan;
    private String totalAmt;
    private Boolean beneficiary;
    private Boolean sender;
    /*Add by aaw 22/07/2021*/
    private boolean balanceInquiry;
    /*End add*/

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            //190926 Modify by AAH encode param
            pPeriod = paramEncode(getRequestParameterMap().get("pPeriod").toString());
            pTimeFr = paramEncode(getRequestParameterMap().get("pTimeFr").toString().replace(":", ""));
            pTimeTo = paramEncode(getRequestParameterMap().get("pTimeTo").toString().replace(":", ""));
            pPan = paramEncode(getRequestParameterMap().get("pPan").toString());
            pPengirim = paramEncode(getRequestParameterMap().get("pPengirim").toString());
            pIssuerBank = paramEncode(getRequestParameterMap().get("pIssuerBank").toString());
            pTranType = paramEncode(getRequestParameterMap().get("pTranType").toString());
            pStatus = paramEncode(getRequestParameterMap().get("pStatus").toString());
            pGroupId = moduleFactory.getPanService().getGroupIdFromTrxName(pTranType);
            pTranTypeId = moduleFactory.getPanService().getTrxIdFromTrxName(pTranType);

            /*Add and modify by aaw 22/07/2021, penambahan kondisi untuk cek tran code 32*/
            if("40".equals(pTranTypeId) || "32".equals(pTranTypeId)){
                sender = true;
            }else{
                sender = false;
            }

            if ("30".equals(pTranTypeId)) {
                balanceInquiry = true;
            } else {
                balanceInquiry = false;
            }
            /*End*/

            if(pPengirim.trim().isEmpty()){
                pPengirim = " ";
            }else{
                pPengirim = pPengirim.trim();
            }

            if (pGroupId.equals("POS")){
                pPengirim = "-";
            }

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            /*Modify by aaw 22/07/2021, menambahkan sorting*/
            List<DetailPanAnomalyBean> d = new ArrayList<>();
            if (pGroupId.equals("ATM")){
                System.out.println("PAN Anomaly Inquiry Detail - " + getUserInfoBean().getUserId() + " - ATM - " + pPeriod + " - " + pTimeFr + " - " + pTimeTo + " - " + pPan + " - " + pTranTypeId + " - " + pPengirim);
                d = moduleFactory.getPanService().listDetailAtm(pPeriod, pTimeFr, pTimeTo, pPan, pTranTypeId, pPengirim);
            }else if(pGroupId.equals("POS")){
                System.out.println("PAN Anomaly Inquiry Detail - " + getUserInfoBean().getUserId() + " - POS - " +pPeriod + " - " + pTimeFr + " - " + pTimeTo + " - " + pPan + " - " + pTranTypeId);
                d = moduleFactory.getPanService().listDetailPos(pPeriod, pTimeFr, pTimeTo, pPan, pTranTypeId, pPengirim, pStatus);
            }

            if(!d.isEmpty()) {
                list = sortingListPan(d);
            }
            /*End*/

            /*Modify by aaw 21/07/2021, menambahkan tran code 32*/
            if("40".equals(pTranTypeId) || "32".equals(pTranTypeId)){
            /*End*/
                beneficiary = true;
                colspan = 13;
            }else{
                beneficiary = false;
                colspan = 10;
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    /*Add by aaw 22/07/2021, sorting data*/
    public List<DetailPanAnomalyBean> sortingListPan(List<DetailPanAnomalyBean> in) {
        Collections.sort(in, new Comparator<DetailPanAnomalyBean>() {
            @Override
            public int compare(DetailPanAnomalyBean o1, DetailPanAnomalyBean o2) {
                return ComparisonChain.start()
                    .compare(o1.getPeriod(), o2.getPeriod())
                    .compare(o1.getTrxTime(), o2.getTrxTime())
                    .result();
            }
        });
        return in;
    }
    /*End add*/

    public String paramEncode(String data){
        //encrypt param
        if(!data.equals(null)){
            data = HttpUtils.encodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }

    public int getValueAppTotal() {
        totalApp = 0;
        if(list != null){
            for(DetailPanAnomalyBean l : list) {
                totalApp += Integer.parseInt(l.getApp().toString());
            }
        }
        return totalApp;
    }

    public int getValueDcTotal() {
        totalDc = 0;
        if(list != null){
            for(DetailPanAnomalyBean l : list) {
                totalDc += Integer.parseInt(l.getDc().toString());
            }
        }
        return totalDc;
    }

    public int getValueDcFreeTotal() {
        totalDcFree = 0;
        if(list != null){
            for(DetailPanAnomalyBean l : list) {
                totalDcFree += Integer.parseInt(l.getDcFree().toString());
            }
        }
        return totalDcFree;
    }

    public int getValueRvslTotal() {
        totalRvsl = 0;
        if(list != null){
            for(DetailPanAnomalyBean l : list) {
                totalRvsl += Integer.parseInt(l.getRvsl().toString());
            }
        }
        return totalRvsl;
    }

    public String getValueAmtTotal() {
        Double totAmt = 0.00;

        if(list != null){
            for(DetailPanAnomalyBean l : list) {
                totAmt += Double.parseDouble(l.getAmount().toString());
            }
        }

        DecimalFormat formatter = new DecimalFormat("###,###,###.00");
        totalAmt = formatter.format(totAmt);

        return totalAmt;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<DetailPanAnomalyBean> getList() {
        return list;
    }

    public void setList(List<DetailPanAnomalyBean> list) {
        this.list = list;
    }

    public String getpPeriod() {
        return pPeriod;
    }

    public void setpPeriod(String pPeriod) {
        this.pPeriod = pPeriod;
    }

    public String getpTimeFr() {
        return pTimeFr;
    }

    public void setpTimeFr(String pTimeFr) {
        this.pTimeFr = pTimeFr;
    }

    public String getpTimeTo() {
        return pTimeTo;
    }

    public void setpTimeTo(String pTimeTo) {
        this.pTimeTo = pTimeTo;
    }

    public String getpPan() {
        return pPan;
    }

    public void setpPan(String pPan) {
        this.pPan = pPan;
    }

    public String getpPengirim() {
        return pPengirim;
    }

    public void setpPengirim(String pPengirim) {
        this.pPengirim = pPengirim;
    }

    public String getpIssuerBank() {
        return pIssuerBank;
    }

    public void setpIssuerBank(String pIssuerBank) {
        this.pIssuerBank = pIssuerBank;
    }

    public String getpTranType() {
        return pTranType;
    }

    public void setpTranType(String pTranType) {
        this.pTranType = pTranType;
    }

    public String getpTranTypeId() {
        return pTranTypeId;
    }

    public void setpTranTypeId(String pTranTypeId) {
        this.pTranTypeId = pTranTypeId;
    }

    public String getpGroupId() {
        return pGroupId;
    }

    public void setpGroupId(String pGroupId) {
        this.pGroupId = pGroupId;
    }

    public Boolean getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(Boolean beneficiary) {
        this.beneficiary = beneficiary;
    }

    public int getColspan() {
        return colspan;
    }

    public void setColspan(int colspan) {
        this.colspan = colspan;
    }

    public Boolean getSender() {
        return sender;
    }

    public void setSender(Boolean sender) {
        this.sender = sender;
    }

    public String getpStatus() {
        return pStatus;
    }

    public void setpStatus(String pStatus) {
        this.pStatus = pStatus;
    }

    public boolean isBalanceInquiry() {
        return balanceInquiry;
    }

    public void setBalanceInquiry(boolean balanceInquiry) {
        this.balanceInquiry = balanceInquiry;
    }
}

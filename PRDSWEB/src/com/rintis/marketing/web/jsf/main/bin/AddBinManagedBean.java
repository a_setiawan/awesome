package com.rintis.marketing.web.jsf.main.bin;


import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.Bin;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.*;

@ManagedBean
@ViewScoped
public class AddBinManagedBean  extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private Bin listBin;
    private List<BankBean> listBank;
    private String selectedBankId, binNo, binLength;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            Map<String, Object> paramBank = new HashMap<>();
            listBank = moduleFactory.getMasterService().listBankCA(paramBank);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String addAction() {
        try {
            if (binNo.equals("")) {
                glowMessageError("Enter BIN No", "");
                return "";
            }
            if (binLength.equals("")) {
                glowMessageError("Enter BIN Length", "");
                return "";
            }

            moduleFactory.getMasterService().addBin(selectedBankId, binNo, binLength);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "/apps/bin/listBin?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public Bin getListBin() {
        return listBin;
    }

    public void setListBin(Bin listBin) {
        this.listBin = listBin;
    }

    public List<BankBean> getListBank() {
        return listBank;
    }

    public void setListBank(List<BankBean> listBank) {
        this.listBank = listBank;
    }

    public String getSelectedBankId() {
        return selectedBankId;
    }

    public void setSelectedBankId(String selectedBankId) {
        this.selectedBankId = selectedBankId;
    }

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }

    public String getBinLength() {
        return binLength;
    }

    public void setBinLength(String binLength) {
        this.binLength = binLength;
    }


}

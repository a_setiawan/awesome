package com.rintis.marketing.web.jsf.main.menu;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import com.rintis.marketing.beans.entity.MenuRoleType;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.constant.DataConstant;

@ManagedBean
@ViewScoped
public class ListMenuRoleTypeManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    
    private List<MenuRoleType> listMenuRoleType;
    private MenuRoleType selectedDeleteItem;
    
    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listMenuRoleType = moduleFactory.getMenuService().listMenuRoleType(DataConstant.MENU_ROLE_TYPE_NOT_HIDDEN);



            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void deleteActionListener(ActionEvent event) {
        try {
            boolean r = moduleFactory.getMenuService().checkMenuRoleTypeUsed(selectedDeleteItem.getMenuRoleTypeId());
            if (r) {
                glowMessageError("Can't delete. Role already used", "");
                return;
            }

            moduleFactory.getMenuService().deleteMenuRoleType(selectedDeleteItem.getMenuRoleTypeId());
            listMenuRoleType = moduleFactory.getMenuService().listMenuRoleType(DataConstant.MENU_ROLE_TYPE_NOT_HIDDEN);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            glowMessageError("This role has already assigned to user", "");
        }
    }

    public String renderDelete(String statusId) {
        if (statusId.equals(DataConstant.Y) ) {
            return "true";
        } else {
            return "false";
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<MenuRoleType> getListMenuRoleType() {
        return listMenuRoleType;
    }

    public void setListMenuRoleType(List<MenuRoleType> listMenuRoleType) {
        this.listMenuRoleType = listMenuRoleType;
    }

    public MenuRoleType getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(MenuRoleType selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }





}

package com.rintis.marketing.web.jsf.main.menu;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.ParamConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import com.rintis.marketing.beans.entity.MenuRoleType;
import com.rintis.marketing.web.common.PageConstant;

@ManagedBean
@ViewScoped
public class EditMenuRoleTypeManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    
    private MenuRoleType menuRoleType;
    private String message;
    
    @Override
    @PostConstruct
    protected void initManagedBean() {
        message = "";
        String id = (String)getRequestParameterMap().get(ParamConstant.PARAM_MENU_ROLE_TYPE_ID);
        if (id != null) {
            try {
                menuRoleType = moduleFactory.getMenuService().getMenuRoleType(Integer.parseInt(id));
                //log.info(menuRoleType);
                if (menuRoleType == null) {
                    message = "Data not available";
                }


                if (!isPostback()) {
                    createTlog(moduleFactory);
                }
            } catch(Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }
    
    public String saveAction() {
        try {
            if (menuRoleType.getMenuRoleName().length() == 0) {
                glowMessageError("Enter Role Name", "");
                return "";
            }
            MenuRoleType mrt = moduleFactory.getMenuService().getMenuRoleByName(menuRoleType.getMenuRoleName());
            if (mrt != null) {
                if (mrt.getMenuRoleTypeId().intValue() != menuRoleType.getMenuRoleTypeId().intValue() ) {
                    glowMessageError("Role Name already exist", "");
                    menuRoleType.setMenuRoleName("");
                    return "";
                }
            }

            moduleFactory.getMenuService().updateMenuRoleType(menuRoleType);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_MENU_ROLE_TYPE_LIST_MENU_ROLE_TYPE + "?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public MenuRoleType getMenuRoleType() {
        return menuRoleType;
    }

    public void setMenuRoleType(MenuRoleType menuRoleType) {
        this.menuRoleType = menuRoleType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

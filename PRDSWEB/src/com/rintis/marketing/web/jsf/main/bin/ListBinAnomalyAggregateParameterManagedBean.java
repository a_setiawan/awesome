package com.rintis.marketing.web.jsf.main.bin;

import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.AlertThresholdGroup;
import com.rintis.marketing.beans.entity.BinList;
import com.rintis.marketing.beans.entity.GroupBank;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by aabraham on 01/02/2021.
 */
@ManagedBean
@ViewScoped
public class ListBinAnomalyAggregateParameterManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<BinList> listBin;
    private BinList selectedDeleteItem;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listBin = moduleFactory.getMasterService().listBin();

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String deleteActionListener(ActionEvent event) {
        try {
            moduleFactory.getMasterService().deleteListBin(selectedDeleteItem.getListId());
            listBin = moduleFactory.getMasterService().listBin();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<BinList> getListBin() {
        return listBin;
    }

    public void setListBin(List<BinList> listBin) {
        this.listBin = listBin;
    }

    public BinList getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(BinList selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }
}

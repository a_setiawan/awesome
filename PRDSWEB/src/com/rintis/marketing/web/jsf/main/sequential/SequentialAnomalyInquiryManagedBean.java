package com.rintis.marketing.web.jsf.main.sequential;

import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.beans.bean.bi.SequentialAnomalyBean;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import org.hibernate.SessionFactory;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@ManagedBean
@ViewScoped
public class SequentialAnomalyInquiryManagedBean extends AbstractManagedBean {

    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private SessionFactory sessionFactory;
    private List<SequentialAnomalyBean> list;
    private List<PanTrxidBean> listTrxId;
    private Date startDate;
    private Date endDate;
    private String periodFr;
    private String periodTo;
    private Boolean roleBank;
    private String transaction;

    public SequentialAnomalyInquiryManagedBean() {}

    @Override
    @PostConstruct
    protected void initManagedBean() {

        try {
            menuId = "052402";
            pageTitle = getMenuName(moduleFactory, menuId);

            startDate = new Date();
            endDate = new Date();
            SimpleDateFormat datefmt = new SimpleDateFormat("yyMMdd");
            periodFr = datefmt.format(startDate);
            periodTo = datefmt.format(endDate);

            listTrxId = moduleFactory.getSequentialService().listTrxId();

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            UserInfoBean userInfoBean = getUserInfoBean();
            String bank_id = "";
            if (userInfoBean.getUserLoginBank() != null) {
                bank_id = userInfoBean.getUserLoginBank().getBankId();
                roleBank = false;
            } else {
                roleBank = true;
            }

            SimpleDateFormat datefmt = new SimpleDateFormat("yyMMdd");
            periodFr = datefmt.format(startDate);
            periodTo = datefmt.format(endDate);

            long numOfDays = getDifferenceDays(startDate, endDate) + 1;

            if (numOfDays <= 90) {
                list = moduleFactory.getSequentialService().listSequentialAnomaly(periodFr, periodTo, transaction, bank_id);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return "";
    }

    public String errorMsg(){
        long days = Integer.parseInt(periodTo) - Integer.parseInt(periodFr);
        if (days > 90) {
            glowMessageError("Max 90 Days", "");
        }

        return "";
    }

    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public String paramDecode(String data){
        //encrypt param
        if(!data.equals(null)){
            data = HttpUtils.decodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<SequentialAnomalyBean> getList() {
        return list;
    }

    public void setList(List<SequentialAnomalyBean> list) {
        this.list = list;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPeriodFr() {
        return periodFr;
    }

    public void setPeriodFr(String periodFr) {
        this.periodFr = periodFr;
    }

    public String getPeriodTo() {
        return periodTo;
    }

    public void setPeriodTo(String periodTo) {
        this.periodTo = periodTo;
    }

    public Boolean getRoleBank() {
        return roleBank;
    }

    public void setRoleBank(Boolean roleBank) {
        this.roleBank = roleBank;
    }

    public List<PanTrxidBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<PanTrxidBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }
}

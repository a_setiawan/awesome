package com.rintis.marketing.web.jsf.main.login;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.login.LoginService;
import com.rintis.marketing.web.common.SessionConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

@ManagedBean
@RequestScoped
public class LogoutManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{"+SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY+"}")
    private ModuleFactory moduleFactory;
    //@ManagedProperty(value = "#{authenticationService}")
    //private AuthenticationService authenticationService;
    
    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    
    public String logoutAction() {
        try {
            getSessionMap().remove(SessionConstant.SESSION_USERINFO);
            //moduleFactory.getAuthenticationService().logout();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        externalContext.invalidateSession();//LoginService.getIDBank = null;
        HttpServletResponse response = (HttpServletResponse)externalContext.getResponse();
        HttpServletRequest request = (HttpServletRequest)externalContext.getRequest();
        try {
            response.sendRedirect(request.getContextPath() + "/index.jsp");
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        facesContext.responseComplete();
        return "";

    }


    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }


    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }


    
    
    
    
    
    
}

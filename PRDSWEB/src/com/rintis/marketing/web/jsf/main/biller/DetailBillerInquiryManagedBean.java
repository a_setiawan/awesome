package com.rintis.marketing.web.jsf.main.biller;

import com.rintis.marketing.beans.bean.biller.DetailBillerAnomalyBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by aaw 20211111
 */

@ManagedBean
@ViewScoped
public class DetailBillerInquiryManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<DetailBillerAnomalyBean> list;
    private String pPeriod;
    private String pTimeFr;
    private String pTimeTo;
    private String pInstCd;
    private String pCustNo;
    private String pCustName;
    private String pTranType;
    private String pTranTypeName;
    private String pPeriodFrom;
    private String pPeriodTo;
    private String pPeriodType;
    private String totalAmt;
    private String bankName = "";
    private String billerName = "";

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            pPeriod = paramDecode(getRequestParameterMap().get("pPeriod").toString());
            pTimeFr = paramDecode(getRequestParameterMap().get("pTimeFr").toString().replace(":", ""));
            pTimeTo = paramDecode(getRequestParameterMap().get("pTimeTo").toString().replace(":", ""));
            pTimeTo = paramDecode(getRequestParameterMap().get("pTimeTo").toString());
            pInstCd = paramDecode(getRequestParameterMap().get("pInstCd").toString());
            pCustNo = paramDecode(getRequestParameterMap().get("pCustNo").toString());
            pCustName = paramDecode(getRequestParameterMap().get("pCustName").toString());
            pPeriodFrom = paramDecode(getRequestParameterMap().get("pPeriodFrom").toString());
            pPeriodTo = paramDecode(getRequestParameterMap().get("pPeriodTo").toString());
            pPeriodType = paramDecode(getRequestParameterMap().get("pPeriodType").toString());
            pTranType = paramDecode(getRequestParameterMap().get("pTranType").toString());
            pTranTypeName = paramDecode(getRequestParameterMap().get("pTranTypeName").toString());

            if (!"".equals(pInstCd)) {
                billerName = moduleFactory.getBillerService().findBillerById(pInstCd).getBillerName();
            }
            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            list = moduleFactory.getBillerService().listDetailBiller(pPeriod, pTimeFr, pTimeTo, pInstCd, pCustNo, pCustName, pTranType, pPeriodType, pPeriodFrom, pPeriodTo);
            if (list.size() > 0) {
                bankName = list.get(0).getIssuer();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String paramDecode(String data){
        if(!data.equals(null)){
            data = HttpUtils.encodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }

    public String getValueAmtTotal() {
        Double totAmt = 0.00;

        if(list != null){
            for(DetailBillerAnomalyBean l : list) {
                totAmt += Double.parseDouble(l.getAmount().toString());
            }
        }

        DecimalFormat formatter = new DecimalFormat("###,###,###.00");
        totalAmt = formatter.format(totAmt);

        return totalAmt;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<DetailBillerAnomalyBean> getList() {
        return list;
    }

    public void setList(List<DetailBillerAnomalyBean> list) {
        this.list = list;
    }

    public String getpPeriod() {
        return pPeriod;
    }

    public void setpPeriod(String pPeriod) {
        this.pPeriod = pPeriod;
    }

    public String getpTimeFr() {
        return pTimeFr;
    }

    public void setpTimeFr(String pTimeFr) {
        this.pTimeFr = pTimeFr;
    }

    public String getpTimeTo() {
        return pTimeTo;
    }

    public void setpTimeTo(String pTimeTo) {
        this.pTimeTo = pTimeTo;
    }

    public String getpInstCd() {
        return pInstCd;
    }

    public void setpInstCd(String pInstCd) {
        this.pInstCd = pInstCd;
    }

    public String getpCustNo() {
        return pCustNo;
    }

    public void setpCustNo(String pCustNo) {
        this.pCustNo = pCustNo;
    }

    public String getpCustName() {
        return pCustName;
    }

    public void setpCustName(String pCustName) {
        this.pCustName = pCustName;
    }

    public String getpTranType() {
        return pTranType;
    }

    public void setpTranType(String pTranType) {
        this.pTranType = pTranType;
    }

    public String getpTranTypeName() {
        return pTranTypeName;
    }

    public void setpTranTypeName(String pTranTypeName) {
        this.pTranTypeName = pTranTypeName;
    }

    public String getpPeriodFrom() {
        return pPeriodFrom;
    }

    public void setpPeriodFrom(String pPeriodFrom) {
        this.pPeriodFrom = pPeriodFrom;
    }

    public String getpPeriodTo() {
        return pPeriodTo;
    }

    public void setpPeriodTo(String pPeriodTo) {
        this.pPeriodTo = pPeriodTo;
    }

    public String getpPeriodType() {
        return pPeriodType;
    }

    public void setpPeriodType(String pPeriodType) {
        this.pPeriodType = pPeriodType;
    }

    public String getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(String totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }
}

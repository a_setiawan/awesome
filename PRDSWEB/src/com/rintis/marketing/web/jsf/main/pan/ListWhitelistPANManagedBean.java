package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.WhitelistBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aabraham on 11/02/2019.
 */
@ManagedBean
@ViewScoped
public class ListWhitelistPANManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;

    private List<WhitelistBean> listWhitelistPan;
    private WhitelistBean selectedDeleteItem;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listWhitelistPan = moduleFactory.getPanService().listPan();
            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    public void deleteActionListener(ActionEvent event) {
        try {
            moduleFactory.getPanService().deletePan(selectedDeleteItem.getPan().toString(), selectedDeleteItem.getPengirim().toString());
            listWhitelistPan = new ArrayList<>();
            listWhitelistPan = moduleFactory.getPanService().listPan();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<WhitelistBean> getListWhitelistPan() {
        return listWhitelistPan;
    }

    public void setListWhitelistPan(List<WhitelistBean> listWhitelistPan) {
        this.listWhitelistPan = listWhitelistPan;
    }

    public WhitelistBean getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(WhitelistBean selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }
}

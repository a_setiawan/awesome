package com.rintis.marketing.web.jsf.main.monitor.old;

import com.martinlinha.c3faces.model.C3DataSet;
import com.martinlinha.c3faces.model.C3DataSetString;
import com.martinlinha.c3faces.model.C3ViewDataSet;
import com.martinlinha.c3faces.model.C3ViewDataSetString;
import com.martinlinha.c3faces.script.property.Data;
import com.martinlinha.c3faces.script.property.Point;
import com.rintis.marketing.beans.bean.bi.C3ChartBean;
import com.rintis.marketing.beans.bean.bi.EsqTrx5MinBean;
import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.EisBank;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.MathUtils;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.*;

@ManagedBean
@ViewScoped
public class TransactionIndicatorMonitoringHourShowPrevDayPercentManagedBeanOld extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<BankBean> listBank;
    private String maxDate;
    private int defaultRangeInHour;
    private Date selectedEndDate;
    private Integer selectedEndHour;
    private Date selectedStartDate;
    private Integer selectedStartHour;
    private String selectedBankId;
    private String selectedBankName;
    private String render;
    private List<EsqTrxIdBean> listTrxId;
    private String selectedTrxId;
    private List<EsqTrx5MinBean>  list;
    private List<EsqTrxIdBean> listTrxIdIndicator;
    private String[] selectedTransactionIndicatorId;
    private String selectedTrxName;
    private String selectedTransactionIndicatorName;
    private String graphRefreshInterval;
    private boolean showTable;
    private boolean disableAutoRefresh;
    private String disableAutoRefreshString;
    private String renderResult;
    private boolean showPercent;
    private String showPercentButtonText;

    private Date selectedTodayDate;
    private Date selectedPrevDate;

    private String renderAcqOnly;
    private String renderIssOnly;
    private String renderBnfOnly;
    private String renderAcqIss;
    private String renderAcqBnf;
    private String renderIssBnf;

    private Boolean[] activeCurr;
    private Boolean[] activePrev;
    private boolean checkAllCurr;
    private boolean checkAllPrev;

    private Boolean[] activePercentPrev;
    private boolean checkAllPercentPrev;


    /*private LineChartModel dataReportAcqOnly;
    private LineChartModel dataReportIssOnly;
    private LineChartModel dataReportBnfOnly;
    private LineChartModel dataReportAcqIss;
    private LineChartModel dataReportAcqBnf;
    private LineChartModel dataReportIssBnf;*/

    private Data dataAcqOnly;
    private com.martinlinha.c3faces.script.property.Axis axisAcqOnly;
    private Point pointAcqOnly;

    private Data dataIssOnly;
    private com.martinlinha.c3faces.script.property.Axis axisIssOnly;
    private Point pointIssOnly;

    private Data dataBnfOnly;
    private com.martinlinha.c3faces.script.property.Axis axisBnfOnly;
    private Point pointBnfOnly;

    private Data dataAcqIss;
    private com.martinlinha.c3faces.script.property.Axis axisAcqIss;
    private Point pointAcqIss;

    private Data dataAcqBnf;
    private com.martinlinha.c3faces.script.property.Axis axisAcqBnf;
    private Point pointAcqBnf;

    private Data dataIssBnf;
    private com.martinlinha.c3faces.script.property.Axis axisIssBnf;
    private Point pointIssBnf;

    C3ChartBean c3ChartBeanAcqOnly;
    C3ChartBean c3ChartBeanIssOnly;
    C3ChartBean c3ChartBeanBnfOnly;

    C3ChartBean c3ChartBeanAcqIss;
    C3ChartBean c3ChartBeanAcqBnf;
    C3ChartBean c3ChartBeanIssBnf;

    private String renderPrevDate;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0509";
            pageTitle = getMenuName(moduleFactory, menuId);

            //selectedBankId = "";
            selectedTrxId = "";

            String s = StringUtils.checkNull((String)getRequestParameterMap().get("bid"));
            if (s.length() > 0) {
                selectedBankId = s;
            }

            s = StringUtils.checkNull((String)getRequestParameterMap().get("trxid"));
            if (s.length() > 0) {
                selectedTrxId = s;
            }

            activeCurr = new Boolean[4];
            activePrev = new Boolean[4];
            for(int i=0; i<4; i++) {
                activeCurr[i] = true;
                activePrev[i] = false;
            }
            renderPrevDate = "visibility: hidden;";
            checkAllCurr = false;
            checkAllPrev = false;

            activePercentPrev = new Boolean[3];
            for(int i=0; i<3; i++) {
                activePercentPrev[i] = false;
            }
            checkAllPercentPrev = false;

            renderResult = "false";
            disableAutoRefresh = false;
            disableAutoRefreshString = "true";
            showTable = false;
            graphRefreshInterval = DataConstant.DEFAULT_GRAPH_REFRESH_INTERVAL;
            defaultRangeInHour = Integer.parseInt(
                    moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(), DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_HOURLY)
            );
            render = "false";
            renderAcqOnly = "false";
            renderIssOnly = "false";
            renderBnfOnly = "false";
            renderAcqIss = "false";
            renderAcqBnf = "false";
            renderIssBnf = "false";
            showPercent = true;
            showPercentButtonText = "Show Percent";
            s = StringUtils.checkNull( (String)getRequestParameterMap().get("sp"));
            if (s.equalsIgnoreCase("true")) {
                showPercent = true;
                showPercentButtonText = "Show Percent";
            } else if (s.equalsIgnoreCase("false")) {
                showPercent = false;
                showPercentButtonText = "Show Total";
            }

            maxDate = DateUtils.convertDate(selectedEndDate, DateUtils.DATE_DDMMYYYY_HHMMSS);

            Map<String, Object> paramBank = new HashMap<>();
            listBank = moduleFactory.getMasterService().listBank(paramBank);

            listTrxId = moduleFactory.getMasterService().listEsqTrxId();


            Date now = new Date();
            selectedTodayDate = new Date();
            selectedPrevDate = DateUtils.addDate(Calendar.DAY_OF_MONTH, selectedTodayDate, -1);
            selectedEndDate = DateUtils.setDate(Calendar.HOUR_OF_DAY, now, DateUtils.getHour(now));
            selectedEndDate = DateUtils.setDate(Calendar.MINUTE, now, DateUtils.getMinute(now));
            Date thdate = selectedEndDate;
            if (selectedEndDate.getTime() < DateUtils.getStartOfDay(now).getTime()  ) {
                thdate = DateUtils.getEndOfDay235959(thdate);
                thdate = DateUtils.addDate(Calendar.SECOND, thdate, 1);
            } else {
                //log.info(thdate);
                //int thminute = DateUtils.getMinute(thdate) % 5;
                //log.info(thminute);
                thdate = DateUtils.setDate(Calendar.MINUTE, thdate, 59);
                thdate = DateUtils.setDate(Calendar.SECOND, thdate, 59);
                //log.info(thdate);
            }
            selectedEndDate = thdate;
            selectedEndHour = DateUtils.getHour(thdate);


            Date frdate = DateUtils.addDate(Calendar.HOUR_OF_DAY, thdate, defaultRangeInHour*-1);
            //log.info(frdate);
            //int frminute = DateUtils.getMinute(frdate) % 5;
            //log.info(frminute);
            frdate = DateUtils.addDate(Calendar.MINUTE, frdate, 0);
            frdate = DateUtils.setDate(Calendar.SECOND, frdate, 0);
            //log.info(frdate);
            selectedStartDate = frdate;
            selectedStartHour = DateUtils.getHour(frdate);

            submitAction();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void disableAutoRefreshChangeListener() {
        if (disableAutoRefresh) {
            disableAutoRefreshString = "false";
        } else {
            disableAutoRefreshString = "true";
        }
    }

    public String submitAction() {
        try {
            trxIdChangeListener();

            if (showPercent) {
                showPercentButtonText = "Show Total";
            } else {
                showPercentButtonText = "Show Percent";
            }

            if (selectedBankId.length() == 0) {
                glowMessageError("Select Bank", "");
                return "";
            }
            if (selectedTrxId.length() == 0) {
                glowMessageError("Select Transaction", "");
                return "";
            }
            if (selectedTransactionIndicatorId == null) {
                glowMessageError("Select Transaction Indicator", "");
                return "";
            }
            if (selectedTransactionIndicatorId.length == 0) {
                glowMessageError("Select Transaction Indicator", "");
                return "";
            }


            EisBank bank = moduleFactory.getMasterService().getBank(selectedBankId);
            selectedBankName = bank.getBankName();

            if (selectedTrxId.equalsIgnoreCase(DataConstant.ALL)) {
                selectedTrxName = DataConstant.STR_ALL_TRANSACTION;
            } else {
                EsqTrxIdBean trxid = moduleFactory.getMasterService().getEsqTrxId(selectedTrxId);
                selectedTrxName = trxid.getTrxname();
            }

            renderAcqOnly = "false";
            renderIssOnly = "false";
            renderBnfOnly = "false";
            renderAcqIss = "false";
            renderAcqBnf = "false";
            renderIssBnf = "false";

            renderResult = "true";

            selectedTransactionIndicatorName = "";

            if (selectedTrxId.equalsIgnoreCase(DataConstant.ALL)) {
                selectedTransactionIndicatorName = "All";
                renderAcqOnly = "true";
                renderIssOnly = "true";
                renderBnfOnly = "true";
                renderAcqIss = "true";
                renderAcqBnf = "true";
                renderIssBnf = "true";

            } else {
                for (String id : selectedTransactionIndicatorId) {
                    EsqTrxIdBean trxIndicator = moduleFactory.getMasterService().getEsqTrxIdIndicator(id);
                    if (selectedTransactionIndicatorName.length() == 0) {
                        selectedTransactionIndicatorName = trxIndicator.getTrxnameindicator();
                    } else {
                        selectedTransactionIndicatorName = selectedTransactionIndicatorName + ", " + trxIndicator.getTrxnameindicator();
                    }

                    if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ONLY)) {
                        renderAcqOnly = "true";
                    }
                    if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_ONLY)) {
                        renderIssOnly = "true";
                    }
                    if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__BNF_ONLY)) {
                        renderBnfOnly = "true";
                    }
                    if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ISS)) {
                        renderAcqIss = "true";
                    }
                    if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_BNF)) {
                        renderAcqBnf = "true";
                    }
                    if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_BNF)) {
                        renderIssBnf = "true";
                    }
                }
            }

            /*Date now = new Date();
            selectedDate = DateUtils.setDate(Calendar.HOUR_OF_DAY, selectedDate, DateUtils.getHour(now));
            selectedDate = DateUtils.setDate(Calendar.MINUTE, selectedDate, DateUtils.getMinute(now));
            Date thdate = selectedDate;
            if (selectedDate.getTime() < DateUtils.getStartOfDay(now).getTime()  ) {
                thdate = DateUtils.getEndOfDay235959(thdate);
                thdate = DateUtils.addDate(Calendar.SECOND, thdate, 1);
            } else {
                log.info(thdate);
                //int thminute = DateUtils.getMinute(thdate) % 5;
                //log.info(thminute);
                thdate = DateUtils.setDate(Calendar.MINUTE, thdate, 59);
                thdate = DateUtils.setDate(Calendar.SECOND, thdate, 59);
                log.info(thdate);
            }*/


            /*Date frdate = DateUtils.addDate(Calendar.HOUR_OF_DAY, thdate, -24);
            log.info(frdate);
            //int frminute = DateUtils.getMinute(frdate) % 5;
            //log.info(frminute);
            frdate = DateUtils.addDate(Calendar.MINUTE, frdate, 0);
            frdate = DateUtils.setDate(Calendar.SECOND, frdate, 0);
            log.info(frdate);*/

            Date thdate = selectedEndDate;
            Date frdate = selectedStartDate;
            int prevDay = 0;

            if (disableAutoRefresh) {
                thdate = selectedEndDate;
                //round minute/second to zero
                thdate = DateUtils.setMinute(thdate, 0);
                thdate = DateUtils.setSecond(thdate, 0);
                frdate = selectedStartDate;

                thdate = DateUtils.setDate(Calendar.HOUR_OF_DAY, thdate, selectedEndHour);
                frdate = DateUtils.setDate(Calendar.HOUR_OF_DAY, frdate, selectedStartHour);
            } else {
                thdate = new Date();
                //round minute/second to zero
                thdate = DateUtils.setMinute(thdate, 0);
                thdate = DateUtils.setSecond(thdate, 0);
                frdate = DateUtils.addDate(Calendar.HOUR, thdate, -1 * defaultRangeInHour);

                int d = (int)DateUtils.getDifferenceDays(selectedPrevDate, thdate);
                prevDay = Math.abs(d);
            }


            list = listDataTrx(frdate, thdate, selectedBankId, selectedTrxId, prevDay);




            List<EsqTrx5MinBean> listAcqOnly = new ArrayList<>();
            List<EsqTrx5MinBean> listIssOnly = new ArrayList<>();
            List<EsqTrx5MinBean> listBnfOnly = new ArrayList<>();
            List<EsqTrx5MinBean> listAcqIss = new ArrayList<>();
            List<EsqTrx5MinBean> listAcqBnf = new ArrayList<>();
            List<EsqTrx5MinBean> listIssBnf = new ArrayList<>();


            List<String> listDateStringAcqOnly = new ArrayList<>();
            List<Integer> listDataTotalTransaksiAcqOnly = new ArrayList<>();
            List<Integer> listDataTotalApproveAcqOnly = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeAcqOnly = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeAcqOnly = new ArrayList<>();
            List<Integer> listDataTotalTransaksiAcqOnlyPrev = new ArrayList<>();
            List<Integer> listDataTotalApproveAcqOnlyPrev = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeAcqOnlyPrev = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeAcqOnlyPrev = new ArrayList<>();

            List<String> listDateStringIssOnly = new ArrayList<>();
            List<Integer> listDataTotalTransaksiIssOnly = new ArrayList<>();
            List<Integer> listDataTotalApproveIssOnly = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeIssOnly = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeIssOnly = new ArrayList<>();
            List<Integer> listDataTotalTransaksiIssOnlyPrev = new ArrayList<>();
            List<Integer> listDataTotalApproveIssOnlyPrev = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeIssOnlyPrev = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeIssOnlyPrev = new ArrayList<>();

            List<String> listDateStringBnfOnly = new ArrayList<>();
            List<Integer> listDataTotalTransaksiBnfOnly = new ArrayList<>();
            List<Integer> listDataTotalApproveBnfOnly = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeBnfOnly = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeBnfOnly = new ArrayList<>();
            List<Integer> listDataTotalTransaksiBnfOnlyPrev = new ArrayList<>();
            List<Integer> listDataTotalApproveBnfOnlyPrev = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeBnfOnlyPrev = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeBnfOnlyPrev = new ArrayList<>();

            List<String> listDateStringAcqIss = new ArrayList<>();
            List<Integer> listDataTotalTransaksiAcqIss = new ArrayList<>();
            List<Integer> listDataTotalApproveAcqIss = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeAcqIss = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeAcqIss = new ArrayList<>();
            List<Integer> listDataTotalTransaksiAcqIssPrev = new ArrayList<>();
            List<Integer> listDataTotalApproveAcqIssPrev = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeAcqIssPrev = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeAcqIssPrev = new ArrayList<>();

            List<String> listDateStringAcqBnf = new ArrayList<>();
            List<Integer> listDataTotalTransaksiAcqBnf = new ArrayList<>();
            List<Integer> listDataTotalApproveAcqBnf = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeAcqBnf = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeAcqBnf = new ArrayList<>();
            List<Integer> listDataTotalTransaksiAcqBnfPrev = new ArrayList<>();
            List<Integer> listDataTotalApproveAcqBnfPrev = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeAcqBnfPrev = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeAcqBnfPrev = new ArrayList<>();

            List<String> listDateStringIssBnf = new ArrayList<>();
            List<Integer> listDataTotalTransaksiIssBnf = new ArrayList<>();
            List<Integer> listDataTotalApproveIssBnf = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeIssBnf = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeIssBnf = new ArrayList<>();
            List<Integer> listDataTotalTransaksiIssBnfPrev = new ArrayList<>();
            List<Integer> listDataTotalApproveIssBnfPrev = new ArrayList<>();
            List<Integer> listDataTotalDeclineChargeIssBnfPrev = new ArrayList<>();
            List<Integer> listDataTotalDeclineFreeIssBnfPrev = new ArrayList<>();


            List<BigDecimal> listDataPercentApprAcqOnly = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCAcqOnly = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFAcqOnly = new ArrayList<>();

            List<BigDecimal> listDataPercentApprIssOnly = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCIssOnly = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFIssOnly = new ArrayList<>();

            List<BigDecimal> listDataPercentApprBnfOnly = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCBnfOnly = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFBnfOnly = new ArrayList<>();

            List<BigDecimal> listDataPercentApprAcqIss = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCAcqIss = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFAcqIss = new ArrayList<>();

            List<BigDecimal> listDataPercentApprAcqBnf = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCAcqBnf = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFAcqBnf = new ArrayList<>();

            List<BigDecimal> listDataPercentApprIssBnf = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCIssBnf = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFIssBnf = new ArrayList<>();

            //prev
            List<BigDecimal> listDataPercentApprAcqOnlyPrev = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCAcqOnlyPrev = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFAcqOnlyPrev = new ArrayList<>();

            List<BigDecimal> listDataPercentApprIssOnlyPrev = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCIssOnlyPrev = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFIssOnlyPrev = new ArrayList<>();

            List<BigDecimal> listDataPercentApprBnfOnlyPrev = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCBnfOnlyPrev = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFBnfOnlyPrev = new ArrayList<>();

            List<BigDecimal> listDataPercentApprAcqIssPrev = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCAcqIssPrev = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFAcqIssPrev = new ArrayList<>();

            List<BigDecimal> listDataPercentApprAcqBnfPrev = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCAcqBnfPrev = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFAcqBnfPrev = new ArrayList<>();

            List<BigDecimal> listDataPercentApprIssBnfPrev = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclCIssBnfPrev = new ArrayList<>();
            List<BigDecimal> listDataPercentDeclFIssBnfPrev = new ArrayList<>();



            for(int i=0; i < list.size() - 1; i++) {
                EsqTrx5MinBean esq = list.get(i);
                Date d = DateUtils.convertDate(esq.getPeriod() + " " + esq.getTimeto(), DateUtils.DATE_YYMMDD_HHMM);
                listDateStringAcqOnly.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringIssOnly.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringBnfOnly.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringAcqIss.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringAcqBnf.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringIssBnf.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));

                for(String trxindid : selectedTransactionIndicatorId) {
                    //log.info(trxindid);
                    if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ONLY)) {
                        listAcqOnly.add(esq);
                        BigDecimal b = MathUtils.checkNull(esq.getFreq_acq_app())
                                .add(MathUtils.checkNull(esq.getFreq_acq_dc()))
                                .add(MathUtils.checkNull(esq.getFreq_acq_df()));

                        BigDecimal bprev = MathUtils.checkNull(esq.getFreq_acq_app_prev())
                                .add(MathUtils.checkNull(esq.getFreq_acq_dc_prev()))
                                .add(MathUtils.checkNull(esq.getFreq_acq_df_prev()));

                        BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acq_app()), b);
                        BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acq_dc(), b);
                        BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acq_df(), b);

                        BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acq_app_prev()), bprev);
                        BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_acq_dc_prev(), bprev);
                        BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_acq_df_prev(), bprev);

                        //csAcqOnlyTotal.set(esq.getPeriod() + " " + esq.getHourfr(), b);
                        //csAcqOnlyAppr.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acq_app()));
                        //csAcqOnlyDc.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acq_dc()));
                        //csAcqOnlyDf.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acq_df()));
                        listDataTotalTransaksiAcqOnly.add(b.intValue());
                        listDataTotalApproveAcqOnly.add(MathUtils.checkNull(esq.getFreq_acq_app()).intValue());
                        listDataTotalDeclineChargeAcqOnly.add(MathUtils.checkNull(esq.getFreq_acq_dc()).intValue());
                        listDataTotalDeclineFreeAcqOnly.add(MathUtils.checkNull(esq.getFreq_acq_df()).intValue());
                        listDataPercentApprAcqOnly.add(pctAppr.multiply(new BigDecimal("100")));
                        listDataPercentDeclCAcqOnly.add(pctDeclC.multiply(new BigDecimal("100")));
                        listDataPercentDeclFAcqOnly.add(pctDeclF.multiply(new BigDecimal("100")));

                        listDataTotalTransaksiAcqOnlyPrev.add(bprev.intValue());
                        listDataTotalApproveAcqOnlyPrev.add(MathUtils.checkNull(esq.getFreq_acq_app_prev()).intValue());
                        listDataTotalDeclineChargeAcqOnlyPrev.add(MathUtils.checkNull(esq.getFreq_acq_dc_prev()).intValue());
                        listDataTotalDeclineFreeAcqOnlyPrev.add(MathUtils.checkNull(esq.getFreq_acq_df_prev()).intValue());

                        listDataPercentApprAcqOnlyPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                        listDataPercentDeclCAcqOnlyPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                        listDataPercentDeclFAcqOnlyPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));
                    }

                    if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_ONLY)) {
                        listIssOnly.add(esq);
                        BigDecimal b = MathUtils.checkNull(esq.getFreq_iss_app())
                                .add(MathUtils.checkNull(esq.getFreq_iss_dc()))
                                .add(MathUtils.checkNull(esq.getFreq_iss_df()));

                        BigDecimal bprev = MathUtils.checkNull(esq.getFreq_iss_app_prev())
                                .add(MathUtils.checkNull(esq.getFreq_iss_dc_prev()))
                                .add(MathUtils.checkNull(esq.getFreq_iss_df_prev()));

                        BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_iss_app()), b);
                        BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_iss_dc(), b);
                        BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_iss_df(), b);

                        BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_iss_app_prev()), bprev);
                        BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_iss_dc_prev(), bprev);
                        BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_iss_df_prev(), bprev);

                        //csIssOnlyTotal.set(esq.getPeriod() + " " + esq.getHourfr(), b);
                        //csIssOnlyAppr.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_iss_app()));
                        //csIssOnlyDc.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_iss_dc()));
                        //csIssOnlyDf.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_iss_df()));
                        listDataTotalTransaksiIssOnly.add(b.intValue());
                        listDataTotalApproveIssOnly.add(MathUtils.checkNull(esq.getFreq_iss_app()).intValue());
                        listDataTotalDeclineChargeIssOnly.add(MathUtils.checkNull(esq.getFreq_iss_dc()).intValue());
                        listDataTotalDeclineFreeIssOnly.add(MathUtils.checkNull(esq.getFreq_iss_df()).intValue());
                        listDataPercentApprIssOnly.add(pctAppr.multiply(new BigDecimal("100")));
                        listDataPercentDeclCIssOnly.add(pctDeclC.multiply(new BigDecimal("100")));
                        listDataPercentDeclFIssOnly.add(pctDeclF.multiply(new BigDecimal("100")));

                        listDataTotalTransaksiIssOnlyPrev.add(bprev.intValue());
                        listDataTotalApproveIssOnlyPrev.add(MathUtils.checkNull(esq.getFreq_iss_app_prev()).intValue());
                        listDataTotalDeclineChargeIssOnlyPrev.add(MathUtils.checkNull(esq.getFreq_iss_dc_prev()).intValue());
                        listDataTotalDeclineFreeIssOnlyPrev.add(MathUtils.checkNull(esq.getFreq_iss_df_prev()).intValue());

                        listDataPercentApprIssOnlyPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                        listDataPercentDeclCIssOnlyPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                        listDataPercentDeclFIssOnlyPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));
                    }

                    if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__BNF_ONLY)) {
                        listBnfOnly.add(esq);
                        BigDecimal b = MathUtils.checkNull(esq.getFreq_bnf_app())
                                .add(MathUtils.checkNull(esq.getFreq_bnf_dc()))
                                .add(MathUtils.checkNull(esq.getFreq_bnf_df()));

                        BigDecimal bprev = MathUtils.checkNull(esq.getFreq_bnf_app_prev())
                                .add(MathUtils.checkNull(esq.getFreq_bnf_dc_prev()))
                                .add(MathUtils.checkNull(esq.getFreq_bnf_df_prev()));

                        BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_bnf_app()), b);
                        BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_bnf_dc(), b);
                        BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_bnf_df(), b);

                        BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_bnf_app_prev()), bprev);
                        BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_bnf_dc_prev(), bprev);
                        BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_bnf_df_prev(), bprev);

                        //csBnfOnlyTotal.set(esq.getPeriod() + " " + esq.getHourfr(), b);
                        //csBnfOnlyAppr.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_bnf_app()));
                        //csBnfOnlyDc.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_bnf_dc()));
                        //csBnfOnlyDf.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_bnf_df()));
                        listDataTotalTransaksiBnfOnly.add(b.intValue());
                        listDataTotalApproveBnfOnly.add(MathUtils.checkNull(esq.getFreq_bnf_app()).intValue());
                        listDataTotalDeclineChargeBnfOnly.add(MathUtils.checkNull(esq.getFreq_bnf_dc()).intValue());
                        listDataTotalDeclineFreeBnfOnly.add(MathUtils.checkNull(esq.getFreq_bnf_df()).intValue());
                        listDataPercentApprBnfOnly.add(pctAppr.multiply(new BigDecimal("100")));
                        listDataPercentDeclCBnfOnly.add(pctDeclC.multiply(new BigDecimal("100")));
                        listDataPercentDeclFBnfOnly.add(pctDeclF.multiply(new BigDecimal("100")));

                        listDataTotalTransaksiBnfOnlyPrev.add(bprev.intValue());
                        listDataTotalApproveBnfOnlyPrev.add(MathUtils.checkNull(esq.getFreq_bnf_app_prev()).intValue());
                        listDataTotalDeclineChargeBnfOnlyPrev.add(MathUtils.checkNull(esq.getFreq_bnf_dc_prev()).intValue());
                        listDataTotalDeclineFreeBnfOnlyPrev.add(MathUtils.checkNull(esq.getFreq_bnf_df_prev()).intValue());

                        listDataPercentApprBnfOnlyPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                        listDataPercentDeclCBnfOnlyPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                        listDataPercentDeclFBnfOnlyPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));
                    }

                    if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ISS)) {
                        listAcqIss.add(esq);
                        BigDecimal b = MathUtils.checkNull(esq.getFreq_acqIss_app())
                                .add(MathUtils.checkNull(esq.getFreq_acqIss_dc()))
                                .add(MathUtils.checkNull(esq.getFreq_acqIss_df()));

                        BigDecimal bprev = MathUtils.checkNull(esq.getFreq_acqIss_app_prev())
                                .add(MathUtils.checkNull(esq.getFreq_acqIss_dc_prev()))
                                .add(MathUtils.checkNull(esq.getFreq_acqIss_df_prev()));

                        BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqIss_app()), b);
                        BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acqIss_dc(), b);
                        BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acqIss_df(), b);

                        BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqIss_app_prev()), bprev);
                        BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_acqIss_dc_prev(), bprev);
                        BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_acqIss_df_prev(), bprev);

                        //csAcqIssTotal.set(esq.getPeriod() + " " + esq.getHourfr(), b);
                        //csAcqIssAppr.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acqIss_app()));
                        //csAcqIssDc.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acqIss_dc()));
                        //csAcqIssDf.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acqIss_df()));
                        listDataTotalTransaksiAcqIss.add(b.intValue());
                        listDataTotalApproveAcqIss.add(MathUtils.checkNull(esq.getFreq_acqIss_app()).intValue());
                        listDataTotalDeclineChargeAcqIss.add(MathUtils.checkNull(esq.getFreq_acqIss_dc()).intValue());
                        listDataTotalDeclineFreeAcqIss.add(MathUtils.checkNull(esq.getFreq_acqIss_df()).intValue());
                        listDataPercentApprAcqIss.add(pctAppr.multiply(new BigDecimal("100")));
                        listDataPercentDeclCAcqIss.add(pctDeclC.multiply(new BigDecimal("100")));
                        listDataPercentDeclFAcqIss.add(pctDeclF.multiply(new BigDecimal("100")));

                        listDataTotalTransaksiAcqIssPrev.add(bprev.intValue());
                        listDataTotalApproveAcqIssPrev.add(MathUtils.checkNull(esq.getFreq_acqIss_app_prev()).intValue());
                        listDataTotalDeclineChargeAcqIssPrev.add(MathUtils.checkNull(esq.getFreq_acqIss_dc_prev()).intValue());
                        listDataTotalDeclineFreeAcqIssPrev.add(MathUtils.checkNull(esq.getFreq_acqIss_df_prev()).intValue());

                        listDataPercentApprAcqIssPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                        listDataPercentDeclCAcqIssPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                        listDataPercentDeclFAcqIssPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));
                    }

                    if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_BNF)) {
                        listAcqBnf.add(esq);
                        BigDecimal b = MathUtils.checkNull(esq.getFreq_acqBnf_app())
                                .add(MathUtils.checkNull(esq.getFreq_acqBnf_dc()))
                                .add(MathUtils.checkNull(esq.getFreq_acqBnf_df()));

                        BigDecimal bprev = MathUtils.checkNull(esq.getFreq_acqBnf_app_prev())
                                .add(MathUtils.checkNull(esq.getFreq_acqBnf_dc_prev()))
                                .add(MathUtils.checkNull(esq.getFreq_acqBnf_df_prev()));

                        BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqBnf_app()), b);
                        BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_dc(), b);
                        BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_df(), b);

                        BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqBnf_app_prev()), bprev);
                        BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_dc_prev(), bprev);
                        BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_df_prev(), bprev);


                        //csAcqBnfTotal.set(esq.getPeriod() + " " + esq.getHourfr(), b);
                        //csAcqBnfAppr.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acqBnf_app()));
                        //csAcqBnfDc.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acqBnf_dc()));
                        //csAcqBnfDf.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_acqBnf_df()));
                        listDataTotalTransaksiAcqBnf.add(b.intValue());
                        listDataTotalApproveAcqBnf.add(MathUtils.checkNull(esq.getFreq_acqBnf_app()).intValue());
                        listDataTotalDeclineChargeAcqBnf.add(MathUtils.checkNull(esq.getFreq_acqBnf_dc()).intValue());
                        listDataTotalDeclineFreeAcqBnf.add(MathUtils.checkNull(esq.getFreq_acqBnf_df()).intValue());
                        listDataPercentApprAcqBnf.add(pctAppr.multiply(new BigDecimal("100")));
                        listDataPercentDeclCAcqBnf.add(pctDeclC.multiply(new BigDecimal("100")));
                        listDataPercentDeclFAcqBnf.add(pctDeclF.multiply(new BigDecimal("100")));

                        listDataTotalTransaksiAcqBnfPrev.add(bprev.intValue());
                        listDataTotalApproveAcqBnfPrev.add(MathUtils.checkNull(esq.getFreq_acqBnf_app_prev()).intValue());
                        listDataTotalDeclineChargeAcqBnfPrev.add(MathUtils.checkNull(esq.getFreq_acqBnf_dc_prev()).intValue());
                        listDataTotalDeclineFreeAcqBnfPrev.add(MathUtils.checkNull(esq.getFreq_acqBnf_df_prev()).intValue());

                        listDataPercentApprAcqBnfPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                        listDataPercentDeclCAcqBnfPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                        listDataPercentDeclFAcqBnfPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));
                    }

                    if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_BNF)) {
                        listIssBnf.add(esq);
                        BigDecimal b = MathUtils.checkNull(esq.getFreq_issBnf_app())
                                .add(MathUtils.checkNull(esq.getFreq_issBnf_dc()))
                                .add(MathUtils.checkNull(esq.getFreq_issBnf_df()));

                        BigDecimal bprev = MathUtils.checkNull(esq.getFreq_issBnf_app_prev())
                                .add(MathUtils.checkNull(esq.getFreq_issBnf_dc_prev()))
                                .add(MathUtils.checkNull(esq.getFreq_issBnf_df_prev()));

                        BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_issBnf_app()), b);
                        BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_issBnf_dc(), b);
                        BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_issBnf_df(), b);

                        BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_issBnf_app_prev()), bprev);
                        BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_issBnf_dc_prev(), bprev);
                        BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_issBnf_df_prev(), bprev);

                        //csIssBnfTotal.set(esq.getPeriod() + " " + esq.getHourfr(), b);
                        //csIssBnfAppr.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_issBnf_app()));
                        //csIssBnfDc.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_issBnf_dc()));
                        //csIssBnfDf.set(esq.getPeriod() + " " + esq.getHourfr(), MathUtils.checkNull(esq.getFreq_issBnf_df()));
                        listDataTotalTransaksiIssBnf.add(b.intValue());
                        listDataTotalApproveIssBnf.add(MathUtils.checkNull(esq.getFreq_issBnf_app()).intValue());
                        listDataTotalDeclineChargeIssBnf.add(MathUtils.checkNull(esq.getFreq_issBnf_dc()).intValue());
                        listDataTotalDeclineFreeIssBnf.add(MathUtils.checkNull(esq.getFreq_issBnf_df()).intValue());
                        listDataPercentApprIssBnf.add(pctAppr.multiply(new BigDecimal("100")));
                        listDataPercentDeclCIssBnf.add(pctDeclC.multiply(new BigDecimal("100")));
                        listDataPercentDeclFIssBnf.add(pctDeclF.multiply(new BigDecimal("100")));

                        listDataTotalTransaksiIssBnfPrev.add(bprev.intValue());
                        listDataTotalApproveIssBnfPrev.add(MathUtils.checkNull(esq.getFreq_issBnf_app_prev()).intValue());
                        listDataTotalDeclineChargeIssBnfPrev.add(MathUtils.checkNull(esq.getFreq_issBnf_dc_prev()).intValue());
                        listDataTotalDeclineFreeIssBnfPrev.add(MathUtils.checkNull(esq.getFreq_issBnf_df_prev()).intValue());

                        listDataPercentApprIssBnfPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                        listDataPercentDeclCIssBnfPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                        listDataPercentDeclFIssBnfPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));

                    }
                }
            }


            /*String seriesColor = "0000FF,00FF00,8F0000,FF0000";

            List<ChartSeries> listCs = null;

            listCs = new ArrayList<>();
            listCs.add(csAcqOnlyTotal);
            listCs.add(csAcqOnlyAppr);
            listCs.add(csAcqOnlyDc);
            listCs.add(csAcqOnlyDf);
            dataReportAcqOnly = new LineChartModel();
            generateChart(dataReportAcqOnly, listCs, seriesColor, titleAcqOnly);

            listCs = new ArrayList<>();
            listCs.add(csIssOnlyTotal);
            listCs.add(csIssOnlyAppr);
            listCs.add(csIssOnlyDc);
            listCs.add(csIssOnlyDf);
            dataReportIssOnly = new LineChartModel();
            generateChart(dataReportIssOnly, listCs, seriesColor, titleIssOnly);

            listCs = new ArrayList<>();
            listCs.add(csBnfOnlyTotal);
            listCs.add(csBnfOnlyAppr);
            listCs.add(csBnfOnlyDc);
            listCs.add(csBnfOnlyDf);
            dataReportBnfOnly = new LineChartModel();
            generateChart(dataReportBnfOnly, listCs, seriesColor, titleBnfOnly);

            listCs = new ArrayList<>();
            listCs.add(csAcqIssTotal);
            listCs.add(csAcqIssAppr);
            listCs.add(csAcqIssDc);
            listCs.add(csAcqIssDf);
            dataReportAcqIss = new LineChartModel();
            generateChart(dataReportAcqIss, listCs, seriesColor, titleAcqIss);

            listCs = new ArrayList<>();
            listCs.add(csAcqBnfTotal);
            listCs.add(csAcqBnfAppr);
            listCs.add(csAcqBnfDc);
            listCs.add(csAcqBnfDf);
            dataReportAcqBnf = new LineChartModel();
            generateChart(dataReportAcqBnf, listCs, seriesColor, titleAcqBnf);

            listCs = new ArrayList<>();
            listCs.add(csIssBnfTotal);
            listCs.add(csIssBnfAppr);
            listCs.add(csIssBnfDc);
            listCs.add(csIssBnfDf);
            dataReportIssBnf = new LineChartModel();
            generateChart(dataReportIssBnf, listCs, seriesColor, titleIssBnf);*/



            String title = selectedBankName + " - " + selectedTrxName + " - " + selectedTransactionIndicatorName + " - 5 Min";


            String colorTotal = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_TOTAL);
            String colorApprove = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_APPR);
            String colorDc = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_DC);
            String colorDf = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_DF);
            String colorTotalPrev = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_TOTAL_PREV);
            String colorApprovePrev = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_APPR_PREV);
            String colorDcPrev = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_DC_PREV);
            String colorDfPrev = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                    DataConstant.COLOR_TX_DF_PREV);

            if (!showPercent) {
                c3ChartBeanAcqOnly = new C3ChartBean();
                generateChart(listDataTotalTransaksiAcqOnly,
                        listDataTotalApproveAcqOnly,
                        listDataTotalDeclineChargeAcqOnly,
                        listDataTotalDeclineFreeAcqOnly,
                        listDataTotalTransaksiAcqOnlyPrev,
                        listDataTotalApproveAcqOnlyPrev,
                        listDataTotalDeclineChargeAcqOnlyPrev,
                        listDataTotalDeclineFreeAcqOnlyPrev,
                        listDateStringAcqOnly,
                        colorTotal, colorApprove, colorDc, colorDf,
                        colorTotalPrev, colorApprovePrev, colorDcPrev, colorDfPrev,
                        title,
                        DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                        DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                        c3ChartBeanAcqOnly);

                c3ChartBeanIssOnly = new C3ChartBean();
                generateChart(listDataTotalTransaksiIssOnly,
                        listDataTotalApproveIssOnly,
                        listDataTotalDeclineChargeIssOnly,
                        listDataTotalDeclineFreeIssOnly,
                        listDataTotalTransaksiIssOnlyPrev,
                        listDataTotalApproveIssOnlyPrev,
                        listDataTotalDeclineChargeIssOnlyPrev,
                        listDataTotalDeclineFreeIssOnlyPrev,
                        listDateStringIssOnly,
                        colorTotal, colorApprove, colorDc, colorDf,
                        colorTotalPrev, colorApprovePrev, colorDcPrev, colorDfPrev,
                        title,
                        DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                        DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                        c3ChartBeanIssOnly);

                c3ChartBeanBnfOnly = new C3ChartBean();
                generateChart(listDataTotalTransaksiBnfOnly,
                        listDataTotalApproveBnfOnly,
                        listDataTotalDeclineChargeBnfOnly,
                        listDataTotalDeclineFreeBnfOnly,
                        listDataTotalTransaksiBnfOnlyPrev,
                        listDataTotalApproveBnfOnlyPrev,
                        listDataTotalDeclineChargeBnfOnlyPrev,
                        listDataTotalDeclineFreeBnfOnlyPrev,
                        listDateStringBnfOnly,
                        colorTotal, colorApprove, colorDc, colorDf,
                        colorTotalPrev, colorApprovePrev, colorDcPrev, colorDfPrev,
                        title,
                        DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                        DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                        c3ChartBeanBnfOnly);

                c3ChartBeanAcqIss = new C3ChartBean();
                generateChart(listDataTotalTransaksiAcqIss,
                        listDataTotalApproveAcqIss,
                        listDataTotalDeclineChargeAcqIss,
                        listDataTotalDeclineFreeAcqIss,
                        listDataTotalTransaksiAcqIssPrev,
                        listDataTotalApproveAcqIssPrev,
                        listDataTotalDeclineChargeAcqIssPrev,
                        listDataTotalDeclineFreeAcqIssPrev,
                        listDateStringAcqIss,
                        colorTotal, colorApprove, colorDc, colorDf,
                        colorTotalPrev, colorApprovePrev, colorDcPrev, colorDfPrev,
                        title,
                        DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                        DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                        c3ChartBeanAcqIss);

                c3ChartBeanAcqBnf = new C3ChartBean();
                generateChart(listDataTotalTransaksiAcqBnf,
                        listDataTotalApproveAcqBnf,
                        listDataTotalDeclineChargeAcqBnf,
                        listDataTotalDeclineFreeAcqBnf,
                        listDataTotalTransaksiAcqBnfPrev,
                        listDataTotalApproveAcqBnfPrev,
                        listDataTotalDeclineChargeAcqBnfPrev,
                        listDataTotalDeclineFreeAcqBnfPrev,
                        listDateStringAcqBnf,
                        colorTotal, colorApprove, colorDc, colorDf,
                        colorTotalPrev, colorApprovePrev, colorDcPrev, colorDfPrev,
                        title,
                        DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                        DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                        c3ChartBeanAcqBnf);

                c3ChartBeanIssBnf = new C3ChartBean();
                generateChart(listDataTotalTransaksiIssBnf,
                        listDataTotalApproveIssBnf,
                        listDataTotalDeclineChargeIssBnf,
                        listDataTotalDeclineFreeIssBnf,
                        listDataTotalTransaksiIssBnfPrev,
                        listDataTotalApproveIssBnfPrev,
                        listDataTotalDeclineChargeIssBnfPrev,
                        listDataTotalDeclineFreeIssBnfPrev,
                        listDateStringIssBnf,
                        colorTotal, colorApprove, colorDc, colorDf,
                        colorTotalPrev, colorApprovePrev, colorDcPrev, colorDfPrev,
                        title,
                        DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                        DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                        c3ChartBeanIssBnf);

            } else {
                List<DsBean> listDsBean = new ArrayList<>();

                DsBean dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprAcqOnly);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCAcqOnly);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFAcqOnly);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                if (activePercentPrev[0]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentApprAcqOnlyPrev);
                    dsBean.setColor(colorApprovePrev);
                    dsBean.setName("% Approval Prev");
                    listDsBean.add(dsBean);
                }

                if (activePercentPrev[1]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentDeclCAcqOnlyPrev);
                    dsBean.setColor(colorDcPrev);
                    dsBean.setName("% Decline Charge Prev");
                    listDsBean.add(dsBean);
                }

                if (activePercentPrev[2]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentDeclFAcqOnlyPrev);
                    dsBean.setColor(colorDfPrev);
                    dsBean.setName("% Decline Free Prev");
                    listDsBean.add(dsBean);
                }

                c3ChartBeanAcqOnly = new C3ChartBean();
                generateChartMultiData(c3ChartBeanAcqOnly, listDsBean, listDateStringAcqOnly, title);


                //iss only
                listDsBean = new ArrayList<>();
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprIssOnly);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCIssOnly);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFIssOnly);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                if (activePercentPrev[0]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentApprIssOnlyPrev);
                    dsBean.setColor(colorApprovePrev);
                    dsBean.setName("% Approval Prev");
                    listDsBean.add(dsBean);
                }

                if (activePercentPrev[1]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentDeclCIssOnlyPrev);
                    dsBean.setColor(colorDcPrev);
                    dsBean.setName("% Decline Charge Prev");
                    listDsBean.add(dsBean);
                }

                if (activePercentPrev[2]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentDeclFIssOnlyPrev);
                    dsBean.setColor(colorDfPrev);
                    dsBean.setName("% Decline Free Prev");
                    listDsBean.add(dsBean);
                }

                c3ChartBeanIssOnly = new C3ChartBean();
                generateChartMultiData(c3ChartBeanIssOnly, listDsBean, listDateStringIssOnly, title);


                //bnf only
                listDsBean = new ArrayList<>();
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprBnfOnly);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCBnfOnly);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFBnfOnly);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                if (activePercentPrev[0]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentApprBnfOnlyPrev);
                    dsBean.setColor(colorApprovePrev);
                    dsBean.setName("% Approval Prev");
                    listDsBean.add(dsBean);
                }

                if (activePercentPrev[1]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentDeclCBnfOnlyPrev);
                    dsBean.setColor(colorDcPrev);
                    dsBean.setName("% Decline Charge Prev");
                    listDsBean.add(dsBean);
                }

                if (activePercentPrev[2]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentDeclFBnfOnlyPrev);
                    dsBean.setColor(colorDfPrev);
                    dsBean.setName("% Decline Free Prev");
                    listDsBean.add(dsBean);
                }

                c3ChartBeanBnfOnly = new C3ChartBean();
                generateChartMultiData(c3ChartBeanBnfOnly, listDsBean, listDateStringBnfOnly, title);


                //acq iss
                listDsBean = new ArrayList<>();
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprAcqIss);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCAcqIss);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFAcqIss);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                if (activePercentPrev[0]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentApprAcqIssPrev);
                    dsBean.setColor(colorApprovePrev);
                    dsBean.setName("% Approval Prev");
                    listDsBean.add(dsBean);
                }

                if (activePercentPrev[1]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentDeclCAcqIssPrev);
                    dsBean.setColor(colorDcPrev);
                    dsBean.setName("% Decline Charge Prev");
                    listDsBean.add(dsBean);
                }

                if (activePercentPrev[2]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentDeclFAcqIssPrev);
                    dsBean.setColor(colorDfPrev);
                    dsBean.setName("% Decline Free Prev");
                    listDsBean.add(dsBean);
                }

                c3ChartBeanAcqIss = new C3ChartBean();
                generateChartMultiData(c3ChartBeanAcqIss, listDsBean, listDateStringAcqIss, title);



                //acq bnf
                listDsBean = new ArrayList<>();
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprAcqBnf);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCAcqBnf);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFAcqBnf);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                if (activePercentPrev[0]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentApprAcqBnfPrev);
                    dsBean.setColor(colorApprovePrev);
                    dsBean.setName("% Approval Prev");
                    listDsBean.add(dsBean);
                }

                if (activePercentPrev[1]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentDeclCAcqBnfPrev);
                    dsBean.setColor(colorDcPrev);
                    dsBean.setName("% Decline Charge Prev");
                    listDsBean.add(dsBean);
                }

                if (activePercentPrev[2]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentDeclFAcqBnfPrev);
                    dsBean.setColor(colorDfPrev);
                    dsBean.setName("% Decline Free Prev");
                    listDsBean.add(dsBean);
                }

                c3ChartBeanAcqBnf = new C3ChartBean();
                generateChartMultiData(c3ChartBeanAcqBnf, listDsBean, listDateStringAcqBnf, title);


                //iss bnf
                listDsBean = new ArrayList<>();
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprIssBnf);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCIssBnf);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFIssBnf);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                if (activePercentPrev[0]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentApprIssBnfPrev);
                    dsBean.setColor(colorApprovePrev);
                    dsBean.setName("% Approval Prev");
                    listDsBean.add(dsBean);
                }

                if (activePercentPrev[1]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentDeclCIssBnfPrev);
                    dsBean.setColor(colorDcPrev);
                    dsBean.setName("% Decline Charge Prev");
                    listDsBean.add(dsBean);
                }

                if (activePercentPrev[2]) {
                    dsBean = new DsBean();
                    dsBean.setListData(listDataPercentDeclFIssBnfPrev);
                    dsBean.setColor(colorDfPrev);
                    dsBean.setName("% Decline Free Prev");
                    listDsBean.add(dsBean);
                }

                c3ChartBeanIssBnf = new C3ChartBean();
                generateChartMultiData(c3ChartBeanIssBnf, listDsBean, listDateStringIssBnf, title);


            }



            render = "true";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String showPercentChangeListener() {
        /*showPercent = !showPercent;
        if (showPercent) {
            showPercentButtonText = "Show Total";
        } else {
            showPercentButtonText = "Show Percent";
        }*/
        //disableAutoRefreshChangeListener();
        //poolAction();
        return "transactionIndicatorMonitoringHourShowPrevDay?faces-redirect=true&bid=" +
                StringUtils.checkNull(selectedBankId) + "&trxid=" + StringUtils.checkNull(selectedTrxId);
    }

    private List<EsqTrx5MinBean> listDataTrx(Date frdate, Date thdate, String bankId, String trxId, int prevDay ) throws Exception {
        List<EsqTrx5MinBean> list = new ArrayList<>();
        Date sd = frdate;
        //Date ed = thdate;
        int idx = 0;
        while (true) {
            //if (idx == 287) {
                //log.info(idx);
            //}
            String period = DateUtils.convertDate(sd, DateUtils.DATE_YYMMDD);
            String periodPrev = DateUtils.convertDate(DateUtils.addDate(Calendar.DAY_OF_MONTH, sd, -1 * prevDay) , DateUtils.DATE_YYMMDD);
            int ihourfr = DateUtils.getHour(sd);
            Date ed = DateUtils.addDate(Calendar.MINUTE, sd, 5);
            int ihourto = DateUtils.getHour(ed);
            //int iminfr= DateUtils.getMinute(sd);
            //int iminto = DateUtils.getMinute(ed);
            //if (ihourto == 0 && iminto == 0) {
            //    ihourto = 24;
            //}
            ihourto++;
            //String periodto = DateUtils.convertDate(ed, DateUtils.DATE_YYMMDD);
            //log.info(period + " " + ihourfr + ":" + ihourto );

            String shourfr = StringUtils.formatFixLength(Integer.toString(ihourfr), "0", 2) + ":00";
            String shourto = StringUtils.formatFixLength(Integer.toString(ihourto), "0", 2) + ":00";
            //String sminfr = StringUtils.formatFixLength(Integer.toString(ihourfr), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(iminfr), "0", 2);
            //String sminto = StringUtils.formatFixLength(Integer.toString(ihourfr), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(iminto), "0", 2);
            if (shourto.equalsIgnoreCase("00:00")) {
                shourto = "24:00";
            }
            EsqTrx5MinBean esqTrx5MinBean = moduleFactory.getBiService().getEsqTrxHourlyWithPrev(selectedBankId, selectedTrxId,
                    period, periodPrev, shourfr, shourto);
            if (esqTrx5MinBean == null) {
                esqTrx5MinBean = new EsqTrx5MinBean();
                esqTrx5MinBean.setPeriod(period);
                esqTrx5MinBean.setBankid(selectedBankId);
                esqTrx5MinBean.setTrxid(selectedTrxId);
                esqTrx5MinBean.setTimefr(shourfr);
                esqTrx5MinBean.setTimeto(shourto);

            }
            list.add(esqTrx5MinBean);

            if (ed.getTime() >= thdate.getTime()) {
                break;
            }

            sd = DateUtils.addDate(Calendar.HOUR_OF_DAY, sd, 1);
            idx++;
        };

        return list;
    }


    private void generateChart(List<Integer> listDataTotalTransaksi,
                               List<Integer> listDataTotalApprove,
                               List<Integer> listDataTotalDeclineCharge,
                               List<Integer> listDataTotalDeclineFree,
                               List<Integer> listDataTotalTransaksiPrev,
                               List<Integer> listDataTotalApprovePrev,
                               List<Integer> listDataTotalDeclineChargePrev,
                               List<Integer> listDataTotalDeclineFreePrev,
                               List<String> listDateString,
                               String colorTotal, String colorApprove,
                               String colorDc, String colorDf,
                               String colorTotalPrev, String colorApprovePrev,
                               String colorDcPrev, String colorDfPrev,
                               String title,
                               String dsNameTotal, String dsNameAppr, String dsNameDc, String dsNameDf,
                               C3ChartBean c3ChartBean) {
        C3DataSet dsTotal = new C3DataSet(listDataTotalTransaksi);
        C3ViewDataSet dataSetTotal = new C3ViewDataSet(dsNameTotal, dsTotal, "#"+colorTotal);

        C3DataSet dsAppr = new C3DataSet(listDataTotalApprove);
        C3ViewDataSet dataSetAppr = new C3ViewDataSet(dsNameAppr, dsAppr, "#"+colorApprove);

        C3DataSet dsDc = new C3DataSet(listDataTotalDeclineCharge);
        C3ViewDataSet dataSetDc = new C3ViewDataSet(dsNameDc, dsDc, "#"+colorDc);

        C3DataSet dsDf = new C3DataSet(listDataTotalDeclineFree);
        C3ViewDataSet dataSetDf = new C3ViewDataSet(dsNameDf, dsDf, "#"+colorDf);

        C3DataSet dsTotalPrev = new C3DataSet(listDataTotalTransaksiPrev);
        C3ViewDataSet dataSetTotalPrev = new C3ViewDataSet(dsNameTotal + " Prev", dsTotalPrev, "#"+colorTotalPrev);

        C3DataSet dsApprPrev = new C3DataSet(listDataTotalApprovePrev);
        C3ViewDataSet dataSetApprPrev = new C3ViewDataSet(dsNameAppr + " Prev", dsApprPrev, "#"+colorApprovePrev);

        C3DataSet dsDcPrev = new C3DataSet(listDataTotalDeclineChargePrev);
        C3ViewDataSet dataSetDcPrev = new C3ViewDataSet(dsNameDc + " Prev", dsDcPrev, "#"+colorDcPrev);

        C3DataSet dsDfPrev = new C3DataSet(listDataTotalDeclineFreePrev);
        C3ViewDataSet dataSetDfPrev = new C3ViewDataSet(dsNameDf + " Prev", dsDfPrev, "#"+colorDfPrev);

        C3DataSetString dsDateString = new C3DataSetString(listDateString);
        C3ViewDataSetString dataSetDateString = new C3ViewDataSetString("x", dsDateString, false);

        Data data = new Data();
        data.getDataSetsString().add(dataSetDateString);
        if (activeCurr[0]) {
            data.getDataSets().add(dataSetTotal);
        }
        if (activeCurr[1]) {
            data.getDataSets().add(dataSetAppr);
        }
        if (activeCurr[2]) {
            data.getDataSets().add(dataSetDc);
        }
        if (activeCurr[3]) {
            data.getDataSets().add(dataSetDf);
        }
        if (activePrev[0]) {
            data.getDataSets().add(dataSetTotalPrev);
        }
        if (activePrev[1]) {
            data.getDataSets().add(dataSetApprPrev);
        }
        if (activePrev[2]) {
            data.getDataSets().add(dataSetDcPrev);
        }
        if (activePrev[3]) {
            data.getDataSets().add(dataSetDfPrev);
        }

        data.setX("z1");
        data.setxFormat("%Y-%m-%d %H:%M");

        com.martinlinha.c3faces.script.property.Axis axis = new com.martinlinha.c3faces.script.property.Axis();
        axis.setHeightX(70);
        axis.setTypeX(com.martinlinha.c3faces.script.property.Axis.Type.TIMESERIES);
        axis.setTickX("fit: true,\n" +
                "format: '%d/%m %H:%M',\n" +
                "rotate: -45,\n" +
                "multiline: false,\n" +
                "culling: true");
        axis.setMinY(0);

        Point point = new Point();
        point.setShow(true);

        c3ChartBean.setData(data);
        c3ChartBean.setAxis(axis);
        c3ChartBean.setPoint(point);
        //c3ChartBean.setPadding(padding);
        c3ChartBean.setTitle(title);
    }

    public String showHistory() {
        return "transactionIndicatorMonitoringHourShowPrevDayHistory?faces-redirect=true&bid=" +
                StringUtils.checkNull(selectedBankId) + "&trxid=" + StringUtils.checkNull(selectedTrxId);
    }

    /*private void generateChart(LineChartModel dataReport, ChartSeries cs, String seriesColor) {
        dataReport.addSeries(cs);
        dataReport.setSeriesColors(seriesColor);
        dataReport.setStacked(false);
        dataReport.setShadow(false);
        dataReport.setAnimate(false);
        dataReport.setShowDatatip(false);
        dataReport.setMouseoverHighlight(true);
        dataReport.setLegendPosition("s");
        //dataReportYearCompare.setSeriesColors(seriesColor);
        dataReport.setExtender("extLegend");
        dataReport.setDatatipFormat("<div>%s</div><div>%s</div>");

        dataReport.getAxes().put(AxisType.X, new CategoryAxis("Hour"));
        Axis xAxis = dataReport.getAxis(AxisType.X);
        xAxis.setTickAngle(-45);
        //xAxis.setTickCount(12);

        Axis yAxis = dataReport.getAxis(AxisType.Y);
        yAxis.setLabel("Total");
        yAxis.setTickFormat("%'d");
        yAxis.setMin(0);
    }*/

    /*private void generateChart(LineChartModel dataReport, List<ChartSeries> listCs, String seriesColor, String title) {
        for(ChartSeries cs : listCs) {
            dataReport.addSeries(cs);
        }
        dataReport.setTitle(title);
        dataReport.setSeriesColors(seriesColor);
        dataReport.setStacked(false);
        dataReport.setShadow(false);
        dataReport.setAnimate(false);
        dataReport.setShowDatatip(false);
        dataReport.setMouseoverHighlight(true);
        dataReport.setLegendPosition("s");
        //dataReportYearCompare.setSeriesColors(seriesColor);
        dataReport.setExtender("extLegend");
        dataReport.setDatatipFormat("<div>%s</div><div>%s</div>");

        dataReport.getAxes().put(AxisType.X, new CategoryAxis("Hour"));
        Axis xAxis = dataReport.getAxis(AxisType.X);
        xAxis.setTickAngle(-45);
        //xAxis.setTickCount(12);

        Axis yAxis = dataReport.getAxis(AxisType.Y);
        yAxis.setLabel("Total");
        yAxis.setTickFormat("%'d");
        yAxis.setMin(0);
    }*/

    public void onPoolListener() {
        try {
            if (disableAutoRefresh) {
                submitAction();
                return;
            }

            submitAction();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void trxIdChangeListener() {
        try {
            if (selectedTrxId.equalsIgnoreCase(DataConstant.ALL)) {
                listTrxIdIndicator = new ArrayList<>();
                selectedTransactionIndicatorId = new String[6];
                selectedTransactionIndicatorId[0] = "01";
                selectedTransactionIndicatorId[1] = "02";
                selectedTransactionIndicatorId[2] = "03";
                selectedTransactionIndicatorId[3] = "04";
                selectedTransactionIndicatorId[4] = "05";
                selectedTransactionIndicatorId[5] = "06";
                return;
            } else {
                listTrxIdIndicator = moduleFactory.getMasterService().listEsqTrxIdTrxIndicator(selectedTrxId);
            }
            //listTrxIdIndicator = moduleFactory.getMasterService().listEsqTrxIdIndicator();

            selectedTransactionIndicatorId = new String[listTrxIdIndicator.size()];
            for(int i = 0; i < listTrxIdIndicator.size(); i++) {
                EsqTrxIdBean id = listTrxIdIndicator.get(i);
                selectedTransactionIndicatorId[i] = id.getTrxidindicator();
            }

            //selectedTransactionIndicatorId = null;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void generateChartMultiData(C3ChartBean c3ChartBean, List<DsBean> listData, List<String> listDateString,
                                        String title) {
        List<C3ViewDataSet> listDataSet = new ArrayList<>();

        for(DsBean dsBean : listData) {
            C3DataSet ds = new C3DataSet(dsBean.getListData());
            C3ViewDataSet dataSet = new C3ViewDataSet(dsBean.getName(), ds, "#"+dsBean.getColor());
            listDataSet.add(dataSet);
        }

        C3DataSetString dsDateString = new C3DataSetString(listDateString);
        C3ViewDataSetString dataSetDateString = new C3ViewDataSetString("x", dsDateString, false);

        Data data = new Data();
        data.getDataSetsString().add(dataSetDateString);
        for(C3ViewDataSet ds : listDataSet) {
            data.getDataSets().add(ds);
        }

        data.setX("z1");
        data.setxFormat("%Y-%m-%d %H:%M");

        com.martinlinha.c3faces.script.property.Axis axis = new com.martinlinha.c3faces.script.property.Axis();
        axis.setHeightX(70);
        axis.setTypeX(com.martinlinha.c3faces.script.property.Axis.Type.TIMESERIES);
        axis.setTickX("fit: true,\n" +
                "format: '%d/%m %H:%M',\n" +
                "rotate: -45,\n" +
                "multiline: false,\n" +
                "culling: true");
        axis.setMinY(0);

        Point point = new Point();
        point.setShow(true);

        //Padding padding = new Padding();

        c3ChartBean.setData(data);
        c3ChartBean.setAxis(axis);
        c3ChartBean.setPoint(point);
        //c3ChartBean.setPadding(padding);
        c3ChartBean.setTitle(title);
    }

    public void checkAllCurrListener() {
        for(int i=0; i < activeCurr.length; i++) {
            if (checkAllCurr) {
                activeCurr[i] = true;
            } else {
                activeCurr[i] = false;
            }
        }
    }

    public void checkAllPrevListener() {
        for(int i=0; i < activePrev.length; i++) {
            if (checkAllPrev) {
                activePrev[i] = true;
            } else {
                activePrev[i] = false;
            }
        }
    }

    public void checkAllPercentPrevListener() {
        for(int i=0; i < activePercentPrev.length; i++) {
            if (checkAllPercentPrev) {
                activePercentPrev[i] = true;
            } else {
                activePercentPrev[i] = false;
            }
        }
    }

    public void checkPrevListener() {
        if (activePercentPrev[0] || activePercentPrev[1] || activePercentPrev[2] ) {
            renderPrevDate = "";
        } else {
            renderPrevDate = "visibility: hidden;";
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<BankBean> getListBank() {
        return listBank;
    }

    public void setListBank(List<BankBean> listBank) {
        this.listBank = listBank;
    }

    public String getSelectedBankId() {
        return selectedBankId;
    }

    public void setSelectedBankId(String selectedBankId) {
        this.selectedBankId = selectedBankId;
    }

    public String getRender() {
        return render;
    }

    public void setRender(String render) {
        this.render = render;
    }

    public List<EsqTrx5MinBean>  getList() {
        return list;
    }

    public void setList(List<EsqTrx5MinBean>  list) {
        this.list = list;
    }

    public String getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(String maxDate) {
        this.maxDate = maxDate;
    }

    public List<EsqTrxIdBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<EsqTrxIdBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

    public String getSelectedTrxId() {
        return selectedTrxId;
    }

    public void setSelectedTrxId(String selectedTrxId) {
        this.selectedTrxId = selectedTrxId;
    }

    public List<EsqTrxIdBean> getListTrxIdIndicator() {
        return listTrxIdIndicator;
    }

    public void setListTrxIdIndicator(List<EsqTrxIdBean> listTrxIdIndicator) {
        this.listTrxIdIndicator = listTrxIdIndicator;
    }

    public String getSelectedBankName() {
        return selectedBankName;
    }

    public void setSelectedBankName(String selectedBankName) {
        this.selectedBankName = selectedBankName;
    }

    public String[] getSelectedTransactionIndicatorId() {
        return selectedTransactionIndicatorId;
    }

    public void setSelectedTransactionIndicatorId(String[] selectedTransactionIndicatorId) {
        this.selectedTransactionIndicatorId = selectedTransactionIndicatorId;
    }

    public String getSelectedTrxName() {
        return selectedTrxName;
    }

    public void setSelectedTrxName(String selectedTrxName) {
        this.selectedTrxName = selectedTrxName;
    }

    public String getSelectedTransactionIndicatorName() {
        return selectedTransactionIndicatorName;
    }

    public void setSelectedTransactionIndicatorName(String selectedTransactionIndicatorName) {
        this.selectedTransactionIndicatorName = selectedTransactionIndicatorName;
    }

    public String getRenderAcqOnly() {
        return renderAcqOnly;
    }

    public void setRenderAcqOnly(String renderAcqOnly) {
        this.renderAcqOnly = renderAcqOnly;
    }

    public String getRenderIssOnly() {
        return renderIssOnly;
    }

    public void setRenderIssOnly(String renderIssOnly) {
        this.renderIssOnly = renderIssOnly;
    }

    public String getRenderBnfOnly() {
        return renderBnfOnly;
    }

    public void setRenderBnfOnly(String renderBnfOnly) {
        this.renderBnfOnly = renderBnfOnly;
    }

    public String getRenderAcqIss() {
        return renderAcqIss;
    }

    public void setRenderAcqIss(String renderAcqIss) {
        this.renderAcqIss = renderAcqIss;
    }

    public String getRenderAcqBnf() {
        return renderAcqBnf;
    }

    public void setRenderAcqBnf(String renderAcqBnf) {
        this.renderAcqBnf = renderAcqBnf;
    }

    public String getRenderIssBnf() {
        return renderIssBnf;
    }

    public void setRenderIssBnf(String renderIssBnf) {
        this.renderIssBnf = renderIssBnf;
    }

    /*public LineChartModel getDataReportAcqOnly() {
        return dataReportAcqOnly;
    }

    public void setDataReportAcqOnly(LineChartModel dataReportAcqOnly) {
        this.dataReportAcqOnly = dataReportAcqOnly;
    }

    public LineChartModel getDataReportIssOnly() {
        return dataReportIssOnly;
    }

    public void setDataReportIssOnly(LineChartModel dataReportIssOnly) {
        this.dataReportIssOnly = dataReportIssOnly;
    }

    public LineChartModel getDataReportBnfOnly() {
        return dataReportBnfOnly;
    }

    public void setDataReportBnfOnly(LineChartModel dataReportBnfOnly) {
        this.dataReportBnfOnly = dataReportBnfOnly;
    }

    public LineChartModel getDataReportAcqIss() {
        return dataReportAcqIss;
    }

    public void setDataReportAcqIss(LineChartModel dataReportAcqIss) {
        this.dataReportAcqIss = dataReportAcqIss;
    }

    public LineChartModel getDataReportAcqBnf() {
        return dataReportAcqBnf;
    }

    public void setDataReportAcqBnf(LineChartModel dataReportAcqBnf) {
        this.dataReportAcqBnf = dataReportAcqBnf;
    }

    public LineChartModel getDataReportIssBnf() {
        return dataReportIssBnf;
    }

    public void setDataReportIssBnf(LineChartModel dataReportIssBnf) {
        this.dataReportIssBnf = dataReportIssBnf;
    }*/

    public int getDefaultRangeInHour() {
        return defaultRangeInHour;
    }

    public void setDefaultRangeInHour(int defaultRangeInHour) {
        this.defaultRangeInHour = defaultRangeInHour;
    }

    public Date getSelectedEndDate() {
        return selectedEndDate;
    }

    public void setSelectedEndDate(Date selectedEndDate) {
        this.selectedEndDate = selectedEndDate;
    }

    public Integer getSelectedEndHour() {
        return selectedEndHour;
    }

    public void setSelectedEndHour(Integer selectedEndHour) {
        this.selectedEndHour = selectedEndHour;
    }

    public Date getSelectedStartDate() {
        return selectedStartDate;
    }

    public void setSelectedStartDate(Date selectedStartDate) {
        this.selectedStartDate = selectedStartDate;
    }

    public Integer getSelectedStartHour() {
        return selectedStartHour;
    }

    public void setSelectedStartHour(Integer selectedStartHour) {
        this.selectedStartHour = selectedStartHour;
    }

    public String getGraphRefreshInterval() {
        return graphRefreshInterval;
    }

    public void setGraphRefreshInterval(String graphRefreshInterval) {
        this.graphRefreshInterval = graphRefreshInterval;
    }

    public boolean isShowTable() {
        return showTable;
    }

    public void setShowTable(boolean showTable) {
        this.showTable = showTable;
    }

    public boolean isDisableAutoRefresh() {
        return disableAutoRefresh;
    }

    public void setDisableAutoRefresh(boolean disableAutoRefresh) {
        this.disableAutoRefresh = disableAutoRefresh;
    }

    public String getDisableAutoRefreshString() {
        return disableAutoRefreshString;
    }

    public void setDisableAutoRefreshString(String disableAutoRefreshString) {
        this.disableAutoRefreshString = disableAutoRefreshString;
    }

    public Data getDataAcqOnly() {
        return dataAcqOnly;
    }

    public void setDataAcqOnly(Data dataAcqOnly) {
        this.dataAcqOnly = dataAcqOnly;
    }

    public com.martinlinha.c3faces.script.property.Axis getAxisAcqOnly() {
        return axisAcqOnly;
    }

    public void setAxisAcqOnly(com.martinlinha.c3faces.script.property.Axis axisAcqOnly) {
        this.axisAcqOnly = axisAcqOnly;
    }

    public Point getPointAcqOnly() {
        return pointAcqOnly;
    }

    public void setPointAcqOnly(Point pointAcqOnly) {
        this.pointAcqOnly = pointAcqOnly;
    }

    public Data getDataIssOnly() {
        return dataIssOnly;
    }

    public void setDataIssOnly(Data dataIssOnly) {
        this.dataIssOnly = dataIssOnly;
    }

    public com.martinlinha.c3faces.script.property.Axis getAxisIssOnly() {
        return axisIssOnly;
    }

    public void setAxisIssOnly(com.martinlinha.c3faces.script.property.Axis axisIssOnly) {
        this.axisIssOnly = axisIssOnly;
    }

    public Point getPointIssOnly() {
        return pointIssOnly;
    }

    public void setPointIssOnly(Point pointIssOnly) {
        this.pointIssOnly = pointIssOnly;
    }

    public Data getDataBnfOnly() {
        return dataBnfOnly;
    }

    public void setDataBnfOnly(Data dataBnfOnly) {
        this.dataBnfOnly = dataBnfOnly;
    }

    public com.martinlinha.c3faces.script.property.Axis getAxisBnfOnly() {
        return axisBnfOnly;
    }

    public void setAxisBnfOnly(com.martinlinha.c3faces.script.property.Axis axisBnfOnly) {
        this.axisBnfOnly = axisBnfOnly;
    }

    public Point getPointBnfOnly() {
        return pointBnfOnly;
    }

    public void setPointBnfOnly(Point pointBnfOnly) {
        this.pointBnfOnly = pointBnfOnly;
    }

    public Data getDataAcqIss() {
        return dataAcqIss;
    }

    public void setDataAcqIss(Data dataAcqIss) {
        this.dataAcqIss = dataAcqIss;
    }

    public com.martinlinha.c3faces.script.property.Axis getAxisAcqIss() {
        return axisAcqIss;
    }

    public void setAxisAcqIss(com.martinlinha.c3faces.script.property.Axis axisAcqIss) {
        this.axisAcqIss = axisAcqIss;
    }

    public Point getPointAcqIss() {
        return pointAcqIss;
    }

    public void setPointAcqIss(Point pointAcqIss) {
        this.pointAcqIss = pointAcqIss;
    }

    public Data getDataAcqBnf() {
        return dataAcqBnf;
    }

    public void setDataAcqBnf(Data dataAcqBnf) {
        this.dataAcqBnf = dataAcqBnf;
    }

    public com.martinlinha.c3faces.script.property.Axis getAxisAcqBnf() {
        return axisAcqBnf;
    }

    public void setAxisAcqBnf(com.martinlinha.c3faces.script.property.Axis axisAcqBnf) {
        this.axisAcqBnf = axisAcqBnf;
    }

    public Point getPointAcqBnf() {
        return pointAcqBnf;
    }

    public void setPointAcqBnf(Point pointAcqBnf) {
        this.pointAcqBnf = pointAcqBnf;
    }

    public Data getDataIssBnf() {
        return dataIssBnf;
    }

    public void setDataIssBnf(Data dataIssBnf) {
        this.dataIssBnf = dataIssBnf;
    }

    public com.martinlinha.c3faces.script.property.Axis getAxisIssBnf() {
        return axisIssBnf;
    }

    public void setAxisIssBnf(com.martinlinha.c3faces.script.property.Axis axisIssBnf) {
        this.axisIssBnf = axisIssBnf;
    }

    public Point getPointIssBnf() {
        return pointIssBnf;
    }

    public void setPointIssBnf(Point pointIssBnf) {
        this.pointIssBnf = pointIssBnf;
    }

    public C3ChartBean getC3ChartBeanAcqOnly() {
        return c3ChartBeanAcqOnly;
    }

    public void setC3ChartBeanAcqOnly(C3ChartBean c3ChartBeanAcqOnly) {
        this.c3ChartBeanAcqOnly = c3ChartBeanAcqOnly;
    }

    public C3ChartBean getC3ChartBeanIssOnly() {
        return c3ChartBeanIssOnly;
    }

    public void setC3ChartBeanIssOnly(C3ChartBean c3ChartBeanIssOnly) {
        this.c3ChartBeanIssOnly = c3ChartBeanIssOnly;
    }

    public C3ChartBean getC3ChartBeanBnfOnly() {
        return c3ChartBeanBnfOnly;
    }

    public void setC3ChartBeanBnfOnly(C3ChartBean c3ChartBeanBnfOnly) {
        this.c3ChartBeanBnfOnly = c3ChartBeanBnfOnly;
    }

    public C3ChartBean getC3ChartBeanAcqIss() {
        return c3ChartBeanAcqIss;
    }

    public void setC3ChartBeanAcqIss(C3ChartBean c3ChartBeanAcqIss) {
        this.c3ChartBeanAcqIss = c3ChartBeanAcqIss;
    }

    public C3ChartBean getC3ChartBeanAcqBnf() {
        return c3ChartBeanAcqBnf;
    }

    public void setC3ChartBeanAcqBnf(C3ChartBean c3ChartBeanAcqBnf) {
        this.c3ChartBeanAcqBnf = c3ChartBeanAcqBnf;
    }

    public C3ChartBean getC3ChartBeanIssBnf() {
        return c3ChartBeanIssBnf;
    }

    public void setC3ChartBeanIssBnf(C3ChartBean c3ChartBeanIssBnf) {
        this.c3ChartBeanIssBnf = c3ChartBeanIssBnf;
    }

    public String getRenderResult() {
        return renderResult;
    }

    public void setRenderResult(String renderResult) {
        this.renderResult = renderResult;
    }

    public boolean isShowPercent() {
        return showPercent;
    }

    public void setShowPercent(boolean showPercent) {
        this.showPercent = showPercent;
    }

    public String getShowPercentButtonText() {
        return showPercentButtonText;
    }

    public void setShowPercentButtonText(String showPercentButtonText) {
        this.showPercentButtonText = showPercentButtonText;
    }

    public Date getSelectedTodayDate() {
        return selectedTodayDate;
    }

    public void setSelectedTodayDate(Date selectedTodayDate) {
        this.selectedTodayDate = selectedTodayDate;
    }

    public Date getSelectedPrevDate() {
        return selectedPrevDate;
    }

    public void setSelectedPrevDate(Date selectedPrevDate) {
        this.selectedPrevDate = selectedPrevDate;
    }

    public Boolean[] getActiveCurr() {
        return activeCurr;
    }

    public void setActiveCurr(Boolean[] activeCurr) {
        this.activeCurr = activeCurr;
    }

    public Boolean[] getActivePrev() {
        return activePrev;
    }

    public void setActivePrev(Boolean[] activePrev) {
        this.activePrev = activePrev;
    }

    public boolean isCheckAllCurr() {
        return checkAllCurr;
    }

    public void setCheckAllCurr(boolean checkAllCurr) {
        this.checkAllCurr = checkAllCurr;
    }

    public boolean isCheckAllPrev() {
        return checkAllPrev;
    }

    public void setCheckAllPrev(boolean checkAllPrev) {
        this.checkAllPrev = checkAllPrev;
    }

    public Boolean[] getActivePercentPrev() {
        return activePercentPrev;
    }

    public void setActivePercentPrev(Boolean[] activePercentPrev) {
        this.activePercentPrev = activePercentPrev;
    }

    public boolean isCheckAllPercentPrev() {
        return checkAllPercentPrev;
    }

    public void setCheckAllPercentPrev(boolean checkAllPercentPrev) {
        this.checkAllPercentPrev = checkAllPercentPrev;
    }

    public String getRenderPrevDate() {
        return renderPrevDate;
    }

    public void setRenderPrevDate(String renderPrevDate) {
        this.renderPrevDate = renderPrevDate;
    }

    public class DsBean {
        private List<? extends Number> listData;
        private String color;
        private String name;

        public List<? extends Number> getListData() {
            return listData;
        }

        public void setListData(List<? extends Number> listData) {
            this.listData = listData;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}

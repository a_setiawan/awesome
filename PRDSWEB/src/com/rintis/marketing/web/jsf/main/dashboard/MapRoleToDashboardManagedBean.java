package com.rintis.marketing.web.jsf.main.dashboard;

import com.rintis.marketing.beans.bean.bi.DashboardRoleBean;
import com.rintis.marketing.beans.entity.MenuRoleType;
import com.rintis.marketing.beans.entity.MktUserDashboard;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class MapRoleToDashboardManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<MenuRoleType> listRole;
    private List<DashboardRoleBean> listDashboard;
    private Integer selectedRoleId;
    private String selectedRoleName;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listDashboard = new ArrayList<>();
            menuId = "0406";
            pageTitle = getMenuName(moduleFactory, menuId);

            listRole = moduleFactory.getMenuService().listMenuRoleType(DataConstant.MENU_ROLE_TYPE_NOT_HIDDEN);


            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String searchAction() {
        try {

            MenuRoleType menuRoleType = moduleFactory.getMenuService().getMenuRoleType(selectedRoleId);
            if (menuRoleType == null) {
                glowMessageError("Select Role Access", "");
                return "";
            }

            selectedRoleName = menuRoleType.getMenuRoleName();

            listDashboard = moduleFactory.getBiService().listDashboardByRole(selectedRoleId);
            for(DashboardRoleBean drb : listDashboard) {
                if (drb.getRoletypeid() == null) {
                    drb.setSelected(false);
                } else {
                    drb.setSelected(true);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String saveAction() {
        try {
            if (listDashboard.size() == 0) {
                return "";
            }

            moduleFactory.getBiService().updateDashboardRole(selectedRoleId, listDashboard);

            glowMessageInfo("Update Success", "");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            glowMessageError("Update Failed", "");
        }
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<MenuRoleType> getListRole() {
        return listRole;
    }

    public void setListRole(List<MenuRoleType> listRole) {
        this.listRole = listRole;
    }

    public List<DashboardRoleBean> getListDashboard() {
        return listDashboard;
    }

    public void setListDashboard(List<DashboardRoleBean> listDashboard) {
        this.listDashboard = listDashboard;
    }

    public Integer getSelectedRoleId() {
        return selectedRoleId;
    }

    public void setSelectedRoleId(Integer selectedRoleId) {
        this.selectedRoleId = selectedRoleId;
    }

    public String getSelectedRoleName() {
        return selectedRoleName;
    }

    public void setSelectedRoleName(String selectedRoleName) {
        this.selectedRoleName = selectedRoleName;
    }
}

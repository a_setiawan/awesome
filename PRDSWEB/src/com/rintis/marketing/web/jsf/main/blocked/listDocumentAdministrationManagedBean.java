package com.rintis.marketing.web.jsf.main.blocked;

import com.rintis.marketing.beans.bean.blocked.BlockedAdministrationHeader;
import com.rintis.marketing.beans.bean.blocked.PanHistoryBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
public class listDocumentAdministrationManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<BlockedAdministrationHeader> list;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "060103";
            pageTitle = getMenuName(moduleFactory, menuId);

            list = moduleFactory.getSequentialService().listBlockedAdministrationHeaderAll();

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String paramDecode(String data){
        //encrypt param
        if(!data.equals(null)){
            data = HttpUtils.decodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<BlockedAdministrationHeader> getList() {
        return list;
    }

    public void setList(List<BlockedAdministrationHeader> list) {
        this.list = list;
    }
}

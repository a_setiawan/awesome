package com.rintis.marketing.web.jsf.main.login;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.servlet.http.HttpServletRequest;

import com.rintis.marketing.beans.entity.UserLoginBank;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.MessagesResourceBundle;
import com.rintis.marketing.web.common.SessionConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.core.utils.StringUtils;

import com.rintis.marketing.beans.entity.UserLogin;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.common.ParamConstant;

@ManagedBean
@RequestScoped
public class LoginManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{"+SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY+"}")
    private ModuleFactory moduleFactory;
    
    private String userId;
    private String password;
    
    private String oldPassword;
    private String newPassword;
    private String reNewPassword;
    
    private String message;
    
    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String loginAction() {
        String url = "";

        //dummy query, utk reconnect connection jika koneksi mysql putus.
        try {
            //moduleFactory.getGeoService().findAllProvince("");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            try {
                Thread.sleep(5000);
                //log.info("after sleep");
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }

        if (userId.length() == 0) {
            message = "Enter UserId";
            return "";
        }
        if (password.length() == 0) {
            message = "Enter Password";
            return "";
        }

        if (userId.length() > 20) {
            userId = userId.substring(0, 20);
        }
        if (password.length() > 20) {
            password = password.substring(0, 20);
        }

        if (userId.indexOf("delete") > -1 || userId.indexOf("drop") > -1 || userId.indexOf("update") > -1 || userId.indexOf("select") > -1 ) {
            userId = "";
        }
        if (password.indexOf("delete") > -1 || password.indexOf("drop") > -1 || password.indexOf("update") > -1 || password.indexOf("select") > -1 ) {
            password = "";
        }

        UserInfoBean result = null;
        try {
            String ipAddress = ((HttpServletRequest)getFacesContext().getExternalContext().getRequest()).getRemoteAddr();  
            result = moduleFactory.getLoginService().login(userId, password, ipAddress);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        //log.info("result = "+result);
        
        if (result != null) {
            getSessionMap().put(SessionConstant.SESSION_USERINFO, DataConstant.Y);
            getSessionMap().put(SessionConstant.SESSION_USERINFOBEAN, result);
            UserLogin ul = getUserInfoBean().getUserLogin();
            UserLoginBank ulb = getUserInfoBean().getUserLoginBank();

            if (ul != null) {
                if (ul.getLastLogin() == null) {
                    if (ul.getValidationType().equalsIgnoreCase(DataConstant.USER_VALIDATION_TYPE_INTERNAL)) {
                        url = PageConstant.PAGE_FORCE_CHANGE_PASS + "?faces-redirect=true";
                    } else {
                        if (result.getUserLogin().getMenuRoleType().getMenuRoleName().equalsIgnoreCase(DataConstant.MENU_ROLE_NAME_BOD)) {
                            url = PageConstant.PAGE_WELCOME_BOD + "?faces-redirect=true";
                        } else {
                            url = PageConstant.PAGE_WELCOME + "?faces-redirect=true";
                        }
                    }
                } else {
                    //load menu yg tidak boleh diakses
                    try {
                    /*List<Menu> listMenu = moduleFactory.getMenuService().listMenuRoleNoAccess(userId);
                    StringBuffer sb = new StringBuffer("");
                    for(Menu menu : listMenu) {
                        sb.append(menu.getMenuUrl()).append(",");
                    }
                    //save ke session
                    getSessionMap().put(SessionConstant.SESSION_ACCESS_MENU, sb.toString());*/
                    } catch (Exception e) {
                        log.info(e.getMessage(), e);
                    }
                    //log.info(getUserInfoBean().getUserId());
                    //log.info(result.getUserLogin().getMenuRoleType().getMenuRoleName() );
                    if (result.getUserLogin().getMenuRoleType().getMenuRoleName().equalsIgnoreCase(DataConstant.MENU_ROLE_NAME_BOD)) {
                        url = PageConstant.PAGE_WELCOME_BOD + "?faces-redirect=true";
                    } else {
                        url = PageConstant.PAGE_WELCOME + "?faces-redirect=true";
                    }
                }

            } else if (ulb != null) {
                if (ulb.getLastLogin() == null) {
                    if (ulb.getValidationType().equalsIgnoreCase(DataConstant.USER_VALIDATION_TYPE_INTERNAL)) {
                        url = PageConstant.PAGE_FORCE_CHANGE_PASS + "?faces-redirect=true";
                    } else {
                        if (result.getUserLogin().getMenuRoleType().getMenuRoleName().equalsIgnoreCase(DataConstant.MENU_ROLE_NAME_BOD)) {
                            url = PageConstant.PAGE_WELCOME_BOD + "?faces-redirect=true";
                        } else {
                            url = PageConstant.PAGE_WELCOME + "?faces-redirect=true";
                        }
                    }
                } else {
                    //load menu yg tidak boleh diakses
                    try {
                    /*List<Menu> listMenu = moduleFactory.getMenuService().listMenuRoleNoAccess(userId);
                    StringBuffer sb = new StringBuffer("");
                    for(Menu menu : listMenu) {
                        sb.append(menu.getMenuUrl()).append(",");
                    }
                    //save ke session
                    getSessionMap().put(SessionConstant.SESSION_ACCESS_MENU, sb.toString());*/
                    } catch (Exception e) {
                        log.info(e.getMessage(), e);
                    }
                    //log.info(getUserInfoBean().getUserId());
                    //log.info(result.getUserLogin().getMenuRoleType().getMenuRoleName() );
                    if (result.getUserLoginBank().getMenuRoleType().getMenuRoleName().equalsIgnoreCase(DataConstant.MENU_ROLE_NAME_BOD)) {
                        url = PageConstant.PAGE_WELCOME_BOD + "?faces-redirect=true";
                    } else {
                        url = PageConstant.PAGE_WELCOME + "?faces-redirect=true";
                    }
                }
            }


        } else {
            try {
                UserLogin ul = moduleFactory.getLoginService().getUserLogin(userId);
                if (ul != null) {
                    message = "Invalid Password";
                } else {
                    UserLoginBank ulb = moduleFactory.getLoginService().getUserLoginBank(userId);
                    if (ulb != null) {
                        message = "Invalid Password";
                    } else {
                        message = "Invalid User Id";
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
            //addMessage(FacesMessage.SEVERITY_ERROR,
            //        MessagesResourceBundle.getMessage("login_failed"), "");
            //message = MessagesResourceBundle.getMessage("login_failed");
            userId = "";
            password = "";
            url = "";//PageConstant.PAGE_LOGIN + "?faces-redirect=true";
        }


        
        return url;
    }
    
    
    public String forceChangePassAction() {
        if (oldPassword.length() == 0) {
            glowMessageError("Enter Old Password" , "");
            return "";
        }
        if (newPassword.length() == 0) {
            glowMessageError("Enter New Password" , "");
            return "";
        }
        if (reNewPassword.length() == 0) {
            glowMessageError("Enter Re New Password" , "");
            return "";
        }
        if (newPassword.length() == 0 && reNewPassword.length() == 0 ) {
            glowMessageError("Enter New Password and Re New Password" , "");
            return "";
        }
        if (!newPassword.equals(reNewPassword)) {
            glowMessageError("New Password and Re New Password are not same" , "");
            return "";
        }


        String url = "";
        try {
            String userId = getUserInfoBean().getUserId(); //SecurityContextHolder.getContext().getAuthentication().getName();
            if (getUserInfoBean().getUserLogin() != null) {
                boolean result = moduleFactory.getLoginService().changePassword(userId, oldPassword, newPassword);
                if (result) {
                    getSessionMap().remove(SessionConstant.SESSION_FORCE_CHANGE_PASS);
                    if (getUserInfoBean().getUserLogin().getMenuRoleType().getMenuRoleName().equalsIgnoreCase(DataConstant.MENU_ROLE_NAME_BOD)) {
                        url = PageConstant.PAGE_WELCOME_BOD + "?faces-redirect=true";
                    } else {
                        url = PageConstant.PAGE_WELCOME + "?faces-redirect=true";
                    }
                } else {
                    String message = MessagesResourceBundle.getMessage("old_password_wrong");
                    getFacesContext().addMessage(ParamConstant.GLOW_MESSAGE, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", message));
                }
            } else if (getUserInfoBean().getUserLoginBank() != null) {
                boolean result = moduleFactory.getLoginService().changePasswordBank(userId, oldPassword, newPassword);
                if (result) {
                    getSessionMap().remove(SessionConstant.SESSION_FORCE_CHANGE_PASS);
                    if (getUserInfoBean().getUserLoginBank().getMenuRoleType().getMenuRoleName().equalsIgnoreCase(DataConstant.MENU_ROLE_NAME_BOD)) {
                        url = PageConstant.PAGE_WELCOME_BOD + "?faces-redirect=true";
                    } else {
                        url = PageConstant.PAGE_WELCOME + "?faces-redirect=true";
                    }
                } else {
                    String message = MessagesResourceBundle.getMessage("old_password_wrong");
                    getFacesContext().addMessage(ParamConstant.GLOW_MESSAGE, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", message));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return url;
    }

    public String changePassAction() {
        if (oldPassword.length() == 0) {
            glowMessageError(MessagesResourceBundle.getMessage("m01001") , "");
            return "";
        }
        if (newPassword.length() == 0) {
            glowMessageError(MessagesResourceBundle.getMessage("m01002") , "");
            return "";
        }
        if (newPassword.length() == 0 && reNewPassword.length() == 0 ) {
            glowMessageError(MessagesResourceBundle.getMessage("m01003") , "");
            return "";
        }
        if (!newPassword.equals(reNewPassword)) {
            glowMessageError(MessagesResourceBundle.getMessage("m01003") , "");
            return "";
        }


        String url = "";
        try {
            String userId = getUserInfoBean().getUserId(); //SecurityContextHolder.getContext().getAuthentication().getName();
            boolean result = false;
            UserLogin ul = moduleFactory.getLoginService().getUserLogin(userId);
            if (ul != null) {
                result = moduleFactory.getLoginService().changePassword(userId, oldPassword, newPassword);
            } else {
                UserLoginBank ulb = moduleFactory.getLoginService().getUserLoginBank(userId);
                if (ulb != null) {
                    result = moduleFactory.getLoginService().changePasswordBank(userId, oldPassword, newPassword);
                }
            }

            if (result) {
                url = "";
                glowMessageInfo(MessagesResourceBundle.getMessage("change_password_success"), "");
            } else {
                String message = MessagesResourceBundle.getMessage("old_password_wrong");
                glowMessageError(message, "");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return url;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = StringUtils.upperCaseNull(userId);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getReNewPassword() {
        return reNewPassword;
    }

    public void setReNewPassword(String reNewPassword) {
        this.reNewPassword = reNewPassword;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package com.rintis.marketing.web.jsf.main.groupbank;

import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.EisBank;
import com.rintis.marketing.beans.entity.TmsBankCaBiller;

import java.io.Serializable;

public class BankComparableBean implements Serializable, Comparable<BankComparableBean> {
    private BankBean eisBank;
    //private EisBank eisBank;
    public BankBean getEisBank() {
        return eisBank;
    }

    public void setEisBank(BankBean eisBank) {
        this.eisBank = eisBank;
    }

    @Override
    public int compareTo(BankComparableBean comparestu) {
        String compareage = ((BankComparableBean)comparestu).eisBank.getBankname();
        /* For Ascending order*/
        return this.eisBank.getBankname().compareTo(compareage);

        /* For Descending order do like this */
        //return compareage-this.studentage;
    }


}

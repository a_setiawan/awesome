package com.rintis.marketing.web.jsf.main.blocked;

import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.beans.bean.bi.SequentialAnomalyBean;
import com.rintis.marketing.beans.bean.blocked.BlockedAdministrationDetail;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import org.hibernate.SessionFactory;
import org.primefaces.model.SelectableDataModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.ListDataModel;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@ManagedBean
@ViewScoped
public class unblockedAdministrationManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private SessionFactory sessionFactory;
    private List<SequentialAnomalyBean> listUnblocked;
    private SequentialAnomalyBean[] selectedItems;
    private ListSequentialDataModel listSequentialDataModel;

    private String transaction;

    public unblockedAdministrationManagedBean() {}

    @Override
    @PostConstruct
    protected void initManagedBean() {

        try {
            menuId = "060102";
            pageTitle = getMenuName(moduleFactory, menuId);

            listUnblocked = moduleFactory.getSequentialService().listUnblockedDetail();
            listSequentialDataModel = new ListSequentialDataModel(listUnblocked);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String saveItemAction() {

        try {

            String updt = "false";
            String doc_id, userId;
            Date date, createDate, statusDate;
            if (selectedItems.length == 0) return "";
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat datefmt = new SimpleDateFormat("yyyyMMdd");
            date = calendar.getTime();
            userId = getUserInfoBean().getUserId();
            doc_id = moduleFactory.getSequentialService().getDocId(datefmt.format(date), "U");

            for(SequentialAnomalyBean list : selectedItems) {
                moduleFactory.getSequentialService().saveBlockedPanDetail(doc_id, list.getPan(), list.getBankid(), "PGW", "");
                moduleFactory.getSequentialService().updatePan(list.getPan(), "Unblocked Selected");
                updt = "true";
            }

            if(updt.equals("true")){
                moduleFactory.getSequentialService().saveBlockedPanHeader(doc_id, date, userId, date, "Created", "");
                return PageConstant.PAGE_UNBLOCKED_ADMINISTRATION_DETAIL + "?faces-redirect=true" + "&pDocId=" + paramDecode(doc_id);
            }

            selectedItems = null;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String paramDecode(String data){
        //encrypt param
        if(!data.equals(null)){
            data = HttpUtils.decodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<SequentialAnomalyBean> getListUnblocked() {
        return listUnblocked;
    }

    public void setListUnblocked(List<SequentialAnomalyBean> listUnblocked) {
        this.listUnblocked = listUnblocked;
    }

    public SequentialAnomalyBean[] getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(SequentialAnomalyBean[] selectedItems) {
        this.selectedItems = selectedItems;
    }
    public class ListSequentialDataModel extends ListDataModel<SequentialAnomalyBean> implements SelectableDataModel<SequentialAnomalyBean> {

        public ListSequentialDataModel(List<SequentialAnomalyBean> listUnblocked) {
            super(listUnblocked);
        }

        @Override
        public SequentialAnomalyBean getRowData(String rowKey) {
            List<SequentialAnomalyBean> listUnblocked = (List<SequentialAnomalyBean>)getWrappedData();
            for(SequentialAnomalyBean list : listUnblocked) {
                String pp = list.getPan().toString() + list.getBankid().toString();
                if (pp.equals(rowKey) ) {
                    return list;
                }
            }
            return null;
        }

        @Override
        public Object getRowKey(SequentialAnomalyBean listUnblocked) {
            return listUnblocked.getPan() + listUnblocked.getBankid().toString();
        }

    }

    public ListSequentialDataModel getListSequentialDataModel() {
        return listSequentialDataModel;
    }

    public void setListSequentialDataModel(ListSequentialDataModel listSequentialDataModel) {
        this.listSequentialDataModel = listSequentialDataModel;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }
}

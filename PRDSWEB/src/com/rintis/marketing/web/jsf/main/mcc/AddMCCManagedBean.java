package com.rintis.marketing.web.jsf.main.mcc;

import com.rintis.marketing.beans.bean.bi.PanAnomalyEmailBean;
import com.rintis.marketing.beans.bean.master.MCCBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 * Created by aabraham on 19/04/2021.
 */
@ManagedBean
@ViewScoped
public class AddMCCManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private MCCBean mccBean;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        mccBean = new MCCBean();
    }

    public String saveAction() {
        try {
            if (mccBean.getMcc().length() == 0) {
                glowMessageError("Enter MCC", "");
                return "";
            }

            if (mccBean.getDescription().length() == 0) {
                glowMessageError("Enter Description", "");
                return "";
            }

            boolean chk = moduleFactory.getMasterService().checkMccUsed(mccBean.getMcc());
            System.out.println(chk);
            System.out.println(mccBean.getMcc());
            if (chk) {
                glowMessageError("MCC already exist", "");
                return "";
            }

            String mcc = mccBean.getMcc().toString();
            String description = mccBean.getDescription().toString();

            moduleFactory.getMasterService().saveMcc(mcc, description);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_MASTER_MCC + "?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public MCCBean getMccBean() {
        return mccBean;
    }

    public void setMccBean(MCCBean mccBean) {
        this.mccBean = mccBean;
    }
}

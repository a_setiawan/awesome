package com.rintis.marketing.web.jsf.main.dashboard;

import com.martinlinha.c3faces.model.C3DataSet;
import com.martinlinha.c3faces.model.C3DataSetString;
import com.martinlinha.c3faces.model.C3ViewDataSet;
import com.martinlinha.c3faces.model.C3ViewDataSetString;
import com.martinlinha.c3faces.script.property.Data;
import com.martinlinha.c3faces.script.property.Point;
import com.rintis.marketing.beans.bean.bi.C3ChartBean;
import com.rintis.marketing.beans.bean.bi.EsqTrx5MinBean;
import com.rintis.marketing.beans.bean.bi.UserDashboardDetailBean;
import com.rintis.marketing.beans.entity.MktUserDashboard;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.MathUtils;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.*;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@ManagedBean
@ViewScoped
public class UserDashboardHistoryManagedBeanOld extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private MktUserDashboard mktUserDashboard;
    private List<UserDashboardDetailBean> listDetail;
    private BigDecimal id;

    private String maxDate;
    private int defaultRangeInHour5Min;
    private int defaultRangeInHourHourly;
    private Date selectedEndDate;
    private Integer selectedEndHour;
    private Integer selectedEndMinute;
    private Date selectedStartDate;
    private Integer selectedStartHour;
    private Integer selectedStartMinute;
    private String graphRefreshInterval;
    private boolean showTable;
    private boolean disableAutoRefresh;
    private String disableAutoRefreshString;
    private boolean showPercent;
    private String showPercentButtonText;

    //private List<LineChartModel> listLineChardModel;
    private List<C3ChartBean> listC3Chart;

    protected final int TIMEFRAME_5M = 5;
    protected final int TIMEFRAME_15M = 15;
    protected final int TIMEFRAME_30M = 30;
    protected final int TIMEFRAME_1H = 60;
    protected final int TIMEFRAME_1D = 1440;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            disableAutoRefresh = true;
            disableAutoRefreshString = "false";
            showTable = false;

            listC3Chart = new ArrayList<>();
            graphRefreshInterval = DataConstant.DEFAULT_GRAPH_REFRESH_INTERVAL;
            defaultRangeInHour5Min = 2;
            defaultRangeInHourHourly = Integer.parseInt(
                    moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(), DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_HOURLY)
            );
            showPercent = false;
            showPercentButtonText = "Show Percent";

            String sid = (String)getRequestParameterMap().get("id");
            if (sid == null) sid = "-1";
            id = new BigDecimal(sid);

            mktUserDashboard = moduleFactory.getBiService().getUserDashboard(id);
            listDetail = moduleFactory.getBiService().listUserDashboardDetailBean(id);

            //maxDate = DateUtils.convertDate(selectedEndDate, DateUtils.DATE_DDMMYYYY_HHMMSS);
            Date now = new Date();
            selectedEndDate = DateUtils.setDate(Calendar.HOUR_OF_DAY, now, DateUtils.getHour(now));
            selectedEndDate = DateUtils.setDate(Calendar.MINUTE, now, DateUtils.getMinute(now));
            selectedEndDate = DateUtils.addDate(Calendar.MINUTE, selectedEndDate, -5);
            maxDate = DateUtils.convertDate(selectedEndDate, DateUtils.DATE_DDMMYYYY_HHMMSS);
            Date thdate = selectedEndDate;
            //log.info(thdate);
            int thminute = DateUtils.getMinute(thdate) % 5;
            //log.info(thminute);
            thdate = DateUtils.addDate(Calendar.MINUTE, thdate, thminute*-1);
            thdate = DateUtils.setDate(Calendar.SECOND, thdate, 0);
            selectedEndHour = DateUtils.getHour(thdate);
            selectedEndMinute = DateUtils.getMinute(thdate);

            Date frdate = DateUtils.addDate(Calendar.HOUR_OF_DAY, thdate, defaultRangeInHour5Min*-1);
            //log.info(frdate);
            int frminute = DateUtils.getMinute(frdate) % 5;
            //log.info(frminute);
            frdate = DateUtils.addDate(Calendar.MINUTE, frdate, frminute*-1);
            frdate = DateUtils.setDate(Calendar.SECOND, frdate, 0);
            selectedStartDate = frdate;
            selectedStartHour = DateUtils.getHour(frdate);
            selectedStartMinute = DateUtils.getMinute(frdate);

            poolAction();

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String searchAction() {
        poolAction();
        return "";
    }

    public String onPoolAction() {
        poolAction();
        return "";
    }

    public String showCurrentAction() {
        return "userDashboard?faces-redirect=true&id=" + id;
    }

    public void poolAction() {
        try {
            if (showPercent) {
                showPercentButtonText = "Show Total";
            } else {
                showPercentButtonText = "Show Percent";
            }


            //String sid = (String)getRequestParameterMap().get("id");
            //id = new BigDecimal(sid);
            listDetail = moduleFactory.getBiService().listUserDashboardDetailBean(id);

            listC3Chart = new ArrayList<>();
            if (!disableAutoRefresh) {
                //maxDate = DateUtils.convertDate(selectedEndDate, DateUtils.DATE_DDMMYYYY_HHMMSS);
                Date now = new Date();
                selectedEndDate = DateUtils.setDate(Calendar.HOUR_OF_DAY, now, DateUtils.getHour(now));
                selectedEndDate = DateUtils.setDate(Calendar.MINUTE, now, DateUtils.getMinute(now));
                selectedEndDate = DateUtils.addDate(Calendar.MINUTE, selectedEndDate, -5);
                maxDate = DateUtils.convertDate(selectedEndDate, DateUtils.DATE_DDMMYYYY_HHMMSS);
                Date thdate = selectedEndDate;
                //log.info(thdate);
                int thminute = DateUtils.getMinute(thdate) % 5;
                //log.info(thminute);
                thdate = DateUtils.addDate(Calendar.MINUTE, thdate, thminute * -1);
                thdate = DateUtils.setDate(Calendar.SECOND, thdate, 0);
                selectedEndHour = DateUtils.getHour(thdate);
                selectedEndMinute = DateUtils.getMinute(thdate);

                Date frdate = DateUtils.addDate(Calendar.HOUR_OF_DAY, thdate, defaultRangeInHour5Min * -1);
                //log.info(frdate);
                int frminute = DateUtils.getMinute(frdate) % 5;
                //log.info(frminute);
                frdate = DateUtils.addDate(Calendar.MINUTE, frdate, frminute * -1);
                frdate = DateUtils.setDate(Calendar.SECOND, frdate, 0);
                selectedStartDate = frdate;
                selectedStartHour = DateUtils.getHour(frdate);
                selectedStartMinute = DateUtils.getMinute(frdate);
            }

            //listLineChardModel = new ArrayList<>();

            for(UserDashboardDetailBean detail : listDetail) {
                if (detail.getMonitoringinterval().equalsIgnoreCase(DataConstant.MONITORING_INTERVAL_5MIN) ) {
                    renderChart5Min(detail);
                } else if (detail.getMonitoringinterval().equalsIgnoreCase(DataConstant.MONITORING_INTERVAL_HOURLY) ) {
                    renderChartHourly(detail);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
	
	public void itemSelect(ItemSelectEvent event) {
        //FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item selected",
        //                "Item Index: " + event.getItemIndex() + ", Series Index:" + event.getSeriesIndex());
        //log.info("Item Index: " + event.getItemIndex() + ", Series Index:" + event.getSeriesIndex());
    }

    public void itemClick() {

    }

    public void disableAutoRefreshChangeListener() {
        if (disableAutoRefresh) {
            disableAutoRefreshString = "false";
        } else {
            disableAutoRefreshString = "true";
        }
    }

    public String showPercentChangeListener() {
        showPercent = !showPercent;
        if (showPercent) {
            showPercentButtonText = "Show Total";
        } else {
            showPercentButtonText = "Show Percent";
        }
        disableAutoRefreshChangeListener();
        poolAction();
        return "";
    }

    private void renderChart5Min(UserDashboardDetailBean detail) throws Exception {
        int timeframe = 0;
        String selectedBankId = detail.getBankid();
        String selectedTrxId = detail.getTransactionid();
        String selectedTransactionIndicatorId = detail.getTransactionindicatorid();

        //EisBank bank = moduleFactory.getMasterService().getBank(selectedBankId);
        String selectedBankName = detail.getBankname();
        //EsqTrxIdBean trx = moduleFactory.getMasterService().getEsqTrxId(selectedTrxId);
        String selectedTransactionName = detail.getTransactionname();
        if (selectedTransactionName == null) {
            selectedTransactionName = "All";
        }
        //EsqTrxIdBean trxIndicator = moduleFactory.getMasterService().getEsqTrxIdIndicator(selectedTransactionIndicatorId);
        String selectedTransactionIndicatorName = detail.getTransactionindicatorname();

        //String seriesColor = "";

        //ChartSeries cs = new ChartSeries();
        //ChartSeries csPctAppr = new ChartSeries();
        //ChartSeries csPsDecl = new ChartSeries();

        Data data = new Data();
        List<String> listDateString = new ArrayList<>();
        List<Integer> listDataTotalTransaksi = new ArrayList<>();
        List<Integer> listData = new ArrayList<>();
        List<Integer> listDataTotalApprove = new ArrayList<>();
        List<Integer> listDataTotalDeclineCharge = new ArrayList<>();
        List<Integer> listDataTotalDeclineFree = new ArrayList<>();
        List<BigDecimal> listDataPercentAppr = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclC = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclF = new ArrayList<>();








        Date thdate = selectedEndDate;
        thdate = DateUtils.setDate(Calendar.HOUR_OF_DAY, thdate, selectedEndHour);
        thdate = DateUtils.setDate(Calendar.MINUTE, thdate, selectedEndMinute);

        Date frdate = selectedStartDate;
        frdate = DateUtils.setDate(Calendar.HOUR_OF_DAY, frdate, selectedStartHour);
        frdate = DateUtils.setDate(Calendar.MINUTE, frdate, selectedStartMinute);
        frdate = DateUtils.addDate(Calendar.MINUTE, frdate, -5);


        List<EsqTrx5MinBean> list = new ArrayList<>();
        Date sd = frdate;
        //int idx = 0;
        while (true) {
            //if (idx == 287) {
            //    log.info(idx);
            //}
            String period = DateUtils.convertDate(sd, DateUtils.DATE_YYMMDD);
            int ihourfr = DateUtils.getHour(sd);
            Date ed = DateUtils.addDate(Calendar.MINUTE, sd, 5);
            int ihourto = DateUtils.getHour(ed);
            int iminfr= DateUtils.getMinute(sd);
            int iminto = DateUtils.getMinute(ed);
            //if (ihourto == 0 && iminto == 0) {
            //    ihourto = 24;
            //}
            //ihourto++;
            int ihourtominto = ihourto;
            if (iminto == 0 && iminfr < 55) {
                ihourtominto++;
            }
            String periodto = DateUtils.convertDate(ed, DateUtils.DATE_YYMMDD);
            //log.info(period + " " + ihourfr + ":" + ihourto + "  -  " + " " + iminfr + ":" + iminto);

            String shourfr = StringUtils.formatFixLength(Integer.toString(ihourfr), "0", 2) + ":00";
            String shourto = StringUtils.formatFixLength(Integer.toString(ihourfr+1), "0", 2) + ":00";
            String sminfr = StringUtils.formatFixLength(Integer.toString(ihourfr), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(iminfr), "0", 2);
            String sminto = StringUtils.formatFixLength(Integer.toString(ihourtominto), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(iminto), "0", 2);
            if (sminfr.equalsIgnoreCase("23:55")) {
                sminto = "24:00";
            }
            EsqTrx5MinBean esqTrx5MinBean = moduleFactory.getBiService().getEsqTrx5Min(selectedBankId, selectedTrxId,
                    period, shourfr, shourto, sminfr, sminto);
            if (esqTrx5MinBean == null) {
                esqTrx5MinBean = new EsqTrx5MinBean();
                esqTrx5MinBean.setPeriod(period);
                esqTrx5MinBean.setBankid(selectedBankId);
                esqTrx5MinBean.setTrxid(selectedTrxId);
                esqTrx5MinBean.setTimefr(shourfr);
                esqTrx5MinBean.setTimeto(shourto);
                esqTrx5MinBean.setTimefr(sminfr);
                esqTrx5MinBean.setTimeto(sminto);

            }
            list.add(esqTrx5MinBean);

            if (ed.getTime() >= thdate.getTime()) {
                break;
            }

            sd = DateUtils.addDate(Calendar.MINUTE, sd, 5);
            //idx++;
        };



        boolean toggle = true;
        for(EsqTrx5MinBean esq : list) {
            Date d = DateUtils.convertDate(esq.getPeriod() + " " + esq.getTimeto(), DateUtils.DATE_YYMMDD_HHMM);
            listDateString.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ONLY)) {
                //listAcqOnly.add(esq);
                BigDecimal b = MathUtils.checkNull(esq.getFreq_acq_app())
                        .add(MathUtils.checkNull(esq.getFreq_acq_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_acq_df()));
                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acq_app()), b);
                //BigDecimal td = MathUtils.checkNull(esq.getFreq_acq_df()).add(MathUtils.checkNull(esq.getFreq_acq_df()));
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acq_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acq_df(), b);
                if (toggle) {
                    //cs.set(esq.getPeriod() + " " + esq.getTimefr(), b);
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_acq_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_acq_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_acq_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));
                } else {
                    //cs.set(" ", b);
                    listDataTotalTransaksi.add(0);
                }
            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_ONLY)) {
                //listIssOnly.add(esq);
                BigDecimal b = MathUtils.checkNull(esq.getFreq_iss_app())
                        .add(MathUtils.checkNull(esq.getFreq_iss_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_iss_df()));
                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_iss_app()), b);
                //BigDecimal td = MathUtils.checkNull(esq.getFreq_iss_df()).add(MathUtils.checkNull(esq.getFreq_iss_df()));
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_iss_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_iss_df(), b);
                if (toggle) {
                    //cs.set(esq.getPeriod() + " " + esq.getTimefr(), b);
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_iss_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_iss_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_iss_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));
                } else {
                    //cs.set(" ", b);
                    listDataTotalTransaksi.add(0);
                }

            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__BNF_ONLY)) {
                //listBnfOnly.add(esq);
                BigDecimal b = MathUtils.checkNull(esq.getFreq_bnf_app())
                        .add(MathUtils.checkNull(esq.getFreq_bnf_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_bnf_df()));
                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_bnf_app()), b);
                //BigDecimal td = MathUtils.checkNull(esq.getFreq_bnf_df()).add(MathUtils.checkNull(esq.getFreq_bnf_df()));
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_bnf_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_bnf_df(), b);
                if (toggle) {
                    //cs.set(esq.getPeriod() + " " + esq.getTimefr(), b);
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_bnf_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_bnf_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_bnf_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));
                } else {
                    //cs.set(" ", b);
                    listDataTotalTransaksi.add(0);
                }

            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ISS)) {
                //listAcqIss.add(esq);
                BigDecimal b = MathUtils.checkNull(esq.getFreq_acqIss_app())
                        .add(MathUtils.checkNull(esq.getFreq_acqIss_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_acqIss_df()));
                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqIss_app()), b);
                //BigDecimal td = MathUtils.checkNull(esq.getFreq_acqIss_df()).add(MathUtils.checkNull(esq.getFreq_acqIss_df()));
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acqIss_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acqIss_df(), b);
                if (toggle) {
                    //cs.set(esq.getPeriod() + " " + esq.getTimefr(), b);
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_acqIss_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_acqIss_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_acqIss_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));
                } else {
                    //cs.set(" ", b);
                    listDataTotalTransaksi.add(0);
                }

            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_BNF)) {
                //listAcqBnf.add(esq);
                BigDecimal b = MathUtils.checkNull(esq.getFreq_acqBnf_app())
                        .add(MathUtils.checkNull(esq.getFreq_acqBnf_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_acqBnf_df()));
                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqBnf_app()), b);
                //BigDecimal td = MathUtils.checkNull(esq.getFreq_acqBnf_df()).add(MathUtils.checkNull(esq.getFreq_acqBnf_df()));
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_df(), b);
                if (toggle) {
                    //cs.set(esq.getPeriod() + " " + esq.getTimefr(), b);
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_acqBnf_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_acqBnf_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_acqBnf_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));
                } else {
                    //cs.set(" ", b);
                    listDataTotalTransaksi.add(0);
                }

            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_BNF)) {
                //listIssBnf.add(esq);
                BigDecimal b = MathUtils.checkNull(esq.getFreq_issBnf_app())
                        .add(MathUtils.checkNull(esq.getFreq_issBnf_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_issBnf_df()));
                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_issBnf_app()), b);
                //BigDecimal td = MathUtils.checkNull(esq.getFreq_issBnf_df()).add(MathUtils.checkNull(esq.getFreq_issBnf_df()));
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_issBnf_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_issBnf_df(), b);
                if (toggle) {
                    //cs.set(esq.getPeriod() + " " + esq.getTimefr(), b);
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_issBnf_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_issBnf_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_issBnf_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));
                } else {
                    //cs.set(" ", b);
                    listDataTotalTransaksi.add(0);
                }

            }
        }

        //LineChartModel dataReport = new LineChartModel();
        String title = selectedBankName + " - " + selectedTransactionName + " - " + selectedTransactionIndicatorName + " - 5 Min";
        //generateChart(title, dataReport, cs, seriesColor);

        //listLineChardModel.add(dataReport);


        String colorTotal = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_TOTAL);
        String colorApprove = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_APPR);
        String colorDc = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_DC);
        String colorDf = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_DF);
        //String colorPctAppr = "0000F0";
        //String colorPctDecl = "00FF0F";

        if (!showPercent) {
            generateChart(listDataTotalTransaksi,
                    listDataTotalApprove,
                    listDataTotalDeclineCharge,
                    listDataTotalDeclineFree,
                    listDateString,
                    colorTotal, colorApprove, colorDc, colorDf,
                    title,
                    DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                    DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                    timeframe);
        } else {
            List<DsBean> listDsBean = new ArrayList<>();

            DsBean dsBean = new DsBean();
            dsBean.setListData(listDataPercentAppr);
            dsBean.setColor(colorApprove);
            dsBean.setName("% Approval");
            listDsBean.add(dsBean);

            dsBean = new DsBean();
            dsBean.setListData(listDataPercentDeclC);
            dsBean.setColor(colorDc);
            dsBean.setName("% Decline Charge ");
            listDsBean.add(dsBean);

            dsBean = new DsBean();
            dsBean.setListData(listDataPercentDeclF);
            dsBean.setColor(colorDf);
            dsBean.setName("% Decline Free ");
            listDsBean.add(dsBean);

            generateChartMultiData(listDsBean, listDateString, title, timeframe);
        }
    }

    private void renderChartHourly(UserDashboardDetailBean detail) throws Exception {
        int timeFrame = 0;
        String selectedBankId = detail.getBankid();
        String selectedTrxId = detail.getTransactionid();
        String selectedTransactionIndicatorId = detail.getTransactionindicatorid();

        //EisBank bank = moduleFactory.getMasterService().getBank(selectedBankId);
        String selectedBankName = detail.getBankname();
        //EsqTrxIdBean trx = moduleFactory.getMasterService().getEsqTrxId(selectedTrxId);
        String selectedTransactionName = detail.getTransactionname();
        if (selectedTransactionName == null) {
            selectedTransactionName = "All";
        }
        //EsqTrxIdBean trxIndicator = moduleFactory.getMasterService().getEsqTrxIdIndicator(selectedTransactionIndicatorId);
        String selectedTransactionIndicatorName = detail.getTransactionindicatorname();

        //String seriesColor = "";

        //ChartSeries cs = new ChartSeries();

        Data data = new Data();
        List<String> listDateString = new ArrayList<>();
        List<Integer> listDataTotalTransaksi = new ArrayList<>();
        List<Integer> listDataTotalApprove = new ArrayList<>();
        List<Integer> listDataTotalDeclineCharge = new ArrayList<>();
        List<Integer> listDataTotalDeclineFree = new ArrayList<>();
        List<BigDecimal> listDataPercentAppr = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclC = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclF = new ArrayList<>();








        Date thdate = selectedEndDate;
        thdate = DateUtils.setDate(Calendar.HOUR_OF_DAY, thdate, selectedEndHour);

        Date frdate = selectedStartDate;
        //frdate = DateUtils.setDate(Calendar.HOUR_OF_DAY, frdate, selectedStartHour);
        frdate = DateUtils.addDate(Calendar.HOUR_OF_DAY, thdate, defaultRangeInHourHourly*-1);



        List<EsqTrx5MinBean> list = new ArrayList<>();
        Date sd = frdate;
        int idx = 0;
        while (true) {
            if (idx == 287) {
                //log.info(idx);
            }
            String period = DateUtils.convertDate(sd, DateUtils.DATE_YYMMDD);
            int ihourfr = DateUtils.getHour(sd);
            Date ed = DateUtils.addDate(Calendar.MINUTE, sd, 5);
            int ihourto = DateUtils.getHour(ed);
            //int iminfr= DateUtils.getMinute(sd);
            //int iminto = DateUtils.getMinute(ed);
            //if (ihourto == 0 && iminto == 0) {
            //    ihourto = 24;
            //}
            ihourto++;
            String periodto = DateUtils.convertDate(ed, DateUtils.DATE_YYMMDD);
            //log.info(period + " " + ihourfr + ":" + ihourto );

            String shourfr = StringUtils.formatFixLength(Integer.toString(ihourfr), "0", 2) + ":00";
            String shourto = StringUtils.formatFixLength(Integer.toString(ihourto), "0", 2) + ":00";
            //String sminfr = StringUtils.formatFixLength(Integer.toString(ihourfr), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(iminfr), "0", 2);
            //String sminto = StringUtils.formatFixLength(Integer.toString(ihourfr), "0", 2) + ":" + StringUtils.formatFixLength(Integer.toString(iminto), "0", 2);
            if (shourto.equalsIgnoreCase("00:00")) {
                shourto = "24:00";
            }
            EsqTrx5MinBean esqTrx5MinBean = moduleFactory.getBiService().getEsqTrxHourly(selectedBankId, selectedTrxId,
                    period, shourfr, shourto);
            if (esqTrx5MinBean == null) {
                esqTrx5MinBean = new EsqTrx5MinBean();
                esqTrx5MinBean.setPeriod(period);
                esqTrx5MinBean.setBankid(selectedBankId);
                esqTrx5MinBean.setTrxid(selectedTrxId);
                esqTrx5MinBean.setTimefr(shourfr);
                esqTrx5MinBean.setTimeto(shourto);
            }
            list.add(esqTrx5MinBean);

            if (ed.getTime() >= thdate.getTime()) {
                break;
            }

            sd = DateUtils.addDate(Calendar.HOUR_OF_DAY, sd, 1);
            idx++;
        };



        boolean toggle = true;
        for(EsqTrx5MinBean esq : list) {
            Date d = DateUtils.convertDate(esq.getPeriod() + " " + esq.getTimeto(), DateUtils.DATE_YYMMDD_HHMM);
            listDateString.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ONLY)) {
                //listAcqOnly.add(esq);
                BigDecimal b = MathUtils.checkNull(esq.getFreq_acq_app())
                        .add(MathUtils.checkNull(esq.getFreq_acq_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_acq_df()));
                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acq_app()), b);
                //BigDecimal td = MathUtils.checkNull(esq.getFreq_acq_df()).add(MathUtils.checkNull(esq.getFreq_acq_df()));
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acq_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acq_df(), b);
                if (toggle) {
                    //cs.set(esq.getPeriod() + " " + esq.getTimefr(), b);
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_acq_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_acq_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_acq_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));
                } else {
                    //cs.set(" ", b);
                    listDataTotalTransaksi.add(0);
                }
            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_ONLY)) {
                //listIssOnly.add(esq);
                BigDecimal b = MathUtils.checkNull(esq.getFreq_iss_app())
                        .add(MathUtils.checkNull(esq.getFreq_iss_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_iss_df()));
                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_iss_app()), b);
                //BigDecimal td = MathUtils.checkNull(esq.getFreq_iss_df()).add(MathUtils.checkNull(esq.getFreq_iss_df()));
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_iss_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_iss_df(), b);
                if (toggle) {
                    //cs.set(esq.getPeriod() + " " + esq.getTimefr(), b);
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_iss_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_iss_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_iss_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));
                } else {
                    //cs.set(" ", b);
                    listDataTotalTransaksi.add(0);
                }

            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__BNF_ONLY)) {
                //listBnfOnly.add(esq);
                BigDecimal b = MathUtils.checkNull(esq.getFreq_bnf_app())
                        .add(MathUtils.checkNull(esq.getFreq_bnf_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_bnf_df()));
                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_bnf_app()), b);
                //BigDecimal td = MathUtils.checkNull(esq.getFreq_bnf_df()).add(MathUtils.checkNull(esq.getFreq_bnf_df()));
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_bnf_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_bnf_df(), b);
                if (toggle) {
                    //cs.set(esq.getPeriod() + " " + esq.getTimefr(), b);
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_bnf_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_bnf_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_bnf_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));
                } else {
                    //cs.set(" ", b);
                    listDataTotalTransaksi.add(0);
                }

            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ISS)) {
                //listAcqIss.add(esq);
                BigDecimal b = MathUtils.checkNull(esq.getFreq_acq_app())
                        .add(MathUtils.checkNull(esq.getFreq_acqIss_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_acqIss_df()));
                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqIss_app()), b);
                //BigDecimal td = MathUtils.checkNull(esq.getFreq_acqIss_df()).add(MathUtils.checkNull(esq.getFreq_acqIss_df()));
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acqIss_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acqIss_df(), b);
                if (toggle) {
                    //cs.set(esq.getPeriod() + " " + esq.getTimefr(), b);
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_acq_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_acqIss_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_acqIss_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));
                } else {
                    //cs.set(" ", b);
                    listDataTotalTransaksi.add(0);
                }

            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_BNF)) {
                //listAcqBnf.add(esq);
                BigDecimal b = MathUtils.checkNull(esq.getFreq_acqBnf_app())
                        .add(MathUtils.checkNull(esq.getFreq_acqBnf_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_acqBnf_df()));
                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqBnf_app()), b);
                //BigDecimal td = MathUtils.checkNull(esq.getFreq_acqBnf_df()).add(MathUtils.checkNull(esq.getFreq_acqBnf_df()));
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_df(), b);
                if (toggle) {
                    //cs.set(esq.getPeriod() + " " + esq.getTimefr(), b);
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_acqBnf_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_acqBnf_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_acqBnf_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));
                } else {
                    //cs.set(" ", b);
                    listDataTotalTransaksi.add(0);
                }

            }

            if (selectedTransactionIndicatorId.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_BNF)) {
                //listIssBnf.add(esq);
                BigDecimal b = MathUtils.checkNull(esq.getFreq_issBnf_app())
                        .add(MathUtils.checkNull(esq.getFreq_issBnf_dc()))
                        .add(MathUtils.checkNull(esq.getFreq_issBnf_df()));
                BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_issBnf_app()), b);
                //BigDecimal td = MathUtils.checkNull(esq.getFreq_issBnf_df()).add(MathUtils.checkNull(esq.getFreq_issBnf_df()));
                BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_issBnf_dc(), b);
                BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_issBnf_df(), b);
                if (toggle) {
                    //cs.set(esq.getPeriod() + " " + esq.getTimefr(), b);
                    listDataTotalTransaksi.add(b.intValue());
                    listDataTotalApprove.add(MathUtils.checkNull(esq.getFreq_issBnf_app()).intValue());
                    listDataTotalDeclineCharge.add(MathUtils.checkNull(esq.getFreq_issBnf_dc()).intValue());
                    listDataTotalDeclineFree.add(MathUtils.checkNull(esq.getFreq_issBnf_df()).intValue());
                    listDataPercentAppr.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclC.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclF.add(pctDeclF.multiply(new BigDecimal("100")));
                } else {
                    //cs.set(" ", b);
                    listDataTotalTransaksi.add(0);
                }

            }
        }



        //LineChartModel dataReport = new LineChartModel();
        String title = selectedBankName + " - " + selectedTransactionName + " - " + selectedTransactionIndicatorName + " - Hourly";
        //generateChart(title, dataReport, cs, seriesColor);

        //listLineChardModel.add(dataReport);



        String colorTotal = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_TOTAL);
        String colorApprove = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_APPR);
        String colorDc = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_DC);
        String colorDf = moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_DF);
        //String colorPctAppr = "0000FF";
        //String colorPctDecl = "00FFFF";

        if (!showPercent) {
            generateChart(listDataTotalTransaksi,
                    listDataTotalApprove,
                    listDataTotalDeclineCharge,
                    listDataTotalDeclineFree,
                    listDateString,
                    colorTotal, colorApprove, colorDc, colorDf,
                    title,
                    DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                    DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                    timeFrame);
        } else {
            List<DsBean> listDsBean = new ArrayList<>();

            DsBean dsBean = new DsBean();
            dsBean.setListData(listDataPercentAppr);
            dsBean.setColor(colorApprove);
            dsBean.setName("% Approval");
            listDsBean.add(dsBean);

            dsBean = new DsBean();
            dsBean.setListData(listDataPercentDeclC);
            dsBean.setColor(colorDc);
            dsBean.setName("% Decline Charge");
            listDsBean.add(dsBean);

            dsBean = new DsBean();
            dsBean.setListData(listDataPercentDeclF);
            dsBean.setColor(colorDf);
            dsBean.setName("% Decline Free");
            listDsBean.add(dsBean);

            generateChartMultiData(listDsBean, listDateString, title, timeFrame);
        }
    }

    private void generateChart(String title, LineChartModel dataReport, ChartSeries cs, String seriesColor) {
        dataReport.setTitle(title);
        dataReport.addSeries(cs);
        dataReport.setSeriesColors(seriesColor);
        dataReport.setStacked(false);
        dataReport.setShadow(false);
        dataReport.setAnimate(false);
        dataReport.setShowDatatip(false);
        dataReport.setMouseoverHighlight(true);
        dataReport.setLegendPosition("s");
        //dataReportYearCompare.setSeriesColors(seriesColor);
        dataReport.setExtender("extLegend");
        dataReport.setDatatipFormat("<div>%s</div><div>%s</div>");

        dataReport.getAxes().put(AxisType.X, new CategoryAxis("Hour"));
        Axis xAxis = dataReport.getAxis(AxisType.X);
        xAxis.setTickAngle(-45);
        //xAxis.setTickCount(12);

        Axis yAxis = dataReport.getAxis(AxisType.Y);
        yAxis.setLabel("Total");
        yAxis.setTickFormat("%'d");
        yAxis.setMin(0);
    }


    private void generateChart(List<Integer> listDataTotalTransaksi,
                               List<Integer> listDataTotalApprove,
                               List<Integer> listDataTotalDeclineCharge,
                               List<Integer> listDataTotalDeclineFree,
                               List<String> listDateString,
                               String colorTotal, String colorApprove,
            String colorDc, String colorDf, String title,
                               String dsNameTotal, String dsNameAppr, String dsNameDc, String dsNameDf,
                               int timeframe) {
        C3DataSet dsTotal = new C3DataSet(listDataTotalTransaksi);
        C3ViewDataSet dataSetTotal = new C3ViewDataSet(dsNameTotal, dsTotal, "#"+colorTotal);

        C3DataSet dsAppr = new C3DataSet(listDataTotalApprove);
        C3ViewDataSet dataSetAppr = new C3ViewDataSet(dsNameAppr, dsAppr, "#"+colorApprove);

        C3DataSet dsDc = new C3DataSet(listDataTotalDeclineCharge);
        C3ViewDataSet dataSetDc = new C3ViewDataSet(dsNameDc, dsDc, "#"+colorDc);

        C3DataSet dsDf = new C3DataSet(listDataTotalDeclineFree);
        C3ViewDataSet dataSetDf = new C3ViewDataSet(dsNameDf, dsDf, "#"+colorDf);

        C3DataSetString dsDateString = new C3DataSetString(listDateString);
        C3ViewDataSetString dataSetDateString = new C3ViewDataSetString("x", dsDateString, false);

        Data data = new Data();
        data.getDataSetsString().add(dataSetDateString);
        data.getDataSets().add(dataSetTotal);
        data.getDataSets().add(dataSetAppr);
        data.getDataSets().add(dataSetDc);
        data.getDataSets().add(dataSetDf);

        data.setX("z1");
        if (timeframe == TIMEFRAME_1D) {
            data.setxFormat("%Y-%m-%d");
        } else {
            data.setxFormat("%Y-%m-%d %H:%M");
        }

        com.martinlinha.c3faces.script.property.Axis axis = new com.martinlinha.c3faces.script.property.Axis();
        axis.setHeightX(70);
        axis.setTypeX(com.martinlinha.c3faces.script.property.Axis.Type.TIMESERIES);
        if (timeframe == TIMEFRAME_1D) {
            axis.setTickX("fit: true,\n" +
                    "format: '%d/%m',\n" +
                    "rotate: -45,\n" +
                    "multiline: false,\n" +
                    "culling: true");
        } else {
            axis.setTickX("fit: true,\n" +
                    "format: '%d/%m %H:%M',\n" +
                    "rotate: -45,\n" +
                    "multiline: false,\n" +
                    "culling: true");
        }
        axis.setMinY(0);

        Point point = new Point();
        point.setShow(true);

        //Padding padding = new Padding();

        C3ChartBean c3ChartBean = new C3ChartBean();
        c3ChartBean.setData(data);
        c3ChartBean.setAxis(axis);
        c3ChartBean.setPoint(point);
        //c3ChartBean.setPadding(padding);
        c3ChartBean.setTitle(title);
        listC3Chart.add(c3ChartBean);
    }

    private void generateChartMultiData(List<DsBean> listData, List<String> listDateString,
                                        String title, int timeframe) {
        List<C3ViewDataSet> listDataSet = new ArrayList<>();

        for(DsBean dsBean : listData) {
            C3DataSet ds = new C3DataSet(dsBean.getListData());
            C3ViewDataSet dataSet = new C3ViewDataSet(dsBean.getName(), ds, "#"+dsBean.getColor());
            listDataSet.add(dataSet);
        }

        C3DataSetString dsDateString = new C3DataSetString(listDateString);
        C3ViewDataSetString dataSetDateString = new C3ViewDataSetString("x", dsDateString, false);

        Data data = new Data();
        data.getDataSetsString().add(dataSetDateString);
        for(C3ViewDataSet ds : listDataSet) {
            data.getDataSets().add(ds);
        }

        data.setX("z1");
        data.setxFormat("%Y-%m-%d %H:%M");

        com.martinlinha.c3faces.script.property.Axis axis = new com.martinlinha.c3faces.script.property.Axis();
        axis.setHeightX(70);
        axis.setTypeX(com.martinlinha.c3faces.script.property.Axis.Type.TIMESERIES);
        if (timeframe == TIMEFRAME_1D) {
            axis.setTickX("fit: true,\n" +
                    "format: '%d/%m',\n" +
                    "rotate: -45,\n" +
                    "multiline: false,\n" +
                    "culling: true");
        } else {
            axis.setTickX("fit: true,\n" +
                    "format: '%d/%m %H:%M',\n" +
                    "rotate: -45,\n" +
                    "multiline: false,\n" +
                    "culling: true");
        }
        axis.setMinY(0);

        Point point = new Point();
        point.setShow(true);

        //Padding padding = new Padding();

        C3ChartBean c3ChartBean = new C3ChartBean();
        c3ChartBean.setData(data);
        c3ChartBean.setAxis(axis);
        c3ChartBean.setPoint(point);
        //c3ChartBean.setPadding(padding);
        c3ChartBean.setTitle(title);
        listC3Chart.add(c3ChartBean);
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public MktUserDashboard getMktUserDashboard() {
        return mktUserDashboard;
    }

    public void setMktUserDashboard(MktUserDashboard mktUserDashboard) {
        this.mktUserDashboard = mktUserDashboard;
    }

    public List<UserDashboardDetailBean> getListDetail() {
        return listDetail;
    }

    public void setListDetail(List<UserDashboardDetailBean> listDetail) {
        this.listDetail = listDetail;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(String maxDate) {
        this.maxDate = maxDate;
    }

    public int getDefaultRangeInHour5Min() {
        return defaultRangeInHour5Min;
    }

    public void setDefaultRangeInHour5Min(int defaultRangeInHour5Min) {
        this.defaultRangeInHour5Min = defaultRangeInHour5Min;
    }

    public int getDefaultRangeInHourHourly() {
        return defaultRangeInHourHourly;
    }

    public void setDefaultRangeInHourHourly(int defaultRangeInHourHourly) {
        this.defaultRangeInHourHourly = defaultRangeInHourHourly;
    }

    public Date getSelectedEndDate() {
        return selectedEndDate;
    }

    public void setSelectedEndDate(Date selectedEndDate) {
        this.selectedEndDate = selectedEndDate;
    }

    public Integer getSelectedEndHour() {
        return selectedEndHour;
    }

    public void setSelectedEndHour(Integer selectedEndHour) {
        this.selectedEndHour = selectedEndHour;
    }

    public Integer getSelectedEndMinute() {
        return selectedEndMinute;
    }

    public void setSelectedEndMinute(Integer selectedEndMinute) {
        this.selectedEndMinute = selectedEndMinute;
    }

    public Date getSelectedStartDate() {
        return selectedStartDate;
    }

    public void setSelectedStartDate(Date selectedStartDate) {
        this.selectedStartDate = selectedStartDate;
    }

    public Integer getSelectedStartHour() {
        return selectedStartHour;
    }

    public void setSelectedStartHour(Integer selectedStartHour) {
        this.selectedStartHour = selectedStartHour;
    }

    public Integer getSelectedStartMinute() {
        return selectedStartMinute;
    }

    public void setSelectedStartMinute(Integer selectedStartMinute) {
        this.selectedStartMinute = selectedStartMinute;
    }


    public String getGraphRefreshInterval() {
        return graphRefreshInterval;
    }

    public void setGraphRefreshInterval(String graphRefreshInterval) {
        this.graphRefreshInterval = graphRefreshInterval;
    }

    public List<C3ChartBean> getListC3Chart() {
        return listC3Chart;
    }

    public void setListC3Chart(List<C3ChartBean> listC3Chart) {
        this.listC3Chart = listC3Chart;
    }

    public boolean isDisableAutoRefresh() {
        return disableAutoRefresh;
    }

    public void setDisableAutoRefresh(boolean disableAutoRefresh) {
        this.disableAutoRefresh = disableAutoRefresh;
    }

    public boolean isShowPercent() {
        return showPercent;
    }

    public void setShowPercent(boolean showPercent) {
        this.showPercent = showPercent;
    }

    public String getDisableAutoRefreshString() {
        return disableAutoRefreshString;
    }

    public void setDisableAutoRefreshString(String disableAutoRefreshString) {
        this.disableAutoRefreshString = disableAutoRefreshString;
    }

    public String getShowPercentButtonText() {
        return showPercentButtonText;
    }

    public void setShowPercentButtonText(String showPercentButtonText) {
        this.showPercentButtonText = showPercentButtonText;
    }

    public boolean isShowTable() {
        return showTable;
    }

    public void setShowTable(boolean showTable) {
        this.showTable = showTable;
    }

    public class DsBean {
        private List<? extends Number> listData;
        private String color;
        private String name;

        public List<? extends Number> getListData() {
            return listData;
        }

        public void setListData(List<? extends Number> listData) {
            this.listData = listData;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}

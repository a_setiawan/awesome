package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.WhitelistBean;
import com.rintis.marketing.beans.bean.bi.WhitelistBeneficiaryBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.util.List;

/**
 * Created by aaw on 20/05/21.
 */
@ManagedBean
@ViewScoped
public class ListWhitelistBeneficiaryManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;

    private List<WhitelistBeneficiaryBean> listWhitelistBeneficiary;
    private WhitelistBeneficiaryBean selectedDeleteItem;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listWhitelistBeneficiary = moduleFactory.getPanService().listBeneficiary();
            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    public void deleteActionListener(ActionEvent event) {
        try {
            moduleFactory.getPanService().deleteBeneficiary(selectedDeleteItem.getAccount().toString(), selectedDeleteItem.getPenerima().toString());
            listWhitelistBeneficiary = moduleFactory.getPanService().listBeneficiary();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<WhitelistBeneficiaryBean> getListWhitelistBeneficiary() {
        return listWhitelistBeneficiary;
    }

    public void setListWhitelistBeneficiary(List<WhitelistBeneficiaryBean> listWhitelistBeneficiary) {
        this.listWhitelistBeneficiary = listWhitelistBeneficiary;
    }

    public WhitelistBeneficiaryBean getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(WhitelistBeneficiaryBean selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }
}

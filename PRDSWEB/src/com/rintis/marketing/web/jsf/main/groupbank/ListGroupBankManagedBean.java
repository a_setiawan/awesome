package com.rintis.marketing.web.jsf.main.groupbank;

import com.rintis.marketing.beans.entity.AlertThresholdGroup;
import com.rintis.marketing.beans.entity.GroupBank;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.util.List;

@ManagedBean
@ViewScoped
public class ListGroupBankManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<GroupBank> listGroupBank;
    private GroupBank selectedDeleteItem;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listGroupBank = moduleFactory.getMasterService().listGroupBank();


            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String deleteActionListener(ActionEvent event) {
        try {
            //check group is available
            List<AlertThresholdGroup> list = moduleFactory.getAlertService().listAlertThresholdDetail(selectedDeleteItem.getGroupId());
            if (list.size() > 0) {
                glowMessageError("Cannot delete Group Bank (used in alert threshold)", "");
                return "";
            }

            moduleFactory.getMasterService().deleteGroupBank(selectedDeleteItem.getGroupId());
            listGroupBank = moduleFactory.getMasterService().listGroupBank();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<GroupBank> getListGroupBank() {
        return listGroupBank;
    }

    public void setListGroupBank(List<GroupBank> listGroupBank) {
        this.listGroupBank = listGroupBank;
    }

    public GroupBank getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(GroupBank selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }
}

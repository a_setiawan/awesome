package com.rintis.marketing.web.jsf.main.dashboard;

import com.rintis.marketing.beans.entity.MktUserDashboard;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
public class ListUserDashboardManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<MktUserDashboard> listUserDashboard;
    private MktUserDashboard selectedDeleteItem;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0405";
            pageTitle = getMenuName(moduleFactory, menuId);

            listUserDashboard = moduleFactory.getBiService().listUserDashboard(null);


            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void deleteActionListener() {
        try {
            moduleFactory.getBiService().deleteUserDashboard(selectedDeleteItem.getId(), getUserInfoBean());

            listUserDashboard = moduleFactory.getBiService().listUserDashboard(null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<MktUserDashboard> getListUserDashboard() {
        return listUserDashboard;
    }

    public void setListUserDashboard(List<MktUserDashboard> listUserDashboard) {
        this.listUserDashboard = listUserDashboard;
    }

    public MktUserDashboard getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(MktUserDashboard selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }
}

package com.rintis.marketing.web.jsf.main.blocked;

import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.beans.bean.bi.SequentialAnomalyBean;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import org.hibernate.SessionFactory;
import org.primefaces.model.SelectableDataModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.ListDataModel;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by aabraham on 29/07/2021.
 */
@ManagedBean
@ViewScoped
public class blockedAdministrationManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private SessionFactory sessionFactory;
    private List<SequentialAnomalyBean> list;
    private SequentialAnomalyBean[] selectedItems;
    private ListSequentialDataModel listSequentialDataModel;
    private List<PanTrxidBean> listTrxId;
    private Date startDate;
    private Date endDate;
    private String periodFr;
    private String periodTo;
    private Boolean roleBank;
    private String transaction;

    public blockedAdministrationManagedBean() {}

    @Override
    @PostConstruct
    protected void initManagedBean() {

        try {
            menuId = "060101";
            pageTitle = getMenuName(moduleFactory, menuId);

            startDate = new Date();
            endDate = new Date();
            SimpleDateFormat datefmt = new SimpleDateFormat("yyMMdd");
            periodFr = datefmt.format(startDate);
            periodTo = datefmt.format(endDate);

            listTrxId = moduleFactory.getSequentialService().listTrxIdBlocked();

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            UserInfoBean userInfoBean = getUserInfoBean();
            String bank_id = "";
            if (userInfoBean.getUserLoginBank() != null) {
                bank_id = userInfoBean.getUserLoginBank().getBankId();
                roleBank = false;
            } else {
                roleBank = true;
            }

            SimpleDateFormat datefmt = new SimpleDateFormat("yyMMdd");
            periodFr = datefmt.format(startDate);
            periodTo = datefmt.format(endDate);
            long numOfDays = getDifferenceDays(startDate, endDate) + 1;

            if (numOfDays <= 90) {
                list = moduleFactory.getSequentialService().listSequentialAnomaly(periodFr, periodTo, transaction, bank_id);
                listSequentialDataModel = new ListSequentialDataModel(list);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return "";
    }

    public String errorMsg(){
        long numOfDays = getDifferenceDays(startDate, endDate) + 1;
        if (numOfDays > 90) {
            glowMessageError("Max 90 Days", "");
        }

        return "";
    }

    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public String saveItemAction() {

        try {

            String updt = "false";
            String doc_id, userId;
            Date date, createDate, statusDate;
            if (selectedItems.length == 0) return "";
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat datefmt = new SimpleDateFormat("yyyyMMdd");
            date = calendar.getTime();
            userId = getUserInfoBean().getUserId();
            doc_id = moduleFactory.getSequentialService().getDocId(datefmt.format(date), "B");

            for(SequentialAnomalyBean list : selectedItems) {
                boolean chk = moduleFactory.getSequentialService().checkPanBankUsed(list.getPan(), list.getBankid());
                if (chk) {
                    String blocked_to = ((list.getTrxtype().equals("SP1")) ? "PGW" : "B24");
                    moduleFactory.getSequentialService().saveBlockedPanDetail(doc_id, list.getPan(), blocked_to, list.getBankid(), "");
                    moduleFactory.getSequentialService().updatePan(list.getPan(), "Blocked Selected");
                    updt = "true";
                }
            }

            if(updt.equals("true")){
                moduleFactory.getSequentialService().saveBlockedPanHeader(doc_id, date, userId, date, "Created", "");
                return PageConstant.PAGE_BLOCKED_ADMINISTRATION_DETAIL + "?faces-redirect=true" + "&pDocId=" + paramDecode(doc_id);
            }

            selectedItems = null;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }


    public String addAction() {
        try {
            String doc_id, userId;
            Date date;
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat datefmt = new SimpleDateFormat("yyyyMMdd");
            date = calendar.getTime();
            userId = getUserInfoBean().getUserId();
            doc_id = moduleFactory.getSequentialService().getDocId(datefmt.format(date), "B");
            moduleFactory.getSequentialService().saveBlockedPanHeader(doc_id, date, userId, date, "Created", "");

            return PageConstant.PAGE_BLOCKED_ADMINISTRATION_DETAIL_MANUAL + "?faces-redirect=true" + "&pDocId=" + paramDecode(doc_id);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String paramDecode(String data){
        //encrypt param
        if(!data.equals(null)){
            data = HttpUtils.decodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<SequentialAnomalyBean> getList() {
        return list;
    }

    public void setList(List<SequentialAnomalyBean> list) {
        this.list = list;
    }

    public List<PanTrxidBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<PanTrxidBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPeriodFr() {
        return periodFr;
    }

    public void setPeriodFr(String periodFr) {
        this.periodFr = periodFr;
    }

    public String getPeriodTo() {
        return periodTo;
    }

    public void setPeriodTo(String periodTo) {
        this.periodTo = periodTo;
    }

    public Boolean getRoleBank() {
        return roleBank;
    }

    public void setRoleBank(Boolean roleBank) {
        this.roleBank = roleBank;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public SequentialAnomalyBean[] getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(SequentialAnomalyBean[] selectedItems) {
        this.selectedItems = selectedItems;
    }

    public class ListSequentialDataModel extends ListDataModel<SequentialAnomalyBean> implements SelectableDataModel<SequentialAnomalyBean> {

        public ListSequentialDataModel(List<SequentialAnomalyBean> list) {
            super(list);
        }

        @Override
        public SequentialAnomalyBean getRowData(String rowKey) {
            List<SequentialAnomalyBean> list = (List<SequentialAnomalyBean>)getWrappedData();
            for(SequentialAnomalyBean listSequential : list) {
                String pp = listSequential.getPan().toString() + listSequential.getBankid().toString();
                if (pp.equals(rowKey) ) {
                    return listSequential;
                }
            }
            return null;
        }

        @Override
        public Object getRowKey(SequentialAnomalyBean listSequential) {
            return listSequential.getPan() + listSequential.getBankid().toString();
        }

    }

    public ListSequentialDataModel getListSequentialDataModel() {
        return listSequentialDataModel;
    }

    public void setListSequentialDataModel(ListSequentialDataModel listSequentialDataModel) {
        this.listSequentialDataModel = listSequentialDataModel;
    }


}

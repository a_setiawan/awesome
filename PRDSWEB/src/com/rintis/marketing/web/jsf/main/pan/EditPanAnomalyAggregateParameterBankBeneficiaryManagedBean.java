package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.PanAnomalyParamBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by aabraham on 04/03/2019.
 */
@ManagedBean
@ViewScoped
public class EditPanAnomalyAggregateParameterBankBeneficiaryManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<PanAnomalyParamBean> listPanAnomalyParam;
    private String trx_id;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        String trx = (String)getRequestParameterMap().get("wlTrx");

        if(trx.equals("POS/Debit")){

            trx = "01";

        }else if(trx.equals("Cash Withdrawal")){

            trx = "10";

        }else if(trx.equals("Payment")){

            trx = "17";

        }else if(trx.equals("Transfer Debet/Kredit")){

            trx = "40";

        }else if(trx.equals("Top-up E-Money")){

            trx = "85";

        }

        if (trx != null) {
            try {
                listPanAnomalyParam = moduleFactory.getPanService().getParamBankBene(trx);
                if (!isPostback()) {
                    createTlog(moduleFactory);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    //start modify by erichie
    public String editAction() {
        try {
            String trx = listPanAnomalyParam.get(0).getTrx();
            BigDecimal mintrxapp1h = listPanAnomalyParam.get(0).getTrxApp1h();
            BigDecimal minamtapp1h = listPanAnomalyParam.get(0).getAmtApp1h();
            BigDecimal mintrxdc1h = listPanAnomalyParam.get(0).getTrxDc1h();
            BigDecimal minamtdc1h = listPanAnomalyParam.get(0).getAmtDc1h();
            BigDecimal mintrxdcfree1h = listPanAnomalyParam.get(0).getTrxDcFree1h();
            BigDecimal minamtdcfree1h = listPanAnomalyParam.get(0).getAmtDcFree1h();
            BigDecimal mintrxrvsl1h = listPanAnomalyParam.get(0).getTrxRvsl1h();
            BigDecimal minamtrvsl1h = listPanAnomalyParam.get(0).getAmtRvsl1h();
            BigDecimal minamttot1h = listPanAnomalyParam.get(0).getAmtTot1h();

            BigDecimal mintrxapp3h = listPanAnomalyParam.get(0).getTrxApp3h();
            BigDecimal minamtapp3h = listPanAnomalyParam.get(0).getAmtApp3h();
            BigDecimal mintrxdc3h = listPanAnomalyParam.get(0).getTrxDc3h();
            BigDecimal minamtdc3h = listPanAnomalyParam.get(0).getAmtDc3h();
            BigDecimal mintrxdcfree3h = listPanAnomalyParam.get(0).getTrxDcFree3h();
            BigDecimal minamtdcfree3h = listPanAnomalyParam.get(0).getAmtDcFree3h();
            BigDecimal mintrxrvsl3h = listPanAnomalyParam.get(0).getTrxRvsl3h();
            BigDecimal minamtrvsl3h = listPanAnomalyParam.get(0).getAmtRvsl3h();
            BigDecimal minamttot3h = listPanAnomalyParam.get(0).getAmtTot3h();

            BigDecimal mintrxapp6h = listPanAnomalyParam.get(0).getTrxApp6h();
            BigDecimal minamtapp6h = listPanAnomalyParam.get(0).getAmtApp6h();
            BigDecimal mintrxdc6h = listPanAnomalyParam.get(0).getTrxDc6h();
            BigDecimal minamtdc6h = listPanAnomalyParam.get(0).getAmtDc6h();
            BigDecimal mintrxdcfree6h = listPanAnomalyParam.get(0).getTrxDcFree6h();
            BigDecimal minamtdcfree6h = listPanAnomalyParam.get(0).getAmtDcFree6h();
            BigDecimal mintrxrvsl6h = listPanAnomalyParam.get(0).getTrxRvsl6h();
            BigDecimal minamtrvsl6h = listPanAnomalyParam.get(0).getAmtRvsl6h();
            BigDecimal minamttot6h = listPanAnomalyParam.get(0).getAmtTot6h();

            BigDecimal mintrxapp1d = listPanAnomalyParam.get(0).getTrxApp1d();
            BigDecimal minamtapp1d = listPanAnomalyParam.get(0).getAmtApp1d();
            BigDecimal mintrxdc1d = listPanAnomalyParam.get(0).getTrxDc1d();
            BigDecimal minamtdc1d = listPanAnomalyParam.get(0).getAmtDc1d();
            BigDecimal mintrxdcfree1d = listPanAnomalyParam.get(0).getTrxDcFree1d();
            BigDecimal minamtdcfree1d = listPanAnomalyParam.get(0).getAmtDcFree1d();
            BigDecimal mintrxrvsl1d = listPanAnomalyParam.get(0).getTrxRvsl1d();
            BigDecimal minamtrvsl1d = listPanAnomalyParam.get(0).getAmtRvsl1d();
            BigDecimal minamttot1d = listPanAnomalyParam.get(0).getAmtTot1d();

            BigDecimal mintrxapp3d = listPanAnomalyParam.get(0).getTrxApp3d();
            BigDecimal minamtapp3d = listPanAnomalyParam.get(0).getAmtApp3d();
            BigDecimal mintrxdc3d = listPanAnomalyParam.get(0).getTrxDc3d();
            BigDecimal minamtdc3d = listPanAnomalyParam.get(0).getAmtDc3d();
            BigDecimal mintrxdcfree3d = listPanAnomalyParam.get(0).getTrxDcFree3d();
            BigDecimal minamtdcfree3d = listPanAnomalyParam.get(0).getAmtDcFree3d();
            BigDecimal mintrxrvsl3d = listPanAnomalyParam.get(0).getTrxRvsl3d();
            BigDecimal minamtrvsl3d = listPanAnomalyParam.get(0).getAmtRvsl3d();
            BigDecimal minamttot3d = listPanAnomalyParam.get(0).getAmtTot3d();

            System.out.println(trx);

            moduleFactory.getPanService().updateParamBankBene(trx, mintrxapp1h, minamtapp1h, mintrxdc1h, minamtdc1h, mintrxdcfree1h, minamtdcfree1h, mintrxrvsl1h, minamtrvsl1h, minamttot1h, mintrxapp3h, minamtapp3h, mintrxdc3h, minamtdc3h, mintrxdcfree3h, minamtdcfree3h, mintrxrvsl3h, minamtrvsl3h, minamttot3h, mintrxapp6h, minamtapp6h, mintrxdc6h, minamtdc6h, mintrxdcfree6h, minamtdcfree6h, mintrxrvsl6h, minamtrvsl6h, minamttot6h, mintrxapp1d, minamtapp1d, mintrxdc1d, minamtdc1d, mintrxdcfree1d, minamtdcfree1d, mintrxrvsl1d, minamtrvsl1d, minamttot1d, mintrxapp3d, minamtapp3d, mintrxdc3d, minamtdc3d, mintrxdcfree3d, minamtdcfree3d, mintrxrvsl3d, minamtrvsl3d, minamttot3d);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_PARAM_BANK_BENE_PARAM + "?faces-redirect=true";
    }
    //end modify by erichie

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<PanAnomalyParamBean> getPanAnomalyParam() {
        return listPanAnomalyParam;
    }

    public void setPanAnomalyParam(List<PanAnomalyParamBean> listPanAnomalyParam) {
        this.listPanAnomalyParam = listPanAnomalyParam;
    }
}

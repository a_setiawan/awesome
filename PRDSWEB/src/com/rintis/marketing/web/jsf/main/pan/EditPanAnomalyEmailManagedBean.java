package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.PanAnomalyEmailBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * Created by aabraham on 01/03/2019.
 */
@ManagedBean
@ViewScoped
public class EditPanAnomalyEmailManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<PanAnomalyEmailBean> listPanAnomalyEmail;
    private PanAnomalyEmailBean panAnomalyEmailBean;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        String useremail = (String)getRequestParameterMap().get("wlEmail");
        if (useremail != null) {
            try {
                listPanAnomalyEmail = moduleFactory.getPanService().getEmail(useremail);
                if (!isPostback()) {
                    createTlog(moduleFactory);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public String editAction() {
    //start modify by erichie
        try {
            String useremail = listPanAnomalyEmail.get(0).getUserEmail();
            String gender = listPanAnomalyEmail.get(0).getGender();
            String username = listPanAnomalyEmail.get(0).getUserName();
            String alert1h = listPanAnomalyEmail.get(0).getAlert1H();
            String alert3h = listPanAnomalyEmail.get(0).getAlert3H();
            String alert6h = listPanAnomalyEmail.get(0).getAlert6H();
            String alert1d = listPanAnomalyEmail.get(0).getAlert1D();
            String alert3d = listPanAnomalyEmail.get(0).getAlert3D();
            String alertMaxAmt = listPanAnomalyEmail.get(0).getAlertMaxAmt();
            String alertMccVelocity = listPanAnomalyEmail.get(0).getAlertMccVelocity();
            String alertSeqVelocity = listPanAnomalyEmail.get(0).getAlertSeqVelocity();
            /*Add and modify by aaw 02/07/2021*/
            String alertMpanAcq = listPanAnomalyEmail.get(0).getAlertMpanAcq();

            moduleFactory.getPanService().updateEmail(useremail, gender, username, alert1h, alert3h, alert6h, alert1d, alert3d, alertMaxAmt, alertMccVelocity, alertSeqVelocity, alertMpanAcq);
            /*End*/

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_EMAIL_PAN_EMAIL_PAN + "?faces-redirect=true";
    }
    //end modify by erichie

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<PanAnomalyEmailBean> getPanAnomalyEmail() {
        return listPanAnomalyEmail;
    }

    public void setPanAnomalyEmail(List<PanAnomalyEmailBean> listPanAnomalyEmail) {
        this.listPanAnomalyEmail = listPanAnomalyEmail;
    }
}

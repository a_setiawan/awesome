package com.rintis.marketing.web.jsf.main.biller;

import com.rintis.marketing.beans.entity.BillerDummy;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 * Created by aaw on 20211112
 */

@ManagedBean
@ViewScoped
public class EditWhitelistBillerManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private BillerDummy whitelistBiller;
    private String isValid = "true";

    @Override
    @PostConstruct
    protected void initManagedBean() {
        String pInstCd = paramDecodeUrl((String)getRequestParameterMap().get("pInstCd"));
        String pCustNo = paramDecodeUrl((String)getRequestParameterMap().get("pCustNo"));

        if (!"".equals(pInstCd) && !"".equals(pCustNo)) {
            try {
                whitelistBiller = moduleFactory.getBillerService().findBillerDummyById(pInstCd, pCustNo);
                if (!isPostback()) {
                    createTlog(moduleFactory);
                }
                isValid = "true";
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        } else {
            isValid = "false";
        }
    }

    public String editAction() {
        try {
            if (whitelistBiller.getInstCd().length() == 0) {
                glowMessageError("Data Not Valid, 'Institution Code' Is Blank !", "");
                return "";
            }

            if (whitelistBiller.getCustNo().length() == 0) {
                glowMessageError("Data Not Valid, 'Customer No' Is Blank !", "");
                return "";
            }

            moduleFactory.getBillerService().updateWhitelistBiller(whitelistBiller);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "/apps/biller/listWhitelistBiller.do?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public BillerDummy getWhitelistBiller() {
        return whitelistBiller;
    }

    public void setWhitelistBiller(BillerDummy whitelistBiller) {
        this.whitelistBiller = whitelistBiller;
    }

    public String getIsValid() {
        return isValid;
    }

    public void setIsValid(String isValid) {
        this.isValid = isValid;
    }
}

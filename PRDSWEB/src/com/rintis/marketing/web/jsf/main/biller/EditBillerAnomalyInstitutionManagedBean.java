package com.rintis.marketing.web.jsf.main.biller;

import com.rintis.marketing.beans.entity.BillerAnomalyInstitusiParam;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * Created by aaw 20211116
 */

@ManagedBean
@ViewScoped
public class EditBillerAnomalyInstitutionManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private BillerAnomalyInstitusiParam billerAnomalyInstitusiParam;
    private List<BillerAnomalyInstitusiParam> listParamBiller;
    String pBillerId = "";
    String pBillerName = "";

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            pBillerId = paramDecodeUrl(getRequestParameterMap().get("billerId").toString());
            pBillerName = paramDecodeUrl(getRequestParameterMap().get("billerName").toString());

            if (!"".equals(pBillerId)) {
                listParamBiller = moduleFactory.getBillerService().fetchDataBillerParamInstitusiByID(pBillerId);
            }

            createTlog(moduleFactory);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String saveAction() {
        try {
            if (listParamBiller.size() == 0) {
                glowMessageError("Update failed !", "");
                return "";
            }
            moduleFactory.getBillerService().updateMultipleBillerAnomalyinstitution(listParamBiller);
            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "billerAnomalyInstitusiParameter?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public BillerAnomalyInstitusiParam getBillerAnomalyInstitusiParam() {
        return billerAnomalyInstitusiParam;
    }

    public void setBillerAnomalyInstitusiParam(BillerAnomalyInstitusiParam billerAnomalyInstitusiParam) {
        this.billerAnomalyInstitusiParam = billerAnomalyInstitusiParam;
    }

    public List<BillerAnomalyInstitusiParam> getListParamBiller() {
        return listParamBiller;
    }

    public void setListParamBiller(List<BillerAnomalyInstitusiParam> listParamBiller) {
        this.listParamBiller = listParamBiller;
    }

    public String getpBillerId() {
        return pBillerId;
    }

    public void setpBillerId(String pBillerId) {
        this.pBillerId = pBillerId;
    }

    public String getpBillerName() {
        return pBillerName;
    }

    public void setpBillerName(String pBillerName) {
        this.pBillerName = pBillerName;
    }
}

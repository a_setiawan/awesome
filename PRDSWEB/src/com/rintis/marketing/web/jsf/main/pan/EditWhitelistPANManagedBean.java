package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.WhitelistBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.common.ParamConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Created by aabraham on 13/02/2019.
 */
@ManagedBean
@ViewScoped
public class EditWhitelistPANManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<WhitelistBean> listWhitelistPan;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        String pan = (String)getRequestParameterMap().get(ParamConstant.PARAM_PAN);
        String pengirim = (String)getRequestParameterMap().get(ParamConstant.PARAM_PENGIRIM);

        if (pan != null) {
            try {
                listWhitelistPan = moduleFactory.getPanService().getWhitelistPan(pan, pengirim);
                if (!isPostback()) {
                    createTlog(moduleFactory);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public String editAction() {
        try {
            String pan = listWhitelistPan.get(0).getPan();
            String pengirim = listWhitelistPan.get(0).getPengirim();
            BigDecimal freqApp = listWhitelistPan.get(0).getFreqApp();
            BigDecimal freqDc = listWhitelistPan.get(0).getFreqDc();
            BigDecimal freqDcFree = listWhitelistPan.get(0).getFreqDcFree();
            BigDecimal freqRvsl = listWhitelistPan.get(0).getFreqRvsl();
            BigDecimal totalAmtApp = listWhitelistPan.get(0).getTotalAmtApp();
            BigDecimal totalAmtDc = listWhitelistPan.get(0).getTotalAmtDc();
            BigDecimal totalAmtDcFree = listWhitelistPan.get(0).getTotalAmtDcFree();
            BigDecimal totalAmtRvsl = listWhitelistPan.get(0).getTotalAmtRvsl();
            BigDecimal totalAmt = listWhitelistPan.get(0).getTotalAmt();
            String active = listWhitelistPan.get(0).getActive();

            moduleFactory.getPanService().updatePan(pan, pengirim, freqApp, freqDc, freqDcFree, freqRvsl, totalAmtApp, totalAmtDc, totalAmtDcFree, totalAmtRvsl, totalAmt, active);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_WHITELIST_PAN_WHITELIST_PAN + "?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<WhitelistBean> getWhitelistPan() {
        return listWhitelistPan;
    }

    public void setWhitelistPan(List<WhitelistBean> listWhitelistPan) {
        this.listWhitelistPan = listWhitelistPan;
    }
}

package com.rintis.marketing.web.jsf.main.monitor.tm;

import com.martinlinha.c3faces.model.C3DataSet;
import com.martinlinha.c3faces.model.C3DataSetString;
import com.martinlinha.c3faces.model.C3ViewDataSet;
import com.martinlinha.c3faces.model.C3ViewDataSetString;
import com.martinlinha.c3faces.script.property.*;
import com.rintis.marketing.beans.bean.bi.C3ChartBean;
import com.rintis.marketing.beans.bean.bi.EsqTrx5MinBean;
import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.EisBank;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.constant.ParameterMapName;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.MathUtils;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import com.rintis.marketing.web.jsf.main.dashboard.UserDashboardManagedBean;
import com.rintis.marketing.web.jsf.main.monitor.AbstractTransactionIndicatorMonitoring;
import org.primefaces.model.chart.*;
import org.primefaces.model.chart.Axis;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.*;

@ManagedBean
@ViewScoped
public class TransactionIndicatorMonitoringHourHistoryManagedBean extends AbstractTransactionIndicatorMonitoring {

    private List<BankBean> listBank;
    private String render;
    private List<EsqTrxIdBean> listTrxId;
    private String graphRefreshInterval;
    private boolean showTable;
    private String showPercentButtonText;


    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0502";
            pageTitle = getMenuName(moduleFactory, menuId);

            String s = StringUtils.checkNull((String)getRequestParameterMap().get("bid"));
            if (s.length() > 0) {
                selectedBankId = s;
            }

            s = StringUtils.checkNull((String)getRequestParameterMap().get("trxid"));
            if (s.length() > 0) {
                selectedTrxId = s;
            }

            aggregateTimeFrame = DataConstant.T1H;
            timeFrame = TIMEFRAME_1H;
            history = true;
            renderResult = "false";
            showTable = false;
            graphRefreshInterval = DataConstant.DEFAULT_GRAPH_REFRESH_INTERVAL;
            defaultRangeInHour = Integer.parseInt(
                    moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                            DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_HOURLY)
            );

            if (selectedTrxId != null) {
                //selectedStartDate = new Date();
                //selectedEndDate = new Date();
                s = moduleFactory.getAggregateService().getAggrLastTime(selectedTrxId, aggregateTimeFrame);
                Date lastthdate = DateUtils.convertDate(s, DateUtils.DATE_YYMMDD_HHMM);
                //if (lastthdate.getTime() < selectedEndDate.getTime()) {
                selectedEndDate = lastthdate;
                selectedEndHour = DateUtils.getHour(selectedEndDate);
                selectedEndMinute = DateUtils.getMinute(selectedEndDate);
                //}
                Date frdate = DateUtils.addDate(Calendar.HOUR_OF_DAY, selectedEndDate, defaultRangeInHour * -1);
                selectedStartDate = frdate;
                selectedStartHour = DateUtils.getHour(frdate);
                selectedStartMinute = DateUtils.getMinute(frdate);
            }

            render = "false";
            renderAcqOnly = "false";
            renderIssOnly = "false";
            renderBnfOnly = "false";
            renderAcqIss = "false";
            renderAcqBnf = "false";
            renderIssBnf = "false";
            showPercent = false;
            showPercentButtonText = "Show Percent";

            Map<String, Object> paramBank = new HashMap<>();
            listBank = moduleFactory.getMasterService().listBank(paramBank);

            listTrxId = moduleFactory.getMasterService().listEsqTrxId();

            if (StringUtils.checkNull(selectedBankId).length() > 0 && StringUtils.checkNull(selectedTrxId).length() > 0) {
                submitAction();
            }

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            trxIdChangeListener();


            if (selectedBankId.length() == 0) {
                glowMessageError("Select Bank", "");
                return "";
            }
            if (selectedTrxId.length() == 0) {
                glowMessageError("Select Transaction", "");
                return "";
            }
            if (selectedTransactionIndicatorId == null) {
                glowMessageError("Select Transaction Indicator", "");
                return "";
            }
            if (selectedTransactionIndicatorId.length == 0) {
                glowMessageError("Select Transaction Indicator", "");
                return "";
            }


            processSubmit();

            render = "true";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }


    public String showPercentChangeListener() {
        return "transactionIndicatorMonitoringHourPercent?faces-redirect=true&bid=" +
                StringUtils.checkNull(selectedBankId) + "&trxid=" + StringUtils.checkNull(selectedTrxId);
    }


    public String showHistory() {
        return "transactionIndicatorMonitoringHourHistory?faces-redirect=true&bid=" +
                StringUtils.checkNull(selectedBankId) + "&trxid=" + StringUtils.checkNull(selectedTrxId);
    }










    public void onPoolListener() {
        try {
            String s = moduleFactory.getAggregateService().getAggrLastTime(selectedTrxId, aggregateTimeFrame);
            Date thdate = DateUtils.convertDate(s, DateUtils.DATE_YYMMDD_HHMM);
            selectedEndDate = thdate;

            selectedEndHour = DateUtils.getHour(thdate);
            selectedEndMinute = DateUtils.getMinute(thdate);

            Date frdate = DateUtils.addDate(Calendar.HOUR_OF_DAY, thdate, defaultRangeInHour*-1);

            selectedStartDate = frdate;
            selectedStartHour = DateUtils.getHour(frdate);
            selectedStartMinute = DateUtils.getMinute(frdate);


            submitAction();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    public List<BankBean> getListBank() {
        return listBank;
    }

    public void setListBank(List<BankBean> listBank) {
        this.listBank = listBank;
    }


    public String getRender() {
        return render;
    }

    public void setRender(String render) {
        this.render = render;
    }

    public List<EsqTrxIdBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<EsqTrxIdBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

    public String getGraphRefreshInterval() {
        return graphRefreshInterval;
    }

    public void setGraphRefreshInterval(String graphRefreshInterval) {
        this.graphRefreshInterval = graphRefreshInterval;
    }

    public boolean isShowTable() {
        return showTable;
    }

    public void setShowTable(boolean showTable) {
        this.showTable = showTable;
    }

    public String getShowPercentButtonText() {
        return showPercentButtonText;
    }

    public void setShowPercentButtonText(String showPercentButtonText) {
        this.showPercentButtonText = showPercentButtonText;
    }
}

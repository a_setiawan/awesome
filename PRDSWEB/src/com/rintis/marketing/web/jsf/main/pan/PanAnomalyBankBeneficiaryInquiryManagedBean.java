
package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.beans.bean.bi.PanAnomalyBean;
import com.rintis.marketing.beans.bean.monitor.PanTrxidBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.app.main.module.login.LoginService;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.SessionFactory;
import org.primefaces.model.SelectableDataModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.ListDataModel;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by aabraham on 28/01/2019.
 */
@ManagedBean
@ViewScoped
public class PanAnomalyBankBeneficiaryInquiryManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private SessionFactory sessionFactory;
    private List<PanTrxidBean> listTrxId;
    private List<PanAnomalyBean> list;
    private PanAnomalyDataModel listPanAnomalyDataModel;
    private List<PanAnomalyBean> selectedAlertAnomaly;
    private PanAnomalyBean[] selectedItems;
    private Date startDate;
    private Date endDate;
    private Date startTime;
    private Date endTime;
    private Date nowTime;
    private String transaction;
    private String selectedTrxId;
    private String periodRange;
    private String periodFr;
    private String periodTo;
    private String timeFr;
    private String timeTo;
    private String pan;
    private String roleID;
    private Boolean sender;
    private String period;
    private Boolean whitelist;
    private Boolean role;
    private Boolean roleBank;
    private Boolean disable;
    private Boolean enable3D;
    private Boolean disable3D;
    /*Add by aaw 16/072021*/
    private boolean isUpdate;
    /*End add*/

    public PanAnomalyBankBeneficiaryInquiryManagedBean() {
    }

    @Override
    @PostConstruct
    protected void initManagedBean() {

        roleID = LoginService.getRoleID;
        try {
            menuId = "0523";
            pageTitle = getMenuName(moduleFactory, menuId);

            startDate = new Date();
            endDate = new Date();
            startTime = new Date();
            endTime = new Date();
            nowTime = new Date();

            startTime = DateUtils.setMinutes(startTime, 0);
            startTime = DateUtils.setHours(startTime, startTime.getHours() - 1);
            endTime = DateUtils.setMinutes(endTime, 0);

            SimpleDateFormat datefmt = new SimpleDateFormat("yyMMdd");
            periodRange = datefmt.format(startDate);

            period = "60M";
            transaction = "ATM";
            disable = false;
            whitelist = false;

            listTrxId = moduleFactory.getPanService().panListTrxIdBene();

            if(Integer.parseInt(this.getRoleID()) == 99){
                role = true;
            } else {
                role = false;
            }

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            UserInfoBean userInfoBean = getUserInfoBean();
            String bank_id = "";
            if (userInfoBean.getUserLoginBank() != null) {
                bank_id = userInfoBean.getUserLoginBank().getBankId();
                roleBank = false;
            } else {
                roleBank = true;
            }

            if (period.equals("1D") || period.equals("3D")){
                disable = false;
            } else {
                disable = true;
            }

            if(period.equals("3D")){
                enable3D = true;
                disable3D = false;
            }else{
                enable3D = false;
                disable3D = true;
            }

            SimpleDateFormat datefmt = new SimpleDateFormat("yyMMdd");
            SimpleDateFormat timefmt = new SimpleDateFormat("HHmm");
            periodFr = datefmt.format(startDate);
            periodTo = datefmt.format(endDate);
            timeFr = timefmt.format(startTime);
            timeTo = timefmt.format(endTime);

            if(timeTo.substring(0,2).equals("00")){
                timeTo = "24" + timeTo.substring(2,4);
            }

            if(transaction.equals("40")){
                sender = true;
            }else{
                sender = false;
            }

            long numOfDays = getDifferenceDays(startDate, nowTime) + 1;
            long days = Integer.parseInt(periodRange) - Integer.parseInt(periodFr);

            System.out.println(period);
            if (numOfDays < 30) {
                if(period.equals("1H")){
                    //PanTrxidBean trxid = moduleFactory.getPanService().getPanTrxId(selectedItems.toString());
                    //transaction = trxid.getTrxid();
                    //System.out.println(trxid);
                    list = moduleFactory.getPanService().listAnomalyPanBene1H(periodFr, periodTo, timeFr, timeTo, transaction, whitelist, bank_id);
                    listPanAnomalyDataModel = new PanAnomalyDataModel(list);
                }else if(period.equals("3H")){
                    //PanTrxidBean trxid = moduleFactory.getPanService().getPanTrxId(selectedItems.toString());
                    //System.out.println(transaction);
                    list = moduleFactory.getPanService().listAnomalyPanBene3H(periodFr, periodTo, timeFr, timeTo, transaction, whitelist, bank_id);
                    listPanAnomalyDataModel = new PanAnomalyDataModel(list);
                }else if(period.equals("6H")){
                    //PanTrxidBean trxid = moduleFactory.getPanService().getPanTrxId(selectedItems.toString());
                    //System.out.println(transaction);
                    list = moduleFactory.getPanService().listAnomalyPanBene6H(periodFr, periodTo, timeFr, timeTo, transaction, whitelist, bank_id);
                    listPanAnomalyDataModel = new PanAnomalyDataModel(list);
                }else if(period.equals("1D")){
                    //PanTrxidBean trxid = moduleFactory.getPanService().getPanTrxId(selectedItems.toString());
                    //System.out.println(transaction);
                    list = moduleFactory.getPanService().listAnomalyPanBene1D(periodFr, periodTo, transaction, whitelist, bank_id);
                    listPanAnomalyDataModel = new PanAnomalyDataModel(list);
                }else if(period.equals("3D")){
                    //PanTrxidBean trxid = moduleFactory.getPanService().getPanTrxId(selectedItems.toString());
                    //System.out.println(transaction);
                    list = moduleFactory.getPanService().listAnomalyPanBene3D(periodFr, periodTo, transaction, whitelist, bank_id);
                    listPanAnomalyDataModel = new PanAnomalyDataModel(list);
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String errorMsg(){
        long numOfDays = getDifferenceDays(startDate, nowTime) + 1;
        /*comment by aaw, 2/6/21*/
        //long days = Integer.parseInt(periodRange) - Integer.parseInt(periodFr);
        /*End comment*/
        if (numOfDays > 7) {
            glowMessageError("Max 7 Days", "");
        }

        return "";
    }

    /*Add by aaw 16/07/2021*/
    public void popupMsg() {
        if (isUpdate) {
            glowMessageInfo("Beneficiary Has Been Saved", "");
        }
    }
    /*End add*/

    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public void onPoolAction() {
        submitAction();
    }

    public String saveItemAction() {

        try {
            String act = "true";
            isUpdate = false;
            BigDecimal freq = BigDecimal.valueOf(0);
            if (selectedItems == null) return "";
            for(PanAnomalyBean aa : selectedItems) {
                /*Modify by aaw 21/05/21*/
                boolean chk = moduleFactory.getPanService().checkBeneficiaryReceiverUsed(aa.getToAcct().trim(), aa.getBeneficiaryName().trim());
                if (!chk) {
                    moduleFactory.getPanService().saveBeneficiary(aa.getToAcct().trim(), aa.getBeneficiaryName().trim(), freq, freq, freq, freq, freq, freq, freq, freq, freq, act);
                /*End modify*/
                    isUpdate = true;
                }
            }

            if(isUpdate){
                submitAction();
                //glowMessageInfo("Beneficiary has been Saved", "");
            }

            selectedItems = null;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String paramDecode(String data){
        //encrypt param
        if(!data.equals(null)){
            data = HttpUtils.decodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<PanAnomalyBean> getList() {
        return list;
    }

    public void setList(List<PanAnomalyBean> list) {
        this.list = list;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public Boolean getWhitelist() {
        return whitelist;
    }

    public void setWhitelist(Boolean whitelist) {
        this.whitelist = whitelist;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPeriodRange() {
        return periodRange;
    }

    public void setPeriodRange(String periodRange) {
        this.periodRange = periodRange;
    }

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    public Boolean getRole(){
        return role;
    }

    public Boolean getSender(){
        return sender;
    }

    public void setRole(Boolean role){
        this.role = role;
    }

    public Boolean getRoleBank() {
        return roleBank;
    }

    public void setRoleBank(Boolean roleBank) {
        this.roleBank = roleBank;
    }

    public Boolean getDisable() {
        return disable;
    }

    public void setDisable(Boolean disable) {
        this.disable = disable;
    }

    public Boolean getEnable3D() {
        return enable3D;
    }

    public void setEnable3D(Boolean enable3D) {
        this.enable3D = enable3D;
    }

    public Boolean getDisable3D() {
        return disable3D;
    }

    public void setDisable3D(Boolean disable3D) {
        this.disable3D = disable3D;
    }

    public class PanAnomalyDataModel extends ListDataModel<PanAnomalyBean> implements SelectableDataModel<PanAnomalyBean> {

    public PanAnomalyDataModel(List<PanAnomalyBean> list) {
        super(list);
    }

    @Override
    public PanAnomalyBean getRowData(String rowKey) {
        List<PanAnomalyBean> list = (List<PanAnomalyBean>)getWrappedData();
        for(PanAnomalyBean panAnomaly : list) {
            /*Modify by aaw 24/05/21*/
            String pp = panAnomaly.getToAcct().toString() + panAnomaly.getBeneficiaryName().toString();
            /*End modify*/
            if (pp.equals(rowKey) ) {
                return panAnomaly;
            }
        }
        return null;
    }

    @Override
    public Object getRowKey(PanAnomalyBean panAnomaly) {
        /*Modify by aaw 24/05/21*/
        return panAnomaly.getToAcct() + panAnomaly.getBeneficiaryName();
        /*End modify*/
    }

}

    public PanAnomalyDataModel getListPanAnomalyDataModel() {
        return listPanAnomalyDataModel;
    }

    public void setListPanAnomalyDataModel(PanAnomalyDataModel listPanAnomalyDataModel) {
        this.listPanAnomalyDataModel = listPanAnomalyDataModel;
    }

    public List<PanAnomalyBean> getSelectedAnomaly() {
        return selectedAlertAnomaly;
    }

    public void setSelectedAnomaly(List<PanAnomalyBean> selectedAlertAnomaly) {
        this.selectedAlertAnomaly = selectedAlertAnomaly;
    }

    public PanAnomalyBean[] getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(PanAnomalyBean[] selectedItems) {
        this.selectedItems = selectedItems;
    }

    public List<PanTrxidBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<PanTrxidBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

}

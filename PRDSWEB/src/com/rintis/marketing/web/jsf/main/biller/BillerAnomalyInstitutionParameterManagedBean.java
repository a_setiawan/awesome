package com.rintis.marketing.web.jsf.main.biller;

import com.rintis.marketing.beans.bean.biller.TmsBillerBean;
import com.rintis.marketing.beans.entity.BillerAnomalyInstitusiParam;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aaw 20211115
 */

@ManagedBean
@ViewScoped
public class BillerAnomalyInstitutionParameterManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<BillerAnomalyInstitusiParam> listBillerAnomalyParam = new ArrayList<>();
    private List<TmsBillerBean> listBiller = new ArrayList<>();
    private String selectedBillerId = "";
    private String link = "";

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listBiller = moduleFactory.getBillerService().fetchAllActiveBiller();
        }catch(Exception e){
            log.error(e.getMessage(), e);
        }
    }

    public String submitBillerId(){
        if(selectedBillerId.isEmpty()){
            glowMessageError("Input institusi terlebih dahulu sebelum add parameter","");
            link = "";
        }else{
            List<BillerAnomalyInstitusiParam> res = new ArrayList<>();
            try {
                res = moduleFactory.getBillerService().checkBillerInstitusiExist(selectedBillerId);
                if (res.size() > 0) {
                    glowMessageError("Parameter institusi terpilih sudah ada, silahkan lakukan update bukan add !","");
                    link = "";
                } else {
                    TmsBillerBean billerBean = moduleFactory.getBillerService().findBillerById(selectedBillerId);
                    link = "addBillerAnomalyInstitusiParameter.do?faces-redirect=true&billerId=" + paramEncodeUrl(selectedBillerId) + "&billerName=" + paramEncodeUrl(billerBean.getBillerName());
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                link = "";
                glowMessageError("Something went wrong !","");
            }
        }
        return link;
    }

    public void submitAction(){
        try {
            if(selectedBillerId != ""){
                listBillerAnomalyParam = moduleFactory.getBillerService().fetchBillerAnomalyInstitusiParam(selectedBillerId);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    public String submitUpdate(){
        if("".equals(selectedBillerId)){
            glowMessageError("Input institusi terlebih dahulu sebelum add parameter","");
            link = "";
        } else {
            try {
                List<BillerAnomalyInstitusiParam> res = moduleFactory.getBillerService().checkBillerInstitusiExist(selectedBillerId);
                if (res.size() > 0) {
                    TmsBillerBean billerBean = moduleFactory.getBillerService().findBillerById(selectedBillerId);
                    link = "editBillerAnomalyInstitusiParameter.do?faces-redirect=true&billerId=" + paramEncodeUrl(selectedBillerId) + "&billerName=" + paramEncodeUrl(billerBean.getBillerName());
                } else {
                    glowMessageError("Data not exist, please click add first !","");
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                glowMessageError("Something went wrong !","");
            }
        }
        return link;
    }

    public String submitDelete(){
        String url = "";
        if ("".equals(selectedBillerId)) {
            glowMessageError("Select Institution first !","");
            return "";
        }
        try {
            moduleFactory.getBillerService().deleteBillerAnomalyInstitusi(selectedBillerId);
            url = "billerAnomalyInstitusiParameter?faces-redirect=true";
        }catch(Exception e){
            log.error(e.getMessage(), e);
            glowMessageError("Something went wrong !","");
        }
        return url;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public String getSelectedBillerId() {
        return selectedBillerId;
    }

    public void setSelectedBillerId(String selectedBillerId) {
        this.selectedBillerId = selectedBillerId;
    }

    public List<BillerAnomalyInstitusiParam> getListBillerAnomalyParam() {
        return listBillerAnomalyParam;
    }

    public void setListBillerAnomalyParam(List<BillerAnomalyInstitusiParam> listBillerAnomalyParam) {
        this.listBillerAnomalyParam = listBillerAnomalyParam;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<TmsBillerBean> getListBiller() {
        return listBiller;
    }

    public void setListBiller(List<TmsBillerBean> listBiller) {
        this.listBiller = listBiller;
    }
}

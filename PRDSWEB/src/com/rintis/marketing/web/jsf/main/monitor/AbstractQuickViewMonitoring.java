package com.rintis.marketing.web.jsf.main.monitor;

import com.martinlinha.c3faces.model.C3DataSet;
import com.martinlinha.c3faces.model.C3DataSetString;
import com.martinlinha.c3faces.model.C3ViewDataSet;
import com.martinlinha.c3faces.model.C3ViewDataSetString;
import com.martinlinha.c3faces.script.property.Axis;
import com.martinlinha.c3faces.script.property.Data;
import com.martinlinha.c3faces.script.property.Point;
import com.rintis.marketing.beans.bean.bi.C3ChartBean;
import com.rintis.marketing.beans.bean.bi.EsqTrx5MinBean;
import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.EisBank;
import com.rintis.marketing.beans.entity.EsqTrxid;
import com.rintis.marketing.beans.entity.TmsUserQuickView;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.MathUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.faces.bean.ManagedProperty;
import java.math.BigDecimal;
import java.util.*;

public abstract class AbstractQuickViewMonitoring extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    protected ModuleFactory moduleFactory;
    protected String selectedTrxId;
    protected List<EsqTrxIdBean> listTrxIdIndicator;
    protected String[] selectedTransactionIndicatorId;
    protected String selectedBankId;
    protected String selectedBankName;
    protected String selectedTrxName;
    protected String selectedTransactionIndicatorName;
    protected String renderResult;
    protected List<EsqTrx5MinBean>  list;
    protected int defaultRangeInHour;

    protected Date selectedEndDate;
    protected Integer selectedEndHour;
    protected Integer selectedEndMinute;
    protected Date selectedStartDate;
    protected Integer selectedStartHour;
    protected Integer selectedStartMinute;
    protected boolean showPercent;
    protected boolean history;
    //protected Date selectedTodayDate;
    protected Date selectedPrevDate;

    protected String renderAcqOnly;
    protected String renderIssOnly;
    protected String renderBnfOnly;
    protected String renderAcqIss;
    protected String renderAcqBnf;
    protected String renderIssBnf;

    protected C3ChartBean c3ChartBeanAcqOnly;
    protected C3ChartBean c3ChartBeanIssOnly;
    protected C3ChartBean c3ChartBeanBnfOnly;

    protected C3ChartBean c3ChartBeanAcqIss;
    protected C3ChartBean c3ChartBeanAcqBnf;
    protected C3ChartBean c3ChartBeanIssBnf;

    protected String aggregateTimeFrame;
    protected int timeFrame;
    protected final int TIMEFRAME_5M = 5;
    protected final int TIMEFRAME_15M = 15;
    protected final int TIMEFRAME_30M = 30;
    protected final int TIMEFRAME_1H = 60;
    protected final int TIMEFRAME_1D = 1440;

    //protected Boolean[] activeCurr;
    protected Boolean[] activePrev;
    //protected boolean checkAllCurr;
    protected boolean checkAllPrev;
    protected Boolean[] activePercentPrev;

    protected String renderPrevDate;

    protected List<TmsUserQuickView> listTmsUserQuickView;
    protected String selectedUserQuickView;

    public void processSubmit() throws Exception {
        //EisBank bank = moduleFactory.getMasterService().getBank(selectedBankId);
        BankBean bank = moduleFactory.getMasterService().getBankCA(selectedBankId);
        //selectedBankName = bank.getBankName();
        selectedBankName = bank.getBankname();

        if (selectedTrxId.equalsIgnoreCase(DataConstant.ALL)) {
            selectedTrxName = DataConstant.STR_ALL_TRANSACTION;
        } else {
            EsqTrxIdBean trxid = moduleFactory.getMasterService().getEsqTrxId(selectedTrxId);
            selectedTrxName = trxid.getTrxname();
        }

        renderAcqOnly = "false";
        renderIssOnly = "false";
        renderBnfOnly = "false";
        renderAcqIss = "false";
        renderAcqBnf = "false";
        renderIssBnf = "false";

        renderResult = "true";

        selectedTransactionIndicatorName = "";

        if (selectedTrxId.equalsIgnoreCase(DataConstant.ALL)) {
            selectedTransactionIndicatorName = "All";
            renderAcqOnly = "true";
            renderIssOnly = "true";
            renderBnfOnly = "true";
            renderAcqIss = "true";
            renderAcqBnf = "true";
            renderIssBnf = "true";

        } else {
            for (String id : selectedTransactionIndicatorId) {
                EsqTrxIdBean trxIndicator = moduleFactory.getMasterService().getEsqTrxIdIndicator(id);
                if (selectedTransactionIndicatorName.length() == 0) {
                    selectedTransactionIndicatorName = trxIndicator.getTrxnameindicator();
                } else {
                    selectedTransactionIndicatorName = selectedTransactionIndicatorName + ", " + trxIndicator.getTrxnameindicator();
                }

                if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ONLY)) {
                    renderAcqOnly = "true";
                }
                if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_ONLY)) {
                    renderIssOnly = "true";
                }
                if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__BNF_ONLY)) {
                    renderBnfOnly = "true";
                }
                if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ISS)) {
                    renderAcqIss = "true";
                }
                if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_BNF)) {
                    renderAcqBnf = "true";
                }
                if (id.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_BNF)) {
                    renderIssBnf = "true";
                }
            }
        }



        Date thdate = selectedEndDate;
        Date frdate = selectedStartDate;
        int prevDay = 0;

        String s = moduleFactory.getAggregateService().getAggrLastTime(selectedTrxId, aggregateTimeFrame);
        Date actualLastAggrTime = null;
        if (!history) {
            String aggid = "";
            Date actualNow = new Date();
            String sdnow = "";
            if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T5M)) {
                Map<String, String> map = DateUtils.DateToTimeFrTimeTo5m(actualNow);
                sdnow = map.get(DataConstant.PARAM_PERIOD_FROM) + " " + map.get(DataConstant.PARAM_TIME_TO);
                aggid = DataConstant.T5M;
            } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T15M)) {
                Map<String, String> map = DateUtils.DateToTimeFrTimeTo15m(actualNow);
                sdnow = map.get(DataConstant.PARAM_PERIOD_FROM) + " " + map.get(DataConstant.PARAM_TIME_TO);
                aggid = DataConstant.T15M;
            } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T30M)) {
                Map<String, String> map = DateUtils.DateToTimeFrTimeTo30m(actualNow);
                sdnow = map.get(DataConstant.PARAM_PERIOD_FROM) + " " + map.get(DataConstant.PARAM_TIME_TO);
                aggid = DataConstant.T30M;
            } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T1H)) {
                Map<String, String> map = DateUtils.DateToTimeFrTimeTo1h(actualNow);
                sdnow = map.get(DataConstant.PARAM_PERIOD_FROM) + " " + map.get(DataConstant.PARAM_TIME_TO);
                aggid = DataConstant.T1H;
            } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T1D)) {
                Map<String, String> map = DateUtils.DateToTimeFrTimeTo1d(actualNow);
                sdnow = map.get(DataConstant.PARAM_PERIOD_FROM) + " " + map.get(DataConstant.PARAM_TIME_TO);
                aggid = DataConstant.T1D;
            }
            actualLastAggrTime = DateUtils.convertDate(s, DateUtils.DATE_YYMMDD_HHMM);

            //thdate = DateUtils.convertDate(s, DateUtils.DATE_YYMMDD_HHMM);
            thdate = DateUtils.convertDate(sdnow, DateUtils.DATE_YYMMDD_HHMM);

            if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T1D)) {
                log.info(thdate);
                thdate = DateUtils.addDate(Calendar.DAY_OF_MONTH, thdate, -1);
            }
            frdate = DateUtils.addDate(Calendar.HOUR_OF_DAY, thdate, defaultRangeInHour*-1);

            selectedStartDate = frdate;
            selectedStartHour = DateUtils.getHour(frdate);
            selectedStartMinute = DateUtils.getMinute(frdate);

            selectedEndDate = thdate;
            selectedEndHour = DateUtils.getHour(thdate);
            selectedEndMinute = DateUtils.getMinute(thdate);

            //get aggr last_time_rnd
            String slastTimeRnd = moduleFactory.getAggregateService().getAggrLastTime(selectedTrxId, aggid);
            Date lastTimeRnd = DateUtils.convertDate(slastTimeRnd, DateUtils.DATE_YYMMDD_HHMM);
            if (lastTimeRnd.getTime() < thdate.getTime()) {
                log.info("mundur=====");
                log.info(lastTimeRnd);
                log.info(frdate + " " + thdate);
                log.info(lastTimeRnd.getTime() + " " + thdate.getTime());
                thdate = lastTimeRnd;
                long diff = thdate.getTime() - lastTimeRnd.getTime();
                diff = diff / 1000;
                log.info(diff);
                //mundurkan start date
                frdate = DateUtils.addDate(Calendar.SECOND, frdate, (int)(diff * -1));
                log.info(frdate + " " + thdate);
                log.info("\r\n");
            }

        } else {
            selectedEndDate = DateUtils.setHour(selectedEndDate, 0);
            selectedEndDate = DateUtils.setMinute(selectedEndDate, 0);
            selectedEndDate = DateUtils.setSecond(selectedEndDate, 0);
            selectedEndDate = DateUtils.setHour(selectedEndDate, selectedEndHour);
            selectedEndDate = DateUtils.setMinute(selectedEndDate, selectedEndMinute);
            thdate = selectedEndDate;

            selectedStartDate = DateUtils.setHour(selectedStartDate, 0);
            selectedStartDate = DateUtils.setMinute(selectedStartDate, 0);
            selectedStartDate = DateUtils.setSecond(selectedStartDate, 0);
            selectedStartDate = DateUtils.setHour(selectedStartDate, selectedStartHour);
            selectedStartDate = DateUtils.setMinute(selectedStartDate, selectedStartMinute);
            frdate = selectedStartDate;

            Date lastthdate = DateUtils.convertDate(s, DateUtils.DATE_YYMMDD_HHMM);
            if (lastthdate.getTime() < thdate.getTime()) {
                selectedEndDate = lastthdate;
                thdate = selectedEndDate;
                selectedEndHour = DateUtils.getHour(thdate);
                selectedEndMinute = DateUtils.getMinute(thdate);
            }



        }

        //selectedPrevDate = DateUtils.addDate(Calendar.DAY_OF_MONTH, thdate, -1);
        int dt = (int)DateUtils.getDifferenceDays(selectedPrevDate, thdate);
        if (dt < 0) {
            selectedPrevDate = DateUtils.addDate(Calendar.DAY_OF_MONTH, thdate, -1);
            dt = (int)DateUtils.getDifferenceDays(selectedPrevDate, thdate);
        }
        prevDay = Math.abs(dt);


        list = listDataTrx(actualLastAggrTime, frdate, thdate, selectedBankId, selectedTrxId, prevDay);
        //listPrev = listDataTrx(prevfrdate, prevthdate, selectedBankId, selectedTrxId);




        List<EsqTrx5MinBean> listAcqOnly = new ArrayList<>();
        List<EsqTrx5MinBean> listIssOnly = new ArrayList<>();
        List<EsqTrx5MinBean> listBnfOnly = new ArrayList<>();
        List<EsqTrx5MinBean> listAcqIss = new ArrayList<>();
        List<EsqTrx5MinBean> listAcqBnf = new ArrayList<>();
        List<EsqTrx5MinBean> listIssBnf = new ArrayList<>();

        List<String> listDateStringAcqOnly = new ArrayList<>();
        List<Integer> listDataTotalTransaksiAcqOnly = new ArrayList<>();
        List<Integer> listDataTotalApproveAcqOnly = new ArrayList<>();
        List<Integer> listDataTotalDeclineChargeAcqOnly = new ArrayList<>();
        List<Integer> listDataTotalDeclineFreeAcqOnly = new ArrayList<>();
        List<Integer> listDataTotalTransaksiAcqOnlyPrev = new ArrayList<>();
        List<Integer> listDataTotalApproveAcqOnlyPrev = new ArrayList<>();
        List<Integer> listDataTotalDeclineChargeAcqOnlyPrev = new ArrayList<>();
        List<Integer> listDataTotalDeclineFreeAcqOnlyPrev = new ArrayList<>();

        List<String> listDateStringIssOnly = new ArrayList<>();
        List<Integer> listDataTotalTransaksiIssOnly = new ArrayList<>();
        List<Integer> listDataTotalApproveIssOnly = new ArrayList<>();
        List<Integer> listDataTotalDeclineChargeIssOnly = new ArrayList<>();
        List<Integer> listDataTotalDeclineFreeIssOnly = new ArrayList<>();
        List<Integer> listDataTotalTransaksiIssOnlyPrev = new ArrayList<>();
        List<Integer> listDataTotalApproveIssOnlyPrev = new ArrayList<>();
        List<Integer> listDataTotalDeclineChargeIssOnlyPrev = new ArrayList<>();
        List<Integer> listDataTotalDeclineFreeIssOnlyPrev = new ArrayList<>();

        List<String> listDateStringBnfOnly = new ArrayList<>();
        List<Integer> listDataTotalTransaksiBnfOnly = new ArrayList<>();
        List<Integer> listDataTotalApproveBnfOnly = new ArrayList<>();
        List<Integer> listDataTotalDeclineChargeBnfOnly = new ArrayList<>();
        List<Integer> listDataTotalDeclineFreeBnfOnly = new ArrayList<>();
        List<Integer> listDataTotalTransaksiBnfOnlyPrev = new ArrayList<>();
        List<Integer> listDataTotalApproveBnfOnlyPrev = new ArrayList<>();
        List<Integer> listDataTotalDeclineChargeBnfOnlyPrev = new ArrayList<>();
        List<Integer> listDataTotalDeclineFreeBnfOnlyPrev = new ArrayList<>();

        List<String> listDateStringAcqIss = new ArrayList<>();
        List<Integer> listDataTotalTransaksiAcqIss = new ArrayList<>();
        List<Integer> listDataTotalApproveAcqIss = new ArrayList<>();
        List<Integer> listDataTotalDeclineChargeAcqIss = new ArrayList<>();
        List<Integer> listDataTotalDeclineFreeAcqIss = new ArrayList<>();
        List<Integer> listDataTotalTransaksiAcqIssPrev = new ArrayList<>();
        List<Integer> listDataTotalApproveAcqIssPrev = new ArrayList<>();
        List<Integer> listDataTotalDeclineChargeAcqIssPrev = new ArrayList<>();
        List<Integer> listDataTotalDeclineFreeAcqIssPrev = new ArrayList<>();

        List<String> listDateStringAcqBnf = new ArrayList<>();
        List<Integer> listDataTotalTransaksiAcqBnf = new ArrayList<>();
        List<Integer> listDataTotalApproveAcqBnf = new ArrayList<>();
        List<Integer> listDataTotalDeclineChargeAcqBnf = new ArrayList<>();
        List<Integer> listDataTotalDeclineFreeAcqBnf = new ArrayList<>();
        List<Integer> listDataTotalTransaksiAcqBnfPrev = new ArrayList<>();
        List<Integer> listDataTotalApproveAcqBnfPrev = new ArrayList<>();
        List<Integer> listDataTotalDeclineChargeAcqBnfPrev = new ArrayList<>();
        List<Integer> listDataTotalDeclineFreeAcqBnfPrev = new ArrayList<>();

        List<String> listDateStringIssBnf = new ArrayList<>();
        List<Integer> listDataTotalTransaksiIssBnf = new ArrayList<>();
        List<Integer> listDataTotalApproveIssBnf = new ArrayList<>();
        List<Integer> listDataTotalDeclineChargeIssBnf = new ArrayList<>();
        List<Integer> listDataTotalDeclineFreeIssBnf = new ArrayList<>();
        List<Integer> listDataTotalTransaksiIssBnfPrev = new ArrayList<>();
        List<Integer> listDataTotalApproveIssBnfPrev = new ArrayList<>();
        List<Integer> listDataTotalDeclineChargeIssBnfPrev = new ArrayList<>();
        List<Integer> listDataTotalDeclineFreeIssBnfPrev = new ArrayList<>();



        List<BigDecimal> listDataPercentApprAcqOnly = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclCAcqOnly = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclFAcqOnly = new ArrayList<>();

        List<BigDecimal> listDataPercentApprIssOnly = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclCIssOnly = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclFIssOnly = new ArrayList<>();

        List<BigDecimal> listDataPercentApprBnfOnly = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclCBnfOnly = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclFBnfOnly = new ArrayList<>();

        List<BigDecimal> listDataPercentApprAcqIss = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclCAcqIss = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclFAcqIss = new ArrayList<>();

        List<BigDecimal> listDataPercentApprAcqBnf = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclCAcqBnf = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclFAcqBnf = new ArrayList<>();

        List<BigDecimal> listDataPercentApprIssBnf = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclCIssBnf = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclFIssBnf = new ArrayList<>();

        //prev
        List<BigDecimal> listDataPercentApprAcqOnlyPrev = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclCAcqOnlyPrev = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclFAcqOnlyPrev = new ArrayList<>();

        List<BigDecimal> listDataPercentApprIssOnlyPrev = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclCIssOnlyPrev = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclFIssOnlyPrev = new ArrayList<>();

        List<BigDecimal> listDataPercentApprBnfOnlyPrev = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclCBnfOnlyPrev = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclFBnfOnlyPrev = new ArrayList<>();

        List<BigDecimal> listDataPercentApprAcqIssPrev = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclCAcqIssPrev = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclFAcqIssPrev = new ArrayList<>();

        List<BigDecimal> listDataPercentApprAcqBnfPrev = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclCAcqBnfPrev = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclFAcqBnfPrev = new ArrayList<>();

        List<BigDecimal> listDataPercentApprIssBnfPrev = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclCIssBnfPrev = new ArrayList<>();
        List<BigDecimal> listDataPercentDeclFIssBnfPrev = new ArrayList<>();

        //boolean toggle = true;
        for(int i=0; i < list.size(); i++) {
            EsqTrx5MinBean esq = list.get(i);

            Date d = DateUtils.convertDate(esq.getPeriod() + " " + esq.getTimeto(), DateUtils.DATE_YYMMDD_HHMM);
            if (timeFrame == TIMEFRAME_1D) {
                listDateStringAcqOnly.add(DateUtils.convertDate(d, DateUtils.DATE_DDMMM));
                listDateStringIssOnly.add(DateUtils.convertDate(d, DateUtils.DATE_DDMMM));
                listDateStringBnfOnly.add(DateUtils.convertDate(d, DateUtils.DATE_DDMMM));
                listDateStringAcqIss.add(DateUtils.convertDate(d, DateUtils.DATE_DDMMM));
                listDateStringAcqBnf.add(DateUtils.convertDate(d, DateUtils.DATE_DDMMM));
                listDateStringIssBnf.add(DateUtils.convertDate(d, DateUtils.DATE_DDMMM));
            } else {
                listDateStringAcqOnly.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringIssOnly.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringBnfOnly.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringAcqIss.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringAcqBnf.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
                listDateStringIssBnf.add(DateUtils.convertDate(d, DateUtils.DATE_YYYYMMDD2_HHMM));
            }

            for(String trxindid : selectedTransactionIndicatorId) {
                //log.info(trxindid);
                if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ONLY)) {
                    listAcqOnly.add(esq);
                    BigDecimal b = MathUtils.checkNull(esq.getFreq_acq_app())
                            .add(MathUtils.checkNull(esq.getFreq_acq_dc()))
                            .add(MathUtils.checkNull(esq.getFreq_acq_df()));

                    BigDecimal bprev = MathUtils.checkNull(esq.getFreq_acq_app_prev())
                            .add(MathUtils.checkNull(esq.getFreq_acq_dc_prev()))
                            .add(MathUtils.checkNull(esq.getFreq_acq_df_prev()));

                    BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acq_app()), b);
                    BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acq_dc(), b);
                    BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acq_df(), b);

                    BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acq_app_prev()), bprev);
                    BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_acq_dc_prev(), bprev);
                    BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_acq_df_prev(), bprev);

                    listDataTotalTransaksiAcqOnly.add(b.intValue());
                    listDataTotalApproveAcqOnly.add(MathUtils.checkNull(esq.getFreq_acq_app()).intValue());
                    listDataTotalDeclineChargeAcqOnly.add(MathUtils.checkNull(esq.getFreq_acq_dc()).intValue());
                    listDataTotalDeclineFreeAcqOnly.add(MathUtils.checkNull(esq.getFreq_acq_df()).intValue());
                    listDataPercentApprAcqOnly.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclCAcqOnly.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclFAcqOnly.add(pctDeclF.multiply(new BigDecimal("100")));

                    listDataTotalTransaksiAcqOnlyPrev.add(bprev.intValue());
                    listDataTotalApproveAcqOnlyPrev.add(MathUtils.checkNull(esq.getFreq_acq_app_prev()).intValue());
                    listDataTotalDeclineChargeAcqOnlyPrev.add(MathUtils.checkNull(esq.getFreq_acq_dc_prev()).intValue());
                    listDataTotalDeclineFreeAcqOnlyPrev.add(MathUtils.checkNull(esq.getFreq_acq_df_prev()).intValue());

                    listDataPercentApprAcqOnlyPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclCAcqOnlyPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclFAcqOnlyPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));
                }

                if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_ONLY)) {
                    listIssOnly.add(esq);
                    BigDecimal b = MathUtils.checkNull(esq.getFreq_iss_app())
                            .add(MathUtils.checkNull(esq.getFreq_iss_dc()))
                            .add(MathUtils.checkNull(esq.getFreq_iss_df()));

                    BigDecimal bprev = MathUtils.checkNull(esq.getFreq_iss_app_prev())
                            .add(MathUtils.checkNull(esq.getFreq_iss_dc_prev()))
                            .add(MathUtils.checkNull(esq.getFreq_iss_df_prev()));

                    BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_iss_app()), b);
                    BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_iss_dc(), b);
                    BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_iss_df(), b);

                    BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_iss_app_prev()), bprev);
                    BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_iss_dc_prev(), bprev);
                    BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_iss_df_prev(), bprev);

                    listDataTotalTransaksiIssOnly.add(b.intValue());
                    listDataTotalApproveIssOnly.add(MathUtils.checkNull(esq.getFreq_iss_app()).intValue());
                    listDataTotalDeclineChargeIssOnly.add(MathUtils.checkNull(esq.getFreq_iss_dc()).intValue());
                    listDataTotalDeclineFreeIssOnly.add(MathUtils.checkNull(esq.getFreq_iss_df()).intValue());
                    listDataPercentApprIssOnly.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclCIssOnly.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclFIssOnly.add(pctDeclF.multiply(new BigDecimal("100")));

                    listDataTotalTransaksiIssOnlyPrev.add(bprev.intValue());
                    listDataTotalApproveIssOnlyPrev.add(MathUtils.checkNull(esq.getFreq_iss_app_prev()).intValue());
                    listDataTotalDeclineChargeIssOnlyPrev.add(MathUtils.checkNull(esq.getFreq_iss_dc_prev()).intValue());
                    listDataTotalDeclineFreeIssOnlyPrev.add(MathUtils.checkNull(esq.getFreq_iss_df_prev()).intValue());

                    listDataPercentApprIssOnlyPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclCIssOnlyPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclFIssOnlyPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));
                }

                if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__BNF_ONLY)) {
                    listBnfOnly.add(esq);
                    BigDecimal b = MathUtils.checkNull(esq.getFreq_bnf_app())
                            .add(MathUtils.checkNull(esq.getFreq_bnf_dc()))
                            .add(MathUtils.checkNull(esq.getFreq_bnf_df()));

                    BigDecimal bprev = MathUtils.checkNull(esq.getFreq_bnf_app_prev())
                            .add(MathUtils.checkNull(esq.getFreq_bnf_dc_prev()))
                            .add(MathUtils.checkNull(esq.getFreq_bnf_df_prev()));

                    BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_bnf_app()), b);
                    BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_bnf_dc(), b);
                    BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_bnf_df(), b);

                    BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_bnf_app_prev()), bprev);
                    BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_bnf_dc_prev(), bprev);
                    BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_bnf_df_prev(), bprev);

                    listDataTotalTransaksiBnfOnly.add(b.intValue());
                    listDataTotalApproveBnfOnly.add(MathUtils.checkNull(esq.getFreq_bnf_app()).intValue());
                    listDataTotalDeclineChargeBnfOnly.add(MathUtils.checkNull(esq.getFreq_bnf_dc()).intValue());
                    listDataTotalDeclineFreeBnfOnly.add(MathUtils.checkNull(esq.getFreq_bnf_df()).intValue());
                    listDataPercentApprBnfOnly.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclCBnfOnly.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclFBnfOnly.add(pctDeclF.multiply(new BigDecimal("100")));

                    listDataTotalTransaksiBnfOnlyPrev.add(bprev.intValue());
                    listDataTotalApproveBnfOnlyPrev.add(MathUtils.checkNull(esq.getFreq_bnf_app_prev()).intValue());
                    listDataTotalDeclineChargeBnfOnlyPrev.add(MathUtils.checkNull(esq.getFreq_bnf_dc_prev()).intValue());
                    listDataTotalDeclineFreeBnfOnlyPrev.add(MathUtils.checkNull(esq.getFreq_bnf_df_prev()).intValue());

                    listDataPercentApprBnfOnlyPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclCBnfOnlyPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclFBnfOnlyPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));
                }

                if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_ISS)) {
                    listAcqIss.add(esq);
                    BigDecimal b = MathUtils.checkNull(esq.getFreq_acqIss_app())
                            .add(MathUtils.checkNull(esq.getFreq_acqIss_dc()))
                            .add(MathUtils.checkNull(esq.getFreq_acqIss_df()));

                    BigDecimal bprev = MathUtils.checkNull(esq.getFreq_acqIss_app_prev())
                            .add(MathUtils.checkNull(esq.getFreq_acqIss_dc_prev()))
                            .add(MathUtils.checkNull(esq.getFreq_acqIss_df_prev()));

                    BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqIss_app()), b);
                    BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acqIss_dc(), b);
                    BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acqIss_df(), b);

                    BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqIss_app_prev()), bprev);
                    BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_acqIss_dc_prev(), bprev);
                    BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_acqIss_df_prev(), bprev);

                    listDataTotalTransaksiAcqIss.add(b.intValue());
                    listDataTotalApproveAcqIss.add(MathUtils.checkNull(esq.getFreq_acqIss_app()).intValue());
                    listDataTotalDeclineChargeAcqIss.add(MathUtils.checkNull(esq.getFreq_acqIss_dc()).intValue());
                    listDataTotalDeclineFreeAcqIss.add(MathUtils.checkNull(esq.getFreq_acqIss_df()).intValue());
                    listDataPercentApprAcqIss.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclCAcqIss.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclFAcqIss.add(pctDeclF.multiply(new BigDecimal("100")));

                    listDataTotalTransaksiAcqIssPrev.add(bprev.intValue());
                    listDataTotalApproveAcqIssPrev.add(MathUtils.checkNull(esq.getFreq_acqIss_app_prev()).intValue());
                    listDataTotalDeclineChargeAcqIssPrev.add(MathUtils.checkNull(esq.getFreq_acqIss_dc_prev()).intValue());
                    listDataTotalDeclineFreeAcqIssPrev.add(MathUtils.checkNull(esq.getFreq_acqIss_df_prev()).intValue());

                    listDataPercentApprAcqIssPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclCAcqIssPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclFAcqIssPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));
                }

                if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ACQ_BNF)) {
                    listAcqBnf.add(esq);
                    BigDecimal b = MathUtils.checkNull(esq.getFreq_acqBnf_app())
                            .add(MathUtils.checkNull(esq.getFreq_acqBnf_dc()))
                            .add(MathUtils.checkNull(esq.getFreq_acqBnf_df()));

                    BigDecimal bprev = MathUtils.checkNull(esq.getFreq_acqBnf_app_prev())
                            .add(MathUtils.checkNull(esq.getFreq_acqBnf_dc_prev()))
                            .add(MathUtils.checkNull(esq.getFreq_acqBnf_df_prev()));

                    BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqBnf_app()), b);
                    BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_dc(), b);
                    BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_df(), b);

                    BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_acqBnf_app_prev()), bprev);
                    BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_dc_prev(), bprev);
                    BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_acqBnf_df_prev(), bprev);

                    listDataTotalTransaksiAcqBnf.add(b.intValue());
                    listDataTotalApproveAcqBnf.add(MathUtils.checkNull(esq.getFreq_acqBnf_app()).intValue());
                    listDataTotalDeclineChargeAcqBnf.add(MathUtils.checkNull(esq.getFreq_acqBnf_dc()).intValue());
                    listDataTotalDeclineFreeAcqBnf.add(MathUtils.checkNull(esq.getFreq_acqBnf_df()).intValue());
                    listDataPercentApprAcqBnf.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclCAcqBnf.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclFAcqBnf.add(pctDeclF.multiply(new BigDecimal("100")));

                    listDataTotalTransaksiAcqBnfPrev.add(bprev.intValue());
                    listDataTotalApproveAcqBnfPrev.add(MathUtils.checkNull(esq.getFreq_acqBnf_app_prev()).intValue());
                    listDataTotalDeclineChargeAcqBnfPrev.add(MathUtils.checkNull(esq.getFreq_acqBnf_dc_prev()).intValue());
                    listDataTotalDeclineFreeAcqBnfPrev.add(MathUtils.checkNull(esq.getFreq_acqBnf_df_prev()).intValue());

                    listDataPercentApprAcqBnfPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclCAcqBnfPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclFAcqBnfPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));
                }

                if (trxindid.equalsIgnoreCase(DataConstant.TRX_INDICATOR__ISS_BNF)) {
                    listIssBnf.add(esq);
                    BigDecimal b = MathUtils.checkNull(esq.getFreq_issBnf_app())
                            .add(MathUtils.checkNull(esq.getFreq_issBnf_dc()))
                            .add(MathUtils.checkNull(esq.getFreq_issBnf_df()));

                    BigDecimal bprev = MathUtils.checkNull(esq.getFreq_issBnf_app_prev())
                            .add(MathUtils.checkNull(esq.getFreq_issBnf_dc_prev()))
                            .add(MathUtils.checkNull(esq.getFreq_issBnf_df_prev()));

                    BigDecimal pctAppr = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_issBnf_app()), b);
                    BigDecimal pctDeclC = MathUtils.calcPercentComparison(esq.getFreq_issBnf_dc(), b);
                    BigDecimal pctDeclF = MathUtils.calcPercentComparison(esq.getFreq_issBnf_df(), b);

                    BigDecimal pctApprPrev = MathUtils.calcPercentComparison(MathUtils.checkNull(esq.getFreq_issBnf_app_prev()), bprev);
                    BigDecimal pctDeclCPrev = MathUtils.calcPercentComparison(esq.getFreq_issBnf_dc_prev(), bprev);
                    BigDecimal pctDeclFPrev = MathUtils.calcPercentComparison(esq.getFreq_issBnf_df_prev(), bprev);

                    listDataTotalTransaksiIssBnf.add(b.intValue());
                    listDataTotalApproveIssBnf.add(MathUtils.checkNull(esq.getFreq_issBnf_app()).intValue());
                    listDataTotalDeclineChargeIssBnf.add(MathUtils.checkNull(esq.getFreq_issBnf_dc()).intValue());
                    listDataTotalDeclineFreeIssBnf.add(MathUtils.checkNull(esq.getFreq_issBnf_df()).intValue());
                    listDataPercentApprIssBnf.add(pctAppr.multiply(new BigDecimal("100")));
                    listDataPercentDeclCIssBnf.add(pctDeclC.multiply(new BigDecimal("100")));
                    listDataPercentDeclFIssBnf.add(pctDeclF.multiply(new BigDecimal("100")));

                    listDataTotalTransaksiIssBnfPrev.add(bprev.intValue());
                    listDataTotalApproveIssBnfPrev.add(MathUtils.checkNull(esq.getFreq_issBnf_app_prev()).intValue());
                    listDataTotalDeclineChargeIssBnfPrev.add(MathUtils.checkNull(esq.getFreq_issBnf_dc_prev()).intValue());
                    listDataTotalDeclineFreeIssBnfPrev.add(MathUtils.checkNull(esq.getFreq_issBnf_df_prev()).intValue());

                    listDataPercentApprIssBnfPrev.add(pctApprPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclCIssBnfPrev.add(pctDeclCPrev.multiply(new BigDecimal("100")));
                    listDataPercentDeclFIssBnfPrev.add(pctDeclFPrev.multiply(new BigDecimal("100")));
                }
            }
            //toggle = !toggle;
        }



        String title = selectedBankName + " - " + selectedTrxName + " - " + selectedTransactionIndicatorName + " - 5 Min";


        String colorTotal = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_TOTAL);
        String colorApprove = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_APPR);
        String colorDc = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_DC);
        String colorDf = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_DF);
        String colorTotalPrev = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_TOTAL_PREV);
        String colorApprovePrev = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_APPR_PREV);
        String colorDcPrev = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_DC_PREV);
        String colorDfPrev = moduleFactory.getCommonService().getSystemPropertyUser(getUserInfoBean().getUserId(),
                DataConstant.COLOR_TX_DF_PREV);

        if (!showPercent) {
            c3ChartBeanAcqOnly = new C3ChartBean();
            generateChart(listDataTotalTransaksiAcqOnly,
                    listDataTotalApproveAcqOnly,
                    listDataTotalDeclineChargeAcqOnly,
                    listDataTotalDeclineFreeAcqOnly,
                    listDataTotalTransaksiAcqOnlyPrev,
                    listDataTotalApproveAcqOnlyPrev,
                    listDataTotalDeclineChargeAcqOnlyPrev,
                    listDataTotalDeclineFreeAcqOnlyPrev,
                    listDateStringAcqOnly,
                    colorTotal, colorApprove, colorDc, colorDf,
                    colorTotalPrev, colorApprovePrev, colorDcPrev, colorDfPrev,
                    title,
                    DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                    DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                    c3ChartBeanAcqOnly);

            c3ChartBeanIssOnly = new C3ChartBean();
            generateChart(listDataTotalTransaksiIssOnly,
                    listDataTotalApproveIssOnly,
                    listDataTotalDeclineChargeIssOnly,
                    listDataTotalDeclineFreeIssOnly,
                    listDataTotalTransaksiIssOnlyPrev,
                    listDataTotalApproveIssOnlyPrev,
                    listDataTotalDeclineChargeIssOnlyPrev,
                    listDataTotalDeclineFreeIssOnlyPrev,
                    listDateStringIssOnly,
                    colorTotal, colorApprove, colorDc, colorDf,
                    colorTotalPrev, colorApprovePrev, colorDcPrev, colorDfPrev,
                    title,
                    DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                    DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                    c3ChartBeanIssOnly);

            c3ChartBeanBnfOnly = new C3ChartBean();
            generateChart(listDataTotalTransaksiBnfOnly,
                    listDataTotalApproveBnfOnly,
                    listDataTotalDeclineChargeBnfOnly,
                    listDataTotalDeclineFreeBnfOnly,
                    listDataTotalTransaksiBnfOnlyPrev,
                    listDataTotalApproveBnfOnlyPrev,
                    listDataTotalDeclineChargeBnfOnlyPrev,
                    listDataTotalDeclineFreeBnfOnlyPrev,
                    listDateStringBnfOnly,
                    colorTotal, colorApprove, colorDc, colorDf,
                    colorTotalPrev, colorApprovePrev, colorDcPrev, colorDfPrev,
                    title,
                    DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                    DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                    c3ChartBeanBnfOnly);

            c3ChartBeanAcqIss = new C3ChartBean();
            generateChart(listDataTotalTransaksiAcqIss,
                    listDataTotalApproveAcqIss,
                    listDataTotalDeclineChargeAcqIss,
                    listDataTotalDeclineFreeAcqIss,
                    listDataTotalTransaksiAcqIssPrev,
                    listDataTotalApproveAcqIssPrev,
                    listDataTotalDeclineChargeAcqIssPrev,
                    listDataTotalDeclineFreeAcqIssPrev,
                    listDateStringAcqIss,
                    colorTotal, colorApprove, colorDc, colorDf,
                    colorTotalPrev, colorApprovePrev, colorDcPrev, colorDfPrev,
                    title,
                    DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                    DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                    c3ChartBeanAcqIss);

            c3ChartBeanAcqBnf = new C3ChartBean();
            generateChart(listDataTotalTransaksiAcqBnf,
                    listDataTotalApproveAcqBnf,
                    listDataTotalDeclineChargeAcqBnf,
                    listDataTotalDeclineFreeAcqBnf,
                    listDataTotalTransaksiAcqBnfPrev,
                    listDataTotalApproveAcqBnfPrev,
                    listDataTotalDeclineChargeAcqBnfPrev,
                    listDataTotalDeclineFreeAcqBnfPrev,
                    listDateStringAcqBnf,
                    colorTotal, colorApprove, colorDc, colorDf,
                    colorTotalPrev, colorApprovePrev, colorDcPrev, colorDfPrev,
                    title,
                    DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                    DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                    c3ChartBeanAcqBnf);

            c3ChartBeanIssBnf = new C3ChartBean();
            generateChart(listDataTotalTransaksiIssBnf,
                    listDataTotalApproveIssBnf,
                    listDataTotalDeclineChargeIssBnf,
                    listDataTotalDeclineFreeIssBnf,
                    listDataTotalTransaksiIssBnfPrev,
                    listDataTotalApproveIssBnfPrev,
                    listDataTotalDeclineChargeIssBnfPrev,
                    listDataTotalDeclineFreeIssBnfPrev,
                    listDateStringIssBnf,
                    colorTotal, colorApprove, colorDc, colorDf,
                    colorTotalPrev, colorApprovePrev, colorDcPrev, colorDfPrev,
                    title,
                    DataConstant.STRING_TOTAL_TRANSACTION, DataConstant.STRING_TOTAL_APPROVE,
                    DataConstant.STRING_TOTAL_DECLINE_CHARGE, DataConstant.STRING_TOTAL_DECLINE_FREE,
                    c3ChartBeanIssBnf);

        } else {
            List<DsBean> listDsBean = new ArrayList<>();

            DsBean dsBean = null;

            if (activePercentPrev[0]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprAcqOnly);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprAcqOnlyPrev);
                dsBean.setColor(colorApprovePrev);
                dsBean.setName("% Approval Prev");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[1]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCAcqOnly);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCAcqOnlyPrev);
                dsBean.setColor(colorDcPrev);
                dsBean.setName("% Decline Charge Prev ");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[2]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFAcqOnly);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFAcqOnlyPrev);
                dsBean.setColor(colorDfPrev);
                dsBean.setName("% Decline Free Prev ");
                listDsBean.add(dsBean);
            }

            c3ChartBeanAcqOnly = new C3ChartBean();
            generateChartMultiData(c3ChartBeanAcqOnly, listDsBean, listDateStringAcqOnly, title);


            //iss only
            listDsBean = new ArrayList<>();
            if (activePercentPrev[0]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprIssOnly);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprIssOnlyPrev);
                dsBean.setColor(colorApprovePrev);
                dsBean.setName("% Approval Prev");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[1]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCIssOnly);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCIssOnlyPrev);
                dsBean.setColor(colorDcPrev);
                dsBean.setName("% Decline Charge Prev ");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[2]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFIssOnly);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFIssOnlyPrev);
                dsBean.setColor(colorDfPrev);
                dsBean.setName("% Decline Free Prev ");
                listDsBean.add(dsBean);
            }

            c3ChartBeanIssOnly = new C3ChartBean();
            generateChartMultiData(c3ChartBeanIssOnly, listDsBean, listDateStringIssOnly, title);


            //bnf only
            listDsBean = new ArrayList<>();
            if (activePercentPrev[0]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprBnfOnly);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprBnfOnlyPrev);
                dsBean.setColor(colorApprovePrev);
                dsBean.setName("% Approval Prev");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[1]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCBnfOnly);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCBnfOnlyPrev);
                dsBean.setColor(colorDcPrev);
                dsBean.setName("% Decline Charge Prev ");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[2]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFBnfOnly);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFBnfOnlyPrev);
                dsBean.setColor(colorDfPrev);
                dsBean.setName("% Decline Free Prev ");
                listDsBean.add(dsBean);
            }

            c3ChartBeanBnfOnly = new C3ChartBean();
            generateChartMultiData(c3ChartBeanBnfOnly, listDsBean, listDateStringBnfOnly, title);


            //acq iss
            listDsBean = new ArrayList<>();
            if (activePercentPrev[0]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprAcqIss);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprAcqIssPrev);
                dsBean.setColor(colorApprovePrev);
                dsBean.setName("% Approval Prev");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[1]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCAcqIss);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCAcqIssPrev);
                dsBean.setColor(colorDcPrev);
                dsBean.setName("% Decline Charge Prev ");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[2]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFAcqIss);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFAcqIssPrev);
                dsBean.setColor(colorDfPrev);
                dsBean.setName("% Decline Free Prev ");
                listDsBean.add(dsBean);
            }

            c3ChartBeanAcqIss = new C3ChartBean();
            generateChartMultiData(c3ChartBeanAcqIss, listDsBean, listDateStringAcqIss, title);



            //acq bnf
            listDsBean = new ArrayList<>();
            if (activePercentPrev[0]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprAcqBnf);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprAcqBnfPrev);
                dsBean.setColor(colorApprovePrev);
                dsBean.setName("% Approval Prev");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[1]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCAcqBnf);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCAcqBnfPrev);
                dsBean.setColor(colorDcPrev);
                dsBean.setName("% Decline Charge Prev ");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[2]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFAcqBnf);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFAcqBnfPrev);
                dsBean.setColor(colorDfPrev);
                dsBean.setName("% Decline Free Prev ");
                listDsBean.add(dsBean);
            }

            c3ChartBeanAcqBnf = new C3ChartBean();
            generateChartMultiData(c3ChartBeanAcqBnf, listDsBean, listDateStringAcqBnf, title);


            //iss bnf
            listDsBean = new ArrayList<>();
            if (activePercentPrev[0]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprIssBnf);
                dsBean.setColor(colorApprove);
                dsBean.setName("% Approval");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentApprIssBnfPrev);
                dsBean.setColor(colorApprovePrev);
                dsBean.setName("% Approval Prev");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[1]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCIssBnf);
                dsBean.setColor(colorDc);
                dsBean.setName("% Decline Charge ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclCIssBnfPrev);
                dsBean.setColor(colorDcPrev);
                dsBean.setName("% Decline Charge Prev ");
                listDsBean.add(dsBean);
            }

            if (activePercentPrev[2]) {
                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFIssBnf);
                dsBean.setColor(colorDf);
                dsBean.setName("% Decline Free ");
                listDsBean.add(dsBean);

                dsBean = new DsBean();
                dsBean.setListData(listDataPercentDeclFIssBnfPrev);
                dsBean.setColor(colorDfPrev);
                dsBean.setName("% Decline Free Prev ");
                listDsBean.add(dsBean);
            }

            c3ChartBeanIssBnf = new C3ChartBean();
            generateChartMultiData(c3ChartBeanIssBnf, listDsBean, listDateStringIssBnf, title);


        }
    }

    protected List<EsqTrx5MinBean> listDataTrx(Date actualLastAggrTime, Date frdate, Date thdate, String bankId, String trxId, int prevDay) throws Exception {
        log.info("frdate = " + frdate);
        log.info("thdate = " + thdate);
        List<EsqTrx5MinBean> list = new ArrayList<>();
        Date sd = frdate;


        String tableName = "";
        String sminref = "";
        if (timeFrame == TIMEFRAME_5M) {
            sminref = "23:55";
            tableName = DataConstant.TABLE_TRX5MIN;
        } else if (timeFrame == TIMEFRAME_15M) {
            sminref = "23:45";
            tableName = DataConstant.TABLE_TRX15MIN;
        } else if (timeFrame == TIMEFRAME_30M) {
            sminref = "23:30";
            tableName = DataConstant.TABLE_TRX30MIN;
        } else if (timeFrame == TIMEFRAME_1H) {
            sminref = "23:00";
            tableName = DataConstant.TABLE_TRX1HOUR;
        } else if (timeFrame == TIMEFRAME_1D) {
            sminref = "00:00";
            tableName = DataConstant.TABLE_TRX1DAY;
        }



        //int idx = 0;
        while (true) {
            //if (idx == 287) {
            //log.info(idx);
            //}
            String period = DateUtils.convertDate(sd, DateUtils.DATE_YYMMDD);
            String periodPrev = DateUtils.convertDate(DateUtils.addDate(Calendar.DAY_OF_MONTH, sd, -1 * prevDay) , DateUtils.DATE_YYMMDD);
            Date ed = DateUtils.addDate(Calendar.MINUTE, sd, timeFrame);
            String sminfr = DateUtils.convertDate(sd, DateUtils.DATE_HHMM);
            String sminto = DateUtils.convertDate(ed, DateUtils.DATE_HHMM);

            if (sminfr.equalsIgnoreCase(sminref)) {
                sminto = "24:00";
            }

            EsqTrx5MinBean esqTrx5MinBean = null;
            if (sd.getTime() < actualLastAggrTime.getTime()) {
                esqTrx5MinBean = moduleFactory.getBiService().getEsqTrxWithPrev(
                        tableName, bankId, trxId,
                        period, periodPrev, sminfr, sminto);
                if (esqTrx5MinBean == null) {
                    esqTrx5MinBean = new EsqTrx5MinBean();
                    esqTrx5MinBean.setPeriod(period);
                    esqTrx5MinBean.setBankid(bankId);
                    esqTrx5MinBean.setTrxid(trxId);
                    esqTrx5MinBean.setTimefr(sminfr);
                    esqTrx5MinBean.setTimeto(sminto);

                }
            } else {
                esqTrx5MinBean = new EsqTrx5MinBean();
                esqTrx5MinBean.setPeriod(period);
                esqTrx5MinBean.setBankid(bankId);
                esqTrx5MinBean.setTrxid(trxId);
                esqTrx5MinBean.setTimefr(sminfr);
                esqTrx5MinBean.setTimeto(sminto);
            }
            list.add(esqTrx5MinBean);

            if (ed.getTime() >= thdate.getTime()) {
                break;
            }

            sd = DateUtils.addDate(Calendar.MINUTE, sd, timeFrame);
            //idx++;
        };

        return list;
    }

    public void trxIdChangeListener() {
        try {
            if (selectedTrxId.equalsIgnoreCase(DataConstant.ALL)) {
                listTrxIdIndicator = new ArrayList<>();
                selectedTransactionIndicatorId = new String[6];
                selectedTransactionIndicatorId[0] = "01";
                selectedTransactionIndicatorId[1] = "02";
                selectedTransactionIndicatorId[2] = "03";
                selectedTransactionIndicatorId[3] = "04";
                selectedTransactionIndicatorId[4] = "05";
                selectedTransactionIndicatorId[5] = "06";
                return;
            } else {
                listTrxIdIndicator = moduleFactory.getMasterService().listEsqTrxIdTrxIndicator(selectedTrxId);
            }
            //listTrxIdIndicator = moduleFactory.getMasterService().listEsqTrxIdIndicator();

            selectedTransactionIndicatorId = new String[listTrxIdIndicator.size()];
            for(int i = 0; i < listTrxIdIndicator.size(); i++) {
                EsqTrxIdBean id = listTrxIdIndicator.get(i);
                selectedTransactionIndicatorId[i] = id.getTrxidindicator();
            }

            //selectedTransactionIndicatorId = null;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    protected void generateChart(List<Integer> listDataTotalTransaksi,
                                 List<Integer> listDataTotalApprove,
                                 List<Integer> listDataTotalDeclineCharge,
                                 List<Integer> listDataTotalDeclineFree,
                                 List<Integer> listDataTotalTransaksiPrev,
                                 List<Integer> listDataTotalApprovePrev,
                                 List<Integer> listDataTotalDeclineChargePrev,
                                 List<Integer> listDataTotalDeclineFreePrev,
                                 List<String> listDateString,
                                 String colorTotal, String colorApprove,
                                 String colorDc, String colorDf,
                                 String colorTotalPrev, String colorApprovePrev,
                                 String colorDcPrev, String colorDfPrev,
                                 String title,
                                 String dsNameTotal, String dsNameAppr, String dsNameDc, String dsNameDf,
                                 C3ChartBean c3ChartBean) {
        C3DataSet dsTotal = new C3DataSet(listDataTotalTransaksi);
        C3ViewDataSet dataSetTotal = new C3ViewDataSet(dsNameTotal, dsTotal, "#"+colorTotal);

        C3DataSet dsAppr = new C3DataSet(listDataTotalApprove);
        C3ViewDataSet dataSetAppr = new C3ViewDataSet(dsNameAppr, dsAppr, "#"+colorApprove);

        C3DataSet dsDc = new C3DataSet(listDataTotalDeclineCharge);
        C3ViewDataSet dataSetDc = new C3ViewDataSet(dsNameDc, dsDc, "#"+colorDc);

        C3DataSet dsDf = new C3DataSet(listDataTotalDeclineFree);
        C3ViewDataSet dataSetDf = new C3ViewDataSet(dsNameDf, dsDf, "#"+colorDf);

        C3DataSet dsTotalPrev = new C3DataSet(listDataTotalTransaksiPrev);
        C3ViewDataSet dataSetTotalPrev = new C3ViewDataSet(dsNameTotal + " Prev", dsTotalPrev, "#"+colorTotalPrev);

        C3DataSet dsApprPrev = new C3DataSet(listDataTotalApprovePrev);
        C3ViewDataSet dataSetApprPrev = new C3ViewDataSet(dsNameAppr + " Prev", dsApprPrev, "#"+colorApprovePrev);

        C3DataSet dsDcPrev = new C3DataSet(listDataTotalDeclineChargePrev);
        C3ViewDataSet dataSetDcPrev = new C3ViewDataSet(dsNameDc + " Prev", dsDcPrev, "#"+colorDcPrev);

        C3DataSet dsDfPrev = new C3DataSet(listDataTotalDeclineFreePrev);
        C3ViewDataSet dataSetDfPrev = new C3ViewDataSet(dsNameDf + " Prev", dsDfPrev, "#"+colorDfPrev);

        C3DataSetString dsDateString = new C3DataSetString(listDateString);
        C3ViewDataSetString dataSetDateString = new C3ViewDataSetString("x", dsDateString, false);

        Data data = new Data();
        data.getDataSetsString().add(dataSetDateString);
        if (activePrev[0]) {
            data.getDataSets().add(dataSetTotal);
        }
        if (activePrev[1]) {
            data.getDataSets().add(dataSetAppr);
        }
        if (activePrev[2]) {
            data.getDataSets().add(dataSetDc);
        }
        if (activePrev[3]) {
            data.getDataSets().add(dataSetDf);
        }
        if (activePrev[0]) {
            data.getDataSets().add(dataSetTotalPrev);
        }
        if (activePrev[1]) {
            data.getDataSets().add(dataSetApprPrev);
        }
        if (activePrev[2]) {
            data.getDataSets().add(dataSetDcPrev);
        }
        if (activePrev[3]) {
            data.getDataSets().add(dataSetDfPrev);
        }

        data.setX("z1");
        //if (timeFrame == TIMEFRAME_1D) {
        //    data.setxFormat("%Y-%m-%d");
        //} else {
            data.setxFormat("%Y-%m-%d %H:%M");
        //}

        com.martinlinha.c3faces.script.property.Axis axis = new com.martinlinha.c3faces.script.property.Axis();
        axis.setHeightX(70);

        if (timeFrame == TIMEFRAME_1D) {
            axis.setTypeX(Axis.Type.CATEGORY);
            axis.setTickX("fit: true,\n" +
                    "format: '%d/%m',\n" +
                    "rotate: -45,\n" +
                    "multiline: false,\n" +
                    "culling: true");
            axis.setFormat("'%d/%m'");
        } else {
            axis.setTypeX(com.martinlinha.c3faces.script.property.Axis.Type.TIMESERIES);
            axis.setTickX("fit: true,\n" +
                    "format: '%d/%m %H:%M',\n" +
                    "rotate: -45,\n" +
                    "multiline: false,\n" +
                    "culling: true");
        }
        axis.setMinY(0);

        Point point = new Point();
        point.setShow(true);

        c3ChartBean.setData(data);
        c3ChartBean.setAxis(axis);
        c3ChartBean.setPoint(point);
        //c3ChartBean.setPadding(padding);
        c3ChartBean.setTitle(title);
    }

    protected void generateChartMultiData(C3ChartBean c3ChartBean, List<DsBean> listData, List<String> listDateString,
                                          String title) {
        List<C3ViewDataSet> listDataSet = new ArrayList<>();

        for(DsBean dsBean : listData) {
            C3DataSet ds = new C3DataSet(dsBean.getListData());
            C3ViewDataSet dataSet = new C3ViewDataSet(dsBean.getName(), ds, "#"+dsBean.getColor());
            listDataSet.add(dataSet);
        }

        C3DataSetString dsDateString = new C3DataSetString(listDateString);
        C3ViewDataSetString dataSetDateString = new C3ViewDataSetString("x", dsDateString, false);

        Data data = new Data();
        data.getDataSetsString().add(dataSetDateString);
        for(C3ViewDataSet ds : listDataSet) {
            data.getDataSets().add(ds);
        }

        data.setX("z1");
        //if (timeFrame == TIMEFRAME_1D) {
        //    data.setxFormat("%Y-%m-%d");
        //} else {
            data.setxFormat("%Y-%m-%d %H:%M");
        //}

        com.martinlinha.c3faces.script.property.Axis axis = new com.martinlinha.c3faces.script.property.Axis();
        axis.setHeightX(70);
        if (timeFrame == TIMEFRAME_1D) {
            axis.setTypeX(Axis.Type.CATEGORY);
            axis.setTickX("fit: true,\n" +
                    "format: '%d/%m',\n" +
                    "rotate: -45,\n" +
                    "multiline: false,\n" +
                    "culling: true");
            axis.setFormat("'%d/%m'");
        } else {
            axis.setTypeX(com.martinlinha.c3faces.script.property.Axis.Type.TIMESERIES);
            axis.setTickX("fit: true,\n" +
                    "format: '%d/%m %H:%M',\n" +
                    "rotate: -45,\n" +
                    "multiline: false,\n" +
                    "culling: true");
        }
        axis.setMinY(0);

        Point point = new Point();
        point.setShow(true);

        //Padding padding = new Padding();

        c3ChartBean.setData(data);
        c3ChartBean.setAxis(axis);
        c3ChartBean.setPoint(point);
        //c3ChartBean.setPadding(padding);
        c3ChartBean.setTitle(title);
    }

    public class DsBean {
        private List<? extends Number> listData;
        private String color;
        private String name;

        public List<? extends Number> getListData() {
            return listData;
        }

        public void setListData(List<? extends Number> listData) {
            this.listData = listData;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }


    public void listUserQuickView(String userId) throws Exception {
        listTmsUserQuickView = moduleFactory.getMasterService().listUserQuickview(userId);
        try {
            for (TmsUserQuickView uqv : listTmsUserQuickView) {
                if (uqv.getTmsUserQuickViewPk().getTrxId().equalsIgnoreCase(DataConstant.ALL)) {
                    uqv.setTrxName("ALL");
                } else {
                    EsqTrxIdBean trx = moduleFactory.getMasterService().getEsqTrxId(uqv.getTmsUserQuickViewPk().getTrxId());
                    uqv.setTrxName(trx.getTrxname());
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String getTimeFrameName() {
        return DataConstant.getTimeFrameName(aggregateTimeFrame);
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public String getSelectedTrxId() {
        return selectedTrxId;
    }

    public void setSelectedTrxId(String selectedTrxId) {
        this.selectedTrxId = selectedTrxId;
    }

    public List<EsqTrxIdBean> getListTrxIdIndicator() {
        return listTrxIdIndicator;
    }

    public void setListTrxIdIndicator(List<EsqTrxIdBean> listTrxIdIndicator) {
        this.listTrxIdIndicator = listTrxIdIndicator;
    }

    public String[] getSelectedTransactionIndicatorId() {
        return selectedTransactionIndicatorId;
    }

    public void setSelectedTransactionIndicatorId(String[] selectedTransactionIndicatorId) {
        this.selectedTransactionIndicatorId = selectedTransactionIndicatorId;
    }

    public String getSelectedBankId() {
        return selectedBankId;
    }

    public void setSelectedBankId(String selectedBankId) {
        this.selectedBankId = selectedBankId;
    }

    public String getSelectedBankName() {
        return selectedBankName;
    }

    public void setSelectedBankName(String selectedBankName) {
        this.selectedBankName = selectedBankName;
    }

    public String getSelectedTrxName() {
        return selectedTrxName;
    }

    public void setSelectedTrxName(String selectedTrxName) {
        this.selectedTrxName = selectedTrxName;
    }

    public String getSelectedTransactionIndicatorName() {
        return selectedTransactionIndicatorName;
    }

    public void setSelectedTransactionIndicatorName(String selectedTransactionIndicatorName) {
        this.selectedTransactionIndicatorName = selectedTransactionIndicatorName;
    }

    public String getRenderResult() {
        return renderResult;
    }

    public void setRenderResult(String renderResult) {
        this.renderResult = renderResult;
    }

    public List<EsqTrx5MinBean> getList() {
        return list;
    }

    public void setList(List<EsqTrx5MinBean> list) {
        this.list = list;
    }

    public int getDefaultRangeInHour() {
        return defaultRangeInHour;
    }

    public void setDefaultRangeInHour(int defaultRangeInHour) {
        this.defaultRangeInHour = defaultRangeInHour;
    }

    public Date getSelectedEndDate() {
        return selectedEndDate;
    }

    public void setSelectedEndDate(Date selectedEndDate) {
        this.selectedEndDate = selectedEndDate;
    }

    public Integer getSelectedEndHour() {
        return selectedEndHour;
    }

    public void setSelectedEndHour(Integer selectedEndHour) {
        this.selectedEndHour = selectedEndHour;
    }

    public Integer getSelectedEndMinute() {
        return selectedEndMinute;
    }

    public void setSelectedEndMinute(Integer selectedEndMinute) {
        this.selectedEndMinute = selectedEndMinute;
    }

    public Date getSelectedStartDate() {
        return selectedStartDate;
    }

    public void setSelectedStartDate(Date selectedStartDate) {
        this.selectedStartDate = selectedStartDate;
    }

    public Integer getSelectedStartHour() {
        return selectedStartHour;
    }

    public void setSelectedStartHour(Integer selectedStartHour) {
        this.selectedStartHour = selectedStartHour;
    }

    public Integer getSelectedStartMinute() {
        return selectedStartMinute;
    }

    public void setSelectedStartMinute(Integer selectedStartMinute) {
        this.selectedStartMinute = selectedStartMinute;
    }

    public boolean isShowPercent() {
        return showPercent;
    }

    public void setShowPercent(boolean showPercent) {
        this.showPercent = showPercent;
    }

    public String getRenderAcqOnly() {
        return renderAcqOnly;
    }

    public void setRenderAcqOnly(String renderAcqOnly) {
        this.renderAcqOnly = renderAcqOnly;
    }

    public String getRenderIssOnly() {
        return renderIssOnly;
    }

    public void setRenderIssOnly(String renderIssOnly) {
        this.renderIssOnly = renderIssOnly;
    }

    public String getRenderBnfOnly() {
        return renderBnfOnly;
    }

    public void setRenderBnfOnly(String renderBnfOnly) {
        this.renderBnfOnly = renderBnfOnly;
    }

    public String getRenderAcqIss() {
        return renderAcqIss;
    }

    public void setRenderAcqIss(String renderAcqIss) {
        this.renderAcqIss = renderAcqIss;
    }

    public String getRenderAcqBnf() {
        return renderAcqBnf;
    }

    public void setRenderAcqBnf(String renderAcqBnf) {
        this.renderAcqBnf = renderAcqBnf;
    }

    public String getRenderIssBnf() {
        return renderIssBnf;
    }

    public void setRenderIssBnf(String renderIssBnf) {
        this.renderIssBnf = renderIssBnf;
    }

    public C3ChartBean getC3ChartBeanAcqOnly() {
        return c3ChartBeanAcqOnly;
    }

    public void setC3ChartBeanAcqOnly(C3ChartBean c3ChartBeanAcqOnly) {
        this.c3ChartBeanAcqOnly = c3ChartBeanAcqOnly;
    }

    public C3ChartBean getC3ChartBeanIssOnly() {
        return c3ChartBeanIssOnly;
    }

    public void setC3ChartBeanIssOnly(C3ChartBean c3ChartBeanIssOnly) {
        this.c3ChartBeanIssOnly = c3ChartBeanIssOnly;
    }

    public C3ChartBean getC3ChartBeanBnfOnly() {
        return c3ChartBeanBnfOnly;
    }

    public void setC3ChartBeanBnfOnly(C3ChartBean c3ChartBeanBnfOnly) {
        this.c3ChartBeanBnfOnly = c3ChartBeanBnfOnly;
    }

    public C3ChartBean getC3ChartBeanAcqIss() {
        return c3ChartBeanAcqIss;
    }

    public void setC3ChartBeanAcqIss(C3ChartBean c3ChartBeanAcqIss) {
        this.c3ChartBeanAcqIss = c3ChartBeanAcqIss;
    }

    public C3ChartBean getC3ChartBeanAcqBnf() {
        return c3ChartBeanAcqBnf;
    }

    public void setC3ChartBeanAcqBnf(C3ChartBean c3ChartBeanAcqBnf) {
        this.c3ChartBeanAcqBnf = c3ChartBeanAcqBnf;
    }

    public C3ChartBean getC3ChartBeanIssBnf() {
        return c3ChartBeanIssBnf;
    }

    public void setC3ChartBeanIssBnf(C3ChartBean c3ChartBeanIssBnf) {
        this.c3ChartBeanIssBnf = c3ChartBeanIssBnf;
    }

    public String getAggregateTimeFrame() {
        return aggregateTimeFrame;
    }

    public void setAggregateTimeFrame(String aggregateTimeFrame) {
        this.aggregateTimeFrame = aggregateTimeFrame;
    }

    public int getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(int timeFrame) {
        this.timeFrame = timeFrame;
    }

    public boolean isHistory() {
        return history;
    }

    public void setHistory(boolean history) {
        this.history = history;
    }

    /*public Boolean[] getActiveCurr() {
        return activeCurr;
    }

    public void setActiveCurr(Boolean[] activeCurr) {
        this.activeCurr = activeCurr;
    }*/

    public Boolean[] getActivePrev() {
        return activePrev;
    }

    public void setActivePrev(Boolean[] activePrev) {
        this.activePrev = activePrev;
    }

    /*public boolean isCheckAllCurr() {
        return checkAllCurr;
    }

    public void setCheckAllCurr(boolean checkAllCurr) {
        this.checkAllCurr = checkAllCurr;
    }*/

    public boolean isCheckAllPrev() {
        return checkAllPrev;
    }

    public void setCheckAllPrev(boolean checkAllPrev) {
        this.checkAllPrev = checkAllPrev;
    }

    public Date getSelectedPrevDate() {
        return selectedPrevDate;
    }

    public void setSelectedPrevDate(Date selectedPrevDate) {
        this.selectedPrevDate = selectedPrevDate;
    }

    public String getRenderPrevDate() {
        return renderPrevDate;
    }

    public void setRenderPrevDate(String renderPrevDate) {
        this.renderPrevDate = renderPrevDate;
    }

    public Boolean[] getActivePercentPrev() {
        return activePercentPrev;
    }

    public void setActivePercentPrev(Boolean[] activePercentPrev) {
        this.activePercentPrev = activePercentPrev;
    }

    public List<TmsUserQuickView> getListTmsUserQuickView() {
        return listTmsUserQuickView;
    }

    public void setListTmsUserQuickView(List<TmsUserQuickView> listTmsUserQuickView) {
        this.listTmsUserQuickView = listTmsUserQuickView;
    }

    public String getSelectedUserQuickView() {
        return selectedUserQuickView;
    }

    public void setSelectedUserQuickView(String selectedUserQuickView) {
        this.selectedUserQuickView = selectedUserQuickView;
    }
}

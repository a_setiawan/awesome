package com.rintis.marketing.web.jsf.main.pan;

import com.google.common.collect.ComparisonChain;
import com.rintis.marketing.beans.bean.bi.DetailBeneficiaryAnomalyBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by ait on 7/05/2021.
 */
@ManagedBean
@ViewScoped
public class DetailBeneficiaryAnomalyInquiryManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<DetailBeneficiaryAnomalyBean> list;
    private String pPeriod;
    private String pTimeFr;
    private String pTimeTo;
    private String pPan;
    private String pReceiver;
    private String pIssuerBank;
    private String toacct;
    private String pBeneficiaryBank;
    private String pTranType;
    private String pTranTypeId;
    private String pGroupId;
    private String pStatus;
    private int colspan;
    private String totalAmt;
    private Boolean beneficiary;
    private Boolean sender;
    /*Add by aaw 13/07/2021, fixing detail beneficiary untuk 3D*/
    private String pPeriodTo;
    private String pPeriodType;
    /*End add*/

    @Override
    @PostConstruct
    protected void initManagedBean() {

        try {
            /*Add and modify by aaw 13/07/2021, fixing detail beneficiari for 3D*/
            pPeriodType = paramEncode(getRequestParameterMap().get("pPeriodType").toString());
            if ("3D".equalsIgnoreCase(pPeriodType)) {
                pPeriod = paramEncode(getRequestParameterMap().get("pPeriodFrom").toString());
            } else {
                pPeriod = paramEncode(getRequestParameterMap().get("pPeriod").toString());
            }
            pPeriodTo = paramEncode(getRequestParameterMap().get("pPeriodTo").toString());
            /*End*/
            pTimeFr = paramEncode(getRequestParameterMap().get("pTimeFr").toString().replace(":", ""));
            pTimeTo = paramEncode(getRequestParameterMap().get("pTimeTo").toString().replace(":", ""));
            pReceiver = paramEncode(getRequestParameterMap().get("pReceiver").toString());
            pIssuerBank = paramEncode(getRequestParameterMap().get("pIssuerBank").toString());
            pTranType = "Transfer Debet/Kredit";
            pStatus = paramEncode(getRequestParameterMap().get("pStatus").toString());
            pGroupId = moduleFactory.getPanService().getGroupIdFromTrxName(pTranType);
            pTranTypeId = moduleFactory.getPanService().getTrxIdFromTrxName(pTranType);
            pBeneficiaryBank = paramEncode(getRequestParameterMap().get("pBeneficiaryBank").toString());
            toacct = paramEncode(getRequestParameterMap().get("pToAcct").toString());

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            System.out.println("Beneficiary Anomaly Inquiry Detail - " + getUserInfoBean().getUserId() + " - ATM - " + pPeriod + " - " + pTimeFr + " - " + pTimeTo + " - " + toacct + " - " + pTranTypeId + " - " + pReceiver);
            List<DetailBeneficiaryAnomalyBean> def = new ArrayList<>();
            /*Modify by aaw 13/07/2021, fixing detail beneficiary for 3D*/
            def = moduleFactory.getPanService().listDetailBeneficiaryAtm(pPeriod, pTimeFr, pTimeTo, toacct, pTranTypeId, pReceiver, pPeriodTo, pPeriodType);
            /*End*/
            list = sortingListBene(def);

            beneficiary = true;
            colspan = 13;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public List<DetailBeneficiaryAnomalyBean> sortingListBene (List<DetailBeneficiaryAnomalyBean> in) {

        Collections.sort(in, new Comparator<DetailBeneficiaryAnomalyBean>() {
            @Override
            public int compare(DetailBeneficiaryAnomalyBean o1, DetailBeneficiaryAnomalyBean o2) {
                return ComparisonChain.start()
                    .compare(o1.getPeriod(), o2.getPeriod())
                    .compare(o1.getTrxTime(), o2.getTrxTime())
                    .result();
            }
        });
        System.out.println("Result Sort : " + in);
        return in;
    }

    public String paramEncode(String data){
        if(!data.equals(null)){
            data = HttpUtils.encodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }

    public String getValueAmtTotal() {
        Double totAmt = 0.00;

        if(list != null){
            for(DetailBeneficiaryAnomalyBean l : list) {
                totAmt += Double.parseDouble(l.getAmount().toString());
            }
        }

        DecimalFormat formatter = new DecimalFormat("###,###,###.00");
        totalAmt = formatter.format(totAmt);

        return totalAmt;
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<DetailBeneficiaryAnomalyBean> getList() {
        return list;
    }

    public void setList(List<DetailBeneficiaryAnomalyBean> list) {
        this.list = list;
    }

    public String getpPeriod() {
        return pPeriod;
    }

    public void setpPeriod(String pPeriod) {
        this.pPeriod = pPeriod;
    }

    public String getpTimeFr() {
        return pTimeFr;
    }

    public void setpTimeFr(String pTimeFr) {
        this.pTimeFr = pTimeFr;
    }

    public String getpTimeTo() {
        return pTimeTo;
    }

    public void setpTimeTo(String pTimeTo) {
        this.pTimeTo = pTimeTo;
    }

    public String getpPan() {
        return pPan;
    }

    public void setpPan(String pPan) {
        this.pPan = pPan;
    }

    public String getpIssuerBank() {
        return pIssuerBank;
    }

    public void setpIssuerBank(String pIssuerBank) {
        this.pIssuerBank = pIssuerBank;
    }

    public String getpTranType() {
        return pTranType;
    }

    public void setpTranType(String pTranType) {
        this.pTranType = pTranType;
    }

    public String getpTranTypeId() {
        return pTranTypeId;
    }

    public void setpTranTypeId(String pTranTypeId) {
        this.pTranTypeId = pTranTypeId;
    }

    public String getpGroupId() {
        return pGroupId;
    }

    public void setpGroupId(String pGroupId) {
        this.pGroupId = pGroupId;
    }

    public Boolean getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(Boolean beneficiary) {
        this.beneficiary = beneficiary;
    }

    public int getColspan() {
        return colspan;
    }

    public void setColspan(int colspan) {
        this.colspan = colspan;
    }

    public Boolean getSender() {
        return sender;
    }

    public void setSender(Boolean sender) {
        this.sender = sender;
    }

    public String getpStatus() {
        return pStatus;
    }

    public void setpStatus(String pStatus) {
        this.pStatus = pStatus;
    }

    public String getpBeneficiaryBank() {
        return pBeneficiaryBank;
    }

    public void setpBeneficiaryBank(String pBeneficiaryBank) {
        this.pBeneficiaryBank = pBeneficiaryBank;
    }

    public String getToacct() {
        return toacct;
    }

    public void setToacct(String toacct) {
        this.toacct = toacct;
    }

    public String getpReceiver() {
        return pReceiver;
    }

    public void setpReceiver(String pReceiver) {
        this.pReceiver = pReceiver;
    }

    public String getpPeriodTo() {
        return pPeriodTo;
    }

    public void setpPeriodTo(String pPeriodTo) {
        this.pPeriodTo = pPeriodTo;
    }

    public String getpPeriodType() {
        return pPeriodType;
    }

    public void setpPeriodType(String pPeriodType) {
        this.pPeriodType = pPeriodType;
    }
}

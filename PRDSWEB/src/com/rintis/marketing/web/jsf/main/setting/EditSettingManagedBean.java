package com.rintis.marketing.web.jsf.main.setting;

import com.rintis.marketing.beans.entity.SystemProperty;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class EditSettingManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<SystemProperty> listSystemProperty;
    private List<SystemProperty> listSystemPropertyColor;
    private SystemProperty emailAlertTo;
    private SystemProperty emailAlertSubject;
    private SystemProperty emailAlertMessage;
    private List<SystemProperty> listSettingEmail;
    private SystemProperty emailAlertToM3M4;
    private SystemProperty emailAlertSubjectM3M4;
    private SystemProperty emailAlertMessageM3M4;
    private List<SystemProperty> listSettingEmailM3M4;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            emailAlertTo = moduleFactory.getCommonService().getSystemPropertyEntity(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_TO);
            emailAlertSubject = moduleFactory.getCommonService().getSystemPropertyEntity(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_SUBJECT);
            emailAlertMessage = moduleFactory.getCommonService().getSystemPropertyEntity(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_MESSAGE);
            listSettingEmail = new ArrayList<>();
            listSettingEmail.add(emailAlertTo);
            listSettingEmail.add(emailAlertSubject);
            listSettingEmail.add(emailAlertMessage);

            emailAlertToM3M4 = moduleFactory.getCommonService().getSystemPropertyEntity(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_TO_M3M4);
            emailAlertSubjectM3M4 = moduleFactory.getCommonService().getSystemPropertyEntity(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_SUBJECT_M3M4);
            emailAlertMessageM3M4 = moduleFactory.getCommonService().getSystemPropertyEntity(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_MESSAGE_M3M4);
            listSettingEmailM3M4 = new ArrayList<>();
            listSettingEmailM3M4.add(emailAlertToM3M4);
            listSettingEmailM3M4.add(emailAlertSubjectM3M4);
            listSettingEmailM3M4.add(emailAlertMessageM3M4);

            List<String> listExclude = new ArrayList<>();
            listExclude.add(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_TO);
            listExclude.add(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_SUBJECT);
            listExclude.add(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_MESSAGE);
            listExclude.add(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_TO_M3M4);
            listExclude.add(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_SUBJECT_M3M4);
            listExclude.add(DataConstant.SYSTEM_PROPERTY_EMAIL_ALERT_MESSAGE_M3M4);

            listSystemProperty = moduleFactory.getCommonService().listSystemPropertyNotPerUser(listExclude);
            listSystemPropertyColor = moduleFactory.getCommonService().listSystemPropertyPerUser("color");

            for(SystemProperty sp : listSystemPropertyColor) {
                sp.setTempValue(sp.getSystemValue());
            }

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String saveAction() {
        try {
            //validation
            for(SystemProperty sp : listSystemProperty) {
                if (sp.getSystemName().equalsIgnoreCase("smtp_port")) {
                    try {
                        Integer.parseInt(sp.getSystemValue());
                    } catch (Exception ex) {
                        glowMessageError("SMTP_PORT must be numeric", "");
                        return "";
                    }
                }

                if (sp.getSystemName().equalsIgnoreCase("limit_year")) {
                    try {
                        Integer.parseInt(sp.getSystemValue());
                    } catch (Exception ex) {
                        glowMessageError("LIMIT_YEAR must be numeric", "");
                        return "";
                    }
                }

                if (sp.getSystemName().equalsIgnoreCase("top_n")) {
                    try {
                        Integer.parseInt(sp.getSystemValue());
                    } catch (Exception ex) {
                        glowMessageError("TOP_N must be numeric", "");
                        return "";
                    }
                }
            }

            moduleFactory.getCommonService().editSystemProperty(listSystemProperty);
            moduleFactory.getCommonService().editSystemProperty(listSystemPropertyColor);

            moduleFactory.getCommonService().editSystemProperty(emailAlertTo);
            moduleFactory.getCommonService().editSystemProperty(emailAlertSubject);
            moduleFactory.getCommonService().editSystemProperty(emailAlertMessage);

            moduleFactory.getCommonService().editSystemProperty(emailAlertToM3M4);
            moduleFactory.getCommonService().editSystemProperty(emailAlertSubjectM3M4);
            moduleFactory.getCommonService().editSystemProperty(emailAlertMessageM3M4);


            glowMessageInfo("Update Success", "");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            glowMessageInfo("Update Failed", "");
        }
        return "";
    }

    public String updateColor(SystemProperty item) {
        item.setTempValue(item.getSystemValue());
        return "";
    }

    public String copyColorToPicker(SystemProperty item) {
        item.setSystemValue(item.getTempValue());
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<SystemProperty> getListSystemProperty() {
        return listSystemProperty;
    }

    public void setListSystemProperty(List<SystemProperty> listSystemProperty) {
        this.listSystemProperty = listSystemProperty;
    }

    public List<SystemProperty> getListSystemPropertyColor() {
        return listSystemPropertyColor;
    }

    public void setListSystemPropertyColor(List<SystemProperty> listSystemPropertyColor) {
        this.listSystemPropertyColor = listSystemPropertyColor;
    }

    public SystemProperty getEmailAlertTo() {
        return emailAlertTo;
    }

    public void setEmailAlertTo(SystemProperty emailAlertTo) {
        this.emailAlertTo = emailAlertTo;
    }

    public SystemProperty getEmailAlertSubject() {
        return emailAlertSubject;
    }

    public void setEmailAlertSubject(SystemProperty emailAlertSubject) {
        this.emailAlertSubject = emailAlertSubject;
    }

    public SystemProperty getEmailAlertMessage() {
        return emailAlertMessage;
    }

    public void setEmailAlertMessage(SystemProperty emailAlertMessage) {
        this.emailAlertMessage = emailAlertMessage;
    }

    public List<SystemProperty> getListSettingEmail() {
        return listSettingEmail;
    }

    public void setListSettingEmail(List<SystemProperty> listSettingEmail) {
        this.listSettingEmail = listSettingEmail;
    }

    public SystemProperty getEmailAlertToM3M4() {
        return emailAlertToM3M4;
    }

    public void setEmailAlertToM3M4(SystemProperty emailAlertToM3M4) {
        this.emailAlertToM3M4 = emailAlertToM3M4;
    }

    public SystemProperty getEmailAlertSubjectM3M4() {
        return emailAlertSubjectM3M4;
    }

    public void setEmailAlertSubjectM3M4(SystemProperty emailAlertSubjectM3M4) {
        this.emailAlertSubjectM3M4 = emailAlertSubjectM3M4;
    }

    public SystemProperty getEmailAlertMessageM3M4() {
        return emailAlertMessageM3M4;
    }

    public void setEmailAlertMessageM3M4(SystemProperty emailAlertMessageM3M4) {
        this.emailAlertMessageM3M4 = emailAlertMessageM3M4;
    }

    public List<SystemProperty> getListSettingEmailM3M4() {
        return listSettingEmailM3M4;
    }

    public void setListSettingEmailM3M4(List<SystemProperty> listSettingEmailM3M4) {
        this.listSettingEmailM3M4 = listSettingEmailM3M4;
    }
}

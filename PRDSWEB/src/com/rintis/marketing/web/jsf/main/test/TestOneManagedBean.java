package com.rintis.marketing.web.jsf.main.test;


import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@ManagedBean
@ViewScoped
public class TestOneManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<TestDateBean> listDate;
    private Date selectedDate;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listDate = new ArrayList<>();
            
            selectedDate = new Date();

            submitAction();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String submitAction() {
        try {
            String[] daystr = {"", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
            listDate = new ArrayList<>();

            Calendar cal = Calendar.getInstance();
            cal.setTime(selectedDate);
            int maxday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            //log.info(maxday);

            for(int i=1; i <= 7; i++) {
                TestDateBean db = new TestDateBean();
                db.setHeader(1);
                db.setDayOfWeekString(daystr[i]);
                listDate.add(db);
            }

            for(int i=1; i <= maxday; i++) {
                cal = Calendar.getInstance();
                cal.setTime(selectedDate);
                cal.set(Calendar.DAY_OF_MONTH, i);
                int dayofweek = cal.get(Calendar.DAY_OF_WEEK);

                if (i == 1) {
                    if (dayofweek > 1) {
                        for(int x = 1; x <= dayofweek-1; x++) {
                            TestDateBean db = new TestDateBean();
                            db.setHeader(0);
                            listDate.add(db);
                        }
                    }
                }

                TestDateBean db = new TestDateBean();
                db.setDay(i);
                db.setMonth(cal.get(Calendar.MONTH));
                db.setYear(cal.get(Calendar.YEAR));
                db.setHoliday(false);
                db.setDayOfWeek(dayofweek);
                db.setHeader(0);
                db.setColor("color: black;");
                if (dayofweek == 1) {
                    db.setColor("color: red;");
                }
                listDate.add(db);

                if (i == maxday) {
                    if (dayofweek < 7) {
                        for(int x=1; x <= (7-maxday); x++) {
                            db = new TestDateBean();
                            db.setHeader(0);
                            listDate.add(db);
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<TestDateBean> getListDate() {
        return listDate;
    }

    public void setListDate(List<TestDateBean> listDate) {
        this.listDate = listDate;
    }

    public Date getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(Date selectedDate) {
        this.selectedDate = selectedDate;
    }
}

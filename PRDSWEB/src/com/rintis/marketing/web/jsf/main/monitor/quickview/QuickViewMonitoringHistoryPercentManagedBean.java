package com.rintis.marketing.web.jsf.main.monitor.quickview;

import com.rintis.marketing.beans.bean.monitor.EsqTrxIdBean;
import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.StringUtils;
import com.rintis.marketing.web.jsf.main.monitor.AbstractQuickViewMonitoring;
import com.rintis.marketing.web.jsf.main.monitor.AbstractQuickViewMonitoringHistory;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;

@ManagedBean
@ViewScoped
public class QuickViewMonitoringHistoryPercentManagedBean extends AbstractQuickViewMonitoringHistory {

    private List<BankBean> listBank;
    private String render;
    private List<EsqTrxIdBean> listTrxId;
    private String graphRefreshInterval;
    private boolean showTable;
    private String showPercentButtonText;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            menuId = "0517";
            pageTitle = getMenuName(moduleFactory, menuId);

            String s = StringUtils.checkNull((String)getRequestParameterMap().get("bid"));
            if (s.length() > 0) {
                selectedBankId = s;
            }

            s = StringUtils.checkNull((String)getRequestParameterMap().get("trxid"));
            if (s.length() > 0) {
                selectedTrxId = s;
            }

            s = StringUtils.checkNull((String)getRequestParameterMap().get("tf"));
            if (s.length() > 0) {
                aggregateTimeFrame = s;
                if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T5M)) {
                    timeFrame = TIMEFRAME_5M;
                    defaultRangeInHour = Integer.parseInt(
                            moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                    DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_5M)
                    );
                } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T15M)) {
                    timeFrame = TIMEFRAME_15M;
                    defaultRangeInHour = Integer.parseInt(
                            moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                    DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_15M)
                    );
                } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T30M)) {
                    timeFrame = TIMEFRAME_30M;
                    defaultRangeInHour = Integer.parseInt(
                            moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                    DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_30M)
                    );
                } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T1H)) {
                    timeFrame = TIMEFRAME_1H;
                    defaultRangeInHour = Integer.parseInt(
                            moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                    DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_HOURLY)
                    );
                } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T1D)) {
                    timeFrame = TIMEFRAME_1D;
                    defaultRangeInHour = Integer.parseInt(
                            moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                    DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_1D)
                    );
                }
            } else {
                aggregateTimeFrame = DataConstant.T5M;
                timeFrame = TIMEFRAME_5M;
                defaultRangeInHour = Integer.parseInt(
                        moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_5M)
                );
            }

            activePercentPrev = new Boolean[3];
            for(int i=0; i<3; i++) {
                //activeCurr[i] = true;
                activePercentPrev[i] = true;
            }
            renderPrevDate = "";

            //checkAllCurr = false;
            checkAllPrev = false;
            selectedEndDate = new Date();
            selectedStartMinute = 0;
            selectedEndMinute = 0;
            selectedStartHour = 0;
            selectedEndHour = 0;

            history = true;
            renderResult = "false";
            showTable = false;
            graphRefreshInterval = DataConstant.DEFAULT_GRAPH_REFRESH_INTERVAL;

            render = "false";
            renderAcqOnly = "false";
            renderIssOnly = "false";
            renderBnfOnly = "false";
            renderAcqIss = "false";
            renderAcqBnf = "false";
            renderIssBnf = "false";
            showPercent = true;
            showPercentButtonText = "Show Current";


            Map<String, Object> paramBank = new HashMap<>();
            listBank = moduleFactory.getMasterService().listBankCA(paramBank);

            listTrxId = moduleFactory.getMasterService().listEsqTrxId();


            if (!isPostback()) {
                selectedPrevDate = DateUtils.addDayDate(new Date(), -1);
                createTlog(moduleFactory);
            }

            if (StringUtils.checkNull(selectedBankId).length() > 0 &&
                    StringUtils.checkNull(selectedTrxId).length() > 0 &&
                    StringUtils.checkNull(aggregateTimeFrame).length() > 0
                    ) {
                submitAction();
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }



    public String submitAction() {
        try {
            trxIdChangeListener();

            if (selectedBankId.length() == 0) {
                glowMessageError("Select Bank", "");
                return "";
            }
            if (selectedTrxId.length() == 0) {
                glowMessageError("Select Transaction", "");
                return "";
            }
            if (selectedTransactionIndicatorId == null) {
                glowMessageError("Select Transaction Indicator", "");
                return "";
            }
            if (selectedTransactionIndicatorId.length == 0) {
                glowMessageError("Select Transaction Indicator", "");
                return "";
            }

            if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T5M)) {
                timeFrame = TIMEFRAME_5M;
                defaultRangeInHour = Integer.parseInt(
                        moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_5M)
                );
            } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T15M)) {
                timeFrame = TIMEFRAME_15M;
                defaultRangeInHour = Integer.parseInt(
                        moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_15M)
                );
            } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T30M)) {
                timeFrame = TIMEFRAME_30M;
                defaultRangeInHour = Integer.parseInt(
                        moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_30M)
                );
            } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T1H)) {
                timeFrame = TIMEFRAME_1H;
                defaultRangeInHour = Integer.parseInt(
                        moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_HOURLY)
                );
            } else if (aggregateTimeFrame.equalsIgnoreCase(DataConstant.T1D)) {
                timeFrame = TIMEFRAME_1D;
                defaultRangeInHour = Integer.parseInt(
                        moduleFactory.getCommonService().getSystemPropertyUserDefault(getUserInfoBean().getUserId(),
                                DataConstant.SYSTEM_PROPERTY_DEFAULT_RANGE_1D)
                );
            }


            //validate time from, time to
            if (timeFrame != TIMEFRAME_1D) {
                if (selectedEndHour < selectedStartHour) {
                    glowMessageError("Time From > Time To", "");
                    return "";
                }
            }


            processSubmit();

            render = "true";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }


    public String showPercentChangeListener() {
        return "quickViewMonitoringHistory?faces-redirect=true&bid=" +
                StringUtils.checkNull(selectedBankId) + "&trxid=" + StringUtils.checkNull(selectedTrxId) +
                "&tf=" + StringUtils.checkNull(aggregateTimeFrame);
    }

    public String showHistory() {
        return "quickViewMonitoring?faces-redirect=true&bid=" +
                StringUtils.checkNull(selectedBankId) + "&trxid=" + StringUtils.checkNull(selectedTrxId) +
                "&tf=" + StringUtils.checkNull(aggregateTimeFrame);
    }





    public void onPoolListener() {
        try {
            String s = moduleFactory.getAggregateService().getAggrLastTime(selectedTrxId, aggregateTimeFrame);
            Date thdate = DateUtils.convertDate(s, DateUtils.DATE_YYMMDD_HHMM);
            selectedEndDate = thdate;

            selectedEndHour = DateUtils.getHour(thdate);
            selectedEndMinute = DateUtils.getMinute(thdate);

            Date frdate = DateUtils.addDate(Calendar.HOUR_OF_DAY, thdate, defaultRangeInHour*-1);

            selectedStartDate = frdate;
            selectedStartHour = DateUtils.getHour(frdate);
            selectedStartMinute = DateUtils.getMinute(frdate);


            submitAction();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }







    /*public void checkAllCurrListener() {
        for(int i=0; i < activeCurr.length; i++) {
            if (checkAllCurr) {
                activeCurr[i] = true;
            } else {
                activeCurr[i] = false;
            }
        }
    }

    public void checkAllPrevListener() {
        for(int i=0; i < activePrev.length; i++) {
            if (checkAllPrev) {
                activePrev[i] = true;
            } else {
                activePrev[i] = false;
            }
        }
    }*/

    public void checkPrevListener() {
        /*if (activePrev[0] || activePrev[1] || activePrev[2] || activePrev[3]) {
            renderPrevDate = "";
        } else {
            renderPrevDate = "visibility: hidden;";
        }*/
    }




    public List<BankBean> getListBank() {
        return listBank;
    }

    public void setListBank(List<BankBean> listBank) {
        this.listBank = listBank;
    }

    public String getRender() {
        return render;
    }

    public void setRender(String render) {
        this.render = render;
    }

    public List<EsqTrxIdBean> getListTrxId() {
        return listTrxId;
    }

    public void setListTrxId(List<EsqTrxIdBean> listTrxId) {
        this.listTrxId = listTrxId;
    }

    public String getGraphRefreshInterval() {
        return graphRefreshInterval;
    }

    public void setGraphRefreshInterval(String graphRefreshInterval) {
        this.graphRefreshInterval = graphRefreshInterval;
    }

    public boolean isShowTable() {
        return showTable;
    }

    public void setShowTable(boolean showTable) {
        this.showTable = showTable;
    }

    public String getShowPercentButtonText() {
        return showPercentButtonText;
    }

    public void setShowPercentButtonText(String showPercentButtonText) {
        this.showPercentButtonText = showPercentButtonText;
    }
}


package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.PanAnomalyParamBean;
import com.rintis.marketing.beans.entity.EisBank;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.HashMap;
import java.util.List;

/**
 * Created by aabraham on 04/03/2019.
 */
@ManagedBean
@ViewScoped
public class ListPanAnomalyAggregatePerBankParameterManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<PanAnomalyParamBean> listBankMaster;
    private List<com.rintis.marketing.beans.bean.report.BankBean> listBankReport;
    private String selectedBankId;
    private String link;
    private String bank;


    @Override
    @PostConstruct
    protected void initManagedBean() {

        try {

            listBankReport = moduleFactory.getMasterService().listBank(new HashMap<>());
//            selectedBankId = null;

        }catch(Exception e){
            log.error(e.getMessage(), e);
        }
    }

    public String submitBankID(){

        bank = getSelectedBankId();
        if(bank.isEmpty()){
            glowMessageError("Input Bank terlebih dahulu sebelum add parameter","");
            link = "";
        }else{
            /*add validation by aaw 13/-0/2021*/
            String p = "";
            try {
                p = moduleFactory.getMasterService().checkDataIsExist(bank);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }

            if (p != null && !"".equals(p)) {
                glowMessageError("Parameter bank terpilih sudah ada, silahkan lakukan update bukan add !","");
                link = "";
            } else {
                link = "addPanAnomalyAggregatePerBankParameter.do?faces-redirect=true&pBankId=" + selectedBankId;
            }
            /*End add validation*/
        }

        return link;

    }

    public void submitAction(){

        try {
            if(selectedBankId != ""){
                EisBank bank = moduleFactory.getMasterService().getBank(selectedBankId);
                listBankMaster = moduleFactory.getPanService().getParamBank(bank.getBankId());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    public String submitUpdate(){

        bank = getSelectedBankId();
        if(bank.isEmpty()){
            glowMessageError("Input Bank terlebih dahulu sebelum add parameter","");
            link = "";
        }else{
            link = "editPanAnomalyAggregatePerBankParameter.do?faces-redirect=true&pBankId=" + selectedBankId;
        }

        return link;

    }

    public String submitDelete(){

        try {

            moduleFactory.getPanService().deleteParamBank(getSelectedBankId());

        }catch(Exception e){
            log.error(e.getMessage(), e);
        }

        return PageConstant.PAGE_PARAM_PER_BANK_PARAM + "?faces-redirect=true";

    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }
    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public String getSelectedBankId() {
        return selectedBankId;
    }
    public void setSelectedBankId(String selectedBankId) {
        this.selectedBankId = selectedBankId;
    }

    public List<PanAnomalyParamBean> getListBankMaster() {
        return listBankMaster;
    }
    public void setListBankMaster(List<PanAnomalyParamBean> listBankMaster) {
        this.listBankMaster = listBankMaster;
    }

    public List<com.rintis.marketing.beans.bean.report.BankBean> getListBankReport() {
        return listBankReport;
    }
    public void setListBankReport(List<com.rintis.marketing.beans.bean.report.BankBean> listBankReport) {
        this.listBankReport = listBankReport;
    }

    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }

    public String getBank() {
        return bank;
    }
    public void setBank(String bank) {
        this.bank = bank;
    }
}

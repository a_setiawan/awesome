package com.rintis.marketing.web.jsf.main.menu;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.rintis.marketing.beans.entity.MenuRoleType;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;
import com.rintis.marketing.core.constant.DataConstant;

@ManagedBean
@ViewScoped
public class AddMenuRoleTypeManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    
    private MenuRoleType menuRoleType;
    
    @Override
    @PostConstruct
    protected void initManagedBean() {
        menuRoleType = new MenuRoleType();
    }
    
    public String saveAction() {
        try {
            if (menuRoleType.getMenuRoleName().length() == 0) {
                glowMessageError("Enter Role Name", "");
                return "";
            }
            MenuRoleType mrt = moduleFactory.getMenuService().getMenuRoleByName(menuRoleType.getMenuRoleName());
            if (mrt != null) {
                glowMessageError("Role Name already exist", "");
                menuRoleType.setMenuRoleName("");
                return "";
            }

            menuRoleType.setHidden(DataConstant.MENU_ROLE_TYPE_NOT_HIDDEN);
            moduleFactory.getMenuService().createMenuRoleType(menuRoleType);


            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_MENU_ROLE_TYPE_LIST_MENU_ROLE_TYPE + "?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public MenuRoleType getMenuRoleType() {
        return menuRoleType;
    }

    public void setMenuRoleType(MenuRoleType menuRoleType) {
        this.menuRoleType = menuRoleType;
    }





}

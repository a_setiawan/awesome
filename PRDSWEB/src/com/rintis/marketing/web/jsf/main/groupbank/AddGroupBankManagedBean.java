package com.rintis.marketing.web.jsf.main.groupbank;

import com.rintis.marketing.beans.bean.report.BankBean;
import com.rintis.marketing.beans.entity.EisBank;
import com.rintis.marketing.beans.entity.GroupBank;
import com.rintis.marketing.beans.entity.GroupBankMember;
import com.rintis.marketing.beans.entity.TmsBankCaBiller;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.*;

@ManagedBean
@ViewScoped
public class AddGroupBankManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private GroupBank groupBank;

    private List<BankComparableBean> listBankSource;
    private List<BankComparableBean> listBankSelected;
    private List<String> selectedOptionSource;
    private List<String> selectedOptionTarget;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            groupBank = new GroupBank();

            listBankSource = new ArrayList<>();
            listBankSelected = new ArrayList<>();
            //Map<String, Object> paramBank = new HashMap<>();
            //List<EisBank> list = moduleFactory.getMasterService().listBankEntity();
            Map<String, Object> paramBank = new HashMap<>();
            List<BankBean> list = moduleFactory.getMasterService().listBankCA(paramBank);
            for(BankBean eb : list) {
                BankComparableBean bcb = new BankComparableBean();
                bcb.setEisBank(eb);
                listBankSource.add(bcb);
            }

            listBankSelected = new ArrayList<>();



            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String saveAction() {
        try {
            if (groupBank.getGroupId().length() == 0) {
                glowMessageError("Enter Group Id", "");
                return "";
            }

            if (groupBank.getGroupName().length() == 0) {
                glowMessageError("Enter Group Name", "");
                return "";
            }

            List<String> list = new ArrayList<>();
            for(BankComparableBean bcb : listBankSelected) {
                list.add(bcb.getEisBank().getBankid());
            }

            moduleFactory.getMasterService().addGroupBank(groupBank, list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "/apps/groupbank/listGroupBank?faces-redirect=true";
    }


    public String addSelectedBankAction() {
        try {
            String bankstring = "";
            for (String s : selectedOptionSource) {
                log.info(s);
                BankBean bb = moduleFactory.getMasterService().getBankCA(s);
                BankComparableBean bcb = new BankComparableBean();
                bcb.setEisBank(bb);
                listBankSelected.add(bcb);
                bankstring = bankstring + "," + s;
            }
            Collections.sort(listBankSelected);

            int size = listBankSource.size();
            for(int i = size - 1; i >= 0; i--) {
                BankComparableBean bcb = listBankSource.get(i);
                if (bankstring.indexOf(bcb.getEisBank().getBankid()) > -1) {
                    listBankSource.remove(i);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public String removeSelectedBankAction() {
        try {
            String bankstring = "";
            for (String s : selectedOptionTarget) {
                log.info(s);
                BankBean bb = moduleFactory.getMasterService().getBankCA(s);
                BankComparableBean bcb = new BankComparableBean();
                bcb.setEisBank(bb);
                listBankSource.add(bcb);
                bankstring = bankstring + "," + s;
            }
            Collections.sort(listBankSource);

            int size = listBankSelected.size();
            for(int i = size - 1; i >= 0; i--) {
                BankComparableBean bcb = listBankSelected.get(i);
                if (bankstring.indexOf(bcb.getEisBank().getBankid()) > -1) {
                    listBankSelected.remove(i);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public GroupBank getGroupBank() {
        return groupBank;
    }

    public void setGroupBank(GroupBank groupBank) {
        this.groupBank = groupBank;
    }

    public List<BankComparableBean> getListBankSource() {
        return listBankSource;
    }

    public void setListBankSource(List<BankComparableBean> listBankSource) {
        this.listBankSource = listBankSource;
    }

    public List<BankComparableBean> getListBankSelected() {
        return listBankSelected;
    }

    public void setListBankSelected(List<BankComparableBean> listBankSelected) {
        this.listBankSelected = listBankSelected;
    }

    public List<String> getSelectedOptionSource() {
        return selectedOptionSource;
    }

    public void setSelectedOptionSource(List<String> selectedOptionSource) {
        this.selectedOptionSource = selectedOptionSource;
    }

    public List<String> getSelectedOptionTarget() {
        return selectedOptionTarget;
    }

    public void setSelectedOptionTarget(List<String> selectedOptionTarget) {
        this.selectedOptionTarget = selectedOptionTarget;
    }
}

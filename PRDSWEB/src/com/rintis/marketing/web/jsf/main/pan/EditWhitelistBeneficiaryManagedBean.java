package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.WhitelistBean;
import com.rintis.marketing.beans.bean.bi.WhitelistBeneficiaryBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.common.ParamConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by aabraham on 13/02/2019.
 */
@ManagedBean
@ViewScoped
public class EditWhitelistBeneficiaryManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private List<WhitelistBeneficiaryBean> listWhitelistBeneficiary;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        String account = (String)getRequestParameterMap().get("wlAccount");
        String penerima = (String)getRequestParameterMap().get("wlPenerima");

        if (account != null) {
            try {
                listWhitelistBeneficiary = moduleFactory.getPanService().getWhitelistBeneficiary(account.trim(), penerima.trim());
                if (!isPostback()) {
                    createTlog(moduleFactory);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public String editAction() {
        try {
            if (listWhitelistBeneficiary.get(0).getAccount().length() == 0) {
                glowMessageError("Data Not Valid, 'Account No' Is Blank!", "");
                return "";
            }
            String account = listWhitelistBeneficiary.get(0).getAccount();

            if (listWhitelistBeneficiary.get(0).getPenerima().length() == 0) {
                glowMessageError("Data Not Valid, Receiver Is Blank!", "");
                return "";
            }
            String penerima = listWhitelistBeneficiary.get(0).getPenerima();
            BigDecimal freqApp = listWhitelistBeneficiary.get(0).getFreqApp();
            BigDecimal freqDc = listWhitelistBeneficiary.get(0).getFreqDc();
            BigDecimal freqDcFree = listWhitelistBeneficiary.get(0).getFreqDcFree();
            BigDecimal freqRvsl = listWhitelistBeneficiary.get(0).getFreqRvsl();
            BigDecimal totalAmtApp = listWhitelistBeneficiary.get(0).getTotalAmtApp();
            BigDecimal totalAmtDc = listWhitelistBeneficiary.get(0).getTotalAmtDc();
            BigDecimal totalAmtDcFree = listWhitelistBeneficiary.get(0).getTotalAmtDcFree();
            BigDecimal totalAmtRvsl = listWhitelistBeneficiary.get(0).getTotalAmtRvsl();
            BigDecimal totalAmt = listWhitelistBeneficiary.get(0).getTotalAmt();
            String active = listWhitelistBeneficiary.get(0).getActive();

            moduleFactory.getPanService().updateBeneficiary(account, penerima.trim(), freqApp, freqDc, freqDcFree, freqRvsl, totalAmtApp, totalAmtDc, totalAmtDcFree, totalAmtRvsl, totalAmt, active);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_WHITELIST_BENE_WHITELIST_BENE + "?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<WhitelistBeneficiaryBean> getListWhitelistBeneficiary() {
        return listWhitelistBeneficiary;
    }

    public void setListWhitelistBeneficiary(List<WhitelistBeneficiaryBean> listWhitelistBeneficiary) {
        this.listWhitelistBeneficiary = listWhitelistBeneficiary;
    }
}

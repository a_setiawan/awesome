package com.rintis.marketing.web.jsf.main.mcc;

import com.rintis.marketing.beans.bean.bi.PanAnomalyEmailBean;
import com.rintis.marketing.beans.entity.MCCList;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * Created by aabraham on 19/04/2021.
 */

@ManagedBean
@ViewScoped
public class EditMCCManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private MCCList listMCC;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        String mcc = (String)getRequestParameterMap().get("uid");
        if (mcc != null) {
            try {
                listMCC = moduleFactory.getMasterService().getMcc(mcc);
                if (!isPostback()) {
                    createTlog(moduleFactory);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public String editAction() {
        try {
            String mcc = listMCC.getMcc();
            String description = listMCC.getDescription();

            moduleFactory.getMasterService().updateMcc(mcc, description);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_MASTER_MCC + "?faces-redirect=true";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public MCCList getListMCC() {
        return listMCC;
    }

    public void setListMCC(MCCList listMCC) {
        this.listMCC = listMCC;
    }
}

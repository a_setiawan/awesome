package com.rintis.marketing.web.jsf.main.dashboard;

import com.rintis.marketing.beans.bean.bi.C3ChartBean;
import com.rintis.marketing.beans.bean.bi.UserDashboardDetailBean;
import com.rintis.marketing.beans.entity.MktUserDashboard;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.DateUtils;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.core.utils.MessagesResourceBundle;
import com.rintis.marketing.core.utils.StringUtils;
import org.primefaces.event.ItemSelectEvent;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@ManagedBean
@ViewScoped
public class UserDashboardHistoryManagedBean extends AbstractUserDashboardHistory {

    private MktUserDashboard mktUserDashboard;
    private List<UserDashboardDetailBean> listDetail;
    private BigDecimal id;





    private String graphRefreshInterval;
    private boolean showTable;
    //private boolean disableAutoRefresh;
    //private String disableAutoRefreshString;


    //private List<LineChartModel> listLineChardModel;





    //private Boolean[] activeCurr;

    //private boolean checkAllCurr;
    //private boolean checkAllPrev;


    //private boolean checkAllPercentPrev;
    private String renderPrevDate;





    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            //disableAutoRefresh = false;
            //disableAutoRefreshString = "true";
            showTable = false;
            renderPrevDate = "";

            //activeCurr = new Boolean[4];
            activePrev = new Boolean[4];
            for(int i=0; i<4; i++) {
                //activeCurr[i] = true;
                activePrev[i] = true;
            }
            //checkAllCurr = false;
            //checkAllPrev = false;

            activePercentPrev = new Boolean[3];
            for(int i=0; i<3; i++) {
                activePercentPrev[i] = false;
            }
            //checkAllPercentPrev = false;


            showPrevData= true;
            selectedEndDate = new Date();
            selectedStartMinute = 0;
            selectedEndMinute = 0;
            selectedStartHour = 0;
            selectedEndHour = 6;

            listC3Chart = new ArrayList<>();
            graphRefreshInterval = DataConstant.DEFAULT_GRAPH_REFRESH_INTERVAL;

            showPercent = false;
            showPercentButtonText = MessagesResourceBundle.getMessage("show_percent");


            //String sid = (String)getRequestParameterMap().get("id");
            //if (sid == null) sid = "-1";
            //id = new BigDecimal(sid);
            String xp = StringUtils.checkNull((String)getRequestParameterMap().get("xp"));
            String par = HttpUtils.encodeUrlParam(xp, getUserInfoBean().getUserId(), getHttpSession().getId());
            Map<String, String> mapParam = HttpUtils.extractUrlParam(par);
            String sid = mapParam.get("id");
            if (sid == null) sid = "-1";
            id = new BigDecimal(sid);

            mktUserDashboard = moduleFactory.getBiService().getUserDashboard(id);
            listDetail = moduleFactory.getBiService().listUserDashboardDetailBean(id);

            //validasi role


            if (!isPostback()) {
                selectedPrevDate = DateUtils.addDayDate(new Date(), -1);
                createTlog(moduleFactory);
            }


            poolAction();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void checkBoxShowPrevDataListener() {
    }

    public String searchAction() {
        poolAction();
        return "";
    }

    public String onPoolAction() {
        poolAction();
        return "";
    }

    public String showPrevDataAction() {
        log.info(showPrevData);
        log.info(selectedPrevDate);
        poolAction();
        return "";
    }

    public String showCurrentAction() {
        //encrypt param
        String urlParam = "id=" + id.toPlainString();
        urlParam = HttpUtils.decodeUrlParam(urlParam, getUserInfoBean().getUserId(), getHttpSession().getId());
        urlParam = "xp=" + urlParam;
        return "userDashboard?faces-redirect=true&" + urlParam;
    }

    public String submitAction() {
        poolAction();
        return "";
    }

    public void poolAction() {
        try {
            if (showPercent) {
                showPercentButtonText = MessagesResourceBundle.getMessage("show_total");
            } else {
                showPercentButtonText = MessagesResourceBundle.getMessage("show_percent");
            }

            log.info(showPrevData);
            log.info(selectedPrevDate);

            listDetail = moduleFactory.getBiService().listUserDashboardDetailBean(id);

            listC3Chart = new ArrayList<>();


            listTableData = new ArrayList<>();
            for(UserDashboardDetailBean detail : listDetail) {
                renderChart(detail);
            }

            int size = listC3Chart.size();
            renderChartArray = new String[DataConstant.USER_DASHBOARD_MAX_CHART];
            chartBeanArray = new C3ChartBean[DataConstant.USER_DASHBOARD_MAX_CHART];
            for(int i=0; i < DataConstant.USER_DASHBOARD_MAX_CHART; i++) {
                renderChartArray[i] = "false";
            }
            for(int i=0; i < size; i++) {
                renderChartArray[i] = "true";
                chartBeanArray[i] = listC3Chart.get(i);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
	
	public void itemSelect(ItemSelectEvent event) {
    }

    public void itemClick() {

    }

    public void disableAutoRefreshChangeListener() {
    }

    public String showPercentChangeListener() {
        //encrypt param
        String urlParam = "id=" + id.toPlainString();
        urlParam = HttpUtils.decodeUrlParam(urlParam, getUserInfoBean().getUserId(), getHttpSession().getId());
        urlParam = "xp=" + urlParam;
        return "userDashboardHistoryPercent?faces-redirect=true&" + urlParam;
    }










    /*public void checkAllCurrListener() {
        for(int i=0; i < activeCurr.length; i++) {
            if (checkAllCurr) {
                activeCurr[i] = true;
            } else {
                activeCurr[i] = false;
            }
        }
    }

    public void checkAllPrevListener() {
        for(int i=0; i < activePrev.length; i++) {
            if (checkAllPrev) {
                activePrev[i] = true;
            } else {
                activePrev[i] = false;
            }
        }
    }*/

    public void checkPrevListener() {
        /*if (activePrev[0] || activePrev[1] || activePrev[2] || activePrev[3]) {
            renderPrevDate = "";
        } else {
            renderPrevDate = "visibility: hidden;";
        }*/
    }


    public MktUserDashboard getMktUserDashboard() {
        return mktUserDashboard;
    }

    public void setMktUserDashboard(MktUserDashboard mktUserDashboard) {
        this.mktUserDashboard = mktUserDashboard;
    }

    public List<UserDashboardDetailBean> getListDetail() {
        return listDetail;
    }

    public void setListDetail(List<UserDashboardDetailBean> listDetail) {
        this.listDetail = listDetail;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }



    public String getGraphRefreshInterval() {
        return graphRefreshInterval;
    }

    public void setGraphRefreshInterval(String graphRefreshInterval) {
        this.graphRefreshInterval = graphRefreshInterval;
    }






    public boolean isShowTable() {
        return showTable;
    }

    public void setShowTable(boolean showTable) {
        this.showTable = showTable;
    }



    public String getRenderPrevDate() {
        return renderPrevDate;
    }

    public void setRenderPrevDate(String renderPrevDate) {
        this.renderPrevDate = renderPrevDate;
    }




}

package com.rintis.marketing.web.jsf.main.pan;

import com.rintis.marketing.beans.bean.bi.WhitelistBeneficiaryBean;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.common.PageConstant;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 * Created by aaw on 20/05/21.
 */

@ManagedBean
@ViewScoped
public class AddWhitelistBeneficiaryManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;
    private WhitelistBeneficiaryBean whitelistBeneficiaryBean;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        whitelistBeneficiaryBean = new WhitelistBeneficiaryBean();
    }

    public boolean isAlpha(String acct) {
        boolean res = false;
        for (char c : acct.toCharArray()) {
            if((Character.isLetter(c))) {
                System.out.println("Is a character! " + c);
                res = true;
                break;
            }
        }
        return res;
    }

    public boolean containsDigit(String acct) {
        boolean res = false;
        if (acct != null && !acct.isEmpty()) {
            for (char c :  acct.toCharArray()) {
                if(Character.isDigit(c)) {
                    res = true;
                    break;
                }
            }
        }
        return res;
    }

    public String saveAction() {
        try {
            if (whitelistBeneficiaryBean.getAccount().length() == 0) {
                glowMessageError("Enter Account No", "");
                return "";
            }

            if (isAlpha(whitelistBeneficiaryBean.getAccount().trim()) || !containsDigit(whitelistBeneficiaryBean.getAccount().trim())) {
                glowMessageError("'Account No' Must Consist Of 0-9 and -", "Sample : 014-XXX");
                return "";
            }

            if (whitelistBeneficiaryBean.getPenerima().length() == 0) {
                glowMessageError("Receiver Is Required!", "");
                return "";
            }

            boolean chk = moduleFactory.getPanService().checkBeneficiaryReceiverUsed(whitelistBeneficiaryBean.getAccount().trim(), whitelistBeneficiaryBean.getPenerima().trim());
            if (chk) {
                glowMessageError("Account No and Receiver already exist", "");
                return "";
            }

             /*Add validation by aaw 10/08/2021*/
            if (!isValid(whitelistBeneficiaryBean.getAccount())) {
                glowMessageError("Account Not Valid", "Account No Can't Start With '-' ");
                return "";
            }
            /*End add*/

            String act = whitelistBeneficiaryBean.getActive().toString();

            moduleFactory.getPanService().saveBeneficiary(whitelistBeneficiaryBean.getAccount().trim(), (whitelistBeneficiaryBean.getPenerima()).trim(), whitelistBeneficiaryBean.getFreqApp(),
                    whitelistBeneficiaryBean.getFreqDc(), whitelistBeneficiaryBean.getFreqDcFree(), whitelistBeneficiaryBean.getFreqRvsl(), whitelistBeneficiaryBean.getTotalAmtApp(),
                    whitelistBeneficiaryBean.getTotalAmtDc(), whitelistBeneficiaryBean.getTotalAmtDcFree(), whitelistBeneficiaryBean.getTotalAmtRvsl(), whitelistBeneficiaryBean.getTotalAmt(), act);

            if (!isPostback()) {
                createTlog(moduleFactory);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return PageConstant.PAGE_WHITELIST_BENE_WHITELIST_BENE + "?faces-redirect=true";
    }

    /*Add new method by aaw 10/08/2021*/
    public boolean isValid(String input) {
        boolean result = true;
        if (!"".equals(input.trim())) {
            if ((input.trim()).startsWith("-")) {
                result = false;
            }
        }

        return result;
    }
    /*End add*/

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public WhitelistBeneficiaryBean getWhitelistBeneficiaryBean() {
        return whitelistBeneficiaryBean;
    }

    public void setWhitelistBeneficiaryBean(WhitelistBeneficiaryBean whitelistBeneficiaryBean) {
        this.whitelistBeneficiaryBean = whitelistBeneficiaryBean;
    }
}

package com.rintis.marketing.web.jsf.main.mcc;

import com.rintis.marketing.beans.entity.MCCParameter;
import com.rintis.marketing.core.SpringBeanConstant;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.web.jsf.AbstractManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.util.List;

/**
 * Created by aabraham on 22/04/2021.
 */
@ManagedBean
@ViewScoped
public class ListMCCParameterManagedBean extends AbstractManagedBean {
    @ManagedProperty("#{" + SpringBeanConstant.SPRING_BEAN_MODULE_FACTORY + "}")
    private ModuleFactory moduleFactory;

    private List<MCCParameter> listMCCParameter;
    private String selectedDeleteItem;
    private String selectedMCCParameter;

    @Override
    @PostConstruct
    protected void initManagedBean() {
        try {
            listMCCParameter = moduleFactory.getMasterService().listMccParameter("-1");
            if (!isPostback()) {
                createTlog(moduleFactory);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    public void deleteMccActionListener(ActionEvent event) {
        try {
            moduleFactory.getMasterService().deleteMccParameter(selectedDeleteItem);
            listMCCParameter = moduleFactory.getMasterService().listMccParameter("-1");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String searchAction() {
        try {
            listMCCParameter = moduleFactory.getMasterService().listMccParameter(selectedMCCParameter);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public ModuleFactory getModuleFactory() {
        return moduleFactory;
    }

    public void setModuleFactory(ModuleFactory moduleFactory) {
        this.moduleFactory = moduleFactory;
    }

    public List<MCCParameter> getListMCCParameter() {
        return listMCCParameter;
    }

    public void setListMCCParameter(List<MCCParameter> listMCCParameter) {
        this.listMCCParameter = listMCCParameter;
    }

    public String getSelectedDeleteItem() {
        return selectedDeleteItem;
    }

    public void setSelectedDeleteItem(String selectedDeleteItem) {
        this.selectedDeleteItem = selectedDeleteItem;
    }

    public String getSelectedMCCParameter() {
        return selectedMCCParameter;
    }

    public void setSelectedMCCParameter(String selectedMCCParameter) {
        this.selectedMCCParameter = selectedMCCParameter;
    }
}

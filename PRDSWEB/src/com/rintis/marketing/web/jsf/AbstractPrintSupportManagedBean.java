package com.rintis.marketing.web.jsf;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public abstract class AbstractPrintSupportManagedBean extends AbstractManagedBean {



    protected void generateXls(HSSFWorkbook workbook, String filename) {
        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();

            response.reset();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment; filename=\"" + filename + "\"");

            workbook.write(response.getOutputStream());
            facesContext.responseComplete();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public abstract String printAction();

    public String getCompanyInfo() {
        StringBuffer companyinfo = new StringBuffer("");

        return companyinfo.toString();
    }
    
}

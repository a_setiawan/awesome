package com.rintis.marketing.web.jsf;

import com.rintis.marketing.beans.bean.UserInfoBean;
import com.rintis.marketing.beans.entity.Menu;
import com.rintis.marketing.beans.entity.TransactionLog;
import com.rintis.marketing.core.app.main.module.ModuleFactory;
import com.rintis.marketing.core.constant.DataConstant;
import com.rintis.marketing.core.utils.HttpUtils;
import com.rintis.marketing.web.common.ParamConstant;
import com.rintis.marketing.web.common.SessionConstant;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

    public abstract class AbstractManagedBean  {
    protected Logger log = Logger.getLogger(this.getClass());

    private final String defaultTableRow = "20";
    private final String defaultCalendarDateFormat = "dd/MM/yyyy";
    private final String defaultCalendarDateTimeFormat = "dd/MM/yyyy HH:mm";

    protected String menuId;
    protected String pageTitle;
    protected String showTable;

    @PostConstruct
    abstract protected void initManagedBean();

    public void createTlog(ModuleFactory moduleFactory) throws Exception {
        if (getUserInfoBean() == null) return;

        TransactionLog tlog = new TransactionLog();
        tlog.setUserId(getUserInfoBean().getUserId());
        tlog.setDescription(((HttpServletRequest) getFacesContext().getExternalContext().getRequest() ).getRequestURL().toString());
        moduleFactory.getCommonService().createTransLog(tlog);
    }
    

    public void addMessage(Severity severity, String summary, String detail) {
        FacesMessage message = null;
        if (severity == null) {
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        } else {
            message = new FacesMessage(severity, summary, detail);
        }
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String getMenuName(ModuleFactory moduleFactory, String menuId) throws Exception {
        Menu menu = moduleFactory.getMenuService().getMenu(menuId);
        if (menu != null) {
            return menu.getMenuName();
        }
        return "";
    }

    protected FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    protected Map getSessionMap() {
        return getFacesContext().getExternalContext().getSessionMap();
    }

    protected HttpSession getHttpSession() {
        HttpSession session = (HttpSession) getFacesContext().getExternalContext().getSession(false);
        return session;
    }

    protected Map getRequestParameterMap() {
        return getFacesContext().getExternalContext().getRequestParameterMap();
    }
    
    public UserInfoBean getUserInfoBean() {
        return (UserInfoBean)getFacesContext().getExternalContext().getSessionMap().get(SessionConstant.SESSION_USERINFOBEAN);
    }

    public static boolean isPostback() {
        return FacesContext.getCurrentInstance().isPostback();
    }


    protected void glowMessage(Severity severity, String summary, String detail) {
        getFacesContext().addMessage(ParamConstant.GLOW_MESSAGE, new FacesMessage(severity, summary, detail));
    }
    
    protected void glowMessageError(String summary, String detail) {
        glowMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
    }
    
    protected void glowMessageInfo(String summary, String detail) {
        glowMessage(FacesMessage.SEVERITY_INFO, summary, detail);
    }

    public String getDefaultTableRow() {
        return defaultTableRow;
    }

    public String getDefaultCalendarDateFormat() {
        return defaultCalendarDateFormat;
    }

    public String getDefaultCalendarDateTimeFormat() {
        return defaultCalendarDateTimeFormat;
    }

    protected void redirectPageMessage(String uri, String messageSummary, String messageDetail) {
        try {
            getFacesContext().getExternalContext().redirect(uri);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    protected void generateXls(HSSFWorkbook workbook, String filename) {
        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();

            response.reset();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment; filename=\"" + filename + "\"");

            workbook.write(response.getOutputStream());
            facesContext.responseComplete();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    protected String getIndicatorName(String indicatorId) {
        if (indicatorId.equalsIgnoreCase(DataConstant.ACQ)) {
            return "Acquirer";
        } else if (indicatorId.equalsIgnoreCase(DataConstant.ISS)) {
            return "Issuer";
        } else if (indicatorId.equalsIgnoreCase(DataConstant.BNF)) {
            return "Beneficiary";
        } else {
            return "";
        }
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String paramEncodeUrl(String data){
        if(!data.equals(null)){
            data = HttpUtils.decodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }

    public String paramDecodeUrl(String data){
        if(!data.equals(null)){
            data = HttpUtils.encodeUrlParam(data, getUserInfoBean().getUserId(), getHttpSession().getId());
        }
        return data;
    }
}

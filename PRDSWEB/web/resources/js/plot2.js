//****************************************************************
//****************************************************************
//****************************************************************
function extLegend()
{
    this.cfg.legend= {
        show: true,
        location: 's',
        placement: 'outsideGrid',
        renderer: $.jqplot.EnhancedLegendRenderer,
        rendererOptions: {
            numberColumns: 5
        }
     };

    this.cfg.grid = {
        background: 'rgba(57,57,57,0.0)',
        drawBorder: false,
        shadow: false,
        gridLineColor: '#666666',
        gridLineWidth: 1
    };

    this.cfg.seriesDefaults = {
        fill: false,
        showMarker : false,
        lineWidth: 2,
		breakOnNull: true,

        markerOptions: {
            show: false
        },

        rendererOptions: {
            smooth: true
        }
    }

    /*this.cfg.series = [
        {
            fill: false,
            showMarker : true
        }
    ]*/

    this.cfg.axes = {
        yaxis: {
            //renderer: $.jqplot.LogAxisRenderer,
            pad: 0,
            rendererOptions: {
                minorTicks: 0
            },
            tickOptions: {
                //%'d = integer, %'#.2f desimal
                formatString: "%'#.2f",
                showMark: false,
                formatter: tickFormatter
            },
            min: 0
        }
    };

    /*this.cfg.highlighter = {
        sizeAdjust: 10,
        tooltipLocation: 'n',
        tooltipAxes: 'y',
        useAxesFormatters: false
    };

    this.cfg.cursor = {
        show: true,
        zoom:true
    };*/
}


tickFormatter = function (format, val) {
    console.log(val);
    val = val.toFixed(2);
    if (val >= 1000000000) {
        val = val / 1000000000;
        return val.toFixed(1)+"G";
    }
    if (val >= 1000000) {
        val = val / 1000000;
        return val.toFixed(1)+"M";
    }
    if (val >= 1000) {
        val = val / 1000;
        if (val < 10) {
            return val.toFixed(1)+"K";
        }
        return val.toFixed(0)+"K";
    }
    return val;
}






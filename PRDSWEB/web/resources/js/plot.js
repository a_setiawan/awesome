function extLegend()
{
    this.cfg.legend= {
        show: true,
        location: 'e',
        placement: 'outsideGrid',
        renderer: $.jqplot.EnhancedLegendRenderer,
        rendererOptions: {
            numberColumns: 1
        }
    };

    /*this.cfg.seriesDefaults = {
        breakOnNull: true
    }*/

    this.cfg.highlighter = {
        show : true,
        sizeAdjust: 10,
        //tooltipLocation: 'n',
        tooltipAxes: 'y',
        useAxesFormatters: true,
        showTooltip : true
    };
}

function extLegend3()
{
    this.cfg.legend= {
        show: true,
        location: 's',
        placement: 'outsideGrid',
        renderer: $.jqplot.EnhancedLegendRenderer,
        rendererOptions: {
            numberColumns: 8
        }
    };

    /*this.cfg.seriesDefaults = {
     breakOnNull: true
     }*/

    this.cfg.highlighter = {
        show : true,
        sizeAdjust: 10,
        //tooltipLocation: 'n',
        tooltipAxes: 'y',
        useAxesFormatters: true,
        showTooltip : true
    };
}

function extNoLegend()
{
    this.cfg.legend= {
        show: false
    };

    /*this.cfg.seriesDefaults = {
     breakOnNull: true
     }*/

    this.cfg.highlighter = {
        show : true,
        sizeAdjust: 10,
        //tooltipLocation: 'n',
        tooltipAxes: 'y',
        useAxesFormatters: true,
        showTooltip : true
    };
}


function extLegend2()
{
    this.cfg.axes = {
        axes:{
            xaxis:{

                tickOptions:{
                    formatString:'%b&nbsp;%#d'
                }
            },
            yaxis:{
                tickOptions:{
                    formatString:'$%.2f'
                }
            }
        },
        highlighter: {
            show: true,
            sizeAdjust: 7.5
        },
        cursor: {
            show: false
        }
    };
}


